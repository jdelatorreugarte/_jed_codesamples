package com
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.events.Event;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class Main_abn12098_3x2 extends Sprite
	{
		private var sparkler:Sparkler;
		var isi_start:Number
		var gradeWidth:int;
		var contentRange:Number;
		var scrollRange:Number;
		var loopCount:Number;
		var scrollOffset:Number;
		var scrollDuration:Number;
		var timerUP:Timer = new Timer(100); //run 3 times a second
		var timerDOWN:Timer = new Timer(100); //run 3 times a second
		var currentScrollPosition:Number;// set up for the restart animation function
		var durationRemainingPercent:Number;// set up for the restart animation function
		var durationRemaining:Number;// set up for the restart animation function
		var yDiff:Number;// set up for the restart animation function
		var amountScrolled:Number;// set up for the restart animation function
		
		
		public function Main_abn12098_3x2()
		{
			isi_start = isi_main.y;
			loopCount = 0;
			pi_btn.buttonMode=true;
			pi_btn.mouseChildren = false;
			pi_btn.addEventListener(MouseEvent.CLICK, piClick);
			isi_main.isi_pi_btn.addEventListener(MouseEvent.CLICK, piClick);
			main_btn.addEventListener(MouseEvent.CLICK, onClick);
			timerUP.addEventListener(TimerEvent.TIMER, scrollUP, false, 0, true); // TIMER!!up
			timerDOWN.addEventListener(TimerEvent.TIMER, scrollDOWN, false, 0, true); // TIMER!!DOWN
			up_arrow.addEventListener(MouseEvent.MOUSE_DOWN, upButtonDown);
			down_arrow.addEventListener(MouseEvent.MOUSE_DOWN, downButtonDown);
			up_arrow.addEventListener(MouseEvent.MOUSE_UP, upButtonUp);
			down_arrow.addEventListener(MouseEvent.MOUSE_UP, downButtonUp);
			main_btn.buttonMode=true;
			init();
			
		}
		
		private function upButtonDown(e:MouseEvent):void{
			timerUP.start();
		}
		
		private function downButtonDown(e:MouseEvent):void{
			timerDOWN.start();
		}
		
		private function upButtonUp(e:MouseEvent):void{
			timerUP.stop();
			restartAnimation();
		}
		
		private function downButtonUp(e:MouseEvent):void{
			timerDOWN.stop();
			restartAnimation();
		}
		
		private function restartAnimation():void //For when the button is released
		{
			yDiff =  (isi_main.y - isi_start) * -1;
			amountScrolled = yDiff/scrollOffset;
			durationRemaining = scrollDuration - (scrollDuration*amountScrolled);
			TweenLite.to(isi_main, durationRemaining, {y:-(contentRange), ease:Linear.easeNone, onComplete:moneyShot});
		}
		
		private function scrollUP(e:Event = null):void {
			if (isi_main.y >= isi_start - 10)
			{
				isi_main.y = isi_main.y;
				TweenLite.killTweensOf(isi_main);
				return
			};
			isi_main.y += 20;
			TweenLite.killTweensOf(isi_main);
		}
		
		private function scrollDOWN(e:Event = null):void {
			if (isi_main.y <= -(contentRange) + 10)
			{
				isi_main.y = isi_main.y;
				TweenLite.killTweensOf(isi_main);
				return
			};
			isi_main.y -= 20;
			TweenLite.killTweensOf(isi_main);
		}
		
		
		private function init():void
		{
			isi_main.y = isi_start;
			text1.visible = true;
			text1.alpha = 1;
			text2.visible = false;
			text2.alpha = 0;
			CTA.visible = false;
			CTA.alpha = 0;
			logo.visible = false;
			logo.alpha = 0;
			grade.mask = fr1txt;
			fr2txt.visible = false;
			fr3txt.visible = false;
			fr4txt.visible = false;
			up_arrow.mouseEnabled = down_arrow.mouseEnabled = false;
			up_arrow.mouseChildren = down_arrow.mouseChildren = false;
			isi_main.mask = isi_mask;
			gradeWidth = grade.width;
			contentRange = isi_main.height - isi_mask.height - isi_start;
			scrollOffset = isi_main.height - isi_mask.height;
			scrollDuration = contentRange * .19; // the scroll duration is the total time of the content compared to the constant of the scroll duration of the 640 banner.
			startSparkle();
			getStarted();
		}
		
		private function getStarted():void
		{
			TweenLite.to(grade, 4, {delay:1, x:0, ease:Expo.easeOut, onComplete:fr2});			
		}
		
		private function fr2():void
		{
			TweenLite.to(grade, 1, {x:-(gradeWidth), onComplete:fr3});
		}
		
		private function fr3():void
		{
			fr1txt.visible = false;
			fr2txt.visible = true;
			grade.mask = fr2txt;
			TweenLite.to(grade, 4, {x:0, ease:Expo.easeOut, onComplete:fr4});
		}
		private function fr4():void
		{
			TweenLite.to(grade, 1, {x:-(gradeWidth), onComplete:fr5});
		}
		private function fr5():void
		{
			fr2txt.visible = false;
			fr3txt.visible = true;
			grade.mask = fr3txt;
			TweenLite.to(grade, 4, {x:0, ease:Expo.easeOut, onComplete:fr6});
		}
		
		private function fr6():void
		{
			TweenLite.to(grade, 1, {x:-(gradeWidth), onComplete:fr7});
		}
		
		private function fr7():void
		{
			logo.visible = CTA.visible = text2.visible = true;
			
			fr3txt.visible = false;
			fr4txt.visible = true;
			grade.mask = fr4txt;
			TweenLite.to(grade, 4, {delay:.4, x:0, ease:Expo.easeOut});
			TweenLite.to(text1, .5, {alpha:0, ease:Sine.easeOut});
			TweenLite.to(text2, 1, {delay:.4, alpha:1, ease:Sine.easeOut});
			TweenLite.to(logo, 1, {delay:.8,alpha:1, ease:Sine.easeOut});
			TweenLite.to(CTA, 1, {delay:1, alpha:1, ease:Sine.easeOut, onComplete:endFr});
		}
		private function endFr():void
		{
			removeChild(sparkler);
			up_arrow.mouseEnabled = down_arrow.mouseEnabled = true;
			up_arrow.mouseChildren = down_arrow.mouseChildren = true;
			loopCount++;
			TweenLite.to(isi_main, scrollDuration, {y:-(contentRange), ease:Linear.easeNone, onComplete:moneyShot});
		}
		private function moneyShot():void
		{
			if (loopCount < 3)
			{
				init();
			}
		}
		private function startSparkle():void
		{
//			if(sparkToggle == 0){
				sparkler = new Sparkler();
				sparkler.gabarit = 300;
				sparkler.first_color_line = 0xffcc66;
				sparkler.second_color_line = 0xffffcc;
				sparkler.center_color_circle = 0xffffff;
				sparkler.limit_color_circle = 0xffffcc;
				sparkler.beam_counts = 5;
				sparkler.beam_thikness = 2;
				sparkler.beam_width = 20;
				sparkler.beamTail_width = 80;
				sparkler.count_sparks_perFrame = 6;
				sparkler.radius_particular_center = 6;
				sparkler.apacity_brink_circle = 0.5;
				sparkler.apacity_center_circle = 1;
				sparkler.angle = 360;
				sparkler.rotation_container = 0;
				sparkler.gfColor = 0xffcc66;
				sparkler.gfBlurX = 5;
				sparkler.gfBlurY = 5;
				sparkler.gfAlpha = 0.7;
				
				sparkler.init(false, false, false, false);
				
				addChild(sparkler);
				sparkler.scaleX = sparkler.scaleY = .3;
				sparkler.x = 240;
				sparkler.y = 90;
//			};
		}
		
		private function onClick(e:MouseEvent):void{
			navigateToURL(new URLRequest(root.loaderInfo.parameters.clickTag), "_blank"); // PRODUCTION LINK
			//navigateToURL(new URLRequest("http://www.abraxane.com/nsclc/hcp/"), "_blank"); // THIS WAS JUST SO THAT WE COULD SIMULATE THE LINK - NOT FOR PRODUCTION
			//navigateToURL(new URLRequest('docs/LearnMore_NSCLC.pdf'), "_blank");
		}
		private function piClick(e:MouseEvent):void{
			//navigateToURL(new URLRequest("docs/Abraxane_PrescribingInformation.pdf"), "_blank"); // ONLY FOR STAGING
			//navigateToURL(new URLRequest("http://abraxane.com/docs/Abraxane_PrescribingInformation.pdf"), "_blank"); // STAGING LINK
			navigateToURL(new URLRequest(root.loaderInfo.parameters.clickTag1), "_blank");
		}
		
	}
}