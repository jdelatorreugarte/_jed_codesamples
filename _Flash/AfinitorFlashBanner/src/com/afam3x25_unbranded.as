package com
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.events.Event;
	
	public class afam3x25_unbranded extends Sprite
	{
		public function afam3x25_unbranded()
		{
			var loopCount:Number = 0;
			
			main_btn.addEventListener(MouseEvent.CLICK, onClick);
			main_btn.buttonMode=true;
		
			function initialize():void
			{
				textFr1.alpha = 1;
				textFr2.alpha = frame3.alpha = frame5.alpha = 0;
				fr1();
			};
			
			function onClick(e:MouseEvent):void {
				var click_url:String = root.loaderInfo.parameters.clickTAG;
				if(click_url) {
					navigateToURL(new URLRequest(click_url), '_blank');
				}
				navigateToURL(new URLRequest("http://www.afinitor.com/angiomyolipoma-tsc/patient/index.jsp"), "_blank");
			}
			function fr1():void
			{
				TweenLite.to(textFr1, .6, {delay:4, alpha:0, ease:Sine.easeIn});
				TweenLite.to(textFr2, .6, {delay:4.5, alpha:1, ease:Sine.easeIn, onComplete:fr2});
			}
			function fr2():void
			{
				TweenLite.to(frame3, .6, {delay:4, alpha:1, ease:Sine.easeIn, onComplete:fr3});
			}
			function fr3():void
			{
				TweenLite.to(frame5, .5, {delay:4, alpha:1, ease:Sine.easeIn});
				TweenLite.delayedCall(10, checkLoop);
			}
			function checkLoop():void
			{
				if (loopCount < 2){initialize(); loopCount++;}
			}
			initialize();
		}
		
	}
}