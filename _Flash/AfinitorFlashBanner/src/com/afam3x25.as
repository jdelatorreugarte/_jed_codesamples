package com
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.events.Event;
	
	public class afam3x25 extends Sprite
	{
		public function afam3x25()
		{
			var yOffset:Number;
			var topLimit:Number = isi.track.y;
			var thumbRange:Number = isi.track.height - isi.thumb.height;
			var bottomLimit:Number = topLimit + thumbRange;
			
			var scrollPercent:Number = 0;
			var contentRange:Number = isi.myContent.height - isi.myMask.height;
			//var contentRange:Number = isi.myContent.height;
			var loopCount:Number = 0;
			var perChange:Number; // set a variable that will be the percentage value of the change between the starting ISI text and where it is currently 
			var initPositionerValue = (thumbRange * isi.myContent.y) / contentRange; // ratio comparing the ISI to the thumb scroller
			var thumbYInit = isi.thumb.y; // starting value of the slider button
			
			isi.myContent.pi_btn.addEventListener(MouseEvent.CLICK, piClick);
			isi.myContent.pi_btn.buttonMode=true;
			isi.myContent.mask = isi.myMask;
			isi.track.buttonMode = true;
			isi.thumb.addEventListener(MouseEvent.MOUSE_DOWN, thumb_onMouseDown);
			
			// LET THE AUTOSCROLLING BEGIN!!!!!!
			stage.addEventListener(Event.ENTER_FRAME, updateThumb); // frame event that moves the thumb in relation to the current position of the ISI text
			function updateThumb(e:Event):void
			{
				if(isi.thumb.y > bottomLimit)
				{
					isi.thumb.y = bottomLimit;
					return;
				}
				perChange = (thumbRange * isi.myContent.y) / contentRange; // function that relates the current position of the ISI,
				//it's total value and the total value of the thumbRange this then compared to the inital value of these things. The difference will be added to
				//the thumb to reflect the changes made.
				isi.thumb.y =+ (initPositionerValue - perChange) + thumbYInit; //compare the start and current position of the ISI converted to a ratio that is added
				//to the thumb.y value
				
			}
			//var autoScroll = contentRange * -1 + isi.myMask.height;
			
			var autoScroll = -(contentRange) + stage.stageHeight - (isi.myMask.height*2); // the total height of the ISI times -1 to make it a negative number to that it scrolls up
			// offset by the height of the stage minus the text mask. Note that stageHeight is the visible stage area
//			TweenLite.to(isi.myContent, 360, {y:autoScroll, onComplete:whereAmI});
			function whereAmI():void
			{
				stage.removeEventListener(Event.ENTER_FRAME, updateThumb); //removing the event listener is better for performance
				trace('isi y position = ' + isi.myContent.y)
			}
			// END AUTOSCROLLING!!!!
			
			function thumb_onMouseDown(event:MouseEvent):void
			{
				
				yOffset = mouseY - isi.thumb.y;
				stage.addEventListener(MouseEvent.MOUSE_MOVE, stage_onMouseMove);
				stage.addEventListener(MouseEvent.MOUSE_UP, stage_onMouseUp);
				stage.removeEventListener(Event.ENTER_FRAME, updateThumb); // removes eventlistener to keep conflicts from happening with animation and aid performance
				TweenLite.killTweensOf(isi.myContent); // once the scroll bar has been clicked there is no need for anymore auto scroll
			}
			
			function stage_onMouseMove(event:MouseEvent):void {
				isi.thumb.y = mouseY - yOffset;
				//restrict the movement of the thumb:
				if(isi.thumb.y < topLimit) {
					isi.thumb.y = topLimit;
				}
				if(isi.thumb.y > bottomLimit) {
					isi.thumb.y = bottomLimit;
				}
				scrollPercent = (isi.thumb.y - isi.track.y) / thumbRange;
				isi.myContent.y = isi.myMask.y - (scrollPercent * contentRange);


				event.updateAfterEvent();
			}
			
			function stage_onMouseUp(event:MouseEvent):void {
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, stage_onMouseMove);
				stage.removeEventListener(MouseEvent.MOUSE_UP, stage_onMouseUp);
			}
			
			function initialize():void
			{
				//main_btn.height = 250;
				if(main_btn.hasEventListener(MouseEvent.MOUSE_DOWN)) {
					removeEventListener(MouseEvent.CLICK, onClick)
				};
				main_btn.buttonMode=false;
				textFr1.alpha = 1;
				textFr2.alpha = frame3.alpha = frame4.alpha = frame5.alpha = isi.alpha = logo.alpha = model.alpha = 0;
				fr1();
			};
			
			
			function fr1():void
			{
				TweenLite.to(textFr1, .6, {delay:4, alpha:0, ease:Sine.easeIn});
				TweenLite.to(textFr2, .6, {delay:4.5, alpha:1, ease:Sine.easeIn, onComplete:fr2});
			}
//			
			function fr2():void
			{
				TweenLite.to(frame3, .6, {delay:4, alpha:1, ease:Sine.easeIn, onComplete:fr3});
			}
			function fr3():void
			{
				TweenLite.to(frame4, .6, {delay:5, alpha:1, ease:Sine.easeIn});
				TweenLite.to(isi, .6, {delay:5, alpha:1, ease:Sine.easeIn});
				TweenLite.to(logo, .6, {delay:5, alpha:1, ease:Sine.easeIn});
				TweenLite.to(model, .6, {delay:6, alpha:1, ease:Sine.easeIn, onComplete:fr4});
				
			}	
			function fr4():void
			{
				trace('autoScroll = ' + autoScroll + 'contentRange = ' + contentRange * -1 + 'stage = ' + stage.stageHeight + 'isi mask = ' + isi.myMask.height);
				
				TweenLite.to(frame4, 1, {delay:3, alpha:0, ease:Sine.easeOut, onComplete:initBtn});
				TweenLite.to(frame5, .5, {delay:3.5, alpha:1, ease:Sine.easeIn});
				TweenLite.to(isi.myContent, 300, {delay:4.5, y:autoScroll, onComplete:whereAmI, ease:Linear.easeNone});
			}
			function initBtn():void
			{
				main_btn.addEventListener(MouseEvent.CLICK, onClick);
				main_btn.buttonMode=true;
			}
			function onClick(e:MouseEvent):void {
				var click_url:String = root.loaderInfo.parameters.clickTAG;
				if(click_url) {
					navigateToURL(new URLRequest(click_url), '_blank');
				}
				//navigateToURL(new URLRequest("http://www.afinitor.com/angiomyolipoma-tsc/patient/index.jsp"), "_blank");
			}
			function piClick(e:MouseEvent):void {
				navigateToURL(new URLRequest("http://www.pharma.us.novartis.com/product/pi/pdf/afinitor.pdf"), "_blank");
			}
			initialize();
		}
		
	}
}