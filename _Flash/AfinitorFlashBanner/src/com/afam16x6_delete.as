package com
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.events.Event;
	
	public class ABC16x6 extends Sprite
	{
		public function ABC16x6()
		{
			var yOffset:Number;
			var topLimit:Number = track.y;
			var thumbRange:Number = track.height - thumb.height;
			var bottomLimit:Number = track.y + thumbRange;
			var scrollPercent:Number = 0;
			var contentRange:Number = myContent.height - myMask.height;
			var loopCount:Number = 0;
			var perChange:Number; // set a variable that will be the percentage value of the change between the starting ISI text and where it is currently 
			var initPositionerValue = (thumbRange * myContent.y) / contentRange; // ratio comparing the ISI to the thumb scroller
			var thumbYInit = thumb.y; // starting value of the slider button
			pi_btn.buttonMode=true;
			pi_btn.mouseChildren = false;
			pi_btn.addEventListener(MouseEvent.CLICK, piClick);
			myContent.isi_pi_btn.addEventListener(MouseEvent.CLICK, piClick);
			main_btn.addEventListener(MouseEvent.CLICK, onClick);
			main_btn.buttonMode=true;
			myContent.mask = myMask;
			thumb.buttonMode = true;
			thumb.addEventListener(MouseEvent.MOUSE_DOWN, thumb_onMouseDown);
			
			
			// LET THE AUTOSCROLLING BEGIN!!!!!!
			stage.addEventListener(Event.ENTER_FRAME, updateThumb); // frame event that moves the thumb in relation to the current position of the ISI text
			function updateThumb(e:Event):void {
				if(thumb.y > bottomLimit) {
					thumb.y = bottomLimit;
				}
				perChange = (thumbRange * myContent.y) / contentRange; // function that relates the current position of the ISI,
				//it's total value and the total value of the thumbRange this then compared to the inital value of these things. The difference will be added to
				//the thumb to reflect the changes made.
				thumb.y =+ (initPositionerValue - perChange) + thumbYInit; //compare the start and current position of the ISI converted to a ratio that is added
				//to the thumb.y value
				
			}
			var autoScroll = contentRange * -1 + stage.stageHeight - myMask.height; // the total height of the ISI times -1 to make it a negative number to that it scrolls up
			// offset by the height of the stage minus the text mask. Note that stageHeight is the visible stage area
			TweenLite.to(myContent, 360, {y:autoScroll, onComplete:whereAmI});
			function whereAmI():void
			{
				stage.removeEventListener(Event.ENTER_FRAME, updateThumb); //removing the event listener is better for performance
			}
			// END AUTOSCROLLING!!!!
			
			function stage_onMouseMove(event:MouseEvent):void {
				thumb.y = mouseY - yOffset;
				//restrict the movement of the thumb:
				if(thumb.y < topLimit) {
					thumb.y = topLimit;
				}
				if(thumb.y > bottomLimit) {
					thumb.y = bottomLimit;
				}
				scrollPercent = (thumb.y - track.y) / thumbRange;
				myContent.y = myMask.y - (scrollPercent * contentRange);

				event.updateAfterEvent();
			}
			
			function thumb_onMouseDown(event:MouseEvent):void {
				yOffset = mouseY - thumb.y;
				stage.addEventListener(MouseEvent.MOUSE_MOVE, stage_onMouseMove);
				stage.addEventListener(MouseEvent.MOUSE_UP, stage_onMouseUp);
				stage.removeEventListener(Event.ENTER_FRAME, updateThumb); // removes eventlistener to keep conflicts from happening with animation and aid performance
				TweenLite.killTweensOf(myContent); // once the scroll bar has been clicked there is no need for anymore auto scroll
			}
			
			function stage_onMouseUp(event:MouseEvent):void {
				stage.removeEventListener(MouseEvent.MOUSE_MOVE, stage_onMouseMove);
				stage.removeEventListener(MouseEvent.MOUSE_UP, stage_onMouseUp);
			}
			
			function initialize():void
			{
				main_btn.height = 600;
				main_mc.alpha = main_mc.banner.alpha = fr1Txt.alpha = 1;
				main_mc.more.y = 0;
				main_mc.more.scaleX = main_mc.more.scaleY = 1;
				main_mc.options.alpha = endFr.alpha = endFr.CTA.alpha = main_mc.information.alpha = main_mc.support.alpha = 0;
				TweenLite.to(main_mc.options, .6, {delay:.4, alpha:1, ease:Sine.easeIn, onComplete:fr2});
			};
			
			function onClick(e:MouseEvent):void {
				var click_url:String = root.loaderInfo.parameters.clickTAG;
				if(click_url) {
					navigateToURL(new URLRequest(click_url), '_blank');
				}
				//navigateToURL(new URLRequest("http://www.afinitor.com/advanced-breast-cancer/patient/index.jsp"), "_blank");
			}
			
			function piClick(e:MouseEvent):void{
				navigateToURL(new URLRequest("http://www.pharma.us.novartis.com/product/pi/pdf/afinitor.pdf"), "_blank");
			}
			
			function fr2():void
			{
				TweenLite.to(main_mc.options, .6, {delay:1, alpha:0, ease:Sine.easeIn});
				TweenLite.to(main_mc.support, .6, {delay:2, alpha:1, ease:Sine.easeIn, onComplete:fr3});
			}
			function fr3():void
			{
				TweenLite.to(main_mc.support, .6, {delay:.9, alpha:0, ease:Sine.easeIn});
				TweenLite.to(main_mc.information, .6, {delay:1.5, alpha:1, ease:Sine.easeIn, onComplete:fr4});
			}	
			function fr4():void
			{
				main_btn.height = 290;
				TweenLite.to(main_mc.information, .5, {delay:1, alpha:0, ease:Sine.easeIn});
				TweenLite.to(main_mc.banner, .5, {delay:1, alpha:0, ease:Sine.easeIn});
				TweenLite.to(fr1Txt, .5, {delay:1.2, alpha:0, ease:Sine.easeIn, onComplete:fr5});
			}
			function fr5():void
			{
				TweenLite.to(endFr, .5, {alpha:1, ease:Sine.easeIn});
				TweenLite.to(endFr.CTA, .5, {delay:1.5, alpha:1, ease:Sine.easeIn});
				TweenLite.delayedCall(7, checkLoop);
			}
			function checkLoop():void
			{
				if (loopCount < 2){initialize(); loopCount++;}
			}
			initialize();
		}
		
	}
}