﻿package com.eliquis
{
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.net.navigateToURL;
	import flash.net.URLRequest;
	import flash.events.MouseEvent;
	
	import com.greensock.TweenNano;
	import com.greensock.easing.Quad;
	import com.greensock.easing.Linear;
	
	public class EliquisHoriz extends Sprite
	{
		private var _count:int = 0;
		private var _percentage:Number;
		private var _mouseY:Number;
		private var _handleStart:Number;
		private var _handleStop:Number;
		private var _isiStart:Number;
		private var _scrollIncrement:Number;
		private var _scrollStop:Number;
		private var _arrow:String;
		
		
		public function EliquisHoriz()
		{
			_handleStart = handle.y;
			_handleStop = (bar.height - handle.height) + _handleStart;
			_scrollStop = isi.height - isiMask.height + 10;	// this last number is an arbitrary number to make the end of the isi look good
			_isiStart = isi.y;
			_scrollIncrement = 1;	// this is set so you can see some of the previous lines of isi as you click on the arrows
			
			handle.buttonMode = true;
			handle.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
			handle.stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
			handle.stage.addEventListener(Event.MOUSE_LEAVE, mouseUpHandler);
			
			btn.buttonMode = true;
			btn.addEventListener(MouseEvent.CLICK, clickHandler);
			btnISI.buttonMode = true;
			btnISI.addEventListener(MouseEvent.CLICK, isiClickHandler);
			btnFPI.buttonMode = true;
			btnFPI.addEventListener(MouseEvent.CLICK, fpiClickHandler);
			
			arrowUp.buttonMode = true;
			arrowUp.addEventListener(MouseEvent.MOUSE_DOWN, arrowClickHandler);
			arrowUp.addEventListener(MouseEvent.MOUSE_UP, arrowUnclickHandler);
			arrowUp.addEventListener(MouseEvent.MOUSE_OUT, arrowUnclickHandler);
			arrowDown.buttonMode = true;
			arrowDown.addEventListener(MouseEvent.MOUSE_DOWN, arrowClickHandler);
			arrowDown.addEventListener(MouseEvent.MOUSE_UP, arrowUnclickHandler);
			arrowDown.addEventListener(MouseEvent.MOUSE_OUT, arrowUnclickHandler);
			
			init();
		}
		
		private function fpiClickHandler($event:MouseEvent):void 
		{
			navigateToURL(new URLRequest("http://packageinserts.bms.com/pi/pi_eliquis.pdf"), "_blank");
		}
		
		private function isiClickHandler($event:MouseEvent):void 
		{
			navigateToURL(new URLRequest("https://www.hcp.eliquis.com/pages/important-safety-information.aspx"), "_blank");
		}
		
		private function clickHandler($event:MouseEvent):void
		{
			var click_url:String = root.loaderInfo.parameters.clickTAG;
			if (click_url)
			{
				navigateToURL(new URLRequest(click_url), '_blank');
			}
			navigateToURL(new URLRequest("#"), "_blank");
		}
		
		private function init():void
		{
			_count++;
			
			text2.alpha = 0;
			text2.y = -69;
			text3.alpha = 0;
			text3.y = -69;
			
			logo.alpha = 0;
			logo.x = 116;
			logo.y = 13;
			logo.scaleX = 1.14;
			logo.scaleY = 1.14;
			
			red.x = -341;
			
			slideRed();
		}
		
		public function slideRed():void
		{
			TweenNano.to(red, .7, {x: 0, delay: .2, ease: Quad.easeInOut, onComplete: fadeIns});
		}
		
		public function fadeIns():void
		{
			TweenNano.to(glow, .5, {alpha: 1, delay: .4, ease: Quad.easeInOut, onComplete: shrinkLogo});
			TweenNano.to(logo, .2, {alpha: 1, ease: Quad.easeInOut});
		}
		
		private function shrinkLogo():void
		{
			TweenNano.to(logo, .4, {x:17, y:18, scaleX: 1, scaleY: 1, delay: .3, ease: Quad.easeInOut, onComplete: showFrame2});
			TweenNano.to(glow, .5, {alpha: 0, ease: Quad.easeInOut});
		}
		
		/*public function moveLogo():void
		{
			TweenNano.to(logo, .5, {x:17,delay: .3, ease: Quad.easeInOut, onComplete: showFrame2});
		}*/
		
		public function showFrame2():void
		{
			TweenNano.to(text2, .5, {y:13, alpha: 1, ease: Quad.easeInOut, onComplete: showFrame3});
		}
		
		private function showFrame3():void
		{
			TweenNano.to(text2, .5, {y:100, alpha: 0, delay: 2, ease: Quad.easeInOut});
			TweenNano.to(text3, .5, {y:17, alpha: 1, delay: 2, ease: Quad.easeInOut, onComplete: fadeFrame3 } );
		}
		
		private function fadeFrame3():void 
		{
			if (_count < 3) 
				TweenNano.delayedCall(5, init);
			else
				scrollISIAuto();
		}
		
		private function scrollISIAuto():void 
		{
			TweenNano.to(isi, 200, {y:-(_scrollStop - _isiStart), ease:Linear.easeNone});
			TweenNano.to(handle, 200, {y:_handleStop, ease:Linear.easeNone});
		}
		
		
		private function mouseDownHandler($event:MouseEvent):void
		{
			_mouseY = $event.target.mouseY;
			addEventListener(Event.ENTER_FRAME, moveHandleHandler);
		}
		
		private function mouseUpHandler(event:Event):void
		{
			removeEventListener(Event.ENTER_FRAME, moveHandleHandler);
		}
		
		private function moveHandleHandler(event:Event):void
		{
			updateAssets(mouseY - _mouseY);
		}
		
		private function arrowClickHandler(event:MouseEvent):void 
		{
			_arrow = event.target.name;
			addEventListener(Event.ENTER_FRAME, scrollISI);
		}
		
		private function arrowUnclickHandler(event:MouseEvent):void 
		{
			removeEventListener(Event.ENTER_FRAME, scrollISI);
		}
		
		private function scrollISI(event:Event):void
		{
			var newY:int = handle.y;
			
			switch(_arrow)
			{
				case "arrowUp":
					newY += _scrollIncrement;
					break;
					
				case "arrowDown":
					newY -= _scrollIncrement;
					break;
			}
			
			updateAssets(newY);
		}
		
		private function updateAssets($handleY:Number):void 
		{
			handle.y = $handleY;
			
			if (handle.y <= _handleStart)
			{
				handle.y = _handleStart;
			}
			
			if (handle.y >= bar.height - handle.height + _handleStart)
			{
				handle.y = bar.height - handle.height + _handleStart;
			}
			
			_percentage = (handle.y - _handleStart) / (bar.height - handle.height);
			
			isi.y = ( -(_percentage * _scrollStop)) + _isiStart;
		}
		
		
		
		
		
		
	}
}

