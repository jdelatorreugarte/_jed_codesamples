package com
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.events.Event;
	
	public class zoetis11996_16x6 extends Sprite
	{
		var loopCount:Number;
		
		public function zoetis11996_16x6()
		{
			loopCount = 0;
			pi_btn.buttonMode=true;
			pi_btn.mouseChildren = false;
			pi_btn.addEventListener(MouseEvent.CLICK, piClick);
			main_btn.addEventListener(MouseEvent.CLICK, onClick);
			main_btn.buttonMode=true;

			this.addEventListener(Event.ENTER_FRAME,LoadInterval);

			init();
			
		}

		function LoadInterval(e:Event):void {
			var nLoadedBytes:Number = loaderInfo.bytesLoaded;
			var nTotalBytes:Number = loaderInfo.bytesTotal;
			
			//percent.text = Math.round(nLoadedBytes / nTotalBytes * 100)+ "% complete";
			//trace (nLoadedBytes / nTotalBytes * 100);
			if (nLoadedBytes >= nTotalBytes) {
				removeChild(loadingMask);
				this.removeEventListener(Event.ENTER_FRAME,LoadInterval);
			}
		}

		private function init():void
		{
			cta_mc.y = 70;// end 113
			fr1_text.visible = true;
			fr1_text.alpha = 1;
			fr2_text.visible = false;
			fr2_text.alpha = 0;
			fr3_text.visible = false;
			fr3_text.alpha = 0;
			arrow_mc.alpha = 0;

			getStarted();
		}
		
		
		private function getStarted():void
		{
			TweenNano.to(arrow_mc, 8, {rotation:360, alpha:1, ease:Circ.easeIn, onComplete:fr4});
			TweenNano.delayedCall(2, fr2);			
		}
		
		private function fr2():void
		{
			fr2_text.visible = true;
			TweenNano.to(fr1_text, .6, {alpha:0, ease:Sine.easeInOut});
			TweenNano.to(fr2_text, 1, {alpha:1, delay:.4, ease:Sine.easeInOut, onComplete:fr3});
		}
		
		private function fr3():void
		{
			fr1_text.visible = false;
			fr3_text.visible = true;
			TweenNano.to(fr2_text, .6, {alpha:0, delay:2, ease:Sine.easeInOut});
			TweenNano.to(fr3_text, 1, {alpha:1, delay:2.4, ease:Sine.easeInOut});
		}	
		private function fr4():void
		{
			fr2_text.visible = false;
			TweenNano.to(cta_mc, 1, {y:113, ease:Expo.easeInOut, onComplete:moneyShot});
		}
		private function moneyShot():void
		{
			loopCount++;
			if (loopCount < 3)
			{
				TweenNano.delayedCall(3, init);
			}
		}
		
		
		private function onClick(e:MouseEvent):void{
			//navigateToURL(new URLRequest(root.loaderInfo.parameters.clickTag), "_blank"); // PRODUCTION LINK
			navigateToURL(new URLRequest("http://www.draxxin.com"), "_blank"); // THIS WAS JUST SO THAT WE COULD SIMULATE THE LINK - NOT FOR PRODUCTION
			//navigateToURL(new URLRequest('docs/LearnMore_NSCLC.pdf'), "_blank");
		}
		private function piClick(e:MouseEvent):void{
			//navigateToURL(new URLRequest("docs/Abraxane_PrescribingInformation.pdf"), "_blank"); // ONLY FOR STAGING
			navigateToURL(new URLRequest("https://online.zoetis.com/US/EN/Products/Assets/DraxxinPork/PDF/Draxxin_25Full_PI_R2.pdf"), "_blank"); // STAGING LINK
			//navigateToURL(new URLRequest(root.loaderInfo.parameters.clickTag1), "_blank");
		}
		
	}
}