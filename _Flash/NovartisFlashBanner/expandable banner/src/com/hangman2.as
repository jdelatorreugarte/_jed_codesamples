package com
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	
	public class hangman2 extends Sprite
	{	
		var xPos:Number;
		var yPos:Number;
		
		public function hangman2()
		{
			addListeners(j_mc, a_mc, n_mc, u_mc, a2_mc, r_mc, y_mc, f_mc, e_mc, b_mc, r2_mc, u2_mc, a3_mc, r3_mc, y2_mc);// add events
		}
		private function getPosition(target:Object):void
		{
			xPos = target.x;//store starting pos
			yPos = target.y;//store starting pos
		}
		private function dragObject(e:MouseEvent):void
		{
			bringToFront(e.target);
			getPosition(e.target);
			e.target.startDrag(true);
		}
		
		private function stopDragObject(e:MouseEvent):void
		{
			var letterObject = e.target;
			var noNumLetter:String = letterObject.name;
			var regEx:RegExp = /\d+/g;
			noNumLetter = noNumLetter.replace(regEx, '');
	
			if(letterObject.dropTarget)
			{
				var targetNoNum = letterObject.dropTarget.parent.name;
				targetNoNum = targetNoNum.replace(regEx, '');
				targetNoNum = targetNoNum.split('Target').join('');
				trace(targetNoNum);
				if (targetNoNum == noNumLetter)//Checks the correct drop target
				{
					letterObject.x = getChildByName(letterObject.dropTarget.parent.name).x;//If its correct, place the clip in the same position as the target
					letterObject.y = getChildByName(letterObject.dropTarget.parent.name).y;//If its correct, place the clip in the same position as the target
					letterObject.removeEventListener(MouseEvent.MOUSE_DOWN, dragObject);//once in the correct place remove the object's ability to be dragged
				} else
				{
					letterObject.x = xPos;
					letterObject.y = yPos;
				}
			}	
			
			letterObject.stopDrag();
		}
		
		private function addListeners(... objects):void
		{
			for(var i:int = 0; i < objects.length; i++)
			{
				objects[i].addEventListener(MouseEvent.MOUSE_DOWN, dragObject);
				objects[i].addEventListener(MouseEvent.MOUSE_UP, stopDragObject);
			}
		}
		private function bringToFront(mcl)
		{
			mcl.parent.setChildIndex(mcl,mcl.parent.numChildren - 1);
		}

	}
}