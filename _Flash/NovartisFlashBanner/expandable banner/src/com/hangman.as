package com
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.display.MovieClip;
	
	public class hangman extends Sprite
	{	
		var currentClip:MovieClip;
		var startX:Number;
		var startY:Number;
		var dragArray:Array = [circle, square, triangle];
		var matchArray:Array = [circlematch, squarematch, trianglematch];


		public function hangman()
		{
			init();
		}
		private function init()
		{
			
			for(var i:int = 0; i < dragArray.length; i++)
			{
				dragArray[i].buttonMode = true;
				dragArray[i].addEventListener(MouseEvent.MOUSE_DOWN, item_onMouseDown);
				matchArray[i].alpha = .2;
				
			}
		}

		private function item_onMouseDown(event:MouseEvent):void
		{
			currentClip = MovieClip(event.target);
			startY = currentClip.y;
			startX = currentClip.x;
			addChild(currentClip);
			currentClip.startDrag();
			stage.addEventListener(MouseEvent.MOUSE_UP, item_onMouseUp);
		}
		private function item_onMouseUp(event:MouseEvent):void
		{
			currentClip.stopDrag();
			stage.removeEventListener(MouseEvent.MOUSE_UP, item_onMouseUp);
			var index:int = dragArray.indexOf(currentClip);
			var matchClip:MovieClip = MovieClip(matchArray[index]);
			if (currentClip.hitTestObject(matchClip))
			{
				currentClip.x = matchClip.x;
				currentClip.y = matchClip.y;
				currentClip.removeEventListener(MouseEvent.MOUSE_DOWN, item_onMouseDown);
				currentClip.buttonMode = false;
			} else
			{
				currentClip.y = startY;
				currentClip.x = startX;
			}
			
		}

	}
}