package com
{
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import com.greensock.*;
	import com.greensock.easing.*;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.events.Event;
	
	public class base extends Sprite
	{	
		public function base()
		{
			init();
			start();
			mainCircle.addEventListener(MouseEvent.MOUSE_DOWN, circClickHandler);
			mainCircle.addEventListener(MouseEvent.MOUSE_UP, circClickHandlerOff);
		}
		private function circClickHandler(event:MouseEvent):void 
		{
			event.target.parent.startDrag();
			addEventListener(Event.ENTER_FRAME, posFeedback);
		}
		private function circClickHandlerOff(event:MouseEvent):void 
		{
			event.target.parent.stopDrag();
			removeEventListener(Event.ENTER_FRAME, posFeedback);
		}
		private function posFeedback(event:Event):void
		{
			trace(mainCircle.x + " " + mainCircle.y);

		}
		private function init():void
		{
			mainCircle.greenArrow.alpha = 1;
			mainCircle.alpha = 0;
			mainCircle.fluxLogo.alpha = 1;
			mainCircle.innerCircle.scaleY = mainCircle.innerCircle.scaleX = 1;

		}
		private function start():void
		{
			TweenMax.to(mainCircle, .6, {alpha:1, delay:.2, ease:Sine.easeInOut, onComplete:fr1})
		}
		private function fr1():void
		{
			TweenMax.to(mainCircle, 3, {bezier:{autoRotate:false, values:[{x:400, y:230}, {x:600, y:90}]}, delay:1, ease:Expo.easeInOut, onComplete:fr2});
		}
		private function fr2():void
		{
			TweenMax.to(mainCircle.greenArrow, .5, {alpha:0});
			TweenMax.to(mainCircle.innerCircle, .5, {scaleY: 1.8, scaleX:1.8, delay:.2, ease:Expo.easeOut});
			TweenMax.to(mainCircle, .5, {x:507, y:-52, delay:.2, ease:Expo.easeOut});
		}
	}
}