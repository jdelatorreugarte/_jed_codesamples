/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/14/13
 * Time: 12:08 AM
 */

define(['ng/modules/mainApp'] , function (mainApp) {
    mainApp.directive('ratingPanel' , function () {
	    // The above name 'myDirective' will be parsed out as 'my-directive'
	    // for in-markup uses.
	    return {
		    // restrict to an element (A = attribute, C = class, M = comment, E = element)
		    // or any combination like 'EACM' or 'EC'
		    restrict: 'E',
		    transclude: false,
		    scope: {
			    name: '=name' // set the name on the directive's scope
			    // to the name attribute on the directive element.
			    , ratingValue: '=data-rating-value'
			    , ratingComment: '=data-rating-comment'
			    , selectedRatingValue: '@'
		    },
		    //the template for the directive.
		    templateUrl: '/javascripts/ng/directives/ratingPanel.template.html',
		    //the controller for the directive
		    controller: function($scope) {
		    },
		    replace: true, //replace the directive element with the output of the template.
		    compile: function compile(tElement, tAttrs, transclude) {
			    return {
				    pre: function preLink(scope, iElement, iAttrs, controller) {
				    },
				    post: function postLink(scope, iElement, iAttrs, controller) {
				    }
			    }
			    // or
			    // return function postLink( ... ) { ... }
		    }
		    /*
		    //the link method does the work of setting the directive
		    // up, things like bindings, jquery calls, etc are done in here
		    link: function(scope, elem, attr) {
			    // scope is the directive's scope,
			    // elem is a jquery lite (or jquery full) object for the directive root element.
			    // attr is a dictionary of attributes on the directive element.
			    elem.bind('dblclick', function() {
				    scope.name += '!';
				    scope.$apply();
			    });
		    }*/
	    };
    });
});