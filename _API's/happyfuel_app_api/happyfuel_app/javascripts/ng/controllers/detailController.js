/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/25/13
 * Time: 8:41 AM
 */

define(['ng/modules/mainApp'] , function (mainApp) {
	mainApp.controller('detailController' , ['$scope', '$rootScope' , function ($scope, $rootScope) {

		$scope.postRatings = [];
		$scope.svc.post.getPostRatings(1);
		$scope.ratingIsOn = false;
		$scope.ratingValue = 0;
		$scope.ratingComment = '';
		$scope.selectedPostIsFollowingUser = false;
		$scope.fullPostDetail = {};
		$scope.postHashTags = [];
		$scope.deepLinkUrl = '';

		$rootScope.$watch('selectedPost', function (newVal, oldVal) {
			if ($rootScope.selectedPost !== null) {
				console.log('selected post:', $rootScope.selectedPost);
				$scope.selectedPost = $rootScope.selectedPost;
				$scope.deepLinkUrl = encodeURIComponent(location.protocol + '//' + location.hostname + '/#/fuel-post/' + $scope.selectedPost.FuelPostId);
				$scope.selectedRatingValue = $scope.selectedPost.FuelRatingCount;
				$scope.postHashTags = [];
				$scope.svc.post.getPost($scope.selectedPost.FuelPostId)
					.success(function(data, status) {
						$scope.fullPostDetail = data.GetPostByIDResult.Post[0];
					})
					.error(function(data, status) {
						console.log(data, status);
					});
				$scope.svc.post.getPostHashTags($scope.selectedPost.FuelPostId)
					.success(function(data){
						$scope.postHashTags = (data === '""') ? [] : data.split(',');
					});
				$scope.svc.post.getPostRatings($scope.selectedPost.FuelPostId)
					.success(function(data, status) {
						$rootScope.postRatings = data.Ratings || [];
					})
					.error(function(data, status) {
						console.log(data, status);
					});
				if ($scope.currentUser !== null) {
					$scope.svc.user.isFollower($scope.currentUser.UserId, $scope.selectedPost.UserId)
						.success(function(data, status) {
							$scope.selectedPostIsFollowingUser = (data === "1");
						})
						.error(function(data, status) {
							console.log(data, status);
						});
				}
				$scope.setCurrentRating($rootScope.selectedPost.FuelRatingCount);
			}
		});

		$rootScope.$watch('postRatings', function (newVal, oldVal) {
			if ($rootScope.postRatings !== null) {
				console.log('post ratings:', $rootScope.postRatings);
				$scope.postRatings = $rootScope.postRatings;
				if ($scope.currentUser !== null) {
					//$scope.currentUserHasRatedSelectedPost = _.some($scope.postRatings, {UserId:$scope.currentUser.UserId});
				}
			}
		});

		$scope.$watch('matchingUsers', function (newVal, oldVal) {
			console.log(newVal);
		});

		$scope.ratePost = function() {
			$scope.selectedRatingValue++;
			$rootScope.selectedPost.FuelRatingCount = $scope.selectedRatingValue;
			$scope.ratingValue = $scope.svc.post.getActiveRateValue();
			$scope.svc.post.savePostRating(0, $scope.currentUser.UserId, $rootScope.selectedPost.FuelPostId, $scope.ratingValue, $scope.ratingComment)
				.success(function(data, status) {
					$rootScope.postRatings = data.Ratings;
				})
				.error(function(data, status) {
					console.log(data, status);
				});
			console.log('rating:', $scope.selectedRatingValue);
		};
		$scope.getPostType = function () {
			return ($scope.selectedPost == null)?'':$scope.selectedPost.FuelType;
		};
		$scope.getMediaUrl = function () {
			return ($scope.selectedPost == null)?'':'https://s3.amazonaws.com/HappyFuelApp/' + $scope.selectedPost.FuelData;
		};
		$scope.followUser = function (friendId) {
			$scope.svc.user.followUser(friendId);
		};

		$scope.followUser = function () {
			$scope.svc.user.saveFriend(0,$scope.currentUser.UserId, $scope.selectedPost.UserId,1);
		};

		$scope.selectUser = function (user) {
			var words = $scope.getWords($scope.ratingComment);
			words[words.length-1] = '@' + user.UserName;
			$scope.ratingComment = words.join(' ') + ' ';
			$scope.matchingUsers = [];
		};

		$scope.checkInputForUsername = function () {
			var lastWord = $scope.getLastWord($scope.ratingComment);
			if (/^@.*/.test(lastWord) && lastWord.length > 2 && !(/\s$/.test($scope.ratingComment))) {
				$scope.svc.search.searchUsers(lastWord.substr(1))
					.success(function (data, status) {
						$scope.matchingUsers = data.Users;
					})
					.error(function (data, status) {

					});
			}
			else {
				$scope.matchingUsers = [];
			}
		};

	}]);
});