/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/25/13
 * Time: 8:41 AM
 */

define([
	'ng/modules/mainApp'
	, 'amd/app'
	, 'ng/controllers/dashboardController'
	, 'ng/controllers/detailController'
	, 'ng/controllers/wallController'
	, 'ng/controllers/profileController'
	, 'ng/services/mainService'
	, 'ng/providers/cookieProvider'
	]
	, function (mainApp) {
	mainApp.controller('mainController' , ['$scope', 'hfService', '$rootScope', '$route', '$location', '$cookieStore' , function ($scope, hfsvc, $rootScope, $route, $location, $cookieStore) {
		var _svc = new hfsvc();
		$scope.svc = _svc;
		$scope.wallPostsResponse = null;
		$rootScope.selectedPostType = null;
		$rootScope.selectedFuelType = '';
		$rootScope.selectedUser = '';
		$rootScope.selectedHashTag = '';
		$rootScope.selectedPost = null;
		$scope.selectedRatingValue = 0;
		$rootScope.currentUser = null; //{UserId:1};
		$scope.currentUserHasRatedSelectedPost = false;
		$rootScope.currentPath = '';
		$rootScope.isLoggedIn = 'false';
		$scope.searchText = '';
		$rootScope.commentsPaneState = 'expanded';

		$scope.initDashboardData = function () {
			return {
				topFans: null,
				favoriteFuelers: null,
				userFuelers: null,
				userFollowedFuelers: null,
				myPostsByType: null,
				postsIRated: null,
				myTopPosts: null,
				myFavoritePosts: null
			};
		};

		$rootScope.selectPost = function (value) {
			console.log(value);
			$rootScope.selectedPost = value;
		};

		$rootScope.selectPostType = function (value) {
			console.log(value);
			$rootScope.selectedPostType = value;
		};

		$rootScope.selectFuelType = function (value) {
			console.log(value);
			$rootScope.selectedFuelType = value;
		};

		$scope.getCurrentUsername = function () {
			return ($rootScope.currentUser != null)?$rootScope.currentUser.UserName + ', ':'';
		};

		$scope.getBkgdImg = function (postItem, fuelType, resourceUrl) {
			switch ($rootScope.selectedFuelType) {
                case 'S':
                    return '/css/images/bkgd-sound.png';
                    break;
                case 'M':
                    return 'https://s3.amazonaws.com/HappyFuelApp/' + postItem.FuelThumbData;
                    break;
				case 'T':
					return '';
					break;
				case 'L':
					return 'http://maps.googleapis.com/maps/api/staticmap?center=' + postItem.FuelLatitude + ',' + postItem.FuelLongitude + '&zoom=14&size=400x400&sensor=false';
					break;
			}
			return 'https://s3.amazonaws.com/HappyFuelApp/' + resourceUrl;
		};

		$scope.getImgPath = function (resourceFilename) {
			return (resourceFilename != null && resourceFilename.length>0)?'https://s3.amazonaws.com/HappyFuelApp/' + resourceFilename : '';
		};

		$scope.getAvatarBkgdImg = function (resourceFilename) {
			var path = $scope.getImgPath(resourceFilename);
			if (path != '')
				return 'background-image:url(' + path + ');'
			return '';
		};

		$scope.myPostsByType = null;
		$scope.postsIRated = null;
		$scope.getDashboardInfo = function () {
			if ($rootScope.currentUser != null) {
				var usr = $rootScope.currentUser;
				_svc.dashboard.getTopFansByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.topFans = data.Users.slice(0,8);
					});
				_svc.dashboard.getFavoriteFuelersByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.favoriteFuelers = data.Users.slice(0,8);
					});
				_svc.dashboard.getUserFuelersByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.userFuelers = data.Users.slice(0,8);
					});
				_svc.dashboard.getUserFollowedFuelersByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.userFollowedFuelers = data.Users.slice(0,8);
					});
				_svc.dashboard.getPostsByTypeByUserId(usr.UserId)
					.success(function (data) {
						$scope.myPostsByType = data.FuelTypeCounts;
						$scope.setPostRatingCount('myposts');
					});
				_svc.dashboard.getTopPostsByTypeByRatingByUserId(usr.UserId)
					.success(function (data) {
						$scope.postsIRated = data.FuelTypeCounts;
						$scope.setPostRatingCount('postsirated');
					});
				_svc.dashboard.getTopPostsByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.myTopPosts = data.Post.slice(0,8);
					});
				_svc.dashboard.getFavoritePostsByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.myFavoritePosts = data.Post.slice(0,8);
					});
			}
		};


		$scope.setPostRatingCount = function (dataSet) {
			var fnFindElement = function (item) {
				return item.FuelType == this;
			}
				, fnSetPctValues = function(obj) {
					var highestValue = 0;
					for (var key in obj) {
						if (obj[key] && !isNaN(obj[key].FuelTypeRowCount) && obj[key].FuelTypeRowCount > highestValue) {
							highestValue = obj[key].FuelTypeRowCount;
						}
					}
					for (var key in obj) {
						if (obj[key] && !isNaN(obj[key].FuelTypeRowCount)) {
							obj[key]['PctValue'] = Math.floor((obj[key].FuelTypeRowCount / highestValue)*100);
						}
					}
				};
			if (dataSet == 'myposts') {
				$scope.dashboardData.myPostsByType = {};
				$scope.dashboardData.myPostsByType.P = _.find($scope.myPostsByType, fnFindElement, 'P') || {"FuelType":"P","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.L = _.find($scope.myPostsByType, fnFindElement, 'L') || {"FuelType":"L","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.V = _.find($scope.myPostsByType, fnFindElement, 'V') || {"FuelType":"V","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.M = _.find($scope.myPostsByType, fnFindElement, 'M') || {"FuelType":"M","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.S = _.find($scope.myPostsByType, fnFindElement, 'A') || {"FuelType":"A","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.T = _.find($scope.myPostsByType, fnFindElement, 'T') || {"FuelType":"T","FuelTypeRowCount":0};
				fnSetPctValues($scope.dashboardData.myPostsByType);
			}
			else if (dataSet == 'postsirated') {
				$scope.dashboardData.postsIRated = {};
				$scope.dashboardData.postsIRated.P = _.find($scope.postsIRated, fnFindElement, 'P') || {"FuelType":"P","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.L = _.find($scope.postsIRated, fnFindElement, 'L') || {"FuelType":"L","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.V = _.find($scope.postsIRated, fnFindElement, 'V') || {"FuelType":"V","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.M = _.find($scope.postsIRated, fnFindElement, 'M') || {"FuelType":"M","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.S = _.find($scope.postsIRated, fnFindElement, 'A') || {"FuelType":"A","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.T = _.find($scope.postsIRated, fnFindElement, 'T') || {"FuelType":"T","FuelTypeRowCount":0};
				fnSetPctValues($scope.dashboardData.postsIRated);
			}
			console.log($scope.dashboardData);
		}

		$rootScope.$on("$routeChangeSuccess", function(event, current) {
			console.log('location: ', $location);
			$rootScope.currentPath = $location.$$path;
			//$scope.number = $routeParams['number'];
		});

		$scope.logout = function () {
			$rootScope.currentUser = null;
			$cookieStore.remove('currentUser');
		}

		$scope.setView = function (view) {
			switch(view) {
			    case 'sign-in':
			        $('.unknown').hide();
					$('section.profile .register, section.profile .manage-account, section.profile .reset-password').hide();
					$('section.profile, section.profile .login, .lightbox-screen.profile').show();
					break;
				case 'reset-password':
					$('section.profile .login, section.profile .register, section.profile .manage-account').hide();
					$('section.profile, section.profile .reset-password, .lightbox-screen.profile').show();
					break;
				case 'sign-up':
					$('section.profile .login, section.profile .manage-account, section.profile .reset-password').hide();
					$('section.profile, section.profile .register, .lightbox-screen.profile').show();
					break;
				case 'manage-account':
					$('section.profile .login, section.profile .register, section.profile .reset-password, section.profile .reset-password').hide();
					$('section.profile, section.profile .manage-account, .lightbox-screen.profile').show();
					break;
				default:
					$('section.profile, section.profile .login, section.profile .register, section.profile .manage-account, .lightbox-screen.profile').hide();
					break;
			}
		};

		/* Ratings */
		$scope.resetCurrentRatingValues = function () {
			$scope.selectedRatingValue = $rootScope.selectedPost.FuelRatingCount;
			$scope.ratingComment = '';
		};
		$scope.setCurrentRating = function (value) {
			console.log(value);
			$scope.selectedRatingValue = value;
		};
		$scope.getPostRatings = function (postId) {
			$scope.svc.post.getPostRatings(postId);
		};



		if ($cookieStore.get('currentUser')) {
			$rootScope.currentUser = angular.fromJson($cookieStore.get('currentUser'));
            $rootScope.selectedUser = $rootScope.currentUser.UserId;
			$rootScope.isLoggedIn = 'true';
		}

		$scope.dashboardData = $scope.initDashboardData();
		if ($rootScope.currentUser) {
			$scope.getDashboardInfo()
		};


		$scope.matchingUsers = [];
		$scope.matchingSearchUsers = [];
		$scope.matchingSearchHashtags = [];

		$scope.checkInputForUsernameAndHashtags = function () {
			if ($scope.searchText === '') {
				$scope.matchingSearchHashtags = [];
				$scope.matchingSearchUsers = [];
			}
			else {
				$scope.svc.search.searchUsers($scope.searchText)
					.success(function (data, status) {
						$scope.matchingSearchUsers = data.Users;
					})
					.error(function (data, status) {

					});
				$scope.svc.search.searchTags($scope.searchText)
					.success(function (data, status) {
						$scope.matchingSearchHashtags = data.replace(/["#@]/g,'').split(', ');
					})
					.error(function (data, status) {

					});
			}
		};

		$rootScope.searchUser = function (user) {
			$rootScope.selectedUser = user.UserId;
			$scope.searchText = user.UserName;
			$scope.matchingSearchHashtags = [];
			$scope.matchingSearchUsers = [];
		};

		$rootScope.searchTag = function (tag) {
			$rootScope.selectedHashTag = tag;
			$scope.searchText = tag;
			$scope.matchingSearchHashtags = [];
			$scope.matchingSearchUsers = [];
		};

		$scope.getWords = function (text) {
			return text.split(/\s/);
		};

		$scope.getLastWord = function (text) {
			var words = $scope.getWords(text)
				, lastWord = words[words.length-1];
			return lastWord;
		};
	}]);
});