/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/15/13
 * Time: 11:13 PM
 */

define(['angular','jquery','amd/util','mejs'], function (ng, $, Util) {
	var app = function() {

		}
		, _dialTransformOriginPercentX = .5
		, _dialTransformOriginPercentY = .54275
		, _POST_TYPE_LOCATION = 'L'
		, _POST_TYPE_PHOTO = 'P'
		, _POST_TYPE_VIDEO = 'V'
		, _POST_TYPE_MUSIC = 'M'
		, _POST_TYPE_SOUND = 'S'
		, _POST_TYPE_THOUGHT = 'T'
		, _currentRating = null
		, _ngMainControllerScope;

	function setRating(value, comment) {
		_currentRating = value;
	}

	app.prototype = {
		getCurrentRating: function() {
			return _currentRating;
		}
		, eventTypes: {
			onLoadMoreModules: 'load-more-modules'
		}
		, moduleTypes: {
			m1:     {units:2}
			, m2:   {units:3}
			, m3:   {units:9}
			, m4:   {units:2}
			, m5:   {units:2}
			, m6:   {units:4}
		}
		, util: Util
		, clickRatingDial: function (e) {
			e.stopImmediatePropagation();
			if (e.target.className.indexOf('rate-listener') !== -1) {
				var x1 = $(this).width() * _dialTransformOriginPercentX,
					y1 = $(this).height() * _dialTransformOriginPercentY;

				if(e.offsetX==undefined) // this works for Firefox
				{
					e.offsetX = e.pageX-$(this).offset().left;
					e.offsetY = e.pageY-$(this).offset().top;
				}
				if (e.offsetY < y1) {
					var angleInDeg = Util.getAngle(e.offsetX - x1, y1 - e.offsetY)
						, rotClassname = $this.getRotationClassname(angleInDeg);
					setRating(parseInt(rotClassname.replace('rot','')));
					$(this)
						.prev()
						.removeClass('rot0 rot1 rot2 rot3 rot4 rot5')
						.addClass(rotClassname);
				}
			}
		}
		, getRotationClassname: function (angleOfRotation) {
			if (angleOfRotation <= 30) {
				return 'rot5';
			}
			else if (angleOfRotation <= 60) {
				return 'rot4';
			}
			else if (angleOfRotation <= 90) {
				return 'rot3';
			}
			else if (angleOfRotation <= 120) {
				return 'rot2';
			}
			else if (angleOfRotation <= 150) {
				return 'rot1';
			}
			else {
				return 'rot0';
			}
		}
		, showRatingPanel: function (e) {
			e.stopImmediatePropagation();
			if ($(document.body).attr('data-isloggedin') == 'true') {
				var bkgdImg = $(this).closest('.item').data('bkgd-img')
					, postType = $(this).closest('.item').data('post-type')
					, rating = $(this).closest('.item').data('post-rating');
				$('section.wall .lightbox-screen').show();
				if (postType === _POST_TYPE_THOUGHT) {

				}
				else {
					$('section.wall .rating-container')
						.css({display:'block!important', 'background-image':'url('+bkgdImg+')',left:'50%',top:'50%','margin-left':$(document.body).scrollLeft()-229,'margin-top':$(document.body).scrollTop()-150})
						.show();
				}
			}
		}
		, showDetailRatingPanel: function (e) {
			e.stopImmediatePropagation();
			if ($(document.body).attr('data-isloggedin') == 'true') {
				var postType = $(this).closest('.item').data('post-type')
					, rating = $(this).closest('.item').data('post-rating');
				if (postType === _POST_TYPE_THOUGHT) {

				}
				else {
					$('.rating-container').show();
				}
			}
		}
		, handleDetailRatingPost: function (e) {
			e.stopImmediatePropagation();
			$('section.post-detail .rating-container').fadeOut(function () {
				$('section.post-detail .lightbox-screen').hide();
			});
			$('section.post-detail .thank-you-message').fadeIn(function () {
				setTimeout(function () {
					$('section.post-detail .thank-you-message').fadeOut();
				}, 3000);
			});
		}
		, hideRatingPanel: function (e) {
			e.stopImmediatePropagation();
			$('.rating-container, .lightbox-screen').fadeOut();
			_currentRating = null;
		}
		, showDetailView: function (e) {
			e.stopImmediatePropagation();
			var removeClass = (_instance.modulesAcross == 1)?'expanded':'collapsed';
			var addClass = (_instance.modulesAcross == 1)?'collapsed':'expanded';
			var asset = $(this).data('bkgd-img');
			$('section.post-detail')
				.removeClass(removeClass)
				.addClass(addClass)
				.show();

			if ($(this).hasClass('V')) {

				var vid = $('<video id="hfvideo" width="100%" height="90%" controls="controls" preload="none">' +
					'<source type="video/mp4" src="' + asset + '" />' +
					'<object width="640" height="480" type="application/x-shockwave-flash" data="flashmediaelement.swf">' +
					'	<param name="movie" value="flashmediaelement.swf" />' +
					'	<param name="flashvars" value="controls=true&file=' + asset + '" />' +
					'</object></video>');
				$('section.post-detail').append(vid);
				$('video').mediaelementplayer(/*options*/);
			}
			else if ($(this).hasClass('S')) {
				var audio = $('<audio id="hfaudio" controls="true" name="media" src="' + asset + '"></audio>');
				$('section.post-detail').append(audio);
				$('audio').mediaelementplayer(/*options*/);
			}
			else {
				$('section.post-detail .full-page-image')
					.css({'background-image':'url('+asset+')'});
			}
		}
		, hideStaticContent: function (e) {
			$('section.static, section.static section').hide();
		}
		, showStaticContent: function (e) {
			e.stopImmediatePropagation();
			$('.mobile-menu').removeClass('active');
			$('section.static section').hide();
			$('section.static').show();
			switch ($(this).data('static-content-target')) {
				case 'terms':
					$('section.static section.terms').show();
					break;
				case 'contact':
					$('section.static section.contact').show();
					break;
				case 'mission':
				default:
					$('section.static section.mission').show();
					break;
			}
			$('.welcome, .lightbox-screen').hide();
		}
		, closeDetailView: function (e) {
			e.stopImmediatePropagation();
			$('section.post-detail, .thank-you-message').hide();
			$('section.post-detail .follow').show();
			$('section.post-detail audio').attr('src', '');
			$('section.post-detail video').attr('src', '');
			$('.mejs-container').remove();
			$('section.post-detail textarea').val('');
			$('section.post-detail .full-page-image')
				.css({'background-image':'none'});
			$this.hideRatingPanel(e);
		}
		, expandCollapseDetailPane: function (e) {
			e.stopImmediatePropagation();
			$('section.post-detail').toggleClass('expanded collapsed');
			$(this).html(($('section.post-detail').hasClass('expanded'))?'&minus;':'+');
		}
		, showHideFilterMenu: function (e) {
			e.stopImmediatePropagation();
			$('header nav .sort-menu .menu ul').hide();
			$('.menu ul', this).toggle();
		}
		, showHideSortMenu: function (e) {
			e.stopImmediatePropagation();
			$('header nav .filter-menu .menu ul').hide();
			$('.menu ul', this).toggle();
		}
		, showSearchBar: function (e) {
			e.stopImmediatePropagation();
			$('header nav .filter-menu, header nav .sort-menu').hide();
			$('header nav .search').removeClass('off');
			$('header nav .search input').focus();
		}
		, hideSearchBar: function (e) {
			e.stopImmediatePropagation();
			$('header nav .filter-menu, header nav .sort-menu').show();
			$('header nav .search').addClass('off');
		}
		, showHideDashboard: function (e) {
			e.stopImmediatePropagation();
			if ($(document.body).attr('data-isloggedin') == 'true') {
				$('section.dashboard').toggle();
			}
		}
		, hideDashboard: function (e) {
			if (e)
				e.stopImmediatePropagation();
			$('section.dashboard').hide();
		}
		, handleEmptySpaceClick: function (e) {
			$('section.dashboard').hide();
			$('header nav .menu ul').hide();
		}
		, navigateDashboard: function (e) {
			var fnGetActiveDashboardView = function () {
					var active = $('section.dashboard .navigation .nav-state.on');
					for (var i= 0, count = dashboardViews.length; i<count; i++) {
						if (active.data('view') === dashboardViews[i]) {
							return {view: dashboardViews[i], currentPos: i, totalViews: count};
						}
					}
					return null;
				}
				fnSetActiveView = function (viewName) {
					$('.nav-state').removeClass('on').addClass('off');
					$('div[data-view="' + viewName + '"]').removeClass('off').addClass('on');
					$('section.dashboard > section').hide();
					$('section.dashboard > section.' + viewName).show();
				}
				, dashboardViews = ['fuels', 'fuelers', 'friends']
				, currentDashboardInfo = fnGetActiveDashboardView()
				, navDirectionIsUp = $(this).hasClass('arrow-up');

			if (!currentDashboardInfo) {
				fnSetActiveView(dashboardViews[0]);
			}
			else if (navDirectionIsUp) {
				if (currentDashboardInfo.currentPos > 0) {
					fnSetActiveView(dashboardViews[currentDashboardInfo.currentPos-1]);
				}
			}
			else {
				if (currentDashboardInfo.currentPos < (dashboardViews.length-1)) {
					fnSetActiveView(dashboardViews[currentDashboardInfo.currentPos+1]);
				}
			}
		}
		, modulesAcross: 0
		, sizeWallModules: function () {
			var deferred = $.Deferred();
			setTimeout(function () {
				var docWidth = $(window).width()
					, moduleSquare = docWidth * .996;
					_instance.modulesAcross = 1;
				// breakpoints
				if (docWidth > 1440) {
					moduleSquare = docWidth * .247;
					_instance.modulesAcross = 4;
				}
				else if (docWidth > 1024) {
					moduleSquare = docWidth * .331;
					_instance.modulesAcross = 3;
				}
				else if (docWidth > 480) {
					moduleSquare = docWidth * .4965;
					_instance.modulesAcross = 2;
				}
				$('section.wall div.module').css({width:moduleSquare, height:moduleSquare});
				deferred.resolve();
			}
			, 100);
			return deferred;
		}
		, docHeight: 0
		, viewportHeight: 0
		, hasTriggeredScrollLoad: false
		, handleScrollTrigger: function () {
			var scrollY = $(document).scrollTop()
				, triggerRangeInPixels = 30;
			if (!_instance.hasTriggeredScrollLoad) {
				_instance.docHeight = $(document).height();
				_instance.viewportHeight = $(window).height();
				if (scrollY > (_instance.docHeight - _instance.viewportHeight - triggerRangeInPixels)) {
					_instance.hasTriggeredScrollLoad = true;
					ng.element('body').scope().$broadcast(_instance.eventTypes.onLoadMoreModules);
				}
			}
		}
        , resetWallPosition: function () {
            $(window).scrollTop(0);
        }
        , setPostTypeMenuTitle: function (type) {
            var title = 'the newest';
            switch (type) {
                case 'H':
                    title = 'the happiest';
                    break;
                case 'F':
                    title = 'your friend\'s';
                    break;
                case 'O':
                    title = 'your own';
                    break;
            }
            $('header .sort-menu .title').text(title);
        }
        , setFuelTypeMenuTitle: function (type) {
            var title = 'photos';
            switch (type) {
                case 'L':
                    title = 'locations';
                    break;
                case 'M':
                    title = 'music';
                    break;
                case 'S':
                    title = 'sounds';
                    break;
                case 'V':
                    title = 'videos';
                    break;
                case 'T':
                    title = 'thoughts';
                    break;
            }
            $('header .filter-menu .title').text(title);
        }
	};

	var _instance = null;
	_instance = _instance || new app()
	var $this = _instance;

	// bootstrap event handlers
	$(document.body).on('click touch', 'section.post-detail > .btn-close', _instance.closeDetailView);
	$(document.body).on('click touch', 'section.post-detail .rate-listener .btn-close', _instance.hideRatingPanel);
	$(document.body).on('click touch', 'section.post-detail .btn-expand-collapse', _instance.expandCollapseDetailPane);
	$(document.body).on('click touch', 'header nav .sort-menu', _instance.showHideSortMenu);
	$(document.body).on('click touch', 'header nav .filter-menu', _instance.showHideFilterMenu);
	$(document.body).on('click touch', 'header nav .user', _instance.showHideDashboard);
	$(document.body).on('click touch', 'section.dashboard .user-box', _instance.hideDashboard);
	$(document.body).on('click touch', 'header', _instance.handleEmptySpaceClick);
	$(document.body).on('click touch', 'section.dashboard .navigation > div[class*="arrow"]', _instance.navigateDashboard);
	$(document.body).on('click touch', 'section.wall .item .post-info', _instance.showRatingPanel);
	$(document.body).on('click touch', 'section.post-detail .dial-tab', _instance.showDetailRatingPanel);
	$(document.body).on('click touch', 'section.post-detail .btn-post', _instance.handleDetailRatingPost);
	$(document.body).on('click touch', 'section.wall .item, section.dashboard .fuels .avatar-row .avatar', _instance.showDetailView);
	$(document.body).on('click touch', 'section.wall .lightbox-screen, section.post-detail .lightbox-screen, section.wall .rating-container .btn-post', _instance.hideRatingPanel);
	$(document.body).on('click touch', '.rate-listener', _instance.clickRatingDial);
	$(document.body).on('click touch', 'header nav .sort-menu .menu-item-search', _instance.showSearchBar);
	$(document.body).on('click touch', 'header nav .search .btn-close', _instance.hideSearchBar);
	$(document.body).on('click touch', 'section.static .btn-close, header nav .menu ul li', _instance.hideStaticContent);
	$(document.body).on('click touch', '.static-content-target', _instance.showStaticContent);
	$(document.body).on('click touch', 'a.follow', function (e) {
		e.stopPropagation();
		$(this).fadeOut();
	});
	$(document.body).on('click touch', '.mobile-menu', function (e) {
		e.stopPropagation();
		$(this).toggleClass('active');
	});
	$(document.body).on('click touch', 'div.welcome button, div.welcome, .lightbox-screen', function (e) {
		e.stopPropagation();
		$(this).fadeOut();
		$('.lightbox-screen').fadeOut();
	});
	$(document.body).on('click touch', 'section.profile .button.pink', function (e) {
		$('.welcome, .lightbox-screen, section.profile').fadeOut();
	});
	$(window).resize(_instance.sizeWallModules);
	$(window).scroll(_instance.handleScrollTrigger);

	return _instance;
});