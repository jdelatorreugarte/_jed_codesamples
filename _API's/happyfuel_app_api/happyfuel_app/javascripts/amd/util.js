/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/14/13
 * Time: 12:19 AM
 */

define ([], function () {
   var util = function () {

   } ;

	util.prototype = {
		getAngle: function (x,y) {
			return Math.atan2(y,x) * 180/Math.PI;
		}
		, getRandomIntegerInRange: function (min, max) {
			return Math.floor((Math.random()*max)+min);
		}
		, getArrayOfNonRepeatingRandomIntegers: function (min, max, arraySize) {
			var container = []
				, min = min || 1
				, max = max || 10
				, arraySize = arraySize || max;
			if (arraySize > max) {
				arraySize = max;
			}
			while (container.length == 0 || container.length < arraySize) {
				var tryIntValue = this.getRandomIntegerInRange(min, max);
				if (_.indexOf(container, tryIntValue) == -1) {
					container.push(tryIntValue);
				}
			}
			return container;
		}
	};

    return new util();
});