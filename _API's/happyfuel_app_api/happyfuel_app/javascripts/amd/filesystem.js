/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 9/27/13
 * Time: 11:05 AM
 */


define(['fsapi', 'jquery'], function (fs, $) {


	var _class = function (options) {
		this.options = initOptions(options);
		var input = document.getElementById(this.options.fileInputId);
		$this = this;
		$(input).on('change', function (evt) { $this.onFileSelection(evt); });
	};

	// private methods
	function initOptions(o) {
		if (!o)
			o = {};
		o.fileInputId = o.fileInputId || 'user-files';
		o.uploadUrl = o.uploadUrl || '';
		o.imgPreviewContainerId = o.imgPreviewContainerId || 'img-preview';
		o.imgPreviewSize = o.imgPreviewSize || {width: 150, height: 150};
		o.fileTypeFilter = o.fileTypeFilter || /image/;
		return o;
	}

	// public instance
	_class.prototype = {
		options: null
		, files: null
		, onFileSelection: function (evt) {
			this.files = FileAPI.getFiles(evt.target);
			this.setImgPreview(evt);
		}
		, setImgPreview: function (evt) {
			var imageList = FileAPI.filter(this.files, function (file){ return /image/.test(file.type); })
				, previewNode = $('#'+$(evt.target).data('preview-container'))
				, $this = this;

			FileAPI.each(imageList, function (imageFile){
				FileAPI.Image(imageFile)
					.preview($this.options.imgPreviewSize.width, $this.options.imgPreviewSize.height)
					.get(function (err, image){
						if( err ){
							console.log('preview image error: ', err);
						}
						else {
							previewNode
								.html('')
								.append(image);
						}
					})
				;
			});
		}
		, upload: function (url, postData, headers, imgTransform, fnComplete) {
			url = url || this.options.uploadUrl;

			var xhr = FileAPI.upload({
				url: url,
				data: postData || {},
				headers: headers || {},
				files: {
					images: FileAPI.filter(this.files, function (file){ return /image/.test(file.type); })
				},

				chunkSize: 0, // or chunk size in bytes, eg: FileAPI.MB*.5 (html5)
				chunkUploadRetry: 0, // number of retries during upload chunks (html5)

				imageTransform: imgTransform || {
					maxWidth: 1024,
					maxHeight: 768
				},
				imageAutoOrientation: true,
				prepare: function (file, options){
					// prepare options for current file
					options.data.filename = file.name;
				},
				upload: function (xhr, options){
					// start uploading
					console.log('all files start uploading');
				},
				fileupload: function (xhr, options){
					// start file uploading
					console.log('file start uploading');
				},
				fileprogress: function (evt){
					// progress file uploading
					var filePercent = evt.loaded/evt.total*100;
					console.log(filePercent, '% of file uploaded');
				},
				filecomplete: function (err, xhr){
					if( !err ){
						var response = xhr.responseText;
					}
				},
				progress: function (evt){
					// total progress uploading
					var totalPercent = evt.loaded/evt.total*100;
					console.log(totalPercent,'% of all files uploaded');
				},
				complete: fnComplete || function (err, xhr){
					if( !err ){
						// Congratulations, the uploading was successful!
						console.log('upload was successful');
					}
				}
			});
		}
	};

	return _class;
});