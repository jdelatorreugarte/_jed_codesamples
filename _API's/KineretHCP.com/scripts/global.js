﻿$(document).ready(function () {


//HOME SCRIPTS

    // rollover
    $('#content_body section  img').mouseenter(function () {
        $(this).attr('src', 'images/header_' + $(this).attr('data') + '_on.png');
    });

    $('#content_body section  img').mouseout(function () {
        $(this).attr('src', 'images/header_' + $(this).attr('data') + '_off.png');
    });


//HCP SCRIPTS


    // RA rollover
    $('.sub_indented img').mouseenter(function () {
        $(this).attr('src', '../images/hcp-images/sub_ra_' + $(this).attr('data') + '_on.png');
    });

    $('.sub_indented img').mouseout(function () {
        $(this).attr('src', '../images/hcp-images/sub_ra_' + $(this).attr('data') + '_off.png');
    });



// swap hero image
var _counter = 1;
setInterval(function () {
    _counter = _counter + 1;
    if (_counter == 6) {
        _counter = 1;
    }

    // swap image
    $('#hero img').attr('src', '../images/hcp-images/hero_' + _counter + '.png');
}, 6000);



//PATIENT SCRIPTS

// swap hero image
var _counter = 1;
setInterval(function () {
    _counter = _counter + 1;
    if (_counter == 5) {
        _counter = 1;
    }

    // swap image
    $('#hero img').attr('src', '../images/patient-images/hero_' + _counter + '.png');
}, 6000);

});

