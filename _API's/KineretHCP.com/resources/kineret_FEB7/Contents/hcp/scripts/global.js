﻿$(document).ready(function () {
    // RA rollover
    $('.sub_indented img').mouseenter(function () {
        $(this).attr('src', '/Contents/hcp/images/sub_ra_' + $(this).attr('data') + '_on.png');
    });

    $('.sub_indented img').mouseout(function () {
        $(this).attr('src', '/Contents/hcp/images/sub_ra_' + $(this).attr('data') + '_off.png');
    });

    
});

// swap hero image
var _counter = 1;
setInterval(function () {
    _counter = _counter + 1;
    if (_counter == 6) {
        _counter = 1;
    }

    // swap image
    $('#hero img').attr('src', '/Contents/hcp/images/hero_' + _counter + '.png');
}, 6000);