﻿$(document).ready(function () {
    // rollover
    $('#content_body section  img').mouseenter(function () {
        $(this).attr('src', '/Contents/home/images/header_' + $(this).attr('data') + '_on.png');
    });

    $('#content_body section  img').mouseout(function () {
        $(this).attr('src', '/Contents/home/images/header_' + $(this).attr('data') + '_off.png');
    });
});