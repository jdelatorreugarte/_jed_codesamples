﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace kineret.Controllers
{
    public class HCPController : Controller
    {
        // GET: /HCP/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /HCP/AboutKineret
        public ActionResult AboutKineret()
        {
            return View();
        }

        // GET: /HCP/TreatingRAPatients
        public ActionResult TreatingRAPatients()
        {
            return View();
        }

        // GET: /HCP/TreatingNOMIDPatients
        public ActionResult TreatingNOMIDPatients()
        {
            return View();
        }

        // GET: /HCP/Isi
        public ActionResult ISI()
        {
            return View();
        }


        // GET: /HCP/ReimbursementSupport
        public ActionResult ReimbursementSupport()
        {
            return View();
        }
    }
}
