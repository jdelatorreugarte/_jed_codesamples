﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace kineret.Controllers
{
    public class PatientController : Controller
    {
        // GET: /HCP/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /HCP/AboutKineret
        public ActionResult AboutKineret()
        {
            return View();
        }

        // GET: /HCP/TakingKineret
        public ActionResult TakingKineret()
        {
            return View();
        }

        // GET: /HCP/AboutAutoinflammatoryDisease
        public ActionResult AboutAutoinflammatoryDisease()
        {
            return View();
        }

        // GET: /HCP/KineretKarePatientSupport
        public ActionResult KineretKarePatientSupport()
        {
            return View();
        }
        // GET: /HCP/Isi
        public ActionResult ISI()
        {
            return View();
        }
    }
}
