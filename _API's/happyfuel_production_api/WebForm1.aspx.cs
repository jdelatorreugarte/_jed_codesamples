﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.Text;
using System.IO;


namespace HappyFuelAppServices
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnConvert_Click(object sender, EventArgs e)
        {
            string NewFileName = System.IO.Path.GetFileNameWithoutExtension(this.txtFileName.Text) + ".mp4";

            string ext = System.IO.Path.GetExtension(this.txtFileName.Text).ToLower();

            if (ext == ".caf" || ext == ".amr" || ext == ".m4a")
            {

                HappyFuelAmazonTransCoder.CreateJobRequest(this.txtFileName.Text, NewFileName, "HappyFuel", "HappyFuel");

                Response.Write("File Converted Successfully");
            }
            
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {

            DAL dal = new DAL();

            var url = this.txtURL.Text;
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = "POST";
            request.ContentType = "application/json; charset=utf-8";


            var addRequest = new { Password=this.txtPassword.Text, GUID=this.txtGUID.Text };

            var jsSerializer = new JavaScriptSerializer();
            var jsonPostRequest = jsSerializer.Serialize(addRequest);

            var writer = new StreamWriter(request.GetRequestStream());
            writer.Write(jsonPostRequest);
            writer.Close();

            var httpWebResponse = (HttpWebResponse)request.GetResponse();
            string jsonString;

            using (var sr = new StreamReader(httpWebResponse.GetResponseStream()))
            {
                jsonString = sr.ReadToEnd();
            }

            var jsonPostResponse = jsSerializer.Deserialize<dynamic>(jsonString);

            this.txtResponse.Text = jsonString;

        }

        internal static void AddValue(System.Collections.Specialized.NameValueCollection values, string key, object value)
        {
            DataContractJsonSerializer jss = new DataContractJsonSerializer(value.GetType());
            using (MemoryStream ms = new MemoryStream())
            {
                jss.WriteObject(ms, value);
                string json = System.Text.Encoding.UTF8.GetString(ms.ToArray());
                values.Add(key, json);
            }
        }
    }
}