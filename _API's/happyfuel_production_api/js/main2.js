// GET QUERYSTRING DATA FUNCTION
function getQueryVariable(variable) {
    var query = window.location.search.substring(1),
		vars = query.split("&");

    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }

    return (false);
}

// GOOGLE MAP FUNCTION
var map;
function initialize(x, y) {
    var mapOptions = {
        zoom: 16,
        center: new google.maps.LatLng(x, y),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById('map-canvas'),
        mapOptions);
}

(function ($) {

    var url = "http://services.happyfuelapp.com/HFWebServices.svc";
    //url="http://localhost:15555/HFWebServices.svc";

    var postID = getQueryVariable('ID'), //Pulls postID from querystring
  		postByID = url + "/GetPostByID/" + postID + "?callback=?;"; //didn't work. Server might not be configured for cross-domain JSONP. Had to use test data. Won't be an issue is page is on same domain as webservice.

    $.getJSON(postByID, function (data) {

        var hfInfo = data.GetPostByIDResult.Post[0],
		userID = hfInfo.UserId,
		userInfo = url + "/GetUser/" + userID + "?callback=?;"; //didn't work. Server might not be configured for cross-domain JSONP. Had to use test data. Won't be an issue is page is on same domain as webservice.


        $.getJSON(userInfo, function (data) {

            var hfUserInfo = data.GetUserResult.CurrentUser;

            $('.name').text(hfUserInfo.UserName); // CHANGE USERNAME
            // $('.location').text(hfUserInfo); CHANGE CITY, STATE - Values do not exist

        });

        var src_file = "https://s3.amazonaws.com/HappyFuelApp/" + hfInfo.FuelData;
        //var src_file = "http://localhost:15555/HappyFuelWeb/Media/" + hfInfo.FuelData;
		var src_thumbnail_file= "'https://s3.amazonaws.com/HappyFuelApp/" + hfInfo.FuelThumbData + "'";
 
        var fuel_caption = hfInfo.FuelCaption;

        var fuel_title = hfInfo.FuelTitle;

        $('.caption-title').text(hfInfo.FuelTitle); // CHANGE POST TITLE

        $('.caption-text').text(hfInfo.FuelCaption); // CHANGE POST CAPTION

        switch (hfInfo.FuelType) {

            // PICTURE		
            case "P":
                $('.post .picture').attr('src', src_file).show(); // CHANGE POST IMAGE
				$('.post .picture').attr('onerror', 'this.src=' + src_thumbnail_file).show();
                fuel_title = " fueled a photo: " + fuel_title;
                break;

                // LOCATION		
            case "L":
                $('#map-canvas').show();
                initialize(hfInfo.FuelLatitude, hfInfo.FuelLongitude);
                fuel_title = " fueled a spot: " + fuel_title;
                break;

                // TEXT		
            case "T":
                $('.post .picture').attr('src', src_file).show(); // CHANGE POST IMAGE
                fuel_title = " fueled a thought: " + fuel_title;
                break;
                // MUSIC		
            case "V":
                fuel_title = " fueled a video: " + fuel_title;
                $('.video-post').show();

                //alert(src_file);
                // IF IE, USE FLASH
                if (navigator.userAgent.indexOf("Trident/5") > -1 || navigator.userAgent.indexOf("Trident/6") > -1) {
                    _V_.options.techOrder = ["flash"];
                }

                // IF NO MP4 SUPPORT, USE FLASH
                if (!Modernizr.video.h264) {
                    _V_.options.techOrder = ["flash"];
                }
				

                // INITIALIZE VIDEOJS PLAYER
                _V_("video-file", {}, function () {
                    var myPlayer = this;
                    myPlayer.src(src_file);
					
                });
                break;


				
                // AUDIO		
            case "M":
                fuel_title = " fueled a beat: " + fuel_title;
                src_file = "https://s3.amazonaws.com/HappyFuelApp/" + hfInfo.FuelThumbData;
                $('.post .picture').attr('src', src_file).show(); // CHANGE POST IMAGE
                break;

            case "A":
                $('.audio-post').show();
                fuel_title = " fueled a sound: " + fuel_title;
                src_file = "https://s3.amazonaws.com/HappyFuelApp/" + hfInfo.FuelData.replace(/\..+$/, '') + ".mp4";
                //src_file = src_file.replace(/\..+$/, '')+".mp4";
                $('#audio-file').attr('src', src_file);
                audiojs.events.ready(function () {
                    var as = audiojs.createAll();
                });
                break;

        }

        //fuel_caption = fuel_caption + ": ) fuel (pronounced happy fuel) takes your happiness tank from empty to happy. It gives you a mobile place to collect and share the things that make you happy, whether it’s a photo, song, spot, video, sound or thought. And while you’re sharing happiness, : ) fuel even lets you gauge your own happiness.";
        fuel_caption = fuel_caption + '%0A%0Ahttp://www.happyfuelapp.com%0A';



        $('.facebook').attr('href', 'http://www.facebook.com/sharer/sharer.php?s=100&p[url]=' + hfInfo.TinyURL + '&p[images][0]=' + src_file + '&p[title]=' + fuel_title + '&p[summary]=' + fuel_caption); // CHANGE FACEBOOK SHARE LINK

        $('.twitter').attr('href', 'http://twitter.com/share?text=' + fuel_title + '%0A&url=' + hfInfo.TinyURL); // CHANGE TWITTER SHARE LINK

        $('.email').attr('href', 'mailto:someone@typeemailhere.com?subject=HappyFuel Post - ' + fuel_title + '&body=' + fuel_caption + '%0A' + hfInfo.TinyURL); // CHANGE FACEBOOK SHARE LINK


    });


})(jQuery);
