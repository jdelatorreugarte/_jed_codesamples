﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using HappyFuelAppServices;
using System.Data;
using System.Data.SqlClient;



namespace HappyFuelAppServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class HFWebServices : HFServices
    {
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public StatusMode ValidateUser(string Email, string Password)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                Int64 id = dal.ValidateUser(Email, Password);

                if (id > 0)
                {
                    status.Status = true;
                    status.Result = "SUCCESS";
                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: User Validation Failed";
                }

                status.ID = id.ToString();


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetUser(string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetUser(Convert.ToInt64(UserId));
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                        status.ID = user_id.ToString();
                        status.Result = "SUCCESS";

                        HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                        user.UserId = user_id;
                        user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                        user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                        user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                        user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                        user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                        user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                        user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                        user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                        user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                        status.CurrentUser = user;
                    }
                    else
                    {
                        status.Result = "FAIL:No User Found";
                        status.ID = "0";
                        status.Status = false;
                    }
                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetUserByGUID(string GUID)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetUserByGUID(GUID);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                        status.ID = user_id.ToString();
                        status.Result = "SUCCESS";

                        HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                        user.UserId = user_id;
                        user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                        user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                        user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                        user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                        user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                        user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                        user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                        user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                        user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                        status.CurrentUser = user;
                    }
                    else
                    {
                        status.Status = false;
                        status.Result = "FAIL:No User Found";
                        status.ID = "0";
                    }

                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode ResetPassword(string Email)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                string new_guid = Guid.NewGuid().ToString();


                //reset password as new guid and send as link
                DataSet ds = dal.ResetPassword( Email, new_guid, 1);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                    status.CurrentUser = user;

                    try
                    {
                        //Send email to user with link to reset password
                        EmailSender email_client = new EmailSender();

                        StringBuilder sb = new StringBuilder();

                        sb.AppendLine("You have requested to reset your password for HappyFuel Application.</br>");

                        sb.AppendLine("Your USER ID is " + user.UserName + ".</br>");

                        sb.AppendLine("Please click following URL to reset your password " + ConfigManager.HappyFuelWebSiteURL + @"hgid=" + new_guid.ToUpper() + ".</br>");

                        string body = sb.ToString();

                        string subject = "Happy Fuel App Services: Reset Password Notification";
                        string To = user.UserEmail;
                        email_client.SendEmail(body, subject, To, "", "", ConfigManager.SMTPFromAddress.ToString(), "High", ConfigManager.SMTPServer.ToString(), "", "1");
                    }
                    catch (Exception smtpex)
                    {
                        LogError.LogException(smtpex);
                        status.Result = "FAIL:SMTP Failed";
                        status.ID = "0";
                        status.Status = false;
                    }
                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode ChangePassword(string UserName, string Email, string Password, string GUID)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();

                //reset password as new guid and send as link
                DataSet ds = dal.ChangePassword(UserName, Email, Password,GUID, 0);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                    status.CurrentUser = user;

                    //Send email to user with link to reset password

                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode ChangePasswordByGUID(string Password, string GUID)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();

                //reset password as new guid and send as link
                DataSet ds = dal.ChangePasswordByGUID(Password, GUID, 0);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                    status.CurrentUser = user;

                    //Send email to user with link to reset password

                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode SaveUser(string UserId,string UserName, string Password, string Email)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();

                DataSet dsVerify = dal.VerifyUserNameEmail(Convert.ToInt64(UserId),UserName, Email);
                if (dsVerify != null && dsVerify.Tables.Count > 0 && dsVerify.Tables[0].Rows.Count > 0)
                {
                    string StatusCode = dsVerify.Tables[0].Rows[0]["Status"].ToString();

                    if (StatusCode.Trim() != "")
                    {
                        status.Status = false;
                        status.Result = StatusCode;
                        status.ID = "0";

                        return status;
                    }

                }

                
                
                DataSet ds = dal.SaveUser(Convert.ToInt64(UserId), "", "", "", "", UserName, Password, Email, 1,0);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.IsTemporaryPassword = Convert.ToInt16(ds.Tables[0].Rows[0]["is_temp_password"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                    //status.CurrentUser = user;
                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode UpdateUser(string UserId, string UserName, string Password, string Email)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();

                DataSet dsVerify = dal.VerifyUserNameEmail(Convert.ToInt64(UserId), UserName, Email);
                if (dsVerify != null && dsVerify.Tables.Count > 0 && dsVerify.Tables[0].Rows.Count > 0)
                {
                    string StatusCode = dsVerify.Tables[0].Rows[0]["Status"].ToString();

                    if (StatusCode.Trim() != "")
                    {
                        status.Status = false;
                        status.Result = StatusCode;
                        status.ID = "0";

                        return status;
                    }

                }


                DataSet ds = dal.SaveUser(Convert.ToInt64(UserId), "", "", "", "", UserName, Password, Email, 1, 0);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.IsTemporaryPassword = Convert.ToInt16(ds.Tables[0].Rows[0]["is_temp_password"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                    //status.CurrentUser = user;
                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode DeletePost(string UserId, string PostId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                dal.DeletePost(Convert.ToInt64(UserId), Convert.ToInt64(PostId));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = PostId;
            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetPostsByUser(string UserId, string Limit)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                Int32 limit = 0;

                bool result = Int32.TryParse(Limit, out limit);


                DataSet ds = dal.GetPostsByUser(Convert.ToInt64(UserId),limit);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the user";
                    status.ID = UserId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetPostByID(string PostId)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetPostById(Convert.ToInt64(PostId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = PostId;

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the ID. Or ID is not made Public yet.";
                    status.ID = PostId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode MakePostPublic(string GUID, string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                dal.MakePostPublic(GUID, Convert.ToInt64(UserId));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = GUID;
            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetPostByGUID(string GUID)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetPostByGUID(GUID);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = GUID;
                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the GUID";
                    status.ID = GUID;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode SavePost(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.SavePost(Convert.ToInt64(UserId), Convert.ToInt64(FuelPostId), FuelPostDate, FuelType, FuelTitle, FuelCaption, Convert.ToInt16(FuelRating), FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, "", "", AppVersion, OSVersion, DeviceId, Convert.ToInt16(IsPublic), 1, Convert.ToInt16(IsFileUploaded));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = ds.Tables[0].Rows[0]["post_guid"].ToString();

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;
                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:POST Save failed.";
                    status.ID = "";
                }

                //Convert CAF Files to MP4 for able to listen in all browsers only for Audio Files generated from iPhone
                try
                {
                    ConvertToMP4(Convert.ToInt16(IsFileUploaded), FuelType, FuelData);
                }
                catch (Exception cex)
                {
                    LogError.LogException("Conversion to MP4 failed due to " + cex.ToString());
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode UpdatePost(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.SavePost(Convert.ToInt64(UserId), Convert.ToInt64(FuelPostId), FuelPostDate, FuelType, FuelTitle, FuelCaption, Convert.ToInt16(FuelRating), FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, "", "", AppVersion, OSVersion, DeviceId, Convert.ToInt16(IsPublic), 1, Convert.ToInt16(IsFileUploaded));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = ds.Tables[0].Rows[0]["post_guid"].ToString();

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;
                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:POST Save failed.";
                    status.ID = "";
                }

                //Convert CAF Files to MP4 for able to listen in all browsers only for Audio Files generated from iPhone
                try
                {
                    ConvertToMP4(Convert.ToInt16(IsFileUploaded), FuelType, FuelData);
                }
                catch (Exception cex)
                {
                    LogError.LogException("Conversion to MP4 failed due to " + cex.ToString());
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        private FuelPost GetPostByDataRow(DataRow dr)
        {
            FuelPost fp = new FuelPost();

            fp.FuelPostId = Convert.ToInt64(dr["fuel_post_id"].ToString());
            fp.UserId = Convert.ToInt64(dr["user_id"].ToString());
            fp.PostGUID = dr["post_guid"].ToString();
            fp.FuelPostDate = dr["fuel_post_date"].ToString();
            fp.FuelType = dr["fuel_type"].ToString();
            fp.FuelTitle = dr["fuel_title"].ToString();
            fp.FuelCaption = dr["fuel_caption"].ToString();
            fp.FuelRating = Convert.ToInt16(dr["fuel_rating"].ToString());
            fp.FuelData = dr["fuel_data"].ToString();
            fp.FuelThumbData = dr["fuel_thumb_data"].ToString();
            fp.FuelLatitude = dr["fuel_latitude"].ToString();
            fp.FuelLongitude = dr["fuel_longitude"].ToString();
            fp.UserLatitude = dr["user_latitude"].ToString();
            fp.UserLongitude = dr["user_longitude"].ToString();
            fp.ShareURL = ConfigManager.HappyFuelPostPageURL + dr["fuel_post_id"].ToString();
            fp.TinyURL = TinyUrlGenerator.MakeTinyUrl(ConfigManager.HappyFuelPostPageURL + dr["fuel_post_id"].ToString());
            fp.AppVersion = dr["app_version"].ToString();
            fp.OSVersion = dr["os_version"].ToString();
            fp.DeviceID = dr["device_id"].ToString();
            fp.IsPublic = Convert.ToInt16(dr["is_public"].ToString());
            fp.IsActive = Convert.ToInt16(dr["is_active"].ToString());
            fp.IsFileUploaded = Convert.ToInt16(dr["is_file_uploaded"].ToString());
            fp.InsertDateTime = dr["insert_datetime"].ToString();
            fp.UpdateDateTime = dr["update_datetime"].ToString();
            return fp;
        }

        public StatusMode GetLatestPostByUser(string UserId)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();

                DataSet ds = dal.GetLatestPostByUser(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the user";
                    status.ID = UserId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        /// <summary>
        /// POST methods. Necessary for sending large amount of texts
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="FuelPostId"></param>
        /// <param name="FuelPostDate"></param>
        /// <param name="FuelType"></param>
        /// <param name="FuelTitle"></param>
        /// <param name="FuelCaption"></param>
        /// <param name="FuelRating"></param>
        /// <param name="FuelData"></param>
        /// <param name="FuelThumbData"></param>
        /// <param name="FuelLatitude"></param>
        /// <param name="FuelLongitude"></param>
        /// <param name="UserLatitude"></param>
        /// <param name="UserLongitude"></param>
        /// <param name="AppVersion"></param>
        /// <param name="OSVersion"></param>
        /// <param name="DeviceId"></param>
        /// <param name="IsPublic"></param>
        /// <param name="IsFileUploaded"></param>
        /// <returns></returns>

        public StatusMode SavePostByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded)
        {
            return SavePost(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded);
        }

        public StatusMode UpdatePostByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded)
        {
            return UpdatePost(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded);
        }

        public StatusMode ChangePasswordByGUIDByPOST(string Password, string GUID)
        {
            return ChangePasswordByGUID(Password, GUID);
        }

        public string ChangePasswordByGUIDByPOSTNew(string Password, string GUID)
        {
            StatusMode status = ChangePasswordByGUID(Password, GUID);

            return status.Result;
        }

        public StatusMode ChangePasswordByPOST(string UserName, string Email, string Password, string GUID)
        {
            return  ChangePassword(UserName, Email, Password, GUID);
        }

        public StatusMode SaveUserByPOST(string UserId,string UserName, string Password, string Email)
        {
            return SaveUser(UserId, UserName, Password, Email);
        }

        public StatusMode UpdateUserByPOST(string UserId, string UserName, string Password, string Email)
        {
            return UpdateUser(UserId, UserName, Password, Email);
        }


        private void ConvertToMP4(Int16 IsFileUploaded, string FuelType, string FileName)
        {

            try
            {
                //Convert CAF Files to MP4 for able to listen in all browsers only for Audio Files generated from iPhone
                string NewFileName = System.IO.Path.GetFileNameWithoutExtension(FileName) + ".mp4";

                string ext = System.IO.Path.GetExtension(FileName).ToLower();

                if (ext == ".caf" || ext == ".amr" || ext == ".m4a")
                {

                    if (Convert.ToInt16(IsFileUploaded) == 1 && FileName.Trim() != "" && FuelType == "A")
                    {
                        HappyFuelAmazonTransCoder.CreateJobRequest(FileName, NewFileName, "HappyFuel", "HappyFuel");
                    }
                }
            }
            catch (Exception cex)
            {
                LogError.LogException("Conversion to MP4 Method failed due to " + cex.ToString());
            }


                        
        }
    }
}
