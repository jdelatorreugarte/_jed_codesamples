﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using Amazon;
using Amazon.ElasticTranscoder;
using Amazon.ElasticTranscoder.Model;



namespace HappyFuelAppServices
{
    public static class HappyFuelAmazonTransCoder
    {

        public static void CreateJobRequest(string OldFileName, string NewFileName, string BucketName, string OutPutBucketName)
        {


            string accsessKey = ConfigManager.HappyFuelAmazonAccessKey;
            string secretKey = ConfigManager.HappyFuelAmazonSecretKey;
            var etsClient = new Amazon.ElasticTranscoder.AmazonElasticTranscoderClient(accsessKey, secretKey, Amazon.RegionEndpoint.USEast1);


            //This is not required as we are using PreCreated PipeLines

            /*
            var notifications = new Amazon.ElasticTranscoder.Model.Notifications()
            {
                Completed = ConfigManager.HappyFuelAmazonARNCode,
                Error = ConfigManager.HappyFuelAmazonARNCode,
                Progressing = ConfigManager.HappyFuelAmazonARNCode,
                Warning = ConfigManager.HappyFuelAmazonARNCode
            };
            var pipeline = etsClient.CreatePipeline(new Amazon.ElasticTranscoder.Model.CreatePipelineRequest()
            {
                Name = "HappyFuelPipeLine",
                InputBucket = BucketName,
                OutputBucket = OutPutBucketName,
                Notifications = notifications,
                Role = "arn:aws:iam::209887743518:role/Elastic_Transcoder_Default_Role"
            }).CreatePipelineResult.Pipeline;
            */

            etsClient.CreateJob(new Amazon.ElasticTranscoder.Model.CreateJobRequest()
            {
                PipelineId = ConfigManager.HappyFuelAmazonTransCodePipeLineID,
                Input = new Amazon.ElasticTranscoder.Model.JobInput()
                {
                    AspectRatio = "auto",
                    Container = "mp4",
                    FrameRate = "auto",
                    Interlaced = "auto",
                    Resolution = "auto",
                    Key = OldFileName
                },
                Output = new Amazon.ElasticTranscoder.Model.CreateJobOutput()
                {
                    ThumbnailPattern = "",
                    Rotate = "0",
                    PresetId = ConfigManager.HappyFuelAmazonPresetID,
                    Key = NewFileName
                }
            });

        }

    }
}
