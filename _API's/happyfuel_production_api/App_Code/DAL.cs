﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using HappyFuelAppServices;

namespace HappyFuelAppServices
{
    public class DAL
    {

        public DataSet GetUser(Int64 user_id)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_get";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public DataSet GetUserByGUID(string GUID)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_getby_guid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@temp_password", GUID));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public DataSet ResetPassword(string Email, string TempPassword, Int16 IsTempPassword)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_reset_password";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Parameters.Add(new SqlParameter("@temp_password", TempPassword));
            cmd.Parameters.Add(new SqlParameter("@is_temp_password", IsTempPassword));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        public DataSet ChangePassword(string UserName, string Email, string Password, string GUID, Int16 IsTempPassword)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_change_password";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_name", UserName));
            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Parameters.Add(new SqlParameter("@password", Password));
            cmd.Parameters.Add(new SqlParameter("@GUID", GUID));
            cmd.Parameters.Add(new SqlParameter("@is_temp_password", IsTempPassword));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        public DataSet ChangePasswordByGUID(string Password, string GUID, Int16 IsTempPassword)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_change_passwordby_guid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@password", Password));
            cmd.Parameters.Add(new SqlParameter("@GUID", GUID));
            cmd.Parameters.Add(new SqlParameter("@is_temp_password", IsTempPassword));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        public DataSet VerifyUserNameEmail(Int64 UserId, string UserName, string Email)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_validate_username_email";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@user_name", UserName));
            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        //public DataSet VerifyEmail(Int64 UserId, string Email)
        //{

        //    DataSet ds = new DataSet();
        //    SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

        //    SqlCommand cmd = new SqlCommand();
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.CommandText = "hfsp_users_validate_email";
        //    cmd.CommandTimeout = 0;

        //    cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
        //    cmd.Parameters.Add(new SqlParameter("@user_email", Email));
        //    cmd.Connection = conn;

        //    SqlDataAdapter da = new SqlDataAdapter(cmd);

        //    conn.Open();
        //    da.Fill(ds);

        //    return ds;

        //}

        public DataSet SaveUser(Int64 UserId, string FirstName, string LastName, string MiddleName, string AvatarId, string UserName, string Password, string Email, Int16 IsActive, Int16 IsTempPassword)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_save";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@first_name", FirstName));
            cmd.Parameters.Add(new SqlParameter("@last_name", LastName));
            cmd.Parameters.Add(new SqlParameter("@middle_name", MiddleName));
            cmd.Parameters.Add(new SqlParameter("@avatar_id", AvatarId));
            cmd.Parameters.Add(new SqlParameter("@user_name", UserName));
            cmd.Parameters.Add(new SqlParameter("@password", Password));
            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Parameters.Add(new SqlParameter("@is_active", IsActive));
            cmd.Parameters.Add(new SqlParameter("@is_temp_password", IsTempPassword));


            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public Int64 ValidateUser(string Email, string Password)
        {
            Int64 user_id = 0;

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_validate";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Parameters.Add(new SqlParameter("@password", Password));
            cmd.Connection = conn;
            
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());
            }
            return user_id;
        }

        public DataSet SavePost(Int64 UserId, Int64 FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, Int16 FuelRating,string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string ShareUrl, string TinyUrl, string AppVersion, string OSVersion, string DeviceId, Int16 IsPublic, Int16 IsActive, Int16 IsFileUploaded)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_save";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", FuelPostId));
            cmd.Parameters.Add(new SqlParameter("@fuel_post_date", FuelPostDate));
            cmd.Parameters.Add(new SqlParameter("@fuel_type", FuelType));
            
            cmd.Parameters.Add(new SqlParameter("@fuel_title", FuelTitle));
            cmd.Parameters.Add(new SqlParameter("@fuel_caption", FuelCaption));
            cmd.Parameters.Add(new SqlParameter("@fuel_rating", FuelRating));
            cmd.Parameters.Add(new SqlParameter("@fuel_data", FuelData));
            cmd.Parameters.Add(new SqlParameter("@fuel_thumb_data", FuelThumbData));
            cmd.Parameters.Add(new SqlParameter("@fuel_latitude", FuelLatitude));
            cmd.Parameters.Add(new SqlParameter("@fuel_longitude", FuelLongitude));
            cmd.Parameters.Add(new SqlParameter("@user_latitude", UserLatitude));
            cmd.Parameters.Add(new SqlParameter("@user_longitude", UserLongitude));
            cmd.Parameters.Add(new SqlParameter("@share_url", ShareUrl));
            cmd.Parameters.Add(new SqlParameter("@tiny_url", TinyUrl));

            cmd.Parameters.Add(new SqlParameter("@app_version", AppVersion));
            cmd.Parameters.Add(new SqlParameter("@os_version", OSVersion));
            cmd.Parameters.Add(new SqlParameter("@device_id", DeviceId));
            cmd.Parameters.Add(new SqlParameter("@is_public", IsPublic));
            cmd.Parameters.Add(new SqlParameter("@is_active", IsActive));
            cmd.Parameters.Add(new SqlParameter("@is_file_uploaded", IsFileUploaded));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public void DeletePost(Int64 user_id,Int64 fuel_post_id )
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_delete";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet GetPostsByUser(Int64 user_id, Int32 limit)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_getby_user";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@limit", limit));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public DataSet GetPostById(Int64 fuel_post_id)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_getby_id";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public void MakePostPublic(string post_guid, Int64 user_id)
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_update_public";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@post_guid", post_guid));
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet GetPostByGUID(string post_guid)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_getby_guid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@post_guid", post_guid));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public DataSet GetLatestPostByUser(Int64 user_id)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_get_latest_user";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }


    }
}