﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using HappyFuelAppServices;

namespace HappyFuelAppServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface HFServices
    {

        [OperationContract]
        string GetData(int value);

        // TODO: Add your service operations here

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ValidateUser/{Email}/{Password}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ValidateUser(string Email, string Password);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ResetPassword/{Email}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ResetPassword(string Email);

   
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetUser/{UserId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetUser(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetUserByGUID/{GUID}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetUserByGUID(string GUID);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeletePost/{UserId}/{PostId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode DeletePost(string UserId, string PostId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPostsByUser/{UserId}/{Limit}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetPostsByUser(string UserId, string Limit);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPostByID/{PostId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetPostByID(string PostId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/MakePostPublic/{GUID}/{UserId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode MakePostPublic(string GUID,string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPostByGUID/{GUID}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetPostByGUID(string GUID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetLatestPostByUser/{UserId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetLatestPostByUser(string UserId);

        /// <summary>
        /// Save/Update methods..
        /// </summary>
        /// <param name="UserId"></param>
        /// <param name="UserName"></param>
        /// <param name="Password"></param>
        /// <param name="Email"></param>
        /// <returns></returns>
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SaveUser/{UserId}/{UserName}/{Password}/{Email}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SaveUser(string UserId, string UserName, string Password, string Email);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UpdateUser/{UserId}/{UserName}/{Password}/{Email}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdateUser(string UserId, string UserName, string Password, string Email);
        
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SavePost/{UserId}/{FuelPostId}/{FuelPostDate}/{FuelType}/{FuelTitle}/{FuelCaption}/{FuelRating}/{FuelData}/{FuelThumbData}/{FuelLatitude}/{FuelLongitude}/{UserLatitude}/{UserLongitude}/{AppVersion}/{OSVersion}/{DeviceId}/{IsPublic}/{IsFileUploaded}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SavePost(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UpdatePost/{UserId}/{FuelPostId}/{FuelPostDate}/{FuelType}/{FuelTitle}/{FuelCaption}/{FuelRating}/{FuelData}/{FuelThumbData}/{FuelLatitude}/{FuelLongitude}/{UserLatitude}/{UserLongitude}/{AppVersion}/{OSVersion}/{DeviceId}/{IsPublic}/{IsFileUploaded}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdatePost(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ChangePassword/{UserName}/{Email}/{Password}/{GUID}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ChangePassword(string UserName, string Email, string Password, string GUID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ChangePasswordByGUID/{Password}/{GUID}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ChangePasswordByGUID(string Password, string GUID);

        ///POST METHODS
        ///
        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveUserByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SaveUserByPOST(string UserId, string UserName, string Password, string Email);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateUserByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdateUserByPOST(string UserId, string UserName, string Password, string Email);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SavePostByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SavePostByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdatePostByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdatePostByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded);


        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ChangePasswordByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ChangePasswordByPOST(string UserName, string Email, string Password, string GUID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ChangePasswordByGUIDByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ChangePasswordByGUIDByPOST(string Password, string GUID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ChangePasswordByGUIDByPOSTNew", BodyStyle = WebMessageBodyStyle.WrappedRequest, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string ChangePasswordByGUIDByPOSTNew(string Password, string GUID);

    }

    

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    [DataContract]
    public class StatusMode
    {
        bool status = true;
        string id = "";
        string result = "";
        HappyFuelAppServices.FuelUser _user;
        List<HappyFuelAppServices.FuelPost> _post;

        [DataMember]
        public bool Status
        {
            get { return status; }
            set { status = value; }
        }

        [DataMember]
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Result
        {
            get { return result; }
            set { result = value; }
        }

        [DataMember]
        public HappyFuelAppServices.FuelUser CurrentUser
        {
            get { return _user; }
            set { _user=value;  }
        }

        [DataMember]
        public List<HappyFuelAppServices.FuelPost> Post
        {
            get { return _post; }
            set { _post = value; }
        }

    }




}
