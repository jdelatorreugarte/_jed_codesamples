//generate the feed hyperlink and pop it into a new window
function generateFeed() {
  var zone_id = $('#zone_id')[0].value;
  var content_type_id = $('#content_type_id')[0].value;
  var keywords =  $('#keywords')[0].value.replace(/, /g,"+").replace(/,/g,"+").replace(/ /g,"+")
  var format =  $('#format')[0].value;
  var count =  $('#count')[0].value;
  var url = "/feeds/" + format
  var append = false
  if(zone_id != "" || content_type_id != "" || keywords != "" || count != "20") {
  	url+= "?"
  }
  if(zone_id != "") {
  	url+= "zone_id=" + zone_id
  	append = true
  }
  if(content_type_id != "") {
  	if(append) {
  	  url+= "&"	
  	}
  	url+= "content_type_id=" + content_type_id
  	append = true
  }
  if(keywords != "") {
  	if(append) {
  	  url+= "&"
	  append = true	
  	}
  	url+= "keywords=" + keywords;
  }
  if(count != "20") {
  	if(append) {
  	  url+= "&"
  	}
  	url+= "count=" + count;
  }
  window.open(url, 'feed', 'height=460,width=660,status=1,toolbar=1,location=1,resizable=1,scrollbars=1,menubar=1');
}

//Sync zone_id multi select with hidden form field
function syncZones(obj) {
  var zone_list = "";
  for (var i=0; i < obj.options.length; i++) {
    if(obj.options[i].selected == true){
		zone_list+= obj.options[i].value + ',';
    }
  }
  //get rid of last comma
  var zone_length = zone_list.length - 1;
  zone_list = zone_list.substring(0,zone_length);
  document.getElementById('zone_id').value = zone_list;	
}

//Sync content_type multi select with hidden form field
function syncContentTypes(obj) {
  var content_type_list = "";
  for (var i=0; i < obj.options.length; i++) {
    if(obj.options[i].selected == true){
		content_type_list+= obj.options[i].value + ',';
    }
  }
  //get rid of last comma
  var content_type_length = content_type_list.length - 1;
  content_type_list = content_type_list.substring(0,content_type_length);
  document.getElementById('content_type_id').value = content_type_list;	
}