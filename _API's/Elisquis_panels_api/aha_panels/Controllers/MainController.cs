﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace aha_panels.Controllers
{
    public class MainController : Controller
    {
        // GET: /Main/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Main/Panels
        public ActionResult Panels()
        {
            return View();
        }

        // GET: /Main/Panels-1
        [ActionName("Panels-1")]
        public ActionResult Link1()
        {
            return View("Link1");
        }

        // GET: /Main/Panels-1
        [ActionName("Panels-2")]
        public ActionResult Link2()
        {
            return View("Link2");
        }

        // GET: /Main/Panels-1
        [ActionName("Panels-3")]
        public ActionResult Link3()
        {
            return View("Link3");
        }

        // GET: /Main/Panels-1
        [ActionName("Panels-4")]
        public ActionResult Link4()
        {
            return View("Link4");
        }

        public ActionResult Pie()
        {
            return View();
        }
   
    }
}
