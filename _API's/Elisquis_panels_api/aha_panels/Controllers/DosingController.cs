﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace aha_panels.Controllers
{
    public class DosingController : Controller
    {
        //
        // GET: /Dosing/

        public ActionResult Index()
        {
            return View();
        }

        [ActionName("Dosing-2")]
        public ActionResult Dosing2()
        {
            return View("Dosing2");
        }

        [ActionName("Dosing-3")]
        public ActionResult Dosing3()
        {
            return View("Dosing3");
        }

    }
}
