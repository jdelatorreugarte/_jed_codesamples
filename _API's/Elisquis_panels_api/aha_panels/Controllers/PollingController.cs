﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace aha_panels.Controllers
{
    public class PollingController : Controller
    {
        // local declaration
        Models.responseclass _db = new Models.responseclass();

        //
        // GET: /Polling/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Clear()
        {
            // clear data
            _db.removeResponses();

            return View();
        }

    }
}
