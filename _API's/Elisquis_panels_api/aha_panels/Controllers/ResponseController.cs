﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using aha_panels.Models.ahapanelTableAdapters;
using System.Data;

namespace aha_panels.Controllers
{
    public class ResponseController : ApiController
    {
        // private declaration
        private Models.responseclass _db = new Models.responseclass();

        [HttpGet]
        public string getsample()
        {
            return "1";
        }

        // add response and get sumation
        [HttpGet]
        public string addresponse(int _qtype, int _a, int _b, int _c, int _d)
        {
            Models.ahapanel.sumDataTable dt = _db.addResponse(_qtype, _a, _b, _c, _d);

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);

            //return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }

        // get sum
        [HttpGet]
        public object getsum()
        {
            Models.ahapanel.sumDataTable dt = _db.getSum();

            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        // sample response
        [HttpGet]
        public string getresponse()
        {
            Models.ahapanel.responsesDataTable dt = _db.getResponses();
            
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();

            Dictionary<string, object> row;
            foreach (DataRow dr in dt.Rows) {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns) {
                    row.Add(col.ColumnName, dr[col]);
                }
                rows.Add(row);
            }

            return serializer.Serialize(rows);

            //List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
            //Dictionary<string, object> row = null;

            //foreach (DataRow dr in dt.Rows)
            //{
            //    row = new Dictionary<string, object>();
            //    foreach (DataColumn col in dt.Columns)
            //    {
            //        row.Add(col.ColumnName.Trim(), dr[col]);
            //    }
            //    rows.Add(row);
            //}
            //return serializer.Serialize(rows);

            //return Newtonsoft.Json.JsonConvert.SerializeObject(dt, Newtonsoft.Json.Formatting.Indented);
        }
    }
}
