﻿var last;

$(document).ready(function () {
    var t = timeout_redirect();
    var x = redirect();

    // side bar
    $('#sidebar').click(function () {
        $('#tap_here').hide();

        $('#sidebar_bg').fadeIn();

        if ($(this).css('marginLeft') == '190px') {
            $('#sidebar').animate({
                marginLeft: "0px"
            }, 500);

            $('#sidebar_bg').fadeOut();
            $('#tap_here').show('slow');
        } else {
            $('#sidebar').animate({
                marginLeft: "190px"
            }, 500);
        }

        
    });

    $('.sidebar_link').click(function () {
        $('#popup_sidebar .popup_sidebar_content').attr('src', '/Content/images/main/content_' + $(this).attr('data') + '.png');

        $('#popup_sidebar').show();
    });

    $('.popup_sidebar_link').click(function () {
        $('#popup_sidebar .popup_sidebar_content').attr('src', '/Content/images/main/content_' + $(this).attr('data') + '.png');
    });

    $('#popup_sidebar .close').click(function () {
        $('#popup_sidebar').hide();
        $('#tap_here').show();

        $('#sidebar_bg').fadeOut();
        $('#sidebar').animate({
            marginLeft:"0px"
        }, 500);
    });

    // page 2
    $('#content_2 .landing_link').click(function () {
        // reset
        $('#isi_1').hide();
        $('#isi_2').hide();
        $('#isi_3').hide();

        // change header
        $('header img').attr('src', '/Content/images/main/header_2_1.png');

        $('#content_2 #landing').hide();

        $('#content_2 .subcontent').attr('src', '/Content/images/main/content_' + $(this).attr('data') + '.png');

        if ($(this).attr('data') == '2_2') {
            $('#isi_1').show();
        }

        if ($(this).attr('data') == '2_3') {
            $('#isi_2').show();
        }

        if ($(this).attr('data') == '2_4') {
            $('#isi_3').show();
        }

        $('#content_2 #sub').show();

        last = $(this).attr('data');
    });

    $('#content_2 .sub_link').click(function () {
        // reset
        $('#isi_1').hide();
        $('#isi_2').hide();
        $('#isi_3').hide();

        $('#content_2 .subcontent').attr('src', '/Content/images/main/content_' + $(this).attr('data') + '.png');

        if ($(this).attr('data') == '2_2') {
            $('#isi_1').show();
        }

        if ($(this).attr('data') == '2_3') {
            $('#isi_2').show();
        }

        if ($(this).attr('data') == '2_4') {
            $('#isi_3').show();
        }

        last = $(this).attr('data');
    });

    // ref
    $('#content_2 .ref_link').click(function () {
        if (last != '2_4')
        {
            $('#popup_ref .popup_ref_content').attr('src', '/Content/images/main/content_' + last + '_ref.png');

            $('#popup_ref').show();
        }
    });

    $('#popup_ref .close').click(function () {
        $('#popup_ref').hide();
    });

    // page 1 chart toggle
    $('#1_2_toggle').click(function () {
        if ($('#content_1_2').attr('src') == '/Content/images/main/content_1_2.png') {
            $('#content_1_2').attr('src', '/Content/images/main/content_1_3.png');
        } else {
            $('#content_1_2').attr('src', '/Content/images/main/content_1_2.png');
        }
    });


    // page 3
    $('#content_3 .landing_link').click(function () {
        $('#content_3 #landing').hide();

        $('#content_3 .subcontent').attr('src', '/Content/images/main/content_' + $(this).attr('data') + '.png');
        $('#content_3 #sub').show();
    });

    // 
    $('#content_3 .sub_link').click(function () {
        $('#content_3 .subcontent').attr('src', '/Content/images/main/content_' + $(this).attr('data') + '.png' );
    });

    // subcontent
    $('.subnav_4').click(function () {
        $('#isi_1').hide();
        $('#isi_2').hide();
        $('#isi_3').show();
        $('header img').attr('src', '/Content/images/main/header_4.png'); // reset header
        $('.subcontent').attr('src', '/Content/images/main/content_' + $(this).attr('data') + '.png');


        if ($(this).attr('data') == '4_1') {
            $('#isi_3').hide();

            $('#isi_1').show();
        }

        if ($(this).attr('data') == '4_2') {
            $('#isi_3').hide();

            $('#isi_2').show();
        }

        if ($(this).attr('data') == '4_3') {
            $('header img').attr('src', '/Content/images/main/header_4_2.png');
        }

        last = '/Content/images/main/content_' + $(this).attr('data') + '.png';
    });

    // subcontent pop
    $('.subnav_4_pop').click(function () {
        $('#popup_top').show();
        $('.subcontent_popup_close').show();

        $('.subcontent').attr('src', '/Content/images/main/content_' + $(this).attr('data') + '.png');
    });

    // subcontent popup close
    $('.subcontent_popup_close').click(function () {
        $('#popup_top').hide();
        $('.subcontent_popup_close').hide();

        // show last
        $('.subcontent').attr('src', last);
        //alert(last);
    });

    // open popup
    $('.popup_open').click(function () {
        $('#popup').show();
    });

    // close popup
    $('.popup_close').click(function(){
        $('#popup').hide();
    });

    // content_swap
    $('.sublink_swap').click(function () {
        // reset
        $('#content .subcontent').hide();

        // show
        $('#content #content_' + $(this).attr('data')).show();
    });

    // sublink click
    $('.sublink').click(function () {
        // reset
        $('.sublink').each(function () {
            $(this).attr('src', '/Content/images/main/' + $(this).attr('data') + '.png');
        });

        //alert('/Content/images/main/' + $(this).attr('data') + '_on.png');
        $(this).attr('src', '/Content/images/main/' + $(this).attr('data') + '_on.png');
    });

    // close polling
    $('.close_polling').click(function () {
        $('.polling').hide();
    });

    // process result
    function processresult(data) {
        var a = JSON.parse(data)[0].a;
        var b = JSON.parse(data)[0].b;
        var c = JSON.parse(data)[0].c;
        var d = JSON.parse(data)[0].d;

        var total = a + b + c + d;

        $('.result_a').html(((a / total) * 100).toFixed(0) + '%');
        $('.result_b').html(((b / total) * 100).toFixed(0) + '%');
        $('.result_c').html(((c / total) * 100).toFixed(0) + '%');
        $('.result_d').html(((d / total) * 100).toFixed(0) + '%');


        // draw graph
        $('#donutchart').show();
        drawChart(a, b, c, d);
        
    }

    // responses
    $('.response_a').click(function () {
        // add value
        $.ajax({
            url: "/api/response/addresponse",
            data: { _qtype: $('.polling_question').attr('data'), _a: "1", _b: "0", _c: "0", _d: "0" },
            format:JSON
        }).done(function (msg) {
            processresult(msg);
        });

        // change bg
        $('#polling_bg').attr('src', '/Content/images/main/polling_background_2.png');

        // change font-size
        $('.response_text').css({
            fontSize: '25px'
        });

        // show result
        $('.result_text').show();
        $('.message').show();
        $('.reference').show();
    });

    $('.response_b').click(function () {
        // add value
        $.ajax({
            url: "/api/response/addresponse",
            data: { _qtype: $('.polling_question').attr('data'), _a: "0", _b: "1", _c: "0", _d: "0" },
            format: JSON
        }).done(function (msg) {
            processresult(msg);
        });

        // change bg
        $('#polling_bg').attr('src', '/Content/images/main/polling_background_2.png');

        // change font-size
        $('.response_text').css({
            fontSize: '25px'
        });
        
        // show result
        $('.result_text').show();
        $('.message').show();
        $('.reference').show();
    });

    $('.response_c').click(function () {
        // add value
        $.ajax({
            url: "/api/response/addresponse",
            data: { _qtype: $('.polling_question').attr('data'), _a: "0", _b: "0", _c: "1", _d: "0" },
            format: JSON
        }).done(function (msg) {
            processresult(msg);
        });

        // change bg
        $('#polling_bg').attr('src', '/Content/images/main/polling_background_2.png');

        // change font-size
        $('.response_text').css({
            fontSize: '25px'
        });

        // show result
        $('.result_text').show();
        $('.message').show();
        $('.reference').show();
    });

    $('.response_d').click(function () {
        // add value
        $.ajax({
            url: "/api/response/addresponse",
            data: { _qtype: $('.polling_question').attr('data'), _a: "0", _b: "0", _c: "0", _d: "1" },
            format: JSON
        }).done(function (msg) {
            processresult(msg);
        });

        // change bg
        $('#polling_bg').attr('src', '/Content/images/main/polling_background_2.png');

        // change font-size
        $('.response_text').css({
            fontSize: '25px'
        });

        // show result
        $('.result_text').show();
        $('.message').show();
        $('.reference').show();
    });

    // click on screensaver to start
    $('#screensaver').click(function () {
        // redirect
        window.location.href = '/main/panels';
    });

    // enable movement detection
    $('.timer').mousemove(function () {
        clearTimeout(t);
        t = timeout_redirect();

        clearTimeout(x);
        x = redirect();
    });

    $('#cancel_timeout').click(function () {
        clearTimeout(t);
        t = timeout_redirect();

        clearTimeout(x);
        x = redirect();

        $('.timeout').hide();
    });
});

// redirect if no activity
function timeout_redirect() {
    var val = setTimeout(function () {
        //document.location.href = "/main";
        // show timeout
        $('.timeout').show();
    }, 60000);

    return val;
}

function redirect() {
    var vals = setTimeout(function () {
        location.href = '/main';
    }, 65000);

    return vals;
}