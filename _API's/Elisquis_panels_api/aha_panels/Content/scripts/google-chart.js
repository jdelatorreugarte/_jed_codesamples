﻿google.load("visualization", "1", { packages: ["corechart"] });
//google.setOnLoadCallback(drawChart);

function drawChart(_a, _b, _c, _d) {
    var data = google.visualization.arrayToDataTable([
      ['Task', 'Hours per Day'],
      ['A', _a],
      ['B', _b],
      ['C', _c],
      ['D', _d]
    ]);

    var options = {
        //title: 'My Daily Activities',
        pieHole: 0.65,
        legend: 'none',
        pieSliceText: 'none',
        backgroundColor: 'none',
        pieSliceBorderColor: 'none',
        slices: {
            0: { color: '#f26522' },
            1: { color: '#f20553' },
            2: { color: '#f16eaa' },
            3: { color: '#ed008c' }
        }
    };

    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);
}