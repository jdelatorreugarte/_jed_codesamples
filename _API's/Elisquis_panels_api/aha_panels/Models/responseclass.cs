﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace aha_panels.Models
{
    public class responseclass
    {
        // declaration
        private ahapanelTableAdapters.responsesTableAdapter _responseAdapter = null;
        private ahapanelTableAdapters.sumTableAdapter _sumAdapter = null;

        protected ahapanelTableAdapters.responsesTableAdapter responseAdapter
        {
            get
            {
                if (_responseAdapter == null) _responseAdapter = new ahapanelTableAdapters.responsesTableAdapter();
                return _responseAdapter;
            }
        }

        protected ahapanelTableAdapters.sumTableAdapter sumAdapter
        {
            get
            {
                if (_sumAdapter == null) _sumAdapter = new ahapanelTableAdapters.sumTableAdapter();
                return _sumAdapter;
            }
        }

        // add response and return response
        public ahapanel.sumDataTable addResponse(int _qtype, int _a, int _b, int _c, int _d)
        {
            responseAdapter.addresponse(_qtype, _a, _b, _c, _d);

            return sumAdapter.getresponsesum(_qtype);
        }

        // get sum
        public ahapanel.sumDataTable getSum()
        {
            return sumAdapter.getresponsesum(1);
        }

        // get all
        public ahapanel.responsesDataTable getResponses()
        {
            return responseAdapter.getresponses();
        }

        // remove all
        public void removeResponses()
        {
            responseAdapter.deleteresponses();
        }
    }
}