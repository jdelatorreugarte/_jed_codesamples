﻿$(function(){
    $('.tab-content').hide();
    $('#subheader a').bind('click', function(e){
        $('#subheader a.active').removeClass('active');
        $('.tab-content:visible').hide();
        $(this.hash).show();
        $(this).addClass('active');
        e.preventDefault();
    }).filter(':first').click();
});