﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Google.GData.Contacts;
using Google.GData.Client;
using Google.GData.Extensions;
using Google.Contacts;

using System.Data;


namespace HappyFuelAppServices
{
    public partial class GoogleContacts : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
           
           
        }

        public static DataSet GetGmailContacts(string App_Name, string Uname, string UPassword)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataColumn C2 = new DataColumn();
            C2.DataType = Type.GetType("System.String");
            C2.ColumnName = "EmailID";
            dt.Columns.Add(C2);
            RequestSettings rs = new RequestSettings(App_Name, Uname, UPassword);
            rs.AutoPaging = true;
            ContactsRequest cr = new ContactsRequest(rs);
            Feed<Contact> f = cr.GetContacts();
            foreach (Contact t in f.Entries)
            {

                foreach (Google.GData.Extensions.EMail email in t.Emails)
                {
                    DataRow dr1 = dt.NewRow();
                    dr1["EmailID"] = email.Address.ToString();
                    dt.Rows.Add(dr1);
                }
            }
            ds.Tables.Add(dt);
            return ds;
        }

        protected void btnSignIn_Click(object sender, EventArgs e)
        {
            BindContacts();
        }


        private void BindContacts()
        {
            string App_Name = "HappyFuelWebApp";
            string username = this.txtUserName.Text;
            string password = this.txtPassword.Text;
            DataSet ds = GetGmailContacts(App_Name, username, password);
            GridView1.DataSource = ds;
            GridView1.DataBind();  
        }

        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.PageIndex = e.NewPageIndex;  
        }
    }
}