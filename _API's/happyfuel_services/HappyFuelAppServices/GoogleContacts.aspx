﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GoogleContacts.aspx.cs" Inherits="HappyFuelAppServices.GoogleContacts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        User Name:
        <asp:TextBox ID="txtUserName" runat="server" Width="356px"></asp:TextBox>
        <br />
        Password:
        <asp:TextBox ID="txtPassword" runat="server" TextMode="Password" Width="359px"></asp:TextBox>
        <br />
        <asp:Button ID="btnSignIn" runat="server" OnClick="btnSignIn_Click" Text="Sign In" Width="302px" />
        <br />
        <br />
        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging" >
            <Columns>
                <asp:BoundField DataField="EmailID" HeaderText="Email" />

            </Columns>
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
