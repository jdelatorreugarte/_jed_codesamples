//$.ajaxSetup({
//    cache:false
//    });


// GET QUERYSTRING DATA FUNCTION

function getQueryVariable(variable) {
    var query = window.location.search.substring(1),
		vars = query.split("&");

    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }

    return (false);
}

(function ($) {

    var url = "http://www.happyfuel.me/HFWebServices.svc";
    // url="http://localhost:15555/HFWebServices.svc";

    var guid = getQueryVariable('hgid'); //Pulls GUID from querystring

    if (guid != "") {

        var userByGUID = url + "/GetUserByGUID/" + guid + "?callback=?;"; //didn't work. Server might not be configured for cross-domain JSONP. Had to use test data. Won't be an issue is page is on same domain as webservice.


        $.getJSON(userByGUID, function (data) {


            var userId = data.ID;
            if (userId > 0) {
                $('#password-fields').show();
            }
            else {
                $('#error-fields').show();
            }


        });
    }
    else {
        $('#error-fields').show();
    }


})(jQuery);

(function ($) {
    $('#form-reset').submit(function (e) {
        e.preventDefault();
        var url = "http://www.happyfuel.me/HFWebServices.svc";
        //url = "http://localhost:15555/HFWebServices.svc";

        var guid = getQueryVariable('hgid'); //Pulls GUID from querystring

        var pass = $('#password1').val();

        url = url + "/ChangePasswordByGUID/" + pass + "/" + guid + "?callback=?;";


        $.getJSON(url, function (data) {
            var userId = data.ID;
            if (userId > 0) {

                //    window.location.replace("http://www.happyfuel.me");

                if ((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPad/i))) {
                    if (document.cookie.indexOf("iphone_redirect=false") == -1) window.location = "happyfuel://";
                }
                else {
                    window.location = "http://www.happyfuel.me";
                }

            }
            else {
                $('#post-error-message').show();
                $('#password-fields').hide();
                $('#error-fields').hide();
            }

        }).error(function (xhr, textStatus, errorThrown) { alert('Unable to change Password. Please remove invalid characters or contact administrator -> ' + xhr.statusText); });

    });



})(jQuery);


$(document).ready(function () {

    $("#form-reset").validate({

        rules: {
            password1: { required: true, minlength: 8 },
            passwordconfirm1: { required: true, minlength: 8, equalTo: "#password1" }
        },
        messages:
                {
                    password1: { required: "*", minlength: " min 8 characters" },
                    passwordconfirm1: { required: "*", minlength: " min 8 characters", equalTo: "Confirm Password is not same as Password" }
                },
        errorElement: "div", errorClass: "error-class"
    });

});