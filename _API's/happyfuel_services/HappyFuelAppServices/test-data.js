{
    "GetPostByIDResult": {
        "CurrentUser": null,
        "ID": "152",
        "Post": [
            {
                "AppVersion": "1.1",
                "DeviceID": "ABCD-EFGH-IJKLM-NOPQ-RSTU",
                "FuelCaption": "This keeps me happy because even though we are at schools in different states, when we're together there is always as much love as this moment.",
                "FuelData": "test-image.jpg",
                "FuelLatitude": "40.74832820",
                "FuelLongitude": "-73.98556010",
                "FuelPostDate": "6/17/2013 12:51:01 PM",
                "FuelPostId": 152,
                "FuelRating": 5,
                "FuelThumbData": "test-image.jpg",
                "FuelTitle": "The Best Kind of Prom Dates",
                "FuelType": "L",
                "InsertDateTime": "6/17/2013 12:51:01 PM",
                "IsActive": 1,
                "IsPublic": 1,
                "OSVersion": "6.1.4",
                "PostGUID": "deeb636a-da9d-4293-bc12-cc0741d7dedc",
                "ShareURL": "http://www.happyfuelapp.com/post/deeb636a-da9d-4293-bc12-cc0741d7dedc",
                "TinyURL": "http://www.happyfuelapp.com/post/deeb636a-da9d-4293-bc12-cc0741d7dedc",
                "UpdateDateTime": "6/17/2013 12:51:01 PM",
                "UserId": 14,
                "UserLatitude": "40.74832820",
                "UserLongitude": "-73.98556010"
            }
        ],
        "Result": "SUCCESS:",
        "Status": true
    }
}