/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/14/13
 * Time: 12:11 AM
 */

define([
	'angular'
	, 'angular-resource'] , function (angular) {
    return angular.module('mainApp' , ['ngResource','angularOauth']);
});