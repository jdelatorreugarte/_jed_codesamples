'use strict';

/**
 * A module to include instead of `angularOauth` for a service preconfigured
 * for Google OAuth authentication.
 *
 * Guide: https://developers.google.com/accounts/docs/OAuth2UserAgent
 */
 define([
	'angular'
	, 'angular-resource'] , function (angular) {
     // angular.module('mainApp' , ['ngResource','angularOauth','googleOauth']);
return angular.module('googleOauth', ['angularOauth']).

  constant('GoogleTokenVerifier', function(config, accessToken) {
    var $injector = angular.injector(['ng']);
    return $injector.invoke(['$http', '$rootScope', '$q', function($http, $rootScope, $q) {
      var deferred = $q.defer();
      var verificationEndpoint = 'https://www.googleapis.com/oauth2/v1/tokeninfo';
      // var verificationEndpoint = 'https://www.google.com/m8/feeds/contacts/joe.smith@gmail.com/full';

      $rootScope.$apply(function() {
          $http({method: 'GET', url: verificationEndpoint, params: {access_token: accessToken}}).
          success(function(data) {
            if (data.audience == config.clientId) {
			console.log(data);
              deferred.resolve(data);
            } else {
              deferred.reject({name: 'invalid_audience'});
            }
          }).
          error(function(data, status, headers, config) {
            deferred.reject({
              name: 'error_response',
              data: data,
              status: status,
              headers: headers,
              config: config
            });
          }); 
      });
		$http.get('https://www.google.com/m8/feeds/contacts/default/full/?access_token=' + accessToken + "&alt=json&max-results=200").success( function(result){
					if (result.audience == config.clientId) {
						console.log(result);
						deferred.resolve(result);
					} else {
						deferred.reject({name: 'invalid_audience'});
					}
					console.log(JSON.stringify(result));
			}).error(function(data, status, headers, config) {
				deferred.reject({
              name: 'error_response',
              data: data,
              status: status,
              headers: headers,
              config: config
            });
          });
	 
      return deferred.promise;
    }]);
  }).

  config(function(TokenProvider, GoogleTokenVerifier) {
    TokenProvider.extendConfig({
        clientId: '193839560964-i2506hgcqrdu1j6epjpmkin8qlos710r.apps.googleusercontent.com',
		redirectUri: 'http://localhost:15555/oauth2callback.html',
		authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
		scopes: ["https://www.google.com/m8/feeds"],
		verifyFunc: GoogleTokenVerifier
    });
  });
});
// https://www.googleapis.com/auth/userinfo.email 
