/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/25/13
 * Time: 8:41 AM
 */

define([
	'ng/modules/mainApp'
	, 'ng/modules/angularOauth'
	, 'amd/app'
	, 'ng/controllers/dashboardController'
	, 'ng/controllers/detailController'
	, 'ng/controllers/wallController'
	, 'ng/controllers/profileController'
	, 'ng/services/mainService'
	, 'ng/providers/cookieProvider'
	]
	, function (mainApp) {
	mainApp.controller('mainController' , ['$scope', 'hfService', '$rootScope', '$route', '$location', '$cookieStore' ,'Token' , function ($scope, hfsvc, $rootScope, $route, $location, $cookieStore,Token) {
	
		
		var _svc = new hfsvc();
		var Event = YAHOO.util.Event;
		$scope.svc = _svc;
		$scope.wallPostsResponse = null;
		$rootScope.selectedPostType = null;
		$rootScope.selectedFuelType = '';
		$rootScope.selectedUser = '';
		$rootScope.selectedHashTag = '';
		$rootScope.selectedPost = null;
		$scope.selectedRatingValue = 0;
		$rootScope.currentUser = null; //{UserId:1};
        $rootScope.currentUserFriends = [];
		$rootScope.currentUserHasRatedSelectedPost = false;
		$rootScope.currentPath = '';
		$rootScope.isLoggedIn = 'false';
		$scope.searchText = '';
		$rootScope.commentsPaneState = 'expanded';
		$scope.yahootoken = _svc.yahoo.getyahootoken();
		
		$scope.initDashboardData = function () {
			return {
				topFans: null,
				favoriteFuelers: null,
				userFuelers: null,
				userFollowedFuelers: null,
				myPostsByType: null,
				postsIRated: null,
				myTopPosts: null,
				myFavoritePosts: null
			};
		};

		$rootScope.selectPost = function (value) {
			console.log(value);
			$rootScope.selectedPost = value;
		};

		$rootScope.selectPostType = function (value) {
			console.log(value);
			$rootScope.selectedPostType = value;
		};

		$rootScope.selectFuelType = function (value) {
			console.log(value);
			$rootScope.selectedFuelType = value;
		};

		$scope.getCurrentUsername = function () {
			return ($rootScope.currentUser != null)?$rootScope.currentUser.UserName + ', ':'';
		};
		
		$scope.getBkgdImg = function (postItem, fuelType, resourceUrl) {
			switch ($rootScope.selectedFuelType) {
                case 'A':
                    return '/css/images/bkgd-sound.png';
                    //return 'http://assets.happyfuelapp.com/' + postItem.FuelData.replace('amr','mp4');
                    break;
			    case 'P':
			        return 'http://assets.happyfuelapp.com/' + postItem.FuelThumbData;
			        break;
			    case 'V':
			        return 'http://assets.happyfuelapp.com/' + postItem.FuelThumbData;
			        break;
                case 'M':
                    //return 'https://s3.amazonaws.com/HappyFuelApp/' + postItem.FuelThumbData;
                    return 'http://assets.happyfuelapp.com/' + postItem.FuelThumbData;
                    break;
			    case 'T':
			        //return '';
			        //return 'http://assets.happyfuelapp.com/' + postItem.FuelData;
					break;
				case 'L':
					return 'http://maps.googleapis.com/maps/api/staticmap?center=' + postItem.FuelLatitude + ',' + postItem.FuelLongitude + '&zoom=14&size=400x400&sensor=false';
					break;
			}
		    //return 'https://s3.amazonaws.com/HappyFuelApp/' + resourceUrl;

			if (resourceUrl == 'recording') { return '/css/images/bkgd-sound.png'; }
			else { return 'http://assets.happyfuelapp.com/' + resourceUrl; }
		};

		$scope.getImg = function (postItem, fuelType, resourceUrl) {
		    switch ($rootScope.selectedFuelType) {
		        case 'A':
		            //return '/css/images/bkgd-sound.png';
		            return 'http://assets.happyfuelapp.com/' + postItem.FuelData.replace('amr', 'mp4');
		            break;
		        case 'P':
		            return 'http://assets.happyfuelapp.com/' + postItem.FuelThumbData;
		            break;
		        case 'V':
		            return 'http://assets.happyfuelapp.com/' + postItem.FuelData;
		            break;
		        case 'M':
		            //return 'https://s3.amazonaws.com/HappyFuelApp/' + postItem.FuelThumbData;
		            return 'http://assets.happyfuelapp.com/' + postItem.FuelThumbData;
		            break;
		        case 'T':
		            //return '';
		            //return 'http://assets.happyfuelapp.com/' + postItem.FuelData;
		            break;
		        case 'L':
		            return 'http://maps.googleapis.com/maps/api/staticmap?center=' + postItem.FuelLatitude + ',' + postItem.FuelLongitude + '&zoom=14&size=400x400&sensor=false';
		            break;
		    }
		    //return 'https://s3.amazonaws.com/HappyFuelApp/' + resourceUrl;
		    return 'http://assets.happyfuelapp.com/' + resourceUrl;
		};

		$rootScope.getImgPath = function (resourceFilename) {
		    //return (resourceFilename != null && resourceFilename.length>0)?'https://s3.amazonaws.com/HappyFuelApp/' + resourceFilename : '';
		    return (resourceFilename != null && resourceFilename.length > 0) ? 'http://assets.happyfuelapp.com/' + resourceFilename : '';
		};

		$rootScope.getAvatarBkgdImg = function (resourceFilename) {
			var path = $rootScope.getImgPath(resourceFilename);
			if (path != '')
				return 'background-image:url(' + path + ');'
			return '';
		};

		$scope.myPostsByType = null;
		$scope.postsIRated = null;
		$scope.getDashboardInfo = function () {
			if ($rootScope.currentUser != null) {
				var usr = $rootScope.currentUser;
				_svc.dashboard.getTopFansByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.topFans = data.Users.slice(0,8);
					});
				_svc.dashboard.getFavoriteFuelersByUserId(usr.UserId)
					.success(function (data) {
                        $scope.dashboardData.favoriteFuelers = data.Users;
                        $scope.dashboardData.favoriteFuelersUI = data.Users.slice(0,8);
					});
				_svc.dashboard.getUserFuelersByUserId(usr.UserId)
					.success(function (data) {
                        $scope.dashboardData.userFuelers = data.Users;
                        $scope.dashboardData.userFuelersUI = data.Users.slice(0,8);
					});
				_svc.dashboard.getUserFollowedFuelersByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.userFollowedFuelers = data.Users.slice(0,8);
					});
				_svc.dashboard.getPostsByTypeByUserId(usr.UserId)
					.success(function (data) {
						$scope.myPostsByType = data.FuelTypeCounts;
						$scope.setPostRatingCount('myposts');
					});
				_svc.dashboard.getTopPostsByTypeByRatingByUserId(usr.UserId)
					.success(function (data) {
						$scope.postsIRated = data.FuelTypeCounts;
						$scope.setPostRatingCount('postsirated');
					});
				_svc.dashboard.getTopPostsByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.myTopPosts = data.Post.slice(0,8);
					});
				_svc.dashboard.getFavoritePostsByUserId(usr.UserId)
					.success(function (data) {
						$scope.dashboardData.myFavoritePosts = data.Post.slice(0,8);
					});
			}
		};

        $scope.myFriendsPage = 1;
        $scope.navigateMyFriends = function (direction) {
            var count = $scope.dashboardData.favoriteFuelers.length
                , pgSz = 8
                , offset;
            if (direction === 'next') {
                if (($scope.myFuelersPage)*pgSz < count) {
                    offset = ($scope.myFuelersPage)*pgSz;
                    $scope.dashboardData.favoriteFuelersUI = $scope.dashboardData.favoriteFuelers.slice(offset, offset+8);
                    $scope.myFuelersPage++;
                }
            }
            else {
                if (($scope.myFuelersPage - 1)*pgSz >= 0) {
                    offset = ($scope.myFuelersPage - 1)*pgSz;
                    $scope.dashboardData.favoriteFuelersUI = $scope.dashboardData.favoriteFuelers.slice(offset, offset+8);
                    $scope.myFuelersPage--;
                }
            }
        };

        $scope.myFuelersPage = 1;
        $scope.navigateMyFuelers = function (direction) {
            var count = $scope.dashboardData.userFuelers.length
                , pgSz = 8
                , offset;
            if (direction === 'next') {
                if (($scope.myFriendsPage)*pgSz < count) {
                    offset = ($scope.myFriendsPage)*pgSz;
                    $scope.dashboardData.userFuelersUI = $scope.dashboardData.userFuelers.slice(offset, offset+8);
                    $scope.myFriendsPage++;
                }
            }
            else {
                if (($scope.myFriendsPage - 1)*pgSz >= 0) {
                    offset = ($scope.myFriendsPage - 1)*pgSz;
                    $scope.dashboardData.userFuelersUI = $scope.dashboardData.userFuelers.slice(offset, offset+8);
                    $scope.myFriendsPage--;
                }
            }
        };


		$scope.setPostRatingCount = function (dataSet) {
			var fnFindElement = function (item) {
				return item.FuelType == this;
			}
				, fnSetPctValues = function(obj) {
					var highestValue = 0;
					for (var key in obj) {
						if (obj[key] && !isNaN(obj[key].FuelTypeRowCount) && obj[key].FuelTypeRowCount > highestValue) {
							highestValue = obj[key].FuelTypeRowCount;
						}
					}
					for (var key in obj) {
						if (obj[key] && !isNaN(obj[key].FuelTypeRowCount)) {
							obj[key]['PctValue'] = Math.floor((obj[key].FuelTypeRowCount / highestValue)*100);
						}
					}
				};
			if (dataSet == 'myposts') {
				$scope.dashboardData.myPostsByType = {};
				$scope.dashboardData.myPostsByType.P = _.find($scope.myPostsByType, fnFindElement, 'P') || {"FuelType":"P","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.L = _.find($scope.myPostsByType, fnFindElement, 'L') || {"FuelType":"L","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.V = _.find($scope.myPostsByType, fnFindElement, 'V') || {"FuelType":"V","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.M = _.find($scope.myPostsByType, fnFindElement, 'M') || {"FuelType":"M","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.S = _.find($scope.myPostsByType, fnFindElement, 'A') || {"FuelType":"A","FuelTypeRowCount":0};
				$scope.dashboardData.myPostsByType.T = _.find($scope.myPostsByType, fnFindElement, 'T') || {"FuelType":"T","FuelTypeRowCount":0};
				fnSetPctValues($scope.dashboardData.myPostsByType);
			}
			else if (dataSet == 'postsirated') {
				$scope.dashboardData.postsIRated = {};
				$scope.dashboardData.postsIRated.P = _.find($scope.postsIRated, fnFindElement, 'P') || {"FuelType":"P","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.L = _.find($scope.postsIRated, fnFindElement, 'L') || {"FuelType":"L","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.V = _.find($scope.postsIRated, fnFindElement, 'V') || {"FuelType":"V","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.M = _.find($scope.postsIRated, fnFindElement, 'M') || {"FuelType":"M","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.S = _.find($scope.postsIRated, fnFindElement, 'A') || {"FuelType":"A","FuelTypeRowCount":0};
				$scope.dashboardData.postsIRated.T = _.find($scope.postsIRated, fnFindElement, 'T') || {"FuelType":"T","FuelTypeRowCount":0};
				fnSetPctValues($scope.dashboardData.postsIRated);
			}
			console.log($scope.dashboardData);
		}

		$rootScope.$on("$routeChangeSuccess", function(event, current) {
			console.log('location: ', $location);
			$rootScope.currentPath = $location.$$path;
			//$scope.number = $routeParams['number'];
		});

		$scope.logout = function () {
			$rootScope.currentUser = null;
			$cookieStore.remove('currentUser');

            // redirect
			location.href = '/';
		}


		$scope.deleteUser = function () {
		    //$scope.svc.user.updateUser($rootScope.currentUser.UserId, $scope.username, $scope.password, $scope.email)

		    var deleteConfirm = confirm('Are you absolutely sure you want to delete your account?. This is not reversible and all data will be lost.');

		    if (deleteConfirm)
		    {
		        $scope.svc.user.deleteUser($rootScope.currentUser.UserId);

		        $rootScope.currentUser = null;
		        $cookieStore.remove('currentUser');

		        // redirect
		        location.href = '/';
		    }

		}
        

		$scope.setView = function (view) {
			switch(view) {
			    case 'sign-in':
			        $('.unknown').hide();
			        $('section.profile .register, section.profile .manage-account, section.profile .reset-password, section.profile .find-friends').hide();
					$('section.profile, section.profile .login, .lightbox-screen.profile').show();
					break;
				case 'reset-password':
				    $('section.profile .login, section.profile .register, section.profile .manage-account , section.profile .find-friends').hide();
					$('section.profile, section.profile .reset-password, .lightbox-screen.profile').show();
					break;
				case 'sign-up':
				    $('section.profile .login, section.profile .manage-account, section.profile .reset-password , section.profile .find-friends').hide();
					$('section.profile, section.profile .register, .lightbox-screen.profile').show();
					break;
				case 'manage-account':
				    $('section.profile .login, section.profile .register, section.profile .reset-password, section.profile .reset-password , section.profile .find-friends').hide();
					$('section.profile, section.profile .manage-account, .lightbox-screen.profile').show();
					break;
				case 'find-friends':
					// $scope.yahootoken = _svc.yahoo.getyahootoken();
				    $('section.profile .register, section.profile .manage-account, section.profile .reset-password ').hide();
					$('section.profile, section.profile .find-friends, .lightbox-screen.profile').show();
					break;
				default:
					$('section.profile, section.profile .login, section.profile .register, section.profile .manage-account, .lightbox-screen.profile').hide();
					break;
			}
		};

		/* Ratings */
		$scope.resetCurrentRatingValues = function () {
			$scope.selectedRatingValue = $rootScope.selectedPost.FuelRatingCount;
			$scope.ratingComment = '';
		};
		$scope.setCurrentRating = function (value) {
			console.log(value);
			$scope.selectedRatingValue = value;
		};
		$scope.getPostRatings = function (postId) {
			$scope.svc.post.getPostRatings(postId);
		};



		if ($cookieStore.get('currentUser')) {
			$rootScope.currentUser = angular.fromJson($cookieStore.get('currentUser'));
            $rootScope.selectedUser = $rootScope.currentUser.UserId;
            $rootScope.isLoggedIn = 'true';
			// call the yahoo token service
			
		}

		$scope.dashboardData = $scope.initDashboardData();
		if ($rootScope.currentUser) {
			$scope.getDashboardInfo()
		};


		$scope.matchingUsers = [];
		$scope.matchingSearchUsers = [];
		$scope.matchingSearchHashtags = [];

		$scope.checkInputForUsernameAndHashtags = function () {
			if ($scope.searchText === '') {
				$scope.matchingSearchHashtags = [];
				$scope.matchingSearchUsers = [];
			}
			else {
				$scope.svc.search.searchUsers($scope.searchText)
					.success(function (data, status) {
						$scope.matchingSearchUsers = data.Users;
					})
					.error(function (data, status) {

					});
			    $scope.svc.search.searchTags($scope.searchText)
					.success(function (data, status) {
						$scope.matchingSearchHashtags = data.replace(/["#@]/g,'').split(', ');
					})
					.error(function (data, status) {

					});
			}
		};

		$rootScope.searchUser = function (user) {
			$rootScope.selectedUser = user.UserId;
			$scope.searchText = user.UserName;
			$scope.matchingSearchHashtags = [];
			$scope.matchingSearchUsers = [];
		};

		$rootScope.searchTag = function (tag) {
		    var _tag = tag.replace('#', '');
		    console.log(_tag);
		    //$rootScope.selectedHashTag = tag; // original
		    $rootScope.selectedHashTag = _tag; // update tags to globally accept keywork without hashtag
			$scope.searchText = _tag // update the value on the search bar;
			$scope.matchingSearchHashtags = [];
			$scope.matchingSearchUsers = [];
		};

		$scope.getWords = function (text) {
			return text.split(/\s/);
		};

		$scope.getLastWord = function (text) {
			var words = $scope.getWords(text)
				, lastWord = words[words.length-1];
			return lastWord;
		};
		
		
		
		$scope.selected = 0;

    $scope.authenticate = function() {
			
		$('.tav_nav li a').removeClass('tabselected');
		$('.tav_nav #gloginLink').addClass('tabselected');
		
      var extraParams = $scope.askApproval ? {approval_prompt: 'force'} : {};
      Token.getTokenByPopup(extraParams)
        .then(function(params) {
			console.log(params);
			 $("#ajx_loader").css('display','block');
          // Success getting token from popup.
			Token.set(params.access_token);
          // Verify the token before setting it, to avoid the confused deputy problem.
          Token.verifyAsync(params.access_token).
            then(function(data) {
					console.log(data);
					 var usr = $rootScope.currentUser;
					 // data = 'neeraj.garg@classicinformatics.com,asheesh.sharma@classicinformatics.com,vinod.kandwal@classicinformatics.com,aditya.tayal@classicinformatics.com,manu.garg@classicinformatics.com';
					 // data = 'kalyan9@gmail.com';
					 // _svc.googleauth.GetAlertsByUserContacts(usr.UserId,data)
					 // please use post method instead of get method on your server.
					 _svc.googleauth.GetAlertsByUserContactsByPOST(usr.UserId,data)
					.success(function (data) {
						console.log(data);
						var followedArr = new Array();
						var tofollowArr = new Array();
						var inviteArr = new Array();
						$.each(data.GetAlertsByUserContactsByPOSTResult.Alerts, function(key, val){
						$("#ajx_loader").css('display','none');
							if(val.FriendUserId==0)
								inviteArr.push(val);
							else if(val.FriendUserId > 0 && (val.StatusId==0 || val.StatusId==6 || val.StatusId==1))
								tofollowArr.push(val);
							else if(val.FriendUserId > 0 && (val.StatusId==2 ))	
								followedArr.push(val);
						});
						$scope.followedArr = followedArr;
						$scope.tofollowArr = tofollowArr;
						$scope.inviteArr = inviteArr;
						$scope.selected = 1;
					
					});
				// alert('success');
              $rootScope.$apply(function() {
				
                
              });
            }, function() {
             // alert("Failed to verify token.")
            });

        }, function() {
          // Failure getting token from popup.
        });
    };
	
	// $scope.isinvited = 0;
	$scope.inviteUser = function (userEmail) {
			if ($rootScope.currentUser != null) {
				var usr = $rootScope.currentUser;
				_svc.userInfo.inviteFriend(usr.UserId,userEmail)
					.success(function (data) {
					});
			}
		};
		// $scope.isfollowed = 0;
		$scope.followUser = function (friendUserId) {
			if ($rootScope.currentUser != null) {
				var usr = $rootScope.currentUser;
				_svc.user.deleteFriend('0',friendUserId,usr.UserId)
					.success(function (data) {
						_svc.userInfo.followFriend(friendUserId,usr.UserId)
						.success(function (data) {
					});
					});
				
			}
		};	
		// $scope.isunfollowed = 0;
		$scope.unfollowUser = function (friendUserId) {
			
			if ($rootScope.currentUser != null) {
				var usr = $rootScope.currentUser;
				
				_svc.user.deleteFriend('0',friendUserId,usr.UserId)
					.success(function (data) {
						_svc.userInfo.unfollowFriend(friendUserId,usr.UserId)
						.success(function (data) {
					});
					});
				
			}
		};

		/* function to delete the user's comment on his post or his own comment */
		$scope.deleteRating = function(ratingid) {
				_svc.post.deleteRating(ratingid)
					.success(function (data) {
							console.log(data);
						});
		};
		
		/* function to report the post */
		$scope.ReportPost = function(postid) {
		if ($rootScope.currentUser != null) {
				var usr = $rootScope.currentUser;
				_svc.post.ReportPost(usr.UserId,postid)
					.success(function (data) {
							console.log(data);
						});
			}
				
		};
		
		$scope.splitString = function(string, nb) {
			$scope.array = string.split('|');
			return $scope.result = $scope.array[nb];
		};
		
		
		$scope.yahooAuthenticate = function() {
		
		$('.tav_nav li a').removeClass('tabselected');
		$('.tav_nav #yloginLink').addClass('tabselected');
		console.log(Event);
		$scope.handleLoginClick();
		// Event.onDOMReady($scope.handleDOMReady);
	  


		};
		
		
		 $scope._gel = function(el) {
		 
			return document.getElementById(el);
		 
		 };
		 
		 $scope.is_show=0;
		 $scope.handleDOMReady =  function() {
			if($scope._gel("yloginLink")) {
			Event.addListener("yloginLink", "click", $scope.handleLoginClick);
			Event.removeListener();
			}
		};
		
		$scope.handleLoginClick = function (event) {
        // block the url from opening like normal
        // Event.preventDefault(event);

        // open pop-up using the auth_url
        // var auth_url = $scope._gel("yloginLink").href;
		var linkurl  = $("#ylogin").attr('href');
		var auth_url = linkurl;
        var c = PopupManager.open(auth_url,600,435);
		var poll = window.setInterval(function() {
			if (c.closed) { // !== is required for compatibility with Opera
				$("#ajx_loader").css('display','block');
				PopupManager.destroyPopup();
				window.clearInterval(poll);
				$scope.someFunction('fetch');
				clearTimeout(poll);
			}
		}, 200);

		// $scope.pollTimer(c);
		
      };
	  // $scope.pollTimer = 
	  $scope.someFunction = function (act){
			var linkURL = "/yahooinvites/process.php";
				$.post(linkURL, function(resp){
					if(act!='fetch' || resp.indexOf("api.login.yahoo.com") !== -1) {
						$("#ylogin").attr('href', resp);
						$("#ajx_loader").css('display','none');
					}else{
						$.post(linkURL, function(data){
								$("#ylogin").attr('href', data);	
							});
					
					var usr = $rootScope.currentUser;
							// resp = 'neeraj.garg@classicinformatics.com,asheesh.sharma@classicinformatics.com,vinod.kandwal@classicinformatics.com,aditya.tayal@classicinformatics.com,manu.garg@classicinformatics.com';
					 // data = 'kalyan9@gmail.com';
					 // _svc.googleauth.GetAlertsByUserContacts(usr.UserId,resp)
					 // please use post method instead of get method on your server.
					 _svc.googleauth.GetAlertsByUserContactsByPOST(usr.UserId,resp)
					.success(function (data) {
					 $("#ajx_loader").css('display','none');
						console.log(data);
						var followedArr = new Array();
						var tofollowArr = new Array();
						var inviteArr = new Array();
						$.each(data.GetAlertsByUserContactsByPOSTResult.Alerts, function(key, val){
							if(val.FriendUserId==0)
								inviteArr.push(val);
							else if(val.FriendUserId > 0 && (val.StatusId==0 || val.StatusId==6 || val.StatusId==1))
								tofollowArr.push(val);
							else if(val.FriendUserId > 0 && (val.StatusId==2 ))	
								followedArr.push(val);
						});
						$scope.followedArr = followedArr;
						$scope.tofollowArr = tofollowArr;
						$scope.inviteArr = inviteArr;
						$scope.selected = 2;
					// $scope.dashboardData.myTopPosts = data.Post.slice(0,8);
						// alert('success after server');
					});
					}
				})
		};
	}]); 
});