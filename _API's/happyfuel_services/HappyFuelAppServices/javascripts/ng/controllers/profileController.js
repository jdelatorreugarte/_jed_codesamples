 /**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/25/13
 * Time: 8:41 AM
 */

define([
	'ng/modules/mainApp'
	, 'amd/filesystem'
	] , function (mainApp, amdFS) {
	mainApp.controller('profileController' , ['$scope', '$rootScope', '$cookieStore', function ($scope, $rootScope, $cookieStore) {
		$scope.username = '';
		$scope.email = '';
		$scope.password = '';
		$scope.passwordConfirm = '';
		$scope.avatar = '';
		$scope.resetEmail = '';
		$scope.bypassAvatarUpload = true;
		$scope.fsUploadCreate = new amdFS({imgPreviewContainerId:'img-preview-create', fileInputId:'user-files-create'});
		$scope.fsUploadUpdate = new amdFS({imgPreviewContainerId:'img-preview-update', fileInputId:'user-files-update'});
		$scope.serverResponseError = '';

		$scope.validation = {
			username: /[0-9a-zA-Z_\.]{8,12}/
			, password: /[0-9a-zA-Z]{8,12}/
			, getPassword: function () {
				return $scope.password;
			}
		};

		$rootScope.$watch('currentUser', function (newVal, oldVal) {
			if (newVal == null) {
				$scope.username = '';
				$scope.email = '';
				$scope.password = '';
				$scope.passwordConfirm = '';
				$scope.avatar = '';
				$rootScope.isLoggedIn = 'false';
                $rootScope.currentUserFriends = [];
			}
            else {
                $scope.username = newVal.UserName;
                $scope.email = newVal.UserEmail;
                $scope.password = '';
                $scope.passwordConfirm = '';
                $scope.avatar = $rootScope.getAvatarBkgdImg(newVal.AvatarId);
                $rootScope.isLoggedIn = 'true';
                $scope.svc.user.getFriendsByUserId(newVal.UserId)
                    .success(function (data) {
                        $rootScope.currentUserFriends = data.Friends;
                    });
            }
		});

		$scope.isDisabled = function (formIsValid) {
			return formIsValid ? '' : 'disabled';
		};

		$scope.login = function (form) {
			$scope.serverResponseError = '';
			$scope.svc.user.validateUser($scope.username, $scope.password)
				.success(function (data) {
					if (data.ValidateUserResult.Status) {
						$scope.svc.user.getUser(data.ValidateUserResult.ID)
							.success(function (data) {
								$rootScope.currentUser = data.GetUserResult.CurrentUser;
								$rootScope.isLoggedIn = 'true';
								$cookieStore.set('currentUser', angular.toJson($rootScope.currentUser));
								$scope.email = $rootScope.currentUser.UserEmail;
								// update dashboard
								$scope.getDashboardInfo();
								$scope.setView('');

							    // redirect to home
								location.reload();
							});

					}
				})
				.error(function (data,status) {
					console.log(data);
				});
		};

		$scope.resetPassword = function (form) {
			if (form.$valid) {
				$scope.serverResponseError = '';
				$scope.svc.user.resetPassword($scope.resetEmail);
			}
		};

		$scope.saveProfile = function (form) {
			var fnSave = function () {
				$scope.svc.user.updateUser($rootScope.currentUser.UserId, $scope.username, $scope.password, $scope.email)
					.success(function (data) {
						if (data.UpdateUserResult.Status) {
							$scope.svc.user.getUser(data.UpdateUserResult.ID)
								.success(function (data) {
									$rootScope.currentUser = data.GetUserResult.CurrentUser;
									$cookieStore.set('currentUser', angular.toJson($rootScope.currentUser));
									$scope.setView('');
								});
						}
						else
							$scope.serverResponseError = data.UpdateUserResult.Result;
					})
					.error(function (data,status) {
						console.log(data);
					});
			};
			if (form.$valid) {
				$scope.serverResponseError = '';
				if ($scope.fsUploadUpdate.files && $scope.fsUploadUpdate.files.length > 0) {
					$scope.fsUploadUpdate.upload('/HFWebServices.svc/UploadFileByPOST', {FileName: 'wes-test.jpg'}, null, null, function (err, xhr) {
						if (!err || $scope.bypassAvatarUpload) {
							fnSave();
						}
					});
				}
				else
					fnSave();
			}
		};

	$scope.createProfile = function (form) {
		var fnSave = function () {
			$scope.svc.user.saveUser($scope.username, $scope.password, $scope.email)
				.success(function (data) {
					if (data.SaveUserResult.Status) {
						$scope.svc.user.getUser(data.SaveUserResult.ID)
							.success(function (data) {
								$rootScope.currentUser = data.GetUserResult.CurrentUser;
								$cookieStore.set('currentUser', angular.toJson($rootScope.currentUser));
								$scope.setView('');
							});
					}
					else
						$scope.serverResponseError = data.SaveUserResult.Result;
				})
				.error(function (data,status) {
					console.log(data);
				});
		};
		if (form.$valid) {
			$scope.serverResponseError = '';
			if ($scope.fsUploadCreate.files && $scope.fsUploadCreate.files.length > 0) {
				$scope.fsUploadCreate.upload('/HFWebServices.svc/UploadFileByPOST', {FileName: 'wes-test.jpg'}, null, null, function (err, xhr) {
					if (!err || $scope.bypassAvatarUpload) {
						fnSave();
					}
				});
			}
			else
				fnSave();
		}
	};
}]);
});