/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/25/13
 * Time: 8:41 AM
 */

define(['ng/modules/mainApp'] , function (mainApp) {
	mainApp.controller('detailController' , ['$scope', '$rootScope' , function ($scope, $rootScope) {

		$scope.postRatings = [];
		$scope.svc.post.getPostRatings(1);
		$scope.ratingIsOn = false;
		$scope.ratingValue = 0;
		$scope.ratingComment = '';
		$scope.selectedPostIsFollowingUser = false;
		$scope.fullPostDetail = {};
		$scope.postHashTags = [];
		$scope.deepLinkUrl = '';

		$rootScope.$watch('selectedPost', function (newVal, oldVal) {
			if ($rootScope.selectedPost !== null) {
				console.log('selected post:', $rootScope.selectedPost);
				$scope.selectedPost = $rootScope.selectedPost;
				$scope.deepLinkUrl = encodeURIComponent(location.protocol + '//' + location.hostname + '/index.html#/fuel-post/' + $scope.selectedPost.FuelPostId);
				$scope.selectedRatingValue = $scope.selectedPost.FuelRatingCount;
				$scope.postHashTags = [];
				$scope.svc.post.getPost($scope.selectedPost.FuelPostId)
					.success(function(data, status) {
						console.log(data);
						$scope.fullPostDetail = data.GetPostByIDResult.Post[0];
					})
					.error(function(data, status) {
						console.log(data, status);
					});
				$scope.svc.post.getPostHashTags($scope.selectedPost.FuelPostId)
					.success(function (data) {
					    // view data
					    console.log(data);

                        // modify data
					    var _val = data;
					    _val = data.replace(/"/g, ''); // remove quote - globally
					    _val = _val.substr(0, _val.length - 1); // remove last comma - remove extra index
					    _val = _val.split(','); // break item by comma

					    //_val = (data === '""') ? [] : data.split(','); // original

					    $scope.postHashTags = _val;
					});
				$scope.svc.post.getPostRatings($scope.selectedPost.FuelPostId)
					.success(function(data, status) {
						$rootScope.postRatings = data.Ratings || [];
					})
					.error(function(data, status) {
						console.log(data, status);
					});
				$scope.setCurrentRating($rootScope.selectedPost.FuelRatingCount);
			}
		});

        $scope.isFollowingUser = function () {
            if ($rootScope.currentUser !== null && $scope.selectedPost) {
                if ($rootScope.currentUser.UserId === $scope.selectedPost.UserId) {
                    return true;
                }
                else {
                    return (_.find($rootScope.currentUserFriends, {UserId:$scope.selectedPost.UserId}) !== undefined);
                }
            }
            return false;
        };

		$rootScope.$watch('postRatings', function (newVal, oldVal) {
			if ($rootScope.postRatings !== null) {
				console.log('post ratings:', $rootScope.postRatings);
				$scope.postRatings = $rootScope.postRatings;
				if ($rootScope.currentUser !== null) {
					//$rootScope.currentUserHasRatedSelectedPost = _.some($scope.postRatings, {UserId:$rootScope.currentUser.UserId});
				}
			}
		});

		$scope.$watch('matchingUsers', function (newVal, oldVal) {
			console.log(newVal);
		});

		$scope.ratePost = function() {
			$scope.selectedRatingValue++;
			$rootScope.selectedPost.FuelRatingCount = $scope.selectedRatingValue;
			$scope.ratingValue = $scope.svc.post.getActiveRateValue();
			$scope.svc.post.savePostRating(0, $rootScope.currentUser.UserId, $rootScope.selectedPost.FuelPostId, $scope.ratingValue, $scope.ratingComment)
				.success(function(data, status) {
                    $scope.svc.post.getPostRatings($scope.selectedPost.FuelPostId)
                        .success(function(data, status) {
                            $rootScope.postRatings = data.Ratings || [];
                        })
                        .error(function(data, status) {
                            console.log(data, status);
                        });
				})
				.error(function(data, status) {
					console.log(data, status);
				});
			console.log('rating:', $scope.selectedRatingValue);
		};
		$scope.getPostType = function () {
			return ($scope.selectedPost == null)?'':$scope.selectedPost.FuelType;
		};
		$scope.getMediaUrl = function () {
		    //return ($scope.selectedPost == null)?'':'https://s3.amazonaws.com/HappyFuelApp/' + $scope.selectedPost.FuelData;
		    return ($scope.selectedPost == null) ? '' : 'http://assets.happyfuelapp.com/' + $scope.selectedPost.FuelData;
		};
		$scope.followUser = function (friendId) {
			$scope.svc.user.followUser(friendId);
		};

		$scope.followUser = function () {
			$scope.svc.user.saveFriend(0,$rootScope.currentUser.UserId, $scope.selectedPost.UserId,1);
		};

		$scope.selectUser = function (user) {
			var words = $scope.getWords($scope.ratingComment);
			words[words.length-1] = '@' + user.UserName;
			$scope.ratingComment = words.join(' ') + ' ';
			$scope.matchingUsers = [];
		};

		$scope.checkInputForUsername = function () {
		    var lastWord = $scope.getLastWord($scope.ratingComment);

		    // check username (@)
			if (/^@.*/.test(lastWord) && lastWord.length > 2 && !(/\s$/.test($scope.ratingComment))) {
				$scope.svc.search.searchUsers(lastWord.substr(1))
					.success(function (data, status) {
						$scope.matchingUsers = data.Users;
					})
					.error(function (data, status) {

					});
			}
			else {
				$scope.matchingUsers = [];
			}
		};

	}]);
});