/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/14/13
 * Time: 12:05 AM
 */

define(['ng/modules/mainApp' , 'ng/factories/Item'] , function (mainApp) {
    mainApp.controller('listCtrl' , ['$scope' , 'Item' , function ($scope , Item) {
        $scope.items = Item;
	    console.log($scope.items);
    }]);
});