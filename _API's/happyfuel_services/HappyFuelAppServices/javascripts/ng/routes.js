/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/14/13
 * Time: 12:03 AM
 */

define(['ng/modules/mainApp' , 'ng/controllers/mainController'] , function (mainApp) {
    return mainApp.config(['$routeProvider' , function ($routeProvider) {
        $routeProvider
	        .when('/' , {controller: 'mainController'});
	    //.when('/' , {controller: 'mainCtrl' , templateUrl: '/views/root.html'});
    }]);

});