{
    "GetUserResult": {
        "CurrentUser": {
            "AvatarId": "",
            "FirstName": "Jane",
            "InsertDateTime": "6/14/2013 12:22:07 PM",
            "IsActive": 14,
            "IsTemporaryPassword": 0,
            "LastName": "Doe",
            "MiddleName": "",
            "Password": null,
            "UpdateDateTime": "6/21/2013 5:20:26 AM",
            "UserEmail": "JaneDoe@example.com",
            "UserId": 14,
            "UserName": "janedoe"
        },
        "ID": "14",
        "Post": null,
        "Result": "SUCCESS",
        "Status": true
    }
}