﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ViewPostData.aspx.cs" Inherits="HappyFuelAppServices.ViewPostData"  %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ --> 
<!--[if lt IE 7 ]> <html lang="en" class="no-js ie6" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="no-js ie7" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="no-js ie8" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="no-js ie9" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://ogp.me/ns/fb#"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> 
<html lang="en" class="no-js" xmlns:fb="http://www.facebook.com/2008/fbml" xmlns:og="http://opengraphprotocol.org/schema/" xmlns:fb="http://ogp.me/ns/fb#"> <!--<![endif]-->
<head id="header" runat="server" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# HappyFuel: http://ogp.me/ns/fb/HappyFuel#">

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>:) Fuel</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
		<link rel="icon" href="images/favicon.ico" type="image/x-icon">
        <link rel="stylesheet" href="css/normalize.min.css">
        <link rel="stylesheet" href="css/main.css">
        <link href="js/vendor/videojs/video-js3-2.css" rel="stylesheet">
        <link href="js/vendor/audiojs/audiojs.css" rel="stylesheet">	
        <script src="js/vendor/modernizr-2.6.2.min.js" type="text/javascript"></script>
        
    </head>

    <body  >
    <form id="form1" runat="server">
    <header>
    	<div id="header-content">
        	<div id="logo" class="ir">:) Fuel&#8482;</div>
            <div id="intro-copy">
				<p><span class="happy">: )</span> <strong>fuel</strong> (pronounced happy fuel) takes your happiness tank from empty to happy. It gives you a mobile place to collect and share the things that make you happy, whether it&#8217;s a photo, song, spot, video, sound or thought. And while you&#8217;re sharing happiness, <span class="happy">: )</span> <strong>fuel</strong> even lets you gauge your own happiness.</p>
            </div>
        </div>
    </header>
    <div id="content" class="clearfix">
    	<a href="https://itunes.apple.com/us/app/fuel/id645030722?ls=1&mt=8" target="_blank" class="ir button-appstore">Available on the iPhone App Store</a>
        
        <div class="user clearfix">
        	<div class="avatar" style="background: url(images/test-avatar.jpg) no-repeat; display:none;"></div>
            <div>
            	<p class="name">HAPPY FUEL</p>
                <p class="location" style="display:none" ></p>
            </div>
        </div>
             
        <div class="post">
            <div id="map-canvas"></div>
        	<img class="picture" src="" style="image-orientation:90deg;">
            <div class="video-post">
                <video id="video-file" preload="none" class="video-js vjs-default-skin" controls width="552" height="300">
                    <p>Your Browser does not support Video</p>
                </video>
            </div>

            <div class="audio-post">
                <audio id="audio-file" preload="none" autoplay >
                    <p>Your Browser does not support Audio</p>
                </audio>
            </div>

            <div class="caption">
            	<p><span class="happy">: ) </span><span class="caption-title">This post does not exist</span></p>
				<p class="caption-text"></p>
            </div>
        
        </div>
        
        <div class="share-links">
            <a href="" class="facebook ir" target="_blank">Facebook</a>
            <a href="" class="twitter ir" target="_blank">Twitter</a>
            <a href="" class="email ir" target="_blank">Email</a>        
        </div>
        
    </div>
    <footer>
	    <p>&#169; 2013 happyfuel LLC </p>
    </footer>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"  type="text/javascript"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.9.1.min.js"><\/script>')</script>
    	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"  type="text/javascript"></script>
        <script src="http://vjs.zencdn.net/3.2/video.js"></script>
        <script src="js/vendor/audiojs/audio.min.js" type="text/javascript"></script>     
        <script src="js/main.js" type="text/javascript"></script>
  
    </form>
</body>
</html>
