﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyFuelAppServices
{
    public class FuelTypeCount
    {
        private Int32 _FuelTypeRowCount;
        private string _FuelType;


        public Int32 FuelTypeRowCount
        {
            get
            {
                return _FuelTypeRowCount;
            }
            set
            {
                _FuelTypeRowCount = value;
            }
        }

        public string FuelType
        {
            get
            {
                return _FuelType;
            }
            set
            {
                _FuelType = value;
            }
        }

    }
}