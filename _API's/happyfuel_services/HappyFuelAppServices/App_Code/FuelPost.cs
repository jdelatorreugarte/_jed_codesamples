﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyFuelAppServices
{
    public class FuelPost
    {

        private Int64 _FuelPostId;
        private Int64 _UserId;
        private string _PostGUID;
        private string _FuelPostDate;
        private string _FuelType;
        private string _FuelTitle;
        private string _FuelCaption;
        private Int16 _FuelRating;
        private string _FuelData;

        private string _FuelThumbData;
        private string _FuelLatitude;
        private string _FuelLongitude;
        private string _UserLatitude;
        private string _UserLongitude;

        private string _ShareURL;
        private string _TinyURL;
        private string _AppVersion;
        private string _OSVersion;
        private string _DeviceID;

        private Int16 _IsPublic;
        private Int16 _IsActive;
        private Int16 _IsFileUploaded;

        private string _InsertDateTime;
        private string _UpdateDateTime;

        private Int32 _FuelRatingCount;
        private string _AverageFuelRating;
        private string _UserName;
        private string _UserLocation;
        private string _UserAvatar;
        private Int16 _IsFlagged;

        public Int64 FuelPostId
        {
            get
            {
                return _FuelPostId;
            }
            set
            {
                _FuelPostId = value;
            }
        }

        public Int64 UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                _UserId = value;
            }
        }

        public string PostGUID
        {
            get
            {
                return _PostGUID;
            }
            set
            {
                _PostGUID = value;
            }
        }

        public string FuelPostDate
        {
            get
            {
                return _FuelPostDate;
            }
            set
            {
                _FuelPostDate = value;
            }
        }

        public string FuelType
        {
            get
            {
                return _FuelType;
            }
            set
            {
                _FuelType = value;
            }
        }

        public string FuelTitle
        {
            get
            {
                return _FuelTitle;
            }
            set
            {
                _FuelTitle = value;
            }
        }

        public string FuelCaption
        {
            get
            {
                return _FuelCaption;
            }
            set
            {
                _FuelCaption = value;
            }
        }

        public Int16 FuelRating
        {
            get
            {
                return _FuelRating;
            }
            set
            {
                _FuelRating = value;
            }
        }

        public string FuelData
        {
            get
            {
                return _FuelData;
            }
            set
            {
                _FuelData = value;
            }
        }

        public string FuelThumbData
        {
            get
            {
                return _FuelThumbData;
            }
            set
            {
                _FuelThumbData = value;
            }
        }
        
        public string FuelLatitude
        {
            get
            {
                return _FuelLatitude;
            }
            set
            {
                _FuelLatitude = value;
            }
        }

        public string FuelLongitude
        {
            get
            {
                return _FuelLongitude;
            }
            set
            {
                _FuelLongitude = value;
            }
        }

        public string UserLatitude
        {
            get
            {
                return _UserLatitude;
            }
            set
            {
                _UserLatitude = value;
            }
        }

        public string UserLongitude
        {
            get
            {
                return _UserLongitude;
            }
            set
            {
                _UserLongitude = value;
            }
        }

        public string ShareURL
        {
            get
            {
                return _ShareURL;
            }
            set
            {
                _ShareURL = value;
            }
        }

        public string TinyURL
        {
            get
            {
                return _TinyURL;
            }
            set
            {
                _TinyURL = value;
            }
        }

        public string AppVersion
        {
            get
            {
                return _AppVersion;
            }
            set
            {
                _AppVersion = value;
            }
        }

        public string OSVersion
        {
            get
            {
                return _OSVersion;
            }
            set
            {
                _OSVersion = value;
            }
        }

        public string DeviceID
        {
            get
            {
                return _DeviceID;
            }
            set
            {
                _DeviceID = value;
            }
        }

        public Int16 IsPublic
        {
            get
            {
                return _IsPublic;
            }
            set
            {
                _IsPublic = value;
            }
        }

        public Int16 IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                _IsActive = value;
            }
        }

        public Int16 IsFileUploaded
        {
            get
            {
                return _IsFileUploaded;
            }
            set
            {
                _IsFileUploaded = value;
            }
        }

        public string InsertDateTime
        {
            get
            {
                return _InsertDateTime;
            }
            set
            {
                _InsertDateTime = value;
            }
        }

        public string UpdateDateTime
        {
            get
            {
                return _UpdateDateTime;
            }
            set
            {
                _UpdateDateTime = value;
            }
        }

        public Int32 FuelRatingCount
        {
            get
            {
                return _FuelRatingCount;
            }
            set
            {
                _FuelRatingCount = value;
            }
        }

        public string AverageFuelRating
        {
            get
            {
                return _AverageFuelRating;
            }
            set
            {
                _AverageFuelRating = value;
            }
        }

        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }

        public string UserLocation
        {
            get
            {
                return _UserLocation;
            }
            set
            {
                _UserLocation = value;
            }
        }

        public string UserAvatar
        {
            get
            {
                return _UserAvatar;
            }
            set
            {
                _UserAvatar = value;
            }
        }

        public Int16 IsFlagged
        {
            get
            {
                return _IsFlagged;
            }
            set
            {
                _IsFlagged = value;
            }
        }
    }
}