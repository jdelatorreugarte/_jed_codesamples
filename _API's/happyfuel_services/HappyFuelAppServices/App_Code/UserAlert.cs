﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyFuelAppServices
{
    public class UserAlert
    {

        private Int64 _UserId;
        private Int64 _FriendUserId;
        private string _FriendUserName;
        private Int32 _StatusId;
        private string _StatusKey;
        private string _StatusValue;
        private string _AvatarId;


        public Int64 UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                _UserId = value;
            }
        }

        public Int64 FriendUserId
        {
            get
            {
                return _FriendUserId;
            }
            set
            {
                _FriendUserId = value;
            }
        }

        public Int32 StatusId
        {
            get
            {
                return _StatusId;
            }
            set
            {
                _StatusId = value;
            }
        }

        public string FriendUserName
        {
            get
            {
                return _FriendUserName;
            }
            set
            {
                _FriendUserName = value;
            }
        }
        public string StatusKey
       {
           get
           {
               return _StatusKey;
           }
           set
           {
               _StatusKey = value;
           }
       }
        public string StatusValue
       {
           get
           {
               return _StatusValue;
           }
           set
           {
               _StatusValue = value;
           }
       }
        public string AvatarId
        {
            get
            {
                return _AvatarId;
            }
            set
            {
                _AvatarId = value;
            }
        }
      

    }
}