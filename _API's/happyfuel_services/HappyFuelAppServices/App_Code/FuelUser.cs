﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyFuelAppServices
{
    public class FuelUser
    {

        private Int64 _UserId;
        private string _FirstName;
        private string _LastName;
        private string _MiddleName;
        private string _AvatarId;
        private string _UserName;
        private string _Password;
        private string _UserEmail;
        private Int16 _IsActive;
        private Int16 _IsTempPassword;
        private string _InsertDateTime;
        private string _UpdateDateTime;
        private Int16 _IsProfilePublic;
        private string _Location;
        private Int32 _PostsCount;
        private Int32 _RatingsCount;

       


        public Int64 UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                _UserId = value;
            }
        }

        public string FirstName
        {
            get
            {
                return _FirstName;
            }
            set
            {
                _FirstName = value;
            }
        }

        public string LastName
        {
            get
            {
                return _LastName;
            }
            set
            {
                _LastName = value;
            }
        }

        public string MiddleName
        {
            get
            {
                return _MiddleName;
            }
            set
            {
                _MiddleName = value;
            }
        }

        public string AvatarId
        {
            get
            {
                return _AvatarId;
            }
            set
            {
                _AvatarId = value;
            }
        }

        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }

        public string Password
        {
            get
            {
                return _Password;
            }
            set
            {
                _Password = value;
            }
        }
        
        public string UserEmail
        {
            get
            {
                return _UserEmail;
            }
            set
            {
                _UserEmail = value;
            }
        }

        public Int16 IsActive
        {
            get
            {
                return _IsActive;
            }
            set
            {
                _IsActive = value;
            }
        }

        public Int16 IsTemporaryPassword
        {
            get
            {
                return _IsTempPassword;
            }
            set
            {
                _IsTempPassword = value;
            }
        }

        public string InsertDateTime
        {
            get
            {
                return _InsertDateTime;
            }
            set
            {
                _InsertDateTime = value;
            }
        }

        public string UpdateDateTime
        {
            get
            {
                return _UpdateDateTime;
            }
            set
            {
                _UpdateDateTime = value;
            }
        }

        public Int16 IsProfilePublic
        {
            get
            {
                return _IsProfilePublic;
            }
            set
            {
                _IsProfilePublic = value;
            }
        }
        public string Location
        {
            get
            {
                return _Location;
            }
            set
            {
                _Location = value;
            }
        }

        public Int32 PostsCount
        {
            get
            {
                return _PostsCount;
            }
            set
            {
                _PostsCount = value;
            }
        }

        public Int32 RatingsCount
        {
            get
            {
                return _RatingsCount;
            }
            set
            {
                _RatingsCount = value;
            }
        }
     }
}