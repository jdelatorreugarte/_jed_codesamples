$(function(){
	//jQuery slider
  var triggers = $('ul.triggers li');
  var images = $('ul.images li');
  var lastElem = triggers.length-1;
  var mask = $('.mask ul.images');
  var imgWidth = images.width();
  var target;

  //Calculate the width of each thumbnail based on the total amout of thumbnails.
  var triggersContainer = $('ul.triggers');
  var thumbTotalContainer = triggersContainer.width();
  var thumbTotal = triggers.length;
  var thumbnailSize = thumbTotalContainer / thumbTotal;
  var thumbnailSizewMargin = thumbnailSize - 3 ;

  triggers.last().css("width", thumbnailSize);
  triggers.each(function(){
    $(this).css("width", thumbnailSizewMargin);
  });

  triggers.first().addClass('selected');
  mask.css('width', imgWidth*(lastElem+1) +'px');

  function sliderResponse(target) {
      mask.stop(true,false).animate({'left':'-'+ imgWidth*target +'px'},300);
      triggers.removeClass('selected').eq(target).addClass('selected');
  }

  triggers.click(function() {
      if ( !$(this).hasClass('selected') ) {
          target = $(this).index();
          sliderResponse(target);
          clearInterval(timingRun);
      }
  });
  function sliderTiming() {
      target = $('ul.triggers li.selected').index();
      target === lastElem ? target = 0 : target = target+1;
      sliderResponse(target);
  }
  var timingRun = setInterval(function() { sliderTiming(); },8000);

  function resetTiming() {
      clearInterval(timingRun);
      timingRun = setInterval(function() { sliderTiming(); },8000);
  }
  //End of jquery slider

  //jQuery Accordian
  (function(){
    // $('dt').on('click', function (){
    //     dt.removeClass('active');
    //     $(this)
    //       .addClass('active')
    //       .next()
    //       .animate({height: "toggle", opacity: "toggle"}, 400);
    //       //.siblings('dd').slideUp(200);
    // });

    // $('span.expandAll').on('click', function(){
    //   dd.slideDown(300);
    // });
    // $('span.collapseAll').on('click', function(){
    //   dd.slideUp(300);
    // });

    $('dd').addClass('hide');

    $('dt').click(function() {
        $(this).toggleClass('active');
        if($(this).hasClass("active")) {
            $(this).next().slideDown(300);
        } else {
            $(this).next().slideUp(300);
        }
    });

  })();
  //End of accordian

  $('ul.tabs').each(function(){
    var $active, $content, $links = $(this).find('a');
    $active = $($links.filter('[href="'+location.hash+'"]')[0] || $links[0]);
    $active.addClass('active');
    $content = $($active.attr('href'));
    $links.not($active).each(function () {
      $($(this).attr('href')).hide();
    });
    $(this).on('click', 'a', function(e){
      $active.removeClass('active');
      $content.hide();
      $active = $(this);
      $content = $($(this).attr('href'));
      $active.addClass('active');
      $content.show();
      e.preventDefault();
    });
  });
  // This removes the CSS 'falback'
  $("#main_nav ul.child").removeClass("child");
  $("#main_nav ul.grandchild").removeClass("grandchild");
  $("#main_nav li").has("ul").hover(
    function(){
      $(this).addClass("current").children("ul").fadeIn();
    },
    function() {
      $(this).removeClass("current").children("ul").stop(true, true).css("display", "none");
    }
  );

  var hour1=0;
  var hour2=0;
  var min1=0;
  var ADBC1=0;
  var t1=0;
  var t2=0;
  var t3=0;
  var t4=0;
  var t5=0;
  var t6=0;
  var t7=0;
  var t8=0;
  var t9=0;
  var h1 = 0;
  var m1 = 0;
  var h2 = 0;
  var m2 = 0;
  var h3 = 0;
  var m3 = 0;

  function compute2() {
    var mm = "";
    var mm2 = "";
    h1 = parseFloat(stripBad($('.hr_digit').text()));
    m1 = parseFloat(stripBad($('.min_digit').text()));
    h2 = 10;
    m2 = 0;
    h3 = 14;
    m3 = 0;
    var er3a = $('.format_value').text();
    var er3b = 'add';
    var er3a;

    if(er3a == 'AM'){er3a_val = 0;}
    else if(er3a == 'PM'){er3a_val = 1;}

    if ((er3a_val==1) && (h1!=12)) {t1 = ((h1 + 12)*60) + m1;}
    else if ((er3a_val==0) && (h1==12)) {t1 = m1;}
    else {t1= (h1*60) + m1;}

    t2= (h2 * 60) + m2;

    t3= t1 + t2;
    if (t3<1) {t3 = t3 + (24*60);}

    t4= Math.floor(t3/60);
    
    t5= t3-(t4*60);
    t5=pad(t5);

    if (t4<12) {
      if (t4==0) {t4=12;}
      mm = "AM"
    }
    else if (t4==12) {mm = "PM"}
    else if ((t4>12) && (t4<24)) {t4=t4-12; mm = "PM"}
    else if (t4==24) {t4=t4-12; mm = "AM"}
    else if (t4>24) {
      t4=t4-24;
      if (t4==0) {t4=12;}
      mm = "AM"
    }

    t6= (h3 * 60) + m3;

    t7= t1 + t6;
    if (t7<1) {t7 = t7 + (24*60);}

    t8= Math.floor(t7/60);

     t9= t7-(t8*60);
     t9=pad(t9);

    if (t8<12) {
      if (t8==0) {t8=12;}
      mm2 = "AM"
    }
    else if (t8==12) {mm2 = "PM"}
    else if ((t8>12) && (t8<24)) {t8=t8-12; mm2 = "PM"}
    else if (t8==24) {t8=t8-12; mm2 = "AM"}
    else if (t8>24) {
      t8=t8-24;
      if (t8==0) {t8=12;}
      mm2 = "AM"
    }

    $('span.from-time').text( "  " + t4  + ":" + t5 + "  " + mm);
    $('span.to-time').text("  " + t8  + ":" + t9 + "  " + mm2);
  }

  function stripBad(string) {
    for (var i=0, output='', valid="eE-0123456789."; i<string.length; i++){
      if (valid.indexOf(string.charAt(i)) != -1){
        output += string.charAt(i)
      }
    }
    if (output=='') {output=0}
      return output;
  }

  function pad(n){
    if(n < 10){return "0"+ n;}
    else{return n;}
  }

  var $hour = $(".hr");
  var $hourBox = $hour.find(".digit");
  $hour.find(".arrowUp").on("click", hour_up);
  $hour.find(".arrowDown").on("click", hour_down);

  function hour_up(e) {
    var $this = $(this);
    currentHour = parseInt($hourBox.text());
    currentHour++;
    if(currentHour === 12){
        $hourBox.text(currentHour);
        format_up();
    } else if(currentHour > 12){
        currentHour = 1;
        $hourBox.text(currentHour);
    } else {
        $hourBox.text(currentHour);
    }
    compute2();
  }

  function hour_down(e) {
    var $this = $(this);
    currentHour = parseInt($hourBox.text());
    currentHour--;
    if(currentHour === 0){
        $hourBox.text(12);
        currentHour = 12;
    } else if(currentHour === 11){
        $hourBox.text(currentHour);
        format_down();
    }else {
        $hourBox.text(currentHour);
    }
    compute2();
  }

  var $min = $(".min");
  var $minBox = $min.find(".digit");
  $min.find(".arrowUp").on("click", min_up);
  $min.find(".arrowDown").on("click", min_down);
  var minInterval = 15;

  var $fromAndToMin = $(".from-min,.to-min");

  function min_up(e) {
    var $this = $(this);
    var currentMin = parseInt($minBox.text());
    currentMin += minInterval;
    
    if(currentMin === 60) {
        $minBox.text("00");
        $fromAndToMin.text("00");
        currentMin = 0;
        hour_up();
    } else {
        $minBox.text(currentMin);
        $fromAndToMin.text(currentMin);
    }
    compute2();
  }

  function min_down(e) {
    var $this = $(this);
    var currentMin = parseInt($minBox.text());
    currentMin -= minInterval;
    
    if(currentMin === 0) {
        $minBox.text("00");
        $fromAndToMin.text("00");
    } else {
        if(Math.abs(currentMin) === 60){
            currentMin = 0;
        } else if(currentMin < 0){
            currentMin = 45;
            $minBox.text(currentMin);
            $fromAndToMin.text(currentMin);
            hour_down();
        }else {
            $minBox.text(currentMin);
            $fromAndToMin.text(currentMin);
        }
    }
    compute2();
  }

  var $format = $(".format");
  var $formatVal = $(".format_value");
  $format.find(".arrowUp").on("click", format_up);
  $format.find(".arrowDown").on("click", format_down);
  
  function format_up(e) {
    var $this = $(this);
    var currentFormatValue = $(".format_value").text();
    if(currentFormatValue === 'AM') {
        $(".format_value").text('PM')
    }
    else if(currentFormatValue === 'PM'){
        $(".format_value").text('AM')
    }
    compute2();
  }

  function format_down(e) {
    var $this = $(this);
    var currentFormatValue = $(".format_value").text();
    if(currentFormatValue === 'AM') {
        $(".format_value").text('PM')
    }
    else if(currentFormatValue === 'PM'){
        $(".format_value").text('AM')
    }
    compute2();
  } 
  
  //Active State for both navigations
  var url = window.location.href;
  var page = url.substring(url.lastIndexOf('/') + 1);

  $('#main_nav a').each(function(){
     var myHref= $(this).attr('href');
     if( page == myHref) {
          $(this).addClass('active');
          $(this).parent().parent().prev().addClass('active');
     }
  });
  $('#left_sidebar ul a').each(function(){
     var myHref= $(this).attr('href');
     if( page == myHref) {
          $(this).addClass('active');
     }
  });

  //$('a[href^=http]').click(function(e){
  $('.xtrnlnk').click(function(e){
    e.preventDefault();
    var addressValue = $(this).attr("href");
    $('#overlay_modal').show();
    $('.cont_link').on('click', function(){
      window.open(addressValue);
      location.reload();
    });
    $('.back_link').on('click', function(){
      $('#overlay_modal').hide();
    });
  });
});