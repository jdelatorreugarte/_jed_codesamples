
// AJAX Login Handler
function vx_login( link,pass,lang ){
	//var loginurl = "https://" + window.location.host + "/ajax/vx_login";
	var loginurl = "/ajax/vx_login";
	var url = link.attr("href");
	
	jQuery.ajax({
      url: loginurl,
      data: "lc="+pass+"&lang="+lang,
      cache: false,
      type: "POST",
      dataType: "html",
      success: function(html){
    	if(html=='true')
        {
    		//Login Successful
    		window.location = url;
        }
        else
        {
          //Invalid login
          //trackLink(this, 'o', {event: 'event14', evars: [{name: 'eVar8', value: lang}]});
          trackLink(lang+"_Unsuccessful", 'o', {event: 'event14', evars: [{name: 'eVar8', value: lang}]});
          jQuery('.enter-pw').addClass('red');
        }
      }
	});
}