<div id="container">
  <div id="header">
    <div id="incivek-logo">
      <?php if ($page['logo']): ?>
        <a href="<?php print url('<front>'); ?>"><?php print render($page['logo']); ?></a>
      <?php endif; ?>
    </div>
    <div id="header-img-sub">
    <?php if ($page['headerbg']): ?>
      <?php print render($page['headerbg']); ?>
    <?php endif; ?>
    </div> <!-- /header-img -->
  </div>
  <div id="admin">
    <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
    <?php print $messages; ?>
  </div>
  <div id="body-wrapper">
    <?php print render($page['content']); ?>
    <div id="footer">
      <?php print render($page['footer']); ?>
    </div>
    <div class="clear"></div>
  </div>
  <br />
</div>
<?php print render($page['modal']); ?>
