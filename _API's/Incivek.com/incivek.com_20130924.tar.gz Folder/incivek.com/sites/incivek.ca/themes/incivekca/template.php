<?php

function incivekca_preprocess_node(&$variables) {
        
  if ($variables['is_front']) {
    // Remove "Read More" links from front page
    unset($variables['content']['links']);
  }
  
}

function incivekca_css_alter(&$css) {
    unset($css[drupal_get_path('module','system').'/system.base.css']);
    unset($css[drupal_get_path('module','system').'/system.theme.css']);
    unset($css[drupal_get_path('module','system').'/system.menus.css']);
    unset($css[drupal_get_path('module','node').'/node.css']);
    unset($css[drupal_get_path('module','user').'/user.css']);
    unset($css[drupal_get_path('module','field').'/theme/field.css']);
    unset($css[drupal_get_path('module','system').'/system.messages.css']);
}

function incivekca_preprocess_page(&$vars) {
  // Remove Edit tab from user profile page
  if (arg(0) == 'user') {
    incivekca_removetab('Edit', $vars);
  }
  
  // Remove View tab from all pages
  incivekca_removetab('View', $vars);

  // Remove tabs from search results page
  incivekca_removetab('Incivek Custom Search', $vars);
  incivekca_removetab('Content', $vars);
}

function incivekca_js_alter(&$js){
  // Remove Drupal's canned jQuery at Incivek theme layer only
  // leaves it in place for the Admin
  unset($js['settings']);
  unset($js['misc/drupal.js']);
  unset($js['misc/jquery.js']);
  unset($js['misc/jquery.once.js']);
}

// Remove undesired local task tabs.
// Related to yourthemename_removetab() in yourthemename_preprocess_page().
function incivekca_removetab($label, &$vars) {

  // Remove from primary tabs
  $i = 0;
  if (is_array($vars['tabs']['#primary'])) {
    foreach ($vars['tabs']['#primary'] as $primary_tab) {
      if ($primary_tab['#link']['title'] == $label) {
        unset($vars['tabs']['#primary'][$i]);
      }
      ++$i;
    }
  }

  // Remove from secondary tabs
  $i = 0;
  if (is_array($vars['tabs']['#secondary'])) {
    foreach ($vars['tabs']['#secondary'] as $secondary_tab) {
      if ($secondary_tab['#link']['title'] == $label) {
        unset($vars['tabs']['#secondary'][$i]);
      }
      ++$i;
    }
  }
}

function incivekca_preprocess_html(&$variables) {
    if(arg(0)=='node' && is_numeric(arg(1))) {
        $node = node_load(arg(1));
        $term = field_get_items('node', $node, 'field_language');
        if ($term) {
          $result = field_view_value('node', $node, 'field_language', $term[0] ,array('default'));
          $language = strtolower($result['#title']);
          $variables['classes_array'][] = $language;
        }
   }
}

?>