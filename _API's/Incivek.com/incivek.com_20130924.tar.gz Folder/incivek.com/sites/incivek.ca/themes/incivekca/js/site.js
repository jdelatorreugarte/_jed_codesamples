jQuery(document).ready(function(){
	// jqModal event handler centers window
	var centerPopup = function(hash){
		windowHeight   = jQuery(window).height();
		if(jQuery.browser.safari || jQuery.browser.chrome) bodyelem = jQuery("body")
		else bodyelem = jQuery("html,body")

		linkOffset     = jQuery(bodyelem).scrollTop();
		hash.w.css('top', linkOffset + (windowHeight * .2)).show();
	};

	jQuery("#fr-pass").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
        	jQuery('#button-entrer').click();
            return false;
        } else {
            return true;
        }
    });
	
	jQuery("#en-pass").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            jQuery('#button-enter').click();
            return false;
        } else {
            return true;
        }
    });
    
	jQuery('#button-entrer').click( function( link ) {
		link.preventDefault();
		password=jQuery("#fr-pass").val();
		vx_login(jQuery(this),password,'French');
	});
	
	jQuery('#button-enter').click( function( link ) {
		link.preventDefault();
		password=jQuery("#en-pass").val();
		vx_login(jQuery(this),password,'English');
	});
	
});


function leaveSite() {
    window.open(readCookie("siteCookie"), '_blank');
}

function redirect() {
    window.location = readCookie("siteCookie");
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);

        if (c.indexOf(nameEQ) == 0) 
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function trackLink() {
    if (arguments.length < 2) {
        alert("Error: Not enough arguments to TrackLink()");
        return false;
    }

    //Need to save global state
    var old_s = {};

    old_s.linkTrackVars = s.linkTrackVars;
    old_s.linkTrackEvents = s.linkTrackEvents;
    old_s.events = s.events;

    var name = arguments[0];

    var linkType = arguments[1];

    var options;

    if (arguments.length > 2) {
        options = arguments[2];
    }

    var linkTrackVars = 'channel'; //override to always track the channel
    var linkTrackEvents = '';

    var suppressTracking = false;

    if (options != undefined) {

        if (options.props != undefined) {
            for (var i in options.props) {
                var prop = options.props[i];

                if (linkTrackVars.length > 0) {
                    linkTrackVars += ",";
                }

                linkTrackVars += prop.name;

                //save old value
                old_s[prop.name] = s[prop.name];

                s[prop.name] = prop.value;
            }
        }

        if (options.event != undefined) {
            if (linkTrackVars.length > 0) {
                linkTrackVars += ",";
            }

            linkTrackVars += "events";

            linkTrackEvents += options.event;

            s.events = options.event;
        }

        if (options.evars != undefined) {
            for (var i in options.evars) {
                var evar = options.evars[i];

                if (linkTrackVars.length > 0) {
                    linkTrackVars += ",";
                }

                linkTrackVars += evar.name;

                //save old value
                old_s[evar.name] = s[evar.name];

                s[evar.name] = evar.value;
            }
        }

        if (options.supress != undefined) {
            suppressTracking = options.supress;
        }
    }

    s.linkTrackVars = linkTrackVars;
    s.linkTrackEvents = linkTrackEvents;

    if (!suppressTracking) {
        s.tl(true, linkType, name);
    }

    //Restore global state
    for (val in old_s) {
        s[val] = old_s[val];
    }

    return false;
}
