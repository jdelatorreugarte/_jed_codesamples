<?php
/**
 * VX_QR_Redirect module.  Adds QR code redirection functionality
 */

/**
 * Implementation of hook_menu().
 */
function vx_qr_redirect_menu() {
  $items['admin/config/system/vx_qr_redirect'] = array(
    'title' => 'Incivek QR Code Redirect',
    'description' => 'Control settings for the QR code redirect module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vx_qr_redirect_admin_settings'),
    'access arguments' => array('administer vx_qr_redirect'),
    'type' => MENU_NORMAL_ITEM,
  );
  
  return $items;
}

/**
 * Configuration page settings definitions corresponding with hook_menu
 */
function vx_qr_redirect_admin_settings() {
  $form = array();

  $form['vx_qr_redirect_map'] = array(
    '#type' => 'textarea',
    '#title' => t('QR Code Redirect Map'),
    '#default_value' => variable_get('vx_qr_redirect_map', ''),
    '#cols' => 60,
    '#rows' => 8,
    '#description' => t("Simple Array mapping QR codes to their associated target URLs.  QR codes should begin with /.  External URLs must be prefixed with http://.  Use <front> to redirect to the home page.  All other internal page redirects should not begin with /."),
  	'#required' => TRUE,
  );
  
  return system_settings_form($form);
}

/**
 * Implements hook_init().
 */
function vx_qr_redirect_init() {
    // Retrieve the QR code map configuration and parse it
    $cfg = variable_get("vx_qr_redirect_map",'');
    $map = array();

    if ($cfg) {
      foreach (explode(',',$cfg) as $mapping) {
        list($path,$target) = explode("=", trim($mapping), 2);
        $map[$path] = $target;
      }
    }

    // If this URL is mapped to a QR code redirect to the specified target
    $url=request_uri();
    
    if ($target = vx_qr_redirect_is_vanity_url($url)) {
      // Add tracking if omniture is enabled
      if (variable_get('vx_tracking_omniture_enable', 0)) {
        // Add vanity URL tracking per manifest
        $_SESSION['props']['Prop2']=$url;
        $_SESSION['evars']['eVar2']=$url;
      }
      
      // This is a mapped QR code.  301 redirect to the specified target
      drupal_goto($target,array('query' => array('qr'=>$url)),301);
    }
    
    // Check if qr code was passed in on the query string
    if (isset($_GET['qr'])) {
      // Add tracking if omniture is enabled
      if (variable_get('vx_tracking_omniture_enable', 0)) {
        // Add vanity URL tracking per manifest
        $_SESSION['props']['Prop2']=$_GET['qr'];
        $_SESSION['evars']['eVar2']=$_GET['qr'];
      }
    }
}

/*
 * Check if the supplied URL is stored as a QR code and return the target
 * if it is found
 */
function vx_qr_redirect_is_vanity_url($url) {
    // Retrieve the QR code map configuration and parse it
    $cfg = variable_get("vx_qr_redirect_map",'');
    $map = array();

    if ($cfg) {
      foreach (explode(',',$cfg) as $mapping) {
        list($path,$target) = explode("=", trim($mapping), 2);
        $map[$path] = $target;
      }
    }
    
    if (isset($map[$url])) {
      // The specified URL is stored in the QR code map.
      return $map[$url];
    }
    
    return false;
}

/**
 * Implements hook_permission
 * @see http://api.drupal.org/api/drupal/modules--system--system.api.php/function/hook_permission/7
 */
function vx_qr_redirect_permission() {
  return array(
    'administer vx_qr_redirect' => array(
      'title' => t('Administer Incivek QR Redirect'), 
    ),
  );
}
