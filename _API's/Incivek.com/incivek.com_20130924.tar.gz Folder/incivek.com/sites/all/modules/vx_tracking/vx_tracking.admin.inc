<?php

/**
 * Returns array of fields needed for vx_tracking, keyed by tag name
 * @return array
 */
function _vx_tracking_known_fields() {
  return array(
    'dblclick' => array(
      'type' => 'tracking_contents',
      'widget' => 'vx_tracking_textarea',
      'title' => t('DoubleClick Tag'),
      'description' => t('Enter a DoubleClick Floodlight tag to associate with this page. '),
    ),
  );
}


function vx_tracking_admin_settings() {
  $current_settings = variable_get('vx_tracking_settings', _vx_tracking_settings_default());
  $module_path = drupal_get_path('module', 'vx_tracking');
  $fields = field_info_fields();
  $field_found = FALSE;

  $known_tags = _vx_tracking_known_fields();
  
  $form['path_based'] = array(
    '#type' => 'fieldset',
    '#title' => t('Path-based tracking tags'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  foreach ($fields as $key => $field) {
    if ($field['module'] != 'vx_tracking') {
      continue;
    }
    $field_found = TRUE;
  }
  if (!$field_found) {
    $form['path_based']['basic_init'] = array(
      '#markup' => t('No tracking tags found in your installation') . '<br/>',
    );
  }
  
  $form['path_based']['use_path_based'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use path-based tracking tags'),
    '#default_value' => variable_get('vx_tracking_use_path_based', 1),
    '#return_value' => 1,
  );
  
  $form['standard_tags'] = array(
    '#type' => 'fieldset',
    '#title' => t('Create and attach'),
    '#description' => t('The following tracking tags are known to the module and can be created automatically.'),
    '#collapsible' => TRUE,
    '#attached' => array(
      'js' => array($module_path . '/js/admin.js'),
      'css' => array($module_path . '/css/admin.css'),
    ),
  );
  
  $field_instances = field_info_instances();

  // Build the sortable table header.
  $header = array(
    'title' => array('data' => t('Bundle / Entity Type')),
  );
  foreach ($known_tags as $name => $tag) {
    $header[$name] = $tag['title'];
  }
  
  foreach (field_info_bundles() as $entity_type => $bundles) {
    $entity_info = entity_get_info($entity_type);

    if (!$entity_info['fieldable'] || !_vx_tracking_supported_entity($entity_type)) {
      continue;
    }
    $options[$entity_type]['data'] = array(
      'title' => array(
        'class' => array('entity_type_collapsible', 'entity_type_collapsed', "entity_name_$entity_type"),
        'data' => $entity_info['label']
      )
    );
    foreach ($known_tags as $name => $tag) {
      $bundle_workable[$name] = FALSE;
      $options[$entity_type]['data'][$name] = array(
        'data' => array(
          '#type' => 'checkbox',
          '#attributes' => array('class' => array('cb_bundle_parent', "cb_bundle_name_{$entity_type}_{$name}")),      
          '#return_value' => 1,
          '#checked' => FALSE,
        ),
      );
    }
    
    // How do we mark that specific tag is already attached to bundle
    $checked_markup = array(
      '#markup' => theme('image', 
        array(
          'path' => 'misc/watchdog-ok.png',
          'width' => 18,
          'height' => 18,
          'alt' => 'ok',
          'title' => 'ok',
        )),
    );
        
    foreach ($bundles as $key => $bundle) {
      // Which tracking tags are set for this bundle?
      $tag_set = array();
      foreach ($field_instances[$entity_type][$key] as $bundle_instance) {
        if ($bundle_instance['widget']['module'] == 'vx_tracking') {
          $field_info = field_info_field_by_id($bundle_instance['field_id']);
          $tag_set[$field_info['settings']['tag_name']] = 1;
        }        
      }
      
      $options[$entity_type . '_' . $key] = array(
        'class' => array('entity_type_children', "entity_child_$entity_type"),
        'style' => 'display: none',
        'data' => array(
          'title' => array(
            'class' => array('entity_type_child_title'),
            'data' => $bundle['label'],
          ),
        ),
      );
      
      foreach ($known_tags as $name => $tag) {
        
        if (empty($tag_set[$name])) {
          // Mark parent bundle as workable - display checkbox.
          $bundle_workable[$name] = TRUE;
          $options[$entity_type . '_' . $key]['data'][$name] = array(
            'data' => array(
              '#name' => $entity_type . '[' . $key . '][' . $name . ']',
              '#type' => 'checkbox',
              '#attributes' => array('class' => array('cb_bundle_child', "cb_child_{$entity_type}_{$name}")),
              '#return_value' => 1,
              '#checked' => FALSE,
            )
          );
        }
        else {
          $options[$entity_type . '_' . $key]['data'][$name]['data'] = $checked_markup; 
        }
      }
    }
    // Now check if we have completely set bundles
    foreach ($known_tags as $name => $tag) {
      if (!$bundle_workable[$name]) {
        $options[$entity_type]['data'][$name]['data'] = $checked_markup; 
      }
    }
  }
  $form['standard_tags']['existing'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No content available.'),
  );
  
  $form['standard_tags']['basic_init_op'] = array(
    '#type' => 'submit',
    '#value' => t('Attach'),
  );
  
  // Omniture specific settings
  $form['omniture'] = array(
    '#type' => 'fieldset',
    '#title' => t('Omniture Settings'),
    '#description' => t('Omniture specific settings.'),
    '#collapsible' => TRUE,
  );

  $form['omniture']['omniture_enable'] = array(
	'#type' => 'checkbox',
    '#title' => t('Enable Omniture tags'),
    '#default_value' => variable_get('vx_tracking_omniture_enable', 0),
    '#description' => t("Control whether or not to add omniture tags to pages."),
    '#required' => FALSE,
  );
  
  $form['omniture']['omniture_pg_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Omniture Page Prefix'),
    '#default_value' => variable_get('vx_tracking_omniture_pg_prefix', ''),
    '#size' => 60,
    '#description' => t("Prefix added to Omniture pagename and channel variables."),
    '#required' => TRUE,
  );

  $form['omniture']['omniture_pg_type'] = array(
    '#type' => 'textfield',
    '#title' => t('Omniture Page Type'),
    '#default_value' => variable_get('vx_tracking_omniture_pg_type', ''),
    '#size' => 60,
    '#description' => t("Page type sent to Omniture via eVar9 and prop9."),
    '#required' => TRUE,
  );

  $form['omniture']['omniture_header'] = array(
    '#type' => 'textarea',
    '#title' => t('Omniture Page Code Header'),
    '#default_value' => variable_get('vx_tracking_omniture_header', ''),
    '#cols' => 60,
    '#rows' => 4,
    '#description' => t("Omniture page code section 1.  All code that comes BEFORE the user vars section of the Omniture page code."),
    '#required' => FALSE,
  );

  $form['omniture']['omniture_footer'] = array(
    '#type' => 'textarea',
    '#title' => t('Omniture Page Code Footer'),
    '#default_value' => variable_get('vx_tracking_omniture_footer', ''),
    '#cols' => 60,
    '#rows' => 4,
    '#description' => t("Omniture page code section 2.  All code that comes AFTER the user vars section of the Omniture page code."),
    '#required' => FALSE,
  );
  
  
  $form['op'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit', 
  );
  return $form;
}

function vx_tracking_admin_settings_submit($form, &$form_state) {
  variable_set('vx_tracking_use_path_based', $form_state['values']['use_path_based']);
  variable_set('vx_tracking_omniture_enable', $form_state['values']['omniture_enable']);
  variable_set('vx_tracking_omniture_pg_prefix', $form_state['values']['omniture_pg_prefix']);
  variable_set('vx_tracking_omniture_pg_type', $form_state['values']['omniture_pg_type']);
  variable_set('vx_tracking_omniture_header', $form_state['values']['omniture_header']);
  variable_set('vx_tracking_omniture_footer', $form_state['values']['omniture_footer']);
  
  if ($form_state['clicked_button']['#value'] == t('Attach')) {
    _vx_tracking_admin_fields_create_attach($form_state['input']);
  }
  else {
  }
  drupal_set_message(t('Incivek tracking settings saved'), 'status');
}

/**
 * Path based form callback
 */
function vx_tracking_admin_path_page() {
  if (!empty($_GET['path']) && !empty($_GET['lang'])) {
    return drupal_get_form('vx_tracking_admin_path', $_GET['lang'], $_GET['path']);
  }
  else {
    return MENU_ACCESS_DENIED;
  }
}

/*
 * Path-based meta tags editing form.
 * @param string $path - path being edited.
 */
function vx_tracking_admin_path($form, &$form_state) {
  $args = func_get_args();
  if (count($args) > 3) {
    $lang = $args[2];
    $path = $args[3];
  }
  else {
    return MENU_ACCESS_DENIED;
  }
  
  $controller = new DrupalDefaultEntityController('tracking_path_based');
  $entity_id = db_select('vx_tracking_path_based', 'm')
    ->fields('m', array('id'))
    ->condition('lang', $lang)
    ->condition('path', $path)
    ->execute()
    ->fetchField();
  if (!$entity_id) {
    $entity_id = db_insert('vx_tracking_path_based')
      ->fields(array('lang' => $lang, 'path' => $path))
      ->execute();
  }
  $entities = $controller->load(array($entity_id));
  $form_state['entity'] = $entities[$entity_id];
  field_attach_form('tracking_path_based', $entities[$entity_id], $form, $form_state);
  // Do we have any fields attached?
  $childen = element_children($form);
  if (!$childen) {
    $form['empty_message'] = array(
      '#markup' => t('No fields attached to the path-based tracking tags. Please !define them first in the module settings screen',
        array('!define' => l(t('define'), 'admin/config/system/vx_tracking'))) . '<br/>',
    );
  }
  $form['op'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit', 
  );  
  return $form;
}

function vx_tracking_admin_path_validate($form, &$form_state) {
  entity_form_field_validate('tracking_path_based', $form, $form_state);
}

function vx_tracking_admin_path_submit($form, &$form_state) {
  if (!$form_state['entity']) {
    form_set_error();
    drupal_set_message(t('Wrong path'), 'error');
    return;
  }
  
  $entity = $form_state['entity'];
  entity_form_submit_build_entity('tracking_path_based', $entity, $form, $form_state);
  field_attach_update('tracking_path_based', $entity);
  
  drupal_set_message('Tracking tags updated', 'status');
  $form_state['redirect'] = FALSE;
}

// Create known tracking fields.
function _vx_tracking_admin_fields_create_attach($input) {
  foreach (field_info_bundles() as $entity_type => $bundles) {
    $entity_info = entity_get_info($entity_type);
    if (!$entity_info['fieldable']) {
      continue;
    }

    foreach ($bundles as $key => $bundle) {
      if (isset($input[$entity_type][$key])) {
        foreach ($input[$entity_type][$key] as $tag_name => $tag_value) {
          _vx_tracking_field_attach($entity_type, $key, $tag_name);
        }
      }
    }
  }
}
  
function _vx_tracking_field_attach($entity_type, $bundle, $tag_name) {
  static $tracking_fields = FALSE;
  static $field_id = array();
  static $known_tags = FALSE;
  
  // Get vx_tracking fields info
  if (!$tracking_fields) {
    $known_tags = _vx_tracking_known_fields();
    
    foreach(field_info_fields() as $name => $field_info) {
      if ($field_info['module'] == 'vx_tracking') {
        $tracking_fields[$field_info['settings']['tag_name']] = $field_info;
        $field_id[$field_info['settings']['tag_name']] = $field_info['id'];
      }
    }
  }

  // Ignore unknown tags.
  if (!isset($known_tags[$tag_name])) {
    return;
  }
  
  // Check if tracking field exists, create if necessary.
  if (empty($field_id[$tag_name])) {
    $field = array(
      'field_name' => "tracking_$tag_name", 
      'type' => 'vx_tracking', 
      'module' => 'vx_tracking',
      'settings' => array('tag_name' => (isset($known_tags[$tag_name]['tag_name']) ? $known_tags[$tag_name]['tag_name'] : $tag_name)), 
      'cardinality' => 1,
    );
    $field = field_create_field($field);
    $field_id[$tag_name] = $field['id'];
    $tracking_fields[$tag_name] = $field;
  }
  else {
    $field = $tracking_fields[$tag_name];
  }
  
  // Do nothing if instance already exists.
  if (isset($field['bundles'][$entity_type])
    && in_array($bundle, $field['bundles'][$entity_type])) {
    return;
  }
  
  // Now create field instance attached to requested bundle
  $instance = array(
    'field_name' => $field['field_name'],
    'entity_type' => $entity_type,
    'bundle' => $bundle,
    'label' => $known_tags[$tag_name]['title'],
    'formatter' => 'vx_tracking_default',
    'widget' => array(
      'type' => $known_tags[$tag_name]['widget'],
      'weight' => 0,
    ),
  );
  if (isset($known_tags[$tag_name]['options'])) {
    $instance['settings']['options'] = $known_tags[$tag_name]['options'];
  }
  field_create_instance($instance);
}

/**
 * Display all paths that have tracking tags defined for them
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags($form, $form_state) {
  // get current language and module path
  global $language;
  $module_path = drupal_get_path('module', 'vx_tracking');
  // grab language list
  $languages = _vx_tracking_language_list();
  // create table form
  $form['new_path_based'] = array(
    '#type' => 'fieldset',
    '#title' => t('New path'),
    '#collapsible' => FALSE,
    '#attached' => array(
      'js' => array($module_path . '/js/admin.js'),
      'css' => array($module_path . '/css/admin.css'),
    ),
    'new_path' => array(
      '#tpye' => 'container',
      '#prefix' => '<div style="float:left">',
      '#suffix' => '</div><div style="clear:both;"> </div>',
      'path' => array(
        '#type' => 'textfield',
        '#title' => t('Path:'),
        '#description' => t('Enter path starting without a leading "/".'),
        '#prefix' => '<div style="float:left;">',
        '#suffix' => '</div>',
      ),
      'language' => array(
        '#type' => 'select',
        '#title' => 'Select language',
        '#options' => $languages,
        '#default_value' => $language->language,
        '#prefix' => '<div style="float:left; margin-left: 40px;">',
        '#suffix' => '</div><div style="clear:both;"> </div>',
      ),
      'new_path_submit' => array(
        '#type' => 'submit',
        '#value' => t('Create'),
        '#attributes' => array('style' => 'margin-bottom: 20px;')
      )
    )
  );
  $form['path_based'] = array(
    '#type' => 'fieldset',
    '#title' => t('Manage Paths'),
    '#collapsible' => FALSE
  );
  $header = array(
    'path' => array('data' => t('Path'), 'field' => 'p.path'),
    'language' => array('data' => t('Language'), 'field' => 'p.lang', 'width' => '10%'),
    'operations' => array('data' => t('Operations'), 'width' => '30%', 'colspan' => 2),
  );
  $query = db_select('vx_tracking_path_based', 'p')->extend('PagerDefault')->extend('TableSort');
  $result = $query
   ->fields('p')
   ->limit(50)
   ->orderByHeader($header)
   ->execute();
  // set up to fill options array
  $destination = drupal_get_destination();
  $options = array();
  // fill options array
  foreach ($result as $path) {
    $options[] = array(
      'path' => l($path->path, $path->path, array('query' => $destination)),
      'language' =>  $languages[check_plain($path->lang)],
      'edit' =>
        l(t('edit'),
           'admin/config/system/vx_tracking/path_based_tags/edit',
           array('query' => array('path' => $path->path, 'lang' => $path->lang, 'destination' => $destination['destination']))),
      'delete' =>
        l(t('delete'),
           'admin/config/system/vx_tracking/path_based_tags/delete',
           array('query' => array('path' => $path->path, 'lang' => $path->lang, 'destination' => $destination['destination'])))
    );
  }
  // add created table
  $form['path_based']['existing'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $options,
    '#empty' => t('No content available.'),
    '#attributes' => array('id' => 'path_based_tags'),
  );
  // add default page
  $form['path_based']['pager'] = array('#markup' => theme('pager', array('tags' => NULL)));
  // return listing form
  return $form;
}

/**
 * Validates the existance of a valid path provided by the user. Path has to be
 * at least 2 characters long.
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_validate($form, $form_state) {
  if(isset($form_state['values']['path'])) {
    // create path and empty entity
    if(strlen(trim($form_state['values']['path'])) < 2) {
        form_set_error('path', t('Illegal path entered.'));
    }
  }
}

/**
 * Handles the creation of a new path by redirecting to
 * vx_tracking_admin_path_based_tags_edit() which autocreates new
 * entities if they don't exist yet for a given path and language.
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_submit($form, &$form_state) {
  if(isset($form_state['values']['path'])) {
    // redirect to existing form
    $destination = drupal_get_destination();
    $form_state['redirect'] = array(
      'admin/config/system/vx_tracking/path_based_tags/edit',
      array(
       'query' => array(
         'lang' => check_plain($form_state['values']['language']),
         'path' => check_plain($form_state['values']['path']),
         'destination' => $destination['destination'])
       )
    );
  }
}

/**
 * Displays a form to the user allowing him to edit the path and all the fields
 * of the metatag entity associated with that path
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_edit($form, $form_state) {
  $controller = new DrupalDefaultEntityController('tracking_path_based');
  $entity_id = db_select('vx_tracking_path_based', 'm')
    ->fields('m', array('id'))
    ->condition('lang', check_plain($_GET['lang']))
    ->condition('path', check_plain($_GET['path']))
    ->execute()
    ->fetchField();
  if (!$entity_id) {
    $entity_id = db_insert('vx_tracking_path_based')
      ->fields(array('lang' => check_plain($_GET['lang']), 'path' => check_plain($_GET['path'])))
      ->execute();
  }
  $entities = $controller->load(array($entity_id));
  $form_state['entity'] = $entities[$entity_id];
  field_attach_form('tracking_path_based', $entities[$entity_id], $form, $form_state);
  //
  $form['path'] = array(
      '#type' => 'textfield',
      '#title' => t('Edit path'),
      '#description' => t('Enter path starting without a leading "/".'),
      '#default_value' => check_plain($_GET['path']),
      '#weight' => '-50'
  );
  $form['old_path'] = array(
      '#type' => 'hidden',
      '#value' => check_plain($_GET['path'])
  );
  $form['old_lang'] = array(
      '#type' => 'hidden',
      '#value' => check_plain($_GET['lang'])
  );
  //
  $form['submit'] = array(
    '#value' => t('Submit'),
    '#type' => 'submit',
    '#weight' => '49'
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
    '#weight' => 20,
    '#submit' => array('vx_tracking_admin_path_based_tags_edit_cancel'),
    '#weight' => '50'
  );
  return $form;
}

/**
 * Returns back to the metatags listing
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_edit_cancel($form, &$form_state) {
  if(isset($_GET['destination']))
    $form_state['redirect'] = $_GET['destination'];
}

/**
 * Checks if the user has submitted a valid path. Path must be at least 2
 * characters long
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_edit_validate($form, $form_state) {
  if(isset($form_state['values']['path'])) {
    // create path and empty entity
    if(strlen(trim($form_state['values']['path'])) < 2) {
        form_set_error('path', t('Illegal path entered.'));
    }
  }
}

/**
 * Updates the tags entity for a given path after the user has submitted the
 * form. Also updates the path if it has been altered.
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_edit_submit($form, $form_state) {
  // abort if there is no valid entity
  if (!$form_state['entity']) {
    form_set_error();
    drupal_set_message(t('Wrong path'), 'error');
    return;
  }
  // update path if it has been altered
  if($form_state['values']['path'] != $form_state['values']['old_path']) {
    // check if there isn't already a path like the new path
    $entity_id = db_select('vx_tracking_path_based', 'm')
      ->fields('m', array('id'))
      ->condition('lang', check_plain($form_state['values']['old_lang']))
      ->condition('path', check_plain($form_state['values']['path']))
      ->execute()
      ->fetchField();
    // if not continue
    if(!$entity_id) {
    db_update('vx_tracking_path_based')
      ->fields(array(
        'path' => check_plain(trim($form_state['values']['path'])),
      ))
      ->condition('id', $form_state['entity']->id)
      ->execute();
    } else {
      // otherwise abort with error message
      form_set_error('path', t('Path already exists!'));
      return;
    }
  }
  // update entity fields
  $entity = $form_state['entity'];
  entity_form_submit_build_entity('tracking_path_based', $entity, $form, $form_state);
  field_attach_update('tracking_path_based', $entity);
  // display message and redirect
  drupal_set_message('Tracking tags updated', 'status');
  if(isset($_GET['destination']))
    $form_state['redirect'] = $_GET['destination'];
  else
    $form_state['redirect'] = FALSE;
}

/**
 * Display a delete confirmation for the user allowing him to choose to proceed
 * or abort the removal.
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_delete($form, $form_state) {
  // display delete confirmation
  if(isset($_GET['path'])) {
    $form['intro'] = array(
      '#type' => 'item',
      '#markup' => t('Do you really want to delete the following path:'),
    );
    $form['item'] = array(
      '#type' => 'item',
      '#markup' => $_GET['path'],
      '#prefix' => '<ul><li>',
      '#suffix' => '</ul></li>'
    );
    $form['note'] = array(
      '#type' => 'item',
      '#markup' => t('This action cannot be undone.'),
    );
    $form['actions'] = array(
      '#type' => 'actions',
      'delete' => array(
        '#type' => 'submit',
        '#value' => t('Delete'),
        '#submit' => array('vx_tracking_admin_path_based_tags_delete_submit')),
      'cancel' => array(
        '#type' => 'submit',
        '#value' => t('Cancel'),
        '#limit_validation_errors' => array(),
        '#submit' => array('vx_tracking_admin_path_based_tags_delete_cancel')));
    return $form;
  }
}

/**
 * Deletion has been confirmed. Item will be deleted if it is a valid one.
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_delete_submit($form, &$form_state) {
  // fetch and delete
  $controller = new DrupalDefaultEntityController('tracking_path_based');
  $entity_id = db_select('vx_tracking_path_based', 'm')
    ->fields('m', array('id'))
    ->condition('lang', check_plain($_GET['lang']))
    ->condition('path', check_plain($_GET['path']))
    ->execute()
    ->fetchField();
  if (!$entity_id) {
    drupal_set_message(t('Path not found!'), 'error');
  } else {
    tracking_path_based_delete($entity_id);
  }
}

/**
 * Cancels the deletion and should return to the listing
 *
 * @param type $form
 * @param type $form_state
 */
function vx_tracking_admin_path_based_tags_delete_cancel($form, &$form_state) {
  if(isset($_GET['destination']))
    $form_state['redirect'] = $_GET['destination'];
}

/**
 * creates a select list compatible list of languages keyed by language tags and
 * with localized language values.
 *
 * @global type $language
 * @return array $languages
 */
function _vx_tracking_language_list() {
  global $language;
  $languages = array();
  foreach(language_list() as $lang) {
    $languages[$lang->language] = t($lang->name);
  }
  return $languages;
}


// Determine whether or not this entity type is supported by vx_tracking
function _vx_tracking_supported_entity($entity_type) {
  switch ($entity_type) {
    case 'node':
      return true;      
      break;
    case 'tracking_path_based':
      return true;      
      break;
  }
  return false; 
}
