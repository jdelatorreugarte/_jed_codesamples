<?php
/**
 * @file Functionality needed for upgrade path from nodewords
 */

/**
 * Returns array of fields needed for upgrade from nodewords, keyed by meta name
 * @return array
 */
function _metatags_quick_known_fields() {
  return array(
    'abstract' => array(
      'type' => 'meta_contents',
      'widget' => 'metatags_quick_textfield',
      'title' => t('Abstract'),
      'description' => t('Enter a short abstract. Typically it is one sentence.'),
    ),
    'copyright' => array(
      'type' => 'meta_contents',
      'widget' => 'metatags_quick_textfield',
      'title' => t('Copyright'),
      'description' => t('Enter a short copyright statement.'),
    ),
    'description' => array(
      'type' => 'meta_contents',
      'widget' => 'metatags_quick_textarea',
      'title' => t('Description'),
      'description' => t('Enter a description. Limit your description to about 20 words, with a maximum of 255 characters. It should not contain any HTML tags or other formatting.'),
    ),
    'keywords' => array(
      'type' => 'meta_contents',
      'widget' => 'metatags_quick_textfield',
      'title' => t('Keywords'),
      'description' => t('Enter a short abstract. Typically it is one sentence.'),
    ),
    'revisit_after' => array(
      'meta_name' => 'revisit-after',
      'type' => 'meta_contents',
      'widget' => 'metatags_quick_textfield',
      'title' => t('Revisit after'),
      'description' => t('day(s)'),
    ),
    'robots' => array(
      'type' => 'meta_contents',
      'widget' => 'metatags_quick_checkboxes',
      'title' => t('Robots'),
      'options' => 'noarchive,follow,nofollow,index,noindex,noodp,nosnippet,noydir', 
    ),
    'googlebot' => array(
      'type' => 'meta_contents',
      'widget' => 'metatags_quick_checkboxes',
      'title' => t('GoogleBot'),
      'options' => 'noarchive,follow,nofollow,index,noindex,noodp,nosnippet,noydir', 
    ),
  );
}
