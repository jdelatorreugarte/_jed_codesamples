<?php
/*
* Global function definitions
*/

function parse_config($filename) {
	$file = file($filename);
	if (is_array($file)) {
		foreach ($file as $line) {
			if (preg_match("/^\s*([a-zA-Z0-9_]+)=([a-zA-Z0-9 .&-@=_<>\$\!\"\']+)\s*$/",$line,$matches)) {
				$conf[ $matches[1] ] = $matches[2];
			}
		}
	} else {
		die("Missing or unreadable config file ($filename)...cannot continue\n");
	}
	
	return $conf;
}

function get_cfg_data() {
	global $db;
	$cfg = array();

	$sql = "select name,value from vx_extract_ctl";
	
	$result_arr=$db->build_results($sql);
	
	if ($db->Error) {
		die($db->Error); 
	}

	foreach ($result_arr as $record) {
		$cfg[$record['name']]=$record['value'];
	}
	
	return $cfg;
}

function set_last_extract() {
	global $db;
	
	$sql = "UPDATE vx_extract_ctl SET `value` = NOW() WHERE `name` = 'LAST_EXTRACT'";

    if (!$db->query($sql))
    {
        die($db->Error);
    }
	
	return;
}

function flag_records_as_extracted() {
	global $db;
	$tables = array('incivek_vx_users','incivek_vx_unsubscribe');
	
	foreach ($tables as $table) {
      $sql = "UPDATE ".$table." SET `extract_flag` = 1 WHERE `extract_flag` <> 1";
  
      if (!$db->query($sql))
      {
          die($db->Error);
      }
	}
	
	return;
}

function get_vx_user_records() {
	global $db;
	global $encoded;
	
	$results = array();
	$tables = array('incivek_vx_users');
	
	foreach ($tables as $table) {
        $sql = "SELECT date_format(update_date,'%Y%m%d%H%i%s') transaction_id,date_format(update_date,'%Y%m%d %H:%i:%s') update_date,vx_uid,title,fnm,mi,lnm,addr_1,addr_2,city,state,zipcode,phone,email,individual_type,specialty,prof_designation FROM ".$table." WHERE extract_flag <> 1 ORDER BY vx_uid";
    
    	$result_arr=$db->build_results($sql);
    
    	if ($db->Error) {
    		die($db->Error); 
    	}
    	
    	// Walk through the results and decode any strings necessary
    	for ($i = 0; $i < sizeof($result_arr); ++$i)
        {
          foreach ($result_arr[$i] as $column => $value) {
            if (in_array($column, $encoded)) {
              // decode value
              $result_arr[$i][$column] = decodeStr($value);
            }
          }
        }    	
    	$results = array_merge($results,$result_arr);
	}

	return $results;
}

function get_vx_unsubscribe_records() {
	global $db;
	global $encoded;
	
	$results = array();
	$tables = array('incivek_vx_unsubscribe');
	
	foreach ($tables as $table) {
  	  $sql = "SELECT date_format(update_date,'%Y%m%d%H%i%s') transaction_id,date_format(update_date,'%Y%m%d %H:%i:%s') update_date,vx_uid,fnm,mi,lnm,addr_1,addr_2,city,state,zipcode,email,individual_type FROM ".$table." WHERE extract_flag <> 1 ORDER BY vx_uid";
  
  	  $result_arr=$db->build_results($sql);
  
  	  if ($db->Error) {
  		die($db->Error); 
  	  }
  
      // Walk through the results and decode any strings necessary
      for ($i = 0; $i < sizeof($result_arr); ++$i)
      {
        foreach ($result_arr[$i] as $column => $value) {
          if (in_array($column, $encoded)) {
            // decode value
            $result_arr[$i][$column] = decodeStr($value);
          }
        }
      }
      $results = array_merge($results,$result_arr);
	}
	
	return $results;
}

function get_user_profile_values($vx_uid,$individual_type) {
	global $db;
	$userprofile=array();
	
	if ($individual_type == 'C') {
	  $table='incivek_vx_user_profile_values';
	} else if ($individual_type == 'H') {
	  $table='incivekhcp_vx_user_profile_values';
	} else {
	  print "ERROR: Unrecognized individual type - ".$individual_type;
	  exit;
	}
	
	$sql = "SELECT name,value FROM ".$table." WHERE vx_uid='".$vx_uid."'";

	$result_arr=$db->build_results($sql);
	
	if ($db->Error) {
		die($db->Error); 
	}

	foreach ($result_arr as $record) {
		$userprofile[decodeStr($record['name'])]=decodeStr($record['value']);
	}
	
	return $userprofile;
}

function get_unsubscribe_opts($vx_uid,$individual_type) {
	global $db;
	$opts=array();
	
	if ($individual_type == 'C') {
	  $table='incivek_vx_unsubscribe_opts';
	} else 	if ($individual_type == 'H') {
	  $table='incivekhcp_vx_unsubscribe_opts';
	} else {
	  print "ERROR: Unrecognized individual type - ".$individual_type;
	  exit;
	}
	
	$sql = "SELECT name,value FROM ".$table." WHERE vx_uid='".$vx_uid."' ORDER BY name";

	$result_arr=$db->build_results($sql);
	
	if ($db->Error) {
		die($db->Error); 
	}

	foreach ($result_arr as $record) {
		$opts[decodeStr($record['name'])]=decodeStr($record['value']);
	}

	return $opts;
}

function get_open_ans_questions() {
	global $db;
	$questions=array();
	
	// Profile questions
	$sql = "SELECT distinct question_name FROM vx_extract_srvy_resp_lkp WHERE open_answer_ind='Y' ORDER BY question_name";

	$result_arr=$db->build_results($sql);
	
	if ($db->Error) {
		die($db->Error); 
	}

	foreach ($result_arr as $record) {
		$questions[$record['question_name']]=true;
	}
	
	return $questions;
}

function translate_question_answer($individual_type,$question,$answer) {
	global $db;
	$question_id='';
	$answer_id='';
	
	$sql = "SELECT question_id,answer_id FROM vx_extract_srvy_resp_lkp WHERE individual_type='".$individual_type."' AND question_name='".$question."' AND answer_text='".$answer."'";

	$result_arr=$db->build_results($sql);
	
	if ($db->Error) {
		die($db->Error); 
	}

	if (count($result_arr)) {
		$question_id=$result_arr[0]['question_id'];
		$answer_id=$result_arr[0]['answer_id'];
	}
		
	return array($question_id,$answer_id);
}

function translate_specialty($answer_text) {
	global $db;
	$specialty_code='';
	
	$sql = "SELECT specialty_code FROM vx_extract_specialty_resp_lkp WHERE answer_text='".$answer_text."'";

	$result_arr=$db->build_results($sql);
	
	if ($db->Error) {
		die($db->Error); 
	}

	if (count($result_arr)) {
		$specialty_code=$result_arr[0]['specialty_code'];
	}
		
	return $specialty_code;
}

/**
 * Decode an encoded string and return in raw text
 * 
 */
function decodeStr($input){
  // Decode String
  $str = base64_decode($input);
  $str = strrev($str);
  $str = base64_decode($str);
  return $str;
}

/**
 * Create and encoded string from the specified input string.
 */
function encodeStr($input){
  // Encode string
  $str = base64_encode($input);
  $str = strrev($str);
  $str = base64_encode($str);
  return $str;
}
?>
