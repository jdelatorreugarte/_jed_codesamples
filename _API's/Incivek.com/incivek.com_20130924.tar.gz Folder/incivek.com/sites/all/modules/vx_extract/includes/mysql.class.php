<?php
/*
* MySQL class
* - MySQL wrapper class for PHP
*
* @last_modification July 18 2010
*
*/

class MySQL
{ 
    var $Host = '';            // Hostname of our MySQL server 
    var $Database = '';        // Logical database name on that server 
    var $User = '';            // Database user 
    var $Password = '';        // Database user's password 
    var $Link_ID = 0;          // Result of mysql_connect() 
    var $Query_ID = 0;         // Result of most recent mysql_query() 
    var $Record    = array();  // array for current record in result
    var $Row;                  // Current row number 
    var $Errno = 0;            // Error state of query 
    var $Error = ""; 
    var $dbname = '';          // Current connected DB


    /**
     * Connects to database.
     * Returns TRUE if connection succeeded, FALSE otherwise.
     * @return boolean
     */
    function connect($Host='', $Database = '', $User = '', $Password = '') 
    { 
        if($this->Link_ID == 0) 
        {

            $this->Error = null;
            
            // Connect to MySQL using SSL encryption
            $this->Link_ID = mysql_connect($Host, $User, $Password, true, MYSQL_CLIENT_SSL); 
            if (!$this->Link_ID) 
            { 
                return FALSE;
            } 
            
            // Select the requested database
            $SelectResult = mysql_select_db($Database, $this->Link_ID); 
            if(!$SelectResult) 
            { 
                $this->Errno = mysql_errno($this->Link_ID); 
                $this->Error = mysql_error($this->Link_ID);
                return FALSE;
            } 
        }
        
        $this->dbname = $Database;
        return TRUE;
    } 

    function query($sql) 
    {
    	$this->Error = null;
    	
        $this->connect(); 
        $this->Query_ID = mysql_query($sql,$this->Link_ID); 
        $this->Row = 0; 
 
        if (!$this->Query_ID) 
        {
            $this->Errno = mysql_errno(); 
            $this->Error = mysql_error();
            return FALSE;
        } 
        
        return $this->Query_ID; 
    } 
    
    function build_results($sql) 
    { 
    	$result=array();
        if ( !$res = $this->query($sql) ) {
        	return FALSE;
        }
        
        while ( $row = mysql_fetch_assoc($res) ) 
        {
            $result[] = $row;
        }

              
        if(!is_array($result)) {
        	return FALSE; 
        }
        
        $this->total_field=mysql_num_fields($this->Query_ID); 

        return $result; 
    } 

    
    function num_field() 
    { 
        return mysql_num_fields($this->Query_ID); 
    } 

		function result_fnames()
		{
				$fnames='';
				$i = 0;
				while ($i < mysql_num_fields($this->Query_ID)) {
					$meta = mysql_fetch_field($this->Query_ID, $i);
					if ($fnames) {
						$fnames .= ','.$meta->name;
					} else {
						$fnames .= $meta->name;
					}
					$i++;
				}
				
				return $fnames;
		}

    
    function next_record() 
    { 
        $this->Record = mysql_fetch_assoc($this->Query_ID); 
        $this->Row += 1; 
        $this->Errno = mysql_errno(); 
        $this->Error = mysql_error(); 
        $stat = is_array($this->Record); 
        
        if (!$stat) 
        { 
            mysql_free_result($this->Query_ID); 
            $this->Query_ID = 0; 
        } 
        return $this->Record; 
    } 
    
    function num_rows() 
    { 
        return mysql_num_rows($this->Query_ID); 
    }
    
    function affected_rows() 
    { 
        return mysql_affected_rows($this->Link_ID); 
    } 
    
    function optimize($tbl_name) 
    { 
        $this->connect(); 
        $this->Query_ID = @mysql_query("OPTIMIZE TABLE $tbl_name",$this->Link_ID); 
    } 
    
    function clean_results() 
    { 
        if($this->Query_ID != 0) mysql_freeresult($this->Query_ID); 
    } 
    
    function close() 
    { 
        if($this->Link_ID != 0) mysql_close($this->Link_ID); 
    } 
} 
?>