<?php require_once('includes/functions.inc.php');
require_once('includes/mysql.class.php');

// list of column names that contain encoded data
$encoded = array('title','fnm','mi','lnm','addr_1','addr_2','city','state','zipcode','phone','email','individual_type','specialty','prof_designation');

// get database settings from config file
$cwd = dirname($_SERVER['PHP_SELF']);

$conf = parse_config("$cwd/conf/settings.conf");

if (!function_exists("mysql_connect")) {
	echo "Error: Unable to connect to the drupal database\n";
	echo "MySQL support does not appear to be installed!\n";
}

$db = new MySQL();
if (!$db->connect($conf['DBHOST'], $conf['DBNAME'], $conf['DBUSER'], $conf['DBPASS'])) {
    echo "Error: Unable to connect to the drupal database>\n";
    echo "Check settings in settings.conf\n";
    exit(2);
};

print "Moving records from vx_users to correct site specific table \n";
$conscnt=0;
$hcpcnt=0;

$sql = "select * from `vx_users` order by vx_uid";

$result_arr=$db->build_results($sql);

if ($db->Error) {
  die($db->Error); 
}

// Loop through all records and insert into appropriate vx_users table
for ($i = 0; $i < sizeof($result_arr); ++$i)
{
  // Determine which table to insert into
  if ($result_arr[$i]['individual_type'] == 'C') {
    $table = 'incivek_vx_users';
    $conscnt++;
  } else if ($result_arr[$i]['individual_type'] == 'H') {
    $table = 'incivekhcp_vx_users';
    $hcpcnt++;
  } else {
    die("ERROR: Unrecognized individual_type for vx_uid=".$result_arr[$i]['vx_uid']." - ".$result_arr[$i]['individual_type']);
  }
  
  // Loop through columns and encode any values necessary
  foreach ($result_arr[$i] as $column => $value) {
    if (in_array($column, $encoded)) {
      // encode value
      $result_arr[$i][$column] = encodeStr($value);
    }
  }
    
  // Insert record
  $sql = 'insert into `'.$table.'` (vx_uid,title,fnm,mi,lnm,addr_1,addr_2,city,state,zipcode,phone,email,individual_type,specialty,prof_designation,update_date) values ('.$result_arr[$i]['vx_uid'].',\''.$result_arr[$i]['title'].'\',\''.$result_arr[$i]['fnm'].'\',\''.$result_arr[$i]['mi'].'\',\''.$result_arr[$i]['lnm'].'\',\''.$result_arr[$i]['addr_1'].'\',\''.$result_arr[$i]['addr_2'].'\',\''.$result_arr[$i]['city'].'\',\''.$result_arr[$i]['state'].'\',\''.$result_arr[$i]['zipcode'].'\',\''.$result_arr[$i]['phone'].'\',\''.$result_arr[$i]['email'].'\',\''.$result_arr[$i]['individual_type'].'\',\''.$result_arr[$i]['specialty'].'\',\''.$result_arr[$i]['prof_designation'].'\',\''.$result_arr[$i]['update_date'].'\')';
  
  if (!$db->query($sql))
  {
      die($db->Error);
  }  
}
print "Successfully moved ".$hcpcnt." HCP records\n";
print "Successfully moved ".$conscnt." Consumer records\n\n";


print "Moving HCP user profile values from vx_user_profile_values to correct site specific table \n";
$hcpcnt = 0;
$sql = "select * from `vx_user_profile_values` where vx_uid in (select vx_uid from incivekhcp_vx_users)";

$result_arr=$db->build_results($sql);

if ($db->Error) {
  die($db->Error); 
}

// Loop and insert values to incivekhcp_vx_user_profile_values
foreach ($result_arr as $record) {
  $sql = 'insert into incivekhcp_vx_user_profile_values (name,vx_uid,value) values (\''.encodeStr($record['name']).'\','.$record['vx_uid'].',\''.encodeStr($record['value']).'\')';
  
  if (!$db->query($sql))
  {
      die($db->Error);
  }
  $hcpcnt++;
}
print "Successfully moved ".$hcpcnt." HCP profile values\n\n";

print "Moving Consumer user profile values from vx_user_profile_values to correct site specific table \n";
$conscnt = 0;
$sql = "select * from `vx_user_profile_values` where vx_uid in (select vx_uid from incivek_vx_users)";

$result_arr=$db->build_results($sql);

if ($db->Error) {
  die($db->Error); 
}

// Loop and insert values to incivek_vx_user_profile_values
foreach ($result_arr as $record) {
  $sql = 'insert into incivek_vx_user_profile_values (name,vx_uid,value) values (\''.encodeStr($record['name']).'\','.$record['vx_uid'].',\''.encodeStr($record['value']).'\')';
  
  if (!$db->query($sql))
  {
      die($db->Error);
  }
  $conscnt++;
}
print "Successfully moved ".$conscnt." Consumer profile values\n\n";


print "Moving unsubscribe records from vx_unsubscribe to correct site specific table \n";
$cnt = 0;
$sql = "select * from `vx_unsubscribe`";

$result_arr=$db->build_results($sql);

if ($db->Error) {
  die($db->Error); 
}

// Loop and insert values to incivek_vx_unsubscribe and incivekhcp_vx_unsubscribe
foreach ($result_arr as $record) {
  // Loop through columns and encode any values necessary
  foreach ($record as $column => $value) {
    if (in_array($column, $encoded)) {
      // encode value
      $record[$column] = encodeStr($value);
    }
  }
    
  $sql = 'insert into incivek_vx_unsubscribe (vx_uid,fnm,mi,lnm,addr_1,addr_2,city,state,zipcode,email,update_date) values ('.$record['vx_uid'].',\''.$record['fnm'].'\',\''.$record['mi'].'\',\''.$record['lnm'].'\',\''.$record['addr_1'].'\',\''.$record['addr_2'].'\',\''.$record['city'].'\',\''.$record['state'].'\',\''.$record['zipcode'].'\',\''.$record['email'].'\',\''.$record['update_date'].'\')';
  
  if (!$db->query($sql))
  {
      die($db->Error);
  }
  
  $sql = 'insert into incivekhcp_vx_unsubscribe (vx_uid,fnm,mi,lnm,addr_1,addr_2,city,state,zipcode,email,update_date) values ('.$record['vx_uid'].',\''.$record['fnm'].'\',\''.$record['mi'].'\',\''.$record['lnm'].'\',\''.$record['addr_1'].'\',\''.$record['addr_2'].'\',\''.$record['city'].'\',\''.$record['state'].'\',\''.$record['zipcode'].'\',\''.$record['email'].'\',\''.$record['update_date'].'\')';
  
  if (!$db->query($sql))
  {
      die($db->Error);
  }
  
  $cnt++;
}
print "Successfully moved ".$cnt." unsubscribe records \n\n";


print "Moving unsubscribe opt-out records from vx_unsubscribe_opts to correct site specific table \n";
$cnt = 0;
$sql = "select * from `vx_unsubscribe_opts`";

$result_arr=$db->build_results($sql);

if ($db->Error) {
  die($db->Error); 
}

// Loop and insert values to incivek_vx_unsubscribe and incivekhcp_vx_unsubscribe
foreach ($result_arr as $record) {
    
  $sql = 'insert into incivek_vx_unsubscribe_opts (name,vx_uid,value) values (\''.encodeStr($record['name']).'\','.$record['vx_uid'].',\''.encodeStr($record['value']).'\')';
  
  if (!$db->query($sql))
  {
      die($db->Error);
  }
  
  $sql = 'insert into incivekhcp_vx_unsubscribe_opts (name,vx_uid,value) values (\''.encodeStr($record['name']).'\','.$record['vx_uid'].',\''.encodeStr($record['value']).'\')';
  
  if (!$db->query($sql))
  {
      die($db->Error);
  }
  
  $cnt++;
}
print "Successfully moved ".$cnt." unsubscribe opt-out records \n\n";

// Rename old v3 tables
print "Renaming old V3 Tables \n";
$sql = 'rename table vx_users to vx_users_v3';
if (!$db->query($sql)) { die($db->Error); }
$sql = 'rename table vx_user_profile_values to vx_user_profile_values_v3';
if (!$db->query($sql)) { die($db->Error); }
$sql = 'rename table vx_unsubscribe to vx_unsubscribe_v3';
if (!$db->query($sql)) { die($db->Error); }
$sql = 'rename table vx_unsubscribe_opts to vx_unsubscribe_opts_v3';
if (!$db->query($sql)) { die($db->Error); }


// Mark all records as extracted so they are not reprocessed
$tables = array ('incivek_vx_users','incivek_vx_unsubscribe','incivekhcp_vx_users','incivekhcp_vx_unsubscribe');

foreach ($tables as $table) {
  
  $sql = 'update '.$table.' set extract_flag=1';
  
  if (!$db->query($sql))
  {
      die($db->Error);
  }
}

print "Completed successfully\n\n";


?>