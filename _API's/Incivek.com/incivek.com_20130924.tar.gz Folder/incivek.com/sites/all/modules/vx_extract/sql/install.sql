DROP TABLE IF EXISTS `vx_extract_ctl`;
CREATE TABLE `vx_extract_ctl` (
  `name` varchar(50) NOT NULL,
  `value` varchar(100) NOT NULL
) ENGINE=MyISAM;

INSERT INTO `vx_extract_ctl` VALUES ('LAST_EXTRACT','1000-01-01 00:00:00');
INSERT INTO `vx_extract_ctl` VALUES ('GPG','/usr/bin/gpg');
INSERT INTO `vx_extract_ctl` VALUES ('EXTRACT_DIR','/var/www/webapp_extracts/incivek');
INSERT INTO `vx_extract_ctl` VALUES ('CONTACT_FILE_PREFIX','VRTX_IGN_TELA_CCONT_RMBR');
INSERT INTO `vx_extract_ctl` VALUES ('PROMOTION_FILE_PREFIX','VRTX_IGN_TELA_CPRSP_RMBR');
INSERT INTO `vx_extract_ctl` VALUES ('SURVEY_FILE_PREFIX','VRTX_IGN_TELA_CSRVY_RMBR');
INSERT INTO `vx_extract_ctl` VALUES ('HCP_CONTACT_FILE_PREFIX','VRTX_IGN_TELA_PCONT_RMBR');
INSERT INTO `vx_extract_ctl` VALUES ('HCP_PROMOTION_FILE_PREFIX','VRTX_IGN_TELA_PPRSP_RMBR');
INSERT INTO `vx_extract_ctl` VALUES ('HCP_SURVEY_FILE_PREFIX','VRTX_IGN_TELA_PSRVY_RMBR');
INSERT INTO `vx_extract_ctl` VALUES ('EPSILON_SFTP_HOST','aslan.epsilon.com');
INSERT INTO `vx_extract_ctl` VALUES ('EPSILON_SFTP_USER','vrtxignite');
INSERT INTO `vx_extract_ctl` VALUES ('EPSILON_SFTP_PASS','$3Tf!re2');
INSERT INTO `vx_extract_ctl` VALUES ('EPSILON_SFTP_CONS_DIR','/TELA/CONS/test/inbound');
INSERT INTO `vx_extract_ctl` VALUES ('EPSILON_SFTP_HCP_DIR','/TELA/HCP/test/inbound');


DROP TABLE IF EXISTS `vx_extract_srvy_resp_lkp`;
CREATE TABLE `vx_extract_srvy_resp_lkp` (
  `individual_type` varchar(1) NOT NULL,
  `question_name` varchar(50) NOT NULL,
  `answer_text`   varchar(200) NOT NULL,
  `question_text` varchar(200) NOT NULL,
  `question_id`   varchar(10) NOT NULL,
  `answer_id`   varchar(10) NOT NULL,
  `open_answer_ind`   varchar(10) NOT NULL,
  PRIMARY KEY  (`individual_type`,`question_name`,`answer_text`)
) ENGINE=MyISAM;

--
-- HCP VALUES
--

-- Branded Opt Questions
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','br_opt_out','1','Branded (All programs) Opt-Out','Q103','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','br_opt_out_date','open_answer','Branded (All programs) Opt-Out Date','Q104','A015','Y');

-- Corporate Opt Questions
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','vrtx_corp_opt_in','1','Vertex Corporate Opt-In','Q001','A001','N');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','vrtx_prgm_opt_in_status','1','Vertex Corporate Opt-In','Q097','A013','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','vrtx_prgm_opt_in_status','0','Vertex Corporate Opt-In','Q097','A014','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','vrtx_prgm_opt_in_status','','Vertex Corporate Opt-In','Q097','A014','N');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','vrtx_prgm_opt_in_date','open_answer','Vertex Corporate Opt-In Date','Q098','A015','Y');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','vrtx_prgm_opt_out_date','open_answer','Vertex Corporate Opt-Out Date','Q053','A015','Y');

-- Web Opt Questions
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','web_opt_out','1','Incivek.com Web Opt-Out','Q100','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('H','web_opt_out_date','open_answer','Incivek.com Web Opt-Out Date','Q096','A015','Y');

--
-- CONSUMER VALUES
--

-- Yes / No Profile Questions
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','age18','Yes','Are you 18 years old or older?','Q008','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','age18','No','Are you 18 years old or older?','Q008','A002','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','age18','','Are you 18 years old or older?','Q008','A021','N');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','hcv_status_reg','Yes','Have you been diagnosed with hepatitis C?','Q019','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','hcv_status_reg','No','Have you been diagnosed with hepatitis C?','Q019','A002','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','hcv_status_reg','Not Sure','Have you been diagnosed with hepatitis C?','Q019','A021','N');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','tvr_status_reg','Yes','Have you started or are you starting Telaprevir therapy?','Q073','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','tvr_status_reg','No','Have you started or are you starting Telaprevir therapy?','Q073','A002','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','tvr_status_reg','','Have you started or are you starting Telaprevir therapy?','Q073','A021','N');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','prev_trtd','Yes','Have you been treated for hepatitis C in the past?','Q022','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','prev_trtd','No','Have you been treated for hepatitis C in the past?','Q022','A002','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','prev_trtd','','Have you been treated for hepatitis C in the past?','Q022','A021','N');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','hcp_comfort_indicator','Yes','Are you comfortable asking your healthcare provider the questions you have about hepatitis C?','Q024','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','hcp_comfort_indicator','No','Are you comfortable asking your healthcare provider the questions you have about hepatitis C?','Q024','A002','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','hcp_comfort_indicator','','Are you comfortable asking your healthcare provider the questions you have about hepatitis C?','Q024','A021','N');

-- Open Answer Profile Questions
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','birth_year','open_answer','What is your year of birth?','Q095','A110','Y');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','birth_year','','What is your year of birth?','Q095','A021','Y');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','tvr_start_date','open_answer','Do you have a date set when you did or will start therapy? If yes what is the date?','Q075','A015','Y');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','tvr_start_date','','Do you have a date set when you did or will start therapy? If yes what is the date?','Q075','A002','Y');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','hcv_diag_date','open_answer','When did you start or when do you plan to start?','Q020','A024','Y');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','hcv_diag_date','','When did you start or when do you plan to start?','Q020','A021','Y');

-- Branded Opt Questions
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','bd1_opt_in','1','Branded Day1 Opt-In','Q056','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','bd1_opt_in','0','Branded Day1 Opt-In','Q056','A002','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','bd1_opt_in','','Branded Day1 Opt-In','Q056','A002','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','bd1_opt_in_date','open_answer','Branded Day1 Opt-In Date','Q057','A015','Y');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','br_opt_out','1','Branded (All programs) Opt-Out','Q103','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','br_opt_out_date','open_answer','Branded (All programs) Opt-Out Date','Q104','A015','Y');

-- Corporate Opt Questions
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','vrtx_prgm_opt_in_status','1','Vertex Corporate Opt-In','Q097','A013','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','vrtx_prgm_opt_in_status','0','Vertex Corporate Opt-In','Q097','A014','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','vrtx_prgm_opt_in_status','','Vertex Corporate Opt-In','Q097','A014','N');

INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','vrtx_prgm_opt_in_date','open_answer','Vertex Corporate Opt-In Date','Q098','A015','Y');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','vrtx_prgm_opt_out_date','open_answer','Vertex Corporate Opt-Out Date','Q053','A015','Y');

-- Web Opt Questions
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','web_opt_out','1','Incivek.com Web Opt-Out','Q100','A001','N');
INSERT INTO `vx_extract_srvy_resp_lkp` VALUES ('C','web_opt_out_date','open_answer','Incivek.com Web Opt-Out Date','Q096','A015','Y');



DROP TABLE IF EXISTS `vx_extract_specialty_resp_lkp`;
CREATE TABLE `vx_extract_specialty_resp_lkp` (
  `answer_text`   varchar(200) NOT NULL,
  `specialty_code`   varchar(10) NOT NULL,
  PRIMARY KEY  (`answer_text`)
) ENGINE=MyISAM;

INSERT INTO `vx_extract_specialty_resp_lkp` VALUES ('Hepatology','HEP');
INSERT INTO `vx_extract_specialty_resp_lkp` VALUES ('Gastroenterology','GAE');
INSERT INTO `vx_extract_specialty_resp_lkp` VALUES ('Infectious Disease','INF');
INSERT INTO `vx_extract_specialty_resp_lkp` VALUES ('Primary Care','PCA');
INSERT INTO `vx_extract_specialty_resp_lkp` VALUES ('Other Specialty','OTH');
	
