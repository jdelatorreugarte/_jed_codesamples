<?php
/*
* Incivek Epsilon Extract
* 
* Description: Generates customer contact, customer promotion, and customer survey extract files
* for the Consumer side of the Incivek.com website.  These files are then encrypted and uploaded 
* via SFTP to Epsilon for processing.
* 
* v2.1 - Stripped out all HCP handling to support site split
*
*/
$version = "2.1";

set_include_path(get_include_path() . PATH_SEPARATOR . dirname($_SERVER['PHP_SELF']).'/Net' . PATH_SEPARATOR . dirname($_SERVER['PHP_SELF']));
require_once('includes/functions.inc.php');
require_once('includes/mysql.class.php');
require_once('Net/SFTP.php');

// set default TZ
date_default_timezone_set('America/New_York');

// list of column names that contain encoded data
$encoded = array('title','fnm','mi','lnm','addr_1','addr_2','city','state','zipcode','phone','email','individual_type','specialty','prof_designation');

// get database settings from config file
$cwd = dirname($_SERVER['PHP_SELF']);

$conf = parse_config("$cwd/conf/settings.conf");

if (!function_exists("mysql_connect")) {
	echo "Error: Unable to connect to the drupal database\n";
	echo "MySQL support does not appear to be installed!\n";
}

$db = new MySQL();
if (!$db->connect($conf['DBHOST'], $conf['DBNAME'], $conf['DBUSER'], $conf['DBPASS'])) {
    echo "Error: Unable to connect to the drupal database>\n";
    echo "Check settings in settings.conf\n";
    exit(2);
};

// Retrieve config data from the database
$conf = array_merge($conf,get_cfg_data());

$last_extract = $conf['LAST_EXTRACT'];
echo "last_extract = $last_extract \n";

// Retrieve records to process
$users = get_vx_user_records();

// Unsubscribe records must be sent up twice.  Once in the HCP file and once in the Consumer file
$unsubscribe = get_vx_unsubscribe_records();

for($i = 0; $i < sizeof($unsubscribe); ++$i)
{
  $unsubscribe[$i]['unsubscribe']=1;
  $unsubscribe[$i]['uid']=$unsubscribe[$i]['vx_uid'];
  $unsubscribe[$i]['vx_uid'] = 'u'.strtolower($unsubscribe[$i]['individual_type']).$unsubscribe[$i]['uid'];
  $users[] = $unsubscribe[$i];
}

// Initialize variables
$i=1;
$ccont = array();
$cprsp = array();
$csrvy = array();

$pcont = array();
$pprsp = array();
$psrvy = array();

$ext_ids = array();
$extract_files = array();
$hcp_files = array();
$cons_files = array();


// -------------------------------------------------------------------
// Construct the Customer Contact File in Epsilon's hardcoded format
// -------------------------------------------------------------------
foreach ($users as $user) {
	// Initialize empty array to hold field values
	$fields = array();

	$fields[] = $user['vx_uid'];  //External ID (vx_uid)
	
	//Source Code (Hardcoded to WEB05 for consumers and WEB04 for HCP per Epsilon DED)
	$src_code = ($user['individual_type'] == 'H')?'WEB04':'WEB05';
	$fields[] = $src_code;
	
	$fields[] = '';  //Epsilon Individual ID (Leave null)
	$fields[] = '';  //Customer Master ID (Leave null)
	$fields[] = $user['transaction_id'].$user['vx_uid'];  //Transaction ID (Update_date plus vx_uid)
	$fields[] = (isset($user['individual_type']))?$user['individual_type']:'';  //Individual type code (individual_type)
	$fields[] = '';  //Full Name (Leave Null)
	$fields[] = (isset($user['title']))?$user['title']:'';  //Name Prefix (title)
	$fields[] = (isset($user['fnm']))?$user['fnm']:'';  //First Name (FNM)
	$fields[] = (isset($user['mi']))?$user['mi']:'';  //Middle Name (MI)
	$fields[] = (isset($user['lnm']))?$user['lnm']:'';  //Last Name (LNM)
	$fields[] = '';  //Name suffix (Leave Null)
	$fields[] = (isset($user['prof_designation']))?$user['prof_designation']:'';  //Professional Designation (prof_designation)
	$fields[] = '';  //Birth Date (Leave null)
	$fields[] = '';  //Age (Leave Null)
	$fields[] = 'U';  //Gender Code (Hardcoded to U)
	$fields[] = '';  //Language Preference (Leave Null)
	$fields[] = '';  //Price Sensitivity Flag (Leave Null)
	$fields[] = '';  //Patient Assistance Flag (Leave Null)
	$fields[] = '';  //Prescriber Indicator (Leave Null)
	$fields[] = '';  //Provider Type Code (Leave Null)
	$fields[] = '';  //Provider SubType Code (Leave Null)
	$fields[] = '';  //Rep Name (Leave Null)
	$fields[] = '';  //Individual Status (Leave Null)
	$fields[] = '';  //Primary Telephone Type (Leave Null)
	$fields[] = (isset($user['phone']))?$user['phone']:'';  //Primary Telephone Number (phone)
	$fields[] = '';  //Secondary Telephone Type (Leave Null)
	$fields[] = '';  //Secondary Telephone Number (Leave Null)
	$fields[] = '';  //Fax Number (Leave Null)
	$fields[] = '';  //Email Address Type (Leave Null)
	$fields[] = (isset($user['email']))?$user['email']:'';  //Email Address (email)
	$fields[] = '';  //Email Format Preference (Leave Null)
	$fields[] = 'ORIG';  //Email Source (Hardcoded to ORIG)
	$fields[] = '';  //Practice Name (Leave Null)
	$fields[] = '';  //Practice Type (Leave Null)
	$fields[] = '';  //Agility Key (Leave Null)
	$fields[] = '';  //Address Type (Leave Null)
	$fields[] = (isset($user['addr_1']))?$user['addr_1']:'';  //Address 1 (addr_1)
	$fields[] = (isset($user['addr_2']))?$user['addr_2']:'';  //Address 2 (addr_2)
	$fields[] = (isset($user['city']))?$user['city']:'';  //City (city)
	$fields[] = (isset($user['state']))?$user['state']:'';  //Sate (state)
	
	//Zip Code Base (zipcode)
	unset($zip);
	unset($suffix);
	
	if (strpos($user['zipcode'],"-")) {
	  list($zip,$suffix) = explode("-", $user['zipcode'], 2);  
	} else {
	  $zip = $user['zipcode'];
	  $suffix = '';
	}
		
	$fields[] = $zip;   //Zip Code Base (zipcode)
	$fields[] = $suffix;   //Zip Code Suffix (zipcode suffix)
		
	$fields[] = '';  //Country Name (Leave Null)
	$fields[] = '';  //Seasonal Start Date (Leave Null)
	$fields[] = '';  //Seasonal End Date (Leave Null)
	$fields[] = '';  //Address Quality Code (Leave Null)
	$fields[] = '';  //Address Error Code (Leave Null)
	$fields[] = '';  //Address Source Code (Leave Null)
	$fields[] = '';  //Sample Eligibility Flag (Leave Null)
	$fields[] = (isset($user['specialty']))?translate_specialty($user['specialty']):'';  //Specialty Code (specialty)
	$fields[] = '';  //SpecialtyGroup (Leave Null)
	$fields[] = '';  //SpecialtySubGroup (Leave Null)
	$fields[] = '';  //SpecialtyAltSubGroup (Leave Null)
	$fields[] = '';  //ME Identifier Code (Leave Null)
	$fields[] = '';  //Medical Education # (Leave Null)
	$fields[] = '';  //IMS Identifier Code (Leave Null)
	$fields[] = '';  //IMSDR # (Leave Null)
	$fields[] = '';  //DEA Identifier Code (Leave Null)
	$fields[] = '';  //DEA# (Leave Null)
	$fields[] = '';  //DEA State (Leave Null)
	$fields[] = '';  //DEA Status (Leave Null)
	$fields[] = '';  //State License Identifier Code (Leave Null)
	$fields[] = '';  //State License Number (Leave Null)
	$fields[] = '';  //License State (Leave Null)
	$fields[] = '';  //State License Status (Leave Null)
	$fields[] = '';  //State License Effective Date (Leave Null)
	$fields[] = '';  //State License Expire Date (Leave Null)
	$fields[] = '';  //Suppression Code (Leave Null)
	$fields[] = '';  //Suppression Active Flag (Leave Null)
	$fields[] = '';  //Minnesota License Check Box (Leave Null)
	$fields[] = '';  //Degree (Leave Null)
	$fields[] = '';  //List_cd (Leave Null)
	$fields[] = (isset($user['update_date']))?$user['update_date']:'';  //Record Date (Update Date in YYYYMMDD HH24:MI:SS)
	
	if ($user['individual_type'] == 'H') {
      $pcont[] = implode('|',$fields);
	} else {
	  $ccont[] = implode('|',$fields);
	}
	
	$i++;
}

// Write out the consumer customer contact extract file
$timestamp = date('YmdHis');
$fname = $conf['EXTRACT_DIR'].'/'.$conf['CONTACT_FILE_PREFIX'].'_'.$timestamp.'_'.count($ccont).'.txt';
$fcontent = implode(PHP_EOL,$ccont);
$fcontent .= PHP_EOL;

echo "Writing file $fname\n";
file_put_contents($fname, $fcontent);
$extract_files[] = $fname;
$cons_files[] = $fname;



// -------------------------------------------------------------------
// Construct the Customer Promotion File in Epsilon's hardcoded format
// -------------------------------------------------------------------
foreach ($users as $user) {
	// Initialize empty array to hold field values
	$fields = array();

	$fields[] = $user['vx_uid'];  //External ID (vx_uid)

	//Source Code (Hardcoded to WEB05 for consumers and WEB04 for HCP per Epsilon DED)
	$src_code = ($user['individual_type'] == 'H')?'WEB04':'WEB05';
	$fields[] = $src_code;
	
	$fields[] = '';  //Epsilon Individual ID (Leave null)
	$fields[] = '';  //Customer Master ID (Leave null)
	$fields[] = $user['transaction_id'].$user['vx_uid'];  //Transaction ID (Update_date plus vx_uid)
	$fields[] = (isset($user['update_date']))?$user['update_date']:'';  //Response Date (Update Date in YYYYMMDD HH24:MI:SS)

	//Response Source Code (Hardcoded to WEB05 for consumers and WEB04 for HCP per Epsilon DED)
	$resp_src_code = ($user['individual_type'] == 'H')?'WEB04':'WEB05';
	$fields[] = $resp_src_code;
	
	$fields[] = '';  //Treatment Code (Leave null)
	$fields[] = '';  //Media Code (Leave null per DED)
	$fields[] = '';  //Document carrier (Leave null)
	$fields[] = '';  //Documeng Group (Leave null)
	$fields[] = '';  //Document Number (Leave null)
	$fields[] = '';  //Document Amount (Leave null)
	$fields[] = 'Ignite';  //Vendor Name (Hardcoded to Ignite)
	$fields[] = '';  //Batch Number (Leave null)
	$fields[] = '';  //Session ID(Leave null)
	$fields[] = '';  //Requester IP Address (Leave null)
	$fields[] = '';  //Service Request ID (Leave null)
	$fields[] = '';  //Call Start Time (Leave null)
	$fields[] = '';  //Call End Time (Leave null)
	$fields[] = '';  //Phone 800 Number (Leave null)
	$fields[] = '';  //Web Action Code (Leave null)
	$fields[] = '';  //Requested Quantity (Leave null)

	if ($user['individual_type'] == 'H') {
	  $pprsp[] = implode('|',$fields);
	} else {
	  $cprsp[] = implode('|',$fields);
	}
	
	$i++;
}

// Write out the consumer customer promotion extract file
$timestamp = date('YmdHis');
$fname = $conf['EXTRACT_DIR'].'/'.$conf['PROMOTION_FILE_PREFIX'].'_'.$timestamp.'_'.count($cprsp).'.txt';
$fcontent = implode(PHP_EOL,$cprsp);
$fcontent .= PHP_EOL;

echo "Writing file $fname\n";
file_put_contents($fname, $fcontent);
$extract_files[] = $fname;
$cons_files[] = $fname;


// --------------------------------------------------------------------------------
// Construct the Customer Survey Response File in Epsilons hardcoded format
// --------------------------------------------------------------------------------
$open_ans_questions = get_open_ans_questions();

$i=1;
foreach ($users as $user) {
	if (isset($user['unsubscribe'])) {
	  $userprofile = get_unsubscribe_opts($user['uid'],$user['individual_type']);	  
	} else {
      $userprofile = get_user_profile_values($user['vx_uid'],$user['individual_type']);   
	}

	// Profile questions
	foreach (array_keys($userprofile) as $question) {
	    //Initialize variables
	    $open_answer='';
	    $question_id='';
	    $answer_id='';
	    
		$answer = $userprofile[$question];
		
		// Initialize empty array to hold field values
		$fields = array();
		
		// Translate this question
		if (isset($open_ans_questions[$question])) {
		    // We have an open answer question to handle
			// See if there is an exact match for this question / answer in vx_extract_srvy_resp_lkp 
			list($question_id,$answer_id) = translate_question_answer($user['individual_type'],$question,$answer);
			$open_answer=$answer;
			
			// If no exact match was found retrieve the default translation for this question
			if (!$question_id) {
				list($question_id,$answer_id) = translate_question_answer($user['individual_type'],$question,'open_answer');
				$open_answer=$answer;
			}
		} else {
		    // This is not an open answer question
			list($question_id,$answer_id) = translate_question_answer($user['individual_type'],$question,$answer);
		}
		
		// If unable to translate some Q&A pairs via vx_extract_srvy_resp_lkp throw error and exit
		if (!$question_id || !$answer_id) {
			echo "ERROR: Unable to translate Q&A Pair via the vx_extract_srvy_resp_lkp table\n";
			echo "DEBUG: Individual Type = ".$user['individual_type']." / Question = ".$question." / Answer = ".$answer."\n";
			die;
		}
				
		$fields[] = $user['vx_uid'];  //External ID (vx_uid)

    	//Source Code (Hardcoded to WEB05 for consumers and WEB04 for HCP per Epsilon DED)
    	$src_code = ($user['individual_type'] == 'H')?'WEB04':'WEB05';
    	$fields[] = $src_code;
	    
	    $fields[] = '';  //Epsilon Individual ID (Leave null)
		$fields[] = '';  //Customer Master ID (Leave null)
		$fields[] = $user['transaction_id'].$user['vx_uid'];  //Transaction ID (Update_date plus vx_uid)
		$fields[] = (isset($user['update_date']))?$user['update_date']:'';  //Response Date (Update Date in YYYYMMDD HH24:MI:SS)

    	//Response Source Code (Hardcoded to WEB05 for consumers and WEB04 for HCP per Epsilon DED)
    	$resp_src_code = ($user['individual_type'] == 'H')?'WEB04':'WEB05';
    	$fields[] = $resp_src_code;

    	//Survey Code (Hardcoded to SRVY02 for consumers and SRVY01 for HCP per Epsilon DED)
    	$survey_code = ($user['individual_type'] == 'H')?'SRVY01':'SRVY02';
    	$fields[] = $survey_code;

		$fields[] = $question_id;  //External Question ID (Translated from vx_extract_srvy_resp_lkp)
		$fields[] = $answer_id;  //External Answer ID (Translated from vx_extract_srvy_resp_lkp)
		$fields[] = $open_answer;  //Open Answer Text
		$fields[] = '';  //Dependant External ID (Leave null)
		$fields[] = '';  //Dependant Source Code(Leave null)
		$fields[] = '';  //Dependant ID (Leave null)
		
		if ($user['individual_type'] == 'H') {
		  $psrvy[] = implode('|',$fields);
		} else {
		  $csrvy[] = implode('|',$fields);
		}
		$i++;
	}
}

// Write out the consumer customer survey extract file
$timestamp = date('YmdHis');
$fname = $conf['EXTRACT_DIR'].'/'.$conf['SURVEY_FILE_PREFIX'].'_'.$timestamp.'_'.count($csrvy).'.txt';
$fcontent = implode(PHP_EOL,$csrvy);
$fcontent .= PHP_EOL;

echo "Writing file $fname\n";
file_put_contents($fname, $fcontent);
$extract_files[] = $fname;
$cons_files[] = $fname;

// Encrypt files
foreach ($extract_files as $file) {
  exec($conf['GPG'].' --encrypt --recipient \'narnia.epsilon.com\' -o '.$file.'.pgp '.$file,$output,$rc);
  
  // Check return code and remove unencrypted source file if successful
  if ($rc == 0) {
    echo "Encrypted file ".$file.".pgp\n";
    //unlink($file);
  } else {
	echo "ERROR: Unable to encrypt file $file\n";
	die;
  }  
}

// Set last extract date to NOW()
echo "Updating last extract date \n";
set_last_extract();

// Copy files over to Epsilon
echo "Connecting to host ".$conf['EPSILON_SFTP_HOST']." as ".$conf['EPSILON_SFTP_USER']." \n";
$sftp = new Net_SFTP($conf['EPSILON_SFTP_HOST']);

if (!$sftp->login($conf['EPSILON_SFTP_USER'], $conf['EPSILON_SFTP_PASS'])) {
  echo "ERROR: SFTP Login Failed";
  exit(2);
}

$sftp->chdir($conf['EPSILON_SFTP_CONS_DIR']);

foreach ($cons_files as $full) {
  $short = basename($full);
  print "Uploading file ".$conf['EPSILON_SFTP_CONS_DIR'].'/'.$short.".pgp\n";
  if (!$sftp->put($short.'.pgp',$full.'.pgp', NET_SFTP_LOCAL_FILE)) {
    echo "ERROR: SFTP file transfer failed\n";
    exit(2);
  }
}


// Successfully transmitted records to Epsilon.  Flag these records as complete
echo "Marking records as successfully extracted.\n";
flag_records_as_extracted();


?>
