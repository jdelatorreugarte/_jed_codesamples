<?php $class = (isset($class))?$class:''; ?>
<div class="tout <?php print $class; ?>">
  <div class="tout-top"></div>
  <div class="tout-body">
    <?php print $body; ?>
  </div>
  <div class="tout-bottom"></div>
</div>
