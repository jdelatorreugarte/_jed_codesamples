<?php
/**
 * VX_Touts module.  Displays appropriate touts menu based upon the node being displayed
 * associated with the currently viewed node.
 */

/**
 * Implementation of hook_menu().
 */
function vx_touts_menu() {
  $items['admin/config/system/vx_touts'] = array(
    'title' => 'Incivek Touts',
    'description' => 'Control settings for the Touts module.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vx_touts_admin_settings'),
    'access arguments' => array('administer vx_touts'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Configuration page settings definitions corresponding with hook_menu
 */
function vx_touts_admin_settings() {
  $form = array();

  $form['vx_touts_home_nid'] = array(
    '#type' => 'textfield',
	'#title' => t('Home Page Node ID'),
    '#default_value' => variable_get('vx_touts_home_nid', '0'),
    '#size' => 10,
    '#maxlength' => 10,
    '#description' => t("Specify the node ID of the home page that contains associated touts that you would like displayed on the landing page."),
    '#required' => TRUE,  );

  return system_settings_form($form);
}

/**
 * Implements hook_block_info to define blocks created by this module
 */
function vx_touts_block_info() {
  $blocks['touts'] = array(
    'info' => t('Incivek Touts'), 
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );
  
  $blocks['isitout'] = array(
    'info' => t('Incivek ISI Tout'), 
    'cache' => DRUPAL_CACHE_PER_PAGE,
  );

  return $blocks;
}

/**
 * Implements hook_block_view to create blocks created by this module
 */
function vx_touts_block_view($delta = '') {
  $block = array();
  $touts = array();

  if ( arg(0) == 'node' || drupal_is_front_page()) {
    if (drupal_is_front_page()) {
      // Retrieve the node id of the home page from configuration
      $id = variable_get("vx_touts_home_nid",'');
      $node = node_load($id);
    } else if (is_numeric(arg(1))) {
      // Retrieve the current node being displayed
      $id = arg(1);
      $node = node_load($id);
    }
  } else {
    //No node being displayed
    return;
  }
  
  switch ($delta) {
    case 'touts':
        if (isset($node) && isset($node->language)) {
          $references = isset($node->field_touts[$node->language])?$node->field_touts[$node->language]:array();
          $max = count($references);
          
          // Retrieve touts associated with this page
          for ($i = 0; $i < $max; $i++) {
            $id=$references[$i]['nid'];
      
            if ($id) {
              $touts[]=$id;
            }
          }
        }
        
        // If any touts were found associated with this page retrieve their contents
        if (!empty($touts)) {
          // Loop through touts and build block content
          for ($i=0; $i < sizeof($touts); ) {
            $tout = $touts[$i];
            $node = node_load($tout);
            
            // Properly set tout class
            $class = 'tout ';
            
            if ($i === 0) {
              $class .= 'first';
            } elseif ($i === (sizeof($touts)-1)) {
              $class .= 'last';
            }
            
            $vars = array(
              'body' => $node->body[$node->language][0]['value'],
              'class' => $class
            );
            
            $block['content'][] = array('#markup' => theme('tout', array('vars' => $vars)));
            $i++;
          }
      } else {
        // No node or user page is currently being displayed
        $block['content']='';
      }
    break;
    case 'isitout':
      if (isset($node->language) && isset($node->field_isitout[$node->language][0])) {
        $vars = array(
          'body' => $node->field_isitout[$node->language][0]['value'],
        );
        
        $block['content'][] = array('#markup' => theme('isitout', array('vars' => $vars)));
      } else {
        // No ISI tout found to render
        $block['content']='';
      }
      
    break;
  }
  
  
  return $block;
}

/**
 * Implements hook_permission
 * @see http://api.drupal.org/api/drupal/modules--system--system.api.php/function/hook_permission/7
 */
function vx_touts_permission() {
  return array(
    'administer vx_touts' => array(
      'title' => t('Administer Incivek Touts'), 
    ),
  );
}


/**
 * Implements hook_theme().
 */

function vx_touts_theme() {
  return array(
    'tout' => array(
      'variables' => array('vars' => NULL),
      'template' => 'tout'
    ),
    'isitout' => array(
      'variables' => array('vars' => NULL),
      'template' => 'isitout'
    ),
  );
}


function template_preprocess_tout(&$variables) {
  $vars = $variables['vars'];
  foreach ($vars as $key => $value) {
    $variables[$key] = $value;
  }
}

function template_preprocess_isitout(&$variables) {
  $vars = $variables['vars'];
  foreach ($vars as $key => $value) {
    $variables[$key] = $value;
  }
}

