jQuery(document).ready(function(){

	// Watermarks
	jQuery('#edit-birth-year').watermark('YYYY', 'watermark');
	jQuery('#edit-tvr-start-date').watermark('MM/DD/YYYY', 'watermark');
	jQuery('#edit-hcv-diag-date').watermark('YYYY', 'watermark');
    
	// Patient registration form event handlers
	//Bound to click event for proper IE compatibility
    jQuery('#edit-age18-yes').click(function () {
    	jQuery('#form-not-age18').hide();
    	jQuery('#form-hidden').slideDown();
    });
    
    jQuery('#edit-age18-no').click(function () {
    	jQuery('#form-hidden').hide();
    	jQuery('#form-not-age18').show();
    });

    if (jQuery('#edit-age18-yes').is(':checked'))
    	jQuery('#form-hidden').show();
    
    if (jQuery('#edit-age18-no').is(':checked'))
    	jQuery('#form-not-age18').show();
    
    jQuery('input#edit-tvr-status-reg-yes').click(function () {
    	jQuery('div.form-item-tvr-start-date').slideDown();
    });

    jQuery('input#edit-tvr-status-reg-no').click(function () {
    	jQuery('div.form-item-tvr-start-date').slideUp();
    	jQuery('input#edit-tvr-start-date').val('');
    });
    
    if (jQuery('input#edit-tvr-status-reg-yes').is(':checked'))
    	jQuery('div.form-item-tvr-start-date').show();
    
    jQuery('input#edit-hcpsubmit').click(function (e) {
        e.preventDefault();
    	jQuery('#toHCPInterstitial').jqmShow();
    	createCookie("siteCookie", 'register/hcp');
    });

    jQuery('input#edit-hcv-status-reg-yes').click(function () {
    	jQuery('#hcv-yes-follow-up-wrapper').slideDown();
    });

    jQuery('input#edit-hcv-status-reg-no').click(function () {
    	jQuery('#hcv-yes-follow-up-wrapper').slideUp();
    	jQuery('input#edit-tvr-start-date').val('');
    	jQuery('input#edit-hcv-diag-date').val('');
    	jQuery('input#edit-tvr-status-reg-yes').attr('checked', false);
    	jQuery('input#edit-tvr-status-reg-no').attr('checked', false);
    	jQuery('input#edit-prev-trtd-yes').attr('checked', false);
    	jQuery('input#edit-prev-trtd-no').attr('checked', false);
    	jQuery('div.form-item-tvr-start-date').hide();
    });
    
    jQuery('input#edit-hcv-status-reg-not-sure').click(function () {
    	jQuery('#hcv-yes-follow-up-wrapper').slideUp();
    	jQuery('input#edit-tvr-start-date').val('');
    	jQuery('input#edit-hcv-diag-date').val('');
    	jQuery('input#edit-tvr-status-reg-yes').attr('checked', false);
    	jQuery('input#edit-tvr-status-reg-no').attr('checked', false);
    	jQuery('input#edit-prev-trtd-yes').attr('checked', false);
    	jQuery('input#edit-prev-trtd-no').attr('checked', false);
    	jQuery('div.form-item-tvr-start-date').hide();
    });
    
    if (jQuery('input#edit-hcv-status-reg-yes').is(':checked'))
    	jQuery('#hcv-yes-follow-up-wrapper').show();

    
});
