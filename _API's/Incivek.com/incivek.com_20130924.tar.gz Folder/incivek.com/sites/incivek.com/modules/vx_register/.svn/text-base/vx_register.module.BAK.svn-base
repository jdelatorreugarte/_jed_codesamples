<?php

/**
 * Implements hook_permission
 * @see http://api.drupal.org/api/drupal/modules--system--system.api.php/function/hook_permission/7
 */
function vx_register_permission() {
  return array(
    'administer vx_register' => array(
      'title' => 'Administer Incivek User Registration', 
    ),
  );
}

/**
 * Implementation of hook_menu().
 */
function vx_register_menu() {
    
  $items['admin/config/system/vx_register'] = array(
    'title' => 'Incivek Registration',
    'description' => 'Control Incivek Registration Module settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vx_register_admin_settings'),
    'access arguments' => array('administer vx_register'),
    'type' => MENU_NORMAL_ITEM,
  );

  $items['register'] = array(
    'title' => 'Register for Updates',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vx_register_dtc_form'),
    'access callback' => TRUE,
    'type' => MENU_VISIBLE_IN_TREE,
  );
  
  $items['register/unsubscribe'] = array(
    'title' => 'Unsubscribe',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('vx_register_unsubscribe_form'),
    'access callback' => TRUE,
    'type' => MENU_NORMAL_ITEM,
  );
     
  return $items;
}

/**
 * Configuration page settings definitions corresponding with hook_menu
 */
function vx_register_admin_settings() {
  $form = array();
  
  $form['vx_register_header'] = array(
    '#type' => 'textarea',
	'#title' => t('Registration Page Header HTML'),
    '#default_value' => variable_get('vx_register_header', ''),
    '#cols' => 60,
    '#rows' => 8,
    '#description' => t("Enter a custom HTML header displayed on the registration pages."),
    '#required' => TRUE,  );

  $form['vx_register_footer'] = array(
    '#type' => 'textarea',
	'#title' => t('Registration Page Footer HTML'),
    '#default_value' => variable_get('vx_register_footer', ''),
    '#cols' => 60,
    '#rows' => 8,
    '#description' => t("Enter a custom HTML footer displayed on the registration pages."),
    '#required' => TRUE,  );
  
  $form['vx_register_thankyou_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Thank You For Registering Page URL',
    '#default_value' => variable_get('vx_register_thankyou_url', '<front>'),
    '#size' => 60,
    '#maxlength' => 80,
    '#description' => 'Whenever a user registers with the site they will be redirected to this thank you page following registration.',
    '#required' => TRUE,
  );
  
  $form['vx_register_unsubscribe_thankyou_url'] = array(
    '#type' => 'textfield',
    '#title' => 'Unsubscribe Thank You URL',
    '#default_value' => variable_get('vx_register_unsubscribe_thankyou_url', '<front>'),
    '#size' => 60,
    '#maxlength' => 80,
    '#description' => 'Whenever a user unsubscribes via the website they will be redirected to this thank you page after their settings are saved.',
    '#required' => TRUE,
  );

  return system_settings_form($form);
}


/**
 * Form builder; DTC user registration
 *
 * @ingroup forms
 * @see vx_register_dtc_form_submit()
 * @see vx_register_dtc_form_validate()
 */
function vx_register_dtc_form($form, &$form_state) {
  // Set pageName & eVar1 per tracking manifest
  $_SESSION['evars']['eVar1']=t('Sign up for support');
  $_SESSION['evars']['pageName']=t('Sign up for support');
  
  if (isset($form_state['storage']['values']['segmentdtc'])) {
    //Display DTC registration form
    
    $form['formcontent']['#markup'] = '<div id="form-content"><div style="height:25px;"></div>';
    $form['header']['#markup'] = '<h2>Sign up for support</h2><p>Register here to get the latest news on INCIVEK and hepatitis C treatment.  Your mailing or email address is required for registration.</p>';
    $form['reqtext']['#markup'] = '<p class="required">*required field</p>';
    $form['age18'] = array (
  		'#type' => 'radios', 
  		'#title' => 'Are you 18 years old or older?', 
  		'#options' => array ('Yes' => 'Yes', 'No' => 'No')
	);
	$form['clear1']['#markup']='<div class="clear"></div>';
	$form['regnotage18']['#markup'] = '<div id="form-not-age18"><p>Thank you for your interest, but only adults 18 or older may register.</p><a id="returnhome" href="/"></a></div>';
    $form['formhidden']['#markup'] = '<div id="form-hidden">';
    $form['birth_year'] = array (
      '#type' => 'textfield',
      '#title' => 'What is your year of birth? (must be 18 or older to register)<span class="form-required">*</span>',
      '#size' => 15,
      '#maxlength' => 4,
      '#required' => 0
    );
    $form['fnm'] = array (
      '#type' => 'textfield',
      '#title' => 'First Name',
      '#maxlength' => 100,
      '#size' => 20,
      '#required' => 1
    );
    $form['mi'] = array (
      '#type' => 'textfield',
      '#title' => 'Middle Name',
      '#maxlength' => 50,
      '#size' => 20,
      '#required' => 0
    );
    $form['lnm'] = array (
      '#type' => 'textfield',
      '#title' => 'Last Name',
      '#maxlength' => 60,
      '#size' => 20,
      '#required' => 1
    );
    $form['clear2']['#markup']='<div class="clear"></div>';
    $form['addr_1'] = array (
      '#type' => 'textfield',
      '#title' => 'Street Address 1',
      '#maxlength' => 255,
      '#size' => 40,
      '#required' => 1
    );
    $form['addr_2'] = array (
      '#type' => 'textfield',
      '#title' => 'Street Address 2',
      '#maxlength' => 255,
      '#size' => 40,
      '#required' => 0
    );
    $form['city'] = array (
      '#type' => 'textfield',
      '#title' => 'City',
      '#maxlength' => 60,
      '#size' => 20,
      '#required' => 1
    );
    $form['state'] = array (
      '#type' => 'select',
      '#title' => 'State',
      '#options' => array (
        '--' => '--',
        'AK' => 'AK',
        'AL' => 'AL',
        'AR' => 'AR',
        'AZ' => 'AZ',
        'CA' => 'CA',
        'CO' => 'CO',
        'CT' => 'CT',
        'DC' => 'DC',
        'DE' => 'DE',
        'FL' => 'FL',
        'GA' => 'GA',
        'HI' => 'HI',
        'IA' => 'IA',
        'ID' => 'ID',
        'IL' => 'IL',
        'IN' => 'IN',
        'KS' => 'KS',
        'KY' => 'KY',
        'LA' => 'LA',
        'MA' => 'MA',
        'MD' => 'MD',
        'ME' => 'ME',
        'MI' => 'MI',
        'MN' => 'MN',
        'MO' => 'MO',
        'MS' => 'MS',
        'MT' => 'MT',
        'NC' => 'NC',
        'ND' => 'ND',
        'NE' => 'NE',
        'NH' => 'NH',
        'NJ' => 'NJ',
        'NM' => 'NM',
        'NV' => 'NV',
        'NY' => 'NY',
        'OH' => 'OH',
        'OK' => 'OK',
        'OR' => 'OR',
        'PA' => 'PA',
        'RI' => 'RI',
        'SC' => 'SC',
        'SD' => 'SD',
        'TN' => 'TN',
        'TX' => 'TX',
        'UT' => 'UT',
        'VA' => 'VA',
        'VT' => 'VT',
        'WA' => 'WA',
        'WI' => 'WI',
        'WV' => 'WV',
        'WY' => 'WY',
      ),  
      '#required' => 1,
      '#default_value' => '--'
    );
    $form['zipcode'] = array (
      '#type' => 'textfield',
      '#title' => 'Zip Code',
      '#maxlength' => 12,
      '#size' => 18,
      '#required' => 1
    );
    $form['clear3']['#markup']='<div class="clear"></div>';
    $form['mail'] = array (
      '#type' => 'textfield',
      '#title' => 'Email Address',
      '#maxlength' => 100,
      '#size' => 22,
      '#required' => 1
    );
    $form['conf_mail'] = array (
      '#type' => 'textfield',
      '#title' => 'Confirm Email Address',
      '#maxlength' => 100,
      '#size' => 22,
      '#required' => 1
    );
    $form['clear4']['#markup']='<div class="clear"></div>';
    $form['hcp_comfort_indicator'] = array (
      '#type' => 'radios',
      '#title' => 'Are you comfortable asking your healthcare provider the questions you have about hepatitis C?<span class="form-required">*</span>',
      '#options' => array ( 'Yes' => 'Yes', 'No' => 'No' ),  
      '#required' => 0
    );
    $form['clear5']['#markup']='<div class="clear"></div>';
    $form['hcv_status_reg'] = array (
      '#type' => 'radios',
      '#title' => 'Have you been diagnosed with hepatitis C?<span class="form-required">*</span>',
      '#options' => array ( 'Yes' => 'Yes', 'No' => 'No', 'Not Sure' => 'Not Sure'),  
      '#required' => 0
    );
    $form['clear6']['#markup']='<div class="clear"></div>';
    $form['hcv_yes_follow_up1']['#markup'] = '<div id="hcv-yes-follow-up-wrapper">';
    $form['hcv_diag_date'] = array (
      '#type' => 'textfield',
      '#title' => 'In what year were you diagnosed?',
      '#maxlength' => 255,
      '#required' => 0,
      '#size' => 20
    );
    $form['prev_trtd'] = array (
      '#type' => 'radios',
      '#title' => 'Have you been treated for hepatitis C in the past?<span class="form-required">*</span>',
      '#options' => array ('Yes' => 'Yes', 'No' => 'No'),
      '#required' => 0
    );
    $form['clear7']['#markup']='<div class="clear"></div>';
    $form['tvr_status_reg'] = array (
      '#type' => 'radios',
      '#title' => 'Have you and your healthcare provider decided to start INCIVEK Triple Therapy?<span class="form-required">*</span>',
      '#options' => array ('Yes' => 'Yes', 'No' => 'No'),
      '#required' => 0
    );
    $form['clear8']['#markup']='<div class="clear"></div>';
    $form['tvr_start_date'] = array (
      '#type' => 'textfield',
      '#title' => 'When did you start or when do you plan to start?',
      '#maxlength' => 255,
      '#required' => 0,
      '#size' => 20
    );
    $form['hcv_yes_follow_up2']['#markup'] = '</div>';
    $form['optin']['#markup'] = '<div class="form-optin"><h4>OPT-IN AGREEMENT</h4><p>By clicking the "Register" button, you agree to allow Vertex Pharmaceuticals Incorporated and companies working on its behalf to send you information about health and wellness offerings, Vertex products, and services that may be available to you. Vertex may also invite you to participate in surveys and other market research.</p><p>Vertex respects your privacy. Your information will never be sold or rented and you can stop these communications at any time. To learn how Vertex will use your information, read our <a href="http://www.vrtx.com/privacy-policy" target="_blank">Privacy Policy</a>.</p></div>';

    $form['submit'] = array ( '#type' => 'submit');
    $form['formclose']['#markup'] = '</div></div>';
    $form['#attributes']['enctype'] = 'multipart/form-data';
    $form['#submit'] = array('vx_register_dtc_form_submit');
    $form['vrtx_prgm_opt_in_status'] = array ( '#type' => 'hidden', '#value' => 1 );
    $form['bd1_opt_in'] = array ( '#type' => 'hidden', '#value' => 1);
    
  } else {
    $form['top']['#markup'] = variable_get('vx_register_header', '<!-- Header -->');
    
    //Display segmentation form
    $form[]['#markup'] = '<div id="form-content">';
    $form[]['#markup'] = '<h2>Sign up for support</h2>';
    $form[]['#markup'] = '<p>If you are a patient or healthcare provider in the U.S., you can sign up to receive information about INCIVEK and hepatitis C treatment.  Choose one of the buttons below to get started.</p><br />';
    $form[]['#markup'] = 'I confirm that I am a:<br />';
    $form[]['#markup'] = '<div id="submitwrapper">';
    $form['submit'] = array('#type' => 'submit', '#id' => 'patientsubmit');
    $form[]['#markup'] = '<a href="https://www.incivekhcp.com/register" class="go-hcp" id="hcpsubmit"></a>';
    $form[]['#markup'] = '</div></div>';
    $form['segmentdtc'] = array('#type' => 'hidden', '#value' => '1');
    $form['#attributes'] = array('enctype' => 'multipart/form-data');
    $form['#submit'] = array('vx_register_dtc_form_submit');
  }
  
  $form['footer']['#markup'] = variable_get('vx_register_footer', '<!-- Footer -->');
    
  return $form;
}

/**
 * Form validation; DTC registration form 
 *
 * @ingroup forms
 * @see vx_register_dtc_form()
 * @see vx_register_dtc_form_submit()
 */
function vx_register_dtc_form_validate($form, &$form_state) {
  if (!isset($form_state['input']['fnm'])) {
    // User submitted the segmentation form.  No validation.
    return;
  }

  $year = date("Y");
  $min_year = $year-100;
  $max_year = $year-18;
  $min_diag = $year-100;
  $max_diag = $year;
  $max_treat_year = $year+5;
  
  //Ensure name fields are alpha only
  if(!preg_match("/^[a-z]+$/i",$form_state['values']['fnm'])) {
    form_set_error('fnm','First Name may not contain numeric or special characters.');  
  }
  
  if(!preg_match("/^([a-z]+)?$/i",$form_state['values']['mi'])) {
    form_set_error('mi','Middle Name may not contain numeric or special characters.');  
  }
  
  if(!preg_match("/^[a-z]+$/i",$form_state['values']['lnm'])) {
    form_set_error('lnm','Last Name may not contain numeric or special characters.');  
  }
  
  //Ensure user indicated that they are 18 and over
  if ($form_state['values']['age18'] != 'Yes') form_set_error('age18','Only adults 18 or older may register.');
  
  //Ensure a valid 4 digit birth year was given
  if(preg_match("/^[0-9]{4}$/",$form_state['values']['birth_year'])) {    
    if (($form_state['values']['birth_year'] < $min_year) || ($form_state['values']['birth_year'] > $max_year)) {
      form_set_error('birth_year','Invalid birth year entered.  You must be 18 or older to register.');
    }  
  } else {
    form_set_error('birth_year','Please enter a valid 4 digit birth year in YYYY format.');
  }
  
  //Ensure a state was chosen
  if ($form_state['values']['state'] == '--') form_set_error('state','Please specify your state.');
  
  //Check for valid zipcode
  if(!preg_match("/^([0-9]{5})(-[0-9]{4})?$/i",$form_state['values']['zipcode'])) {
    form_set_error('zipcode','Please enter a valid zipcode.');  
  }
  
  //Ensure a valid email was entered
  if ($form_state['values']['mail']) {
    if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i", $form_state['values']['mail'])) {
      form_set_error('mail', 'Please enter a valid e-mail address.');
    }
  }

  //Ensure email and confirmation email match
  if ($form_state['values']['mail'] != $form_state['values']['conf_mail']) {
    form_set_error('conf_mail', 'Your e-mail address and confirmed e-mail address must match.');
  }
    
  //Ensure all required registration questions were properly answered
  if ((!$form_state['values']['hcp_comfort_indicator']) || (!$form_state['values']['hcv_status_reg'])) {
    // Must answer the first two required questions
    form_set_error('hcp_comfort_indicator', 'Please answer all required registration questions.');
  } else if (($form_state['values']['hcv_status_reg'] == 'Yes') && (!$form_state['values']['hcv_diag_date'])) {
    // If user indicated they have been diagnosed they must provide a diag date
    form_set_error('hcp_comfort_indicator', 'Please enter the year you were diagnosed in YYYY format.');
  } else if (($form_state['values']['hcv_status_reg'] == 'Yes') && (!$form_state['values']['prev_trtd'])) {
    // If user indicated they have been diagnosed they must answer follow up questions
    form_set_error('hcp_comfort_indicator', 'Please answer all required registration questions.');
  } else if (($form_state['values']['hcv_status_reg'] == 'Yes') && (!$form_state['values']['tvr_status_reg'])) {
    // If user indicated they have been diagnosed they must answer follow up questions
    form_set_error('hcp_comfort_indicator', 'Please answer all required registration questions.');   
  } else if (($form_state['values']['tvr_status_reg'] == 'Yes') && (!$form_state['values']['tvr_start_date'])) {
    // If user indicated they are starting INCIVEK they must provide a start date
    form_set_error('hcp_comfort_indicator','Please enter a valid therapy start date in MM/DD/YYYY format.');
  }
    
  //If treatment start date was provided ensure it is a valid date
  if ($form_state['values']['tvr_start_date']) {
    $parts = preg_split("/[\s\/]+/", $form_state['values']['tvr_start_date']);

    if ((!checkdate((int)$parts[0],(int)$parts[1],(int)$parts[2])) || ($parts[2] < 2006) || ($parts[2] > $max_treat_year)) {
      form_set_error('ptnt-start-dt','Please enter a valid therapy start date in MM/DD/YYYY format.');
    }
  }
  
  //Ensure a valid 4 digit diagnosis year was given (if provided)
  if ($form_state['values']['hcv_diag_date']) {
    if(preg_match("/^[0-9]{4}$/",$form_state['values']['hcv_diag_date'])) {    
      if (($form_state['values']['hcv_diag_date'] < $min_diag) || ($form_state['values']['hcv_diag_date'] > $max_diag)) {
        form_set_error('hcv_diag_date','Please enter a valid 4 digit diagnosis year in YYYY format.');
      }  
    } else {
      form_set_error('hcv_diag_date','Please enter a valid 4 digit diagnosis year in YYYY format.');
    }
  }
}

/**
 * Form handler; DTC registration form 
 *
 * @ingroup forms
 * @see vx_register_dtc_form()
 * @see vx_register_dtc_form_validate()
 */
function vx_register_dtc_form_submit($form, &$form_state) {
  // User submitted registration form and it passed validation
  if (!isset($form_state['input']['fnm'])) {  
    $form_state['rebuild'] = true;
    $form_state['storage']['values']['segmentdtc'] = true;
    return;
  }

  // If this email address is already registered purge the associated
  // existing user completely from the system.
  $encmail = vx_register_encodeStr(strtolower($form_state['values']['mail']));
  
  if ($result = db_query("SELECT vx_uid FROM {vx_users} WHERE email = :email", array(':email' => $encmail))) {
    foreach ($result as $record) {
      $vx_uid = $record->vx_uid;
      vx_register_purge_user($vx_uid);
    }
  }

  if ($result = db_query("SELECT vx_uid FROM {vx_unsubscribe} WHERE email = :email", array(':email' => $encmail))) {
    foreach ($result as $record) {
      $vx_uid = $record->vx_uid;
      vx_register_purge_unsubscribe($vx_uid);
    }
  }
  
  // Add opt dates to form data
  $form_state['values']['vrtx_prgm_opt_in_date'] = format_date(time(), 'custom', 'Ymd H:i:s'); 
  $form_state['values']['bd1_opt_in_date'] = format_date(time(), 'custom', 'Ymd H:i:s');

  // Save users personal information to vx_users
  $vx_uid = db_insert('vx_users')
  ->fields(array(
  	'fnm' => vx_register_encodeStr($form_state['values']['fnm']),
  	'mi' => vx_register_encodeStr($form_state['values']['mi']),
    'lnm' => vx_register_encodeStr($form_state['values']['lnm']),
  	'addr_1' => vx_register_encodeStr($form_state['values']['addr_1']),
    'addr_2' => vx_register_encodeStr($form_state['values']['addr_2']),
    'city' => vx_register_encodeStr($form_state['values']['city']),
  	'state' => vx_register_encodeStr($form_state['values']['state']),
    'zipcode' => vx_register_encodeStr($form_state['values']['zipcode']),
    'email' => vx_register_encodeStr(strtolower($form_state['values']['mail'])),
    'individual_type' => vx_register_encodeStr('C'),
    'update_date' => format_date(time(), 'custom', 'Y-m-d H:i:s'),
  )) ->execute();
    
  // Save profile answers to vx_user_profile_values
  $profile_fields = array(
    'age18',
    'birth_year',
    'hcp_comfort_indicator',
    'hcv_status_reg',
    'hcv_diag_date',
    'prev_trtd',
    'tvr_status_reg',
    'tvr_start_date',
    'vrtx_prgm_opt_in_status',
    'vrtx_prgm_opt_in_date',
    'bd1_opt_in',
    'bd1_opt_in_date'
  );
  
  // Convert format on tvr_start_date if specified into Epsilon's format
  if ($form_state['values']['tvr_start_date']) {
    $pieces = explode("/", $form_state['values']['tvr_start_date']);
    $form_state['values']['tvr_start_date'] = format_date(mktime(0, 0, 0, $pieces[0], $pieces[1], $pieces[2]), 'custom', 'Ymd H:i:s');
  }
  
  foreach ($profile_fields as $field) {
      $id = db_insert('vx_user_profile_values')
        ->fields(array(
          'name' => vx_register_encodeStr($field),
          'vx_uid' => $vx_uid,
          'value' => vx_register_encodeStr($form_state['values'][$field])
        )) ->execute();
  }

  // Set omniture event5 (DTC registration) per tracking matrix if user registered 
  $_SESSION['events'][]='event5';
    
  // Redirect to correct thank you page
  $url = variable_get('vx_register_thankyou_url', '<front>');
  drupal_goto($url);

}

/**
 * Validate that a valid telephone number was passed in
 */
function vx_register_is_phone_num($number, $formats)
{
  $format = trim(preg_replace("/[0-9]/", "#", $number));
  return (in_array($format, $formats)) ? true : false;
}


/**
 * Purge specified user from the system
 */
function vx_register_purge_user($vx_uid) {
  // Purge records for this vx_uid
  db_delete('vx_users')
  ->condition('vx_uid', $vx_uid)
  ->execute();
  
  db_delete('vx_user_profile_values')
  ->condition('vx_uid', $vx_uid)
  ->execute();
}

/**
 * Purge specified user from the unsubscribe tables
 */
function vx_register_purge_unsubscribe($vx_uid) {
  // Purge records for this vx_uid
  db_delete('vx_unsubscribe')
  ->condition('vx_uid', $vx_uid)
  ->execute();
  
  db_delete('vx_unsubscribe_opts')
  ->condition('vx_uid', $vx_uid)
  ->execute();
}

/**
 * Create and encoded URI from the specified input array.  These encrypted URIs
 * can then be used to pre-populate form fields directly.
 * 
 */
function vx_register_encodeURI($array){
  $str = '';

  // Loop through the array and build a keyed list that is pipe delimited
  foreach ($array as $key => $value) {
    $str .= $key.'='.$value.'|';
  }
  
  # Encode string
  $str = base64_encode($str);
  $str = strrev($str);
  $str = base64_encode($str);
  return urlencode($str);
}

/**
 * Decode an encoded URI and return a keyed array.  This array is then
 * used to pre-populate form fields.
 * 
 */
function vx_register_decodeURI($str){
  $decodedArr = array();

  // Decode String
  $str = urldecode($str);
  $str = base64_decode($str);
  $str = strrev($str);
  $str = base64_decode($str);
  $parts = explode('|', $str);
  
  // Loop through the parts and build a keyed array
  foreach ($parts as $part) {
    $pair = explode('=',$part);
    $key = (isset($pair[0])?$pair[0]:'');
    $value = (isset($pair[1])?$pair[1]:'');
    $decodedArr[$key]=$value;
  }
  
  return $decodedArr;
}

/**
 * Create and encoded string from the specified input string.
 */
function vx_register_encodeStr($input){
  // Encode string
  $str = base64_encode($input);
  $str = strrev($str);
  $str = base64_encode($str);
  return $str;
}

/**
 * Decode an encoded string and return in raw text
 * 
 */
function vx_register_decodeStr($input){
  // Decode String
  $str = base64_decode($input);
  $str = strrev($str);
  $str = base64_decode($str);
  return $str;
}

/**
 * Form builder; edit a user account subscriptions
 *
 * @ingroup forms
 * @see vx_register_unsubscribe_form_validate()
 * @see vx_register_unsubscribe_form_submit()
 * @see vx_register_unsubscribe_submit()
 */
function vx_register_unsubscribe_form($form_state) {
  // Set pageName & eVar1 per tracking manifest
  $_SESSION['evars']['eVar1']=t('Unsubscribe');
  $_SESSION['evars']['pageName']=t('Unsubscribe');
    
  // check if an encrypted string was passed in to pre-populate fields
  if (isset($_GET["en"])) {
    $input = vx_register_decodeURI($_GET["en"]);
  }
  
  // Build form
  $form['formcontent']['#markup'] = '<div id="form-content"><div style="height:25px;"></div>';
  $form['header']['#markup']='<h2>Unsubscribe</h2><p>To change your settings, please fill out the form below and click <strong>Save My Settings</strong>.</p>';
  $form['reqtext']['#markup']='<p class="required">*required field</p>';

  $form['fnm'] = array (
    '#type' => 'textfield',
    '#title' => 'First Name',
    '#maxlength' => 100,
  	'#size' => 20,
    '#required' => 1,
    '#default_value' => ((isset($form_state['post']['fnm']))?$form_state['post']['fnm']:(isset($input['fnm'])?$input['fnm']:'')),
  );
  $form['mi'] = array (
    '#type' => 'textfield',
    '#title' => 'Middle Name',
    '#maxlength' => 50,
  	'#size' => 20,
    '#required' => 0,
	'#default_value' => ((isset($form_state['post']['mi']))?$form_state['post']['mi']:(isset($input['mi'])?$input['mi']:'')),
  );
  $form['lnm'] = array (
    '#type' => 'textfield',
    '#title' => 'Last Name',
    '#maxlength' => 60,
  	'#size' => 20,
    '#required' => 1,
  	'#default_value' => ((isset($form_state['post']['lnm']))?$form_state['post']['lnm']:(isset($input['lnm'])?$input['lnm']:'')),
  );
  $form['clear1']['#markup'] = '<div class="clear"></div>';
  $form['addr_1'] = array (
    '#type' => 'textfield',
    '#title' => 'Street Address 1',
    '#maxlength' => 255,
  	'#size' => 40,
    '#required' => 0,
  	'#default_value' => ((isset($form_state['post']['addr_1']))?$form_state['post']['addr_1']:(isset($input['addr1'])?$input['addr1']:'')),
  );
  $form['addr_2'] = array (
    '#type' => 'textfield',
    '#title' => 'Street Address 2',
    '#maxlength' => 255,
  	'#size' => 40,
    '#required' => 0,
    '#default_value' => ((isset($form_state['post']['addr_2']))?$form_state['post']['addr_2']:(isset($input['addr2'])?$input['addr2']:'')),
  );
  $form['city'] = array (
    '#type' => 'textfield',
    '#title' => 'City',
    '#maxlength' => 60,
  	'#size' => 20,
    '#required' => 0,
    '#default_value' => ((isset($form_state['post']['city']))?$form_state['post']['city']:(isset($input['city'])?$input['city']:'')),
  );
  $form['state'] = array (
    '#type' => 'select',
    '#title' => 'State',
    '#options' => array ('--' => '--','AK' => 'AK','AL' => 'AL','AR' => 'AR','AZ' => 'AZ','CA' => 'CA','CO' => 'CO','CT' => 'CT','DC' => 'DC','DE' => 'DE','FL' => 'FL','GA' => 'GA','HI' => 'HI','IA' => 'IA','ID' => 'ID','IL' => 'IL','IN' => 'IN','KS' => 'KS','KY' => 'KY','LA' => 'LA','MA' => 'MA','MD' => 'MD','ME' => 'ME','MI' => 'MI','MN' => 'MN','MO' => 'MO','MS' => 'MS','MT' => 'MT','NC' => 'NC','ND' => 'ND','NE' => 'NE','NH' => 'NH','NJ' => 'NJ','NM' => 'NM','NV' => 'NV','NY' => 'NY','OH' => 'OH','OK' => 'OK','OR' => 'OR','PA' => 'PA','RI' => 'RI','SC' => 'SC','SD' => 'SD','TN' => 'TN','TX' => 'TX','UT' => 'UT','VA' => 'VA','VT' => 'VT','WA' => 'WA','WI' => 'WI','WV' => 'WV','WY' => 'WY',),
    '#required' => 0,
    '#default_value' => ((isset($form_state['post']['state']))?$form_state['post']['state']:(isset($input['state'])?$input['state']:'')),
  );
  $form['zipcode'] = array (
    '#type' => 'textfield',
    '#title' => 'Zip Code',
    '#maxlength' => 12,
  	'#size' => 18,
    '#required' => 0,
    '#default_value' => ((isset($form_state['post']['zipcode']))?$form_state['post']['zipcode']:(isset($input['zipcode'])?$input['zipcode']:'')),
  );
  $form['clear2']['#markup'] = '<div class="clear"></div>';
  $form['mail'] = array (
    '#type' => 'textfield',
    '#title' => 'Email Address',
    '#maxlength' => 100,
  	'#size' => 22,
    '#required' => 0,
    '#default_value' => ((isset($form_state['post']['mail']))?$form_state['post']['mail']:(isset($input['mail'])?$input['mail']:'')),
  );
  
  $form['conf_mail'] = array (
    '#type' => 'textfield',
    '#title' => 'Confirm Email Address',
    '#maxlength' => 100,
  	'#size' => 22,
    '#required' => 0,
  );
  $form['clear3']['#markup'] = '<div class="clear"></div>';
  $form['br_opt_out'] = array(
    '#type' => 'checkbox',
    '#title' => 'I no longer wish to receive INCIVEK program materials.',
    '#default_value' => 0,
  );
  $form['clear4']['#markup'] = '<div class="clear"></div>';
  $form['vrtx_opt_out'] = array(
    '#type' => 'checkbox',
    '#title' => 'I no longer wish to receive updates and information from Vertex Pharmaceuticals.',
    '#default_value' => 0,
  );
  $form['clear5']['#markup'] = '<div class="clear"></div>';
  $form['submit'] = array('#type' => 'submit');
  $form['formclose']['#markup'] = '</div>';
  $form['#attributes'] = array('enctype' => 'multipart/form-data');
  $form['#submit'] = array('vx_register_unsubscribe_form_submit');
  
  return $form;
}

/**
 * Form validation; Unsubscribe form 
 *
 * @ingroup forms
 * @see vx_register_unsubscribe_form()
 * @see vx_register_unsubscribe_form_submit()
 */
function vx_register_unsubscribe_form_validate($form, &$form_state) {

  //Ensure name fields are alpha only
  if(!preg_match("/^[a-z]+$/i",$form_state['values']['fnm'])) {
    form_set_error('fnm','First Name may not contain numeric or special characters.');  
  }
  
  if(!preg_match("/^([a-z]+)?$/i",$form_state['values']['mi'])) {
    form_set_error('mi','Middle Name may not contain numeric or special characters.');  
  }
  
  if(!preg_match("/^[a-z]+$/i",$form_state['values']['lnm'])) {
    form_set_error('lnm','Last Name may not contain numeric or special characters.');  
  }
  
  // If the user entered an email address
  if ($form_state['values']['mail']) {
    //Ensure a valid email was entered
    if ($form_state['values']['mail']) {
      if (!preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i", $form_state['values']['mail'])) {
        form_set_error('mail', 'Please enter a valid e-mail address.');
      }
    }
    
    //Ensure email and confirmation email match
    if ($form_state['values']['mail'] != $form_state['values']['conf_mail']) {
      form_set_error('conf_mail', 'Your e-mail address and confirmed e-mail address must match.');
    }
    
    //Ensure the user selected at least one unsubscribe option
    if ((!$form_state['values']['br_opt_out']) && (!$form_state['values']['vrtx_opt_out'])) {
      form_set_error('br_opt_out', 'Please select at least one program you wish to unsubscribe from.');
    }
  } else {
    if ((!$form_state['values']['zipcode']) || ($form_state['values']['state'] == '--') || (!$form_state['values']['addr_1']) || (!$form_state['values']['city']))  {
      form_set_error('addr_1','Please provide either your email address or full street address you used when registering.');
    }
    
    //Ensure the user selected at least one unsubscribe option
    if ((!$form_state['values']['br_opt_out']) && (!$form_state['values']['vrtx_opt_out'])) {
      form_set_error('br_opt_out', 'Please select at least one program you wish to unsubscribe from.');
    }
  }  
}

/**
 * Form handler; Unsubscribe form 
 *
 * @ingroup forms
 * @see vx_register_unsubscribe_form()
 * @see vx_register_unsubscribe_form_validate()
 */
function vx_register_unsubscribe_form_submit($form, &$form_state) {
  // User submitted unsubscribe form and it passed validation
  
  // Add opt-out dates to form data
  $form_state['values']['vrtx_prgm_opt_out_date'] = format_date(time(), 'custom', 'Ymd H:i:s');
  $form_state['values']['br_opt_out_date'] = format_date(time(), 'custom', 'Ymd H:i:s');
  
  // Gather user opt responses
  $opts = array();
  
  if ($form_state['values']['br_opt_out']) {
   $opts[]='br_opt_out';
   $opts[]='br_opt_out_date'; 
  }
    
  if ($form_state['values']['vrtx_opt_out']) {
    $form_state['values']['vrtx_prgm_opt_in_status']=0;
    $opts[]='vrtx_prgm_opt_in_status';
    $opts[]='vrtx_prgm_opt_out_date';
  }
  
  // Check if this email address exists in vx_users
  $vx_uid=0;
  $encmail = vx_register_encodeStr(strtolower($form_state['values']['mail']));
  
  if ($result = db_query("SELECT vx_uid FROM {vx_users} WHERE email = :email", array(':email' => $encmail))) {
    foreach ($result as $record) {
      $vx_uid = $record->vx_uid;
    }
  }
  
  if ($vx_uid) {
    // This user registered via the website.  Save opts to vx_user_profile_values
    foreach ($opts as $field) {
      $encname = vx_register_encodeStr($field);
      
      // Delete existing values
      db_delete('vx_user_profile_values')
      ->condition('vx_uid', $vx_uid)
      ->condition('name', $encname)
      ->execute();
      
      // Insert new values
      db_insert('vx_user_profile_values')
      ->fields(array(
          'name' => vx_register_encodeStr($field),
          'vx_uid' => $vx_uid,
          'value' => vx_register_encodeStr($form_state['values'][$field])
      )) ->execute();
    }
    
    // Flag this user record as updated so it will be picked up by the extract
    $num=db_update('vx_users')
    ->condition('vx_uid', $vx_uid, '=')
    ->fields(array(
      'update_date' => format_date(time(), 'custom', 'Y-m-d H:i:s'),
      'extract_flag' => 0
    )) ->execute();
    
  } else {
    
    // This user did not register via the website.  Their email address does not exist in vx_users
    // Save users personal information to vx_unsubscribe
    $vx_uid = db_insert('vx_unsubscribe')
    ->fields(array(
      'fnm' => vx_register_encodeStr($form_state['values']['fnm']),
      'mi' => vx_register_encodeStr($form_state['values']['mi']),
      'lnm' => vx_register_encodeStr($form_state['values']['lnm']),
      'addr_1' => vx_register_encodeStr($form_state['values']['addr_1']),
      'addr_2' => vx_register_encodeStr($form_state['values']['addr_2']),
      'city' => vx_register_encodeStr($form_state['values']['city']),
      'state' => vx_register_encodeStr($form_state['values']['state']),
      'zipcode' => vx_register_encodeStr($form_state['values']['zipcode']),
      'email' => vx_register_encodeStr(strtolower($form_state['values']['mail'])),
      'individual_type' => vx_register_encodeStr('C'),
      'update_date' => format_date(time(), 'custom', 'Y-m-d H:i:s'),
    )) ->execute();
    
    // Save unsubscribe opts to vx_unsubscribe_opts
    foreach ($opts as $field) {
      $id = db_insert('vx_unsubscribe_opts')
        ->fields(array(
          'name' => vx_register_encodeStr($field),
          'vx_uid' => $vx_uid,
          'value' => vx_register_encodeStr($form_state['values'][$field])
        )) ->execute();
    }
    
  }
  
  // Set omniture event11 per tracking matrix if user unsubscribed 
  $_SESSION['events'][]='event11';  
  
  // Redirect to correct thank you page
  $url = variable_get('vx_register_unsubscribe_thankyou_url', '');
  drupal_goto($url);
}

/**
 * Implements hook_init().
 */
function vx_register_init() {
  if ((arg(0) != 'admin') && (arg(0) != 'user')) {
    //add module specific JS and CSS
    drupal_add_js(drupal_get_path('module', 'vx_register') . '/vx_register.js', array('group' => JS_THEME));
    drupal_add_css(drupal_get_path('module', 'vx_register') . '/vx_register.css', array('group' => CSS_THEME));    
  }
}


/**
 * Implements hook_search_info().
 */
function vx_register_search_info() {
  return array(
    'title' => 'Incivek Custom Search',
    'path' => 'content',
  );
}


/**
 * Implementation of hook_search_execute().
 */
function vx_register_search_execute($keys = NULL, $conditions = NULL) {
  $results=array();

  // Add custom search results
  if (preg_match("/.*register.*/i", $keys)) {
    $results[] = array(
      'link' => url('register'),
      'type' => 'node',
      'title' => 'Register for Updates',
      'score' => 1000,
      'snippet' => 'If you are a patient or healthcare professional in the U.S., you can sign up ...',
    );
  } else if (preg_match("/.*unsubscribe.*/i", $keys)) {
    $results[] = array(
      'link' => url('register/unsubscribe'),
      'type' => 'node',
      'title' => 'Unsubscribe',
      'score' => 1000,
      'snippet' => 'To change your settings, please fill out the form below ...',
    );    
  }
  
  // Perform standard node module search
  if ($node_results = node_search_execute($keys,$conditions)) {
    $results = array_merge($results,$node_results);
  }
  
  // Remove certain results from the search results
  $paths = array('unsubscribe-confirmation','thank-you');
  $remove = array();
  for ( $i=0; $i < sizeof($results); $i++) {
    foreach ($paths as $path) {
      if (strpos($results[$i]['link'], $path)) {
        $remove[] = $i;
      }
    }
  }
  
  foreach ($remove as $i) {
    unset($results[$i]);
  }
    
  return $results;
}

