<div id="search-result-set">
<?php if ($search_results) : ?>
  <ol class="search-results">
    <?php print $search_results; ?>
  </ol>
  <?php print $pager; ?>
<?php else : ?>
  <h2><?php print t('Your search yielded no results');?></h2>
<?php endif; ?>
</div>
<div class="divider" style="padding-bottom: 40px;"></div>