<div id="container">
  <div id="header">
    <?php print render($page['header']); ?>
  </div>
  <?php if ($logo): ?>
  <div id="incivek-logo">
    <a href="<?php print url('<front>'); ?>"><img src="<?php print $logo; ?>" alt="Incivek Logo" /></a>
  </div>
  <?php endif; ?>
  <div id="left">
    <?php print render($page['left']); ?>
  </div>
  <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
  <?php print $messages; ?>
  <div id="content">
    <p class="white" style="font-weight: bold; padding-left: 20px; margin: 5px 0 15px 0; font-size: 15px;">For adults with chronic hepatitis C genotype 1 infection with stable liver problems</p>
    <div class="top"></div>
    <div class="middle">
      <?php print render($page['content']); ?>
      <?php print render($page['isi']); ?>
    </div>
    <div class="bottom"></div>
    
  </div> <!-- /content -->
  <div class="clear"></div>
</div><!-- /container -->
<div id="footer">
  <?php print theme('links', array('links' => menu_navigation_links('menu-footer-links'), 'attributes' => array('id' => 'footer-links'))); ?>
  <?php print render($page['footer']); ?>
</div><!-- /footer -->
<?php print render($page['modal']); ?>