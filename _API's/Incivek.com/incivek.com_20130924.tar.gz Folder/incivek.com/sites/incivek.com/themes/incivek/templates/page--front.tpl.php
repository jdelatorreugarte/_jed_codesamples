<div id="container">
  <div id="header">
    <?php print render($page['header']); ?>
  </div>
  <?php if ($logo): ?>
  <div id="incivek-logo">
    <a href="<?php print url('<front>'); ?>"><img src="<?php print $logo; ?>" alt="Incivek Logo" /></a>
  </div>
  <?php endif; ?>
  <div id="content">
    <?php print render($page['content']); ?>
    <?php print render($page['isi']); ?>
    <?php if ($tabs): ?><div class="tabs"><?php print render($tabs); ?></div><?php endif; ?>
    <?php print $messages; ?>
  </div> <!-- /content -->
</div><!-- /container -->
<div id="footer">
  <?php print theme('links', array('links' => menu_navigation_links('menu-footer-links'), 'attributes' => array('id' => 'footer-links'))); ?>
  <?php print render($page['footer']); ?>
</div><!-- /footer -->
<?php print render($page['modal']); ?>
