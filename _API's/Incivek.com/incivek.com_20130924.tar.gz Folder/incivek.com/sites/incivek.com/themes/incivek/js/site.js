jQuery(document).ready(function(){
	
  // jqModal event handler centers window
  var centerPopup = function(hash){
	windowHeight   = jQuery(window).height();
	if(jQuery.browser.safari || jQuery.browser.chrome) bodyelem = jQuery("body")
	else bodyelem = jQuery("html,body")

	linkOffset     = jQuery(bodyelem).scrollTop();
	hash.w.css('top', linkOffset + (windowHeight * .2)).show();
  };
  
  // Interstitial Handlers
  jQuery('#compatibilityInterstitial').jqm({ modal: true });
  jQuery('#leavesiteInterstitial').jqm({ modal: true });

  if(jQuery.browser.msie && jQuery.browser.version=="6.0") {
	if (!readCookie("compatCookie") == 1) {
	  createCookie("compatCookie", 1);
	  jQuery('#compatibilityInterstitial').jqmShow();
	}
  }
  
  jQuery('.external').click(function (e) {
    e.preventDefault();
    var value = jQuery(this).attr('href');
    jQuery('#leavesiteInterstitial').jqm({onShow:centerPopup}).jqmShow();
    createCookie("siteCookie", value);
  });

  // Search Form Handler
  jQuery('div.searchWrapper input.form-text').blur(function() {
    jQuery('div.searchWrapper').removeClass('active');
    jQuery('#search-block-form .form-submit').removeClass('active');
  });
  
  // Tab Click Handler
  jQuery('a.tab').click(function(){ 
    var target = jQuery(this).attr('href');
    var tab = target.substring(1);
    var strOver = "-ro";
    var strOn = "-on";
    var strOff = "-off";

    // Hide Current Tab
    var strImg = jQuery('#tabContainer .tab.active img').attr("src");
    jQuery('#tabContainer .tab.active img').attr("src",strImg.replace(strOn,strOff));
    jQuery('#tabContainer .tab.active').removeClass('active');
  
    // Hide Content
    jQuery('#tabContent div.content').removeClass('active').hide();
    
    // Toggle Backrounds
    jQuery('#tabWrapper').removeClass().addClass(tab);
    jQuery('#tabContent').removeClass().addClass(tab);
  
    // Activate New Tab
    jQuery(this).addClass('active');
  
    var strImg = jQuery('#tabContainer .tab.active img').attr("src");
    if (strImg.indexOf(strOver) != -1)
      jQuery(this).find('img').attr("src",strImg.replace(strOver,strOn));
    else
      jQuery(this).find('img').attr("src",strImg.replace(strOff,strOn));
  
    // Show Content
    jQuery(target).addClass('active').show();
    return false;
  });

  // Tab Rollover Hander
  jQuery('#tabContainer .tab').hover(
    function () { 
      var strOver = "-ro";
      var strOff = "-off";
      var strOn = "-on";
      
      // Remove active state from active tab
      //var strImg = jQuery('#tabContainer .tab.active img').attr("src");
      //jQuery('#tabContainer .tab.active img').attr("src",strImg.replace(strOn,strOff));
      
      // Add rollover state to hovered tab
      var strImg = jQuery(this).find('img').attr('src');
      if (strImg.indexOf(strOff) != -1) {
        jQuery(this).find('img').attr('src',strImg.replace(strOff,strOver));
      }
    
      // Display content for hovered tab
      //var target = jQuery(this).attr('href');
      //jQuery('#tabContent .content:visible').hide();
      //jQuery(target).show();
    },
    function () {
      var strOver = "-ro";
      var strOff = "-off";
      var strOn = "-on";
      var strImg = jQuery(this).find('img').attr('src');
      
      // Remove hover state from hovered button
      if (strImg.indexOf(strOver) != -1) {
        jQuery(this).find('img').attr('src',strImg.replace(strOver,strOff));
      }
      
      // Add active state to active tab
      var strImg = jQuery('#tabContainer .tab.active img').attr("src");
      jQuery('#tabContainer .tab.active img').attr("src",strImg.replace(strOff,strOn));
      
         
      // Display active content
      //jQuery('#tabContent .content:visible').hide();
      //jQuery('#tabContent .content.active').show();
    }
  );
  
  // Tab NextTab Handler
  jQuery('a.NextTab').click(function(){
	var target = jQuery(this).attr('href');
	var strOver = "-ro";
	var strOn = "-on";
	var strOff = "-off";
	
	// Hide Current Tab
	var strImg = jQuery('#tabContainer .tab.active img').attr("src");
	jQuery('#tabContainer .tab.active img').attr("src",strImg.replace(strOn,strOff));
	var next = jQuery('#tabContainer .tab.active').next('.tab');
	jQuery('#tabContainer .tab.active').removeClass('active');

	// Activate next tab
	jQuery(next).addClass('active');
	var strImg = jQuery(next).find('img').attr('src');
	jQuery('#tabContainer .tab.active img').attr("src",strImg.replace(strOff,strOn));
	
    jQuery('#tabContent div.content').removeClass('active').hide();
    jQuery(target).addClass('active').show();
    return false;
    });
});

function leaveSite() {
    window.open(readCookie("siteCookie"), '_blank');
}

function redirect() {
    window.location = readCookie("siteCookie");
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);

        if (c.indexOf(nameEQ) == 0) 
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}


// Image preload
function preload(arrayOfImages) {
  jQuery(arrayOfImages).each(function(){
    jQuery('<img/>')[0].src = this;
  });
}

function trackLink() {
    if (arguments.length < 2) {
        alert("Error: Not enough arguments to TrackLink()");
        return false;
    }

    //Need to save global state
    var old_s = {};

    old_s.linkTrackVars = s.linkTrackVars;
    old_s.linkTrackEvents = s.linkTrackEvents;
    old_s.events = s.events;

    var name = arguments[0];

    var linkType = arguments[1];

    var options;

    if (arguments.length > 2) {
        options = arguments[2];
    }

    var linkTrackVars = 'channel'; //override to always track the channel
    var linkTrackEvents = '';

    var suppressTracking = false;

    if (options != undefined) {

        if (options.props != undefined) {
            for (var i in options.props) {
                var prop = options.props[i];

                if (linkTrackVars.length > 0) {
                    linkTrackVars += ",";
                }

                linkTrackVars += prop.name;

                //save old value
                old_s[prop.name] = s[prop.name];

                s[prop.name] = prop.value;
            }
        }

        if (options.event != undefined) {
            if (linkTrackVars.length > 0) {
                linkTrackVars += ",";
            }

            linkTrackVars += "events";

            linkTrackEvents += options.event;

            s.events = options.event;
        }

        if (options.evars != undefined) {
            for (var i in options.evars) {
                var evar = options.evars[i];

                if (linkTrackVars.length > 0) {
                    linkTrackVars += ",";
                }

                linkTrackVars += evar.name;

                //save old value
                old_s[evar.name] = s[evar.name];

                s[evar.name] = evar.value;
            }
        }

        if (options.supress != undefined) {
            suppressTracking = options.supress;
        }
    }

    s.linkTrackVars = linkTrackVars;
    s.linkTrackEvents = linkTrackEvents;

    if (!suppressTracking) {
        s.tl(true, linkType, name);
    }

    //Restore global state
    for (val in old_s) {
        s[val] = old_s[val];
    }

    return false;
}
