<?php

function incivek_preprocess_node(&$variables) {
        
  if ($variables['is_front']) {
    // Remove "Read More" links from front page
    unset($variables['content']['links']);
  }
  
}

function incivek_css_alter(&$css) {
    unset($css[drupal_get_path('module','system').'/system.base.css']);
    unset($css[drupal_get_path('module','system').'/system.theme.css']);
    unset($css[drupal_get_path('module','system').'/system.menus.css']);
    unset($css[drupal_get_path('module','node').'/node.css']);
    unset($css[drupal_get_path('module','user').'/user.css']);
    unset($css[drupal_get_path('module','field').'/theme/field.css']);
    unset($css[drupal_get_path('module','search').'/search.css']);
    unset($css[drupal_get_path('module','system').'/system.messages.css']);
}

function incivek_preprocess_page(&$vars) {
  // Remove Edit tab from user profile page
  if (arg(0) == 'user') {
    incivek_removetab('Edit', $vars);
  }
  
  // Remove View tab from all pages
  incivek_removetab('View', $vars);

  // Remove tabs from search results page
  incivek_removetab('Incivek Custom Search', $vars);
  incivek_removetab('Content', $vars);
  incivek_removetab('content', $vars);
  //print_r($vars);
  
  // Add template hints allowing separate templates by node type
  if (!empty($vars['node'])) {
    $vars['theme_hook_suggestions'][] = 'page__node_' . $vars['node']->type;
  }
}

function incivek_preprocess_html(&$vars) {
  $node = menu_get_object();

  if ($node && $node->nid) {
    $vars['theme_hook_suggestions'][] = 'html__' . $node->type;
  }
}


function incivek_js_alter(&$js){
  // Remove Drupal's canned jQuery at Incivek theme layer only
  // leaves it in place for the Admin
  unset($js['settings']);
  unset($js['misc/drupal.js']);
  unset($js['misc/jquery.js']);
  unset($js['misc/jquery.once.js']);
}

// Remove undesired local task tabs.
// Related to yourthemename_removetab() in yourthemename_preprocess_page().
function incivek_removetab($label, &$vars) {

  // Remove from primary tabs
  if (is_array($vars['tabs']['#primary'])) {
    foreach ($vars['tabs']['#primary'] as $i => $primary_tab) {
      if ($primary_tab['#link']['title'] == $label) {
        unset($vars['tabs']['#primary'][$i]);
      }
    }
  }

  // Remove from secondary tabs
  if (is_array($vars['tabs']['#secondary'])) {
    foreach ($vars['tabs']['#secondary'] as $i => $secondary_tab) {
      if ($secondary_tab['#link']['title'] == $label) {
        unset($vars['tabs']['#secondary'][$i]);
      }
    }
  }
}

// Remove a tag from the head for Drupal 7 if exists
function incivek_html_head_alter(&$head_elements) {
  if (isset($head_elements['system_meta_generator'])) {
    unset($head_elements['system_meta_generator']);
  }
}

?>