<li class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <?php print render($title_prefix); ?>
  <strong>
    <a href="<?php print $url; ?>"><?php print $title; ?></a>
  </strong>
  <?php print render($title_suffix); ?>
  <div>
    <?php if ($snippet) : ?>
      <p class="search-snippet"><?php print $snippet; ?></p>
    <?php endif; ?>
  </div>
</li>
