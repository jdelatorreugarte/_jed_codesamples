﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using vetvance_api.Models;
using System.Web.Security;
using System.Net;
using System.Diagnostics;


namespace vetvance_api.Controllers
{
    public class ProcessController : Controller
    {
        // instantiate model
        VetvanceEntities db = new VetvanceEntities();

        // POST: /Forgot
        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Forgot(string email, string lastname, string redirect)
        {
            user _user = (from p in db.users where p.email == email && p.lastname == lastname select p).FirstOrDefault();
            if (_user != null)
            {
                sso _sso = (from p in db.ssoes where p.guid == _user.guid select p).FirstOrDefault();

                // retain info
                ViewBag.email = email;
                ViewBag.lastname = lastname;
                ViewBag.securityquestion = _sso.securityquestion;
                ViewBag.guid = _sso.guid;

                return View(redirect);
            }
            else
            {
                return View();
            }
        }

        // POST: /Reset (SECURITYANSWER) (PASSWORD)
        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Reset(string securityanswer, string password, Guid guid, string redirect)
        {
            sso _sso = (from p in db.ssoes where p.securityanswer == securityanswer && p.guid == guid select p).FirstOrDefault();

            if (_sso != null)
            {
                _sso.password = password;

                // update and save
                db.Entry(_sso).State = System.Data.EntityState.Modified;
                db.SaveChanges();

                // authenticate
                return Authenticate(_sso.email, password, "connect", redirect, "blank");
            }
            else
            {
                return View();
            }
        }

        // GET: /Authenticate - Accepts (REDIRECT)
        [HttpGet]
        [ValidateInput(true)]
        public ActionResult Authenticate(string redirect)
        {
            if (redirect == "") redirect = "/";

            // prevent open redirection attacks
            if (redirect.Substring(0, 1) != "/") redirect = "/";

            // disconnect
            FormsAuthentication.SignOut();
            Session.Clear();
            Session.Abandon();

            // redirect
            return Redirect(redirect);
        }

        // POST: /Authenticate - Accepts (EMAIL), (PASSWORD), (ACTION), (REDIRECT)
        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Authenticate(string email, string password, string process, string redirect, string promocode)
        {

            Debug.WriteLine("AUTHENTICATE");
            
            if (redirect == "") redirect = "/";
            
            // prevent open redirection attacks
            if (redirect.Substring(0, 1) != "/") redirect = "/";

            try
            {
                // connect to SSO
                sso _sso = (from p in db.ssoes where p.email == email && p.password == password select p).FirstOrDefault();

                // connect LOCAL via GUID
                user _user = db.users.Where(p => p.guid == _sso.guid).FirstOrDefault();

                // update last login
                _user.lastlogin = _user.currentlogin;
                _user.currentlogin = DateTime.Now;
                db.Entry(_user).State = System.Data.EntityState.Modified;
                db.SaveChanges();

                // check if user is ready for connect
                bool _connect = true;
                if (_user.zip == null) _connect = false;
                if (string.IsNullOrEmpty(_user.profession)) _connect = false;
                if (string.IsNullOrEmpty(_user.title)) _connect = false;
                //if (_user.userinterests == null) _connect = false;
                if (string.IsNullOrEmpty(_user.school)) _connect = false;
                if (_user.graduation == null) _connect = false;

                // add session value for the application
                Session["guid"] = _user.guid; // not required
                Session["userid"] = _user.userid; // navigational entity
                Session["usertypeid"] = _user.usertypeid; // navigational entity

                // variant sessions
                Session["name"] = _user.firstname + ' ' + _user.lastname; // not required
                Session["email"] = _user.email; // navigational entity
                Session["connect"] = _connect; // connect entity
                Session["avatar"] = _user.avatar; // not required
                Session["graduation"] = _user.graduation; // not required

                // check remember - set/remove cookie
                if (Request.Form["remember"] == "on")
                {
                    // set cookie (1 month)
                    HttpCookie _cookie = new HttpCookie("signin");
                    _cookie["email"] = _user.email;
                    _cookie.Expires = DateTime.Now.AddMonths(1);
                    Response.Cookies.Add(_cookie);
                }
                else
                {
                    // remove cookie - expire
                    HttpCookie _cookie = new HttpCookie("signin");
                    _cookie.Expires = DateTime.Now.AddMonths(-1);
                    Response.Cookies.Add(_cookie);
                }

                // authenticate
                FormsAuthentication.SetAuthCookie(_user.email, false);

                Debug.WriteLine("ATER SETTING COOKIE");


                // redirect
                if (string.IsNullOrEmpty(promocode))
                {
                    return Redirect(redirect);
                }
                else
                {
                    return Redirect(redirect + "?pc=" + promocode);
                }
            } 
            catch
            {
                // show error
                Debug.WriteLine("ERROR I AUTHENTICATE");
                return View();
            }
        }

        // POST: /UpdateProfile - Accepts (USERID), (AVATAR)
        [HttpPost]
        [ValidateInput(true)]
        public ActionResult UpdateProfile(int id,
            string firstname,
            string lastname,
            string email,
            string zip,
            string profession,
            string title,
            string school,
            int graduation,
            string password,
            string securityquestion,
            string securityanswer, 
            string optpublic,
            string optemail,
            HttpPostedFileBase avatar,
            string redirect)
        {
            // convert boolean
            bool _optemail = false;
            bool _optpublic = false;

            if (optemail == "on") { _optemail = true; }
            if (optpublic== "on") { _optpublic = true; }

            try {
                user _user = (from p in db.users where p.userid == id select p).FirstOrDefault();
                sso _sso = (from p in db.ssoes where p.guid == _user.guid select p).FirstOrDefault();

                // update SSO
                _sso.email = email;
                _sso.password = password;
                _sso.securityquestion = securityquestion;
                _sso.securityanswer = securityanswer;

                // update and save SSO
                db.Entry(_sso).State = System.Data.EntityState.Modified;
                db.SaveChanges();

                // update LOCAL profile
                _user.firstname = firstname;
                _user.lastname = lastname;
                _user.email = email;
                _user.optemail = _optemail;
                _user.optpublic = _optpublic;
                _user.graduation = graduation;
                _user.school = school;
                _user.title = title;
                _user.profession = profession;
                _user.zip = zip;

                zipcode _zipcode = (from p in db.zipcodes where p.zipcode1 == zip select p).FirstOrDefault();
                if (_zipcode != null)
                {
                    _user.city = _zipcode.cityname;
                    _user.state = _zipcode.state;
                }

                // check for avatar
                //string[] validFileTypes = {"bmp", "jpg", "png", "gif"};
                //bool isValidType = false;

                if (avatar != null)
                {
                    //for (int i = 0; i < validFileTypes.Length; i++)
                    //{
                    //    if (System.IO.Path.GetExtension(avatar.FileName) == "." + validFileTypes[i]) { isValidType = true; }
                    //}

                    //if (avatar != null && avatar.ContentLength > 0 && isValidType)
                    if (avatar != null && avatar.ContentLength > 0)
                    {
                        // create distinct user id
                        string _filename = id + "_" + avatar.FileName;

                        // save on disk
                        avatar.SaveAs(HttpContext.Server.MapPath("~/Contents/avatars/" + _filename));

                        // save file name on db
                        _user.avatar = _filename;
                    }
                }
                // update and save
                db.Entry(_user).State = System.Data.EntityState.Modified;
                db.SaveChanges();

                // check if user is ready for connect
                bool _connect = true;
                if (_user.zip == null) _connect = false;
                if (string.IsNullOrEmpty(_user.profession)) _connect = false;
                if (string.IsNullOrEmpty(_user.title)) _connect = false;
                //if (_user.userinterests == null) _connect = false;
                if (string.IsNullOrEmpty(_user.school)) _connect = false;
                if (_user.graduation == null) _connect = false;
                if (_user.optpublic != true) _connect = false;
                if (_user.optemail != true) _connect = false;

                // reset session for variant
                Session["name"] = _user.firstname + ' ' + _user.lastname; // not required
                Session["email"] = _user.email; // navigational entity
                Session["connect"] = _connect; // connect entity
                Session["avatar"] = _user.avatar; // not required

                // re-authenticate
                return Authenticate(email, password, "connect", redirect,"blank");
            }
            catch {
                // show error
                return View();
            }

            
        }

        [HttpGet]
        public ActionResult Testemail()
        {
            try
            {
                // read email content
                WebClient client = new WebClient();

                // detect type of registration
                string downloadString = downloadString = client.DownloadString("http://vetvance-api.dev.thebloc.com/email/welcome?guid=0A010110-E8CB-48A9-B314-4A7123AB3E77");

                // send welcome email
                vetvance_api.Controllers.APIController _controller = new APIController();
                _controller.sendEmail("no-reply@vetvance.com", "dgomez@thebloc.com", "[TEST] Welcome to Vetvance", downloadString);
                _controller.sendEmail("no-reply@vetvance.com", "jlee@thebloc.com", "[TEST] Welcome to Vetvance", downloadString);
            }
            catch { }
            return View();
        }

        // POST: /Register - Accepts (EMAIL), (PASSWORD), (ACTION), (REDIRECT)
        [HttpPost]
        [ValidateInput(true)]
        public ActionResult Register(string email, string edu_email, string password, string securityquestion, string securityanswer, string firstname, string lastname, int careertypeid, string school, int graduation, string optemail, string optterms, string redirect)
        {
            
            Debug.WriteLine("=====> Register");
            Debug.WriteLine("=====> School =" + school);
            Debug.WriteLine("=====> Graduation =" + graduation);
            Debug.WriteLine("=====> careertypeid =" + careertypeid);

            try {
                // create new GUID
                Guid _guid = Guid.NewGuid();

                if ((string.IsNullOrEmpty(email)) || (string.IsNullOrEmpty(password))) { throw new System.ArgumentException("Invalid email and password"); }
                
                // convert boolean
                bool _optemail = false;
                bool _optterms = false;

                if (optemail == "on") { _optemail = true; }
                if (optterms == "on") { _optterms = true; }
                
                // check duplicate
                sso _sso = (from p in db.ssoes where p.email == email select p).FirstOrDefault();
                if (_sso == null)
                {

                    // connect to SSO
                    _sso = new sso();
                    _sso.email = email;
                    //_sso.edu_email = edu_email;
                    _sso.password = password;
                    _sso.guid = _guid;
                    _sso.securityquestion = securityquestion;
                    _sso.securityanswer = securityanswer;
                    _sso.createdate = DateTime.Now;

                    // add & save
                    db.ssoes.Add(_sso);
                   db.SaveChanges();

                    // connect to LOCAL
                    user _user = new user();
                    _user.guid = _guid;
                    _user.email = email;
                    _user.eduemail = edu_email;
                    _user.firstname = firstname;
                    _user.lastname = lastname;
                    _user.careertypeid = careertypeid;
                    _user.school = school;
                    _user.graduation = graduation;
                    _user.optpublic = true;
                    _user.optemail = _optemail;
                    _user.optterms = _optterms;
                    _user.usertypeid = 1;
                    _user.avatar = "default.png";
                    _user.createdate = DateTime.Now;

                    // add & save
                    db.users.Add(_user);
                   db.SaveChanges();
                    // get zuku promo code
                    zukupromo _promo = (from p in db.zukupromos where p.userid == null select p).First();

                    try
                    {
                        // read email content
                        WebClient client = new WebClient();

                        // detect type of registration
                        string downloadString = null;

                            
                        if (string.IsNullOrEmpty(edu_email))
                        {
                            downloadString = client.DownloadString("http://www.vetvance.com/email/welcome?guid=" + _guid);
                        }
                        else
                        {

                            // get zuku promo code
                            //zukupromo _promo = (from p in db.zukupromos where p.userid == null select p).First();
                            // assign code
                            _promo.userid = _user.userid;

                            // update promo code & save
                            db.Entry(_promo).State = System.Data.EntityState.Modified;
                            db.SaveChanges();

                            downloadString = client.DownloadString("http://www.vetvance.com/email/welcomezuku?guid=" + _guid + "&code=" + _promo.promocode);
                        }

                        // send welcome email
                        /*
                        vetvance_api.Controllers.APIController _controller = new APIController();
                        _controller.sendEmail("no-reply@vetvance.com", _user.email, "Welcome to Vetvance", downloadString);
                        */
                    }
                    catch { }

                     // authenticate automatically & redirect

                    if (string.IsNullOrEmpty(edu_email))
                    {
                        return Authenticate(email, password, "connect", "/registersuccessreg", null);
                    }
                    else
                    {
                        try
                        {

                        var schoolcheck = (from p in db.schools where p.schoolname == school select p);
                        var schoolcount = schoolcheck.Count();

                            if (schoolcount > 0 && (_user.graduation == 2014 || _user.graduation == 2015) && _user.careertypeid == 1 && edu_email.Contains(".edu"))
                            {

                                Debug.WriteLine("ELIGIBLE");

                                return Authenticate(email, password, "connect", redirect, _promo.promocode);

                            }
                            else
                            {
                                Debug.WriteLine("NOT ELIGIBLE");

                                return Authenticate(email, password, "connect", "/registerzukkunoteligible", null);

                            }
                        } catch(Exception e) {
                            Debug.WriteLine("critera check exception :" + e.Message);
                            return View();

                        }

                    }

               }
                else
                {
                    Debug.WriteLine("USER EXISTS");
                    throw new System.ArgumentException("Duplicate user exists");
                }


            } catch (Exception e)      {
                // show error
                Debug.WriteLine("SOME ERROR :" + e.Message);
                return View();
            }
        }
    }
}
