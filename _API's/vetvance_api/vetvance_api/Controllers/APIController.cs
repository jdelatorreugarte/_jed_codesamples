using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using vetvance_api.Models;
using System.Diagnostics;
using System.Data.Objects.SqlClient;

namespace vetvance_api.Controllers
{
    public class APIController : ApiController
    {

        // declare entity model
        private VetvanceEntities db = new VetvanceEntities();

        // SSO
        [HttpPost]
        public IEnumerable<sso> SSO(Guid id)
        {
            return (from p in db.ssoes where p.guid == id select p);
        }
        
        // Users
        [HttpGet]
        public IEnumerable<user> Users(int id)
        {
            return (from p in db.users where p.userid == id select p);
        }

        // Users (EMAIL) (LASTNAME) - Page: Forget Password
        [HttpGet]
        public IEnumerable<user> Users(string email, string lastname)
        {
            return (from p in db.users where p.email == email && p.lastname == lastname select p);
        }

        // ConnectionInfo
        [HttpGet]
        public IQueryable ConnectionInfo(int id, int connectid)
        {
            return (from p in db.users
                    where p.userid == connectid
                    select new
                    {
                        connection = p,
                        connected = (from c in db.userconnections where c.userid == id && c.connectid == connectid select c).Any()
                    }
                    );
        }

        // Disconnect (USERID, CONNECTID)
        [HttpGet]
        public void Disconnect(int id, int connectid)
        {
            userconnection _userconnection = (from p in db.userconnections where p.userid == id && p.connectid == connectid select p).FirstOrDefault();

            if (_userconnection != null)
            {
                db.userconnections.Remove(_userconnection);
                db.SaveChanges();
            }

        }

        // Connect
        [HttpGet]
        public Boolean Connect(int id, int connectid)
        {
            // check valid user
            user _user = (from u in db.users where u.userid == id select u).FirstOrDefault();
            user _connect = (from c in db.users where c.userid == connectid select c).FirstOrDefault();
            bool stat = false;
            // if both exists
            if ((_user != null) && (_connect != null))
            {
                userconnection _userconnection = new userconnection();

                _userconnection.userid = id;
                _userconnection.connectid = connectid;
                _userconnection.createdate = DateTime.Now;

                db.userconnections.Add(_userconnection);
                db.SaveChanges();
                stat = true;

            }
            // temporary
            stat = true;

            return stat;
        }

        //// UsersList (SORT) - Page: Connect
        //[HttpGet]
        //public IEnumerable<user> UsersList(string sort, bool newmember, bool student, bool veterinarian, bool technician, string school, int graduation, int organizationid)
        //{
        //    IEnumerable<user> _user = (from p in db.users select p);

        //    // filter for valid connections
        //    _user.Where(p => p.zip != null && p.profession != null && p.title != null && p.school != null && p.graduation != null);

        //    // sequential filter
        //    if (newmember) { _user = _user.Where(p => p.createdate > DateTime.Now.AddMonths(-1)); }
        //    if (student) { _user = _user.Where(p => p.careertype.careertypename.Contains("Student")); }
        //    if (veterinarian) { _user = _user.Where(p => p.careertype.careertypename.Contains("Veterinary/DVM")); }
        //    if (technician) { _user = _user.Where(p => p.careertype.careertypename.Contains("Veterinary Technician")); }
        //    if (!string.IsNullOrEmpty(school)) { _user = _user.Where(p => p.school == school); }
        //    if (graduation > 0) { _user = _user.Where(p => p.graduation == graduation); }
        //    if (organizationid > 0) { _user = _user.Where(p => p.affiliations.Any(o=> o.organizationid == organizationid)); }

        //    // sort order
        //    switch (sort)
        //    {
        //        case "active":
        //            _user = _user.OrderByDescending(p => p.lastlogin);
        //            break;
        //        case "zip":
        //            _user = _user.OrderBy(p => p.lastname).ThenBy(p => p.firstname);
        //            break;
        //        default:
        //            _user = _user.OrderBy(p => p.lastname).ThenBy(p => p.firstname);
        //            break;
        //    }

        //    return _user;

        //}

        // findzip
        [HttpGet]
        public IQueryable findzip(string id, int distance)
        {
            return (from p in db.ZipProximity(id, distance) select p);
        }

        // UsersList (SORT) - Page: Connect
        [HttpGet]
        public IQueryable UsersList(int id, string sort, bool newmember, bool student, bool veterinarian, bool technician, string school, int graduation, int organizationid,
            bool acquatic,
            bool mixedcattle,
            bool mixedswine,
            bool mixedequine,
            bool mixedcompanion,
            bool smallcanine,
            bool smallfeline,
            bool smallexotics,
            bool foodbeef,
            bool fooddairy,
            bool foodcow,
            bool foodpoultry,
            bool foodswine,
            bool equine,
            bool smallruminant,
            bool labanimal,
            bool government,
            bool armedforces,
            bool publichealth,
            bool industry,
            bool zoo,
            bool other,
            bool anesthesiology,
            bool animalwelfare,
            bool behavior,
            bool clinicalpharmacology,
            bool dermatology,
            bool emergencyandcriticalcare,
            bool internalmedicine,
            bool laboratorymedicine,
            bool microbiology,
            bool nutrition,
            bool ophthalmology,
            bool pathology,
            bool preventivemedicine,
            bool radiology,
            bool sportsmedicineandrehabilitation,
            bool surgery,
            bool theriogenology,
            bool toxicology,
            bool veterinarypractitioners,
            bool zoologicalmedicine
            )
        {


            IEnumerable<user> _user = (from p in db.users where p.userid != id select p);

            // filter for valid connections
            _user.Where(p => p.zip != null && p.profession != null && p.title != null && p.school != null && p.graduation != null);

            // sequential filter
            if (newmember) { _user = _user.Where(p => p.createdate > DateTime.Now.AddMonths(-1)); }
            if (student) { _user = _user.Where(p => p.careertype.careertypename.Contains("Student")); }
            if (veterinarian) { _user = _user.Where(p => p.careertype.careertypename.Contains("Veterinarian/DVM")); }
            if (technician) { _user = _user.Where(p => p.careertype.careertypename.Contains("Veterinary Technician")); }
            if (!string.IsNullOrEmpty(school)) { _user = _user.Where(p => p.school == school); }
            if (graduation > 0) { _user = _user.Where(p => p.graduation == graduation); }
            if (organizationid > 0) { _user = _user.Where(p => p.affiliations.Any(o => o.organizationid == organizationid)); }
            if (acquatic) { _user = _user.Where(p => p.userinterests.Any(s => s.acquatic == true)); }
            if (mixedcattle) { _user = _user.Where(p => p.userinterests.Any(s => s.mixedcattle == true)); }
            if (mixedswine) { _user = _user.Where(p => p.userinterests.Any(s => s.mixedswine == true)); }
            if (mixedequine) { _user = _user.Where(p => p.userinterests.Any(s => s.mixedequine == true)); }
            if (mixedcompanion) { _user = _user.Where(p => p.userinterests.Any(s => s.mixedcompanion == true)); }
            if (smallcanine) { _user = _user.Where(p => p.userinterests.Any(s => s.smallcanine == true)); }
            if (smallfeline) { _user = _user.Where(p => p.userinterests.Any(s => s.smallfeline == true)); }
            if (smallexotics) { _user = _user.Where(p => p.userinterests.Any(s => s.smallexotics == true)); }
            if (foodbeef) { _user = _user.Where(p => p.userinterests.Any(s => s.foodbeef == true)); }
            if (fooddairy) { _user = _user.Where(p => p.userinterests.Any(s => s.fooddairy == true)); }
            if (foodcow) { _user = _user.Where(p => p.userinterests.Any(s => s.foodcow == true)); }
            if (foodpoultry) { _user = _user.Where(p => p.userinterests.Any(s => s.foodpoultry == true)); }
            if (foodswine) { _user = _user.Where(p => p.userinterests.Any(s => s.foodswine == true)); }
            if (equine) { _user = _user.Where(p => p.userinterests.Any(s => s.equine == true)); }
            if (smallruminant) { _user = _user.Where(p => p.userinterests.Any(s => s.smallruminant == true)); }
            if (labanimal) { _user = _user.Where(p => p.userinterests.Any(s => s.labanimal == true)); }
            if (government) { _user = _user.Where(p => p.userinterests.Any(s => s.government == true)); }
            if (armedforces) { _user = _user.Where(p => p.userinterests.Any(s => s.armedforces == true)); }
            if (publichealth) { _user = _user.Where(p => p.userinterests.Any(s => s.publichealth == true)); }
            if (industry) { _user = _user.Where(p => p.userinterests.Any(s => s.industry == true)); }
            if (zoo) { _user = _user.Where(p => p.userinterests.Any(s => s.zoo == true)); }
            if (other) { _user = _user.Where(p => p.userinterests.Any(s => s.other == true)); }
            if (anesthesiology) { _user = _user.Where(p => p.userspecialties.Any(s => s.anesthesiology == true)); }
            if (animalwelfare) { _user = _user.Where(p => p.userspecialties.Any(s => s.animalwelfare == true)); }
            if (behavior) { _user = _user.Where(p => p.userspecialties.Any(s => s.behavior == true)); }
            if (clinicalpharmacology) { _user = _user.Where(p => p.userspecialties.Any(s => s.clinicalpharmacology == true)); }
            if (dermatology) { _user = _user.Where(p => p.userspecialties.Any(s => s.dermatology == true)); }
            if (emergencyandcriticalcare) { _user = _user.Where(p => p.userspecialties.Any(s => s.emergencyandcriticalcare == true)); }
            if (internalmedicine) { _user = _user.Where(p => p.userspecialties.Any(s => s.internalmedicine == true)); }
            if (laboratorymedicine) { _user = _user.Where(p => p.userspecialties.Any(s => s.laboratorymedicine == true)); }
            if (microbiology) { _user = _user.Where(p => p.userspecialties.Any(s => s.microbiology == true)); }
            if (nutrition) { _user = _user.Where(p => p.userspecialties.Any(s => s.nutrition == true)); }
            if (ophthalmology) { _user = _user.Where(p => p.userspecialties.Any(s => s.ophthalmology == true)); }
            if (pathology) { _user = _user.Where(p => p.userspecialties.Any(s => s.pathology == true)); }
            if (preventivemedicine) { _user = _user.Where(p => p.userspecialties.Any(s => s.preventivemedicine == true)); }
            if (radiology) { _user = _user.Where(p => p.userspecialties.Any(s => s.radiology == true)); }
            if (sportsmedicineandrehabilitation) { _user = _user.Where(p => p.userspecialties.Any(s => s.sportsmedicineandrehabilitation == true)); }
            if (surgery) { _user = _user.Where(p => p.userspecialties.Any(s => s.surgery == true)); }
            if (theriogenology) { _user = _user.Where(p => p.userspecialties.Any(s => s.theriogenology == true)); }
            if (toxicology) { _user = _user.Where(p => p.userspecialties.Any(s => s.toxicology == true)); }
            if (veterinarypractitioners) { _user = _user.Where(p => p.userspecialties.Any(s => s.veterinarypractitioners == true)); }
            if (zoologicalmedicine) { _user = _user.Where(p => p.userspecialties.Any(s => s.zoologicalmedicine == true)); }

            // sort order
            switch (sort)
            {
                case "active":
                    _user = _user.OrderByDescending(p => p.lastlogin);
                    break;
                case "zip":
                    _user = _user.OrderBy(p => p.lastname).ThenBy(p => p.firstname);
                    break;
                default:
                    _user = _user.OrderBy(p => p.lastname).ThenBy(p => p.firstname);
                    break;
            }

            

             
            var _quser = (from p in _user
                     select new
                     {
                         email = p.email,
                         firstname = p.firstname,
                         lastname = p.lastname,
                         school = p.school,
                         graduation = p.graduation,
                         zip = p.zip,
                         city = (from c in db.zipcodes where c.zipcode1 == p.zip select c.cityname),
            			 state = (from s in db.zipcodes where s.zipcode1 == p.zip select s.state),
                         profession = p.profession,
                         title = p.title,
                         avatar = p.avatar,
                         lastlogin = p.lastlogin,
                         userid = p.userid,
                         optpublic = p.optpublic
                     });


            return (from p in _quser
                    select new
                    {
                        user = p,
                        connected = (from c in db.userconnections where c.userid == id && c.connectid == p.userid select c).Any()
                    }).AsQueryable();

        
        
        
        }

        // ConnectBrowse (USERID)
        [HttpGet]
        public IQueryable ConnectBrowse(int id)
        {
            return (from p in db.users
                    where p.userid == id
                    select new
                    {
                        near = (from nm in db.users where nm.zip == p.zip && nm.userid != p.userid orderby nm.lastlogin select nm).Take(25),
                        school = (from sc in db.users where sc.school == p.school && sc.userid != p.userid orderby sc.lastlogin select sc).Take(25),
                        gradute = (from gr in db.users where gr.graduation == p.graduation && gr.userid != p.userid orderby gr.lastlogin select gr).Take(25),
                    }
                );
        }

        // UserConnections (USERID) - Page: User Profile
        [HttpGet]
        public IEnumerable<userconnection> UserConnections(int id, string sort)
        {
            IEnumerable<userconnection> _userconnection;

            switch (sort)
            {
                case "active":
                    _userconnection = (from p in db.userconnections where p.userid == id && p.user.zip != null orderby p.user.lastlogin descending select p);
                    break;
                case "zip":
                    _userconnection = (from p in db.userconnections where p.userid == id && p.user.zip != null orderby p.user.lastname, p.user.firstname select p);
                    break;
                default:
                    _userconnection = (from p in db.userconnections where p.userid == id && p.user.zip != null orderby p.user.lastname, p.user.firstname select p);
                    break;
            }
            return _userconnection;
        }

        // NewConnections (USERID) - Page: User Profile and Global
        [HttpGet]
        public IEnumerable<userconnection> NewConnections(int id)
        {
            // get last login
            user _user = (from p in db.users where p.userid == id select p).FirstOrDefault();

            // return new connection since last login
            DateTime _since = DateTime.Now.AddMonths(-1); // since 30 days ago

            // return data
            return (from p in db.userconnections where p.userid == id && p.createdate > _since orderby p.createdate descending select p);
        }

        // MutualConnections (USERID) (CONNECTID)
        [HttpGet]
        public IEnumerable<userconnection> MutualConnections(int id, int connectid)
        {
            return (from p in db.userconnections where p.userid == id && db.userconnections.Any(c=> c.userid == connectid && c.connectid == p.connectid) orderby p.user.lastname, p.user.firstname select p);
        }

        // UserSpecialties (USERID) - Page: User Profile
        [HttpGet]
        public IEnumerable<userspecialty> UserSpecialties(int id)
        {
            return (from p in db.userspecialties where p.userid == id select p);
        }

        // updateUserSpecialist - Page: Upadate Profile
        [HttpGet]
        public IEnumerable<user> updateUserSpecialty(
            int id,
            bool anesthesiology,
            bool animalwelfare,
            bool behavior,
            bool clinicalpharmacology,
            bool dermatology,
            bool emergencyandcriticalcare,
            bool internalmedicine,
            bool laboratorymedicine,
            bool microbiology,
            bool nutrition,
            bool ophthalmology,
            bool pathology,
            bool preventivemedicine,
            bool radiology,
            bool sportsmedicineandrehabilitation,
            bool surgery,
            bool theriogenology,
            bool toxicology,
            bool veterinarypractitioners,
            bool zoologicalmedicine
            )
        {
            // find record
            userspecialty _userspecialty = (from p in db.userspecialties where p.userid == id select p).FirstOrDefault();

            // if empty - create
            bool _new = false;
            if (_userspecialty == null)
            {
                _new = true;
                _userspecialty = new userspecialty();
                _userspecialty.userid = id;
            }

            // update attributes
            _userspecialty.anesthesiology = anesthesiology;
            _userspecialty.animalwelfare = animalwelfare;
            _userspecialty.behavior = behavior;
            _userspecialty.clinicalpharmacology = clinicalpharmacology;
            _userspecialty.dermatology = dermatology;
            _userspecialty.emergencyandcriticalcare = emergencyandcriticalcare;
            _userspecialty.internalmedicine = internalmedicine;
            _userspecialty.laboratorymedicine = laboratorymedicine;
            _userspecialty.microbiology = microbiology;
            _userspecialty.nutrition = nutrition;
            _userspecialty.ophthalmology = ophthalmology;
            _userspecialty.pathology = pathology;
            _userspecialty.preventivemedicine = preventivemedicine;
            _userspecialty.radiology = radiology;
            _userspecialty.sportsmedicineandrehabilitation = sportsmedicineandrehabilitation;
            _userspecialty.surgery = surgery;
            _userspecialty.theriogenology = theriogenology;
            _userspecialty.toxicology = toxicology;
            _userspecialty.veterinarypractitioners = veterinarypractitioners;
            _userspecialty.zoologicalmedicine = zoologicalmedicine;

            if (_new == true)
            {
                // if new record
                db.userspecialties.Add(_userspecialty);
            }
            else
            {
                // existing recod
                db.Entry(_userspecialty).State = System.Data.EntityState.Modified;
            }

            // save
            db.SaveChanges();

            return (from p in db.users where p.userid == id select p);
        }


        // UserInterest (USERID) - Page: User Profile
        [HttpGet]
        public IEnumerable<userinterest> UserInterests(int id)
        {
            return (from p in db.userinterests where p.userid == id select p);
        }

        // updateUserInterest (USERID) - Page: Update Profile
        [HttpGet]
        public IEnumerable<user> updateUserInterest(
            int id,
            bool acquatic, 
            bool mixedcattle,
            bool mixedswine,
            bool mixedequine,
            bool mixedcompanion,
            bool smallcanine,
            bool smallfeline,
            bool smallexotics,
            bool foodbeef,
            bool fooddairy,
            bool foodcow,
            bool foodpoultry,
            bool foodswine,
            bool equine,
            bool smallruminant,
            bool labanimal,
            bool government,
            bool armedforces,
            bool publichealth,
            bool industry,
            bool zoo,
            bool other
            )
        {

            // find record
            userinterest _userinterest = (from p in db.userinterests where p.userid == id select p).FirstOrDefault();

            // if empty - create
            bool _new = false;
            if (_userinterest == null) {
                _new = true;
                _userinterest = new userinterest();
                _userinterest.userid = id;
            }

            // update attributes
            _userinterest.acquatic = acquatic;
            _userinterest.mixedcattle = mixedcattle;
            _userinterest.mixedswine = mixedswine;
            _userinterest.mixedequine = mixedequine;
            _userinterest.mixedcompanion = mixedcompanion;
            _userinterest.smallcanine = smallcanine;
            _userinterest.smallfeline = smallfeline;
            _userinterest.smallexotics = smallexotics;
            _userinterest.foodbeef = foodbeef;
            _userinterest.fooddairy = fooddairy;
            _userinterest.foodcow = foodcow;
            _userinterest.foodpoultry = foodpoultry;
            _userinterest.foodswine = foodswine;
            _userinterest.equine = equine;
            _userinterest.smallruminant = smallruminant;
            _userinterest.labanimal = labanimal;
            _userinterest.government = government;
            _userinterest.armedforces = armedforces;
            _userinterest.publichealth = publichealth;
            _userinterest.industry = industry;
            _userinterest.zoo = zoo;
            _userinterest.other = other;

            if (_new == true)
            {
                // if new record
                db.userinterests.Add(_userinterest);
            }
            else
            {
                // existing recod
                db.Entry(_userinterest).State = System.Data.EntityState.Modified;
            }

            // save
            db.SaveChanges();

            // return user dataset
            return (from p in db.users where p.userid == id select p);
        }

        // Career Types - Page: Registration & Global
        [HttpGet]
        public IEnumerable<careertype> CareerTypes()
        {
            return db.careertypes;
        }

        // Schools - Page: Registration and Global
        [HttpGet]
        public IEnumerable<school> Schools()
        {
            return db.schools;
        }

        // UserSchools - Page: Registration and Global
        [HttpGet]
        public IQueryable UserSchools()
        {
            return (from p in db.users where p.school != null && p.school != ""
                    select new
                    {
                        schoolname = p.school
                    }).Distinct().OrderBy(c=>c.schoolname);
        }

        // UserJobs (USERID) - Page: User Profile & Global
        [HttpGet]
        public IEnumerable<userjob> UserJobs(int id)
        {
            return (from p in db.userjobs where p.userid == id orderby p.year select p);
        }

        // SaveJob (USERID) (TITLE) (COMPANY) (YEAR)
        [HttpGet]
        public IEnumerable<userjob> saveJob(int id, string title, string company, int year)
        {
            // create entity
            userjob _userjob = new userjob();
            _userjob.userid = id; 
            _userjob.title = title;
            _userjob.company = company;
            _userjob.year = year;

            // add and save
            db.userjobs.Add(_userjob);
            db.SaveChanges();

            return (from p in db.userjobs where p.userid == id orderby p.year select p);
        }

        // RemoveUserJob (JOBID)
        [HttpGet]
        public void removeUserJob(int id)
        {
            // find
            userjob _userjob = db.userjobs.Find(id);

            // remove and save
            db.userjobs.Remove(_userjob);
            db.SaveChanges();
        }

        // Organizations
        [HttpGet]
        public IEnumerable<organization> Organizations()
        {
            return (from p in db.organizations orderby p.organizationname, p.organizationtypename select p);
        }

        // SaveAffiliation (USERID) (ORGANIZATIONID)
        [HttpGet]
        public IEnumerable<affiliation> saveAffiliation(int id, int organizationid)
        {
            affiliation _affiliation = (from p in db.affiliations where p.userid == id && p.organizationid == organizationid select p).FirstOrDefault();

            // only when no duplicates
            if (_affiliation == null)
            {
                _affiliation = new affiliation();
                _affiliation.userid = id;
                _affiliation.organizationid = organizationid;

                // add and update
                db.affiliations.Add(_affiliation);
                db.SaveChanges();
            }

            return (from p in db.affiliations where p.userid == id select p);
        }

        // RemoveAffiliation (AFFILIATIONID)
        [HttpGet]
        //public void removeAffiliation(int id)
        public void removeAffiliation(int id, int organizationid)
        {
            Debug.WriteLine("id=" + id + "orgid= " + organizationid);   
            // find record
            //affiliation _affiliation = db.affiliations.Find(id);

            affiliation _affiliation = (from p in db.affiliations where p.userid == id && p.organizationid == organizationid select p).FirstOrDefault();

            if (_affiliation != null)
            {
                db.affiliations.Remove(_affiliation);
                db.SaveChanges();
            }

            // remove and save
            db.affiliations.Remove(_affiliation);
            db.SaveChanges();
        }

        // Affiliation (USERID)
        [HttpGet]
        public IEnumerable<affiliation> Affiliations(int id)
        {
            return (from p in db.affiliations where p.userid == id orderby p.organization.organizationname, p.organization.organizationtypename select p);
        }

        // searchSchool
        [HttpGet]
        public IEnumerable<school> searchSchool(string id)
        {
            return (from p in db.schools where p.schoolname.Contains(id) select p);
        }

        // Security Questions
        [HttpGet]
        public IEnumerable<securityquestion> SecurityQuestions()
        {
            return db.securityquestions;
        }

        // UserType (UserTypeID)
        [HttpGet]
        public IEnumerable<usertype> UserType(int id)
        {
            return (from p in db.usertypes where p.usertypeid == id select p);
        }

        // UserPaths/ID (USERID) (UserTypeID) - Page: Advance
        [HttpGet]
        public IQueryable UserPaths(int id, int usertypeid)
        {
            return (from p in db.courses
                    select new
                    {
                        course = p,
                        completed = (from c in db.modulestatus where c.courseid == p.courseid && c.userid == id select c).Count(),
                        total = (from t in db.modules where t.courseid == p.courseid select t).Count(),
                        student = (from st in db.usertypes where st.usertypeid == usertypeid select st.student),
                        job = (from jb in db.usertypes where jb.usertypeid == usertypeid select jb.job),
                        debt = (from dt in db.usertypes where dt.usertypeid == usertypeid select dt.debt),
                    });
        }

        // updateUserPath (UserID, Student, Job, Debt) - Page: Advance
        [HttpGet]
        public IEnumerable<user> updateUserPath(int id, bool student, bool? job, bool? debt)
        {
            usertype _usertype;

            if (debt == null)
            {
                if (job == null)
                {
                    _usertype = db.usertypes.Where(x => x.student == student && x.job == null && x.debt == null).FirstOrDefault();
                }
                else
                {
                    _usertype = db.usertypes.Where(x => x.student == student && x.job == job && x.debt == null).FirstOrDefault();
                }
                
            }
            else
            {
                _usertype = db.usertypes.Where(x => x.student == student && x.job == job && x.debt == debt).FirstOrDefault();
            }

            user _user = db.users.Where(x => x.userid == id).FirstOrDefault();

            _user.usertypeid = _usertype.usertypeid;

            db.Entry(_user).State = System.Data.EntityState.Modified;
            db.SaveChanges();

            return (from p in db.users where p.userid == id select p);
        }

        // Presenters - Page: WhoWeAre
        [HttpGet]
        public IEnumerable<presenter> Presenters()
        {
            return db.presenters.OrderBy(o=>o.presenterlastname).ThenBy(o=>o.presenterfirstname);
        }

        [HttpGet]
        public IQueryable Presenter(int id)
        {

            return (from p in db.presenters
                    where p.presenterid == id
                    select new
                    {
                        presenter = p,
                        presenterid = (from c in db.presenters where c.presenterid == id select c.presenterid).FirstOrDefault(),
                        presenterfirstname = (from c in db.presenters where c.presenterid == id select c.presenterfirstname).FirstOrDefault(),
                        presenterlastname = (from c in db.presenters where c.presenterid == id select c.presenterlastname).FirstOrDefault()
                    }
        );
        }


        // Course
        [HttpGet]
        public IEnumerable<cours> Course(int id)
        {
            return (from p in db.courses where p.courseid == id select p);
        }

        // CourseCourseType (CourseTypeID)
        [HttpGet]
        public IEnumerable<cours> CourseCourseType(int id)
        {
            return (from p in db.courses where p.coursetypeid == id select p);
        }

        // CourseList/ID (USERID) - Page:CourseList
        [HttpGet]
        public IQueryable CourseList(int id)
        {
            return (from p in db.courses select p);
        }

        // CourseModules/ID (COURSEID)(USERID) - Page:CourseLanding
        [HttpGet]
        public IQueryable CourseModules(int id, int userid)
        {
            return (from p in db.modules where p.courseid == id orderby p.sequence select new { 
                module = p,
                modulecomplete = (from ms in db.modulestatus where ms.userid == userid && ms.moduleid == p.moduleid && ms.courseid == id select ms.modulestatusid).Any(),
                course = (from c in db.courses where c.courseid == p.courseid select c)
            });
        }

        // Modules/ID (ModuleID)(UserID) - Page:Module (Mark as Complete)
        [HttpGet]
        public IQueryable Module(int id, int userid)
        {
            return (from p in db.modules
                    where p.moduleid == id
                    select new
                    {
                        module = p,
                        nextmodule = (from nm in db.modules where nm.courseid == p.courseid && nm.sequence > p.sequence orderby nm.sequence select nm.moduleid).FirstOrDefault(),
                        nextmodulename = (from nmn in db.modules where nmn.courseid == p.courseid && nmn.sequence > p.sequence orderby nmn.sequence select nmn.modulename).FirstOrDefault(),
                        previousmodule = (from pm in db.modules where pm.courseid == p.courseid && pm.sequence < p.sequence orderby pm.sequence descending select pm.moduleid).FirstOrDefault(),
                        previousmodulename = (from pmm in db.modules where pmm.courseid == p.courseid && pmm.sequence < p.sequence orderby pmm.sequence descending select pmm.modulename).FirstOrDefault(),
                        modulecomplete = (from s in db.modulestatus where s.userid == userid && s.moduleid == id select s.modulestatusid).Any(),
                        coursename = (from c in db.courses where c.courseid == p.courseid select c.coursename),
                        coursetypeid = (from t in db.courses where t.courseid == p.courseid select t.coursetypeid),
                        nextpresenterid = (from nmn in db.modules from pr in db.presenters where nmn.courseid == p.courseid && nmn.sequence > p.sequence && pr.presenterid == nmn.presenterid orderby nmn.sequence select pr.presenterid).FirstOrDefault(),
                        nextpresentername = (from nmn in db.modules from pr in db.presenters where nmn.courseid == p.courseid && nmn.sequence > p.sequence && pr.presenterid == nmn.presenterid orderby nmn.sequence select pr.presenterfirstname + " " + pr.presenterlastname).FirstOrDefault(),
                        prevpresenterid = (from nmn in db.modules from pr in db.presenters where nmn.courseid == p.courseid && nmn.sequence < p.sequence && pr.presenterid == nmn.presenterid orderby nmn.sequence select pr.presenterid).FirstOrDefault(),
                        prevpresentername = (from pm in db.modules from pr in db.presenters where pm.courseid == p.courseid && pm.sequence < p.sequence && pr.presenterid == pm.presenterid orderby pm.sequence select pr.presenterfirstname + " " + pr.presenterlastname).FirstOrDefault()
                    });

        }

        // MarkModuleComplete - Page:Module (MODULEID) (USERID)
        [HttpGet]
        public void MarkModuleComplete(int id, int userid)
        {
            // find record
            modulestatu _modulestatus = (from p in db.modulestatus where p.moduleid == id && p.userid == userid select p).FirstOrDefault();

            // if no record match
            if (_modulestatus == null)
            {
                module _module = (from p in db.modules where p.moduleid == id select p).FirstOrDefault();

                _modulestatus = new modulestatu();

                // add new module status
                _modulestatus.userid = userid;
                _modulestatus.moduleid = id;
                _modulestatus.courseid = _module.courseid;
                _modulestatus.createdate = DateTime.Now;

                // add and update
                db.modulestatus.Add(_modulestatus);
                db.SaveChanges();
            }
        }
        
        // GetLikeMinded (SURVEYQUESTIONID)
        [HttpGet]
        //public IQueryable getLikeMinded(int id)
        public IQueryable getLikeMinded(int id, int surveyquestionid)
        {
            return (from p in db.surveystatus
                    where p.surveyquestionid == surveyquestionid && p.userid != id
                    select new
                    {
                        user_id = p.userid,
                        user_firstname = (from u in db.users where u.userid == p.userid orderby u.lastname, u.firstname select u.firstname).FirstOrDefault(),
                        user_lastname = (from u in db.users where u.userid == p.userid orderby u.lastname, u.firstname select u.lastname).FirstOrDefault(),
                        avatar = (from u in db.users where u.userid == p.userid orderby u.lastname, u.firstname select u.avatar).FirstOrDefault()
                    });
        }

        // SaveSurveyAnswer (USERID) (SURVEYID) (SURVEYQUESTIONID)
        [HttpGet]
        public IQueryable saveSurveyAnswer(int id, int surveyid, int surveyquestionid)
        {
            //surveystatu _surveystatus = (from p in db.surveystatus where p.userid == id && p.surveyid == surveyid select p).FirstOrDefault();
            surveystatu _surveystatus = (from p in db.surveystatus where p.userid == id && p.surveyid == surveyid && p.surveyquestionid == surveyquestionid select p).FirstOrDefault();

            // add if empty
            if (_surveystatus == null)
            {
                _surveystatus = new surveystatu();

                _surveystatus.userid = id;
                _surveystatus.surveyid = surveyid;
                _surveystatus.surveyquestionid = surveyquestionid;
                _surveystatus.surveycompleted = true;
                _surveystatus.createdate = DateTime.Now;

                // add & update
                db.surveystatus.Add(_surveystatus);
                db.SaveChanges();
            }
            /*
            else
            {
                //Debug.WriteLine("_surveystatus.surveyid ecists..will not add questionid=" + surveyquestionid);
            }
            */
            return (from p in db.surveyquestions
                    where p.surveyid == surveyid
                    select new
                    {
                        surveryquestionid = p.surveyquestionid,
                        total = (from r in db.surveystatus where r.surveyquestionid == p.surveyquestionid select r.surveycompleted).Count()
                    }
                    );

            //return (from p in db.users where p.userid == id select p);
        }

        // Dictionary
        [HttpGet]
        public IEnumerable<dictionary> Dictionary()
        {
            return (from p in db.dictionaries orderby p.keyword select p);
        }

        // Search (KEYWORD)
        [HttpGet]
        public IQueryable Search(string id)
        {

            Debug.WriteLine("SEARCH id=" +  id);
            return (from p in db.modules
                    where p.modulename.Contains(id) || p.moduledescription.Contains(id)
                    orderby p.courseid, p.moduleid
                    select new { 
                        module = p,
                        coursename = (from c in db.courses where c.courseid == p.courseid select c.coursename),
                        coursetypeid = (from t in db.courses where t.courseid == p.courseid select t.coursetypeid),
                    });
        }

        // Contact (TOPIC) (NAME) (EMAIL) (MESSAGE)
        [HttpGet]
        public void Contact(string type, string topic, string name, string email, string message)
        {
            string _message = "Topic: " + topic + "<p>" + message + "</p>";

            switch (type)
            {
                case "support":
                    sendEmail(email, "anand.ramachandran@zoetis.com,Colleen.McClellan@zoetis.com,jlee@thebloc.com,dgomez@thebloc.com,jsuarez@thebloc.com,cpanetta@thebloc.com", "[Vetvance] Support Request from " + name + " @ " + DateTime.Now.ToString(), _message);
                    break;
                default:
                    sendEmail(email, "anand.ramachandran@zoetis.com,Colleen.McClellan@zoetis.com,jlee@thebloc.com,dgomez@thebloc.com,jsuarez@thebloc.com,cpanetta@thebloc.com", "[Vetvance] Feedback from " + name + " @ " + DateTime.Now.ToString(), _message);
                    break;
            }
        }

        // Topics (TOPICTYPE)
        [HttpGet]
        public IEnumerable<topic> Topics(string id)
        {
            return (from p in db.topics where p.topictype == id select p);
        }

        // PRIVATE: sendEmail (FROM) (TO) (SUBJECT) (MESSAGE)
        public bool sendEmail(string from, string to, string subject, string message)
        {
            bool _val = true;

            try {
                System.Net.Mail.MailMessage _mail = new System.Net.Mail.MailMessage();
                System.Net.Mail.SmtpClient _smtp = new System.Net.Mail.SmtpClient("localhost");

                _mail.From = new System.Net.Mail.MailAddress(from);
                _mail.To.Add(to);
                _mail.Subject = subject;
                _mail.IsBodyHtml = true; 
                _mail.Body = message;

                _smtp.Send(_mail);
            }
            catch {
                _val = false;
            }

            return _val;
        }
    }
}
