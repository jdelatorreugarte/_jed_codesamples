﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using vetvance_api.Models;

namespace vetvance_api.Controllers
{
    public class PrototypeController : Controller
    {
        // instantiate
        VetvanceEntities db = new VetvanceEntities();

        // GET: /Prototype/
        public ActionResult Index()
        {
            return View();
        }

        // GET: /Forgot/
        public ActionResult Forgot()
        {
            return View();
        }

        // GET: /Reset/
        public ActionResult Reset()
        {
            return View();
        }

        // GET: /Profile/
        public ActionResult UserProfile()
        {
            return View();
        }

        // GET: /UserPath/
        public ActionResult UserPath()
        {
            return View();
        }

        // GET: /Search/
        public ActionResult Search(string keyword)
        {
            return View((object)keyword);
        }

        // GET: /CourseList/
        public ActionResult CourseList()
        {
            return View();
        }

        // GET: /Course/
        public ActionResult Course(int id)
        {
            return View((object)id);
        }

        // GET: /CourseStatus/
        public ActionResult CourseStatus(int id)
        {
            return View((object)id);
        }

        // GET: /Course/
        public ActionResult Module(int id)
        {
            return View((object)id);
        }

        // GET: /Admin/Register
        public ActionResult Register()
        {
            return View();
        }

        // GET: /Login
        public ActionResult Login()
        {
            // check cookie
            if (Request.Cookies["setting"] != null)
            {
                ViewBag.cookie = Request.Cookies["setting"]["email"];
                ViewBag.remember = "checked";
            }

            return View();
        }

        // GET: /Contact
        public ActionResult Contact()
        {
            return View();
        }
    }
}
