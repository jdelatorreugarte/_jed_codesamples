﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using System.Web.Security;
using vetvance_api.Models;
using System.Diagnostics;


namespace vetvance_api.Controllers
{
    public class HomeController : Controller
    {
        // GET: /SignIn/
       //[RequireHttps]
        public ActionResult SignIn()
        {
            Debug.WriteLine("siginin");
            // check cookie
            try
            {
                ViewBag.email = Request.Cookies["signin"]["email"];
                ViewBag.remember = "checked";
            }
            catch { }

            return View();
        }

        // GET: /Unsubscribe/{GUID}
        public ActionResult Unsubscribe(Guid guid)
        {
            vetvance_api.Models.VetvanceEntities _db = new Models.VetvanceEntities();

            user _user = (from p in _db.users where p.guid == guid select p).FirstOrDefault();

            // if found
            if (_user != null)
            {
                _user.optemail = false;

                _db.Entry(_user).State = System.Data.EntityState.Modified;
                _db.SaveChanges();
            }

            return View();
        }

        //[RequireHttps]
        public ActionResult Register2()
        {
            return View();
        }


        // GET: /Register/
       //[RequireHttps]
        public ActionResult Register()
        {
            return View();
        }

        // GET: /RegisterZuku/
        //[RequireHttps]
        public ActionResult RegisterZuku()
        {
            return View();
        }


        // GET: /Register Zukku Sucess
        public ActionResult RegisterSuccess()
        {
            return View();
        }

        // GET: /Register Zukku Fail
        public ActionResult RegisterZukkuNotEligible()
        {
            return View();
        }



        // GET: /RegisteSuccessReg/
        public ActionResult RegisterSuccessReg()
        {
            return View();
        }



        // GET: /WelcomeZuku/
        public ActionResult WelcomeZuku()
        {
            return View();
        }


        // GET: /Register/
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // GET: /UpdateProfile/
        //[Authorize]
        //[Authorize, RequireHttps]
        public ActionResult UpdateProfile()
        {
            return View();
        }

        // GET: /Home/
        public ActionResult Home()
        {
            return View();
        }

        // GET: /Home/
        public ActionResult Index()
        {
            if (Request.Cookies["promo"] == null)
            {
                // if no cookie exists
                HttpCookie _cookie = new HttpCookie("promo");
                _cookie["video"] = "show";
                _cookie.Expires = DateTime.Now.AddMonths(1);
                Response.Cookies.Add(_cookie);
            }

            return View();
        }

        // GET: /Profile/
        [Authorize]
        public ActionResult UserProfile()
        {
            return View();
        }

        // GET: /Advance/
        public ActionResult Advance()
        {
            return View();
        }

        // GET: /Connect
        public ActionResult Connect()
        {
            return View();
        }

        // GET: /ConnectBrowse
        [Authorize]
        public ActionResult ConnectBrowse()
        {
            return View();
        }

        // GET: /Contact
        public ActionResult Contact()
        {
            return View();
        }

        // GET: /ContactSuccess
        public ActionResult ContactSuccess()
        {
            return View();
        }

        // GET: /Course
        public ActionResult Course(int id)
        {
            return View((object)id);
        }

        // GET: /CourseList
        public ActionResult CourseList()
        {
            return View();
        }

        // GET: /FindJob
        [Authorize]
        public ActionResult FindJob()
        {   
            return View();
        }

        // GET: /Module
        [Authorize]
        public ActionResult Module(int id)
        {
            vetvance_api.Models.VetvanceEntities _db = new Models.VetvanceEntities();

            video _video = (from p in _db.videos where p.moduleid == id select p).FirstOrDefault();

            ViewBag.videoid = _video.videocode;

            return View((object)id);
        }

        // GET: /Module2
        [Authorize]
        public ActionResult Module2(int id)
        {
            return View((object)id);
        }


        // GET: /Privacy
        public ActionResult Privacy()
        {
            return View();
        }

        // GET: /Search
        public ActionResult Search(string keyword)
        {
            return View((object)keyword);
        }

        // GET: /SiteMap
        public ActionResult SiteMap()
        {
            return View();
        }

        // GET: /TermsOfUse
        public ActionResult TermsOfUse()
        {
            return View();
        }

        // GET: Transcript
        public ActionResult Transcript(int id)
        {
            return View((object)id);
        }

        // GET: /WhoWeAre
        public ActionResult WhoWeAre()
        {
            return View();
        }

        // GET: 

        // GET: /Error/
        public ActionResult Error()
        {
            return View();
        }

        // GET: /Test
        [Authorize]
        public ActionResult Test(int id)
        {
            return View((object)id);
        }
    }
}
