﻿@{
    ViewBag.Title = "Connect";
}

@section scripts{
    <script src="/Contents/js/pages/connect.js"></script>
}

<div id="pageId" class="connect-container hidden-xs">
	<div class="header">
		<span>
			<h2>CONNECT WITH OTHER VETVANCE MEMBERS</h2>
			<p>Connecting with other veterinarians can help you form a network of people with similar interests so you can share experiences and advance your careers together.</p>
		</span>
        <span class="showme">
			<p>See VETVANCE members with whom you share interests.</p>
			<button class="btn btnShowMe">SHOW ME ></button>

            <!-- connect blurb -->
            <div style="color:#333 !important;" class="connect_blurb_showme">
                In order to connect with others, you must be a registered user with a complete profile. <a class="purple" href="/register">Register</a> or <a href="/signin" class="purple">sign in</a> now.
            </div>
        </span>
	</div>
	<div class="filter-results-container">
		<form method="post">			
			<div class="filter-container">
				<ul>
					<li><input type="checkbox" class="icheck" name="newmembers"> New Members </li>
					<!-- <li><input type="checkbox" class="icheck" name="mentors"> Mentors </li> -->
					@{if (Session["userid"] != null){<li><input type="checkbox" class="icheck" name="nearme"> Near me </li>}}
				</ul>
				<label>Profession <span class="profilter"><a href="#" class="checkbox-filters" data-switch="all">All</a> | 
					<a href="#" class="checkbox-filters" data-switch="none">None</a></span></label>
				<ul>
					<li><input type="checkbox" class="icheck educationchk" name="student"> Student </li>
					<li><input type="checkbox" class="icheck educationchk" name="veterinarian"> Veterinarian/DVM </li>
					<li><input type="checkbox" class="icheck educationchk" name="technician"> Veterinary Technician </li>
				</ul>
				<label>Veterinary School</label>				
				<select class="combobox" name="school" id="school">
					<option value="">Name of school</option>
				</select>
				<label>Graduation Date</label>
				<select class="combobox" name="graduation" id="graduation">
					<option value="">Any Year</option>
					@for (var i = (DateTime.Now.Year + 7); i >= (DateTime.Now.Year - 65); i--)
					{
		        <option value="@i">@i</option>
					}
				</select>
				<label>Interest/Practice <a href="#" id="btnInterest" class="btn btn-default btn-mini btn-popover">EDIT</a></label>
				<div class="interest-practice">
					<ul id="filtered-tags-interest" class="filtered-tags"></ul>
				</div>
				<label>Affiliation <span class="profilter"><a href="#" class="lookup btn-popover" id="btnLookUp">Look Up</a></span></label>
				<input class="txt" type="text" name="organizations" id="organizations" placeholder="Name of organization">
				<label>Expertise/Specialty<a href="#" class="btn btn-default btn-mini btn-popover" id="btnSpecialty">EDIT</a></label>
				<div class="expertise-specialty">
					<ul id="filtered-tags-specialty" class="filtered-tags"></ul>
				</div>
			</div>
			<div class="results-container">				
				<ul class="sort-filter">
					<li>Sort:</li>
					<li><a href="#" data-sort="default" class="active track-click" trkname="Connect|Alphabet|--">Alphabetical order</a></li>
					@{if (Session["userid"] != null){<li><a href="#" data-sort="zip" class="track-click" trkname="Connect|Nearest|--">Nearest first</a></li>}}
					<li><a href="#" data-sort="active"  class="track-click" trkname="Connect|Active|--">Most active first</a></li>
				</ul>
				<img class="loaderimg" src="/contents/imgs/loader.gif" alt="loading..." />
				<p class="no-results">
					Sorry, no results
				</p>
				<ul class="results-data"></ul>
			</div>
		</form>
	</div>
</div>
<script id="dataitem-template-backup" type="text/x-handlebars-template">
{{#each .}}
<li class="data-item">
	<img class="person img-circle" src="contents/avatars/{{avatar}}" alt="{{firstname}} {{lastname}}" />
	<a href="#" data-id="{{userid}}" class="btnConnect track-click" trkname="Connect|Browse|--"><img src="contents/imgs/connect/btnConnect.gif" alt="" /></a>
	<span>
		<strong>{{firstname}} {{lastname}}</strong>
		<p>{{profession}}</p>
		<p>{{school}} {{graduation}}</p>
		<p style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" title="{{user.city}}, {{user.state}}">{{city}}, {{state}}</p>
	</span>
</li>
{{/each}}
</script>
<script id="dataitem-template" type="text/x-handlebars-template">
{{#each .}}
<li class="data-item">
	<img data-id="{{user.userid}}" class="person btnConnect img-circle" src="contents/avatars/{{user.avatar}}" alt="{{user.firstname}} {{user.lastname}}" />
  {{#if connected}}
  	<a href="#" data-id="{{user.userid}}" class="btnConnect disconnect"><img src="contents/imgs/connect/connected.png" alt="" /></a>  	 
  {{else}}
  	<a href="#" data-id="{{user.userid}}" class="btnConnect connect track-click" trkname="Connect|Browse|connect">
        <span class="connect_tooltip" style="position:absolute;bottom:0px;right:0px;">
            <img class="connectimg track-click" trkname="Connect|Browse|connect" src="contents/imgs/connect/btnConnect.gif" alt="" />
            <!-- connect blurb -->
            <div style="color:#333 !important;" class="connect_blurb">
                </a>In order to connect with others, you must be a registered user with a complete profile. <a class="purple" href="/register">Register</a> or <a href="/signin?returnurl=/connect" class="purple">sign in</a> now.
            </div>
        </span>
  	</a>
  {{/if}}	
	<span class="info">
		<strong>{{user.firstname}} {{user.lastname}}</strong>
		<p>{{user.profession}}</p>
		<p>{{user.school}} {{user.graduation}}</p>
		<p style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;" title="{{user.city}}, {{user.state}}">{{user.city}}, {{user.state}}</p>
	</span>
</li>
{{/each}}
</script>
<div class="popover-container"></div>

<div id="connect-container-mobile" data-mobiletitle="Connect" data-mobileicon="connect-active.png"class="visible-xs mobile-container connect-container-mobile">
	<div id="connect-menu" class="connect-menu top-level">
		<p class="connect-text">Connecting with other students and veterinarians can help you form a network of people with similar interests so you can share experiences and advance your careers together.</p>

		<ul class="list-group hide-if-logged-out">
			<li data-filtertype="new-connections" data-parentpageid="connect-menu" data-target="new-connections-m" class="connect-link list-group-item">
				<a href="#" class="purple">View new connections<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></a>
				
			</li>
			<li data-filtertype="all-connections" data-parentpageid="connect-menu" data-target="all-connections-m" class="connect-link list-group-item">
				<a href="#" class="purple">View all connections<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" />
			</li></a>
				
		</ul>

		<p class="vetvance-recs uppercase gray hide-if-logged-out">Vetvance Recommendations</p>
		<ul class="list-group">
			<li data-filtertype="near-me" data-parentpageid="connect-menu" data-target="near-me-m" class="connect-link list-group-item">
				<a href="#" class="purple">Members near me<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></a>
				
			</li>
			<li data-filtertype="school" data-parentpageid="connect-menu" data-target="school-m" class="connect-link list-group-item">
				<a href="#" class="purple">Members from my school<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></a>
				
			</li>
			<li data-filtertype="graduation" data-parentpageid="connect-menu" data-target="graduation-m" class="connect-link list-group-item">
				<a href="#" class="purple">Members graduating in <span class="user-graduation-year">[xxxx]</span><img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></a>
				
			</li>
			<li data-filtertype="interest-area" data-parentpageid="connect-menu" data-target="interest-area-m" class="hidden connect-link list-group-item">
				<a href="#" class="purple">Members with an interest/practice in [interest/practice area]<img style="margin-top: -6px;" class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></a>
				
			</li>
		</ul>
		<ul class="list-group">
			<li data-filtertype="all" data-parentpageid="connect-menu" data-target="all-m" class="connect-link list-group-item">
				<a href="#" class="purple">View all members<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></a>
				
			</li>
		</ul>
	</div>
	<div style="display: none;" class="mobile-connected-list" id="new-connections-m">
		<p class="connected-list-title uppercase">New Connections</p>
		<ul>
			 <div class="loader">
			 	<center><img src="/Contents/imgs/loader.gif" /></center>
			 </div>
		</ul>
	</div>
	
	<div style="display: none;" class="mobile-connected-list" id="all-connections-m">
		<p class="connected-list-title uppercase">Connections</p>
		<ul>
			 <div class="loader">
			 	<center><img src="/Contents/imgs/loader.gif" /></center>
			 </div>
		</ul>
	</div>
	
	<div style="display: none;" class="mobile-connected-list" id="near-me-m">
		<p class="connected-list-title uppercase">Members Near Me</p>
		<ul>
			 <div class="loader">
			 	<center><img src="/Contents/imgs/loader.gif" /></center>
			 </div>
		</ul>
	</div>
	
	<div style="display: none;" class="mobile-connected-list" id="school-m">
		<p class="connected-list-title uppercase">Members From My School</p>
		<ul>
			 <div class="loader">
			 	<center><img src="/Contents/imgs/loader.gif" /></center>
			 </div>
		</ul>
	</div>
	
	<div style="display: none;" class="mobile-connected-list" id="graduation-m">
		<p class="connected-list-title uppercase">Members who Graduated in <span class="user-graduation-year">[xxxx]</span></p>
		<ul>
			 <div class="loader">
			 	<center><img src="/Contents/imgs/loader.gif" /></center>
			 </div>
		</ul>
	</div>
	
	<div style="display: none;" class="mobile-connected-list" id="interest-area-m">
		<p class="connected-list-title uppercase">Members Interested In</p>
		<ul>
			 <div class="loader">
			 	<center><img src="/Contents/imgs/loader.gif" /></center>
			 </div>
		</ul>
	</div>
	
	<div style="display: none;" class="mobile-connected-list" id="all-m">
		<p class="connected-list-title uppercase">All Members</p>
		<ul>
			<div class="loader">
			 	<center><img src="/Contents/imgs/loader.gif" /></center>
			 </div>
		</ul>
	</div>
	
	
	<div style="display: none;" class="mobile-profile-view" id="connect-details">
		<div class="top-panel">
            <img src="http://placehold.it/70x70" style="height: 70px; width: 70px;" height="70" width="70" class="img-circle pull-left" />
            <div class="connection-details-wrap">
                <p class="contributor-name connect-name purple">Lowell Ackerman</p>
                <p class="contributor-credentials connect-creds">Veterinarian DVM</p>
                <button class="btn-orange short uppercase connect-btn">Connect</button>
                <p style="display:none;" class="connected-email purple">first_lastname@emailaddress.com</p>
            </div>
		</div>

        <div class="connect-tabs-wrap">
            <div class="module-tabs-mobile">
                <div style="width: 50% !important;" class="module-tab-mobile-conn module-tab-mobile m-tab-active m-info-tab">
                    <p class="uppercase purple connect-tab-label">Info</p>
                </div>
                <div style="width: 48% !important;" class="module-tab-mobile-conn module-tab-mobile m-connections-tab">
                    <p class="uppercase purple connect-tab-label">Connections </p>
                </div>
            </div>
        </div>
		<div class="bottom-panel">
			<div class="bottom-panel-info">
                <div class="mobile-data-container">
                    <p class="data-container-title purple uppercase">Basic Information</p>
                    <ul class="connection-info-list-mobile">
                        <li>
                            <div class="form-group connect-form-group">
                                <label>ZIP Code</label>
                                <span id="connection-zip"></span>
                            </div>
                        </li>
                         <li>
                            <div class="form-group connect-form-group">
                                <label>Profession</label>
                                <span id="connection-profession"></span>
                            </div>
                        </li>
                         <li>
                            <div class="form-group connect-form-group">
                                <label>School</label>
                                <span id="connection-school"></span>
                            </div>
                        </li>
                         <li>
                            <div class="form-group connect-form-group">
                                <label>Graduation</label>
                                <span id="connection-graduation"></span>
                            </div>
                        </li>
                        <li>
                            <div class="form-group connect-form-group">
                                <label>Interest/Practice</label>
                                <span id="connection-interests"></span>
                            </div>
                        </li>
                         <li>
                            <div class="form-group connect-form-group">
                                <label>Privacy</label>
                                <span id="connection-privacy"></span>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="mobile-data-container">
                    <p class="data-container-title purple uppercase">Additional Information</p>
                    <ul class="connection-info-list-mobile">
                        <li>
                            <div class="form-group connect-form-group" id="connection-affiliations">
                                <label>Affiliations</label>
                                <span>American Academy of Veterinary Pharmacology and Therapeutics</span>
                                <span>American Animal Hospital Association (AAHA)</span>
                            </div>
                        </li>
                        <li>
                            <div class="form-group connect-form-group" id="connection-jobs">
                                <label>Jobs</label>
                                <span>American Academy of Veterinary Pharmacology and Therapeutics</span>
                            </div>
                        </li>
                         
                         <li>
                            <div class="form-group connect-form-group" id="connection-expertise">
                                <label>Expertise/Specialty</label>
                                <span>none</span>
                            </div>
                        </li>
                    </ul>
                </div>
			</div>
			<div class="bottom-panel-connections " style="display:none;">
                <p class="data-container-title purple uppercase">Mutual Connections</p>
                <ul id="connection-connections">
			         <li class="module-mobile">
			 	        <img class="img-circle pull-left" src="http://placehold.it/50x50" />
	                    <div class="connected-details">
		                    <p class="purple mobile-connect-name">Firstname Lastname</p>
		                    <p class="mobile-connect-occupation">Veterinarian DVM<img class="pull-right" src="/Contents/imgs/mobile/arrow-r.png" /></p>
		                    <p class="mobile-connect-school">University of California - Davis 2008</p>
		                    <p class="mobile-connect-city">New York, NY</p>
	                    </div>
	                </li>
                    <li class="module-mobile">
			 	        <img class="img-circle pull-left" src="http://placehold.it/50x50" />
	                    <div class="connected-details">
		                    <p class="purple mobile-connect-name">Firstname Lastname</p>
		                    <p class="mobile-connect-occupation">Veterinarian DVM<img class="pull-right" src="/Contents/imgs/mobile/arrow-r.png" /></p>
		                    <p class="mobile-connect-school">University of California - Davis 2008</p>
		                    <p class="mobile-connect-city">New York, NY</p>
	                    </div>
	                </li>
                    <li class="module-mobile">
			 	        <img class="img-circle pull-left" src="http://placehold.it/50x50" />
	                    <div class="connected-details">
		                    <p class="purple mobile-connect-name">Firstname Lastname</p>
		                    <p class="mobile-connect-occupation">Veterinarian DVM<img class="pull-right" src="/Contents/imgs/mobile/arrow-r.png" /></p>
		                    <p class="mobile-connect-school">University of California - Davis 2008</p>
		                    <p class="mobile-connect-city">New York, NY</p>
	                    </div>
	                </li>
                    <li class="module-mobile">
			 	        <img class="img-circle pull-left" src="http://placehold.it/50x50" />
	                    <div class="connected-details">
		                    <p class="purple mobile-connect-name">Firstname Lastname</p>
		                    <p class="mobile-connect-occupation">Veterinarian DVM<img class="pull-right" src="/Contents/imgs/mobile/arrow-r.png" /></p>
		                    <p class="mobile-connect-school">University of California - Davis 2008</p>
		                    <p class="mobile-connect-city">New York, NY</p>
	                    </div>
	                </li>
                    <li class="module-mobile">
			 	        <img class="img-circle pull-left" src="http://placehold.it/50x50" />
	                    <div class="connected-details">
		                    <p class="purple mobile-connect-name">Firstname Lastname</p>
		                    <p class="mobile-connect-occupation">Veterinarian DVM<img class="pull-right" src="/Contents/imgs/mobile/arrow-r.png" /></p>
		                    <p class="mobile-connect-school">University of California - Davis 2008</p>
		                    <p class="mobile-connect-city">New York, NY</p>
	                    </div>
	                </li>
                    <li class="module-mobile">
			 	        <img class="img-circle pull-left" src="http://placehold.it/50x50" />
	                    <div class="connected-details">
		                    <p class="purple mobile-connect-name">Firstname Lastname</p>
		                    <p class="mobile-connect-occupation">Veterinarian DVM<img class="pull-right" src="/Contents/imgs/mobile/arrow-r.png" /></p>
		                    <p class="mobile-connect-school">University of California - Davis 2008</p>
		                    <p class="mobile-connect-city">New York, NY</p>
	                    </div>
	                </li>
                </ul>
			</div>
		</div>
	</div>
</div>
@RenderPage("templates/_popover-affiliations.cshtml")
@RenderPage("templates/_popover-interest.cshtml")
@RenderPage("templates/_popover-specialty.cshtml")