﻿/**
 * 
 */
var TracktSimple = TracktSimple ||
    {
        track: function (eventType, qualifier1, qualifier2) {
            //console.log('tracktsimple.track eventType=' + eventType + " qual1=" + qualifier1 + " qual2=" + qualifier2);
            ga('send', {
                'hitType': 'event',         
                'eventCategory': eventType, 
                'eventAction': qualifier1,     
                'eventLabel': qualifier2,
                'hitCallback': function () {
                    //console.log("=> ga callback func for event :" + qualifier1 + " " + qualifier2);
                }
                });
        }
    };

// function gTrackEvent(eventType, param1, param2, param3, dest, target) {
//     //console.log('gTrackEvent : trpe=' + eventType + " " + param1 + " " + param2 + " dest=" + dest + " target=" + target );
//     ga('send', {
//         'hitType': 'event',
//         'eventCategory': eventType,
//         'eventAction': param1,
//         'eventLabel': param2,
//         'hitCallback': function () {
//             //console.log("gTrackEvent GA callback func for event :" + qualifier1 + " " + qualifier2);
//         }
//     });

//     if (target == '_self') {
//         window.location = dest;
//     }
// }

// function trackAdvanceClick(e){
//     $(e).on("click", function (d) {
//         d.preventDefault();
//         var name = $(this).attr('trkname');
//         var arr = name.split('|');
//         //console.log('it was clicked');
//         //gTrackEvent('_trackEvent', arr[0], arr[1], arr[2], null, null);
//         //alert('.track.click() zz ' + arr[0] + " " + arr[1]);
//         ga('send', {
//             'hitType': 'event',
//             'eventCategory': 'button',
//             'eventAction': arr[0],
//             'eventLabel': arr[1] + " " + arr[2],
//             'hitCallback': function () {
//             console.log("=> GA callback func for event :" + arr[0] + " " + arr[1] + " " + arr[2]);
//                 //alert('callback from GA');
//             }
//         });
//     });
// }


$(document).ready(function () {

    /******************Google Event Tracking**************************/
    
    var trackingInit = function () {
        //alert('trackinfInit()');
        $(".track").on("click", function (e) {
            //console.log('.track');
            //e.preventDefault();
            var name = $(this).attr('trkname');
            var arr = name.split('|');
            var dest = $(this).attr('href');
            //console.log('arr=' + arr +  ' dest=' + dest);
            //gTrackEvent('_trackEvent', arr[0], arr[1], arr[2] + '-' + new Date().getHours() + ':' + new Date().getMinutes(), dest);
            ga('send', {
                'hitType': 'event',
                'eventCategory': eventType,
                'eventAction': arr[0],
                'eventLabel': arr[1] + " " + arr[2],
                'hitCallback': function () {
                    //console.log("=> GA callback func for event :" + arr[0] + " " + arr[1] + " " + arr[2]);
                }
            });

        });

        // $(".track-click").on("click", function (e) {
        //     //console.log('.track-click');
        //     var name = $(this).attr('trkname');
        //     var arr = name.split('|');
        //     //gTrackEvent('_trackEvent', arr[0], arr[1], arr[2], null, null);
        //     //alert('.track.click() zz ' + arr[0] + " " + arr[1]);
        //     ga('send', {
        //         'hitType': 'event',
        //         'eventCategory': 'button',
        //         'eventAction': arr[0],
        //         'eventLabel': arr[1] + " " + arr[2],
        //         'hitCallback': function () {
        //             //console.log("=> GA callback func for event :" + arr[0] + " " + arr[1] + " " + arr[2]);
        //             //alert('callback from GA');
        //         }
        //     });
        // });

    };

    trackingInit();

});

