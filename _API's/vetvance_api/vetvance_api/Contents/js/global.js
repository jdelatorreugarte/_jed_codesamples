// add module to namespace
VETAPP.namespace('VETAPP.Modules.Global');

// define global
VETAPP.Modules.Global = (function() {

    var self = this;

	self.init = function init() { ///console.log('init module: global.js');

		var self = this;
		self.isMobile 	= isMobile();
		self.container	= $('.main-container');
		self.pageId		= $('#pageId');
		self.siteNav 	= $('.sitenav');
		self.mobileNavWrap  = $('.mobile-header');
		self.mobileContainer = $('.mobile-container');
		// init Modules
		self.initModules();
		self.bindEvents();


		function isMobile(){
			//check browser size, anything below 768 we consider 'mobile'
			if($(window).width() <= 768){
				return true;
			}else{
				return false;
			}
		}
	}

	self.initModules = function() {

		var self = this;
		
		// load global site nav
		var siteNav = new VETAPP.Classes.Sitenav({ 
			page : self.pageId, 
			container : self.container 
		}, self.siteNav);

		mobileNav = new VETAPP.Classes.Mobilenav({
			page : self.mobileContainer
		}, self.mobileNavWrap);

		// add global connect
		var btnConnect = new VETAPP.Classes.Connect({});

	}
	
	self.bindEvents = function(){
		
		$('.close-modal').click(function(){
			$('.modal').modal('hide');
			return false;
		});

      $('.sitenav li').mouseenter(function () {
          $(this).find('.tooltip_top').show();
      });

      $('.tooltip_top').mouseleave(function () {
          $('.tooltip_top').hide();
      });

      $('.register-link').click(function () {
          location.href = '/register';
      });

      $('.signin-link').click(function () {
          location.href = '/signin';
      });
	}
	
	return self;


	//Adding combobox from jquery ui
	 (function( $ ) {
	$.widget( "custom.combobox", {
	_create: function() {
	this.wrapper = $( "<span>" )
	.addClass( "custom-combobox" )
	.insertAfter( this.element );
	this.element.hide();
	this._createAutocomplete();
	this._createShowAllButton();
	},
	_createAutocomplete: function() {
	var selected = this.element.children( ":selected" ),
	value = selected.val() ? selected.text() : "";
	this.input = $( "<input>" )
	.appendTo( this.wrapper )
	.val( value )
	.attr( "title", "" )
	.addClass( "custom-combobox-input ui-widget ui-widget-content ui-state-default ui-corner-left" )
	.autocomplete({
	delay: 0,
	minLength: 0,
	source: $.proxy( this, "_source" )
	})
	.tooltip({
	tooltipClass: "ui-state-highlight"
	});
	this._on( this.input, {
	autocompleteselect: function( event, ui ) {
	ui.item.option.selected = true;
	this._trigger( "select", event, {
	item: ui.item.option
	});
	},
	autocompletechange: "_removeIfInvalid"
	});
	},
	_createShowAllButton: function() {
	var input = this.input,
	wasOpen = false;
	$( "<a>" )
	.attr( "tabIndex", -1 )
	.attr( "title", "Show All Items" )
	.tooltip()
	.appendTo( this.wrapper )
	.button({
	icons: {
	primary: "ui-icon-triangle-1-s"
	},
	text: false
	})
	.removeClass( "ui-corner-all" )
	.addClass( "custom-combobox-toggle ui-corner-right" )
	.mousedown(function() {
	wasOpen = input.autocomplete( "widget" ).is( ":visible" );
	})
	.click(function() {
	input.focus();
	// Close if already visible
	if ( wasOpen ) {
	return;
	}
	// Pass empty string as value to search for, displaying all results
	input.autocomplete( "search", "" );
	});
	},
	_source: function( request, response ) {
	var matcher = new RegExp( $.ui.autocomplete.escapeRegex(request.term), "i" );
	response( this.element.children( "option" ).map(function() {
	var text = $( this ).text();
	if ( this.value && ( !request.term || matcher.test(text) ) )
	return {
	label: text,
	value: text,
	option: this
	};
	}) );
	},
	_removeIfInvalid: function( event, ui ) {
	// Selected an item, nothing to do
	if ( ui.item ) {
	return;
	}
	// Search for a match (case-insensitive)
	var value = this.input.val(),
	valueLowerCase = value.toLowerCase(),
	valid = false;
	this.element.children( "option" ).each(function() {
	if ( $( this ).text().toLowerCase() === valueLowerCase ) {
	this.selected = valid = true;
	return false;
	}
	});
	// Found a match, nothing to do
	if ( valid ) {
	return;
	}
	// Remove invalid value
	this.input
	.val( "" )
	.attr( "title", value + " didn't match any item" )
	.tooltip( "open" );
	this.element.val( "" );
	this._delay(function() {
	this.input.tooltip( "close" ).attr( "title", "" );
	}, 2500 );
	this.input.data( "ui-autocomplete" ).term = "";
	},
	_destroy: function() {
	this.wrapper.remove();
	this.element.show();
	}
	});
	})( jQuery );
}());

$(document).ready(function(){
	VETAPP.Modules.Global.init();
});