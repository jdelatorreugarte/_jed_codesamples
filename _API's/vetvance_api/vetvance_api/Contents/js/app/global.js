var VET = VET || {};
VET.Global = VET.Global || {};

VET.Global = (function() {

	var self = this;

	self.init = function init()
	{ //console.log('init: global.js');

		var self = this;
		self.isMobile 	= window.orientation || Modernizr.touch;
		self.container	= $('.main-container');
		self.pageId		= $('#pageId');
		self.siteNav 	= $('.sitenav');

		// Bind events
		self.bindEvents(); 
	}

	self.bindEvents = function()
	{
		var self = this;
		
		// load site nav
		var siteNav = new VET.Sitenav({ 
			page : self.pageId, 
			container : self.container 
		}, self.siteNav);
	}
		
	return self;
	
}());

$(document).ready(function(){
	VET.Global.init();
});
