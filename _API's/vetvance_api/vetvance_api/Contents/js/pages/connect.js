/*	author: jonathan schmidt - http://lifeisinteractive.com
*/
// add page to namespace

VETAPP.namespace('VETAPP.pages.connect');
// create page
VETAPP.pages.connect = (function() {

	var self = {};

	self.init = function init() { ///console.log('init page: connect.js');

		var self = this;		
		self.isMobile 		    = window.orientation || Modernizr.touch;
		self.container 	        = $('.main-container');
		self.pageId			    = $('#pageId');
		self.mobileContainer    = $('#connect-container-mobile');
		self.mobileNavWrap      = $('.mobile-header');
		self.mobileConnectLists = [];
		// search switch
		self.isSearching = false;
		// ajax search
		self.searchajax = {};

		// bind page events
		if (self.pageId.hasClass('connect-container')){
			// search search properties 
			self.interests					= [];
			self.searchStr 				= '?';
			self.searchSort 				= 'default';
			self.newmember					= false;
			self.student					= false;
			self.veterinarian				= false;			
			self.technician				= false;
			self.school 					= '';
			self.graduation				= 0;
			self.organizationid			= 0;
			self.organizationStr			= '';
			self.acquatic					= false;
			self.mixedcattle				= false;
			self.mixedswine				= false;
			self.mixedequine 				= false;
			self.mixedcompanion 			= false;
			self.smallcanine 				= false;
			self.smallfeline 				= false;
			self.smallexotics 			= false;
			self.foodbeef 					= false;
			self.fooddairy 				= false;
			self.foodcow 					= false;
			self.foodpoultry 				= false;
			self.foodswine 				= false;
			self.equine 					= false;
			self.smallruminant 			= false;
			self.labanimal 				= false;
			self.government 				= false;
			self.armedforces 				= false;
			self.publichealth 			= false;
			self.industry 					= false;
			self.zoo 						= false;
			self.other 						= false;
			self.anesthesiology 			= false;
			self.animalwelfare 			= false;
			self.behavior 					= false;
			self.clinical 					= false;
			self.dermatology 				= false;
			self.emergency 				= false;
			self.internalmedicine 		= false;
			self.laboratorymedicine 	= false;
			self.microbiology 			= false;
			self.nutrition 				= false;
			self.ophthalmology 			= false;
			self.pathology 				= false;
			self.preventivemedicine 	= false;
			self.radiology					= false;
			self.sportsmedicine 			= false;
			self.surgery 					= false;
			self.theriogenology 			= false;
			self.toxicology 				= false;
			self.veterinary 				= false;
			self.zoologicalmedicine 	= false;

			// all checked
			self.allchecked 				= false;
			self.nonechecked				= false;

			// init Modules
			self.initModules();
			// build default search
			self.buildSearchStr();

			// bind events
			self.bindEvents();

			self.mobileInit();
		}			
	}

	self.initModules = function()
	{
		var self = this;

		// add "Interest / Practice" popover
		var btnInterest = new VETAPP.Classes.Popover({
			type : 'interest', 
			btnId: 'btnInterest',
			direction: 'right',
			output: '#filtered-tags-interest'
		}, $('.connect-container'));

		// add "Expertise / Specialty" popover
		var btnSpecialty = new VETAPP.Classes.Popover({
			type : 'specialty', 
			btnId: 'btnSpecialty',
			direction: 'right',
			output: '#filtered-tags-specialty'
		}, $('.connect-container'));
		
		// add "Affiliations" popover
		var btnSpecialty = new VETAPP.Classes.Popover({
			type : 'affiliations', 
			btnId: 'btnLookUp',
			direction: 'right',
			output: '#filtered-tags-specialty'
		}, $('.connect-container'));

		mobileNav = new VETAPP.Classes.Mobilenav({
			page : '#connect-container-mobile'
		}, self.mobileNavWrap);	
	}

	self.bindSchoolAutoComplete = function(item)
	{		
		var self = this;
		// get schools	
		$.ajax({
    		url: "/api/userschools/",
			format: JSON,
			success: function(data){
				var option = '';
				$.each(data, function(key, val){
					option +='<option value="'+ val.schoolname +'">'+ val.schoolname +'</option>';
				});
				$('#school').append(option);
				$('.combobox').combobox();
			}
		});
	}

	self.buildMobileSearchStr = function(){
		var self = this;
		// filter interests		
		(_.contains(self.interests, 'acquatic')) ? (self.acquatic = true) : (self.acquatic = false);
		(_.contains(self.interests, 'mixedcattle')) ? (self.mixedcattle = true) : (self.mixedcattle = false);
		(_.contains(self.interests, 'mixedswine')) ? (self.mixedswine = true) : (self.mixedswine = false);
		(_.contains(self.interests, 'mixedequine')) ? (self.mixedequine = true) : (self.mixedequine = false);
		(_.contains(self.interests, 'mixedcompanion')) ? (self.mixedcompanion = true) : (self.mixedcompanion = false);
		(_.contains(self.interests, 'smallcanine')) ? (self.smallcanine = true) : (self.smallcanine = false);
		(_.contains(self.interests, 'smallfeline')) ? (self.smallfeline = true) : (self.smallfeline = false);
		(_.contains(self.interests, 'smallexotics')) ? (self.smallexotics = true) : (self.smallexotics = false);
		(_.contains(self.interests, 'foodbeef')) ? (self.foodbeef = true) : (self.foodbeef = false);
		(_.contains(self.interests, 'fooddairy')) ? (self.fooddairy = true) : (self.fooddairy = false);
		(_.contains(self.interests, 'foodcow')) ? (self.foodcow = true) : (self.foodcow = false);
		(_.contains(self.interests, 'foodpoultry')) ? (self.foodpoultry = true) : (self.foodpoultry = false);
		(_.contains(self.interests, 'foodswine')) ? (self.foodswine = true) : (self.foodswine = false);
		(_.contains(self.interests, 'equine')) ? (self.equine = true) : (self.equine = false);
		(_.contains(self.interests, 'smallruminant')) ? (self.smallruminant = true) : (self.smallruminant = false);
		(_.contains(self.interests, 'labanimal')) ? (self.labanimal = true) : (self.labanimal = false);
		(_.contains(self.interests, 'government')) ? (self.government = true) : (self.government = false);
		(_.contains(self.interests, 'armedforces')) ? (self.armedforces = true) : (self.armedforces = false);
		(_.contains(self.interests, 'publichealth')) ? (self.publichealth = true) : (self.publichealth = false);
		(_.contains(self.interests, 'industry')) ? (self.industry = true) : (self.industry = false);
		(_.contains(self.interests, 'zoo')) ? (self.zoo = true) : (self.zoo = false);
		(_.contains(self.interests, 'other')) ? (self.other = true) : (self.other = false);
		(_.contains(self.interests, 'anesthesiology')) ? (self.anesthesiology = true) : (self.anesthesiology = false);
		(_.contains(self.interests, 'animalwelfare')) ? (self.animalwelfare = true) : (self.animalwelfare = false);
		(_.contains(self.interests, 'behavior')) ? (self.behavior = true) : (self.behavior = false);
		(_.contains(self.interests, 'clinicalpharmacology')) ? (self.clinicalpharmacology = true) : (self.clinicalpharmacology = false);
		(_.contains(self.interests, 'dermatology')) ? (self.dermatology = true) : (self.dermatology = false);
		(_.contains(self.interests, 'emergencyandcriticalcare')) ? (self.emergencyandcriticalcare = true) : (self.emergencyandcriticalcare = false);
		(_.contains(self.interests, 'internalmedicine')) ? (self.internalmedicine = true) : (self.internalmedicine = false);
		(_.contains(self.interests, 'laboratorymedicine')) ? (self.laboratorymedicine = true) : (self.laboratorymedicine = false);
		(_.contains(self.interests, 'microbiology')) ? (self.microbiology = true) : (self.microbiology = false);
		(_.contains(self.interests, 'nutrition')) ? (self.nutrition = true) : (self.nutrition = false);
		(_.contains(self.interests, 'ophthalmology')) ? (self.ophthalmology = true) : (self.ophthalmology = false);			
		(_.contains(self.interests, 'pathology')) ? (self.pathology = true) : (self.pathology = false);
		(_.contains(self.interests, 'preventivemedicine')) ? (self.preventivemedicine = true) : (self.preventivemedicine = false);
		(_.contains(self.interests, 'radiology')) ? (self.radiology = true) : (self.radiology = false);
		(_.contains(self.interests, 'sportsmedicineandrehabilitation')) ? (self.sportsmedicineandrehabilitation = true) : (self.sportsmedicineandrehabilitation = false);
		(_.contains(self.interests, 'surgery')) ? (self.surgery = true) : (self.surgery = false);
		(_.contains(self.interests, 'theriogenology')) ? (self.theriogenology = true) : (self.theriogenology = false);
		(_.contains(self.interests, 'toxicology')) ? (self.toxicology = true) : (self.toxicology = false);
		(_.contains(self.interests, 'veterinarypractitioners')) ? (self.veterinarypractitioners = true) : (self.veterinarypractitioners = false);
		(_.contains(self.interests, 'zoologicalmedicine')) ? (self.zoologicalmedicine = true) : (self.zoologicalmedicine = false);

	    //hack to make userid 0 if not set
		userid = userid || 0;

	    // set search properties
		self.searchStr = userid +'?sort='+ self.searchSort
			+'&newmember='+ self.newmember
			+'&student='+ self.student
			+'&veterinarian='+ self.veterinarian
			+'&technician='+ self.technician
			+'&school='+ self.school
			+'&graduation='+ self.graduation
			+'&organizationid='+ self.organizationid
			+'&acquatic='+ self.acquatic
			+'&mixedcattle='+ self.mixedcattle
			+'&mixedswine='+ self.mixedswine
			+'&mixedequine='+ self.mixedequine
			+'&mixedcompanion='+ self.mixedcompanion
			+'&smallcanine='+ self.smallcanine
			+'&smallfeline='+ self.smallfeline
			+'&smallexotics='+ self.smallexotics
			+'&foodbeef='+ self.foodbeef
			+'&fooddairy='+ self.fooddairy
			+'&foodcow='+ self.foodcow
			+'&foodpoultry='+ self.foodpoultry
			+'&foodswine='+ self.foodswine
			+'&equine='+ self.equine
			+'&smallruminant='+ self.smallruminant
			+'&labanimal='+ self.labanimal
			+'&government='+ self.government
			+'&armedforces='+ self.armedforces
			+'&publichealth='+ self.publichealth
			+'&industry='+ self.industry
			+'&zoo='+ self.zoo
			+'&other='+ self.other
			+'&anesthesiology='+ self.anesthesiology
			+'&animalwelfare='+ self.animalwelfare
			+'&behavior='+ self.behavior
			+'&clinicalpharmacology='+ self.clinical
			+'&dermatology='+ self.dermatology
			+'&emergencyandcriticalcare='+ self.emergency
			+'&internalmedicine='+ self.internalmedicine
			+'&laboratorymedicine='+ self.laboratorymedicine
			+'&microbiology='+ self.microbiology
			+'&nutrition='+ self.nutrition
			+'&ophthalmology='+ self.ophthalmology
			+'&pathology='+ self.pathology
			+'&preventivemedicine='+ self.preventivemedicine
			+'&radiology='+ self.radiology
			+'&sportsmedicineandrehabilitation='+ self.sportsmedicine
			+'&surgery='+ self.surgery
			+'&theriogenology='+ self.theriogenology
			+'&toxicology='+ self.toxicology
			+'&veterinarypractitioners='+ self.veterinary
			+'&zoologicalmedicine='+ self.zoologicalmedicine;
	}

	self.mobileConnectQuery = function(){
		var self = this;

		self.buildMobileSearchStr();
		
		var url = '/api/userslist/';
		$('.mobile-no-results').hide();
		$('.loaderimg').show();

		$.ajax({
			url: url + self.searchStr,
			format: JSON
		}).done(function(data){
			if (data.length !== 0) {
			    ///console.log('count: ' + data.length);
			    /////console.log(data);
				// parse data
				
			} else {
				// no data
				$('.loaderimg').hide();
				///console.log('count: '+ data.length);
				
				// no results
				$('.mobile-no-results').show();
			}			
		})
	}

	self.returnEntityName = function(name){
		var self = this;
		var interestKey = {
			"acquatic": "Acquatic",
			"mixedcattle": "Mostly Cattle",
			"mixedswine": "Mostly Swine",
			"mixedequine": "Mostly Equine",
			"mixedcompanion": "Mostly Companion",
			"smallcanine": "Small Canine",
			"smallfeline": "Feline",
			"smallexotics": "Exotics",
			"foodbeef": "Beef Cattle",
			"fooddairy": "Dairy Cattle",
			"foodcow": "Cow Calf",
			"foodpoultry": "Poultry",
			"foodswine": "Swine",
			"equine": "Equine",
			"smallruminant": "Small Ruminant",
			"labanimal": "Lab Animal",
			"government": "Government",
			"armedforces": "Armed Forces",
			"publichealth": "Public Health",
			"industry": "Industry",
			"zoo": "Zoo/Exotic",
			"other": "Other",
			"anesthesiology": "Anesthesiology",
			"animalwelfare": "Animal Welfare",
			"behavior": "Behavior",
			"clinicalpharmacology": "Clinical Pharmacology",
			"dermatology": "Dermatology",
			"emergencyandcriticalcare": "Emergency and Critical Care",
			"internalmedicine": "Internal Medicine",
			"laboratorymedicine": "Laboratory Medicine",
			"microbiology": "Microbiology",
			"nutrition": "Nutrition",
			"ophthalmology": "Ophthalmology",
			"pathology": "Pathology",
			"preventivemedicine": "Preventive Medicine",
			"radiology": "Radiology",
			"sportsmedicineandrehabilitation": "Sports Medicine and Rehabilitation",
			"surgery": "Surgery",
			"theriogenology": "Theriogenology",
			"toxicology": "Toxicology",
			"veterinarypractitioners": "Veterinary Practitioners",
			"zoologicalmedicine": "Zoological Medicine"
		}

		if(_.has(interestKey, name)){
			return interestKey[name];
		}
	}


	self.injectHtml = function(data, insertTarget){
		_html = '';
		_.each(data, function(value,key,list){
			_html += '<li data-parentpageid="' + insertTarget  + '" data-connectionid="' + value.user.userid + '" data-target="connect-details" class="module-mobile connect-person slide-link">';
			_html += '<img height="50" width="50" style="width: 50px; height: 50px;" class="img-circle pull-left" src="/Contents/avatars/' + value.user.avatar + '" />'; 
			_html += '<div class="connected-details">'; 	
		    _html += '<p class="purple mobile-connect-name">' +  value.user.firstname + ' ' + value.user.lastname + '</p>';
			_html += '<p class="mobile-connect-school">' + value.user.school + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></p>';
			if(insertTarget == '#new-connections-m')
				_html += '<p class="mobile-connect-city">' + value.user.city + ', ' + value.user.state + '</p>';
			else
				_html += '<p class="mobile-connect-city">' + value.user.city[0] + ', ' + value.user.state[0] + '</p>';
			_html += '</div></li>';   
		});

		//gotta insert it into the ul
		var target = insertTarget + ' ul';

		//inject html into proper target
		$(target).html(_html);

		//bind handlers
		$('.connect-person').click(function(){
			var elem = this;
			//get data target
			targetId = '#' + $(this).attr('data-target');
			//make api calls to 1) user and 2) connections page
			var slideParentId = $(this).attr('data-parentpageid');

			//connection_id
			var connectionId = $(this).attr('data-connectionid');
			//make API call
			//api/ConnectionInfo/1070?connectid=1059
			var userId = userid || 0;
			var url = '/api/ConnectionInfo/' +  userId + '?connectid=' + connectionId;
			$.ajax({
				url: url,
				format: JSON
			}).done(function(data){
				///console.log(data);
				//insert html
				$('.contributor-name').text(data[0].connection.firstname + ' ' + data[0].connection.lastname);
				$('.contributor-credentials').text(data[0].connection.title);
				$('#connect-details > .top-panel > img').attr('src', '/Contents/avatars/' + data[0].connection.avatar);
				//if connected show email, if not, show connect button
				if(data[0].connected === false){
					//do nothing $('.contributor-credentials').text(data[0].connection.title)
					//add connection id to button
					$('.connect-btn').attr('data-connectionid', data[0].connection.userid).show();
					$('.connected-email').text(data[0].connection.email).hide();
				}else{
					$('.connect-btn').hide();
					$('.connected-email').text(data[0].connection.email).show();
				}

				//TODO: if there are no connections disable the tabbtn
				
				//insert additional data 
				$('#connection-zip').text(data[0].connection.zip);
				$('#connection-profession').text(data[0].connection.profession);
				$('#connection-school').text(data[0].connection.school);
				$('#connection-graduation').text(data[0].connection.graduation);

				//interests - is an array so we turn to csv
				var interests = toCsv(data[0].connection.userinterests);
				$('#connection-interests').html(interests) || 'None';

				//expertise - is array
				//var expertise = toCsv(data[0].connection.userspecialties);
				_expertiseConnectHtml = '';
				_expertiseCounter = 0;
				_.each(data[0].connection.userspecialties[0], function(value, key, list){
					if(key != 'userspecialtyid' && key != 'userid'){
						if(_expertiseCounter == 0){
							if(value === true){
								_expertiseConnectHtml += '<li><label>Expertise/Specialty</label><span>' + self.returnEntityName(key) + '</span></li>';	
								_expertiseCounter++;
							}
						}else{
							if(value === true){
								_expertiseConnectHtml += '<li><label style="color:white !important;">Expertise/Specialty</label><span>' + self.returnEntityName(key) + '</span></li>';	
								_expertiseCounter++;
							}
						}
								
					}
					
				});
				
				$('#connection-expertise').html(_expertiseConnectHtml);

				//jobs is array
				//var jobs = toCsv(data[0].connection.userjobs) || '0';
				_jobsConnectHtml = '';
				_jobsCounter = 0;
				_.each(data[0].connection.userjobs, function(value, key, list){
					if(_jobsCounter == 0){
						_jobsConnectHtml += '<li><label>Jobs</label><span>' + value.title + ' at ' + value.company + ' ' + value.year + '</span></li>';
					}else{
						_jobsConnectHtml += '<li><label style="color:white !important;">Jobs</label><span>' + value.title + ' at ' + value.company + ' ' + value.year + '</span></li>';
					}
					_jobsCounter++;
				});
				$('#connection-jobs').html(_jobsConnectHtml);

				//privacy
				if(data[0].connection.optemail === true)
					$('#connection-privacy').text('Show email to others');
				else
					$('#connection-privacy').text('Dont show email to others');

				var affArray = [];
				//affiliations - is array
				if(data[0].connection.affiliations.length > 0){
					_affiliationConnectHtml = '';
					_affiliationCounter = 0;
					_.each(data[0].connection.affiliations, function(value, key, list){
						//affArray.push(value.organization.organizationname);
						if(_affiliationCounter == 0){
							_affiliationConnectHtml += '<li><label>Affiliations</label><span>' + value.organization.organizationname + '</span></li>';
						}else{
							_affiliationConnectHtml += '<li><label style="color:white !important;">Affiliations</label><span>' + value.organization.organizationname + '</span></li>';
						}
						_affiliationCounter++;
					});

					//var affiliations = toCsv(affArray);
					$('#connection-affiliations').html(_affiliationConnectHtml);
				}else{
					$('#connection-affiliations').html('<label>Affiliations</label><span>None</span>');
				}

				//need to make another api call to get this guy's connections to make the list
				//http://vetvance-api.dev.thebloc.com/api/UserConnections/1059?sort=default
				//var connectionsUrl = 'http://vetvance-api.dev.thebloc.com/api/UserConnections/' + data[0].connection.userid + '?sort=default';
			    var connectionsUrl = '/api/UserConnections/' + data[0].connection.userid + '?sort=default';

				$.ajax({
					url: connectionsUrl,
					format: JSON
				}).done(function(data){
					//loop through connections array, stuff into #connection-connections
					var connectionConnectionsHtml = '';
					_.each(data, function(value, key, list){
						connectionConnectionsHtml += '<li class="module-mobile">';
						connectionConnectionsHtml += '<img style="height:50px; width:50px;" height="50" width="50" class="img-circle pull-left" src="/Contents/avatars/' + value.user.avatar + '" />';
						connectionConnectionsHtml += '<div class="connected-details">';
			 	        connectionConnectionsHtml += '<p class="purple mobile-connect-name">' +  value.user.firstname + ' ' + value.user.lastname + '</p>';
	                    connectionConnectionsHtml += '<p class="mobile-connect-occupation">' + value.user.profession + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></p>'
		                connectionConnectionsHtml += '<p class="mobile-connect-school">' + value.user.school + '</p>';  
		                connectionConnectionsHtml += '<p class="mobile-connect-city">' + value.user.city + ', ' + value.user.state + '</p>';
		                connectionConnectionsHtml += '</div></li>';    
					});

					//insert into page
					$('#connection-connections').html(connectionConnectionsHtml);

					//set page change handler
					//do we allow user to delve into multiple layers of connections?
				});

				//now change the page
				mobileNav.changePage(targetId, slideParentId, 'slide-forward', elem);
			});
		});		         
	}

	self.generateConnectList = function(filterType, elem){
		var self = this;

		//slide new page in
		targetId = '#' + $(elem).attr('data-target');
		var slideParentId = '#' + $(elem).attr('data-parentpageid');
		mobileNav.changePage(targetId, slideParentId, 'slide-forward', elem);

		//prepare api call string
		self.buildMobileSearchStr();
		if(!_.contains(self.mobileConnectLists, filterType)){
			//make api call
			var url = '/api/userslist/';
			$.ajax({
				url: url + self.searchStr,
				format: JSON
			}).done(function(data){
				if (data.length !== 0) {
				    ///console.log('count: ' + data.length);
				    ///console.log(data);

				    //if only connecteds
				    if(filterType == 'all-connections'){
				    	data = _.filter(data, function(value, key, list){
				    		return value.connected == true;
				    	});
				    	// parse data
						var insertTarget = '#' + filterType + '-m';
						self.injectHtml(data, insertTarget);
						
						//add generated filter type to self.mobileConnectLists
						self.mobileConnectLists.push(filterType);

						//slide new page in
						targetId = '#' + $(elem).attr('data-target');
						var slideParentId = '#' + $(elem).attr('data-parentpageid');
						mobileNav.changePage(targetId, slideParentId, 'slide-forward', elem);
				    }else if(filterType == 'new-connections'){
				    	// data = _.filter(data, function(value, key, list){
				    	// 	return value.connected == true;
				    	// });
				    	
				    	//use proper api
				    	var newConnUrl = '/api/NewConnections/' + userid;
				    	$.ajax({
				    		url: newConnUrl,
				    		dataType: JSON
				    	}).done(function(data){
				    		// parse data
							var insertTarget = '#' + filterType + '-m';
							self.injectHtml(data, insertTarget);
							
							//add generated filter type to self.mobileConnectLists
							self.mobileConnectLists.push(filterType);

							
				    	});
				    }else{
				    	// parse data
						var insertTarget = '#' + filterType + '-m';
						self.injectHtml(data, insertTarget);
						
						//add generated filter type to self.mobileConnectLists
						self.mobileConnectLists.push(filterType);

						//slide new page in
						// targetId = '#' + $(elem).attr('data-target');
						// var slideParentId = '#' + $(elem).attr('data-parentpageid');
						// mobileNav.changePage(targetId, slideParentId, 'slide-forward', elem);
				    }
					
					
				} else {
					// no data
					$('.loaderimg').hide();
					///console.log('count: '+ data.length);
					
					// no results
					var insertTarget = '#' + filterType + '-m';
					$(insertTarget).html('<center>No results available.</center>');
				}			
			})
		}else{
			//slide new page in - no need to make api call, we already  have it
			// targetId = '#' + $(elem).attr('data-target');
			//make api calls to 1) user and 2) connections page
			// var slideParentId = '#' + $(elem).attr('data-parentpageid');
			// mobileNav.changePage(targetId, slideParentId, 'slide-forward', elem);	
		}
	}

	self.mobileInit = function(){
		var self = this;
		var isLoggedIn = userid || 0; //test value
		//TODO: set isloggedin and put user object into global scope
		if(isLoggedIn > 0){
			//bind events - page 1
			$('.connect-link').click(function(){
				var filter = $(this).attr('data-filtertype');
				var elem = this;
				//zero out selections
				self.school = '';
				self.graduation = 0;
				self.searchSort = 'default';
				self.newmember = 'false';
				switch(filter){

					case "all":
						self.generateConnectList('all', elem);
						break;

					case "all-connections":
						self.generateConnectList('all-connections', elem); 
						break;

					case "new-connections":
						self.newmember = 'true'; 
						self.generateConnectList('new-connections', elem);
						break;

					case "near-me":
						self.searchSort = 'zip';
						self.generateConnectList('near-me', elem); 
						break;

					case "school":
						$.ajax({
				    		url: "/api/Users/" + userid,
							format: JSON,
							success: function(data){
								self.school = data[0].school;
								self.generateConnectList('school', elem);
							}
						});
						
						break;

					case "graduation":
						$.ajax({
				    		url: "/api/Users/" + userid,
							format: JSON,
							success: function(data){
								self.graduation = data[0].graduation;
								self.generateConnectList('graduation', elem); 
							}
						});
						
						break;

					case "interest-area": 
						self.generateConnectList('interest-area', elem);
						break;
				}
			})

			

			$('.mobile-nav-back').click(function(){
				mobileNav.back();
			});
		}else{
			//fire popup
			$('.connect-link').click(function(){
				alert('You are not logged in');
			})
			///console.log('UH OH, you are not logged in');
		}
	}

	var trackConnFilt = function(q1,q2)
	{
	    ///console.log('trackConnFilt q1=' + q1 + ' q2=' + q2);
	    TracktSimple.track("ConnectFilter", q1, q2);
	}

	self.buildSearchStr = function()
	{
	    var self = this;
	    ///console.log('buildSearchStr');


	    // filter interests		
	    (_.contains(self.interests, 'aquatic')) ? (self.acquatic = true, trackConnFilt('Interest','aquatic')) : (self.acquatic = false);
	    (_.contains(self.interests, 'mixedcattle')) ? (self.mixedcattle = true, trackConnFilt('interest', 'mixedcattle')) : (self.mixedcattle = false);
	    (_.contains(self.interests, 'mixedswine')) ? (self.mixedswine = true, trackConnFilt('Interest', 'mixedswine')) : (self.mixedswine = false);
	    (_.contains(self.interests, 'mixedequine')) ? (self.mixedequine = true, trackConnFilt('Interest', 'mixedequine')) : (self.mixedequine = false);
	    (_.contains(self.interests, 'mixedcompanion')) ? (self.mixedcompanion = true, trackConnFilt('Interest', 'mixedcompanion')) : (self.mixedcompanion = false);
	    (_.contains(self.interests, 'smallcanine')) ? (self.smallcanine = true, trackConnFilt('Interest', 'smallcanine')) : (self.smallcanine = false);
	    (_.contains(self.interests, 'smallfeline')) ? (self.smallfeline = true, trackConnFilt('Interest', 'smallfeline')) : (self.smallfeline = false);
	    (_.contains(self.interests, 'smallexotics')) ? (self.smallexotics = true, trackConnFilt('Interest', 'smallexotics')) : (self.smallexotics = false);
	    (_.contains(self.interests, 'foodbeef')) ? (self.foodbeef = true, trackConnFilt('Interest', 'foodbeef')) : (self.foodbeef = false);
	    (_.contains(self.interests, 'fooddairy')) ? (self.fooddairy = true, trackConnFilt('Interest', 'fooddairy')) : (self.fooddairy = false);
	    (_.contains(self.interests, 'foodcow')) ? (self.foodcow = true, trackConnFilt('Interest', 'foodcow')) : (self.foodcow = false);
	    (_.contains(self.interests, 'foodpoultry')) ? (self.foodpoultry = true, trackConnFilt('Interest', 'foodpoultry')) : (self.foodpoultry = false);
	    (_.contains(self.interests, 'foodswine')) ? (self.foodswine = true, trackConnFilt('Interest', 'foodswine')) : (self.foodswine = false);
	    (_.contains(self.interests, 'equine')) ? (self.equine = true, trackConnFilt('Interest', 'equine')) : (self.equine = false);
	    (_.contains(self.interests, 'smallruminant')) ? (self.smallruminant = true, trackConnFilt('Interest', 'smallruminant')) : (self.smallruminant = false);
	    (_.contains(self.interests, 'labanimal')) ? (self.labanimal = true, trackConnFilt('Interest', 'labanimal')) : (self.labanimal = false);
		(_.contains(self.interests, 'government')) ? (self.government = true, trackConnFilt('Organization','government')) : (self.government = false);
		(_.contains(self.interests, 'armedforces')) ? (self.armedforces = true, trackConnFilt('Organization', 'armedforces')) : (self.armedforces = false);
		(_.contains(self.interests, 'publichealth')) ? (self.publichealth = true, trackConnFilt('Organization', 'publichealth')) : (self.publichealth = false);
		(_.contains(self.interests, 'industry')) ? (self.industry = true, trackConnFilt('Organization', 'industry')) : (self.industry = false);
		(_.contains(self.interests, 'zoo')) ? (self.zoo = true, trackConnFilt('Organization', 'zoo')) : (self.zoo = false);
		(_.contains(self.interests, 'other')) ? (self.other = true, trackConnFilt('Organization', 'other')) : (self.other = false);
		(_.contains(self.interests, 'anesthesiology')) ? (self.anesthesiology = true, trackConnFilt('Specialty', 'anesthesiology')) : (self.anesthesiology = false);
		(_.contains(self.interests, 'animalwelfare')) ? (self.animalwelfare = true,trackConnFilt('Specialty', 'animalwelfare')) : (self.animalwelfare = false);
		(_.contains(self.interests, 'behavior')) ? (self.behavior = true, trackConnFilt('Specialty', 'behavior')) : (self.behavior = false);
		(_.contains(self.interests, 'clinicalpharmacology')) ? (self.clinicalpharmacology = true, trackConnFilt('Specialty', 'clinicalpharmacology')) : (self.clinicalpharmacology = false);
		(_.contains(self.interests, 'dermatology')) ? (self.dermatology = true, trackConnFilt('Specialty', 'dermatology')) : (self.dermatology = false);
		(_.contains(self.interests, 'emergencyandcriticalcare')) ? (self.emergencyandcriticalcare = true) : (self.emergencyandcriticalcare = false);
		(_.contains(self.interests, 'internalmedicine')) ? (self.internalmedicine = true, trackConnFilt('Specialty', 'internalmedicine')) : (self.internalmedicine = false);
		(_.contains(self.interests, 'laboratorymedicine')) ? (self.laboratorymedicine = true, trackConnFilt('Specialty', 'laboratorymedicine')) : (self.laboratorymedicine = false);
		(_.contains(self.interests, 'microbiology')) ? (self.microbiology = true, trackConnFilt('Specialty', 'microbiology')) : (self.microbiology = false);
		(_.contains(self.interests, 'nutrition')) ? (self.nutrition = true, trackConnFilt('Specialty', 'nutrition')) : (self.nutrition = false);
		(_.contains(self.interests, 'ophthalmology')) ? (self.ophthalmology = true, trackConnFilt('Specialty', 'opthalmology')) : (self.ophthalmology = false);
		(_.contains(self.interests, 'pathology')) ? (self.pathology = true,trackConnFilt('Specialty', 'pathology')) : (self.pathology = false);
		(_.contains(self.interests, 'preventivemedicine')) ? (self.preventivemedicine = true, trackConnFilt('Specialty', 'preventivemedicine')) : (self.preventivemedicine = false);
		(_.contains(self.interests, 'radiology')) ? (self.radiology = true, trackConnFilt('Specialty', 'radiology')) : (self.radiology = false);
		(_.contains(self.interests, 'sportsmedicineandrehabilitation')) ? (self.sportsmedicineandrehabilitation = true, trackConnFilt('Specialty', 'sportsmedicineandrehabilitation')) : (self.sportsmedicineandrehabilitation = false);
		(_.contains(self.interests, 'surgery')) ? (self.surgery = true, trackConnFilt('Specialty', 'surgery')) : (self.surgery = false);
		(_.contains(self.interests, 'theriogenology')) ? (self.theriogenology = true, trackConnFilt('Specialty', 'theriogenology')) : (self.theriogenology = false);
		(_.contains(self.interests, 'toxicology')) ? (self.toxicology = true, trackConnFilt('Specialty', 'toxicology')) : (self.toxicology = false);
		(_.contains(self.interests, 'veterinarypractitioners')) ? (self.veterinarypractitioners = true, trackConnFilt('Specialty', 'veterinarypractitioners')) : (self.veterinarypractitioners = false);
		(_.contains(self.interests, 'zoologicalmedicine')) ? (self.zoologicalmedicine = true, trackConnFilt('Specialty', 'zoologicalmedicine')) : (self.zoologicalmedicine = false);

	    //hack to make userid 0 if not set
		userid = userid || 0;

	    // set search properties
		self.searchStr = userid +'?sort='+ self.searchSort
			+'&newmember='+ self.newmember
			+'&student='+ self.student
			+'&veterinarian='+ self.veterinarian
			+'&technician='+ self.technician
			+'&school='+ self.school
			+'&graduation='+ self.graduation
			+'&organizationid='+ self.organizationid
			+'&acquatic='+ self.acquatic
			+'&mixedcattle='+ self.mixedcattle
			+'&mixedswine='+ self.mixedswine
			+'&mixedequine='+ self.mixedequine
			+'&mixedcompanion='+ self.mixedcompanion
			+'&smallcanine='+ self.smallcanine
			+'&smallfeline='+ self.smallfeline
			+'&smallexotics='+ self.smallexotics
			+'&foodbeef='+ self.foodbeef
			+'&fooddairy='+ self.fooddairy
			+'&foodcow='+ self.foodcow
			+'&foodpoultry='+ self.foodpoultry
			+'&foodswine='+ self.foodswine
			+'&equine='+ self.equine
			+'&smallruminant='+ self.smallruminant
			+'&labanimal='+ self.labanimal
			+'&government='+ self.government
			+'&armedforces='+ self.armedforces
			+'&publichealth='+ self.publichealth
			+'&industry='+ self.industry
			+'&zoo='+ self.zoo
			+'&other='+ self.other
			+'&anesthesiology='+ self.anesthesiology
			+'&animalwelfare='+ self.animalwelfare
			+'&behavior='+ self.behavior
			+'&clinicalpharmacology='+ self.clinical
			+'&dermatology='+ self.dermatology
			+'&emergencyandcriticalcare='+ self.emergency
			+'&internalmedicine='+ self.internalmedicine
			+'&laboratorymedicine='+ self.laboratorymedicine
			+'&microbiology='+ self.microbiology
			+'&nutrition='+ self.nutrition
			+'&ophthalmology='+ self.ophthalmology
			+'&pathology='+ self.pathology
			+'&preventivemedicine='+ self.preventivemedicine
			+'&radiology='+ self.radiology
			+'&sportsmedicineandrehabilitation='+ self.sportsmedicine
			+'&surgery='+ self.surgery
			+'&theriogenology='+ self.theriogenology
			+'&toxicology='+ self.toxicology
			+'&veterinarypractitioners='+ self.veterinary
			+'&zoologicalmedicine='+ self.zoologicalmedicine;

		// get data
		self.getConnectUserData();
	}

	self.getConnectUserData = function()
	{
		var self = this;
		var url = '/api/userslist/';
		
		// clear results
		$('.results-data').html(' ');
		$('.no-results').hide();
		// show loader img
		$('.loaderimg').show();

		// check for active query
		if (!self.isSearching) {
			self.isSearching = true;
		} else {
			// abort current search
			self.searchajax.abort();			
		}

		// get data
		self.searchajax = $.ajax({
		   url: url + self.searchStr,
			format: JSON
		}).done(function (data) {
			self.isSearching = false;
			// check data
			if (data.length !== 0) {
			    /////console.log('count: ' + data.length);

			    // extract email
			    //var _email = data[0]['user'].optpublic;

				// parse data
				self.parseUserData(data);
			} else {
				// no data
				$('.loaderimg').hide();
				/////console.log('count: '+ data.length);
				// no results
				$('.no-results').show();
			}			
		});
	}

	self.parseUserData = function(data)
	{		
		var self = this;

		// Load template
		var source = $("#dataitem-template").html();

		// Compile Handlebars template
		var template = Handlebars.compile(source);
		
		// Render html
		var html = template(data);

		// hide loader img
		$('.loaderimg').hide();

		// add data to dom
		$('.results-data').html(html);
		
		//Refreshing results after someone clicks connect in the modal.
		$("body").on("click", ".btn-disconnect,.btn-connect", function(e){
			self.buildSearchStr();
		});
		

        //only apply if user is not logged in
		if (!userid || userid == 0) {
		    var doc = $(document);

		    //unbind click events on connect btn
		    doc.off('click', '.btnConnect');
		    doc.off('click', '.connect');

		    $('li.data-item').on('mouseenter', function () {
		        $(this).find('.connect_blurb').show();
		    });

		    $('li.data-item').on('mouseleave', function () {
		        $(this).find('.connect_blurb').hide();
		    });

		    $('.showme').on('mouseenter', function () {
		        $(this).find('.connect_blurb_showme').show();
		    });

		    $('.showme').on('mouseleave', function () {
		        $(this).find('.connect_blurb_showme').hide();
		    });



		}
	}

	self.sortFilter = function(type)
	{
		var self = this;
		// set sort type
		self.searchSort = type; //'sort='+
		// get data
		self.buildSearchStr();
	}

	self.updateAffiliation = function()
	{
		var self = this;
		$('#organizations').val(self.organizationStr);
	}

	self.bindEvents = function()
	{	
		var self = this;
		// cache elms
		var doc = $(document);

		// filter btns
		$('.icheck').on('ifChecked', function(event){
			if (!self.allchecked && !self.nonechecked) {
				var filter = $(this).attr('name');			

				trackConnFilt('membersort',filter + ' checked');

				switch (filter) {
					case 'newmembers':
					    self.newmember = true;
					break;

					case 'mentors':
					    break;

					case 'nearme':
					    self.nearme = true;
					    break;

					case 'student':
					self.student = true;
					break;

					case 'veterinarian':
					self.veterinarian = true;
					break;

					case 'technician':
					self.technician = true;
					break;
				}
				self.buildSearchStr();
			}
		});

		// filter btns
		$('.icheck').on('ifUnchecked', function(event){
			if (!self.allchecked && !self.nonechecked) {
				var filter = $(this).attr('name');		  	

				trackConnFilt('membersort', filter + ' unchecked');

				switch (filter) {
					case 'newmembers':
					self.newmember = false;
					break;

					case 'mentors':
					break;

					case 'nearme':
					break;	

					case 'student':
					self.student = false;
					break;

					case 'veterinarian':
					self.veterinarian = false;
					break;

					case 'technician':
					self.technician = false;
					break;
				}				
				self.buildSearchStr();
			}
		});

		$('.icheck').on('ifClicked', function(event){
			self.allchecked = false;
			self.nonechecked = false;
		});

		// filter school	
		$('#school').on('change', function(e){		
		    self.school = $(this).val();
		    trackConnFilt('school',self.school);
			/////console.log(this);
			self.buildSearchStr();
		});

		// filter organizations	
		$('#organizations').on('change', function(e){		
		    self.organizations = $(this).val();
		    trackConnFilt('organizations', self.organizations);
		    self.buildSearchStr();
		});
		
		// filter graduation	
		$('#graduation').on('change', function(e){		
			self.graduation = $(this).val();
			trackConnFilt('graduation', self.graduation);
			self.buildSearchStr();
		});

		// checkbox filters
		$('.checkbox-filters').click(function(e){
			e.preventDefault();
			switch($(this).attr('data-switch')) {
				case 'all':
				self.allchecked = true;
				self.student = true;
				self.veterinarian = true;
				self.technician = true;
				$('.educationchk').iCheck('check');
				break;

				case 'none':
				$('.educationchk').iCheck('uncheck');				
				self.nonechecked = true;
				self.student = false;
				self.technician = false;
				self.veterinarian = false;	
				break;
			}
			self.buildSearchStr();
		});

		// sort btns
		var sortBtns = $('.sort-filter a');
		sortBtns.click(function(e){
			e.preventDefault();
			sortBtns.removeClass('active');
			$(this).addClass('active');
			self.sortFilter($(this).attr('data-sort'));
			
		});

		// remove tag from list
		$(doc).on('click', '.removetag', function(e){
			e.preventDefault();
			// remove item from property list
			self.interests = _.without(self.interests, $(this).attr('data-tag'));
			// remove item from dom
			$(this).parent().remove();
			// build search
			self.buildSearchStr();
		})

		// show me btn
		$(doc).on('click', '.btnShowMe', function(e){
			e.preventDefault();
		    // show only connect check items && only when signed in
			if (userid > 0) {
			    window.location.href = '/connectbrowse';
			}
		});

		// bind school auto complete
		self.bindSchoolAutoComplete();

		// add iCheck plugin to filter list
		$('.icheck').iCheck({
			checkboxClass: 'icheckbox_minimal-orange'	
		});

		//tabs for connect page
		$('.module-tab-mobile-conn').click(function(){
			var $tab = $(this);
			if($tab.hasClass('m-tab-active')){
				//do nothing
			}else{
				//add active tab to this and remove from the other
				$('.module-tabs-mobile > div').removeClass('m-tab-active');
				$tab.addClass('m-tab-active');

				if($tab.hasClass('m-info-tab')){
					//now show proper bottom panel by hiding all then showing proper
					$('.bottom-panel > div').hide();
					$('.bottom-panel-info').fadeIn();
				}else{
					$('.bottom-panel > div').hide();
					$('.bottom-panel-connections').fadeIn();
				}
				
			}
		});

		//connect button
		$('.connect-btn').click(function(){
			var elem = this;
			//trigger api call
			var connectid = $(this).attr('data-connectionid');
			//var connectUrl = 'http://vetvance-api.dev.thebloc.com/api/Connect/' + userid + '?connectid=' + connectid;
			var connectUrl = '/api/Connect/' + userid + '?connectid=' + connectid;
			$.ajax({
			   url: connectUrl,
				format: JSON
			}).done(function (data) {
				///console.log(data);
				if(data === true || data == 'true'){
					//show email
					$(elem).hide();
					$('.connected-email').show();
				}else{
					alert('BLARK. The connection did not work, please try again later');
				}
			});
		});

		

	    //bootstrap hack that hides popovers when user clicks off. Fixing bootstrap core bug without hacking the core.
		$('body').on('hidden.bs.popover', function () {
		    $('.popover:not(.in)').hide().detach();
		});
	}

	return self;

}());
$(document).ready(function(){
    VETAPP.pages.connect.init();
});

//random utility functions
function toCsv(data){
	if(_.isArray(data)){
		_csv = [];
		if(data.length > 1){
			//is jobs array
		}else{
			_.each(data[0], function(value, key, list){
				if(value === true)
					_csv.push(key);
			})
		}
		
		
		
		if(_csv.length > 0)
			return _csv.join(', ');
		else
			return '';
	}else{
		return data;
	}
}