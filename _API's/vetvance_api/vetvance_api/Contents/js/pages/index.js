// add page to namespace
VETAPP.namespace('VETAPP.pages.index');

// create page
VETAPP.pages.index = (function() {

	var self = {};

	self.init = function init() { //console.log('init page: index.js');

		var self = this;

		self.isMobile 	= isMobile(); //window.orientation || Modernizr.touch;
		self.container = $('.main-container');
		self.pageId		= $('#pageId');
		
		//if we don't do this, there will be blood - promo var isnt set on any other page than index and breaks js loading
		if(self.isMobile !== true){
			// init Modules
			self.initModules();
			// Bind events
			self.bindEvents();

			if(Modernizr.touch){
				$('#flashContent').html('<img style="width:966px; height: 523px;" src="/Contents/imgs/index/bg.png" id="ipadIndexStaticBg" /><img style="cursor: pointer; position:absolute; top: 238px; left: 780px; height: 50px; width: auto;" src="/Contents/imgs/index/indexbtn.png" id="ipadIndexStaticBtn"/>').click(function(){
					window.location.href = '/advance';
				});

				//modal video fix
				$('#home_video').css('width', '1px').css('height', '1px');
				$('#home_video').append('<img id="hack-img" src="/Contents/imgs/promoposter.jpg" width="939" height="528"/>').find('#hack-img').click(function(){
					//hide hack image
					$(this).remove();
					$('#home_video').css('width', '939px').css('height', '528px');
					var vid = videojs('home_video');
					vid.play();

				});
				// $('#staticIndexContent').show()
			}
		}else{
			//swap desktop flash obj w image
			if(Modernizr.touch){
				$('#flashContent').html('<img style="width:966px; height: 523px;" src="/Contents/imgs/index/bg.png" id="ipadIndexStaticBg" /><img style="cursor: pointer; position:absolute; top: 238px; left: 780px; height: 50px; width: auto;" src="/Contents/imgs/index/indexbtn.png" id="ipadIndexStaticBtn"/>').click(function(){
					window.location.href = '/advance';
				});

				//modal video fix
				$('#home_video').css('width', '1px').css('height', '1px');
				$('#home_video').append('<img id="hack-img" src="/Contents/imgs/promoposter.jpg" width="939" height="528"/>').find('#hack-img').click(function(){
					//hide hack image
					$(this).remove();
					$('#home_video').css('width', '939px').css('height', '528px');
					var vid = videojs('home_video');
					vid.play();

				});
				// $('#staticIndexContent').show()
			}

			//alert('YO WASSUP!');
			if(userid){
				$('#zuku-reg-mobile').hide();

				//show courselist
				$('#courselist-mobile').show();
			}else{
				$('#courselist-mobile').hide();
				$('#zuku-reg-mobile').show();
			}
		}
		

		function isMobile(){
			//check browser size, anything below 768 we consider 'mobile'
			if($(window).width() <= 768){
				return true;
			}else{
				return false;
			}
		}
	}

	self.animateSlides = function()
	{
		var activeSlide = 1;
		var slideCount = 3;

		// fade in first slide
		$('#slide'+activeSlide).fadeIn(500, 'easeInSine');

		// slide fade timer
		var timerFunction = function() {

			if (activeSlide < slideCount)
				// fade slides
				$('#slide'+activeSlide).fadeOut(500, 'easeOutSine', function(){
					activeSlide ++;
			  		$('#slide'+activeSlide).fadeIn(500, 'easeInSine');
			  	});
			else
				clearInterval(timer);
		}

		// set slide timer
		var timer = setInterval(timerFunction, 4000);
	}

	self.initModules = function()
	{
		var self = this;

		// override jquery validate plugin defaults for bootstrap error class
		$.validator.setDefaults({
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    }
		});

		// set up registration form
		self.setUpRegistrationForm();

		if(!_.isUndefined(promo)){
			// show modal
			if (promo == 'show')
				$('#modal_LightBox').modal('show');

		}
		
		// activate slide timer
		self.animateSlides();
	}

	self.setUpRegistrationForm = function()
	{
    	// cache elms
    	var school 			= $('#school');
    	var schoolnames 	= $('#schoolnames');
    	var register_form = $('#register-form');

      // clear school vals
		$('.members-container .radiobtn').on('ifChecked', function(event){
         schoolnames.html('');
         school.val('');

         // show school input field
         school.show();

         // set autocomplete
         if ($(this).val() !== 'Vet') {
         	// destroy autocomplete
         	school.autocomplete( "destroy" );
         } else {

	    	   // bind autocomplete to school name
		    	school.autocomplete({
		     		source: function( request, response ) {
        				$.ajax({
          				url: "/api/searchSchool/" + $('#school').val(),
          				format: JSON,
							success: function(data){
				            response( $.map( data, function( item ) {
				              	return {
				               	label: item.schoolname,
				                	value: item.schoolname
				            	}
				            }));
							}
          			});
          		}
		    	});
         }
		});

		// get career types
		$.ajax({
		   url: "/api/careertypes",
		   // data: { key: val },
		   format: JSON
		}).done(function (data) {
		   var _careerhtml = '<option value="">--Select one--</option>';
		   $.each(data, function (_careerkey) {
		       _careerhtml += '<option value="' + data[_careerkey].careertypeid + '">';
		       _careerhtml += data[_careerkey].careertypename;
		       _careerhtml += '</option>';
		   });
		   $('#careertypeid').html(_careerhtml);
		});

		// get security questions
		$.ajax({
		   url: "/api/securityquestions",
		   // data: { key: val },
		   format: JSON
		}).done(function (data) {
		   ///console.log(data);

		   var _securityhtml = '<option value="">--Select one--</option>';
		   $.each(data, function (_securitykey) {
		       _securityhtml += '<option value="' + data[_securitykey].securityquestionname + '">';
		       _securityhtml += data[_securitykey].securityquestionname;
		       _securityhtml += '</option>';
		   });
		   $('#securityquestion').html(_securityhtml);
		});

		// validate registration form
		register_form.validate({
		  	rules: {
		    	email: {
		    		required: true,
		      	email: true
				},
		    	email_confirm: {
		    		required: true,
	     			equalTo: "#email"
				},
				password: {
    				required: true,
    				minlength: 8
				},
    			password_confirm: {
    				required: true,
      			equalTo: "#password"
    			},
    			firstname: {
    				required: true,
    				minlength: 2,
    			},
    			lastname: {
    				required: true,
    				minlength: 2
    			},
    			careertypeid: "required",
    			school: {
    				required: true,
    				minlength: 2
    			},
    			optterms: {
    				required: true,
    			},
    			graduation: {
    				required: true,
    			},
    			securityquestion: {
    				required: true,
    			},
    			securityanswer:
    			{
    				required: true,
    				minlength: 2,
    			}
		  	},
		  	messages: {
    			email: {
    				required: 'Please enter your email address.',
    				email: 'Invalid email address. Please enter your email address in the format: example@email.com.'
    			},
    			email_confirm: {
    				required: 'Please confirm your email address.',
    				email: 'Invalid email address. Please enter your email address in the format: example@email.com.',
    				equalTo: 'You email address does not match. Please enter your email address.'
    			},
    			password: {
    				required: 'Please enter a password.',
    				minlength: 'Your password must be at least 8-16 characters including at least 1 number. Please try again.'
    			},
    			password_confirm: {
    				required: 'Please confirm your password.',
    				equalTo: 'Your password does not match. Please enter your password.'
    			},
    			firstname: {
    				required: 'Please enter your first name.'
    			},
    			lastname: {
    				required: 'Please enter your last name.'
    			},
    			careertypeid: {
    				required: 'Please tell us where you are in your career.'
    			},
    			school: {
    				required: 'Please tell us where you go, or went, to school.'
    			},
    			graduation: {
    				required: 'Please select a graduation date.'
    			},
    			securityquestion: {
    				required: 'Please select a security question.'
    			},
    			securityanswer: {
    				required: 'Please provide an answer to the security question.'
    			},
    			optterms: {
    				required: ''
    			}
    		},
		  	invalidHandler: function(event, validator) {

		  		if ( $('input[name=optterms]').val() !== '1'){
		  			$('.touerror').show();
		  			$('#optterms').closest('.icheckbox_minimal-orange').addClass('error');

		  		} else {
		  			$('.touerror').hide();
		  		}

    			var errors = validator.numberOfInvalids();
    			if (errors > 0)
    				$('.required-msg')
    					.html('Please check the highlighted fields below.')
    					.addClass('has-error');
    		},
    		submitHandler: function(form) {
    			$(form).attr('action', '/Process/Register?redirect=/registersuccess');
    			form.submit();
  			}
		});
		// add alphanumeric
		$.validator.addMethod('alphanumeric', function (value, element) {
		    return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
		}, 'Password must contain at least one numeric and one alphabetic character.');
	}

	self.bindEvents = function()
	{
		var self = this;

		// cache elms
		var modal = $('.modal_lightBoxContent_video');
		var modalContent = $('.modal_lightBoxContent');

		// add icheck custom events
		$('#optterms').on('ifChecked', function(event){
			$('#optterms').closest('.icheckbox_minimal-orange').removeClass('error');
			$('input[name=optterms]').val('on');
		});
		// add icheck uncheck
		$('#optterms').on('ifUnchecked', function(event){
			$('input[name=optterms]').val('');
		});

		$('.home-page-btn').mouseenter(function () {
			$(this).parent().find('.tooltip_footer').show();
		});

		$('.tooltip_footer').mouseleave(function () {
			$('.tooltip_footer').hide();
		});

		// prompt for video promo
		$('.video_promo').click(function(e) {
			e.preventDefault();
			$('#modal_LightBox').modal('show');
		});

		$('a.promo_video_link').click(function(e){
      	    e.preventDefault();
        	modal.animate({'top':'0px'},350);
        	modalContent.css({'height':'568px'});
     	});

     	$('a.register_toggle').click(function(e){
     		e.preventDefault();
      	    modal.animate({'top':'-566px'},350);
      	    modalContent.css({ 'height': '673px' });

     	    //pause video
      	    videojs('#home_video').pause();
     	});


     	videojs("#home_video").ready(function () {
     	    //stop video
     	    var vid = this;
     	    $('#modal_LightBox').on('hide.bs.modal', function () {
     	        vid.pause();
     	        TracktSimple.track("PromoVideo", "Pause", "Homepage");
     	    });

     	    $('.promo_video_link').click(function () {
     	        vid.play();
     	        TracktSimple.track("PromoVideo", "Play", "PromoLink");

     	    });


     	    $('#home_video').click(function () {
                //default action
     	        vid.play();
     	        TracktSimple.track("PromoVideo", "Play", "Homepage");

     	        //event handler for play event
     	        $('#home_video').on('play', function () {
     	            $('#home_video').click(function () {
     	                //unbind all events
     	                $('#home_video').unbind('click');

     	                //send to registration
     	                window.location = '/register';
     	            });
     	        });

                //event handler for ended
     	        $('#home_video').on('ended', function () {
     	            TracktSimple.track("PromoVideo", "Complete", "Homepage");
     	            $('#home_video').click(function () {
     	                vid.play();
     	                TracktSimple.track("PromoVideo", "EndedAndStarted", "Homepage");
     	            });
     	        });
     	    });
     	});

		


	}
	return self;
}());

$(document).ready(function(){
	VETAPP.pages.index.init();
});