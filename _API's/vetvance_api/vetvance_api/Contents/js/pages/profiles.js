// add page to namespace
VETAPP.namespace('VETAPP.pages.profiles');

// create page
VETAPP.pages.profiles = (function() {
    // create dictionary object
    var dictionary = {};
    $.ajax({
        url: "/api/Dictionary",
        format: JSON
    }).done(function (data) {
        /////console.log(data);

        $.each(data, function (_key) {
            // add key:value
            dictionary[data[_key].keyword] = data[_key].value;
        });
    });

    var self = {};

	self.init = function init() { ///console.log('init page: profiles.js');

		var self = this;
		
		self.isMobile 		= window.orientation || Modernizr.touch;
		self.container 		= $('.main-container');
		self.pageId				= $('#pageId');

		// page modules
		self.popover = {};

		//mobile select
		self.orgSelect = '';
		self.interestMobileSelect = '';
		self.expertiseMobileSelect = '';

		// public properties
		if (self.pageId.hasClass('update-profile-container')){
			self.saveStr 							= '';
			// interests
			self.interests 						= [];			
			self.acquatic							= false;
			self.mixedcattle					= false;
			self.mixedswine						= false;
			self.mixedequine 					= false;
			self.mixedcompanion 			= false;
			self.smallcanine 					= false;
			self.smallfeline 					= false;
			self.smallexotics 				= false;
			self.foodbeef 						= false;
			self.fooddairy 						= false;
			self.foodcow 							= false;
			self.foodpoultry 					= false;
			self.foodswine 						= false;
			self.equine 							= false;
			self.smallruminant 				= false;
			self.labanimal 						= false;
			self.government 					= false;
			self.armedforces 					= false;
			self.publichealth 				= false;
			self.industry 						= false;
			self.zoo 									= false;
			self.other 								= false;
			// expertise
			self.specialty 						= [];
			self.anesthesiology 			= false;
			self.animalwelfare 				= false;
			self.behavior 						= false;
			self.clinicalpharmacology 						= false;
			self.dermatology 					= false;
			self.emergency 						= false;
			self.internalmedicine 		= false;
			self.laboratorymedicine 	= false;
			self.microbiology 				= false;
			self.nutrition 						= false;
			self.ophthalmology 				= false;
			self.pathology 						= false;
			self.preventivemedicine 	= false;
			self.radiology						= false;
			self.sportsmedicine 			= false;
			self.surgery 							= false;
			self.theriogenology 			= false;
			self.toxicology 					= false;
			self.veterinary 					= false;
			self.zoologicalmedicine 	= false;

			self.organizationid				= 0;
			self.organizationStr			= '';

			// all checked
			self.allchecked 				= false;
			self.nonechecked				= false;
		}

		// run page routines
		if (self.pageId.hasClass('profile-container') || self.pageId.hasClass('update-profile-container')){

			// init Modules
			self.initModules();

			// get user profile data
			self.getUserData();
		
			// Bind events
			self.bindEvents();
		}
	}

	self.initModules = function () 
	{
		var self = this;

		// update specific 
		if (self.pageId.hasClass('profile-container')){
			///console.log('add profile connections here')
			

		}

    // add update-profile-container modules
    if (self.pageId.hasClass('update-profile-container')) {
	    // get security questions
	    $.ajax({
        url: "/api/securityquestions",
        format: JSON
	    }).done(function (data) {
        var _securityhtml = '<option value="">Select Security Question</option>';
        $.each(data, function (_securitykey) {
            _securityhtml += '<option value="' + data[_securitykey].securityquestionname + '">';
            _securityhtml += data[_securitykey].securityquestionname;
            _securityhtml += '</option>';
        });
        $('#securityquestion').html(_securityhtml);
	    });

      // add "Interest / Practice" popover
	    var btnInterest = new VETAPP.Classes.Popover({
        type: 'interest',
        btnId: 'btnInterest',
        direction: 'right',
        output: '#filtered-tags-interest'
      }, $('.update-profile-container'));

      // add "Expertise / Specialty" popover
      var btnSpecialty = new VETAPP.Classes.Popover({
        type: 'specialty',
        btnId: 'btnSpecialty',
        direction: 'left',
        output: '#filtered-tags-specialty'
      }, $('.update-profile-container'));

      // add "Affiliations" popover
      var btnSpecialty = new VETAPP.Classes.Popover({
        type: 'affiliations',
        btnId: 'btnLookUp',
        direction: 'left',
        output: '#tags-affiliations'
      }, $('.update-profile-container')); //output used to be 'filtered-tags-specialty'
    }

    // override jquery validate plugin defaults for bootstrap error class
    $.validator.setDefaults({
        highlight: function (element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function (element) {
            $(element).closest('.form-group').removeClass('has-error');
        }
    });

    	//initialize mobile affiliation/expertise/job select
	    if (self.pageId.hasClass('mobile-update-container')){
	    	//get org data
	    	$.ajax({
		          url: "/api/Organizations/",          
		          format: JSON
		      }).done(function (data) {
		      	///console.log(data);
		      	var selectHtml = '<select id="affiliation-mobile-select"><option value=""></option>';
		      	_.each(data, function(value, key, list){
		      		//add to select
		      		selectHtml += '<option data-organizationid="' + value.organizationid + '" data-organizationname="' + value.organizationname + '" value="' + value.organizationname + '">' + value.organizationname + '</option>';
		      	});
		      	selectHtml += '</select>';

		      	var mobileAffWrap = $('#affiliation-mobile-update-wrap');
		      	mobileAffWrap.html(selectHtml);
		      	var $select = $('#affiliation-mobile-select').selectize({
		      		highlight: false,
		      		maxOptions: 5
		      	});
		      	self.orgSelect = $select[0].selectize;

		      	//set up handlers
		      	$('.btn-add.affiliation').click(function(){
		      		//get value
		      		//var orgSelect = $('#affiliation-mobile-select');
		      		var orgValue = self.orgSelect.getValue();

		      		//get jquery obj for option
		      		var $org = self.orgSelect.getOption(orgValue);

		      		//pull orgid
		      		var organizationIdMobile = orgValue;
		      		var organizationNameMobile = $org.text();

		      		var url = "/api/saveAffiliation/";		
					$.ajax({
					    url: url + userid +"?organizationid=" + organizationIdMobile,
					    format: JSON
					}).done(function (data) {
						// update dom
						var str = '<li class="affiliation" data-id="'+ organizationIdMobile +'">'
				    				+'<span>'+ organizationNameMobile +'</span>'
				    				+'<a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item affiliation" alt="" /></a>'
				    			+'</li>';
					    
					    // add item to list
					    $('#tags-affiliations').append(str);
				    	
				    	//clear form
				    	self.orgSelect.clear();

					});
		      	});
		      });

			$interestSelect = $('#interest-mobile-select').selectize({
				highlight: false,
				maxOptions: 5
			});
			self.interestMobileSelect = $interestSelect[0].selectize;

			//save interests
			$('.btn-add.interest').click(function(){
				var interestUpdate = self.interestMobileSelect.getValue();
				var $interest = self.interestMobileSelect.getOption(interestUpdate);

				//add interest to array
				self.interests.push(interestUpdate);

				//save interests
				self.saveInterest();

				//clear input
				self.interestMobileSelect.clear();

				//add to box
				var str = '<li class="interests" data-id="'+ interestUpdate +'">'
		    				+'<span>'+ $interest.text() +'</span>'
		    				+'<a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item interest" alt="" /></a>'
		    			+'</li>';
			    
			    // add item to list
			    $('#tags-interest').append(str);
			});

			//initialize selectizes
			$expertiseSelect = $('#expertise-mobile-select').selectize({
				highlight: false,
				maxOptions: 5
			});
			self.expertiseMobileSelect = $expertiseSelect[0].selectize;

			//save expertise
			$('#mobile-add-specialty').click(function(e){
				e.preventDefault();
				var expertiseUpdate = self.expertiseMobileSelect.getValue();
				var $expertise = self.expertiseMobileSelect.getOption(expertiseUpdate);

				//add expertise 
				self.specialty.push(expertiseUpdate);

				//save
				self.saveSpecialty();

				//clear input
				self.expertiseMobileSelect.clear();

				//add to box
				var str = '<li class="interests" data-id="'+ expertiseUpdate +'">'
		    				+'<span>'+ $expertise.text() +'</span>'
		    				+'<a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item specialty" alt="" /></a>'
		    			+'</li>';
			    
			    // add item to list
			    $('#filtered-tags-specialty').append(str);
			});
		}
	}

	self.saveInterest = function()
	{
		var self = this;
		// check bool vals
		(_.contains(self.interests, 'acquatic')) ? (self.acquatic = true) : (self.acquatic = false);
		(_.contains(self.interests, 'mixedcattle')) ? (self.mixedcattle = true) : (self.mixedcattle = false);
		(_.contains(self.interests, 'mixedswine')) ? (self.mixedswine = true) : (self.mixedswine = false);
		(_.contains(self.interests, 'mixedequine')) ? (self.mixedequine = true) : (self.mixedequine = false);
		(_.contains(self.interests, 'mixedcompanion')) ? (self.mixedcompanion = true) : (self.mixedcompanion = false);
		(_.contains(self.interests, 'smallcanine')) ? (self.smallcanine = true) : (self.smallcanine = false);
		(_.contains(self.interests, 'smallfeline')) ? (self.smallfeline = true) : (self.smallfeline = false);
		(_.contains(self.interests, 'smallexotics')) ? (self.smallexotics = true) : (self.smallexotics = false);
		(_.contains(self.interests, 'foodbeef')) ? (self.foodbeef = true) : (self.foodbeef = false);
		(_.contains(self.interests, 'fooddairy')) ? (self.fooddairy = true) : (self.fooddairy = false);
		(_.contains(self.interests, 'foodcow')) ? (self.foodcow = true) : (self.foodcow = false);
		(_.contains(self.interests, 'foodpoultry')) ? (self.foodpoultry = true) : (self.foodpoultry = false);
		(_.contains(self.interests, 'foodswine')) ? (self.foodswine = true) : (self.foodswine = false);
		(_.contains(self.interests, 'equine')) ? (self.equine = true) : (self.equine = false);
		(_.contains(self.interests, 'smallruminant')) ? (self.smallruminant = true) : (self.smallruminant = false);
		(_.contains(self.interests, 'labanimal')) ? (self.labanimal = true) : (self.labanimal = false);
		(_.contains(self.interests, 'government')) ? (self.government = true) : (self.government = false);
		(_.contains(self.interests, 'armedforces')) ? (self.armedforces = true) : (self.armedforces = false);
		(_.contains(self.interests, 'publichealth')) ? (self.publichealth = true) : (self.publichealth = false);
		(_.contains(self.interests, 'industry')) ? (self.industry = true) : (self.industry = false);
		(_.contains(self.interests, 'zoo')) ? (self.zoo = true) : (self.zoo = false);
		(_.contains(self.interests, 'other')) ? (self.other = true) : (self.other = false);
		
		// set search properties
		self.saveStr = userid +'?'
			+'acquatic='+ self.acquatic
			+'&mixedcattle='+ self.mixedcattle
			+'&mixedswine='+ self.mixedswine
			+'&mixedequine='+ self.mixedequine
			+'&mixedcompanion='+ self.mixedcompanion
			+'&smallcanine='+ self.smallcanine
			+'&smallfeline='+ self.smallfeline
			+'&smallexotics='+ self.smallexotics
			+'&foodbeef='+ self.foodbeef
			+'&fooddairy='+ self.fooddairy
			+'&foodcow='+ self.foodcow
			+'&foodpoultry='+ self.foodpoultry
			+'&foodswine='+ self.foodswine
			+'&equine='+ self.equine
			+'&smallruminant='+ self.smallruminant
			+'&labanimal='+ self.labanimal
			+'&government='+ self.government
			+'&armedforces='+ self.armedforces
			+'&publichealth='+ self.publichealth
			+'&industry='+ self.industry
			+'&zoo='+ self.zoo
			+'&other='+ self.other;
		
		// save data
		$.ajax({
     	url: "/api/updateUserInterest/"+ self.saveStr,
    	format: JSON
    }).done(function (data) {
    	/////console.log(data);
    });
	}

	self.saveSpecialty = function()
	{
		var self = this;
		// check bool vals
		(_.contains(self.specialty, 'anesthesiology')) ? (self.anesthesiology = true) : (self.anesthesiology = false);
		(_.contains(self.specialty, 'animalwelfare')) ? (self.animalwelfare = true) : (self.animalwelfare = false);
		(_.contains(self.specialty, 'behavior')) ? (self.behavior = true) : (self.behavior = false);
		(_.contains(self.specialty, 'clinicalpharmacology')) ? (self.clinicalpharmacology = true) : (self.clinicalpharmacology = false);
		(_.contains(self.specialty, 'dermatology')) ? (self.dermatology = true) : (self.dermatology = false);
		(_.contains(self.specialty, 'emergencyandcriticalcare')) ? (self.emergencyandcriticalcare = true) : (self.emergencyandcriticalcare = false);
		(_.contains(self.specialty, 'internalmedicine')) ? (self.internalmedicine = true) : (self.internalmedicine = false);
		(_.contains(self.specialty, 'laboratorymedicine')) ? (self.laboratorymedicine = true) : (self.laboratorymedicine = false);
		(_.contains(self.specialty, 'microbiology')) ? (self.microbiology = true) : (self.microbiology = false);
		(_.contains(self.specialty, 'nutrition')) ? (self.nutrition = true) : (self.nutrition = false);
		(_.contains(self.specialty, 'ophthalmology')) ? (self.ophthalmology = true) : (self.ophthalmology = false);			
		(_.contains(self.specialty, 'pathology')) ? (self.pathology = true) : (self.pathology = false);
		(_.contains(self.specialty, 'preventivemedicine')) ? (self.preventivemedicine = true) : (self.preventivemedicine = false);
		(_.contains(self.specialty, 'radiology')) ? (self.radiology = true) : (self.radiology = false);
		(_.contains(self.specialty, 'sportsmedicineandrehabilitation')) ? (self.sportsmedicineandrehabilitation = true) : (self.sportsmedicineandrehabilitation = false);
		(_.contains(self.specialty, 'surgery')) ? (self.surgery = true) : (self.surgery = false);
		(_.contains(self.specialty, 'theriogenology')) ? (self.theriogenology = true) : (self.theriogenology = false);
		(_.contains(self.specialty, 'toxicology')) ? (self.toxicology = true) : (self.toxicology = false);
		(_.contains(self.specialty, 'veterinarypractitioners')) ? (self.veterinarypractitioners = true) : (self.veterinarypractitioners = false);
		(_.contains(self.specialty, 'zoologicalmedicine')) ? (self.zoologicalmedicine = true) : (self.zoologicalmedicine = false);

		// set search properties
		self.saveStr = userid +'?'
			+'&anesthesiology='+ self.anesthesiology
			+'&animalwelfare='+ self.animalwelfare
			+'&behavior='+ self.behavior
			+'&clinicalpharmacology='+ self.clinicalpharmacology
			+'&dermatology='+ self.dermatology
			+'&emergencyandcriticalcare='+ self.emergency
			+'&internalmedicine='+ self.internalmedicine
			+'&laboratorymedicine='+ self.laboratorymedicine
			+'&microbiology='+ self.microbiology
			+'&nutrition='+ self.nutrition
			+'&ophthalmology='+ self.ophthalmology
			+'&pathology='+ self.pathology
			+'&preventivemedicine='+ self.preventivemedicine
			+'&radiology='+ self.radiology
			+'&sportsmedicineandrehabilitation='+ self.sportsmedicine
			+'&surgery='+ self.surgery
			+'&theriogenology='+ self.theriogenology
			+'&toxicology='+ self.toxicology
			+'&veterinarypractitioners='+ self.veterinary
			+'&zoologicalmedicine='+ self.zoologicalmedicine;

		// save data
		$.ajax({
     	url: "/api/updateUserSpecialty/"+ self.saveStr,
    	format: JSON
    }).done(function (data) {
    	/////console.log(data);
    });
	}

	self.getUserData = function()
	{	
		var self = this;
		// get user data
    $.ajax({
        url: "/api/Users/"+ userid,          
        format: JSON
    }).done(function (data) {
	      if (self.pageId.hasClass('profile-container')){
	       	// parse user data   
	       	self.parseUserProfileData(data[0]);

	      } else if (self.pageId.hasClass('update-profile-container')){
					// SSO
					$.ajax({
					   url: "/api/SSO/"+ guid,
					   type: "POST",
					   format: JSON
					}).done(function (data) {
					   $('#password').val(data[0].password);
					   $('#password_confirm').val(data[0].password);
					   $('#securityquestion').val(data[0].securityquestion);
					   $('#securityanswer').val(data[0].securityanswer);
					});

					// parse user data
					self.parseUpdateProfileData(data[0]);
		    }
    });
	}

	self.parseUserProfileData = function(data)
	{
		var self = this;
    var user = data;
    var nodata = "n/a";

    // parse user data
    ///console.log(user);

    function checkVal(val) {
    	if (val !== '')
    		return val;
    	else
    		return 'n/a';
    }

    // avatar
    if (user.avatar !== '')
    	$('.user-photo').html('<img src="/Contents/avatars/'+ user.avatar +'" class="user-avatar img-circle" alt="" />');

    // parse display data      
    if (user.firstname !== '')
			$('.user-data .display h1').html(user.firstname + ' ' + user.lastname);
		else
			$('.user-data .display h1').html(nodata);

		// title
		$('.user-data .display h2').html(checkVal(user.title));

		// email
		$('.user-data .display h3').html(checkVal(user.email));
		
		// zip
		$('.data-items .zip').html(checkVal(user.zip));
		
		// profession
		$('.data-items .profession').html(checkVal(user.careertype.careertypename));
		
		// school
		$('.data-items .school').html(checkVal(user.school));
		
		// graduation
		$('.data-items .graduation').html(checkVal(user.graduation));
	
		// private parseDataList
		function parseDataList(datalist){
			var _html = '';
			if (datalist.length !==0) {
				$.each(datalist[0], function (index) {		
					// only items that are true
				    if (datalist[0][index] == true) {
				        if (dictionary[index] != null) {
				            _html += '<li>' + dictionary[index] + '</li>';
				        }
				  	}
				});
			}
			if (_html == '')
				_html = '<li>none</li>';
			return _html;
		}

		// parse user interests
		$('.interest').html(parseDataList(user.userinterests));

		// parse user specialties
		$('.specialty').html(parseDataList(user.userspecialties));

		// parse user affiliation
		$('.affiliations').html(function(){
			var _html = '';
			if (user.affiliations.length !==0) {
				$.each(user.affiliations, function (index) {
				  _html += '<li>'+ user.affiliations[index].organization.organizationname +'</li>';
				});				
			}
			if (_html == '')
				_html = '<li>none</li>';
			return _html;
		});

		// parse user jobs
		$('.jobs').html(function(){
			var _html = '';
			if (user.userjobs.length !==0) {
	          $.each(user.userjobs, function (index) {
	              _html += '<li>'+ user.userjobs[index].title +' at '+ user.userjobs[index].company +' '+ user.userjobs[index].year +'</li>';
	          });
			}
			if (_html == '')
				_html = '<li>none</li>';

			return _html;
		});

		// privacy
		var _privacy = '';   
   	if (user.optpublic == true) { _privacy += '<li>Allow others to see my email address.</li>'; }
    if (user.optemail == true) { _privacy += '<li>You are accepting communications from Zoetis.</li>'; }
		// add privacy
		$('.privacy-container ul').append(_privacy);
	}

	self.returnEntityName = function(name){
		var self = this;
		var interestKey = {
			"acquatic": "Acquatic",
			"mixedcattle": "Mostly Cattle",
			"mixedswine": "Mostly Swine",
			"mixedequine": "Mostly Equine",
			"mixedcompanion": "Mostly Companion",
			"smallcanine": "Small Canine",
			"smallfeline": "Feline",
			"smallexotics": "Exotics",
			"foodbeef": "Beef Cattle",
			"fooddairy": "Dairy Cattle",
			"foodcow": "Cow Calf",
			"foodpoultry": "Poultry",
			"foodswine": "Swine",
			"equine": "Equine",
			"smallruminant": "Small Ruminant",
			"labanimal": "Lab Animal",
			"government": "Government",
			"armedforces": "Armed Forces",
			"publichealth": "Public Health",
			"industry": "Industry",
			"zoo": "Zoo/Exotic",
			"other": "Other",
			"anesthesiology": "Anesthesiology",
			"animalwelfare": "Animal Welfare",
			"behavior": "Behavior",
			"clinicalpharmacology": "Clinical Pharmacology",
			"dermatology": "Dermatology",
			"emergencyandcriticalcare": "Emergency and Critical Care",
			"internalmedicine": "Internal Medicine",
			"laboratorymedicine": "Laboratory Medicine",
			"microbiology": "Microbiology",
			"nutrition": "Nutrition",
			"ophthalmology": "Ophthalmology",
			"pathology": "Pathology",
			"preventivemedicine": "Preventive Medicine",
			"radiology": "Radiology",
			"sportsmedicineandrehabilitation": "Sports Medicine and Rehabilitation",
			"surgery": "Surgery",
			"theriogenology": "Theriogenology",
			"toxicology": "Toxicology",
			"veterinarypractitioners": "Veterinary Practitioners",
			"zoologicalmedicine": "Zoological Medicine"
		}

		if(_.has(interestKey, name)){
			return interestKey[name];
		}
	}

	self.parseUpdateProfileData = function(data)
	{
		var self = this;
		var user = data;
		///console.log(data);

		// set avatar
		$('.user-avatar').attr('src', '/contents/avatars/'+ user.avatar );

		// set basic data
		$('input[name=firstname]').val(user.firstname);
		$('input[name=lastname]').val(user.lastname);
		$('input[name=email]').val(user.email);
		$('input[name=zip]').val(user.zip);
		$('input[name=profession]').val(user.careertype.careertypename);
		$('input[name=title]').val(user.title);
		$('#graduation').val(user.graduation);
		
		if (user.school !== ''){
			$('#school').show();
			$('#school').val(user.school);
		}
      $('#optpublic').attr('checked', user.optpublic);
      $('#optemail').attr('checked', user.optemail);

		// parse user affiliation
		$('#tags-affiliations').html(function(){
			var _html = '';
			if (user.affiliations.length !==0) {
				$.each(user.affiliations, function (index) {
				  _html += '<li class="affiliation" data-id="'+ user.affiliations[index].affiliationid +'">'
				  			+ '<span>'+ user.affiliations[index].organization.organizationname +'</span>'
				  			+ '<a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item affiliation" alt="remove item" /></a>'
				  		   + '</li>';
				});				
			}
			if (_html == '')
				_html = '<li>none</li>';

			return _html;
		});

		// parse user interests
		$('#tags-interest').html(function(){
			var _html = '';
			if (user.userinterests.length !==0) {
				$.each(user.userinterests[0], function (key, value, list) {
					if(value === true){
						_html += '<li class="interest" data-id="'+ key +'">'
				  			+ '<span>'+ self.returnEntityName(key) +'</span>'
				  			+ '<a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item affiliation" alt="remove item" /></a>'
				  		   + '</li>';
				  		self.interests.push(key);
					}
				  
				});				
			}
			if (_html == '')
				_html = '<li>none</li>';

			return _html;
		});

		// parse user interests
		$('#filtered-tags-specialty').html(function(){
			var _html = '';
			if (user.userspecialties.length !==0) {
				$.each(user.userspecialties[0], function (key, value, list) {
					if(value === true){
						_html += '<li class="specialty" data-id="'+ key +'">'
				  			+ '<span>'+ self.returnEntityName(key) +'</span>'
				  			+ '<a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item affiliation" alt="remove item" /></a>'
				  		   + '</li>';
				  		self.specialty.push(key);
					}
				  
				});				
			}
			if (_html == '')
				_html = '<li>none</li>';

			return _html;
		});

		// parse jobs
		$('#tags-jobs').html(function(){
			var _html = '';			
			if (user.userjobs.length !==0) {
	         $.each(user.userjobs, function (index) {
					_html += '<li class="job" data-id="'+ user.userjobs[index].userjobid +'">'
							+ '<span>'+ user.userjobs[index].title +' at '+ user.userjobs[index].company +' '+ user.userjobs[index].year +'</span>'
		    				+ '<a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item job" alt="remove item" /></a>'
		    				+ '</li>';
	          });
			}
			if (_html == '')
				_html = '<li>none</li>';

			return _html;
		});

	    // add iCheck plugin 
		$('.update-profile-container input').iCheck({
		    checkboxClass: 'icheckbox_minimal-orange',
		    radioClass: 'iradio_minimal-orange'
		});
	}

	self.removeItem = function(item)
	{
		var self = this;
		var item = $(item).parent().parent();
		var id = item.attr('data-id');

		// check item
		if ($(item).hasClass('job')) {
			// remove job from db
			$.ajax({
	      	url: "/api/removeUserJob/" + id,
	         	format: JSON
	         }).done(function (data) {
	     			// clear dom
					item.remove();
	    });

		} else if ($(item).hasClass('affiliation')) {
			// remove affiliation from db
			$.ajax({
	      	url: "/api/removeAffiliation/" + id,
	      	format: JSON
	      	}).done(function (data) {
	     			// clear dom
					item.remove();
	   	});
		} else if ($(item).hasClass('interest')){
			var keyword = $(item).attr('data-id');
			//remove from array
			self.interests = _.without(self.interests, keyword);
			//save
		} else if ($(item).hasClass('specialty')){
			var keyword = $(item).attr('data-id');
			self.specialty = _.without(self.specialty, keyword);
		}
	}

	self.updateNewOrganization = function()
	{
		var self = this;
		$('#organization').val(self.organizationStr);
	}

	self.addItem = function(item)
	{
		var self = this;

		if ($(item).hasClass('job')) {
			// add job
			var jobcompany = $('input[name=jobcompany]');
			var jobtitle = $('input[name=jobtitle]');
			var jobyear = $('#jobyear');
			
			if (jobcompany.val().length > 1 && jobtitle.val().length > 1) {
        // save job to db
        $.ajax({
            url: '/api/saveJob/'+ userid +'?title='+ jobtitle.val() + '&company='+ jobcompany.val() +'&year='+ jobyear.val(),                
            format: JSON
        }).done(function (data) {					
					// populate job					
					var id = data[0].userjobid;
					// update dom
					var str = '<li class="job" data-id="'+ id +'">'
			    				+' <span>'+ jobtitle.val() +' at '+ jobcompany.val() +' '+ jobyear.val() +'</span>'
			    				+' <a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item job" alt="" /></a>'
			    			+'</li>';

		    	// add item to list
		    	$('#tags-jobs').append(str);
		    	// clear form
		    	jobcompany.val('');
		    	jobtitle.val('');
      	});				
			}

		} else if ($(item).hasClass('affiliation')) {
			// add affiliation			
			if (self.organizationStr.length > 1) {			
				// update db
				var url = "/api/saveAffiliation/";		
				$.ajax({
				    url: url + userid +"?organizationid=" + self.organizationid,
				    format: JSON
				}).done(function (data) {
					// update dom
					var str = '<li class="affiliation" data-id="'+ self.organizationid +'">'
			    				+'<span>'+ self.organizationStr +'</span>'
			    				+'<a href="#"><img src="contents/imgs/profiles/btn-remove.png" class="remove-item affiliation" alt="" /></a>'
			    			+'</li>';
			    // add item to list
			    $('#tags-affiliations').append(str);
			    // clear form
			    self.organizationid = 0;
			    self.organizationStr = '';
			    $('#organization').val('');
				});
			}			
		}
	}

	self.bindSchoolAutoComplete = function(item)
	{
  	var school 		= $('#school');
  	var schoolwrap  = $('#school-wrap');
    var schoolnames = $('#schoolnames');

   // TO-DO:  replace with hybrid combo box -

		schoolnames.html('');
		school.val('');

  	// show school input field
		school.show();
    // set autocomplete 
	   if ($(item).val() !== 'Vet') {
	     	// destroy selectize 
	     	$.fn.exists = function(){return this.length>0;}

			if ($('#school-selectize').exists()) {
			    self.schoolSelectize[0].selectize.destroy();
			}  	

	     	//destroy the select - eh we should hide and check if exists to avoid api call
	     	$('#school-selectize').remove();
	     	schoolwrap.html('<input name="school" id="school" class="form-control" placeholder="Type or choose a school">').show();
	    }else {       		    	
	    	//implement selectize for AVMA schools...cause its sexy
	    	//first get school options
	     	$.ajax({
	     		url: '/api/schools',
	     		format: JSON
	     	}).done(function(data){
	     		//setup select
	     		_schoolSelectHtml = '<select style="width: 100%;" id="school-selectize" name="school"><option value="">Type or choose a school</option>';
	     		_.each(data, function(value, key, list){
	     			_schoolSelectHtml += '<option value="' + value.schoolname + '">' + value.schoolname + '</option>';
	     		});
	     		_schoolSelectHtml += '</select>';

	      		var strUser;
          		$( "#school-selectize" ).combobox();
          		$('#school-selectize').on('change', function() {
          			var e = document.getElementById("school-selectize");
				 	strUser = e.options[e.selectedIndex].text; 
  					school.val(strUser);
				 });
	     		
	     		//now insert into HTML and bind selectize
	     		// schoolwrap.html('').hide().after(_schoolSelectHtml);
	     		// self.schoolSelectize = $('#school-selectize').selectize({
	     		// 	highlight: false,
	     		// 	maxOptions: 99999,
	     		// 	maxItems: 1
	     		// });

	     		// self.schoolSelectize[0].selectize.on('dropdown_open', function(){
	     		// 	//remove options
	     		// 	self.schoolSelectize[0].selectize.clear();
	     		// });

	     	});
	    }
	}

	self.setUpdateValidation = function()
	{
		var self = this;
		var update_form = $('#update-form');

		// validate registration form
		update_form.validate({
		  	rules: {	    
		    	email: {
		    		required: true,
		      	email: true
				},
		    	email_confirm: {
		    		required: true,
	     			equalTo: "#email"    				
				},
				password: {
				    minlength: 8,
				    maxlength: 16,
				    alphanumeric: true
				},
    			password_confirm: {
    				required: true,
      			equalTo: "#password"
    			},
    			firstname: {
    				required: true,
    				minlength: 2
    			},
    			lastname: {
    				required: true,
    				minlength: 2
    			},
    			zip: "required",
    			title: "required",
    			profession: "required",
    			school: {
    				required: true,
    				minlength: 2
    			},
    			graduation: {
    			    required: true
    			   // {
    			     //   depends: function(element) {
    			     //       return $("#graduation").val('');
    			     //   }
    			},
    			securityquestion: {
    			    required: true
    			    //{
    			    //    depends: function (element) {
    			    //        return $("#securityquestion").val('');
    			    //    }
    			    //}
    			},
    			securityanswer: "required"
                    
		  	},
		  	invalidHandler: function(event, validator) {    			
		  	    //alert('something is invalid');
		  	    ///console.log('something is invalid');
    			var errors = validator.numberOfInvalids();    			
    			if (errors > 0)
    				$('.required-msg')
    					.html('Please check the highlighted fields below')
    					.addClass('has-error');
		  	},
		  	submitHandler: function (form) {
		  		var formz = $(form).serialize();
		  		///console.log(formz);
		  	    form.submit();
		  	}
		});

		$.validator.addMethod('alphanumeric', function (value, element) {
		    return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
		}, 'Password must contain at least one numeric and one alphabetic character.');
	}

	self.bindEvents = function()
	{	
		var self = this;

		// bind UserProfile elements
		if (self.pageId.hasClass('profile-container')){

			$('.btn-update-profile').click(function(){
				window.location.href = 'UpdateProfile';	
			});

		} else { // bind UpdateProfile elements

			// cache elms
			var doc = $(document);

			// bind upload btn
			$('.upload-photo').click(function(e){
				e.preventDefault();
				///console.log(this);
				$('#fileupload').click();
			});

			// bind radio btns to auto complete
			$('.update-profile-container .radiobtn').on('ifChecked', function(event){			
				self.bindSchoolAutoComplete(this);
			});

			$('.update-profile-container #otherschool').on('ifChecked', function(event){			
				$(this).parent().addClass('checked')
				$(this).addClass('checked')
			});

			// remove tag from list
			$(doc).on('click', '.removetag', function(e){
				e.preventDefault();					
				//to-do filter list
				// remove item from list
				$(this).parent().remove();
			});

			// btn Remove Affiliations/Jobs
			$(doc).on('click', '.remove-item', function(e){
				e.preventDefault();
				self.removeItem(this);			
			});

			// btn Add Affiliations/Jobs
			$('.btn-add').click(function(e){
				e.preventDefault();			
				self.addItem(this);	
			});				

			// bind form validation
			self.setUpdateValidation();

			$('.btn-cancel-update-profile').click(function(){
				window.location.href = 'UserProfile';
			});
		}		
	}

	return self;
}());

$(document).ready(function(){
	VETAPP.pages.profiles.init();	
});
