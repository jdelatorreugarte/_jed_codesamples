// add page to namespace
VETAPP.namespace('VETAPP.pages.members');

// create page
VETAPP.pages.members = (function() {

	var self = {};

	self.init = function init() { ///console.log('init page: members.js');

		var self = this;
		
		self.isMobile 		= window.orientation || Modernizr.touch;
		self.container 	= $('.main-container');
		self.pageId			= $('#pageId');
		self.schoolSelectize = '';
		// init Modules
		self.initModules();
		// Bind events
		self.bindEvents();
	}

	self.initModules = function()
	{
		var self = this;

		// override jquery validate plugin defaults for bootstrap error class
		$.validator.setDefaults({
		    highlight: function(element) {
		        $(element).closest('.form-group').addClass('has-error');
		    },
		    unhighlight: function(element) {
		        $(element).closest('.form-group').removeClass('has-error');
		    }
		});		
	}

	self.setUpRegistrationForm = function(){
    	// cache elms
    	var school 			= $('#school');
    	var schoolnames 	= $('#schoolnames');
    	var register_form = $('#register-form');
    	var fieldData = $('#school').val();
    	var doesExist = false;

     	// clear school vals
		$('.members-container .radiobtn').on('ifChecked', function(event){
			schoolnames.html('');
			school.val('');

	      	// show school input field
			school.show();
	        // set selectize if its real
	        if ($(this).val() !== 'Vet') {
	         	// destroy selectize   	
	         	self.schoolSelectize[0].selectize.destroy();

	         	//destroy the select - eh we should hide and check if exists to avoid api call
	         	$('#school-selectize').remove();
	         	school.show();
	        }else {       		    	
		    	//implement selectize for AVMA schools...cause its sexy
	        	//first get school options
	         	$.ajax({
	         		url: '/api/schools',
	         		format: JSON
	         	}).done(function(data){
	         		//setup select
	         		_schoolSelectHtml = '<select style="width: 100%;" id="school-selectize" name="school"><option value="">Type or choose a school</option>';
	         		_.each(data, function(value, key, list){
	         		    //_schoolSelectHtml += '<option value="' + value.schoolid + '">' + value.schoolname + '</option>';
	         		    _schoolSelectHtml += '<option value="' + value.schoolname + '">' + value.schoolname + '</option>';
	         		});
	         		_schoolSelectHtml += '</select>';

	         		//now insert into HTML and bind selectize
	         		school.hide().after(_schoolSelectHtml);
	         		self.schoolSelectize = $('#school-selectize').selectize({
	         			highlight: false,
	         			maxOptions: 7,
	         			maxItems: 1
	         		});

	         		self.schoolSelectize[0].selectize.on('dropdown_open', function(){
	         			//remove options
	         			self.schoolSelectize[0].selectize.clear();
	         		});

	         	});
	        }
		});

		// get career types
		$.ajax({
		   url: "/api/careertypes",
		   // data: { key: val },
		   format: JSON
		}).done(function (data) {
		   var _careerhtml = '<option value="">--Select one--</option>';
		   $.each(data, function (_careerkey) {
		       _careerhtml += '<option value="' + data[_careerkey].careertypeid + '">';
		       _careerhtml += data[_careerkey].careertypename;
		       _careerhtml += '</option>';
		   });
		   $('#careertypeid').html(_careerhtml);
		});

		// get security questions
		$.ajax({
		   url: "/api/securityquestions",
		   // data: { key: val },
		   format: JSON
		}).done(function (data) {
		   ///console.log(data);

		   var _securityhtml = '<option value="">Select Security Question</option>';
		   $.each(data, function (_securitykey) {
		       _securityhtml += '<option value="' + data[_securitykey].securityquestionname + '">';
		       _securityhtml += data[_securitykey].securityquestionname;
		       _securityhtml += '</option>';
		   });
		   $('#securityquestion').html(_securityhtml);
		});

		// validate registration form
		register_form.validate({
		  	rules: {	    
		    	email: {
		    		required: true,
		      	email: true
				},
		    	email_confirm: {
		    		required: true,
	     			equalTo: "#email"    				
				},
				password: {
    				required: true,
    				minlength: 8,
    				maxlength: 16,
    				customv: true
				},
    			password_confirm: {
    				required: true,
      			equalTo: "#password"
    			},
    			firstname: {
    				required: true,
    				minlength: 2,
    			},
    			lastname: {
    				required: true,
    				minlength: 2
    			},
    			careertypeid: "required",
    			school: {
    				required: true,
    				minlength: 2
    			},
    			optterms: {
    				required: true,
    			},
    			graduation: {
    				required: true,    				
    			},
    			securityquestion: {
    				required: true,    				
    			},
    			securityanswer: 
    			{
    				required: true,
    				minlength: 2,
    			}
		  	},
		  	messages: {
    			email: {
    				required: 'Please enter your email address.',
    				email: 'Invalid email address. Please enter your email address in the format: example@email.com.'
    			},
    			email_confirm: {
    				required: 'Please confirm your email address.',
    				email: 'Invalid email address. Please enter your email address in the format: example@email.com.',
    				equalTo: 'You email address does not match. Please enter your email address.'
    			},    			
    			password: {
    				required: 'Please enter a password.',
    				minlength: 'Your password must be at least 8-16 characters including at least 3 of the following characters: number, non-alphanumeric, lower case character, uppercase character.',
    				maxlength: 'Your password must be at least 8-16 characters including at least 3 of the following characters: number, non-alphanumeric, lower case character, uppercase character.'
    			},
    			password_confirm: {
    				required: 'Please confirm your password.',
    				equalTo: 'Your password does not match. Please enter your password.'
    			},    			
    			firstname: {
    				required: 'Please enter your first name.'
    			},
    			lastname: {
    				required: 'Please enter your last name.'
    			},
    			careertypeid: {
    				required: 'Please tell us where you are in your career.'
    			},
    			school: {
    				required: 'Please tell us where you go, or went, to school.'
    			},
    			graduation: {
    				required: 'Please select a graduation date.'
    			},
    			securityquestion: {
    				required: 'Please select a security question.'
    			},
    			securityanswer: {
    				required: 'Please provide an answer to the security question.'
    			},
    			optterms: {
    				required: ''
    			}
    		},
		  	invalidHandler: function(event, validator) { 
		  	    alert('invalidHandler');
		  	    alert("school=" + $('input[name=school]').val());
		  	    alert("school=" + $('school-selectize').val());
		  	    alert("school=" + $('input[name=optterms]').val());
		  	    alert("email=" + $('input[name=email]').val());

                

		  	    alert( "selected=" + $("#school-selectize option:selected").val());


		  	    alert("selecte2=" + $("#school-selectize option:selected").text());

		  	    $('input[name=school]').val($("#school-selectize option:selected").val());

		  	    alert("after school=" + $('input[name=school]').val());



		  	    if ($('input[name=optterms]').val() !== '1') {
		  			$('.touerror').show();
		  			$('#optterms').closest('.icheckbox_minimal-orange').addClass('error');

		  		} else {
		  			$('.touerror').hide();
		  		}

    			var errors = validator.numberOfInvalids();    			
    			if (errors > 0)
    				$('.required-msg')
    					.html('Please check the highlighted fields below.')
    					.addClass('has-error');
    		},
		  	submitHandler: function (form) {
		  	    alert('submit');
    			$(form).attr('action', '/Process/Register?redirect=/registersuccess');    			
    			form.submit();
  			}    		
		});
		// add alphanumeric
		$.validator.addMethod('alphanumeric', function (value, element) {
		    return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
		}, 'Password must contain at least one numeric and one alphabetic character.');

		$.validator.addMethod('capital', function(value, element){
			return this.optional(element) || value.match(/[A-Z]/);
		}, 'Password must contain at least one capital letter.');

		$.validator.addMethod('customv', function(value, element){
			//var charCount = 0;

			////check for lowercase
			//if(value.match(/[a-z]/)){
			//	charCount++;
			//}
			////check for uppercase
			//if(value.match(/[A-Z]/)){
			//	charcount++;
			//}

			////check for numbers
			//if(value.match(/[0-9]/)){
			//	charCount++;
			//}

			////check for nonalphanumeric
			//if(value.match(/^[a-z0-9]+$/i)){
			//	charCount++;
			//}

			//if(charCount >= 3){
			//	return true;
			//}else{
			//	return false;
			//}
			
		    //return /^[A-Za-z0-9\d=!\-@._*]*$/.test(value) // consists of only these
            //&& /[a-z]/.test(value) // has a lowercase letter
		    //&& /\d/.test(value) // has a digit
		    return /(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$)|(^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).+$)|(^(?=.*[a-z])(?=.*[A-Z])(?=.*(_|[^\w])).+$)|(^(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$)|(^(?=.*[a-z])(?=.*\d)(?=.*(_|[^\w])).+$)/.test(value);

			//return this.optional(element) || value.match(/[A-Z]/);
		}, 'Your password must be at least 8-16 characters including at least 3 of the following characters: number, non-alphanumeric, lower case character, uppercase character.');
	}
		
	self.setUpSignInForm = function()
	{
		var form = $('#signin-form');

		// validate registration form
		form.validate({
		  	rules: {	    
		    	email: {
		    		required: true,
		      	email: true
				},
				password: {
    				required: true
				},
		  	},
		  	invalidHandler: function (event, validator) {
		  	    alert('invalidHandler 2');
				var errors = validator.numberOfInvalids();    			
    			if (errors > 0)
    				$('.required-msg')
    					.html('Please check the highlighted fields below.')
    					.addClass('has-error');
    		}
		});		
	}

	self.setUpForgotForm = function()
	{
		var form = $('#forgot-form');

		// validate registration form
		form.validate({
		  	rules: {	    
		    	email: {
		    		required: true,
		      	email: true
				},
				lastname: {
    				required: true
				},
		  	},
		  	invalidHandler: function(event, validator) {    			
				var errors = validator.numberOfInvalids();    			
    			if (errors > 0)
    				$('.required-msg')
    					.html('Please check the highlighted fields below.')
    					.addClass('has-error');
    		}
		});		
	}

	self.bindEvents = function()
	{	
		var self = this;
        
		if (self.pageId.hasClass('register-form')){
			self.setUpRegistrationForm();
			// add icheck custom events
			$('#optterms').on('ifChecked', function(event){
				$('#optterms').closest('.icheckbox_minimal-orange').removeClass('error');
				$('input[name=optterms]').val('on');
			});
			// add icheck uncheck
			$('#optterms').on('ifUnchecked', function(event){
				$('input[name=optterms]').val('');
			});

		} else if (self.pageId.hasClass('signin-form')){
			self.setUpSignInForm();
		} else if (self.pageId.hasClass('forgot-form')){
			self.setUpForgotForm();
		}

		// cancel btn
		$('.btn-cancel').click(function(){
			window.location.href = '/';
		});

		// add iCheck plugin 
		$('.members-container .icheck').iCheck({
			checkboxClass: 'icheckbox_minimal-orange',
			radioClass: 'iradio_minimal-orange'
		});	
	}

	return self;
}());

$(document).ready(function(){
	VETAPP.pages.members.init();	
});
