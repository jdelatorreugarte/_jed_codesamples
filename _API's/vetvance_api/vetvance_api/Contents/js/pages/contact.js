    // add page to namespace
VETAPP.namespace('VETAPP.pages.contact');

// create page
VETAPP.pages.contact = (function() {

    var self = {};

    self.init = function init() { ///console.log('init page: contact.js');

        var self = this;
        
        self.isMobile = window.orientation || Modernizr.touch;
        self.container = $('.main-container');

        // init Modules
        self.initModules();
        // Bind events
        self.bindEvents();
    }


    function validateEmail(email) {
        var re = /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
        return re.test(email);
    }

    // Sub Functions
    function populateFeedbackTopics() {
        $.ajax({
            url: "/api/Topics/feedback",
            // data: { key: val },
            contentType: JSON
        }).done(function (data) {
            ///console.log(data);

            var _html = '<option value="">Please select</option>';
            $.each(data, function (_key) {
                _html += '<option value="' + data[_key].topicname + '">';
                _html += data[_key].topicname;
                _html += '</option>';
            });

            $('#feedback-topic-select').html(_html);
        });
    }

    function populateSupportTopics() {
        $.ajax({
            url: "/api/Topics/support",
            // data: { key: val },
            contentType: JSON
        }).done(function (data) {
            ///console.log(data);

            var _html = '<option value="">Please select</option>';
            $.each(data, function (_key) {
                _html += '<option value="' + data[_key].topicname + '">';
                _html += data[_key].topicname;
                _html += '</option>';
            });

            $('#support-topic-select').html(_html);
        });
    }

    self.initModules = function()
    {
        var self = this;

       // populate options
        populateFeedbackTopics();
        populateSupportTopics();
    }

    self.bindEvents = function()
    {   
                // submit feedback
        $('#submitfeedback').click(function () {

            //remove all error classes because we're optimistic and stuff
            $('#feedback .form-group').removeClass('has-error').find('label.error').hide();

            var topic = $('#feedback-topic-select').val(); // this will never be null for now
            var name = $('#feedbackname').val();
            var email = $('#feedbackemail').val();
            var message = $('#feedbackmessage').val();

            TracktSimple.track("ContactUs","Feedback", topic);

            if(topic != '' && name != '' && validateEmail(email) !== false && message != ''){

                //alert('Feedback sent');
                $.ajax({
                    url: "/api/Contact/",
                    data: { type: "feedback", topic: topic, name: name, email: email, message: message },
                    contentType: JSON
                }).done(function (data) {
                    ///console.log(data);

                    location.href = '/contactsuccess';
                });
            }else{
                //fire error text
                if (topic == '') {
                    $('.fb-topic-container').addClass('has-error').find('#feedbacktopicselecterror').show();
                }

                if(name == ''){
                    $('.fb-name-container').addClass('has-error').find('#feedbacknameerror').show();
                }
                if (email !== '' & typeof email !== undefined) {
                    if (validateEmail(email) !== true) {
                        $('.fb-email-container').addClass('has-error').find('#feedbackemailinvalid').show();
                    } else {
                        //do 'nuffin
                    }
                } else {
                    $('.fb-email-container').addClass('has-error').find('#feedbackemailempty').show();
                }
                
                if(message == ''){
                    $('.fb-message-container').addClass('has-error').find('#feedbackmessageerror').show();
                }
            }
        });

        // submit support
        $('#submitsupport').click(function () {
            //remove all error classes because we're optimistic and stuff
            $('#support .form-group').removeClass('has-error').find('label.error').hide();

            var type = $('#support-topic-select').val(); //this will never be null for now
            var name = $('#supportname').val();
            var email = $('#supportemail').val();
            var message = $('#supportmessage').val();

            TracktSimple.track("ContactUs", "Questions4SupportTeam", type);

            if(type != '' && name != '' && validateEmail(email) !== false && message != ''){
                //alert('Support request sent');
 
                $.ajax({
                    url: "/api/Contact/",
                    data: { type: "support", topic: $('#support-topic-select').val(), name: $('#supportname').val(), email: $('#supportemail').val(), message: $('#supportmessage').val() },
                    contentType: JSON
                }).done(function (data) {
                    ///console.log(data);

                    location.href = '/contactsuccess';
                });
            }else{
                //fire error text
                if(type == ''){
                    $('.sup-topic-container').addClass('has-error').find('#supporttopicselecterror').show();
                }

                if(name == ''){
                    $('.sup-name-container').addClass('has-error').find('#supportnameerror').show();
                }
                if (email !== '' & typeof email !== undefined) {
                    if (validateEmail(email) !== true) {
                        $('.sup-email-container').addClass('has-error').find('#supportemailinvalid').show();
                    } else {
                        //nothin
                    }
                } else {
                    $('.sup-email-container').addClass('has-error').find('#supportemailempty').show();
                }
                    
                if(message == ''){
                    $('.sup-message-container').addClass('has-error');
                }
            }
        });


        //tabs for connect page
        $('.module-tab-mobile-cont').click(function(){
            var $tab = $(this);
            if($tab.hasClass('m-tab-active')){
                //do nothing
            }else{
                //add active tab to this and remove from the other
                $('.module-tabs-mobile > div').removeClass('m-tab-active');
                $tab.addClass('m-tab-active');

                if($tab.hasClass('m-feedback-tab')){
                    //now show proper bottom panel by hiding all then showing proper
                    $('.column').hide();
                    $('.feedback').fadeIn();
                }else{
                    $('.column').hide();
                    $('.support').fadeIn();
                }
                
            }
        });

        var self = this;
    }

    return self;

}());

$(document).ready(function () {
    VETAPP.pages.contact.init();
});


        

