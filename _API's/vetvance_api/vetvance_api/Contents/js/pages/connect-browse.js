// add page to namespace
VETAPP.namespace('VETAPP.pages.connectbrowse');

// create page
VETAPP.pages.connectbrowse = (function() {

	var self = {};

	self.init = function init() { ///console.log('init page: connect-browse.js');

		var self = this;
		
		self.isMobile 		= window.orientation || Modernizr.touch;
		self.container 	= $('.main-container');
		self.pageId			= $('#pageId');

		// bind page events
		if (self.pageId.hasClass('connect-browse-container')){
			// init Modules
			self.initModules();
			// get data			
			self.getConnectUserData('near', '#list-nearme');
			self.getConnectUserData('school', '#list-fromschool');
			self.getConnectUserData('gradute', '#list-graduating');
			//self.getConnectUserData('1?sort=zip', '#list-interestpractice');
			//self.getConnectUserData('1?sort=active', '#list-joinedaffiliation');

			// bind events
			self.bindEvents();
		}			
	}

	self.initModules = function()
	{
		var self = this;
	}

	self.getConnectUserData = function(sort, elm)
	{
		var self = this;
		//var url = 'http://vetvance-api.dev.thebloc.com';
	    var url = 'https://www.vetvance.com';
	    //var userid=1; // tmp
		var api = '/api/ConnectBrowse/'+ userid;

		// get data
		$.ajax({			
			url: url + api,
			format: JSON
		}).done(function (data) {
			/////console.log(data);
			switch (sort) {

				case 'near':
				self.parseUserData(data[0]['near'], elm);
				break;

				case 'school':
				self.parseUserData(data[0]['school'], elm);
				break;	

				case 'gradute':
				self.parseUserData(data[0]['gradute'], elm);
				break;
			}
		});
	}

	self.parseUserData = function(data, elm)
	{		
		var self = this;

		// Load template
		var source = $("#dataitem-template").html();

		// Compile Handlebars template
		var template = Handlebars.compile(source);

		// Render html
		var html = template(data);

		// add data to dom
		$(elm).append(html);

		// calc width
		var width = $(elm +' li').width() * $(elm).find('li').length;
		// set list width
		$(elm).css('width', width);

		// add carousel
		self.addCarousel(elm);
	}

	self.addCarousel = function(elm)
	{
		var self = this;

		switch (elm) {

			case '#list-nearme':
				$('.jcarousel-nearme').jcarousel();
				carouselHandlers('.jcarousel-nearme');
				break;

			case '#list-fromschool':
				$('.jcarousel-fromschool').jcarousel();
				carouselHandlers('.jcarousel-fromschool');
				break;

			case '#list-graduating':
				$('.jcarousel-graduating').jcarousel();
				carouselHandlers('.jcarousel-graduating');
				break;

			case '#list-interestpractice':
				$('.jcarousel-interestpractice').jcarousel();
				carouselHandlers('.jcarousel-interestpractice');
				break;

			case '#list-joinedaffiliation':
				$('.jcarousel-joinedaffiliation').jcarousel();
				carouselHandlers('.jcarousel-joinedaffiliation');
				break;
		}

		// events handlers
		function carouselHandlers(elm) {

			$(elm).parent().find('.control-next')
			   .on('jcarouselcontrol:active', function() {
			       $(this).removeClass('inactive');
			   })
			   .on('jcarouselcontrol:inactive', function() {
			       $(this).addClass('inactive');
			   })
			   .jcarouselControl({
			       target: '+=1'
			   });

			$(elm).parent().find('.control-prev')
			   .on('jcarouselcontrol:active', function() {
			       $(this).removeClass('inactive');
			   })
			   .on('jcarouselcontrol:inactive', function() {
			       $(this).addClass('inactive');
			   })
			   .jcarouselControl({
			       target: '-=1'
			   });			   
		}
	}

	self.bindEvents = function()
	{	
		var self = this;
		// cache elms
		var doc = $(document);

		//
		$(doc).on('click', '.btnShowMe', function(e){
			e.preventDefault();
			window.location.href = 'connectbrowse.html';
		});

        // browse all
		$(doc).on('click', '.btn-show-all', function (e) {
		    e.preventDefault();
		    // show only connect check items
		    window.location.href = '/connect';
		});
	}

	return self;
}());

$(document).ready(function(){
	VETAPP.pages.connectbrowse.init();	
});