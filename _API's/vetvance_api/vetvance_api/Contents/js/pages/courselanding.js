// add page to namespace
VETAPP.namespace('VETAPP.pages.courselanding');

// create page
VETAPP.pages.courselanding = (function() {

	var self = {};

	self.init = function init() { ///console.log('init page: courselanding.js');

		var self = this;

		self.isMobile = isMobile();
		self.container = $('.main-container');

		//if we don't do this, there will be blood - promo var isnt set on any other page than index and breaks js loading
		if(self.isMobile !== true){
			// init Modules
			self.initModules();
			// Bind events
			self.bindEvents();
		}else{
			self.setUpMobile();
		}
		

		function isMobile(){
			//check browser size, anything below 768 we consider 'mobile'
			if($(window).width() <= 768){
				return true;
			}else{
				return false;
			}
		}
	}

	self.setUpMobile = function(){
		//get course data, but also remember we need to change the squiggly file color and classes of all static elements
		//to match the course
		var user_id = window.userid || 0;
	    var course_id = window.model || 1;
		
		//setup query to course list
		var endpoint = "/API/CourseModules/" + course_id + "?userid=" + user_id;
		$.ajax({
            url: endpoint,
            format: JSON
        }).done(function (data) {
            ///console.log(data);
            //data = $.parseJSON(data);
            _html = "";

            //generate course type up here and put into a var
             //setup styles for the page
             var courseType = {};
                    switch (data[0].course[0].coursetypeid) {
                        case 1:
                            courseType = { squigglyFileName: "Career1.png", classs: "bi", bigicon: data[0]['course'][0]['courseid'] + ".png" };
                            break;
                        case 2:
                            courseType = { squigglyFileName: "Professional1.png", classs: "pd", bigicon: data[0]['course'][0]['courseid'] + ".png" };
                            break;
                        case 3:
                            courseType= { squigglyFileName: "Financial1.png", classs: "fb", bigicon: data[0]['course'][0]['courseid'] + ".png" };
                            break;
                    }

			//add style to title, module header and border
			$('.course-title-mobile').addClass(courseType.classs);
			$('.course-module-title').addClass(courseType.classs);

            //prepare data to be inserted into the page
            _.each(data, function(value, key, list){
            	var moduleNum = parseInt(key) + 1;
            	_html += '<a class="nostyle" href="/module/' + value.module.moduleid + '"><li class="module-mobile">';
		        _html += '<p class="' + courseType.classs + ' module-label-mobile uppercase">Module ' + moduleNum + ':';
		        if(value.modulecomplete === true)
		        	_html += '<img src="/Contents/imgs/mobile/module-completed.png" />';
		       
		        _html += '</p><p class="' + courseType.classs + 'module-title-mobile">' + value.module.modulename + '<img class="pull-right" src="/Contents/imgs/mobile/' + courseType.classs + '-arrow-r.png" /></p>';
		        _html += '<p class="module-presenter-mobile">Presented by ' + value.module.presenter.presenterfirstname +  ' ' + value.module.presenter.presenterlastname + '</p>';
		        _html += '</li></a>';

            })

            $('.module-list-mobile').html(_html);

            //replace on page
            //courses[0].course[0].courselongdescription, coursename
            $('.course-title-mobile').html(data[0].course[0].coursename);
            $('.course-description').html(data[0].course[0].courselongdescription);
        });


	}

	//this entire function will be rewritten in angular
	self.initPopovers = function(presenters){
		//convert dom elements to jquery objects
		presenters = $(presenters);

		//TODO: Set up handler for managing different popover placements based on screen loc

		function getContent(presenter){
			//this html packaging depends on the api call...this should reside in the angular. Using static data now
			var html = '<div class="popover-header"><img class="circle" src="http://placehold.it/50x50"><div class="header-text"><h4 class="purple">Mitch Blanding,<small> DVM, MS</small></h4><h5 class="muted">Associate Director, Beef Technical Services</h5></div></div><div class="clearfix"></div><div class="popover-body">This is where the body text will go, my friends.</div>'
			return html;
		}

		//set options
		var options = {
			animation: false,
			html: true,
			placement: 'right',
			trigger: 'hover',
			content: getContent()
		}

		//initialize popovers for all presenters
		$('[rel="popover"]').popover(options);


	}

	self.initAngular = function () {
	    var courselanding = angular.module('courselanding', []);

	    courselanding.controller('courseLandingCtrl', function ($scope, $http) {
	        //call api
	        var user_id = window.userid || 20;
	        var course_id = window.model || 1;
	        if((typeof user_id !== 'undefined' || user_id != '') && (typeof course_id !== 'undefined' || course_id != '')){
	            var endpoint = "/API/CourseModules/" + course_id + "?userid=" + user_id;
	            $http({method: 'GET', cache: true, url: endpoint}).
                success(function (data, status, headers, config) {
                    ///console.log(data);
                    $scope.courses = data;

                    //setup styles for the page
                    switch (data[0].course[0].coursetypeid) {
                        case 1:
                            $scope.styles = { squigglyFileName: 'Career1.png', classs: 'bi', bigicon: data[0].course[0].courseid + '.png' };
                            break;
                        case 2:
                            $scope.styles = { squigglyFileName: 'Professional1.png', classs: 'pd', bigicon: data[0].course[0].courseid + '.png' };
                            break;
                        case 3:
                            $scope.styles = { squigglyFileName: 'Financial1.png', classs: 'fb', bigicon: data[0].course[0].courseid + '.png' };
                            break;
                    }


                }).error(function(data, status, headers, config){
                    ///console.log(data);
                });
	        }


	    });

	    courselanding.directive('popover', function(){
	        return{
	            restrict: 'A',
	            scope:{
	                content: '='
	            },
	            link: function(scope, elem, attr){
	                var options = {
	                    animation: false,
	                    html: true,
	                    placement: 'right',
	                    trigger: 'hover',
	                    content: generatePopoverContents(scope.content)
	                }
	                $(elem).popover(options);
	                $(elem).text( scope.content.presenterfirstname + ' ' + scope.content.presenterlastname);

	            }
	        }

	        function generatePopoverContents(presenter){
	            //	            var html = '<div class="popover-header"><img class="img-circle" src="http://placehold.it/50x50"><div class="header-text"><h4 class="purple">' + presenter.presenterfirstname + ' ' + presenter.presenterlastname + ', <small>' + presenter.presentercertification + '</small></h4><h5 class="muted">' + presenter.presentertitle + '</h5></div></div><div class="clearfix"></div><div class="popover-body">' + presenter.presenterdescription + '</div>';
	            var html = '<div class="popover-header"><img width="50" height="50" class="img-circle" src="/contents/imgs/whoweare/' + presenter.presenterthumbnail+ '"><div class="header-text"><h4 class="purple">' + presenter.presenterfirstname + ' ' + presenter.presenterlastname + ', <small>' + presenter.presentercertification + '</small></h4><h5 class="muted">' + presenter.presentertitle + '</h5></div></div><div class="clearfix"></div><div class="popover-body">' + presenter.presenterdescription + '</div>';
	            return html;
	        }
	    });

	    angular.bootstrap(document, ['courselanding']);
	    }

	self.initModules = function()
	{
		var self = this;
		var presenters = self.container.find('.presenter-popover');

		self.initAngular();
	}

	self.bindEvents = function()
	{
		var self = this;
	}

	return self;

}());

angular.element(document).ready(function () {
    VETAPP.pages.courselanding.init();
});