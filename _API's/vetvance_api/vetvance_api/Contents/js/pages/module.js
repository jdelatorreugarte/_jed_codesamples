// add page to namespace
VETAPP.namespace('VETAPP.pages.vmodule');

// create page
VETAPP.pages.vmodule = (function () {

    var self = {};

    self.init = function init() {
        //console.log('init page: vmodule.js');

        var self = this;

        self.isMobile = isMobile();
        self.container = $('.main-container');
        self.mobileContainer = $('#advance-container-mobile');
        self.mobileNavWrap = $('.mobile-header');
        self.courseId = '';
        self.courseClass = '';
        // init Modules
        self.initModules();
        // Bind events
        self.bindEvents();


        function isMobile() {
            //check browser size, anything below 768 we consider 'mobile'
            if ($(window).width() <= 768) {
                return true;
            } else {
                return false;
            }
        }
    }

    self.isMobile = function(){
        var self = this;
         if ($(window).width() <= 768) {
                return true;
            } else {
                return false;
            }
    }

    self.toggleTabs = function(tab, activeClass) {
        //get and cache tab element set current state
        var self = this;

        //turn clicked elem reference into jquery obj
        var tab = $(tab);

        if (tab.hasClass(activeClass)) {
            //do nothing, joint is already active
        } else {
            //add active class to clicked tab, show matching panel, remove active class from sibling, hide current active panel
            tab.addClass(activeClass);
            $(tab.attr('data-content')).show();
            var otherTab = tab.siblings();
            otherTab.removeClass(activeClass);
            $(otherTab.attr('data-content')).hide();
        }
    }

    self.initAngular = function () {

        var vmodule = angular.module('vmodule', []);

        vmodule.controller('moduleCtrl', function ($scope, $http) {
            var user_id = window.userid || 20;
            var module_id = window.model || 1;
            $scope.questionAnswered = false;
            $scope.likeMinded = false;

            $scope.currentPage = 0;
            $scope.pageSize = 4;
            if ((typeof user_id !== 'undefined' || user_id != '') && (typeof module_id !== 'undefined' || module_id != '')) {
                var endpoint = "/api/Module/" + module_id + "?userid=" + user_id
                $http({ method: 'GET', cache: true, url: endpoint }).
                success(function (data, status, headers, config) {
                    //console.log(data);
                    $scope.data = data[0];
                    $scope.resourcesNull = false;
                    //check if resources is null
                    if ($scope.data.module.resources.length == 0) {
                        $scope.resourcesNull = true;

                        //bad form - remove click handler from resources tab
                        $('.tab.resources').unbind('click').css('cursor', 'not-allowed').attr('title', 'No resources available for this module');

                    };

                    //setup styles for the page
                    switch (data[0].coursetypeid[0]) {
                        case 1:
                            $scope.styles = { squigglyFileName: 'Career1.png', classs: 'bi', hex: '#3f7dc8' };
                            break;
                        case 2:
                            $scope.styles = { squigglyFileName: 'Professional1.png', classs: 'pd', hex: ' #00b3d3' };
                            break;
                        case 3:
                            $scope.styles = { squigglyFileName: 'Financial1.png', classs: 'fb', hex: '#e03e52' };
                            break;
                    }

                    //get other modules
                    var moduleEndpoint = "/api/Course/" + $scope.data.module.courseid;
                    $http({ method: 'GET', cache: true, url: moduleEndpoint }).
                    success(function (data, status, headers, config) {
                        $scope.course = data[0];
                        //console.log(data);
                    }).error(function (data, status, headers, config) {
                        //console.log('Pffft, silent but deadly...api call failed.');
                    });



                }).error(function (data, status, headers, config) {
                    //console.log('Pffft, silent but deadly...api call failed.');
                });


            }

            $scope.prevModule = function (currentModuleId) {
                var prevModuleEndpoint = "/api/previousModule/" + currentModuleId;
                $http({ method: 'GET', cache: true, url: prevModuleEndpoint }).
                success(function (data, status, headers, config) {

                    if (data != 0) {
                        var url = "/module/" + data;
                        window.location = (url);
                    } else
                        return false;

                }).error(function (data, status, headers, config) {
                    //console.log('Pffft, silent but deadly...api call failed.');
                });
            }

            $scope.nextModule = function (currentModuleId) {
                TracktSimple.track("Module", "NextModule", "current:" + currentModuleId);
                var nextModuleEndpoint = "/api/nextModule/" + currentModuleId;
                $http({ method: 'GET', cache: true, url: nextModuleEndpoint }).
                success(function (data, status, headers, config) {

                    if (data != 0) {
                        var url = "/module/" + data;
                        window.location = (url);
                    } else
                        return false;

                }).error(function (data, status, headers, config) {
                    //console.log('API call failed.');
                });
            }

            $scope.saveAnswer = function (surveyid, surveyquestionid) {
                var saveAnswerEndpoint = '/api/saveAnswer/' + window.userid + '?surveyid=' + surveyid + '&surveyquestionid=' + surveyquestionid;
                $http({ method: 'GET', url: saveAnswerEndpoint }).
                    success(function (data, status, headers, config) {
                        //console.log(data);

                    }).error(function (data, status, headers, config) {
                        //console.log('Query failed. :-(');
                    });
            }


            $scope.checkAnswer = function (surveyid) {
                if ($scope.selectedAnswerId) {
                    TracktSimple.track("Survey", "SurveyAnswered", 'surveyid :' + surveyid);
                    $scope.surveyError = false;
                    $scope.questionAnswered = true;
                    $scope.surveyResponsesTotal = 0;
                    //var checkAnswerEndpoint = '/api/SurveyResponse/' + window.userid + '?surveyid=' + surveyid;
                    var checkAnswerEndpoint = '/api/savesurveyanswer/' + window.userid + '?surveyid=' + surveyid + '&surveyquestionid=' + $scope.selectedAnswerId;
                    $http({ method: 'GET', url: checkAnswerEndpoint }).
                        success(function (data, status, headers, config) {
                            $scope.surveyResponses = data;

                            //calc survey responses total
                            angular.forEach(data, function (value, key) {
                                $scope.surveyResponsesTotal = $scope.surveyResponsesTotal + parseInt(value.total);
                            });
                            //retrieve like minded folks
                            $scope.getLikeMinded($scope.selectedAnswerId);

                            //save answer
                            //$scope.saveAnswer(surveyid, $scope.selectedAnswerId);

                        }).error(function (data, status, headers, config) {
                            //console.log('Houston, we have a problem');
                        });
                } else {
                    //show error message
                    $scope.surveyError = true;
                }
            }

            $scope.selectAnswer = function (questionid) {
                $scope.selectedAnswerId = questionid;

                //remove selected from all other li
                $('.answer').removeClass('selected');

                //turn all checks off first
                $('.answer-check-img').attr('src', '/contents/imgs/module/module-check-empty.png');

                //ugh, bad form but get the img in this li
                var selector = '#answer-' + questionid;
                var answerLi = $(selector);
                answerLi.addClass('selected');
                answerLi.find('.answer-check-img').attr('src', '/contents/imgs/module/module-check.png');

            }

            $scope.getLikeMinded = function likeMinded(surveyquestionid) {
                var likeMindedEndpoint = '/api/getLikeMinded/' + window.userid + '?surveyquestionid=' + surveyquestionid;
                $http({ method: 'GET', url: likeMindedEndpoint }).
                    success(function (data, status, headers, config) {
                        //apply likeminded to scope
                        $scope.likeMinded = data;

                    }).error(function (data, status, headers, config) {
                        //console.log('EEP OP ORK, this query is broken');
                    });
            }

        });

        vmodule.filter('startFrom', function() {
            return function(input, start) {
                start = +start; //parse to int
                return _.toArray(input).slice(start);
            }
        });

        vmodule.directive('videoplayer', function ($http) {
            var videoElem = '<video id="module-video" poster="/Contents/imgs/module/poster.jpg" class="video-js vjs-default-skin" controls width="460" height="264"> <source type=\'video/mp4\' /></video>';
            //	            var videoElem = '<video id="module-video" data-setup="{\"poster\":\"/Contents/imgs/video/MODULEPLAYBUTTON.jpg\"}" class="video-js" controls preload="auto" width="460" height="264"> <source type=\'video/mp4\' /></video>';
            var module_id = window.model || 0;


            return {
                restrict: 'E',
                scope: {
                    vsrc: '@'
                },
                template: videoElem,
                replace: true,
                link: function (scope, elem, attr) {

                    var durationTrigger = 60;
                    scope.$watch('vsrc', function (newsrc, oldsrc) {
                        if (newsrc) {
                            //id, options, ready callback
                            videojs("module-video", {}, function () {
                                var videoObj = this;

                                //change source to data from api
                                var srcObj = { type: "video/mp4", src: scope.vsrc };
                                this.src(srcObj);

                                // Player (this) is initialized and ready.
                                var duration = this.duration();
                                var half = duration / 2;
                                var completionTriggered = false;
                                var halfTrackFired = false;
                                //assign tracker for first play
                                this.on('firstplay', function(data){
                                    var modTitle = 'Module ID: ' + module_id;
                                    TracktSimple.track("ModuleVideo", "First Play", module_id);
                                });

                                //set up event listener for 'timeupdate' event
                                this.on('timeupdate', function (data) {
                                    //pull time
                                    var vTime = this.currentTime();

                                    //if time >= 90s trigger switch to resources. 
                                    if (vTime >= durationTrigger) {
                                        
                                        //console.log('module video watched > 60 secs.');

                                        if (!completionTriggered) {
                                            //trigger api call to complete module
                                            triggerCompletion();
                                            completionTriggered = true;
                                        }

                                    }

                                    if(vTime >= half ){
                                        if(!halfTrackFired){
                                            TracktSimple.track("ModuleVideo", "Halfway", module_id); 
                                            halfTrackFired = true;
                                        }
                                        
                                    }
                                });

                                this.on('ended', function () {
                                    //trigger completion
                                    //console.log('module video ended');
                                    triggerCompletion();

                                    //trigger tracking at end
                                    TracktSimple.track("ModuleVideo", "Ended", module_id);
                                    //TODO check if resources tab is disabled first
                                    if ($scope.resourcesNull != true) {
                                        //toggle to resources tab
                                        $('.experts').removeClass('tab-active');
                                        $('.resources').addClass('tab-active');

                                        $('#experts-content').hide();
                                        $('#resources-content').show();
                                    }
                                });

                                //pause if tab nav is triggered
                                $('.tab').click(function () {
                                    videoObj.pause();
                                });

                            });
                        }
                    }, true);
                }


            }

            function triggerCompletion() {
                //trigger api call to complete module
                var user_id = window.userid || 0; //prevent course completeion if not logged in
                var module_id = window.model || 0;
                console.log('2. triggerCompletion module=' + module_id + " " + user_id);
                var completeEndpoint = '/API/MarkModuleComplete/' + module_id + '?userid=' + user_id;
                $http({ method: 'GET', url: completeEndpoint }).
                success(function (data, status, headers, config) {
                    //console.log(data);
                }).error(function (data, status, headers, config) {
                    //console.log(data);
                });
            }


        });





        angular.bootstrap(document, ['vmodule']);
    }

    self.mobileInit = function () {
        var self = this;
        //api call and squirt data in
        var moduleId = model || 1;
        var mobileUrl = '/api/Module/' + moduleId + '?userid=' + userid;

        $.ajax({
            url: mobileUrl,
            format: JSON,
            async: false,
            success: function (data) {
                //set module coursetype with a switch then be sure to apply to the right classes
                switch (data[0].coursetypeid[0]) {
                    case 1:
                        self.courseClass = 'bi';
                        break;

                    case 2:
                        self.courseClass = 'pd';
                        break;

                    case 3:
                        self.courseClass = 'fb';
                        break;
                }
                //set course Id to global
                self.courseId = data[0].module.courseid;

                //set next and prev module
                $prevModule = $('.prev-module-wrap-m');
                $nextModule = $('.next-module-wrap-m');
                if (data[0].module.sequence > 1) {
                    var prevModNum = parseInt(data[0].module.sequence) - 1;
                    $prevModule.find('.module-label-mobile').text('Module ' + prevModNum);
                    $prevModule.find('.module-title-mobile').text(data[0].previousmodulename);
                    $prevModule.find('.module-presenter-mobile').text('Presented by ' + data[0].prevpresentername);
                    var targ_prev_url = '/module/' + data[0].previousmodule;
                    $prevModule.find('a').attr('href', targ_prev_url);
                } else {
                    $prevModule.hide();
                }

                if (data[0].nextmodule) {
                    var nextModNum = parseInt(data[0].module.sequence) + 1;
                    $nextModule.find('.module-label-mobile').text('Module ' + nextModNum);
                    $nextModule.find('.module-title-mobile').text(data[0].nextmodulename);
                    $nextModule.find('.module-presenter-mobile').text('Presented by ' + data[0].nextpresentername);
                    var targ_next_url = '/module/' + data[0].nextmodule;
                    $nextModule.find('a').attr('href', targ_next_url);
                } else {
                    $nextModule.hide();
                }

                //grab other modules in course
                var moduleEndpoint = "/api/Course/" + data[0].module.courseid;
                $.ajax({
                    url: moduleEndpoint,
                    format: JSON,
                    async: false,
                    success: function (data) {
                        var _otherModulesHtml = '';
                        _.each(data[0].modules, function (value, key, list) {
                            _otherModulesHtml += '<li class="module-mobile"><a class="nostyle" href="/module/' + value.moduleid + '">';
                            _otherModulesHtml += '<p class="' + self.courseClass + ' module-label-mobile uppercase">Module ' + value.sequence + ':</p>';
                            _otherModulesHtml += '<p class="' + self.courseClass + ' module-title-mobile">' + value.modulename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></p>';
                            _otherModulesHtml += '<p class="module-presenter-mobile">Presented by ' + value.presenter.presenterfirstname + ' ' + value.presenter.presenterlastname + '</p>';
                            _otherModulesHtml += '</a></li>';
                        });
                        document.getElementById('other-modules-ul').innerHTML = _otherModulesHtml;
                    }

                });
                //insert data
                $('#main-module-coursetitle').text(data[0].coursename[0]);
                $('#main-module-label').text('Module ' + data[0].module.sequence);
                $('#main-module-title').text(data[0].module.modulename);
                $('#main-module-description').text(data[0].module.surveys[0].surveydescription);
                $('#main-module-scenario').text(data[0].module.moduledescription);

                var _questionsHtml = '';
                _.each(data[0].module.surveys[0].surveyquestions, function (value, key, list) {
                    //id = 'answer-'+ value.surveyquestionid-mobile
                    //value.surveyquestionname
                    //value[key].total / total responses = pct & bar height

                    _questionsHtml += '<li data-surveyid="' + data[0].module.surveys[0].surveyid + '" data-surveyquestionid="' + value.surveyquestionid + '" id="answer-' + value.surveyquestionid + '-mobile" class="list-group-item survey-response-item">';
                    _questionsHtml += '<p class="survey-answer-text">' + value.surveyquestionname + '</p>';
                    _questionsHtml += '<img style="margin-top: -22px;" class="pull-right" src="/Contents/imgs/mobile/module-check-off.png" />';
                    _questionsHtml += '</li>';
                });

                $('.survey-questions-tab-content > ul').html(_questionsHtml);

                

                //video - data[0].module.videos[0].videopath
                videojs('promo-video-mobile-mod').ready(function () {
                    var videoObj = this;
                    videoObj.src(data[0].module.videos[0].videopath);

                    // Player (this) is initialized and ready.
                    var duration = this.duration();

                    var completionTriggered = false;

                    //set up event listener for 'timeupdate' event
                    this.on('timeupdate', function (data) {
                        //pull time
                        var vTime = this.currentTime();
                        var durationTrigger = 90;
                        //if time >= 90s trigger switch to resources. 
                        if (vTime >= durationTrigger) {
                            TracktSimple.track("PromoVideo", "watchedMoreThan90Secs", "promovideo");

                            if (!completionTriggered) {
                                //trigger api call to complete module
                                triggerCompletion();
                                completionTriggered = true;
                            }

                        }
                    });

                    this.on('ended', function () {
                        //trigger completion
                        TracktSimple.track("PromoVideo", "CompletedWatching", "promovideo");
                        triggerCompletion();
                    });

                    //pause if tab nav is triggered
                    $('.module-tab-mobile').click(function () {
                        videoObj.pause();
                    });
                });

                var _resourcesHtml = '';
                //resources - data[0].module.resources --- .link
                _.each(data[0].module.resources, function (value, key, list) {
                    _resourcesHtml += '<li class="module-mobile resource"><a href="' + value.link + ' "><p class="resource-title">' + value.title + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></p></a></li>';
                });
                if (_resourcesHtml != '') {
                    $('.resource-list-mobile').html(_resourcesHtml);
                    $('.resource-wrap-mobile.show');
                } else {
                    $('.resource-wrap-mobile').hide();
                }


                //next/prev module - 'previousmodulename' - api call
                //other modules in this course - api call

            }
        });

        //set handlers for tab between survey and video
        $('.module-tab-mobile-mod').click(function () {
            var elem = $(this);
            if (elem.hasClass('m-tab-active')) {
                //do nothing
            } else {
                $('.module-tab-mobile').removeClass('m-tab-active');
                elem.addClass('m-tab-active');

                if (elem.hasClass('m-survey-tab')) {
                    //hide all panels
                    $('.m-module-tab-content').children().hide().removeClass('m-content-active');
                    //use off video icon for video tab
                    $('.m-video-content > img').attr('src', '/Contents/imgs/mobile/module-video-btn-off.png');
                    $('.m-survey-content').addClass('m-content-active').fadeIn();
                } else {
                    //show video
                    $('.m-module-tab-content').children().hide().removeClass('m-content-active');
                    $('.m-video-content > img').attr('src', '/Contents/imgs/mobile/module-video-btn-on.png');
                    $('.m-video-content').addClass('m-content-active').fadeIn();
                }

            }
        });

        //handlers for submit/grade survey
        $('.survey-response-item').click(function () {
            var elem = $(this);
            $('.survey-response-item').removeClass('m-active').find('img').attr('src', '/Contents/imgs/mobile/module-check-off.png');
            elem.addClass('m-active').find('img').attr('src', '/Contents/imgs/mobile/module-check-on.png');
        });

        $('.mobile-submit-survey').click(function () {
            var totalResponses = 0;

            //show error if no answer was selected
            $selectedAnswer = $('.survey-questions-tab-content').find('.m-active');
            if ($selectedAnswer.length == 1) {
                //hide error
                $('.survey-error-mobile').hide();
                triggerCompletion();
                var checkAnswerEndpointMobile = '/api/savesurveyanswer/' + userid + '?surveyid=' + $selectedAnswer.attr('data-surveyid') + '&surveyquestionid=' + $selectedAnswer.attr('data-surveyquestionid');
                $.ajax({
                    url: checkAnswerEndpointMobile,
                    format: JSON,
                    success: function (data) {
                        //console.log(data);
                        _.each(data, function (value, key, list) {
                            //calculate total responses from resp array
                            totalResponses = totalResponses + parseInt(value.total);
                        });

                        _answersHtmlMobile = '';
                        //loop through responses and insert into page
                        _.each(data, function (value, key, list) {
                            var answerPercent = (value.total / totalResponses) * 100;
                            answerPercent = Math.round(answerPercent);
                            var answersArr = {};
                            if (value.surveryquestionid == $selectedAnswer.attr('data-surveyquestionid'))
                                _answersHtmlMobile += '<tr class="mobile-survey-answer-selected">';
                            else
                                _answersHtmlMobile += '<tr>';

                            _.each($('.survey-response-item'), function (value, key, list) {
                                var val = $(value);
                                var k = val.attr('data-surveyquestionid');
                                answersArr[k] = val.find('.survey-answer-text').text();
                            });
                            _answersHtmlMobile += '<td class="mobile-answer-text">' + answersArr[value.surveryquestionid] + '</td>';
                            _answersHtmlMobile += '<td class="mobile-answer-percent">' + answerPercent + '%</td>';
                            _answersHtmlMobile += '<td class="answer-bar">';
                            _answersHtmlMobile += '<div class="v-progress-bar-mobile module-survey-prog">';
                            _answersHtmlMobile += '<span style="width: ' + answerPercent + '%"></span>';
                            _answersHtmlMobile += '</div></td></tr>';
                        });

                        $('.table-wrap-mobile > table > tbody').html(_answersHtmlMobile);
                        $('.survey-questions-tab-content').hide();
                        $('.survey-answers-tab-content').show();
                        //call likeminded
                        var likeMindedEndpointMobile = '/api/getLikeMinded/' + userid + '?surveyquestionid=' + $selectedAnswer.attr('data-surveyquestionid');
                        $.ajax({
                            url: likeMindedEndpointMobile,
                            format: JSON,
                            success: function (data) {
                                //console.log(data);
                                //do something with like minded data
                                var _lmHtml = '';
                                var count = 1;
                                var likePage = 1;
                                function calcPage(pageNum) {
                                    var page = pageNum / 4;
                                    page = Math.ceil(page);
                                    return page;
                                }
                                _.each(data, function (value, key, list) {
                                    if (count <= 4) {
                                        _lmHtml += '<div data-userid="' + value.user_id + '" data-likepage="' + likePage + '" class="vet pull-left">';
                                        _lmHtml += '<img class="img-circle" style="height:42px; width: 42px; " src="/contents/avatars/' + value.avatar + '">';
                                        _lmHtml += '<p class="purple">' + value.user_firstname + ' ' + value.user_lastname + '</p>';
                                        _lmHtml += '</div>';
                                    } else if (count > 4) {
                                        likePage = calcPage(count);
                                        _lmHtml += '<div data-userid="' + value.user_id + '" data-likepage="' + likePage + '" class="vet pull-left" style="display: none;">';
                                        _lmHtml += '<img class="img-circle" style="height:42px; width: 42px; " src="/contents/avatars/' + value.avatar + '">';
                                        _lmHtml += '<p class="purple">' + value.user_firstname + ' ' + value.user_lastname + ' </p>';
                                        _lmHtml += '</div>';
                                    }
                                    count++;
                                });

                                $('.similar-vets').html(_lmHtml);

                                //handle arrows
                                $('.like-nav-arrow.left-arrow').click(function () {
                                    var curPage = $(this).attr('data-curpage');
                                    if (curPage > 1) {
                                        //show left arrow if cur page isnt 1

                                        var nextPage = parseInt(curPage) - 1;
                                        //allow to nav back
                                        $("div[data-likepage]").hide();
                                        $("div[data-likepage = '" + nextPage + "']").show();
                                        $('.like-nav-arrow').attr('data-curpage', nextPage);
                                        if (nextPage == 1) {
                                            $(this).hide();

                                            //show right arrow
                                            $('.like-nav-arrow.right-arrow').show();
                                        } else {
                                            $(this).show();
                                        }
                                    } else {
                                        //do nothing, this is the first page
                                        $(this).hide();
                                    }
                                });

                                $('.like-nav-arrow.right-arrow').click(function () {
                                    var curPage = $(this).attr('data-curpage');
                                    if (curPage == likePage || curPage > likePage) {
                                        //dont do anything
                                        $(this).hide();
                                    } else {
                                        $(this).show();
                                        //navigate forward
                                        var nextPage = parseInt(curPage) + 1;
                                        //allow to nav back
                                        $("div[data-likepage]").hide();
                                        $("div[data-likepage = '" + nextPage + "']").show();
                                        $('.like-nav-arrow').attr('data-curpage', nextPage);
                                        if (nextPage == likePage || nextPage > likePage) {
                                            //hide right arrow
                                            $(this).hide();

                                            //show left arrow
                                            $('.like-nav-arrow.left-arrow').show();
                                        } else {
                                            $(this).show();
                                        }
                                    }
                                });
                            }
                        });
                    }
                });
            } else {
                //show error
                $('.survey-error-mobile').show();

            }

        });


        //utilities
        function triggerCompletion() {
            console.log('triggerCompletion');
            //trigger api call to complete module
            var user_id = window.userid || 0; //prevent course completeion if not logged in
            var module_id = window.model || 0;
            console.log('3. triggerCompletion module=' + module_id + " " +  user_id);
            var completeEndpoint = '/API/MarkModuleComplete/' + module_id + '?userid=' + user_id;
            $.ajax({
                url: completeEndpoint,
                success: function (data) {
                    //console.log(data);
                }
            });
        }
    }

    self.initModules = function () {
        var self = this;

        if($(window).width() <= 768){
            //only init mobile
            self.mobileInit();
        }else{
            //remove mobile
            $('.mod-video-wrap').remove();
            //init desktop
            self.initAngular();
        }
    }

    self.bindEvents = function () {
        //cache elements
        var self = this;
        var tabs = self.container.find('.tab');

        //trigger tab toggle function
        tabs.click(function () {
            self.toggleTabs(this, 'tab-active');

        });
    }

    return self;

}());

angular.element(document).ready(function () {
    VETAPP.pages.vmodule.init();
});



