// add page to namespace
VETAPP.namespace('VETAPP.pages.whoweare');

// create page
VETAPP.pages.whoweare = (function() {

	var self = {};

	self.init = function init() { ///console.log('init page: whoweare.js');

		var self = this;		
		self.isMobile 		= isMobile();
		self.container 	    = $('.whoweare-container');
		self.pageId			= $('#pageId');
		self.mobileContainer= $('#whoweare-container');
		self.mobileNavWrap  = $('.mobile-header');
		// keep track of rendered modal templates
		self.modals = [];

		// modal template
		self.template = {};

		if (self.pageId.hasClass('whoweare-container')) {

			// init Modules
			self.initModules();

			// get presenters
			self.getData();

			// Bind events
			self.bindEvents();

			self.setUpMobile();
		}

		function isMobile(){
			//check browser size, anything below 768 we consider 'mobile'
			if($(window).width() <= 768){
				self.windowWidth = $(window).width();
				return true;
			}else{
				return false;
			}
		}			
	}

	self.setUpMobile = function(){
		//load presenters data
		var url = '/api/presenters';

		$.ajax({
			url: url,
			contentType: JSON
		}).done(function (data) {
			///console.log(data);
			self.insertPresenterHtml(data);
		});

		$('.slide-nav').click(function(){

			var slideParentId = '#' + $(this).attr('data-parentpageid');
			var $slideParent = $(slideParentId);
			var targetId = '#' + $(this).attr('data-targetelem');
			var $target = $(targetId); 
			var $mobileContainer = $('.mobile-container');
			var direction = $(this).attr('data-direction') || 'left';

			mobileNav.changePage(targetId, slideParentId, 'slide-forward');
		});

		$('.mobile-nav-back').click(function(){
			mobileNav.back();
		});
		

		//show new page
		
	}

	self.insertPresenterHtml = function(data){
		var _html = "";
		var _detailsHtml = ""
		_.each(data, function(value, key, list){
			_html += '<li class="module-mobile presenter-link" data-presenterid="' + value.presenterid + '">';
			_html += '<img style="height: 35px; width: auto;" class="img-circle pull-left" src="/Contents/imgs/whoweare/' + value.presenterthumbnail + '" />';
		    _html += '<div class="whoweare-details">';
			_html += '<p class="mobile-connect-name">' + value.presenterfirstname + ' ' + value.presenterlastname + '</p>';
			_html += '<p class="mobile-connect-occupation">' + value.presentercertification + '<img style="margin-top: -8px;" class="pull-right arrow-r" src="/Contents/imgs/mobile/arrow-r.png" /></p>';
		    _html += '</div></li>';

		    _detailsHtml += '<div id="presenter-' + value.presenterid + '-details" data-role="page" style="display:none;" class="contributor-details-page slide-wrap">';
		    _detailsHtml += '<div data-role="content"><div class="top-panel">';
		    _detailsHtml += '<img style="height: 70px; width: 70px;" src="/Contents/imgs/whoweare/' + value.presenterimage + '" class="img-circle pull-left" />';
			_detailsHtml += '<div class="contributor-person-details">';
			_detailsHtml += '<p class="contributor-name purple">' + value.presenterfirstname + ' ' + value.presenterlastname + '</p>';
			_detailsHtml += '<p class="contributor-credentials purple">' + value.presentercertification + '</p>';
			_detailsHtml += '<p class="contributor-title">' + value.presentertitle + '</p>';
			_detailsHtml += '</div></div>';
			_detailsHtml += '<p class="contributor-details-text">' + value.presenterdescription + '</p>';
			_detailsHtml += '</div></div>';
		})

		//insert html
		$('ul.contributors').html(_html);
		$('#contributor-details-wrap').html(_detailsHtml);

		//bind handlers for presenter pages
		$('.presenter-link').click(function(){
			var targetPresenterId = $(this).attr('data-presenterid')
			var targetId = '#presenter-' + targetPresenterId + '-details';
			
			mobileNav.changePage(targetId, mobileNav.navState.currentPage ,'slide-forward');

			// //show presenter page
			// $(targetId).show();

			//todo, bind back button
			//self.setBackLink('#contributors-page', targetId);
		})
	}



	self.setBackLink = function(target, base){
		$('.mobile-nav-back').show();
		var $backLink = $('.mobile-nav-back');
		$backLink.unbind();
		$backLink.click(function(){
			$(base).hide();
			$(target).fadeIn();
		});
	}

	self.initModules = function()
	{
		var self = this;
		mobileNav = new VETAPP.Classes.Mobilenav({
			page : '#whoweare-container'
		}, self.mobileNavWrap);
	}

	self.getData = function()
	{
		var self = this;
		var url = '/api/presenters';

		$.ajax({
			url: url,
			contentType: JSON
		}).done(function (data) {
			///console.log(data);
			self.parsePresenters(data);
		});
	}
	
	self.parsePresenters = function(data)
	{
		var self = this;

		// Load template
		var source = $("#presenter-template").html();

		// Compile Handlebars template
		var template = Handlebars.compile(source);

		// Render html
		var html = template(data);

		// Add to dom
		$('.data-container').append(html);
	}

	self.buildModal = function(data, id, cb)
	{
		var self = this;		

		// Load template
		var source = $("#modal-template").html();

		// Compile Handlebars template
		var template = Handlebars.compile(source);
		
		// Render html
		var html = template(data);

		// create modal storage array
		var modal = {
			id: id,
			html: html
		}

		// push modal to array
		self.modals.push(modal);

		// Add modal to dom
		$('.modal-container').append(html);
		
		// Callback		
		cb.call();
	}

	self.openModal = function(id, source)
	{
		var self = this;

		// check if modal was rendered
		if ( _.findWhere(self.modals, {id: id}) == undefined) {
			// parse data
			var data = {
				person: [{
					id : id,
					fullname : source.find('.fullname').html(),
					status : source.find('.status').html(),
					blurb : source.find('.blurb').val(),
					title : source.find('.title').val(),
					company : source.find('.company').val(),
					photo : source.find('.photo').attr('data-profile')
				}]
			};
			// build modal 
			self.buildModal(data, id, function(){
				self.openId	= id;
				$('#contributorModal'+ id).modal();	
			});

		} else {
			// open rendered modal
			$('#contributorModal'+ id).modal();
		}		
	}

	self.bindEvents = function()
	{	
		var self = this;
		var doc = $(document);
		
		// close modal
		doc.on('click', '.close-modal', function(e){
			e.preventDefault();
			$('#contributorModal'+ $(this).attr('data-id')).modal('hide');
		});

		// add modal
		doc.on('click', '.show-contributor', function(e){
			// get item vals
			var id = $(this).attr('data-id');
			var source = $(this).parent();
			///console.log(this);
			self.openModal(id, source);
		});
	}

	return self;
}());

$(document).ready(function(){
	VETAPP.pages.whoweare.init();	
});