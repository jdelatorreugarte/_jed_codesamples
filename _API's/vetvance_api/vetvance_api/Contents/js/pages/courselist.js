// add page to namespace
VETAPP.namespace('VETAPP.pages.courselist');

// create page
VETAPP.pages.courselist = (function() {

	var self = {};

	self.init = function init() { /////console.log('init page: courselist.js');

		var self = this;
		
		self.isMobile = window.orientation || Modernizr.touch;
		self.container = $('.main-container');
		
		// init Modules
		self.initModules();
		// Bind events
		self.bindEvents();
	}


	self.initAngular = function(){
         	var courselist = angular.module('courselist', []);

         	courselist.controller('courseListCtrl', function($scope, $http){
         		//get page data with get - 
         		//TODO Write a service wrapper for API to make restful calls
         		
         		var user_id = window.userid || 20;
         		if(typeof user_id !== 'undefined' || user_id != ''){
         			endpoint = "/api/courselist/" + user_id;
         			$http({method: 'GET', cache: true, url: endpoint}).
         			success(function (data, status, headers, config) {
         			    /////console.log(data);
         			    $scope.userpaths = {};
         			    $scope.userpaths.bi = [];
         			    $scope.userpaths.pd = [];
         			    $scope.userpaths.fl = [];
         				angular.forEach(data, function(value, key){
         					switch (value['coursetypeid']) {
         					    case 1:
         					        $scope.userpaths.bi.push(value);
         					        break;
         					    case 2:
			                        $scope.userpaths.pd.push(value);
			                        break;
			                    case 3:
			                       $scope.userpaths.fl.push(value);
			                        break;
			                        
			                }
         				}, data);
		              
         			}).error(function(data, status, headers, config){
         				///console.log('Pffft, silent but deadly...api call failed.');
         			});
         		}else{
         			alert('You are not signed in. Tsk Tsk.');
         		}
         		

         		$scope.getCourseTypeName = function(courseTypeId){
         			switch(courseTypeId){
         				case 1:
         					return {coursetypename: "Business Issues", colorclass: "bi", id: courseTypeId};
         					break;

         				case 2:
         					return {coursetypename: "Professional Development", colorclass:"pod", id: courseTypeId};
         					break;

         				case 3:
         					return {coursetypename: "Financial Literacy", colorclass: "fb", id: courseTypeId};
         					break;
         			}
         		}

                // bad form, shame on me :-(
         		$scope.toggleDropdown = function (toggleId) {
         		    var toggleElem = $("ul[data-dropdownid='" + toggleId + "']");
         		    toggleElem.toggle('fast', function () {
         		        //swap arrow 
         		        var toggleImg = $(toggleElem).find('img');
         		        if (toggleImg.attr('src') == '/Contents/imgs/courselist/modules-dropdown-open.gif') {
         		            toggleImg.remove();//attr('src', '/Contents/imgs/courselist/modules-dropdown-closed.gif');
         		        } else {
         		            toggleImg.remove();//.attr('src', '/Contents/imgs/courselist/modules-dropdown-open.gif');
         		        }
         		    });
         		}

         	});

         
		
         	angular.bootstrap(document, ['courselist']);
	}

	self.initModules = function()
	{
		var self = this;

		self.initAngular();

	}
	self.bindEvents = function()
	{	
	    var self = this;
	}

	return self;

}());

angular.element(document).ready(function(){
    VETAPP.pages.courselist.init();	
});
