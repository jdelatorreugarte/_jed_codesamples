// add page to namespace
VETAPP.namespace('VETAPP.pages.advance');

// create page
VETAPP.pages.advance = (function () {

    var self = {};

    self.init = function init() {
        ///console.log('init page: advance.js');


        var self = this;

        self.isMobile = isMobile();
        self.container = $('.main-container');
        self.mobileContainer    = $('#advance-container-mobile');
        self.mobileNavWrap      = $('.mobile-header');
        self.selections = {
            student: 'null',
            job: 'null',
            debt: 'null'
        };
        self.pathVar = '';
        self.on = {};
        // init Modules
        self.initModules();
        // Bind events
        self.bindEvents();

        function isMobile(){
            //check browser size, anything below 768 we consider 'mobile'
            if($(window).width() <= 768){
                return true;
            }else{
                return false;
            }
        }
    }

    self.initAngular = function () {

            var advance = angular.module('advance', []);

        advance.controller('advanceCtrl', function ($scope, $http) {
        	$scope.firstLoad = false;
            var ext = ".png";
            $scope.pathSrc = 0 + ext;
            //initialize icon images
            //TODO add names
            $scope.iconList = [
                { key: '1.1', data: { src: '1-1' + ext, id: 'i1-1', on: false } },
                { key: '1.2', data: { src: '1-2' + ext, id: 'i1-2', on: false } },
                { key: '1.3', data: { src: '1-3' + ext, id: 'i1-3', on: false } },
                { key: '1.4', data: { src: '1-4' + ext, id: 'i1-4', on: false } },
                { key: '1.5', data: { src: '1-5' + ext, id: 'i1-5', on: false } },
                { key: '2.1', data: { src: '2-1' + ext, id: 'i2-1', on: false } },
                { key: '2.1', data: { src: '2-2' + ext, id: 'i2-2', on: false } },
                { key: '2.3', data: { src: '2-3' + ext, id: 'i2-3', on: false } },
                { key: '2.4', data: { src: '2-4' + ext, id: 'i2-4', on: false } },
                { key: '2.5', data: { src: '2-5' + ext, id: 'i2-5', on: false } },
                { key: '2.6', data: { src: '2-6' + ext, id: 'i2-6', on: false } },
                { key: '3.1', data: { src: '3-1' + ext, id: 'i3-1', on: false } },
                { key: '3.2', data: { src: '3-2' + ext, id: 'i3-2', on: false } }
            ];

            //set selects to initial case
            $scope.select1 = '';
            $scope.select2 = '';
            $scope.select3 = '';

            //set userpath
            checkUserPath();

            //change path according to selected options
            $scope.renderPath = function () {

                var pathString = '0';

                //must be nested, can only apply later selections if the prior ones worked
                if (!_.isUndefined($scope.select1) && !_.isEmpty($scope.select1)) {
                    pathString = '1' + $scope.select1;

                    if (!_.isUndefined($scope.select2) && !_.isEmpty($scope.select2)) {
                        pathString = pathString + "2" + $scope.select2;

                        if (!_.isUndefined($scope.select3) && !_.isEmpty($scope.select3)) {
                            pathString = pathString + "3" + $scope.select3;
                        }
                    }
                } else {

                }

                ///console.log(pathString);

                //set path img src
                $scope.pathSrc = pathString + ext;

                //set up opacity of icons based on path
                $scope.renderIconState(pathString);

                //push 'path' state to database
                var pathupdateurl = '/api/updateuserpath/' + window.userid;
                var student, job, debt;


                if(typeof $scope.select1 !== 'undefined'  && $scope.select1 != '' && ($scope.select1 == 'A' || $scope.select1 == 'B')){
                    if($scope.select1 == 'A')
                        student = true;
                    if ($scope.select1 == 'B')
                        student = false;
                }else
                    student = 'null';

                if(typeof $scope.select2 !== 'undefined' && $scope.select1 != ''  && ($scope.select2 == 'A' || $scope.select2 == 'B')) {
                    if($scope.select2 == 'A')
                        job = true;
                    if ($scope.select2 == 'B')
                        job = false;
                }else
                    job = 'null';

                if(typeof $scope.select3 !== 'undefined' && $scope.select1 != '' && ($scope.select3 == 'A' || $scope.select3 == 'B')){
                    if($scope.select3 == 'A')
                        debt = true;
                    if ($scope.select3 == 'B')
                        debt = false;
                }else
                    debt = 'null';

                $scope.firstLoad = true;

                $http({
                    method: 'GET',
                    url: pathupdateurl,
                    params: {
                        student: student,
                        job: job,
                        debt: debt
                    }
                }).success(function(data, status, headers, config) {
                    ///console.log(data);
                }).error(function (data, status, headers, config) {
                    ///console.log('Yikes, the query was unsuccessful');
                });


            }

            //set up icon opacity pased on current path
            $scope.renderIconState = function (pathString) {
                var on = {};

                //generate state array based on user settings
                switch (pathString) {
                    case '1A':
                        on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1B':
                        on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A':
                        on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2B':
                        on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2A':
                        on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2B':
                        on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A3A':
                        on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A3B':
                        on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6'];
                        break;

                    case '1A2B3A':
                        on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2B3B':
                        on = ['1.1', '1.2', '2.6'];
                        break;

                    case '1B2A3A':
                        on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2A3B':
                        on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6'];
                        break;

                    case '1B2B3A':
                        on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2B3B':
                        on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6'];
                        break;

                    default:
                        break;
                }

                applyIconState(on);
            }

            $scope.renderProgress = function (user) {

            }

            function checkUserPath(){
                var userId = window.userid || 0;

                //effed up kludgey code - this returns nothing
                getUserTypeId(userId);//window.usertypeid || 0;

                function getUserTypeId(userId){
                    var getUserTypeUrl = '/api/users/' + userId;
                    $http({method: 'GET', url: getUserTypeUrl}).
                    success(function(data, status, headers, config){
                        ///console.log(data);
                        if(data.length >= 1)
                            userTypeId = data[0].usertypeid;
                        else
                            userTypeId = 0
                        if(userTypeId != 'test'){
                            //make api call
                            var usertypeendpoint = '/API/UserPaths/' + userId + '/?usertypeid='  + userTypeId;
                            $http({method: 'GET', url: usertypeendpoint}).
                            success(function(data, status, headers, config){
                                //set select to appropriate values
                                ///console.log(data);
                                $scope.select1 = transformatron(data[0]['student'][0]);
                                $scope.select2 = transformatron(data[0]['job'][0]);
                                $scope.select3 = transformatron(data[0]['debt'][0]);

                                //reset selectric boxes
                                setTimeout(function () {
                                    $scope.$apply(function () {
                                        $('select').selectric('refresh');
                                    });
                                });

                                $scope.pathHolder = [];
                                //set up path according to api call to db
                                angular.forEach(data, function (value, key) {
                                    var src = '';
                                    var id = '';
                                    var ext = '.png';
                                    var courseCodeEdit = value.course.coursecode.replace('.', '-');
                                    var course = { key: value.course.coursecode, data: {src: courseCodeEdit + ext, id:'i' + courseCodeEdit, on: false, completed: value.completed, total: value.total, course: value.course }};
                                    ///console.log(course);
                                    $scope.pathHolder.push(course);
                                });


                                //render path
                                $scope.renderPath();


                                function transformatron(data) {
                                    switch(data){
                                        case 'null':
                                            return ''; //used to return ''
                                            break;

                                        case null:
                                            return ''; //used to return '' before we removed null case
                                            break;

                                        case 'true':
                                            return 'A';
                                            break;

                                        case true:
                                            return 'A';
                                            break;

                                        case false:
                                            return 'B';
                                            break

                                        case 'false':
                                            return 'B';
                                            break;

                                        default:
                                            return '';
                                            break
                                    }
                                }

                                //refresh selectrics
                            }).error(function(data, status, headers, config){
                                ///console.log(data);

                                //refresh selectrics
                            });
                        }else{
                            //user type is null, start from blank state
                            var nullCaseEndpoint = '/api/userpaths/0?usertypeid=0';
                        }

                    }).error(function(data, status, headers, config){
                        ///console.log(data);
                    });
                }

            }

            function applyIconState(stateArr) {
                //zero out the icon state

                nullIconState($scope.pathHolder);

                //look through the state array and apply the appropriate states to the right icons in scope
                _.each(stateArr, function (iconState) {
                    angular.forEach($scope.pathHolder, function (value, key) {
                        if (value.key == iconState) {
                            this[key].data.on = true;
                        }
                    }, $scope.pathHolder);
                });
            }

            function nullIconState(data) {
                angular.forEach(data, function (value, key) {
                    value.data.on = false;
                });
            }

            }).directive('selectric', function($timeout) {
                return {
                    // Restrict it to be an attribute in this case
                    restrict: 'A',
                    // responsible for registering DOM listeners as well as updating the DOM
                    link: function(scope, element, attrs) {
                        $timeout(function() {
                            $(element).selectric();
                            $('.button').html('<img src="/contents/imgs/orange-dropdown-icon.gif">');

                            //set callback to update scope
                            $(element).change(function(){
                                //refresh
                                $('select').selectric('refresh');

                                $('p .label').addClass('on');
                            });

                            $('.selectric').each(function(i){
							    $(this).addClass('dropdownLabel_'+i);
							});
                        });
                    }
                };
            }).directive('popover', function () {
                var userId = window.userid || 0;
                return {
                    restrict: 'A',
                    scope: {
                        content: '='
                    },
                    link: function (scope, elem, attr) {
                        switch(scope.content.key){
                        	case '1.1':
                        		var options = {
		                            animation: false,
		                            html: true,
		                            placement: 'right',
		                            trigger: 'manual',
		                            content: generatePopoverContents(scope.content, userId)
		                        }
                        		break;

                        	case '1.5':
                        		var options = {
		                            animation: false,
		                            html: true,
		                            placement: 'left',
		                            trigger: 'manual',
		                            content: generatePopoverContents(scope.content, userId)
		                        }
                        		break;

                        	case '2.1':
                        		var options = {
		                            animation: false,
		                            html: true,
		                            placement: 'right',
		                            trigger: 'manual',
		                            content: generatePopoverContents(scope.content, userId)
		                        }
                        		break;

                        	case '2.6':
                        		var options = {
		                            animation: false,
		                            html: true,
		                            placement: 'left',
		                            trigger: 'manual',
		                            content: generatePopoverContents(scope.content, userId)
		                        }
                        		break;

                        	case '3.1':
                        		var options = {
		                            animation: false,
		                            html: true,
		                            placement: 'right',
		                            trigger: 'manual',
		                            content: generatePopoverContents(scope.content, userId)
		                        }
                        		break;
                        	default:
                        		var options = {
		                            animation: false,
		                            html: true,
		                            placement: 'left',
		                            trigger: 'manual',
		                            content: generatePopoverContents(scope.content, userId)
		                        }
                        		break;

                        }


                        function checkPosition() {

                        }

                        //initialize popovers on all elements
                        $(elem).popover(options).on('click', function () {

                        	//manually show the popover
                            $(this).popover('toggle');

                            //toggle that opacity, dog!!!
                            var parentElem = $(this).parent();
                            var opacity = parentElem.css('opacity');
                            if (opacity == '1') {
                                parentElem.css('opacity', '.4');
                            } else {
                                parentElem.css('opacity', '1');
                            }
                            var color, courseType;

                            //check what the primary color should be
                            switch (scope.content.data.course.coursetype.coursetypeid) {
                                case 1:
                                    color = '#3f7dc8 !important';
                                    courseType = 'bi';
                                    break;

                                case 2:
                                    color = '#00b3d3 !important';
                                    courseType = 'pd';
                                    break;

                                case 3:
                                    color = '#e03e52 !important';
                                    courseType = 'fb';
                                    break;
                            }

                            //set color based on which popover you click
                            $('.popover').removeClass('bi pd fb').addClass(courseType).find('.advancetocourse').removeClass('btnbi btnpd btnfb').addClass('btn' + courseType);


                            //hide all other popovers - one at a time only, player.
                            var $popsToHide = $('.v-popover').not($(this));
                            $popsToHide.popover('hide');
                            var $parentPopsToHide = $popsToHide.parent();
                            $parentPopsToHide.css('opacity', '.4');

                            //bind close function to click
                            $('.close').on('click', function(){
                            	var courseId = $(this).attr('data-courseid');
                            	$('#' + courseId).css('opacity', '.4').find('.v-popover').popover('hide');
                            });

                            //attach the tracking logic dawg.
                            $('.track-click').on('click', function(e){
                                //console.log('updated and stuff');

                                $(this).on("click", function (d) {
                                    var name = $(this).attr('trkname');
                                    var arr = name.split('|');
                                    //console.log('it was clicked');
                                    //gTrackEvent('_trackEvent', arr[0], arr[1], arr[2], null, null);
                                    //alert('.track.click() zz ' + arr[0] + " " + arr[1]);
                                    ga('send', {
                                        'hitType': 'event',
                                        'eventCategory': 'button',
                                        'eventAction': arr[0],
                                        'eventLabel': arr[1] + " " + arr[2],
                                        'hitCallback': function () {
                                        //console.log("=> GA callback func for event :" + arr[0] + " " + arr[1] + " " + arr[2]);
                                            //alert('callback from GA');
                                        }
                                    });
                                });
                                //trackAdvanceClick($(this));
                            });
                        });

                        //this line is important, without it the icon html will be obliterated
                        var advancePopoverHtml = '<img src="/contents/imgs/advance/' + scope.content.data.src + '" ><div class="v-progress-bar orange"><span style="width:' + scope.content.data.completed / scope.content.data.total * 100 + '%"></span></div><p>' + scope.content.data.course.coursename + '</p>';
                        $(elem).html(advancePopoverHtml);


                    }
                }
                //template generator function
                function generatePopoverContents(content, isLoggedIn) {

                    var html;
                    if (isLoggedIn != 0) {
                        if (content.data.completed > 0) {
                            html = '<div class="popover-header"><button type="button" data-courseid="' + content.data.id + '" class="close" aria-hidden="true">&times;</button><div class="header-text"><h4 class="uppercase"><strong>' + content.data.course.coursename + '</strong></h4></div></div><div class="clearfix"></div><div class="popover-body" style="color: #5f5f5f !important;">' + content.data.course.coursedescription + '</div><div class="mod-completion uppercase"><span class="modcount"><strong>' + content.data.completed + '</strong> <span style="text-transform: none;">of</span> <strong>' + content.data.total + '</span></strong> modules completed <a href="/course/' + content.data.course.courseid + '" class="btn advancetocourse uppercase popover-link track-click" trkname="Advance|Continue|LoggedIn">Continue</a></div>';
                        }else{
                            html = '<div class="popover-header"><button type="button" data-courseid="' + content.data.id + '" class="close" aria-hidden="true">&times;</button><div class="header-text"><h4 class="uppercase"><strong>' + content.data.course.coursename + '</strong></h4></div></div><div class="clearfix"></div><div class="popover-body" style="color: #5f5f5f !important;">' + content.data.course.coursedescription + '</div><div class="mod-completion uppercase"><span class="modcount"><strong>' + content.data.completed + '</strong> <span style="text-transform: none;">of</span> <strong>' + content.data.total + '</span></strong> modules completed <a href="/course/' + content.data.course.courseid + '" class="btn advancetocourse uppercase popover-link track-click" trkname="Advance|Start|LoggedIn">Start</a></div>';
                        }
                    } else {
                        html = '<div class="popover-header"><button type="button" data-courseid="' + content.data.id + '" class="close pull-right" aria-hidden="true">&times;</button><div class="header-text"><h4 class="uppercase"><strong>' + content.data.course.coursename + '</strong></h4></div></div><div class="clearfix"></div><div class="popover-body" style="color: #5f5f5f !important;">' + content.data.course.coursedescription + '</div><div class="pull-right"><a href="/course/' + content.data.course.courseid + '" class="btn advancetocourse uppercase popover-link track-click" trkname="Advance|Start|NotLoggedIn">Start</a></div>';
                    }
                    return html;
                }
            });

            angular.bootstrap(document, ['advance']);

    }

    self.initMobile = function(){
        var self = this;
         //TODO: Do not allow

         //student job debt
         //selections.student = selections.job = selections.debt = null;

         function setPathVar(selections){
            self.pathVar = '';
            var counter = 1;
            _.each(selections, function(value, key, list){
               switch(value){
                case null:
                    //do nothing
                    break;
                case 'A':
                    var selNum = counter;
                    self.pathVar += selNum + value;
                    break;

                case 'B':
                    var selNum = counter;
                    self.pathVar += selNum + value;
                    break;

               }
                counter++;
            });
         }

        //on click of button, submit to db, generate 'suggested courses' and move to next selection
        $('.advance-select').click(function(){
            var elem = $(this);
            elem.addClass('active').attr('src', '/Contents/imgs/mobile/arrow-r-active.png');

            //set targets from element & slide to next page
            var target = elem.attr('data-target');
            var backTo = elem.attr('data-parentpageid');

            //slide to the next page
            mobileNav.changePage(target, backTo, 'slide-forward', elem);

            //get value and proper var to set
            var selectType = elem.attr('data-selecttype');
            var value = elem.attr('data-value');

            //set the var
            self.selections[selectType] = value;

            //set the pathvar
            setPathVar(self.selections);
            if(userid){
                //generate list - call user, api, get all modules/completion
                 var userDataUrl = '/api/Users/' + userid;
                 $.ajax({
                    url: userDataUrl,
                    contentType: JSON,
                    success: function(data){
                        ///console.log(data);
                        //call user path api
                        var userTypeId = data[0].usertypeid;
                        var userPathCall = '/API/UserPaths/' + userid + '/?usertypeid='  + userTypeId;
                        $.ajax({
                            url: userPathCall,
                            contentType: JSON,
                            success: function(data){
                                ///console.log(data);
                                var suggestedCourses = {};
                                //use switch to find generate selected courses, use course id to copy data from user api call
                                switch (self.pathVar) {
                                    case '1A':
                                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1B':
                                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1A2A':
                                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1A2B':
                                        self.on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1B2A':
                                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1B2B':
                                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1A2A3A':
                                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1A2A3B':
                                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6'];
                                        break;

                                    case '1A2B3A':
                                        self.on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1A2B3B':
                                        self.on = ['1.1', '1.2', '2.6'];
                                        break;

                                    case '1B2A3A':
                                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1B2A3B':
                                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6'];
                                        break;

                                    case '1B2B3A':
                                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                                        break;

                                    case '1B2B3B':
                                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6'];
                                        break;

                                    default:
                                        break;
                                }

                                //compare self.on to data and pluck out data.course when data.course.coursecode is matched in self.on
                                _.each(data, function(value, key, list){
                                    if(_.contains(self.on, value.course.coursecode))
                                        suggestedCourses[key] = value;
                                });

                                //insert suggested courses
                                var _suggestedHtml = '';
                                _.each(suggestedCourses, function(value, key, list){
                                    var courseClass = '';
                                    switch(value.course.coursetypeid){
                                        case 1:
                                            courseClass = 'bi';
                                            break;
                                        case 2:
                                            courseClass = 'pd';
                                            break;
                                        case 3:
                                            courseClass = 'fb';
                                            break;
                                    }

                                    _suggestedHtml += '<li class="advance-result list-group-item">';
                                    _suggestedHtml += '<a class="nostyle" href="/course/' + value.course.courseid + '">';
                                    _suggestedHtml += '<span class="result-title ' + courseClass + '">' + value.course.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                    _suggestedHtml += '<span class="result-module-info muted uppercase">' + value.completed + ' of ' + value.total + ' modules completed</span>';
                                    _suggestedHtml += '</a></li>';
                                });

                                var _allCoursesHtml = {};
                                _allCoursesHtml.bi =  _allCoursesHtml.pd = _allCoursesHtml.fb = '';
                                _.each(data, function(value, key, list){
                                    //sort into buckets by course type
                                    switch (value.course.coursetypeid){
                                        case 1:
                                            courseClass = 'bi';

                                            _allCoursesHtml.bi += '<li class="advance-result list-group-item">';
                                            _allCoursesHtml.bi += '<a class="nostyle" href="/course/' + value.course.courseid + '">';
                                            _allCoursesHtml.bi += '<span class="result-title ' + courseClass + '">' + value.course.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                            _allCoursesHtml.bi += '<span class="result-module-info muted uppercase">' + value.completed + ' of ' + value.total + ' modules completed</span>';
                                            _allCoursesHtml.bi += '</a></li>';
                                            break;
                                        case 2:
                                            courseClass = 'pd';

                                            _allCoursesHtml.pd += '<li class="advance-result list-group-item">';
                                            _allCoursesHtml.pd += '<a class="nostyle" href="/course/' + value.course.courseid + '">';
                                            _allCoursesHtml.pd += '<span class="result-title ' + courseClass + '">' + value.course.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                            _allCoursesHtml.pd += '<span class="result-module-info muted uppercase">' + value.completed + ' of ' + value.total + ' modules completed</span>';
                                            _allCoursesHtml.pd += '</a></li>';
                                            break;
                                        case 3:
                                            courseClass = 'fb';

                                            _allCoursesHtml.fb += '<li class="advance-result list-group-item">';
                                            _allCoursesHtml.fb += '<a class="nostyle" href="/course/' + value.course.courseid + '">';
                                            _allCoursesHtml.fb += '<span class="result-title ' + courseClass + '">' + value.course.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                            _allCoursesHtml.fb += '<span class="result-module-info muted uppercase">' + value.completed + ' of ' + value.total + ' modules completed</span>';
                                            _allCoursesHtml.fb += '</a></li>';
                                            break;
                                    }
                                });

                                //insert courses
                                $('.personalized.advance-results').html(_suggestedHtml);
                                $('#advance-results-bi').html(_allCoursesHtml.bi);
                                $('#advance-results-pd').html(_allCoursesHtml.pd);
                                $('#advance-results-fb').html(_allCoursesHtml.fb);

                                setActiveSelections();


                            }
                        })



                    }
                });
            }else{
                //show list of courses based on the selections the user made
                var suggestedCourses = {};
                //use switch to find generate selected courses, use course id to copy data from user api call
                switch (self.pathVar) {
                    case '1A':
                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1B':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A':
                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2B':
                        self.on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2A':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2B':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A3A':
                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A3B':
                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6'];
                        break;

                    case '1A2B3A':
                        self.on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2B3B':
                        self.on = ['1.1', '1.2', '2.6'];
                        break;

                    case '1B2A3A':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2A3B':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6'];
                        break;

                    case '1B2B3A':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2B3B':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6'];
                        break;

                    default:
                        self.on = [];
                        break;
                }

                //compare self.on to data and pluck out data.course when data.course.coursecode is matched in self.on
                //need to get a list of all courses
                var courseListUrl = '/api/courselist/999';
                $.ajax({
                    url: courseListUrl,
                    contentType: JSON,
                    success: function(data){
                        _.each(data, function(value, key, list){
                            if(_.contains(self.on, value.coursecode))
                                suggestedCourses[key] = value;
                        });

                        //insert suggested courses
                        var _suggestedHtml = '';
                        _.each(suggestedCourses, function(value, key, list){
                            var courseClass = '';
                            switch(value.coursetypeid){
                                case 1:
                                    courseClass = 'bi';
                                    break;
                                case 2:
                                    courseClass = 'pd';
                                    break;
                                case 3:
                                    courseClass = 'fb';
                                    break;
                            }

                            _suggestedHtml += '<li class="advance-result list-group-item">';
                            _suggestedHtml += '<a class="nostyle" href="/course/' + value.courseid + '">';
                            _suggestedHtml += '<span class="result-title ' + courseClass + '">' + value.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                            _suggestedHtml += '</a></li>';
                        });

                        var _allCoursesHtml = {};
                        _allCoursesHtml.bi =  _allCoursesHtml.pd = _allCoursesHtml.fb = '';
                        _.each(data, function(value, key, list){
                            //sort into buckets by course type
                            switch (value.coursetypeid){
                                case 1:
                                    courseClass = 'bi';

                                    _allCoursesHtml.bi += '<li class="advance-result list-group-item">';
                                    _allCoursesHtml.bi += '<a class="nostyle" href="/course/' + value.courseid + '">';
                                    _allCoursesHtml.bi += '<span class="result-title ' + courseClass + '">' + value.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                    _allCoursesHtml.bi += '</a></li>';
                                    break;
                                case 2:
                                    courseClass = 'pd';

                                    _allCoursesHtml.pd += '<li class="advance-result list-group-item">';
                                    _allCoursesHtml.pd += '<a class="nostyle" href="/course/' + value.courseid + '">';
                                    _allCoursesHtml.pd += '<span class="result-title ' + courseClass + '">' + value.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                    _allCoursesHtml.pd += '</a></li>';
                                    break;
                                case 3:
                                    courseClass = 'fb';

                                    _allCoursesHtml.fb += '<li class="advance-result list-group-item">';
                                    _allCoursesHtml.fb += '<a class="nostyle" href="/course/' + value.courseid + '">';
                                    _allCoursesHtml.fb += '<span class="result-title ' + courseClass + '">' + value.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                    _allCoursesHtml.fb += '</a></li>';
                                    break;
                            }
                        });

                        //insert courses
                        $('.personalized.advance-results').html(_suggestedHtml);
                        if(_suggestedHtml == ''){
                            _suggestedHtml = "<center><p>We have no suggested courses for you.</p></center>";
                            $('.personalized.advance-results').html(_suggestedHtml);
                        }
                        $('#advance-results-bi').html(_allCoursesHtml.bi);
                        $('#advance-results-pd').html(_allCoursesHtml.pd);
                        $('#advance-results-fb').html(_allCoursesHtml.fb);

                        //set targets from element
                        var target = elem.attr('data-target');
                        var backTo = elem.attr('data-parentpageid');
                    }
                });
            }



            //update path in db
            var userpathurl = 'api/updateuserpath/' + userid;
            function toBool(data){
                var outputArr = {
                    student: null,
                    job: null,
                    debt: null
                }
                switch(data.student){
                    case 'A':
                        outputArr.student = 'false';
                        break;
                    case 'B':
                        outputArr.student = 'true';
                        break;

                    case 'null':
                        outputArr.student = 'false';
                        break;
                }

                switch(data.job){
                    case 'A':
                        outputArr.job = 'false';
                        break;
                    case 'B':
                        outputArr.job = 'true';
                        break;

                    case 'null':
                        outputArr.job = 'false';
                        break;
                }

                switch(data.debt){
                    case 'A':
                        outputArr.debt = 'false';
                        break;
                    case 'B':
                        outputArr.debt = 'true';
                        break;

                    case 'null':
                        outputArr.debt = 'false';
                        break;
                }

                return outputArr;
            }
            $.ajax({
                url: userpathurl,
                contentType: JSON,
                data: toBool(self.selections),
                success: function(data){
                    ///console.log(data);
                    //generate & insert courselist
                    //self.pathVar

                    //set targets from element
                    var target = elem.attr('data-target');
                    var backTo = elem.attr('data-parentpageid');

                    //slide to the next page
                    mobileNav.changePage(target, backTo, 'slide-forward', elem);
                },
                failure: function(){
                    ///console.log('Update Path Action Failed');
                    mobileNav.changePage(target, backTo, 'slide-forward', elem);
                }

            });
        });

        $('.show-recommended').click(function(){
            var elem = $(this);
            //set targets from element
            var target = elem.attr('data-target');
            var backTo = elem.attr('data-parentpageid');
            mobileNav.changePage(target, backTo, 'slide-forward', elem);

            //check if user is logged in
            if(userid){
                //generate list - call user, api, get all modules/completion
             var userDataUrl = '/api/Users/' + userid;
             $.ajax({
                url: userDataUrl,
                contentType: JSON,
                success: function(data){
                    ///console.log(data);
                    //call user path api
                    var userTypeId = data[0].usertypeid;
                    var userPathCall = '/API/UserPaths/' + userid + '/?usertypeid='  + userTypeId;
                    $.ajax({
                        url: userPathCall,
                        format: JSON,
                        success: function(data){
                            ///console.log(data);
                            var suggestedCourses = {};
                            //use switch to find generate selected courses, use course id to copy data from user api call
                            switch (self.pathVar) {
                                case '1A':
                                    self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                                    break;

                                case '1B':
                                    self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                                    break;

                                case '1A2A':
                                    self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                                    break;

                                case '1A2B':
                                    self.on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                                    break;

                                case '1B2A':
                                    self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                                    break;

                                case '1B2B':
                                    self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                                    break;

                                case '1A2A3A':
                                    self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                                    break;

                                case '1A2A3B':
                                    self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6'];
                                    break;

                                case '1A2B3A':
                                    self.on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                                    break;

                                case '1A2B3B':
                                    self.on = ['1.1', '1.2', '2.6'];
                                    break;

                                case '1B2A3A':
                                    self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                                    break;

                                case '1B2A3B':
                                    self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6'];
                                    break;

                                case '1B2B3A':
                                    self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                                    break;

                                case '1B2B3B':
                                    self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6'];
                                    break;

                                default:
                                    self.on = [];
                                    break;
                            }

                            //compare self.on to data and pluck out data.course when data.course.coursecode is matched in self.on
                            _.each(data, function(value, key, list){
                                if(_.contains(self.on, value.course.coursecode))
                                    suggestedCourses[key] = value;
                            });

                            //insert suggested courses
                            var _suggestedHtml = '';
                            _.each(suggestedCourses, function(value, key, list){
                                var courseClass = '';
                                switch(value.course.coursetypeid){
                                    case 1:
                                        courseClass = 'bi';
                                        break;
                                    case 2:
                                        courseClass = 'pd';
                                        break;
                                    case 3:
                                        courseClass = 'fb';
                                        break;
                                }

                                _suggestedHtml += '<li class="advance-result list-group-item">';
                                _suggestedHtml += '<a class="nostyle" href="/course/' + value.course.courseid + '">';
                                _suggestedHtml += '<span class="result-title ' + courseClass + '">' + value.course.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                _suggestedHtml += '<span class="result-module-info muted uppercase">' + value.completed + ' of ' + value.total + ' modules completed</span>';
                                _suggestedHtml += '</a></li>';
                            });

                            var _allCoursesHtml = {};
                            _allCoursesHtml.bi =  _allCoursesHtml.pd = _allCoursesHtml.fb = '';
                            _.each(data, function(value, key, list){
                                //sort into buckets by course type
                                switch (value.course.coursetypeid){
                                    case 1:
                                        courseClass = 'bi';

                                        _allCoursesHtml.bi += '<li class="advance-result list-group-item">';
                                        _allCoursesHtml.bi += '<a class="nostyle" href="/course/' + value.course.courseid + '">';
                                        _allCoursesHtml.bi += '<span class="result-title ' + courseClass + '">' + value.course.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                        _allCoursesHtml.bi += '<span class="result-module-info muted uppercase">' + value.completed + ' of ' + value.total + ' modules completed</span>';
                                        _allCoursesHtml.bi += '</a></li>';
                                        break;
                                    case 2:
                                        courseClass = 'pd';

                                        _allCoursesHtml.pd += '<li class="advance-result list-group-item">';
                                        _allCoursesHtml.pd += '<a class="nostyle" href="/course/' + value.course.courseid + '">';
                                        _allCoursesHtml.pd += '<span class="result-title ' + courseClass + '">' + value.course.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                        _allCoursesHtml.pd += '<span class="result-module-info muted uppercase">' + value.completed + ' of ' + value.total + ' modules completed</span>';
                                        _allCoursesHtml.pd += '</a></li>';
                                        break;
                                    case 3:
                                        courseClass = 'fb';

                                        _allCoursesHtml.fb += '<li class="advance-result list-group-item">';
                                        _allCoursesHtml.fb += '<a class="nostyle" href="/course/' + value.course.courseid + '">';
                                        _allCoursesHtml.fb += '<span class="result-title ' + courseClass + '">' + value.course.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                        _allCoursesHtml.fb += '<span class="result-module-info muted uppercase">' + value.completed + ' of ' + value.total + ' modules completed</span>';
                                        _allCoursesHtml.fb += '</a></li>';
                                        break;
                                }
                            });

                            //insert courses
                            $('.personalized.advance-results').html(_suggestedHtml);
                            if(_suggestedHtml == ''){
                                _suggestedHtml = "<center><p>We have no suggested courses for you.</p></center>";
                                $('.personalized.advance-results').html(_suggestedHtml);
                            }
                            $('#advance-results-bi').html(_allCoursesHtml.bi);
                            $('#advance-results-pd').html(_allCoursesHtml.pd);
                            $('#advance-results-fb').html(_allCoursesHtml.fb);

                            //set targets from element
                            var target = elem.attr('data-target');
                            var backTo = elem.attr('data-parentpageid');
                        }
                    });
                }
             });
            }else{
                //show list of courses based on the selections the user made
                var suggestedCourses = {};
                //use switch to find generate selected courses, use course id to copy data from user api call
                switch (self.pathVar) {
                    case '1A':
                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1B':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A':
                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2B':
                        self.on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2A':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2B':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A3A':
                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2A3B':
                        self.on = ['1.1', '1.2', '2.1', '2.2', '2.3', '2.4', '2.6'];
                        break;

                    case '1A2B3A':
                        self.on = ['1.1', '1.2', '2.6', '3.1', '3.2'];
                        break;

                    case '1A2B3B':
                        self.on = ['1.1', '1.2', '2.6'];
                        break;

                    case '1B2A3A':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2A3B':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.1', '2.2', '2.3', '2.4', '2.5', '2.6'];
                        break;

                    case '1B2B3A':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6', '3.1', '3.2'];
                        break;

                    case '1B2B3B':
                        self.on = ['1.1', '1.2', '1.3', '1.4', '1.5', '2.5', '2.6'];
                        break;

                    default:
                        self.on = [];
                        break;
                }

                //compare self.on to data and pluck out data.course when data.course.coursecode is matched in self.on
                //need to get a list of all courses
                var courseListUrl = '/api/courselist/999';
                $.ajax({
                    url: courseListUrl,
                    contentType: JSON,
                    success: function(data){
                        _.each(data, function(value, key, list){
                            if(_.contains(self.on, value.coursecode))
                                suggestedCourses[key] = value;
                        });

                        //insert suggested courses
                        var _suggestedHtml = '';
                        _.each(suggestedCourses, function(value, key, list){
                            var courseClass = '';
                            switch(value.coursetypeid){
                                case 1:
                                    courseClass = 'bi';
                                    break;
                                case 2:
                                    courseClass = 'pd';
                                    break;
                                case 3:
                                    courseClass = 'fb';
                                    break;
                            }

                            _suggestedHtml += '<li class="advance-result list-group-item">';
                            _suggestedHtml += '<a class="nostyle" href="/course/' + value.courseid + '">';
                            _suggestedHtml += '<span class="result-title ' + courseClass + '">' + value.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                            _suggestedHtml += '</a></li>';
                        });

                        var _allCoursesHtml = {};
                        _allCoursesHtml.bi =  _allCoursesHtml.pd = _allCoursesHtml.fb = '';
                        _.each(data, function(value, key, list){
                            //sort into buckets by course type
                            switch (value.coursetypeid){
                                case 1:
                                    courseClass = 'bi';

                                    _allCoursesHtml.bi += '<li class="advance-result list-group-item">';
                                    _allCoursesHtml.bi += '<a class="nostyle" href="/course/' + value.courseid + '">';
                                    _allCoursesHtml.bi += '<span class="result-title ' + courseClass + '">' + value.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                    _allCoursesHtml.bi += '</a></li>';
                                    break;
                                case 2:
                                    courseClass = 'pd';

                                    _allCoursesHtml.pd += '<li class="advance-result list-group-item">';
                                    _allCoursesHtml.pd += '<a class="nostyle" href="/course/' + value.courseid + '">';
                                    _allCoursesHtml.pd += '<span class="result-title ' + courseClass + '">' + value.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                    _allCoursesHtml.pd += '</a></li>';
                                    break;
                                case 3:
                                    courseClass = 'fb';

                                    _allCoursesHtml.fb += '<li class="advance-result list-group-item">';
                                    _allCoursesHtml.fb += '<a class="nostyle" href="/course/' + value.courseid + '">';
                                    _allCoursesHtml.fb += '<span class="result-title ' + courseClass + '">' + value.coursename + '<img class="pull-right arrow-r" src="/Contents/imgs/mobile/' + courseClass + '-arrow-r.png"></span>';
                                    _allCoursesHtml.fb += '</a></li>';
                                    break;
                            }
                        });

                        //insert courses
                        $('.personalized.advance-results').html(_suggestedHtml);
                        if(_suggestedHtml == ''){
                            _suggestedHtml = "<center><p>We have no suggested courses for you.</p></center>";
                            $('.personalized.advance-results').html(_suggestedHtml);
                        }
                        $('#advance-results-bi').html(_allCoursesHtml.bi);
                        $('#advance-results-pd').html(_allCoursesHtml.pd);
                        $('#advance-results-fb').html(_allCoursesHtml.fb);

                        //set targets from element
                        var target = elem.attr('data-target');
                        var backTo = elem.attr('data-parentpageid');
                    }
                });
            }
        });

        $('.start-over').click(function(){
            var elem = $(this);
            mobileNav.changePage('advance-student-m', '', 'slide-back', elem);

            //null all selections
            nullSelections();

            //empty recommended courses
            $('.personalized').html('<center><p>We have no suggested courses for you.</p></center>');
        });

        $('.mobile-nav-back').click(function(){
           mobileNav.back();
        });

        function nullSelections(){
            //null elements
            $('.advance-select').removeClass('active').find('img').attr('src', '/Contents/imgs/mobile/arrow-r.png');

            //null selections
            self.selections = {
                student: 'null',
                job: 'null',
                debt: 'null'
            };
            self.pathVar = '';
        }

        function setActiveSelections(){
            //null all
            $('.advance-select').removeClass('active').find('img').attr('src', '/Contents/imgs/mobile/arrow-r.png');


            switch(self.selections.student){
                case 'A':
                    $('#student-a').addClass('active').find('img').attr('src', '/Contents/imgs/mobile/arrow-r-active.png');
                    break;
                case 'B':
                    $('#student-b').addClass('active').find('img').attr('src', '/Contents/imgs/mobile/arrow-r-active.png');
                    break;
            }

            switch(self.selections.job){
                case 'A':
                    $('#job-a').addClass('active').find('img').attr('src', '/Contents/imgs/mobile/arrow-r-active.png');
                    break;
                case 'B':
                    $('#job-b').addClass('active').find('img').attr('src', '/Contents/imgs/mobile/arrow-r-active.png');
                    break;
            }

            switch(self.selections.debt){
                case 'A':
                    $('#debt-a').addClass('active').find('img').attr('src', '/Contents/imgs/mobile/arrow-r-active.png');
                    break;
                case 'B':
                    $('#debt-b').addClass('active').find('img').attr('src', '/Contents/imgs/mobile/arrow-r-active.png');
                    break;
            }

        }

    }

    self.initModules = function () {
        var self = this;

        self.initAngular();

        self.initMobile();

    }
    self.bindEvents = function () {
        var self = this;
        //bootstrap hack that hides popovers when user clicks off. Fixing bootstrap core bug without hacking the core.
        $('body').on('hidden.bs.popover', function() {
		  $('.popover:not(.in)').hide().detach();
		});
    }

    return self;

}());

angular.element(document).ready(function () {
    VETAPP.pages.advance.init();
});
