// add page to namespace
VETAPP.namespace('VETAPP.pages.vsearch');

// create page
VETAPP.pages.vsearch = (function() {

	var self = {};

	self.init = function init() { ///console.log('init page: vsearch.js');

		var self = this;
		
		self.isMobile = window.orientation || Modernizr.touch;
		self.container = $('.main-container');
		
		// init Modules
		self.initModules();
		// Bind events
		self.bindEvents();
	}


	self.initAngular = function(){
   	var vsearch = angular.module('vsearch', ['ngSanitize']);

   	vsearch.controller('vSearchCtrl', function($scope, $http){
   		//trigger search api call if the model is not ''
         var model = window.model || 'a';
         //vsearchendpoint = '/api/Search/' + model;
         $scope.query = model;
         vsearchendpoint = '/api/Search/' + model;

            $http({method: 'GET', cache: true, url: vsearchendpoint}).
            success(function(data, status, headers, config){
               var searchSorted = {};
               var headers = {};

               ///console.log(data);
               
               //organize data by course id
               //TODO: SORT SO THAT THE MODULES WITH KEYWORD IN TITLE APPEAR ABOVE MODULES THAT HAVE MATCHES IN BODY - custom sorting function
               angular.forEach(data, function (value, key) {
                   if (typeof searchSorted[value.module.courseid] !== 'undefined') {
                       if (typeof searchSorted[value.module.courseid][key] !== 'undefined') {
                           searchSorted[value.module.courseid][key] = value.module;
                       } else {
                           searchSorted[value.module.courseid][key] = [];
                           searchSorted[value.module.courseid][key] = value.module;
                       }
                   } else {
                       searchSorted[value.module.courseid] = {};
                       if (typeof searchSorted[value.module.courseid][key] !== 'undefined') {
                           searchSorted[value.module.courseid][key] = value.module;
                       } else {
                           searchSorted[value.module.courseid][key] = [];
                           searchSorted[value.module.courseid][key] = value.module;
                       }
                   }
                  headers[value.module.courseid] = value.coursename[0];
               });

               //send search results to scope for rendering
               $scope.results = searchSorted;
               $scope.headers = headers;
               $scope.modulesNum = data.length;
               $scope.coursesNum = _.size(headers);
            }).error(function(data, status, headers, config){
               ///console.log(data);
            })

      		

      }).filter('highlight', function () {
        return function (text, search, caseSensitive) {
          if (search || angular.isNumber(search)) {
            text = text.toString();
            search = search.toString();
            if (caseSensitive) {
              return text.split(search).join('<span class="highlight">' + search + '</span>');
            } else {
              return text.replace(new RegExp(search, 'gi'), '<span class="highlight">$&</span>');
            }
          } else {
            return text;
          }
        };
      });

      angular.bootstrap(document, ['vsearch']);
	}

  self.initMobile = function(){
    $(document).ready(function(){

    });
  }

	self.initModules = function()
	{
		var self = this;

		self.initAngular();
    self.initMobile();

	}
	self.bindEvents = function()
	{	
	    var self = this;

      $('.mobile-search-btn').click(function(e){
        e.preventDefault();

        //show loader
        $('.loader').show();
        
        var keyword = $('#mobile-search-input').val();

        if(keyword != ''){
          var vsearchendpoint = '/api/Search/' + keyword ;
          $.ajax({
            url: vsearchendpoint,
            format: JSON,
            success: function(data){
              var searchSorted = {};
              var headers = {};
              var courseType = {}
               ///console.log(data);
               
               //organize data by course id
               //TODO: SORT SO THAT THE MODULES WITH KEYWORD IN TITLE APPEAR ABOVE MODULES THAT HAVE MATCHES IN BODY - custom sorting function
               _.each(data, function (value, key, list) {
                   if (typeof searchSorted[value.module.courseid] !== 'undefined') {
                       if (typeof searchSorted[value.module.courseid][key] !== 'undefined') {
                           searchSorted[value.module.courseid][key] = value.module;
                       } else {
                           searchSorted[value.module.courseid][key] = [];
                           searchSorted[value.module.courseid][key] = value.module;
                       }
                   } else {
                       searchSorted[value.module.courseid] = {};
                       if (typeof searchSorted[value.module.courseid][key] !== 'undefined') {
                           searchSorted[value.module.courseid][key] = value.module;
                       } else {
                           searchSorted[value.module.courseid][key] = [];
                           searchSorted[value.module.courseid][key] = value.module;
                       }
                   }
                  headers[value.module.courseid] = value.coursename[0];
                  courseType[value.module.courseid] = value.coursetypeid[0];
               });


               //send search results to scope for rendering
               $('.loader').hide();
               var results = searchSorted;
               var modulesNum = data.length;
               var coursesNum = _.size(headers);
               $('#num-modules').text(modulesNum);
               $('#num-courses').text(coursesNum);
               $('#search-keyword').text(keyword);
               $('.result-meta').show();
               $('.results-header').show();
               var _html = '';

              ///console.log(results);
               //apply results to page
               var courseTypeClass = '';
               _.each(results, function(value, key, list){
                  switch(courseType[key]){
                    case 1:
                      courseTypeClass = 'bi';
                      break;

                    case 2:
                      courseTypeClass = 'pd';
                      break;

                    case 3:
                      courseTypeClass = 'fb';
                      break;

                  }
                  _html += '<li class="result-item">';
                  _html += '<p class="course-title course-fb ' + courseTypeClass + ' uppercase">';
                  _html += '<strong><a class="' + courseTypeClass + '" href="#">' + headers[key] + '</a></strong>';
                  _html += '</p>';
                  _.each(value, function(v, k, l){
                    _html += '<div class="course-results-wrap">';
                    _html += '<div class="module-result">';
                    _html += '<p class="module-result-header ' + courseTypeClass + '"><strong><span class="module-num">Module ' + v.sequence + ':</span> <a class="' + courseTypeClass + '" href="/module/' + v.moduleid + '"><span>' + v.modulename + '</span></a></strong></p>';     
                    _html += '<p><span class="module-subhead">Scenario</span> <span>' + v.moduledescription + '</span></p>'
                    _html += '<p><span class="module-subhead">Transcript</span> <span>' + v['videos'][0]['transcript'].substring(0,150) + '...</span></p>';
                    _html += '</div></div>';
                  });  
               });

                //insert into page
                $('.search-results-mobile-wrap').find('.search-results').html(_html);
            },
            failure: function(){
              ///console.log('Search failed.');
            }
          });
        }else{
          alert('Please enter a keyword to search');
          $('.loader').hide();
        }

        
      });
	}

	return self;

}());

angular.element(document).ready(function(){
    VETAPP.pages.vsearch.init();	
});
