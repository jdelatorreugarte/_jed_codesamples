/**
 * Class VETAPP.Sitenav
 * author: 	Jonathan Schmidt
 * http://lifeisinteractive.com
**/

// add to name space
VETAPP.namespace('VETAPP.Classes');

// define class
VETAPP.Classes.Sitenav = Class.extend({

	init : function init(options, elem)  {  ///console.log('init: Class VETAPP.Sitenav loaded');
		
		this.isMobile 	= window.orientation || Modernizr.touch;		
		this.options 	= $.extend({}, this.options, options);

		// cache elems
		this.siteNav 	= elem;		
		this.page 		= $(this.options.page).attr('class');
		// render
		this.render();
		this.bindEvents();
	},
	
	options : {},
	
	render : function()
	{
	    var self = this;
	    self = self.page.substring(0, self.page.indexOf('-container'));
	    // set active nav item
		switch (self) {

			case 'index':
			$(siteNav).find('.home img').attr('src', '/contents/imgs/home-active.png');
			$('.home').addClass('active');
			break;

		    case 'advanced':
			$(siteNav).find('.advanced').addClass('active');
			break;

		    case 'courselist':
		    $(siteNav).find('.advanced').addClass('active');
		    break;

		    case 'courselanding':
		    $(siteNav).find('.advanced').addClass('active');
		    break;

		    case 'module-container':
		    $(siteNav).find('.advanced').addClass('active');
		    break;

		    case 'connect':
			$(siteNav).find('.connect').addClass('active');
			break;
			
		    case 'connect-browse':
		    $(siteNav).find('.connect').addClass('active');
		    break;

			case 'job':
			$(siteNav).find('.search').addClass('active');
			break;

		    case 'whoweare':
		        $(siteNav).find('.whoweare').addClass('active');
			break;
		}
	},

	bindEvents : function()
	{
		var self = this;
		var navItems = siteNav.find('a');
		var btnHome	= siteNav.find('.home');

		// add mouseover for home button
		if (self.page !== 'index-container')
		{
			btnHome.on({
				mouseenter: function(){
					$(this).find('img').attr('src', '/contents/imgs/home-active.png');
				},
				mouseleave: function(){
					$(this).find('img').attr('src', '/contents/imgs/home.png');
				}
			});			
		}

		// check nav constraints
		navItems.click(function(e){
			//e.preventDefault()
			/////console.log(this);
		});

	}
});
