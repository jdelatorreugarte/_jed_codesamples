// create dictionary object
var dictionary = {};
$.ajax({
    url: "/api/Dictionary",
    format: JSON
}).done(function (data) {
    //console.log(data);

    $.each(data, function (_key) {
        // add key:value
        dictionary[data[_key].keyword] = data[_key].value;
    });
});

// add to name space
VETAPP.namespace('VETAPP.Classes');

// define class
VETAPP.Classes.Popover = Class.extend({

	init : function init(options, elem)  {  //console.log('init: Class VETAPP.Popover loaded');
		
		this.isMobile 	= window.orientation || Modernizr.touch;		
		this.options 	= $.extend({}, this.options, options);

		// set parent container
		this.parent	= elem;
		// set popover type (interest / speciality / ... )
		this.type 	= this.options.type;
		// set popover button
		this.btnId 	= this.options.btnId;
		// set popover direction
		this.direction	= this.options.direction;
		// popover template
		this.template = {};
		// output data
		this.output = this.options.output;

		// render
		this.render();
		this.bindEvents();
	},
	
	options : {},
	
	render : function()
	{
		var self = this;

		switch (self.type) {
			case 'interest':
			self.template = $("#popover-interest-template").html();
			break;

			case 'specialty':
			self.template = $("#popover-specialty-template").html();				
			break;

			case 'affiliations':
			self.template = $("#popover-affiliations-template").html();			
			break;
		}

		// build popover
		self.buildPopover(self.btnId);
	},

	bindEvents : function()
	{
		var self = this;
		var doc = $(document);
		var btnPO = $(self.parent).find('#'+self.btnId);

		// bind popover buttons
		btnPO.click(function(e){
			e.preventDefault();	
			$(this).popover('show');
		});

		doc.on('click', '.tab-nav', function(e){
			// switch tabs			
			if (! $(this).hasClass('active')){
				// set tabs
				$('.tab-nav').removeClass('active').removeClass('inactive');
				$(this).addClass('active');

				// set field list
				switch (this.id) {

					case 'tab-state':
					$('#tab-national').addClass('inactive');
					$('.nationallist').removeClass('active').addClass('inactive');
					$('.statelist').addClass('active');
					// hide nav
					$('.navigation li').hide();	
					break;

					case 'tab-national':
					$('#tab-state').addClass('inactive');
					$('.statelist').removeClass('active').addClass('inactive');
					$('.nationallist').addClass('active');
					// show nav
					$('.navigation li').show();
					break;
				}
				// reset nav
				//$('.navigation li').removeClass('active');
				//$('.navigation li.all').addClass('active');
				// show all
				$('.fieldlists ul').addClass('active');
			}
		});

		doc.on('click', '.navigation li', function(e){			
			// close active fieldset
			if (! $(this).hasClass('active')){
				// clear active nav
				$('.navigation li').removeClass('active');
				// clear active set
				$('.fieldset').removeClass('active');
				// show dataset
				if ($(this).hasClass('all'))
					$('.fieldlists ul').addClass('active');
				else
					$('.fieldlists').find('.'+ $(this).attr('class')).addClass('active');

				// set active nav
				$(this).addClass('active');
			}
		});

		// close popover
		doc.on('click', '.btn-close-popover', function(e){
			$('#'+self.btnId).popover('hide');
		});

		// set organizations
		doc.on('click', '.btnOrganization', function(e){
			e.preventDefault();			
			
			if (self.parent.hasClass('connect-container')){				
				// set organization data				
				VETAPP.pages.connect.organizationid = $(this).attr('data-id');
				VETAPP.pages.connect.organizationStr = $(this).attr('data-str');
				// call build
				VETAPP.pages.connect.buildSearchStr();
				VETAPP.pages.connect.updateAffiliation();

			} else {
				// update user profile page
				VETAPP.pages.profiles.organizationid = $(this).attr('data-id');
				VETAPP.pages.profiles.organizationStr = $(this).attr('data-str');
				VETAPP.pages.profiles.updateNewOrganization();
				$('#'+self.btnId).popover('hide');
			}

		});

		// add icheck to interest popover
		btnPO.on('shown.bs.popover', function () {
			// add iCheck plugin 			
			$('.popover-content .icheck').iCheck({
				checkboxClass: 'icheckbox_minimal-orange'			
			});
			// prepop check boxes from list			
			var items = $.each( $(self.output +' li'), function(key, val){		
				$('.popover-content input[name='+ $(val).attr('data-tag') +']').iCheck('check');
			});
		})

		doc.on('click', '.btnSelectItems', function(e){
			e.preventDefault();
			switch ($(this).attr('data-popover')) {
				case 'interest':
				($(this).attr('data-type') == 'all') ? $('#form-interest').iCheck('check') : $('#form-interest').iCheck('uncheck');
				break;

				case 'specialty':
				($(this).attr('data-type') == 'all') ? $('#form-specialty').iCheck('check') : $('#form-specialty').iCheck('uncheck');				
				break;
			}
		});

		// save popover
		doc.on('click', '.btn-save-popover', function(e) {			
			// check type
			if ( this.id == 'btnInterestSave' && self.type == 'interest') {
				// get form data
				var form = $('#form-interest').serialize();
				// build tag list
				self.buildTagList(form, function(){
					// close popover
					btnPO.popover('hide');
				});
			}

			else if ( this.id == 'btnSpecialtySave' && self.type == 'specialty') {
				// get form data
				var form = $('#form-specialty').serialize();
				// build tag list
				self.buildTagList(form, function(){
					// close popover
					btnPO.popover('hide');
				});
			}
		});
	},

	buildPopover : function(id)
	{
		var self = this;		

		// set template source
		var source = self.template;

		// Compile Handlebars template
		var template = Handlebars.compile(source);
		
		// check type
		if (self.type == 'affiliations') {
			// get data
	      $.ajax({
	          url: "/api/Organizations/",          
	          format: JSON
	      }).done(function (data) {
	      	//stuff org data info filter-packets
	      	var filtered = {};
	  		filtered.all = data; //all
	  		filtered.general = _.filter(data, function(item){
	  			return item.organizationtypename == "General";
	  		}); //general
	  		filtered.specialists = _.filter(data, function(item){
	  			return item.organizationtypename == "Specialists";
	  		});	//specialist
	  		filtered.species = _.filter(data, function(item){
	  			return item.organizationtypename == "Species-specific";
	  		});	//species-specific
	  		filtered.specialinterest = _.filter(data, function(item){
	  			return item.organizationtypename == "Special Interest";
	  		});	//special interest
	  		filtered.student = _.filter(data, function(item){
	  			return item.organizationtypename == "Student";
	  		});	//student

	  		//local
	  		filtered.local = _.filter(data, function(item){
	  			return item.local === true;
	  		});

	      	///console.log(filtered);
	      	// pass data to template
	        var html = template(filtered);

					// Set popover options
					var options = {
						animation: false,
						html: true,
						placement: self.direction,
						trigger: 'manual',
						container: '.popover-container',
						content: html
					}
					// init popover
					$('#'+id).popover(options);
	      });

		} else {
			// Render html
			var html = template();

			// Set popover options
			var options = {
				animation: false,
				html: true,
				placement: self.direction,
				trigger: 'manual',
				container: '.popover-container',
				content: html
			}
			// init popover
			$('#'+id).popover(options);			
		}
	},

	buildTagList : function(data, cb)
	{
		var self = this;
		var items = data.split('&');
		var output = '';
		var interests = [];
		var _val = '';

		for (var x=0; x< items.length; x++ ){
		    if (items[x] !== '')
		        _val = items[x].replace('=on', '');
				output += '<li data-tag="'+ _val +'"><span>'+ dictionary[_val] +'</span><a data-tag="'+ _val +'" href="#" class="removetag '+ self.type +'">X</a></li>';
				interests.push(items[x].replace('=on', ''));				
		}
		
		// output selected items list		
		var _out = $(self.parent).find(self.output);

		// display output
		_out.html(output);
		
		// update controller
		if (self.parent.hasClass('connect-container')){
			// set interests
			VETAPP.pages.connect.interests = interests;
			// call build
			VETAPP.pages.connect.buildSearchStr();

		} else {

			// update user profile page
			switch($(this).attr('btnId')){
				case 'btnInterest':
				VETAPP.pages.profiles.interests = interests;
				VETAPP.pages.profiles.saveInterest();
				break;
				case 'btnSpecialty':
				VETAPP.pages.profiles.specialty = interests;
				VETAPP.pages.profiles.saveSpecialty();
				break;
			}
		}

		// callback
		cb.call();
	}
});