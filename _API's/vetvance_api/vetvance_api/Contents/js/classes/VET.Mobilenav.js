/**
 * Class VETAPP.Mobilenav
 * author: 	Evan Rose
 * http://evanmrose.co
**/

// add to name space
VETAPP.namespace('VETAPP.Classes');

// define class
VETAPP.Classes.Mobilenav = Class.extend({

	init : function init(options, elem)  {  //console.log('init: Class VETAPP.Mobilenav loaded');
		
		this.isMobile 	= isMobile();		
		this.options 	= $.extend({}, this.options, options);
		this.navState   = {
			topLevelPageWrap: $('#' + $(this.options.page).attr('data-mobiletitle') + '-container'),
			topLevelPage:     $(this.options.page).children().first(),
			currentPage:      $(this.options.page),
			backTarget:       [],
			backLink:         $('.mobile-nav-back'),
			pageTitle:        $(this.options.page).attr('data-mobiletitle'),
			pageIcon:         $(this.options.page).attr('data-mobileicon')
		}
		this.user        = {
			isLoggedIn   : userid || 0,
			data         : this.getUserData(userid)
		}
		this.topNav            = $('.nav-mobile');
		this.topNavIcon        = $('.nav-icon');
		this.profileIcon       = $('.mobile-pic');
		this.mobileProfileContainer  = $('.mobile-profile-container');
		this.modSrc = '';
		// cache elems
		this.mobileNav 	= elem;	

		//set page headers
		if(this.navState.pageIcon == 'none')
			$('.page-title').html(this.navState.pageTitle);
		else{
			$('.page-title').html('<img src="/Contents/imgs/' + this.navState.pageIcon + '">' + this.navState.pageTitle);
		}

		//page header position change for long titles
		if(this.navState.pageTitle == "Register and Save"){
			$('.page-title').css('left', '28%');
		}else{
			$('.page-title').css('left', '38%');
		}

		//prep profile connections
		this.initProfileConnections();

		// render
		this.render();
		this.bindEvents();

		function isMobile(){
			//check browser size, anything below 768 we consider 'mobile'
			if($(window).width() <= 768){
				return true;
			}else{
				return false;
			}
		}

		
	},

	initProfileConnections: function(){
		var self = this;
        $.ajax({
            url: '/api/NewConnections/' + userid,
            // data: { key: val },
            dataType: "json",
            async: false
        }).success(function (data) {
            //console.log(data);

            if (data != null) {
                // new connections
        		_html = '<img class="mobile-profile-nav-arrow" src="/Contents/imgs/mobile/profile-dropdown-arrow.png" />';
                _html += '<li style="color: 585858 !important;"><strong>You have ' + (data.length) + ' new connections</strong></li>';
               	var i = 0;
                _.each(data, function(value, key, list){
                	if(i < 2){
                		//_html += '<li><img src="/contents/avatars/' + data[i]['user'].avatar + '" alt="" height="32" class="img-circle" />' + data[i]['user'].firstname + ' ' + data[i]['user'].lastname + '</li>'; // profile is not linked
	                    _html += '<li><a class="userlink" href="#"><div style="float: left; margin: 5px;"><img style="height: 50px !important; width: 50px !important;" src="/contents/avatars/' + data[i]['user'].avatar + '" alt="" height="50" class="img-circle"></div><div style="float: left;"><p style="margin: 0 !important; font-weight: bold;">' + data[i]['user'].firstname + ' ' + data[i]['user'].lastname + '</p><p style="margin: 0; color: black; font-size: 11px;">' + data[i]['user'].profession + '</p><p style="margin: 0; color: black; font-size: 11px;">' + data[i]['user'].school + '</p><p style="margin: 0;color: black; font-size: 11px;">' + data[i]['user'].city + '</p></div></a></li>';
	                    _html += '<li class="divider"></li>';
	                    // _html += '<li><a class="userlink purple" href="#"><img src="/contents/avatars/' + data[i]['user'].avatar + '" alt="" height="50" class="img-circle" />' + data[i]['user'].firstname + ' ' + data[i]['user'].lastname + '</a></li>'
                	}else{
                		//do nothing
                	}

                	i++;
                	
                });

                if (data.length > 2) { // if there are more than 2
                    _html += '<li class="menuitem more"><a class="userlink" href="/connect">See New Connections <img class="arrow-r pull-right" style="width: 15px !important;" src="/Contents/imgs/mobile/arrow-r.png" /></a></li>'; //' + (data.length - 2) + ' more...
                    _html += '<li class="divider"></li>';
                }

                //add fixed pieces
                _html += '<li class="divider"></li><li class="menuitem white purple"><a style="font-weight: normal !important; color: #161b8e !important;" role="menuitem" tabindex="-1" href="/UserProfile">My Profile</a></li><li class="divider"></li><li class="menuitem white purple" style="font-weight: normal !important; color: #161b8e !important;"><a style="font-weight: normal !important; color: #161b8e !important;" role="menuitem" tabindex="-1" href="/process/authenticate/?redirect=/">Sign out</a></li>';
                $('ul.dropdown-menu.user.mob-pro-dd').html(_html); //+ $('ul.dropdown-menu.user.mob-pro-dd').html());
            }
        });
		//console.log('Outside');
	},

	getUserData: function(userId){
		var self = this;
		if(!_.isUndefined(userId) && userId != 0){
			//swap homepage video for placeholder image
			$('.index-video').html('<center><img style="width:100%;" src="/Contents/imgs/mobile/index-img-mobile.png" /></center>');

			//query http://vetvance-api.dev.thebloc.com/api/Users/1070
			$.ajax({
	    		url: "/api/Users/" + userId,
				format: JSON,
				success: function(data){
					//console.log(data);
					//set header logged in info
					$('.login-mobile').hide();

					//show user profile
					$('.mobile-pic').html('<img class="img-circle" style="width:20px; height: 20px;" src="/Contents/avatars/' + data[0].avatar + '" />');
					$('.mobile-name').text(data[0].firstname);
					$('.logged-in').show();

					if(self.navState.pageTitle == "Connect"){
						//insert proper data into connect main page
						$('.user-graduation-year').text(data[0].graduation);
					}
					//return data
					return data[0];
				},
				failure: function(){
					//console.log('Could not get user data');
				}
			});
		}else{
			//console.log('User is not logged in');
			//
		}
	},

	changePage: function(target, backTo, transitionType, elem){
		var self = this;

		//allow to trigger changePage using only element attributes
		if(elem != '' && elem != null && !_.isUndefined(elem) && target == '' && backTo == ''){
			var $elem = $(elem);
			//data-parentpageid="new-connections" data-target="connect-details"
			if($elem.attr('data-parentpageid'))
			self.navState.currentPage = $('#' + $elem.attr('data-parentpageid'));
			self.navState.currentPage.hide();
			backTo = self.navState.currentPage;

			self.navState.currentPage = $('#' + $elem.attr('data-target'));
			self.navState.currentPage.removeClass('slide in out reverse');

			//set transitiontype
			transitionType = $elem.attr('data-transition') || transitionType;
		}else{
			//hide current page
			self.navState.currentPage.hide();

			//**PRE TRANSITION***
			if(typeof target == "string") {
				if(target.indexOf('#') >= 0){
					//has pound
				}else{
					target = '#' + target;
				}
			}
			
			if(typeof backTo == "string"){
				if(backTo.indexOf('#') >= 0){
					//has pound
				}else{
					backTo = '#' + backTo;
				}
			}
			
			
			self.navState.currentPage = $(target);

			//remove transitions
			self.navState.currentPage.removeClass('slide in out reverse');
		}
		
		//slide new element in
		switch(transitionType){
			case 'slide-forward':
				$(backTo).addClass('slide out').hide().removeClass('slide out');
				self.navState.currentPage.show().addClass('slide in');
				//set backTarget but only if we are going forward
				self.navState.backTarget.push($(backTo));
				break;

			case 'slide-back':
				$(backTo).addClass('slide out').hide().removeClass('slide out');
				self.navState.currentPage.show().addClass('slide in reverse');
				break;

			default:
				self.navState.currentPage.fadeIn();
				break;
		}

		//**POST TRANSITION

		
		self.checkBtnState();

		// //reset slidelinks
		// $('.slide-link').click(function(){
		// 	var elem = this;
		// 	self.changePage('', '', '', elem);
		// });
		
		
	},

	back: function(){
		var self = this;
		if(self.navState.backTarget.length > 0){
			if(self.navState.currentPage.hasClass('top-level'))
				self.navState.backTarget = [];
			
			//remove animation triggers
			_.each(self.navState.backTarget, function(value,key,list){
				value.removeClass('slide in out reverse');
			});
			//last page
			var currentLast = self.navState.backTarget.pop();
			
			//set new backto
			var newLast = _.last(self.navState.backTarget) || null;
			
			self.changePage(currentLast, newLast, 'slide-back', '');
		}else{
			//console.log('Back target is undefined');
		}
		//var backString = 'Back to: ' + current
		//console.log('back');

		self.checkBtnState();
	},

	checkBtnState: function(){
		var self = this;
		//make sure page is not parent page, if it is, set the back target to null
		if(self.navState.currentPage.hasClass('top-level')){
			self.navState.backTarget = [];
		}

		if(self.navState.backTarget.length == 0){
			self.navState.backLink.hide();
		}else{
			self.navState.backLink.show();
		}
	},
	
	render : function()
	{
		var self = this;
	},

	bindEvents : function()
	{
		var self = this;
		this.topNavIcon.unbind('click').click(function(e){
			e.preventDefault();
			var $navIcon = $(this);
			var $vidCont = $('.index-video');
			//only need to do this if the user is logged, out, otherwise, use a placeholder image
			if(_.isUndefined(userid) || userid == 0 || !userid || userid == ""){
				if($vidCont.hasClass('img-placeholder')){
					$('.index-video').html('<video id="promo-video-mobile" class="video-js vjs-default-skin" controls preload="auto" width="100%" poster="/Contents/imgs/promoposter.jpg"><source src="http://brightcove04.brightcove.com/23/2342494722001/201311/193/2342494722001_2862001243001_VetVancePromotionalVideo102913.mp4" type="video/mp4"></video>');
					$vidCont.removeClass('img-placeholder');
				}else{
					$('.index-video').html('<img src="/Contents/imgs/promoposter.jpg" style="width:100%; height: auto;" />');
					$vidCont.addClass('img-placeholder');
				}
			}else{
				//do nothing, there is no video to hide and show
			}
			

			var $vidContWho = $('.whoweare-video-mobile-wrap');
			if($vidContWho.hasClass('img-placeholder')){
			    $('.whoweare-video-mobile-wrap').html('<video id="whoweare-video-mobile" class="video-js vjs-default-skin" controls preload="auto" width="100%" poster="/Contents/imgs/promoposter.jpg"> <source src="/Contents/videos/HOMEPAGEVIDEO.mp4" type="video/mp4"></video>');
				$vidContWho.removeClass('img-placeholder');
			}else{
				$('.whoweare-video-mobile-wrap').html('<img src="/Contents/imgs/promoposter.jpg" style="width:100%; height: auto;" />');
				$vidContWho.addClass('img-placeholder');
			}

			var $vidContMod = $('#mod-video-wrap');
			//get source of that video
			if(self.modSrc == '')
				self.modSrc = $vidContMod.find('source').attr('src');
			if($vidContMod.hasClass('img-placeholder')){
				$('#mod-video-wrap').html('<video id="promo-video-mobile-mod" class="video-js vjs-default-skin" controls preload="auto" width="100%" poster="/Contents/imgs/promoposter.jpg"> <source src="' + self.modSrc + '" type="video/mp4"> </video>');
				$vidContMod.removeClass('img-placeholder');
				//initialize video
				videojs('promo-video-mobile-mod').ready(function(){
					var videoObj = this;
                    // Player (this) is initialized and ready.
                    var duration = this.duration();

                    var completionTriggered = false;

                    //set up event listener for 'timeupdate' event
                    this.on('timeupdate', function (data) {
                        //pull time
                        var vTime = this.currentTime();
                        var durationTrigger = 90;
                        //if time >= 90s trigger switch to resources. 
                        if (vTime >= durationTrigger) {
                            if (!completionTriggered) {
                                //trigger api call to complete module
                                triggerCompletion();
                                completionTriggered = true;
                            }

                        }
                    });

                    this.on('ended', function () {
                        //trigger completion
                        triggerCompletion();
                    });

                    //pause if tab nav is triggered
                    $('.module-tab-mobile').click(function(){
                        videoObj.pause();
                    });
				});

				function triggerCompletion() {
				    //console.log('1. triggerCompletion module=' + module_id + " " + user_id);

		            //trigger api call to complete module
		            var user_id = window.userid || 0; //prevent course completeion if not logged in
		            var module_id = window.model || 0;
		            var completeEndpoint = '/API/MarkModuleComplete/' + module_id + '?userid=' + user_id;
		            $http({ method: 'GET', url: completeEndpoint }).
		            success(function (data, status, headers, config) {
		                //console.log(data);
		            }).error(function(data, status, headers, config){
		                //console.log(data);
		            });
		        }
			}else{
				$('#mod-video-wrap').html('<img src="/Contents/imgs/promoposter.jpg" style="width:100%; height: auto;" />');
				$vidContMod.addClass('img-placeholder');
			}
			
			$('.nav-mobile ul').toggle();
			$('.mobile-nav-bg').toggle();

			//toggle class for menu icon
			if($navIcon.hasClass('navIconActive')){
				//turn off
				$navIcon.removeClass('navIconActive');
				$navIcon.find('img').attr('src', '/Contents/imgs/mobile/menu.png');
			}else{
				//turn on
				$navIcon.addClass('navIconActive');
				$navIcon.find('img').attr('src', '/Contents/imgs/mobile/menu-active.png');
			}
			
		})

		this.profileIcon.unbind('click').click(function(e){
			e.preventDefault();

			//only open if nav isnt open
			self.mobileProfileContainer.toggle();
			$('.mobile-profile-bg').toggle();

		})

		//add js active button state
			if(self.isMobile){
				if(Modernizr.touch){
					$('li').on('touchstart', function(){
				        $(this).addClass("mobactive");
				    }).on('touchend', function(){
				        $(this).removeClass("mobactive");
				    });

				    $('.presenter-link').on('touchstart', function(){
				        $(this).addClass("mobactive");
				    }).on('touchend', function(){
				        $(this).removeClass("mobactive");
				    });

				    $('a.nostyle').on('touchstart', function(){
				        $(this).addClass("mobactive");
				    }).on('touchend', function(){
				        $(this).removeClass("mobactive");
				    });

				    //specific to index page
				    $('.index-list').find('li').on('touchstart', function(){
				    	//show white image
				    	$(this).find('.index-icon').hide();
				    	$(this).find('.index-icon-active').show();

				    	//show white arrow
				    	$(this).find('.arrow-r').hide();
				    	$(this).find('.arrow-r-white').show();
				    }).on('touchend', function(){
				    	//hide white image
				    	$(this).find('.index-icon-active').hide();
				    	$(this).find('.index-icon').show();

				    	//hide white arrow
				    	$(this).find('.arrow-r').show();
				    	$(this).find('.arrow-r-white').hide();
				    });
				}else{
					$('li').on('mousedown', function(){
				    	$(this).addClass("mobactive");
				    }).on('mouseup', function(){
				    	$(this).removeClass("mobactive");
				    });

				    $('.presenter-link').on('mousedown', function(){
				    	$(this).addClass("mobactive");
				    }).on('mouseup', function(){
				    	$(this).removeClass("mobactive");
				    });

				    $('a.nostyle').on('mousedown', function(){
				    	$(this).addClass("mobactive");
				    }).on('mouseup', function(){
				    	$(this).removeClass("mobactive");
				    });

				     //specific to index page
				    $('.index-list').find('li').on('mousedown', function(){
				    	//show white image
				    	$(this).find('.index-icon').hide();
				    	$(this).find('.index-icon-active').show();

				    	//show white arrow
				    	$(this).find('.arrow-r').hide();
				    	$(this).find('.arrow-r-white').show();
				    }).on('mouseup', function(){
				    	//hide white image
				    	$(this).find('.index-icon-active').hide();
				    	$(this).find('.index-icon').show();

				    	//hide white arrow
				    	$(this).find('.arrow-r').show();
				    	$(this).find('.arrow-r-white').hide();
				    });
				}
			}
		if(userid > 0){
			$('.hide-if-logged-out').show();
			$('.show-if-logged-in').show();
		}else{
			$('.hide-if-logged-out').hide();
			$('.show-if-logged-in').hide();
		}
		


	}
});
