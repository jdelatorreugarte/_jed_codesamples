/**
 * Class VETAPP.Sitenav
 * author: 	Jonathan Schmidt
 * http://lifeisinteractive.com 
**/

// add to name space
VETAPP.namespace('VETAPP.Classes');

// define class
VETAPP.Classes.Connect = Class.extend({

	init : function init(options, elem)  {  //console.log('init: Class VETAPP.Connect loaded');
		
		this.isMobile 	= window.orientation || Modernizr.touch;		
		this.options 	= $.extend({}, this.options, options);

		// keep track of rendered modal templates
		this.modals = [];

		// modal template
		this.template = {};

		// render
		this.render();
		this.bindEvents();
	},
	
	options : {},
	
	render : function()
	{
		var self = this;
	},

	openModal : function(connectid)
	{

	    TracktSimple.track("Connect","ConnectButton","--");
		var self = this;
		var url = '/api/ConnectionInfo/';

		// check if modal was rendered
		if ( _.findWhere(self.modals, {id: connectid}) == undefined) {

			// get user data
			$.ajax({
				url: url + userid +'?connectid='+ connectid,
				format: JSON
			}).done(function (data) {

				// get connections
				self.populateConnections(connectid, 'mutual');
				self.populateConnections(connectid, 'vet');

				// push connected val to user
				var dataset = {
					user: data[0]['connection']
				};
				dataset.user.connected = data[0]['connected'];
				//console.log(dataset);

				// build modal 
				self.buildModal(dataset, connectid, function(){					
					$('#connectPersonModal'+ connectid).modal();
				});
			});

		} else {
			// open rendered modal
			$('#connectPersonModal'+ id).modal();
		}		
	},

	buildModal : function(data, id, cb)
	{
		var self = this;		

		// parse affiliations
		Handlebars.registerHelper('listAffiliations', function(context, options)
		{			
		  	if (context.organizationname !== undefined) {
		  		ret = '<li>'+ context.organizationname +'</li>';
		  		return new Handlebars.SafeString(ret);
		  	}
		});

		// parse interests
		Handlebars.registerHelper('listInterests', function(context, options)
		{		
			var ret = '';
			$.each(context, function(key, val){
				if (val == true)
					ret += "<li>"+ key + "</li>";
					//console.log(key);
					//console.log(val);
			});

			return new Handlebars.SafeString(ret);
		});

		// button helper
		Handlebars.registerHelper('buttons', function(connected, userid)
		{
			var out = '<div class="buttons pull-left">'
							+'<a href="#" data-userid="'+ userid +'" class="btn btn-default btn-mini btn-connect">connect</a>'
							+'<a href="#" data-userid="'+ userid +'" class="btn btn-default btn-mini btn-disconnect hidden">disconnect</a>'
						+'</div>';

			if (connected) {
				out = '<div class="buttons pull-right">'
							+'<a href="#" data-userid="'+ userid +'" class="btn btn-default btn-mini btn-connect hidden">connect</a>'
							+'<a href="#" data-userid="'+ userid +'" class="btn btn-default btn-mini btn-disconnect">disconnect</a>'
						+'</div>';
			}

			return new Handlebars.SafeString(out);				
		});

		// Load template
		var source = $("#modal-connect-template").html();

		// Compile Handlebars template
		var template = Handlebars.compile(source);
		
		// Render html
		var html = template(data);

		// create modal storage array
		var modal = {
			id: id,
			html: html
		}

		// push modal to array
		self.modals.push(modal);

		// Add modal to dom
		$('.modal-connect-container').append(html);
		
		// Callback		
		cb.call();
	},

	populateConnections : function(connectid, type)
	{
		var self = this;
		var url = '';
      
      switch (type) {
      	case 'mutual':
      		url = '/api/MutualConnections/'+ userid +'?connectid='+ connectid;
      		elm = '.mutal-connections';
      		break;

      	case 'vet':
      		url = '/api/UserConnections/'+ connectid +'?sort=default';
      		elm = '.vet-connections';
      		break;
      }

      // populate connections
      $.ajax({
          url: url,          
          format: JSON
      }).done(function (data) {      	
         // parse user data
         var connections = '';
         $.each(data, function(index, obj){         		
         		connections += '<li><img src="/contents/avatars/'+ obj.user.avatar +'" alt="" class="img-circle user-avatar" /></li>'
         });
         // inject connections to modal
         $('#connectPersonModal'+ connectid).find(elm).html(connections);         
      });
	},

	btnConnect : function(connectid, action)
	{	
		var self = this;		
        //console.log(this);
		var url = '/api/Connect/';
		if (action == 'disconnect')
		 	url = '/api/Disconnect/';

		$.ajax({
			url: url + userid +'?connectid='+ connectid,
			format: JSON
		}).done(function (data) {				
				// update dom buttons
				var buttons = $('#connectPersonModal'+ connectid).find('.buttons');
				if (action == 'disconnect'){
					$(buttons).removeClass('pull-right').addClass('pull-left');
					$(buttons).find('.btn-disconnect').addClass('hidden');
					$(buttons).find('.btn-connect').removeClass('hidden');
				} else {
					$(buttons).removeClass('pull-left').addClass('pull-right');
					$(buttons).find('.btn-connect').addClass('hidden');
					$(buttons).find('.btn-disconnect').removeClass('hidden');					
				}			
		});
	},

	bindEvents : function()
	{
		var self = this;
		var doc = $(document);

		// connect btn
		doc.on('click', '.btn-connect', function(e){
			e.preventDefault();
			var id = $(this).attr('data-userid');
			self.btnConnect(id, 'connect');
		});
		// disconnect
		doc.on('click', '.btn-disconnect', function(e){
			e.preventDefault();
			var id = $(this).attr('data-userid');
			self.btnConnect(id, 'disconnect');
		});
		// open modal
		$(doc).on('click', '.btnConnect', function(e){
			var id = $(this).attr('data-id');
			self.openModal(id);
		});
		// close modal
		doc.on('click', '.close-modal', function(e){
			e.preventDefault();
			$('#connectPersonModal'+ $(this).attr('data-id')).modal('hide');
		});
	}
});
