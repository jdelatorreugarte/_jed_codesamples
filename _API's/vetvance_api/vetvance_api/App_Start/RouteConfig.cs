﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace vetvance_api
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Prototype",
                url: "Prototype/{action}/{id}",
                defaults: new { controller = "Prototype", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Email",
                url: "Email/{action}/{id}",
                defaults: new { controller = "Email", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Reference",
                url: "Reference/{action}/{id}",
                defaults: new { controller = "Reference", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Process",
                url: "Process/{action}/{id}",
                defaults: new { controller = "Process", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}