//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace vetvance_api.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class userspecialty
    {
        public int userspecialtyid { get; set; }
        public Nullable<bool> anesthesiology { get; set; }
        public Nullable<bool> animalwelfare { get; set; }
        public Nullable<bool> behavior { get; set; }
        public Nullable<bool> clinicalpharmacology { get; set; }
        public Nullable<bool> dermatology { get; set; }
        public Nullable<bool> emergencyandcriticalcare { get; set; }
        public Nullable<bool> internalmedicine { get; set; }
        public Nullable<bool> laboratorymedicine { get; set; }
        public Nullable<bool> microbiology { get; set; }
        public Nullable<bool> nutrition { get; set; }
        public Nullable<bool> ophthalmology { get; set; }
        public Nullable<bool> pathology { get; set; }
        public Nullable<bool> preventivemedicine { get; set; }
        public Nullable<bool> radiology { get; set; }
        public Nullable<bool> sportsmedicineandrehabilitation { get; set; }
        public Nullable<bool> surgery { get; set; }
        public Nullable<bool> theriogenology { get; set; }
        public Nullable<bool> toxicology { get; set; }
        public Nullable<bool> veterinarypractitioners { get; set; }
        public Nullable<bool> zoologicalmedicine { get; set; }
        public Nullable<int> userid { get; set; }
    }
}
