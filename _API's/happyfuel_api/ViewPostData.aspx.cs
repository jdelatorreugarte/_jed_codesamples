﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using HappyFuelAppServices;

namespace HappyFuelAppServices
{
    public partial class ViewPostData : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["ID"] != null && Request.QueryString["ID"] != "")
                {
                    Int64 postid = 0;

                    Int64.TryParse(Request.QueryString["ID"], out postid);


                    BindMetaTags(postid);
                }
            }

        }

        private void BindMetaTags(Int64 Id)
        {
            if (Id > 0)
            {

                string fuel_title = " :)Fuel ";
                string fuel_caption = ":)Fuel";
                string image_src = "http://services.happyfuelapp.com/images/iphones.png";


                DAL dal = new DAL();
                System.Data.DataSet ds = dal.GetPostById(Id);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    fuel_title = ds.Tables[0].Rows[0]["fuel_title"].ToString();
                    fuel_caption = ds.Tables[0].Rows[0]["fuel_caption"].ToString();

                    fuel_caption = fuel_caption + "</br> ";
                    //fuel_caption = fuel_caption + ": ) fuel (pronounced happy fuel) takes your happiness tank from empty to happy. It gives you a mobile place to collect and share the things that make you happy, whether it’s a photo, song, spot, video, sound or thought. And while you’re sharing happiness, : ) fuel even lets you gauge your own happiness.";
                    fuel_caption = fuel_caption + "</br>" + "http://www.happyfuelapp.com";

                    if (ds.Tables[0].Rows[0]["fuel_type"].ToString() == "P")
                    {
                        image_src = "https://s3.amazonaws.com/HappyFuelApp/" + ds.Tables[0].Rows[0]["fuel_data"].ToString();
                    }

                    switch (ds.Tables[0].Rows[0]["fuel_type"].ToString().ToLower())
                    {
                        case "p":
                            fuel_title = "fueled a photo: " + fuel_title;
                            break;
                        case "v":
                            fuel_title = "fueled a video: " + fuel_title;
                            break;
                        case "a":
                            fuel_title = "fueled a sound: " + fuel_title;
                            break;
                        case "m":
                            fuel_title = "fueled a beat: " + fuel_title;
                            break;
                        case "l":
                            fuel_title = "fueled a spot: " + fuel_title;
                            break;
                        case "t":
                            fuel_title = "fueled a thought: " + fuel_title;
                            break;

                    }


                }

                HtmlMeta fb = new HtmlMeta();
                //title
                fb.Content = fuel_title;
                fb.Name = "og:title";
                fb.Attributes.Add("property", "og:title");
                this.Header.Controls.Add(fb);

                //caption
                fb = new HtmlMeta();
                fb.Content = fuel_caption;
                fb.Name = "og:description";
                fb.Attributes.Add("property", "og:description");
                this.Header.Controls.Add(fb);

                //image
                fb = new HtmlMeta();
                fb.Content = image_src;
                fb.Name = "og:image";
                fb.Attributes.Add("property", "og:image");
                this.Header.Controls.Add(fb);


            }

        }
    }
}