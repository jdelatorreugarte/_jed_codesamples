﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyFuelAppServices
{
    public class WallFuelPost
    {
        private Int64 _FuelPostId;
        private Int64 _UserId;

        private string _FuelTitle;
        private string _FuelData;
        private string _FuelCaption;
        private string _FuelThumbData;
        private string _FuelType;
        private string _FuelLatitude;
        private string _FuelLongitude;
        private string _UserLatitude;
        private string _UserLongitude;
        private string _UserName;

        private Int32 _FuelRatingCount;
        private string _AverageFuelRating;
        
        public Int64 FuelPostId
        {
            get
            {
                return _FuelPostId;
            }
            set
            {
                _FuelPostId = value;
            }
        }

        public Int64 UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                _UserId = value;
            }
        }

         

        public string FuelTitle
        {
            get
            {
                return _FuelTitle;
            }
            set
            {
                _FuelTitle = value;
            }
        }

        public string FuelType
        {
            get
            {
                return _FuelType;
            }
            set
            {
                _FuelType = value;
            }
        }

        public string FuelData
        {
            get
            {
                return _FuelData;
            }
            set
            {
                _FuelData = value;
            }
        }

        public string FuelCaption
        {
            get
            {
                return _FuelCaption;
            }
            set
            {
                _FuelCaption = value;
            }
        }

        public string FuelThumbData
        {
            get
            {
                return _FuelThumbData;
            }
            set
            {
                _FuelThumbData = value;
            }
        }

        public string FuelLatitude
        {
            get
            {
                return _FuelLatitude;
            }
            set
            {
                _FuelLatitude = value;
            }
        }

        public string FuelLongitude
        {
            get
            {
                return _FuelLongitude;
            }
            set
            {
                _FuelLongitude = value;
            }
        }

        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }
        
        public Int32 FuelRatingCount
        {
            get
            {
                return _FuelRatingCount;
            }
            set
            {
                _FuelRatingCount = value;
            }
        }

        public string AverageFuelRating
        {
            get
            {
                return _AverageFuelRating;
            }
            set
            {
                _AverageFuelRating = value;
            }
        }

     
        
    }
}