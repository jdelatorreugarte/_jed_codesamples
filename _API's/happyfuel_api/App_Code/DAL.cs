﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;

using HappyFuelAppServices;

namespace HappyFuelAppServices
{
    public class DAL
    {

        public DataSet GetUser(Int64 user_id)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_get";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public DataSet GetUserByGUID(string GUID)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_getby_guid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@temp_password", GUID));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public DataSet ResetPassword(string Email, string TempPassword, Int16 IsTempPassword)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_reset_password";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Parameters.Add(new SqlParameter("@temp_password", TempPassword));
            cmd.Parameters.Add(new SqlParameter("@is_temp_password", IsTempPassword));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        public DataSet ChangePassword(string UserName, string Email, string Password, string GUID, Int16 IsTempPassword)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_change_password";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_name", UserName));
            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Parameters.Add(new SqlParameter("@password", Password));
            cmd.Parameters.Add(new SqlParameter("@GUID", GUID));
            cmd.Parameters.Add(new SqlParameter("@is_temp_password", IsTempPassword));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        public DataSet ChangePasswordByGUID(string Password, string GUID, Int16 IsTempPassword)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_change_passwordby_guid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@password", Password));
            cmd.Parameters.Add(new SqlParameter("@GUID", GUID));
            cmd.Parameters.Add(new SqlParameter("@is_temp_password", IsTempPassword));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        public DataSet VerifyUserNameEmail(Int64 UserId, string UserName, string Email)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_validate_username_email";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@user_name", UserName));
            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        //public DataSet VerifyEmail(Int64 UserId, string Email)
        //{

        //    DataSet ds = new DataSet();
        //    SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

        //    SqlCommand cmd = new SqlCommand();
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    cmd.CommandText = "hfsp_users_validate_email";
        //    cmd.CommandTimeout = 0;

        //    cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
        //    cmd.Parameters.Add(new SqlParameter("@user_email", Email));
        //    cmd.Connection = conn;

        //    SqlDataAdapter da = new SqlDataAdapter(cmd);

        //    conn.Open();
        //    da.Fill(ds);

        //    return ds;

        //}

        public DataSet SaveUser(Int64 UserId, string FirstName, string LastName, string MiddleName, string AvatarId, string UserName, string Password, string Email, Int16 IsActive, Int16 IsTempPassword, Int16 IsProfilePublic, string Location)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_save";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@first_name", FirstName));
            cmd.Parameters.Add(new SqlParameter("@last_name", LastName));
            cmd.Parameters.Add(new SqlParameter("@middle_name", MiddleName));
            cmd.Parameters.Add(new SqlParameter("@avatar_id", AvatarId));
            cmd.Parameters.Add(new SqlParameter("@user_name", UserName));
            cmd.Parameters.Add(new SqlParameter("@password", Password));
            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Parameters.Add(new SqlParameter("@is_active", IsActive));
            cmd.Parameters.Add(new SqlParameter("@is_temp_password", IsTempPassword));
            cmd.Parameters.Add(new SqlParameter("@is_profile_public", IsProfilePublic));
            cmd.Parameters.Add(new SqlParameter("@location", Location));


            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public Int64 ValidateUser(string Email, string Password)
        {
            Int64 user_id = 0;

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_validate";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_email", Email));
            cmd.Parameters.Add(new SqlParameter("@password", Password));
            cmd.Connection = conn;
            
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());
            }
            return user_id;
        }

        public DataSet SavePost(Int64 UserId, Int64 FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, Int16 FuelRating,string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string ShareUrl, string TinyUrl, string AppVersion, string OSVersion, string DeviceId, Int16 IsPublic, Int16 IsActive, Int16 IsFileUploaded, string HashTags, string UserTags)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_save";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", FuelPostId));
            cmd.Parameters.Add(new SqlParameter("@fuel_post_date", FuelPostDate));
            cmd.Parameters.Add(new SqlParameter("@fuel_type", FuelType));
            
            cmd.Parameters.Add(new SqlParameter("@fuel_title", FuelTitle));
            cmd.Parameters.Add(new SqlParameter("@fuel_caption", FuelCaption));
            cmd.Parameters.Add(new SqlParameter("@fuel_rating", FuelRating));
            cmd.Parameters.Add(new SqlParameter("@fuel_data", FuelData));
            cmd.Parameters.Add(new SqlParameter("@fuel_thumb_data", FuelThumbData));
            cmd.Parameters.Add(new SqlParameter("@fuel_latitude", FuelLatitude));
            cmd.Parameters.Add(new SqlParameter("@fuel_longitude", FuelLongitude));
            cmd.Parameters.Add(new SqlParameter("@user_latitude", UserLatitude));
            cmd.Parameters.Add(new SqlParameter("@user_longitude", UserLongitude));
            cmd.Parameters.Add(new SqlParameter("@share_url", ShareUrl));
            cmd.Parameters.Add(new SqlParameter("@tiny_url", TinyUrl));

            cmd.Parameters.Add(new SqlParameter("@app_version", AppVersion));
            cmd.Parameters.Add(new SqlParameter("@os_version", OSVersion));
            cmd.Parameters.Add(new SqlParameter("@device_id", DeviceId));
            cmd.Parameters.Add(new SqlParameter("@is_public", IsPublic));
            cmd.Parameters.Add(new SqlParameter("@is_active", IsActive));
            cmd.Parameters.Add(new SqlParameter("@is_file_uploaded", IsFileUploaded));
            cmd.Parameters.Add(new SqlParameter("@hash_tags", HashTags));
            cmd.Parameters.Add(new SqlParameter("@user_tags", UserTags));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public void DeletePost(Int64 user_id,Int64 fuel_post_id )
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_delete";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet GetPostsByUser(Int64 user_id, Int32 limit)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_getby_user";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@limit", limit));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public DataSet GetPostById(Int64 fuel_post_id)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_getby_id";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public void MakePostPublic(string post_guid, Int64 user_id)
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_update_public";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@post_guid", post_guid));
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet GetPostByGUID(string post_guid)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_getby_guid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@post_guid", post_guid));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public DataSet GetLatestPostByUser(Int64 user_id)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_get_latest_user";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        /// <summary>
        /// v2.0 methods
        /// </summary>
        /// <param name="user_id"></param>
        /// <param name="is_public"></param>

        public void MakeUserProfilePublic(Int64 user_id,int is_public)
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_users_update_profile";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@is_public", is_public));
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet SaveFuelRating(Int64 RatingId, Int64 UserId, Int64 FuelPostId, Int32 UserRatingId, string Comments, Int16 IsPublic, string HashTags, string UserTags)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelrating_save";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@rating_id", RatingId));
            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", FuelPostId));
            cmd.Parameters.Add(new SqlParameter("@user_rating_id", UserRatingId));
            cmd.Parameters.Add(new SqlParameter("@comments", Comments));
            cmd.Parameters.Add(new SqlParameter("@is_public", IsPublic));
            cmd.Parameters.Add(new SqlParameter("@hash_tags", HashTags));
            cmd.Parameters.Add(new SqlParameter("@user_tags", UserTags));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public void DeleteFuelRating(Int64 RatingId)
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelrating_delete";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@rating_id", RatingId));
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet GetFuelRatingsByPostId(Int64 fuel_post_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpostrating_getby_id";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public DataSet GetFuelRatingsByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpostrating_getby_user_id";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public DataSet GetFriendsByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_userfriends_getby_id";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public DataSet SaveFriend(Int64 UserFriendId, Int64 UserId, Int64 FriendId, Int16 StatusId)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_user_friends_save";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_friend_id", UserFriendId));
            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@friend_user_id", FriendId));
            cmd.Parameters.Add(new SqlParameter("@status", StatusId));

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }

        public void DeleteFriend(Int64 UserFriendId,Int64 UserId, Int64 FriendId)
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_user_friend_delete";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_friend_id", UserFriendId));
            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@friend_user_id", FriendId));
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet GetWallPosts(string FuelType, string SearchType, Int64 UserId, string SearchTags, Int32 PageNo, Int32 RowCount, Int32 Skip)
        {
 
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_wall_search";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_type", FuelType));
            cmd.Parameters.Add(new SqlParameter("@search_type", SearchType));
            cmd.Parameters.Add(new SqlParameter("@user_id", UserId));
            cmd.Parameters.Add(new SqlParameter("@search_chars", SearchTags));
            cmd.Parameters.Add(new SqlParameter("@page_no", PageNo));
            cmd.Parameters.Add(new SqlParameter("@row_count", RowCount));
            cmd.Parameters.Add(new SqlParameter("@skip", Skip));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public DataSet GetAlertsByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_alerts_getby_user_id";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public DataSet SearchUsers(string search_chars)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_search_users";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@search_chars", search_chars));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public DataSet GetAlertsByUserContactsByPOST(Int64 user_id, string emails)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_get_contacts_alerts";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@emails", emails));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }


        public void DeleteUser(Int64 user_id)
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_delete_account";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet GetFuelPostFlagByPostId(Int64 fuel_post_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_flag_getby_id";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }

        public void SaveFuelPostFlag(Int64 fuel_post_id, Int64 user_id)
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_flag_save";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public void DeleteFuelPostFlag(Int64 fuel_post_id)
        {

            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_flag_delete";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            cmd.Connection = conn;

            conn.Open();
            cmd.ExecuteNonQuery();

        }

        public DataSet GetAllFlaggedPosts()
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_flag_get_all";
            cmd.CommandTimeout = 0;

            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;
        }


        
        #region "DashBoard Methods"

        public DataSet GetPostsByTypeByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_dashboard_posts_type_by_userid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public DataSet GetTopPostsByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_dashboard_top_posts_by_userid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public DataSet GetTopPostsByTypeByRatingByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_dashboard_posts_type_rating_by_userid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public DataSet GetFavoritePostsByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_dashboard_favorite_posts_userid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public DataSet GetBiggestFansByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_dashboard_biggest_fans_userid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public DataSet GetFavoriteFuelersByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_dashboard_favorite_fuelers_userid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public DataSet GetUserFuelersByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_dashboard_fuelers_userid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public DataSet GetUserFollowedFuelersByUserId(Int64 user_id)
        {
            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_dashboard_user_followers_userid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;
        }

        public string IsFollower(Int64 user_id, Int64 friend_id)
        {
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_isfollower";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@friend_id", friend_id));
            cmd.Connection = conn;

            return cmd.ExecuteScalar().ToString();

        }

        public DataSet GetFriendStatus(Int64 user_id, Int64 friend_id)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_getfriend_status";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Parameters.Add(new SqlParameter("@friend_id", friend_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);

            return ds;

        }

        public string GetUserTags(Int64 fuelpost_id)
        {
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_get_usertags_by_postid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuelpost_id));
            cmd.Connection = conn;
            conn.Open();
            return cmd.ExecuteScalar().ToString();

        }

        public string GetHashTags(Int64 fuelpost_id)
        {
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_get_hashtags_by_postid";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuelpost_id));
            cmd.Connection = conn;
            conn.Open();
            return cmd.ExecuteScalar().ToString();

        }

        public string TagSearch(string search_chars)
        {
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_tag_search";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@search_chars", search_chars));
            cmd.Connection = conn;
            conn.Open();
            return cmd.ExecuteScalar().ToString();

        }

        public DataSet GetPostByIdByUserId(Int64 fuel_post_id, Int64 user_id)
        {

            DataSet ds = new DataSet();
            SqlConnection conn = new SqlConnection(ConfigManager.HappyFuelDatabaseConnection);

            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "hfsp_fuelpost_getby_id_by_user_id";
            cmd.CommandTimeout = 0;

            cmd.Parameters.Add(new SqlParameter("@fuel_post_id", fuel_post_id));
            cmd.Parameters.Add(new SqlParameter("@user_id", user_id));
            cmd.Connection = conn;

            SqlDataAdapter da = new SqlDataAdapter(cmd);

            conn.Open();
            da.Fill(ds);



            return ds;

        }
        #endregion
    }
}