﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyFuelAppServices
{
    public class EntityCount
    {
        private Int64 _EntityId;
        private Int32 _EntityRowCount;
        private string _EntityName;

        public Int64 EntityId
        {
            get
            {
                return _EntityId;
            }
            set
            {
                _EntityId = value;
            }
        }

        public Int32 EntityRowCount
        {
            get
            {
                return _EntityRowCount;
            }
            set
            {
                _EntityRowCount = value;
            }
        }

        public string EntityName
        {
            get
            {
                return _EntityName;
            }
            set
            {
                _EntityName = value;
            }
        }

    }
}