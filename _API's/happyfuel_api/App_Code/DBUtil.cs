﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyFuelAppServices
{
    public class DBUtil
    {
        public static string NullToString(object obj)
        {
            if (obj != null)
            {
                return Convert.ToString(obj);
            }
            else
            {
                return "";
            }
            
        }

        public static Int32 NullToInt32(string s)
        {
            Int32 x;
            if (Int32.TryParse(s, out x))
            {
                return x;
            }
            else
            {
                return 0;
            }
        }


        public static Int16 NullToInt16(string s)
        {
            Int16 x;
            if (Int16.TryParse(s, out x))
            {
                return x;
            }
            else
            {
                return 0;
            }
        }

        public static Int64 NullToInt64(string s)
        {
            Int64 x;
            if (Int64.TryParse(s, out x))
            {
                return x;
            }
            else
            {
                return 0;
            }
        }



    }
}