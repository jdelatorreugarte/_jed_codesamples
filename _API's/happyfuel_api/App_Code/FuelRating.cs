﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HappyFuelAppServices
{
    public class FuelRating
    {
        private Int64 _RatingId;
        private Int64 _FuelPostId;
        private Int64 _UserId;
        private Int16 _UserRatingId;
        private string _Comments;
        private Int16 _IsPublic;
        private string _InsertDateTime;
        private string _UpdateDateTime;
        private string _UserName;
        private string _UserLocation;
        private string _UserAvatar;

        public Int64 FuelRatingId
        {
            get
            {
                return _RatingId;
            }
            set
            {
                _RatingId = value;
            }
        }
        public Int64 FuelPostId
        {
            get
            {
                return _FuelPostId;
            }
            set
            {
                _FuelPostId = value;
            }
        }

        public Int64 UserId
        {
            get
            {
                return _UserId;
            }
            set
            {
                _UserId = value;
            }
        }



        public Int16 UserRatingId
        {
            get
            {
                return _UserRatingId;
            }
            set
            {
                _UserRatingId = value;
            }
        }

        public string Comments
        {
            get
            {
                return _Comments;
            }
            set
            {
                _Comments = value;
            }
        }


        public Int16 IsPublic
        {
            get
            {
                return _IsPublic;
            }
            set
            {
                _IsPublic = value;
            }
        }

        public string InsertDateTime
        {
            get
            {
                return _InsertDateTime;
            }
            set
            {
                _InsertDateTime = value;
            }
        }

        public string UpdateDateTime
        {
            get
            {
                return _UpdateDateTime;
            }
            set
            {
                _UpdateDateTime = value;
            }
        }

        public string UserName
        {
            get
            {
                return _UserName;
            }
            set
            {
                _UserName = value;
            }
        }

        public string UserLocation
        {
            get
            {
                return _UserLocation;
            }
            set
            {
                _UserLocation = value;
            }
        }

        public string UserAvatar
        {
            get
            {
                return _UserAvatar;
            }
            set
            {
                _UserAvatar = value;
            }
        }

    }
}