﻿using System;
using System.IO;
using System.Configuration;
using System.Reflection;

namespace HappyFuelAppServices
{

    public class LogError
    {
        public LogError()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public static void LogException(string exception)
        {
            // Dim ca As New MessageArgs
            // ca.Message = exception.ToString
            // ca.StartDelegate = AddressOf LogMessage
            // Dim t As New Thread(AddressOf ca.WriteMessage)
            // t.IsBackground = True
            // t.Start()
            LogError.LogMessage(exception.ToString());
        }


        public static string LogFolderPath
        {
            get
            {
                return HappyFuelAppServices.ConfigManager.LogFolderPath;
            }
        }


        private static void LogMessage(string msg)
        {



            DirectoryInfo dirinfo = new DirectoryInfo(LogFolderPath);

            if (!dirinfo.Exists)
            {
                dirinfo.Create();
            }
            StreamWriter logfile;
            try
            {
                DateTime dt = DateTime.Now;
                string str = (LogFolderPath + "\\Exp_" + (dt.Month + ("_" + (dt.Day + ("_" + (dt.Year + ".txt"))))));

                logfile = new StreamWriter((str), true);

                logfile.WriteLine("");
                logfile.WriteLine(msg);


                if (!(logfile == null))
                {
                    logfile.Close();
                }
                //                               
            }
            catch (Exception)// ex)
            {

            }
            finally
            {
            }
        }


        public static void LogStatusMessage(string msg)
        {

            DirectoryInfo dirinfo = new DirectoryInfo(LogFolderPath);

            if (!dirinfo.Exists)
            {
                dirinfo.Create();
            }
            StreamWriter logfile;
            try
            {

                //                                            msg = msg.Replace(vbCrLf, " ")
                DateTime dt = DateTime.Now;
                string str = (LogFolderPath + "\\App_" + (dt.Month + ("_" + (dt.Day + ("_" + (dt.Year + ".txt"))))));
                logfile = new StreamWriter((str), true);

                logfile.WriteLine("");
                logfile.WriteLine(msg);


                if (!(logfile == null))
                {
                    logfile.Close();
                }

            }
            catch (Exception)// ex)
            {
                //throw ex;
            }
            finally
            {
            }
        }



        public static void LogException(System.Exception myexception, string Friendlymsg)
        {

            System.Text.StringBuilder str = new System.Text.StringBuilder();
            // Warning!!! Optional parameters not supported
            if ((Friendlymsg != ""))
            {
                str.Append(Friendlymsg);
            }
            str.Append('\t');
            str.Append((myexception.Source + '\t'));
            str.Append((myexception.StackTrace + '\t'));
            str.Append(myexception.Message);
            str.Append(("\r\n" + myexception.ToString()));
            LogError.LogException(str.ToString());
        }
        public static void LogException(System.Exception myexception)
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            // Warning!!! Optional parameters not supported

            str.Append('\t');
            str.Append((myexception.Source + '\t'));
            str.Append((myexception.StackTrace + '\t'));
            str.Append(myexception.Message);
            str.Append(("\r\n" + myexception.ToString()));
            LogError.LogException(str.ToString());
        }

        private static void LogEntireException(System.Exception myexception, string Friendlymsg)
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            // Warning!!! Optional parameters not supported
            //        object strQueryString;
            //        object strPostData;
            if ((Friendlymsg != ""))
            {
                str.Append(("\r\n" + ("\r\n" + ("Friendly MSG: " + Friendlymsg))));
            }
            str.Append(("Source      : " + myexception.Source));
            str.Append(("Stack Trace : " + myexception.StackTrace));
            str.Append(("Message     : " + myexception.Message));
            Exception ErrorInfo = myexception;
            string strError = "";
            strError = ((strError + ("Error Message: "
                + (ErrorInfo.Message + ("\r\n" + ("Error Source: "
                + (ErrorInfo.Source + ("\r\n" + ("Error Target Site: "
                + (ErrorInfo.TargetSite.ToString() + "\r\n")))))))))
                + "\\n\\nQueryString Data:\\n-----------------\\n");
            LogError.LogException(str.ToString());
        }
        private static void LogEntireException(System.Exception myexception)
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            // Warning!!! Optional parameters not supported
            //        object strQueryString;
            //        object strPostData;

            str.Append(("Source      : " + myexception.Source));
            str.Append(("Stack Trace : " + myexception.StackTrace));
            str.Append(("Message     : " + myexception.Message));
            Exception ErrorInfo = myexception;
            string strError = "";
            strError = ((strError + ("Error Message: "
                + (ErrorInfo.Message + ("\r\n" + ("Error Source: "
                + (ErrorInfo.Source + ("\r\n" + ("Error Target Site: "
                + (ErrorInfo.TargetSite.ToString() + "\r\n")))))))))
                + "\\n\\nQueryString Data:\\n-----------------\\n");
            LogError.LogException(str.ToString());
        }
        public static string PrintEntireException(System.Exception myexception, string Friendlymsg)
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            // Warning!!! Optional parameters not supported
            //        object strQueryString;
            //        object strPostData;
            if ((Friendlymsg != ""))
            {
                str.Append(("\r\n" + ("\r\n" + ("Friendly MSG: " + Friendlymsg))));
            }
            str.Append(("Source      : " + myexception.Source));
            str.Append(("Stack Trace : " + ("\r\n" + myexception.StackTrace)));
            str.Append(("Message     : " + ("\r\n" + myexception.Message)));
            Exception ErrorInfo = myexception;
            string strError = "";
            strError = ((strError + ("Error Message: "
                + (ErrorInfo.Message + ("\r\n" + ("Error Source: "
                + (ErrorInfo.Source + ("\r\n" + ("Error Target Site: "
                + (ErrorInfo.TargetSite.ToString() + "\r\n")))))))))
                + "\\n\\nQueryString Data:\\n-----------------\\n");
            return strError.ToString();
        }
        public static string PrintEntireException(System.Exception myexception)
        {
            System.Text.StringBuilder str = new System.Text.StringBuilder();
            // Warning!!! Optional parameters not supported
            //        object strQueryString;
            //        object strPostData;

            str.Append(("Source      : " + myexception.Source));
            str.Append(("Stack Trace : " + ("\r\n" + myexception.StackTrace)));
            str.Append(("Message     : " + ("\r\n" + myexception.Message)));
            Exception ErrorInfo = myexception;
            string strError = "";
            strError = ((strError + ("Error Message: "
                + (ErrorInfo.Message + ("\r\n" + ("Error Source: "
                + (ErrorInfo.Source + ("\r\n" + ("Error Target Site: "
                + (ErrorInfo.TargetSite.ToString() + "\r\n")))))))))
                + "\\n\\nQueryString Data:\\n-----------------\\n");
            return strError.ToString();
        }

        private class MessageArgs
        {

            public string Message;

            public StartMessageDelegate StartDelegate;

            public void WriteMessage()
            {
                StartDelegate(Message);
            }

        }
        public delegate void StartMessageDelegate(string Message);
    }
}