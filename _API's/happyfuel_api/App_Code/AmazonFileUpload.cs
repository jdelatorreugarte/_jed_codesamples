﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.IO;
using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;

namespace HappyFuelAppServices
{
    public static class AmazonFileUpload
    {

        public static string UploadFile(string filePath,string fileName)
        {
            
            // Your AWS Credentials.
            string AccessKeyID = "";
            string SecretAccessKey = "";

            string existingBucketName = "HappyFuelApp";
            //string keyName = "*** Provide object key ***";

            AccessKeyID = ConfigManager.HappyFuelAmazonAccessKey;
            SecretAccessKey = ConfigManager.HappyFuelAmazonSecretKey;

            try
            {
                TransferUtility fileTransferUtility = new TransferUtility(AccessKeyID, SecretAccessKey);

                using (FileStream fileToUpload = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    fileTransferUtility.Upload(fileToUpload, existingBucketName, fileName);
                }

                return "SUCCESS";

               }
            catch (AmazonS3Exception s3Exception)
            {
                return "FAIL";
            }
        }

        public static string UploadFileStream(System.IO.Stream FileStream, string fileName)
        {

            // Your AWS Credentials.
            string AccessKeyID = "";
            string SecretAccessKey = "";

            string existingBucketName = "HappyFuelApp";
            //string keyName = "*** Provide object key ***";

            AccessKeyID = ConfigManager.HappyFuelAmazonAccessKey;
            SecretAccessKey = ConfigManager.HappyFuelAmazonSecretKey;

            try
            {
                TransferUtility fileTransferUtility = new TransferUtility(AccessKeyID, SecretAccessKey);

                fileTransferUtility.Upload(FileStream, existingBucketName, fileName);

                return "SUCCESS";

            }
            catch (AmazonS3Exception s3Exception)
            {
                return "FAIL";
            }
        }
    }
}