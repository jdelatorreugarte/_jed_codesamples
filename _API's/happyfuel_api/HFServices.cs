﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using HappyFuelAppServices;

namespace HappyFuelAppServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface HFServices
    {
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ValidateUser/{Email}/{Password}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ValidateUser(string Email, string Password);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ResetPassword/{Email}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ResetPassword(string Email);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetUser/{UserId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetUser(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetUserByGUID/{GUID}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetUserByGUID(string GUID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeletePost/{UserId}/{PostId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode DeletePost(string UserId, string PostId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPostsByUser/{UserId}/{Limit}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetPostsByUser(string UserId, string Limit);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPostByID/{PostId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetPostByID(string PostId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/MakePostPublic/{GUID}/{UserId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode MakePostPublic(string GUID,string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPostByGUID/{GUID}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetPostByGUID(string GUID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetLatestPostByUser/{UserId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetLatestPostByUser(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SaveUser/{UserId}/{UserName}/{Password}/{Email}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SaveUser(string UserId, string UserName, string Password, string Email);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UpdateUser/{UserId}/{UserName}/{Password}/{Email}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdateUser(string UserId, string UserName, string Password, string Email);
        
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SavePost/{UserId}/{FuelPostId}/{FuelPostDate}/{FuelType}/{FuelTitle}/{FuelCaption}/{FuelRating}/{FuelData}/{FuelThumbData}/{FuelLatitude}/{FuelLongitude}/{UserLatitude}/{UserLongitude}/{AppVersion}/{OSVersion}/{DeviceId}/{IsPublic}/{IsFileUploaded}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SavePost(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UpdatePost/{UserId}/{FuelPostId}/{FuelPostDate}/{FuelType}/{FuelTitle}/{FuelCaption}/{FuelRating}/{FuelData}/{FuelThumbData}/{FuelLatitude}/{FuelLongitude}/{UserLatitude}/{UserLongitude}/{AppVersion}/{OSVersion}/{DeviceId}/{IsPublic}/{IsFileUploaded}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdatePost(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ChangePassword/{UserName}/{Email}/{Password}/{GUID}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ChangePassword(string UserName, string Email, string Password, string GUID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ChangePasswordByGUID/{Password}/{GUID}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ChangePasswordByGUID(string Password, string GUID);



        #region "V2 Methods"

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SavePost2/{UserId}/{FuelPostId}/{FuelPostDate}/{FuelType}/{FuelTitle}/{FuelCaption}/{FuelRating}/{FuelData}/{FuelThumbData}/{FuelLatitude}/{FuelLongitude}/{UserLatitude}/{UserLongitude}/{AppVersion}/{OSVersion}/{DeviceId}/{IsPublic}/{IsFileUploaded}/{HashTags}/{UserTags}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SavePost2(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UpdatePost2/{UserId}/{FuelPostId}/{FuelPostDate}/{FuelType}/{FuelTitle}/{FuelCaption}/{FuelRating}/{FuelData}/{FuelThumbData}/{FuelLatitude}/{FuelLongitude}/{UserLatitude}/{UserLongitude}/{AppVersion}/{OSVersion}/{DeviceId}/{IsPublic}/{IsFileUploaded}/{HashTags}/{UserTags}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdatePost2(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UpdateUserProfile/{UserId}/{IsPublic}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdateUserProfile(string UserId, string IsPublic);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SaveUser2/{UserId}/{UserName}/{Password}/{Email}/{IsProfilePublic}/{Location}/{AvatarID}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SaveUser2(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location, string AvatarID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/UpdateUser2/{UserId}/{UserName}/{Password}/{Email}/{IsProfilePublic}/{Location}/{AvatarID}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdateUser2(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location, string AvatarID);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SavePostRating/{RatingId}/{UserId}/{FuelPostId}/{FuelRating}/{Comments}/{IsPublic}/{HashTags}/{UserTags}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SavePostRating(string RatingId, string UserId, string FuelPostId, string FuelRating, string Comments, string IsPublic, string HashTags, string UserTags);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeleteRating/{RatingId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode DeleteRating(string RatingId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetFuelRatingsByPostId/{PostId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetFuelRatingsByPostId(string PostId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetFuelRatingsByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetFuelRatingsByUserId(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetFriendsByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetFriendsByUserId(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SaveFriend/{UserFriendId}/{UserId}/{FriendId}/{StatusId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SaveFriend(string UserFriendId, string UserId, string FriendId, string StatusId);
        
        
        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeleteFriend/{UserFriendId}/{UserId}/{FriendId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode DeleteFriend(string UserFriendId, string UserId, string FriendId);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetWallPosts/{FuelType}/{SearchType}/{UserId}/{SearchTags}/{PageNo}/{RowCount}/{Skip}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetWallPosts(string FuelType, string SearchType, string UserId, string SearchTags, string PageNo, string RowCount, string Skip);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetAlertsByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetAlertsByUserId(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/IsFollower/{UserId}/{FriendId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string IsFollower(string UserId, string FriendId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetFriendStatus/{UserId}/{FriendId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetFriendStatus(string UserId, string FriendId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetUserTags/{FuelPostId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetUserTags(string FuelPostId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetHashTags/{FuelPostId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string GetHashTags(string FuelPostId);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/TagSearch/{SearchChars}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string TagSearch(string SearchChars);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/SearchUsers/{SearchChars}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SearchUsers(string SearchChars);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPostByIDByUserId/{PostId}/{UserId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetPostByIDByUserId(string PostId, string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "GetAlertsByUserContacts/{UserId}/{Emails}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetAlertsByUserContacts(string UserId, string Emails);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeleteUser/{UserId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode DeleteUser(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/ReportPost/{UserId}/{PostId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ReportPost(string UserId, string PostId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/DeletePostFlag/{PostId}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode DeletePostFlag(string PostId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetAllFlaggedPosts", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetAllFlaggedPosts();

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/InviteFriend/{UserId}/{Email}", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode InviteFriend(string UserId, string Email);


        #region "DashBoard Methods"
        

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetPostsByTypeByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetPostsByTypeByUserId(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetTopPostsByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetTopPostsByUserId(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetTopPostsByTypeByRatingByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetTopPostsByTypeByRatingByUserId(string UserId);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetFavoritePostsByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetFavoritePostsByUserId(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetBiggestFansByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetBiggestFansByUserId(string UserId);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetFavoriteFuelersByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetFavoriteFuelersByUserId(string UserId);


        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetUserFuelersByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetUserFuelersByUserId(string UserId);

        [OperationContract]
        [WebInvoke(Method = "GET", UriTemplate = "/GetUserFollowedFuelersByUserId/{UserId}", BodyStyle = WebMessageBodyStyle.Bare, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetUserFollowedFuelersByUserId(string UserId);

        #endregion


        #endregion

        #region "POST Methods"

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveUserByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SaveUserByPOST(string UserId, string UserName, string Password, string Email);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateUserByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdateUserByPOST(string UserId, string UserName, string Password, string Email);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SavePostByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SavePostByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdatePostByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdatePostByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SavePost2ByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SavePost2ByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdatePost2ByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdatePost2ByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags);



        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ChangePasswordByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ChangePasswordByPOST(string UserName, string Email, string Password, string GUID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "ChangePasswordByGUIDByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode ChangePasswordByGUIDByPOST(string Password, string GUID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SaveUser2ByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SaveUser2ByPOST(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location, string AvatarID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UpdateUser2ByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UpdateUser2ByPOST(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location, string AvatarID);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SavePostRatingByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SavePostRatingByPOST(string RatingId, string UserId, string FuelPostId, string FuelRating, string Comments, string IsPublic, string HashTags, string UserTags);

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "TagSearchByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        string TagSearchByPOST(string SearchChars);


        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "SearchUsersByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode SearchUsersByPOST(string SearchChars);


        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "UploadFileByPOST/{FileName}", BodyStyle = WebMessageBodyStyle.Bare, ResponseFormat = WebMessageFormat.Json)]
        StatusMode UploadFileByPOST(string FileName, System.IO.Stream FileContent);
        #endregion

        [OperationContract]
        [WebInvoke(Method = "POST", UriTemplate = "GetAlertsByUserContactsByPOST", BodyStyle = WebMessageBodyStyle.Wrapped, RequestFormat = WebMessageFormat.Json, ResponseFormat = WebMessageFormat.Json)]
        StatusMode GetAlertsByUserContactsByPOST(string UserId, string Emails);

    }

    

    [DataContract]
    public class StatusMode
    {
        bool status = true;
        string id = "";
        string result = "";
        HappyFuelAppServices.FuelUser _user;
        List<HappyFuelAppServices.FuelPost> _post;
        List<HappyFuelAppServices.FuelRating> _rating;
        List<HappyFuelAppServices.FuelFriend> _friends;
        List<HappyFuelAppServices.FuelUser> _users;
        List<HappyFuelAppServices.UserAlert> _alerts;
        List<HappyFuelAppServices.FuelTypeCount> _fueltypecounts;
        List<HappyFuelAppServices.WallFuelPost> _wallposts;

        [DataMember]
        public bool Status
        {
            get { return status; }
            set { status = value; }
        }

        [DataMember]
        public string ID
        {
            get { return id; }
            set { id = value; }
        }

        [DataMember]
        public string Result
        {
            get { return result; }
            set { result = value; }
        }

        [DataMember]
        public HappyFuelAppServices.FuelUser CurrentUser
        {
            get { return _user; }
            set { _user=value;  }
        }

        [DataMember]
        public List<HappyFuelAppServices.FuelPost> Post
        {
            get { return _post; }
            set { _post = value; }
        }

        [DataMember]
        public List<HappyFuelAppServices.FuelRating> Ratings
        {
            get { return _rating; }
            set { _rating = value; }
        }

        [DataMember]
        public List<HappyFuelAppServices.FuelFriend> Friends
        {
            get { return _friends; }
            set { _friends = value; }
        }

        [DataMember]
        public List<HappyFuelAppServices.UserAlert> Alerts
        {
            get { return _alerts; }
            set { _alerts = value; }
        }

        [DataMember]
        public List<HappyFuelAppServices.FuelTypeCount> FuelTypeCounts
        {
            get { return _fueltypecounts; }
            set { _fueltypecounts = value; }
        }

        [DataMember]
        public List<HappyFuelAppServices.FuelUser> Users
        {
            get { return _users; }
            set { _users = value; }
        }

        [DataMember]
        public List<HappyFuelAppServices.WallFuelPost> WallPosts
        {
            get { return _wallposts; }
            set { _wallposts = value; }
        }

    }

    


}
