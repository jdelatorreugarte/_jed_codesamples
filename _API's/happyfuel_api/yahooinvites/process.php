<?php
/*
------------------------------------------------------
  www.idiotminds.com
--------------------------------------------------------
*/
// Include the YOS library.
require dirname(__FILE__).'/lib/Yahoo.inc';

//for converting xml to array
function XmltoArray($xml) {
        $array = json_decode(json_encode($xml), TRUE);
       
        foreach ( array_slice($array, 0) as $key => $value ) {
            if ( empty($value) ) $array[$key] = NULL;
            elseif ( is_array($value) ) $array[$key] = XmltoArray($value);
        }

        return $array;
    }

// debug settings
//error_reporting(E_ALL | E_NOTICE); # do not show notices as library is php4 compatable
//ini_set('display_errors', true);
YahooLogger::setDebug(true);
YahooLogger::setDebugDestination('LOG');

// use memcache to store oauth credentials via php native sessions
//ini_set('session.save_handler', 'files');
//session_save_path('/tmp/');
session_start();

// Make sure you obtain application keys before continuing by visiting:
// https://developer.yahoo.com/dashboard/createKey.html

define('OAUTH_CONSUMER_KEY', 'dj0yJmk9Q2szTTJNc2RtWmRsJmQ9WVdrOVJESnpSRUpyTjJVbWNHbzlNVEl3T1RZd056ZzJNZy0tJnM9Y29uc3VtZXJzZWNyZXQmeD02YQ--');
define('OAUTH_CONSUMER_SECRET', '31474764f6300b63bd05ec3eef2ef1b48ca02dfe');
define('OAUTH_DOMAIN', 'http://www.happyfuel.me');
define('OAUTH_APP_ID', 'D2sDBk7e');


// check for the existance of a session.
// this will determine if we need to show a pop-up and fetch the auth url,
// or fetch the user's social data.
$hasSession = YahooSession::hasSession(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_APP_ID);

if($hasSession == FALSE) {
  // create the callback url,
 // $callback = YahooUtil::current_url()."?in_popup";
 // $callback = "http://ec202.classicinformatics.com/happyfuel/yahooinvites/test.html?in_popup=1";
  // $callback = "http://ec202.classicinformatics.com/happyfuel/yahooinvites/yahoo.php?in_popup";
   $callback = "http://www.happyfuel.me/yahooinvites/yahoo.php?in_popup";
$sessionStore = new NativeSessionStore();
  // pass the credentials to get an auth url.
  // this URL will be used for the pop-up.
  $auth_url = YahooSession::createAuthorizationUrl(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, $callback, $sessionStore);
 
 echo  $auth_url;
 //die;
  
}
else {
  // pass the credentials to initiate a session
  $session = YahooSession::requireSession(OAUTH_CONSUMER_KEY, OAUTH_CONSUMER_SECRET, OAUTH_APP_ID);

  // if the in_popup flag is detected,
  // the pop-up has loaded the callback_url and we can close this window.
 

  // if a session is initialized, fetch the user's profile information
  if($session) {
    // Get the currently sessioned user.
    $user = $session->getSessionedUser();

    // Load the profile for the current user.
    $profile = $user->getProfile();
    $profile_contacts=XmltoArray($user->getContactSync());
   $contacts=array();
   
   if(is_array($profile_contacts['contactsync']['contacts']) && count($profile_contacts['contactsync']['contacts'])>0) {
		foreach($profile_contacts['contactsync']['contacts'] as $key=>$profileContact){
			
			foreach($profileContact['fields'] as $contact){
					if(!is_array($contact['value'])){
						if($contact['type']=='yahooid')
							$contacts[]=$contact['value']."@yahoo.com";
						else if($contact['type']=='email')	
							$contacts[]=$contact['value'];
					}
			}
		}
	}
   echo implode(",", $contacts);
   YahooSession::clearSession();
   die;
   
  }
}

?>
