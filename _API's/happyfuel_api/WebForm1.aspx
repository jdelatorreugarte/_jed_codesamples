﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="HappyFuelAppServices.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        MP4 Conversion<br />
    
        <asp:TextBox ID="txtFileName" runat="server" Width="701px"></asp:TextBox>
        <br />
    
    </div>
        <asp:Button ID="btnConvert" runat="server" OnClick="btnConvert_Click" Text="Convert" />
        <br />
        <br />
        <asp:Panel ID="Panel1" runat="server">
            URL
            <asp:TextBox ID="txtURL" runat="server" TextMode="MultiLine" Width="725px">http://localhost:15555/HFWebServices.svc/ChangePasswordByGUIDByPOST</asp:TextBox>
            <br />
            GUID&nbsp;&nbsp;
            <asp:TextBox ID="txtGUID" runat="server" Width="244px">2c875746-82f6-4b28-a2b0-b4c3871003ed</asp:TextBox>
            <br />
            Password
            <asp:TextBox ID="txtPassword" runat="server" Width="365px"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Reset" />
            <br />
            <asp:TextBox ID="txtResponse" runat="server" Height="201px" TextMode="MultiLine" Width="779px"></asp:TextBox>
            <br />
        </asp:Panel>
    </form>
</body>
</html>
