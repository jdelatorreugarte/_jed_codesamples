﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using HappyFuelAppServices;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.IO;

namespace HappyFuelAppServices
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class HFWebServices : HFServices
    {

        #region "Public Methods"

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public StatusMode ValidateUser(string Email, string Password)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                Int64 id = dal.ValidateUser(Email, Password);

                if (id > 0)
                {
                    status.Status = true;
                    status.Result = "SUCCESS";
                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: User Validation Failed";
                }

                status.ID = id.ToString();


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetUser(string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetUser(Convert.ToInt64(UserId));
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                        status.ID = user_id.ToString();
                        status.Result = "SUCCESS";

                        HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                        user.UserId = user_id;
                        user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                        user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                        user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                        user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                        user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                        user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                        user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                        user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                        user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();
                        user.IsProfilePublic = Convert.ToInt16(ds.Tables[0].Rows[0]["is_profile_public"].ToString());

                        status.CurrentUser = user;
                    }
                    else
                    {
                        status.Result = "FAIL:No User Found";
                        status.ID = "0";
                        status.Status = false;
                    }
                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetUserByGUID(string GUID)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetUserByGUID(GUID);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                        status.ID = user_id.ToString();
                        status.Result = "SUCCESS";

                        HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                        user.UserId = user_id;
                        user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                        user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                        user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                        user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                        user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                        user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                        user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                        user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                        user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                        status.CurrentUser = user;
                    }
                    else
                    {
                        status.Status = false;
                        status.Result = "FAIL:No User Found";
                        status.ID = "0";
                    }

                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode ResetPassword(string Email)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                string new_guid = Guid.NewGuid().ToString();


                //reset password as new guid and send as link
                DataSet ds = dal.ResetPassword( Email, new_guid, 1);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                    status.CurrentUser = user;

                    try
                    {
                        //Send email to user with link to reset password
                        EmailSender email_client = new EmailSender();

                        StringBuilder sb = new StringBuilder();

                        sb.AppendLine("You have requested to reset your password for HappyFuel Application.</br>");

                        sb.AppendLine("Your USER ID is " + user.UserName + ".</br>");

                        sb.AppendLine("Please click following URL to reset your password " + ConfigManager.HappyFuelWebSiteURL + @"hgid=" + new_guid.ToUpper() + ".</br>");

                        string body = sb.ToString();

                        string subject = "Happy Fuel App Services: Reset Password Notification";
                        string To = user.UserEmail;
                        email_client.SendEmail(body, subject, To, "", "", ConfigManager.SMTPFromAddress.ToString(), "High", ConfigManager.SMTPServer.ToString(), "", "1");
                    }
                    catch (Exception smtpex)
                    {
                        LogError.LogException(smtpex);
                        status.Result = "FAIL:SMTP Failed";
                        status.ID = "0";
                        status.Status = false;
                    }
                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode ChangePassword(string UserName, string Email, string Password, string GUID)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();

                //reset password as new guid and send as link
                DataSet ds = dal.ChangePassword(UserName, Email, Password,GUID, 0);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                    status.CurrentUser = user;

                    //Send email to user with link to reset password

                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode ChangePasswordByGUID(string Password, string GUID)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();

                //reset password as new guid and send as link
                DataSet ds = dal.ChangePasswordByGUID(Password, GUID, 0);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();

                    status.CurrentUser = user;

                    //Send email to user with link to reset password

                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode SaveUser(string UserId,string UserName, string Password, string Email)
        {
            return SaveUserConcrete(UserId, UserName, Password, Email, "1", "","");
        }

        public StatusMode UpdateUser(string UserId, string UserName, string Password, string Email)
        {
            return SaveUserConcrete(UserId, UserName, Password, Email, "1", "","");
        }

        public StatusMode DeletePost(string UserId, string PostId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                dal.DeletePost(Convert.ToInt64(UserId), Convert.ToInt64(PostId));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = PostId;
            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetPostsByUser(string UserId, string Limit)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                Int32 limit = 0;

                bool result = Int32.TryParse(Limit, out limit);


                DataSet ds = dal.GetPostsByUser(Convert.ToInt64(UserId),limit);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    /*
                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }
                    */
                    List<FuelPost> fuels = new List<FuelPost>();

                    var maxrows = ds.Tables[0].Rows.Count;

                    Parallel.For(0, maxrows, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        fuels.Add(GetPostByDataRow(dr));
                    });

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the user";
                    status.ID = UserId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetPostByID(string PostId)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetPostById(Convert.ToInt64(PostId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = PostId;

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the ID. Or ID is not made Public yet.";
                    status.ID = PostId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode MakePostPublic(string GUID, string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                dal.MakePostPublic(GUID, Convert.ToInt64(UserId));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = GUID;
            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetPostByGUID(string GUID)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetPostByGUID(GUID);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = GUID;
                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the GUID";
                    status.ID = GUID;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode SavePost(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded)
        {
            return SavePostConcrete(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId,  IsPublic,  IsFileUploaded,"","");
        }

        public StatusMode UpdatePost(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded)
        {
            return SavePostConcrete(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded, "", "");
        }

        public StatusMode GetLatestPostByUser(string UserId)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();

                DataSet ds = dal.GetLatestPostByUser(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the user";
                    status.ID = UserId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }


        #endregion


        #region "V2 Methods"

        public StatusMode SavePost2(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags)
        {
            return SavePostConcrete(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded, HashTags, UserTags);
        }

        public StatusMode UpdatePost2(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags)
        {
            return SavePostConcrete(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded, HashTags, UserTags);
        }
        
        public StatusMode UpdateUserProfile(string UserId, string IsPublic)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();

                dal.MakeUserProfilePublic(Convert.ToInt64(UserId), Convert.ToInt16(IsPublic));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = UserId;

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode SaveUser2(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location, string AvatartID)
        {
            return SaveUserConcrete(UserId, UserName, Password, Email, IsProfilePublic, Location,AvatartID);
        }

        public StatusMode UpdateUser2(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location, string AvatartID)
        {
            return SaveUserConcrete(UserId, UserName, Password, Email, IsProfilePublic, Location,AvatartID);
        }

        public StatusMode SavePostRating(string RatingId, string UserId, string FuelPostId, string FuelRating, string Comments, string IsPublic, string HashTags, string UserTags)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                HashTags = HashTags.Replace('$', '#');

                DataSet ds = dal.SaveFuelRating(Convert.ToInt64(RatingId), Convert.ToInt64(UserId), Convert.ToInt64(FuelPostId), Convert.ToInt16(FuelRating), Comments, Convert.ToInt16(IsPublic), HashTags, UserTags);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = ds.Tables[0].Rows[0]["rating_id"].ToString();

                    List<FuelRating> ratings = new List<FuelRating>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ratings.Add(GetRatingByDataRow(dr));
                    }

                    status.Ratings = ratings;
                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:RATING or Comments Save failed.";
                    status.ID = "";
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode DeleteRating(string RatingId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                dal.DeleteFuelRating(Convert.ToInt64(RatingId));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = RatingId;
            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetFuelRatingsByPostId(string PostId)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetFuelRatingsByPostId(Convert.ToInt64(PostId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = PostId;

                    List<FuelRating> ratings = new List<FuelRating>();
                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ratings.Add(GetRatingByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        ratings.Add(GetRatingByDataRow(dr));
                    });

                    status.Ratings = ratings;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the ID.";
                    status.ID = PostId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetFuelRatingsByUserId(string UserId)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetFuelRatingsByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelRating> ratings = new List<FuelRating>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ratings.Add(GetRatingByDataRow(dr));
                    }
                     */

                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        ratings.Add(GetRatingByDataRow(dr));
                    });

                    status.Ratings = ratings;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the ID.";
                    status.ID = UserId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetFriendsByUserId(string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetFriendsByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelFriend> friends = new List<FuelFriend>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        friends.Add(GetFriendByDataRow(dr));
                    }
                    */

                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        friends.Add(GetFriendByDataRow(dr));
                    });

                    status.Friends = friends;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode SaveFriend(string UserFriendId, string UserId, string FriendId, string StatusId)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.SaveFriend(Convert.ToInt64(UserFriendId), Convert.ToInt64(UserId), Convert.ToInt64(FriendId), Convert.ToInt16(StatusId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = FriendId;

                    List<FuelFriend> friends = new List<FuelFriend>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        friends.Add(GetFriendByDataRow(dr));
                    }

                    status.Friends = friends;
                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:Friends Save failed.";
                    status.ID = "";
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode DeleteFriend(string UserFriendId, string UserId, string FriendId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                dal.DeleteFriend(Convert.ToInt64(UserFriendId), Convert.ToInt64(UserId), Convert.ToInt64(FriendId));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = FriendId;
            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetWallPosts(string FuelType, string SearchType, string UserId, string SearchTags, string PageNo, string RowCount, string Skip)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();

                DataSet ds = dal.GetWallPosts(FuelType, SearchType, Convert.ToInt64(UserId), SearchTags, Convert.ToInt32(PageNo), Convert.ToInt32(RowCount), Convert.ToInt32(Skip));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = "1";

                    //List<WallFuelPost> wallposts = new List<WallFuelPost>();

                    //foreach (DataRow dr in ds.Tables[0].Rows)
                    //{
                    //    wallposts.Add(GetWallPostsByDataRow(dr));
                    //}

                    List<WallFuelPost> posts = new List<WallFuelPost>();

                    var maxrows = ds.Tables[0].Rows.Count;

                    Parallel.For(0, maxrows, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        posts.Add(GetWallPostsByDataRow(dr));
                    });

                    status.WallPosts = posts;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No Posts found for the Search Criteria";
                    status.ID = "0";
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetAlertsByUserId(string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetAlertsByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<UserAlert> alerts = new List<UserAlert>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        alerts.Add(GetUserAlertByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        alerts.Add(GetUserAlertByDataRow(dr));
                    });

                    status.Alerts = alerts;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public string IsFollower(string UserId, string FriendId)
        {
            try
            {
                DAL dal = new DAL();
                return dal.IsFollower(Convert.ToInt64(UserId), Convert.ToInt64(FriendId));
            }
            catch
            {
                return "0";
            }

        }

        public string GetFriendStatus(string UserId, string FriendId)
        {
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetFriendStatus(Convert.ToInt64(UserId), Convert.ToInt64(FriendId));
                return GetJson(ds.Tables[0]);
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }

        }

        public string GetUserTags(string FuelPostId)
        {
            try
            {
                DAL dal = new DAL();
                return  dal.GetUserTags(Convert.ToInt64(FuelPostId));
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }

        }

        public string GetHashTags(string FuelPostId)
        {
            try
            {
                DAL dal = new DAL();
                return dal.GetHashTags(Convert.ToInt64(FuelPostId));
            }
            catch (Exception ex)
            {
                return "Error:" + ex.Message;
            }

        }

        public string TagSearch(string SearchChars)
        {
            try
            {
                SearchChars = SearchChars.Replace("\"", "");
                //SearchChars = ReplaceFirst(SearchChars,"$","#");
                SearchChars = System.Web.HttpUtility.UrlDecode(SearchChars);
                DAL dal = new DAL();
                return dal.TagSearch(SearchChars);
            }
            catch(Exception ex)
            {
                return "Error:" + ex.Message;
            }
        }

        public StatusMode SearchUsers(string SearchChars)
        {
            StatusMode status = new StatusMode();
            try
            {
                SearchChars = SearchChars.Replace("\"", "");
                //SearchChars = ReplaceFirst(SearchChars, "@", "");
                SearchChars = System.Web.HttpUtility.UrlDecode(SearchChars);
                DAL dal = new DAL();
                DataSet ds = dal.SearchUsers(SearchChars);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = "1";

                    List<FuelUser> users = new List<FuelUser>();
                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        users.Add(GetUserByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        users.Add(GetUserByDataRow(dr));
                    });


                    status.Users = users;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = "0";
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetPostByIDByUserId(string PostId, string UserId)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetPostByIdByUserId(Convert.ToInt64(PostId), Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = PostId;

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:No data found for the Post Id. Or Post is not made Public yet. Or User has Profile set to Private";
                    status.ID = PostId;
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetAlertsByUserContacts(string UserId, string Emails)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetAlertsByUserContactsByPOST(Convert.ToInt64(UserId),Emails);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<UserAlert> alerts = new List<UserAlert>();

                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        alerts.Add(GetUserAlertByDataRow(dr));
                    });

                    status.Alerts = alerts;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode DeleteUser(string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                dal.DeleteUser(Convert.ToInt64(UserId));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = "1";
            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode ReportPost(string UserId, string PostId)
        {
            StatusMode status = new StatusMode();
            try
            {
                //1. First verify if Flag already exists
                //2. If exists then do nothing
                //3. If not create flag and send email
                
                //1. 
                Int64 fuelpost_flag_id = 0;
                DAL dal = new DAL();
                DataSet ds = dal.GetFuelPostFlagByPostId(Convert.ToInt64(PostId));
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    fuelpost_flag_id = Convert.ToInt64(ds.Tables[0].Rows[0]["fuelpost_flag_id"].ToString());
                }


                //3
                if (fuelpost_flag_id == 0)
                {

                    //save the flag
                    dal.SaveFuelPostFlag(Convert.ToInt64(PostId), Convert.ToInt64(UserId));


                    //send email
                    ds = dal.GetUser(Convert.ToInt64(UserId));
                    Int64 user_id = 0;

                    if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                    {
                        user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());

                        if (user_id > 0)
                        {
                            status.Status = true;
                            status.ID = user_id.ToString();
                            status.Result = "SUCCESS";

                            HappyFuelAppServices.FuelUser user = GetUserByDataRow(ds.Tables[0].Rows[0]);

                            //Send email to user with link to reset password
                            EmailSender email_client = new EmailSender();

                            StringBuilder sb = new StringBuilder();

                            sb.AppendLine("Following Post is reported as in appropriate. Please review.");

                            sb.AppendLine("</br>");

                            sb.AppendLine(ConfigManager.HappyFuelPostPageURL + PostId);

                            sb.AppendLine("</br>");

                            sb.AppendLine("Reporting User is " + user.UserName + " - " + user.UserEmail + ".</br>");

                            string body = sb.ToString();

                            string subject = "Happy Fuel App Services: Report Post Notification";
                            string To = ConfigManager.SMTPFromAddress.ToString();
                            email_client.SendEmail(body, subject, To, "", "", ConfigManager.SMTPFromAddress.ToString(), "High", ConfigManager.SMTPServer.ToString(), "", "1");


                            status.Result = "SUCCESS: Email sent to HappyFuel Administrators.";
                            status.ID = "1";
                            status.Status = true;

                        }
                        else
                        {
                            status.Result = "FAIL:No User Found";
                            status.ID = "0";
                            status.Status = false;
                        }
                    }
                    else
                    {
                        status.Result = "FAIL:No User Found";
                        status.ID = "0";
                        status.Status = false;
                    }
                }
                else
                {
                    status.Result = "SUCCESS: Flag already exists";
                    status.ID = fuelpost_flag_id.ToString();
                    status.Status = true;
                }

               
            }
            catch (Exception smtpex)
            {
                LogError.LogException(smtpex);
                status.Result = "FAIL:SMTP Failed";
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode DeletePostFlag(string PostId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                dal.DeleteFuelPostFlag(Convert.ToInt64(PostId));

                status.Status = true;
                status.Result = "SUCCESS";
                status.ID = "1";
            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        public StatusMode GetAllFlaggedPosts()
        {

            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetAllFlaggedPosts();

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = "1";

                    List<FuelPost> posts = new List<FuelPost>();

                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        posts.Add(GetPostByDataRowMin(dr));
                    });

                    status.Post = posts;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No flagged posts found.";
                    status.ID = "0";
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;


        }

        public StatusMode InviteFriend(string UserId, string Email)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetUser(Convert.ToInt64(UserId));
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());

                    if (user_id > 0)
                    {
                        status.Status = true;
                        status.ID = user_id.ToString();
                        status.Result = "SUCCESS";

                        HappyFuelAppServices.FuelUser user = GetUserByDataRow(ds.Tables[0].Rows[0]);

                        //Send email to user with link to reset password
                        EmailSender email_client = new EmailSender();
                        
                        

                        string body = System.IO.File.ReadAllText(ConfigManager.HappyFuelEmailTemplatePath + @"\invite_friend.txt");

                        body = body.Replace("<name>", user.UserName);
                        body = body.Replace("<email>", user.UserEmail);
                        body = body.Replace("<sitelink>", ConfigManager.HappyFuelSiteLink);
                       

                        string subject = "Join me on Happy Fuel!";
                        string To = Email;
                        email_client.SendEmail(body, subject, To, "", "", ConfigManager.SMTPJoinUsFromAddress.ToString(), "Low", ConfigManager.SMTPServer.ToString(), "", "1");


                        status.Result = "SUCCESS: Invite sent";
                        status.ID = "1";
                        status.Status = true;

                    }
                    else
                    {
                        status.Result = "FAIL:No User Found";
                        status.ID = "0";
                        status.Status = false;
                    }
                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }
             

            }
            catch (Exception smtpex)
            {
                LogError.LogException(smtpex);
                status.Result = "FAIL:SMTP Failed due to " + smtpex.Message;
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }


        #region "DashBoard Methods"

        public StatusMode GetPostsByTypeByUserId(string UserId)
        {

            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetPostsByTypeByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelTypeCount> entities = new List<FuelTypeCount>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        entities.Add(GetFuelTypeCountByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        entities.Add(GetFuelTypeCountByDataRow(dr));
                    });

                    status.FuelTypeCounts = entities;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;

        }

        public StatusMode GetTopPostsByUserId(string UserId)
        {

            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetTopPostsByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelPost> posts = new List<FuelPost>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        posts.Add(GetPostByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        posts.Add(GetPostByDataRowMin(dr));
                    });

                    status.Post = posts;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;


        }

        public StatusMode GetTopPostsByTypeByRatingByUserId(string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetTopPostsByTypeByRatingByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelTypeCount> entities = new List<FuelTypeCount>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        entities.Add(GetFuelTypeCountByDataRow(dr));
                    }
                    */

                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        entities.Add(GetFuelTypeCountByDataRow(dr));
                    });

                    status.FuelTypeCounts = entities;


                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetFavoritePostsByUserId(string UserId)
        {
 
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetFavoritePostsByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelPost> posts = new List<FuelPost>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        posts.Add(GetPostByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        posts.Add(GetPostByDataRowMin(dr));
                    });

                    status.Post = posts;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;

        }

        public StatusMode GetBiggestFansByUserId(string UserId)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetBiggestFansByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelUser> users = new List<FuelUser>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        users.Add(GetUserByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        users.Add(GetUserByDataRow(dr));
                    });

                    status.Users = users;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetFavoriteFuelersByUserId(string UserId)
        {

            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetFavoriteFuelersByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelUser> users = new List<FuelUser>();
                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        users.Add(GetUserByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        users.Add(GetUserByDataRow(dr));
                    });

                    status.Users = users;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetUserFuelersByUserId(string UserId)
        {

            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetUserFuelersByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelUser> users = new List<FuelUser>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        users.Add(GetUserByDataRow(dr));
                    }
                    */

                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        users.Add(GetUserByDataRow(dr));
                    });


                    status.Users = users;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetUserFollowedFuelersByUserId(string UserId)
        {

            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.GetUserFollowedFuelersByUserId(Convert.ToInt64(UserId));

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = UserId;

                    List<FuelUser> users = new List<FuelUser>();

                    /*
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        users.Add(GetUserByDataRow(dr));
                    }
                    */
                    Parallel.For(0, ds.Tables[0].Rows.Count, i =>
                    {
                        var dr = ds.Tables[0].Rows[i];
                        users.Add(GetUserByDataRow(dr));
                    });

                    status.Users = users;

                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL: No data found for the ID.";
                    status.ID = UserId;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        #endregion

        #endregion

        #region "Private Methods"

        public StatusMode SavePostConcrete(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags)
        {
            StatusMode status = new StatusMode();

            try
            {
                DAL dal = new DAL();
                DataSet ds = dal.SavePost(Convert.ToInt64(UserId), Convert.ToInt64(FuelPostId), FuelPostDate, FuelType, FuelTitle, FuelCaption, Convert.ToInt16(FuelRating), FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, "", "", AppVersion, OSVersion, DeviceId, Convert.ToInt16(IsPublic), 1, Convert.ToInt16(IsFileUploaded),HashTags,UserTags);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    status.Status = true;
                    status.Result = "SUCCESS:";
                    status.ID = ds.Tables[0].Rows[0]["post_guid"].ToString();

                    List<FuelPost> fuels = new List<FuelPost>();

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        fuels.Add(GetPostByDataRow(dr));
                    }

                    status.Post = fuels;
                }
                else
                {
                    status.Status = false;
                    status.Result = "FAIL:POST Save failed.";
                    status.ID = "";
                }

                //Convert CAF Files to MP4 for able to listen in all browsers only for Audio Files generated from iPhone
                try
                {
                    ConvertToMP4(Convert.ToInt16(IsFileUploaded), FuelType, FuelData);
                }
                catch (Exception cex)
                {
                    LogError.LogException("Conversion to MP4 failed due to " + cex.ToString());
                }


            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }

            return status;
        }

        private void ConvertToMP4(Int16 IsFileUploaded, string FuelType, string FileName)
        {

            try
            {
                //Convert CAF Files to MP4 for able to listen in all browsers only for Audio Files generated from iPhone
                string NewFileName = System.IO.Path.GetFileNameWithoutExtension(FileName) + ".mp4";

                string ext = System.IO.Path.GetExtension(FileName).ToLower();

                if (ext == ".caf" || ext == ".amr" || ext == ".m4a")
                {

                    if (Convert.ToInt16(IsFileUploaded) == 1 && FileName.Trim() != "" && FuelType == "A")
                    {
                        HappyFuelAmazonTransCoder.CreateJobRequest(FileName, NewFileName, "HappyFuel", "HappyFuel");
                    }
                }
            }
            catch (Exception cex)
            {
                LogError.LogException("Conversion to MP4 Method failed due to " + cex.ToString());
            }



        }

        private FuelPost GetPostByDataRow(DataRow dr)
        {
            FuelPost fp = new FuelPost();

            fp.FuelPostId = Convert.ToInt64(dr["fuel_post_id"].ToString());
            fp.UserId = Convert.ToInt64(dr["user_id"].ToString());
            fp.PostGUID = dr["post_guid"].ToString();
            fp.FuelPostDate = dr["fuel_post_date"].ToString();
            fp.FuelType = dr["fuel_type"].ToString();
            fp.FuelTitle = dr["fuel_title"].ToString();
            fp.FuelCaption = dr["fuel_caption"].ToString();
            fp.FuelRating = Convert.ToInt16(dr["fuel_rating"].ToString());
            fp.FuelData = dr["fuel_data"].ToString();
            fp.FuelThumbData = dr["fuel_thumb_data"].ToString();
            fp.FuelLatitude = dr["fuel_latitude"].ToString();
            fp.FuelLongitude = dr["fuel_longitude"].ToString();
            fp.UserLatitude = dr["user_latitude"].ToString();
            fp.UserLongitude = dr["user_longitude"].ToString();
            fp.ShareURL = ConfigManager.HappyFuelPostPageURL + dr["fuel_post_id"].ToString();
            fp.TinyURL = ConfigManager.HappyFuelPostPageURL + dr["fuel_post_id"].ToString(); // TinyUrlGenerator.MakeTinyUrl(ConfigManager.HappyFuelPostPageURL + dr["fuel_post_id"].ToString());
            fp.AppVersion = dr["app_version"].ToString();
            fp.OSVersion = dr["os_version"].ToString();
            fp.DeviceID = dr["device_id"].ToString();
            fp.IsPublic = Convert.ToInt16(dr["is_public"].ToString());
            fp.IsActive = Convert.ToInt16(dr["is_active"].ToString());
            fp.IsFileUploaded = DBUtil.NullToInt16(DBUtil.NullToString(dr["is_file_uploaded"])); 
            fp.InsertDateTime = dr["insert_datetime"].ToString();
            fp.UpdateDateTime = dr["update_datetime"].ToString();

            fp.AverageFuelRating = DBUtil.NullToString(dr["average_rating"]);
            fp.FuelRatingCount = DBUtil.NullToInt16(DBUtil.NullToString(dr["rating_count"]));

            fp.UserName =  DBUtil.NullToString(dr["user_name"]);
            fp.UserLocation =  DBUtil.NullToString(dr["user_location"]);
            fp.UserAvatar = DBUtil.NullToString(dr["user_avatar"]);
            fp.IsFlagged = DBUtil.NullToInt16(DBUtil.NullToString(dr["is_flagged"]));
            return fp;
        }

        private WallFuelPost GetWallPostsByDataRow(DataRow dr)
        {
            WallFuelPost fp = new WallFuelPost();

            fp.FuelPostId = Convert.ToInt64(dr["fuel_post_id"].ToString());
            fp.UserId = Convert.ToInt64(dr["user_id"].ToString());
            fp.FuelTitle = dr["fuel_title"].ToString();
            fp.FuelData = dr["fuel_data"].ToString();
            fp.FuelThumbData = dr["fuel_thumb_data"].ToString();
            fp.FuelCaption = dr["fuel_caption"].ToString();
            fp.FuelType = dr["fuel_type"].ToString();
            fp.FuelLatitude = dr["fuel_latitude"].ToString();
            fp.FuelLongitude = dr["fuel_longitude"].ToString();

            fp.UserName = dr["user_name"].ToString();
            fp.AverageFuelRating = DBUtil.NullToString(dr["average_rating"]);
            fp.FuelRatingCount = DBUtil.NullToInt16(DBUtil.NullToString(dr["rating_count"]));


            return fp;
        }

        private FuelPost GetPostByDataRowMin(DataRow dr)
        {
            FuelPost fp = new FuelPost();

            fp.FuelPostId = Convert.ToInt64(dr["fuel_post_id"].ToString());
            fp.UserId = Convert.ToInt64(dr["user_id"].ToString());
            fp.FuelTitle = dr["fuel_title"].ToString();
            fp.FuelData = dr["fuel_data"].ToString();
            fp.FuelCaption = dr["fuel_caption"].ToString();
            fp.FuelThumbData = dr["fuel_thumb_data"].ToString();
            fp.AverageFuelRating = DBUtil.NullToString(dr["average_rating"]);
            fp.FuelRatingCount = DBUtil.NullToInt16(DBUtil.NullToString(dr["rating_count"]));
            return fp;
        }

        private StatusMode SaveUserConcrete(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location,string AvatartID)
        {
            StatusMode status = new StatusMode();
            try
            {
                DAL dal = new DAL();

                DataSet dsVerify = dal.VerifyUserNameEmail(Convert.ToInt64(UserId), UserName, Email);
                if (dsVerify != null && dsVerify.Tables.Count > 0 && dsVerify.Tables[0].Rows.Count > 0)
                {
                    string StatusCode = dsVerify.Tables[0].Rows[0]["Status"].ToString();

                    if (StatusCode.Trim() != "")
                    {
                        status.Status = false;
                        status.Result = StatusCode;
                        status.ID = "0";

                        return status;
                    }

                }



                DataSet ds = dal.SaveUser(Convert.ToInt64(UserId), "", "", "",AvatartID, UserName, Password, Email, 1, 0, Convert.ToInt16(IsProfilePublic), Location);
                Int64 user_id = 0;

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    user_id = Convert.ToInt64(ds.Tables[0].Rows[0]["user_id"].ToString());


                    if (user_id > 0)
                    {
                        status.Status = true;
                    }
                    else
                    {
                        status.Status = false;
                    }

                    status.ID = user_id.ToString();
                    status.Result = "SUCCESS";

                    HappyFuelAppServices.FuelUser user = new HappyFuelAppServices.FuelUser();
                    user.UserId = user_id;
                    user.FirstName = ds.Tables[0].Rows[0]["first_name"].ToString();
                    user.LastName = ds.Tables[0].Rows[0]["last_name"].ToString();
                    user.MiddleName = ds.Tables[0].Rows[0]["middle_name"].ToString();
                    user.AvatarId = ds.Tables[0].Rows[0]["avatar_id"].ToString();
                    user.UserName = ds.Tables[0].Rows[0]["user_name"].ToString();
                    user.UserEmail = ds.Tables[0].Rows[0]["user_email"].ToString();
                    user.IsActive = Convert.ToInt16(ds.Tables[0].Rows[0]["user_id"].ToString());
                    user.IsTemporaryPassword = Convert.ToInt16(ds.Tables[0].Rows[0]["is_temp_password"].ToString());
                    user.InsertDateTime = ds.Tables[0].Rows[0]["insert_datetime"].ToString();
                    user.UpdateDateTime = ds.Tables[0].Rows[0]["update_datetime"].ToString();
                    user.IsProfilePublic = Convert.ToInt16(ds.Tables[0].Rows[0]["is_profile_public"].ToString());
                    user.Location = ds.Tables[0].Rows[0]["location"].ToString();

                    //status.CurrentUser = user;
                }
                else
                {
                    status.Result = "FAIL:No User Found";
                    status.ID = "0";
                    status.Status = false;
                }

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        private FuelRating GetRatingByDataRow(DataRow dr)
        {
            FuelRating fr = new FuelRating();

            fr.FuelRatingId = Convert.ToInt64(dr["rating_id"].ToString());
            fr.FuelPostId = Convert.ToInt64(dr["fuel_post_id"].ToString());
            fr.UserId = Convert.ToInt64(dr["user_id"].ToString());
            fr.UserRatingId = Convert.ToInt16(dr["user_rating_id"].ToString());
            fr.Comments = dr["comments"].ToString();
            fr.IsPublic = Convert.ToInt16(dr["is_public"].ToString());
            fr.InsertDateTime = dr["insert_datetime"].ToString();
            fr.UpdateDateTime = dr["update_datetime"].ToString();
            fr.UserName = dr["user_name"].ToString();
            fr.UserLocation = dr["user_location"].ToString();
            fr.UserAvatar= dr["user_avatar"].ToString();
            return fr;
        }

        private FuelUser GetUserByDataRow(DataRow dr)
        {
            FuelUser user = new FuelUser();
            user.UserId = Convert.ToInt64(dr["user_id"].ToString());
            user.FirstName = DBUtil.NullToString(dr["first_name"]);
            user.LastName = DBUtil.NullToString(dr["last_name"]);
            user.MiddleName = DBUtil.NullToString(dr["middle_name"]);
            user.AvatarId = DBUtil.NullToString(dr["avatar_id"]);
            user.UserName = DBUtil.NullToString(dr["user_name"]);
            user.UserEmail = DBUtil.NullToString(dr["user_email"]);
            user.IsActive = DBUtil.NullToInt16(DBUtil.NullToString(dr["is_active"])); 
            user.InsertDateTime = DBUtil.NullToString(dr["insert_datetime"]);
            user.UpdateDateTime = DBUtil.NullToString(dr["update_datetime"]);
            user.IsProfilePublic = DBUtil.NullToInt16(DBUtil.NullToString(dr["is_profile_public"]));
            user.Location = DBUtil.NullToString(dr["location"]);

            if (dr.Table.Columns.Contains("posts_count"))
            {
                user.PostsCount = DBUtil.NullToInt32(DBUtil.NullToString(dr["posts_count"]));
            }
            if (dr.Table.Columns.Contains("ratings_count"))
            {
                user.RatingsCount = DBUtil.NullToInt32(DBUtil.NullToString(dr["ratings_count"]));
            }
            

            return user;
        }

        private FuelFriend GetFriendByDataRow(DataRow dr)
        {
            FuelFriend user = new FuelFriend();
            user.UserId = Convert.ToInt64(dr["user_id"].ToString());

            user.FirstName = DBUtil.NullToString(dr["first_name"]);

            user.LastName = DBUtil.NullToString(dr["last_name"]);

            user.MiddleName = DBUtil.NullToString(dr["middle_name"]);

            user.AvatarId = DBUtil.NullToString(dr["avatar_id"]);

            user.UserName = DBUtil.NullToString(dr["user_name"]);

            user.UserEmail = DBUtil.NullToString(dr["user_email"]);

            user.IsActive = DBUtil.NullToInt16(DBUtil.NullToString(dr["is_active"]));

            user.InsertDateTime = DBUtil.NullToString(dr["insert_datetime"]);

            user.UpdateDateTime = DBUtil.NullToString(dr["update_datetime"]);

            user.IsProfilePublic = DBUtil.NullToInt16(DBUtil.NullToString(dr["is_profile_public"]));

            user.Location = DBUtil.NullToString(dr["location"]);

            if (dr.Table.Columns.Contains("posts_count"))
            {
                user.PostsCount = DBUtil.NullToInt32(DBUtil.NullToString(dr["posts_count"]));
            }
            if (dr.Table.Columns.Contains("ratings_count"))
            {
                user.RatingsCount = DBUtil.NullToInt32(DBUtil.NullToString(dr["ratings_count"]));
            }

            if (dr.Table.Columns.Contains("status_id"))
            {
                user.StatusId = DBUtil.NullToInt16(DBUtil.NullToString(dr["status_id"]));
            }
            if (dr.Table.Columns.Contains("status_key"))
            {
                user.StatusKey = DBUtil.NullToString(dr["status_key"]);
            }
            if (dr.Table.Columns.Contains("status_value"))
            {
                user.StatusValue = DBUtil.NullToString(dr["status_value"]);
            }


            return user;
        }

        private UserAlert GetUserAlertByDataRow(DataRow dr)
        {
            UserAlert alert = new UserAlert();
            alert.UserId = Convert.ToInt64(dr["user_id"].ToString());
            alert.FriendUserId = Convert.ToInt64(dr["friend_user_id"].ToString());
            alert.FriendUserName=  DBUtil.NullToString(dr["friend_user_name"]);
            alert.StatusId = DBUtil.NullToInt32( DBUtil.NullToString(dr["status_id"]));
            alert.StatusKey = DBUtil.NullToString(dr["status_key"]);
            alert.StatusValue = DBUtil.NullToString(dr["status_value"]);
            alert.AvatarId = DBUtil.NullToString(dr["avatar_id"]);

            return alert;
        }

        private EntityCount GetEntityCountByDataRow(DataRow dr)
        {
            EntityCount ec = new EntityCount();
            ec.EntityRowCount = DBUtil.NullToInt32(DBUtil.NullToString(dr["entity_rowcount"]));
            ec.EntityName = DBUtil.NullToString(dr["entity_name"]);

            return ec;
        }

        private FuelTypeCount GetFuelTypeCountByDataRow(DataRow dr)
        {
            FuelTypeCount ec = new FuelTypeCount();
            ec.FuelTypeRowCount = DBUtil.NullToInt32(DBUtil.NullToString(dr["entity_rowcount"]));
            ec.FuelType = DBUtil.NullToString(dr["entity_name"]);

            return ec;
        }

        private string GetJson(DataTable dt)
        {
            System.Web.Script.Serialization.JavaScriptSerializer serializer = new

            System.Web.Script.Serialization.JavaScriptSerializer();
            List<Dictionary<string, object>> rows =
              new List<Dictionary<string, object>>();
            Dictionary<string, object> row = null;

            foreach (DataRow dr in dt.Rows)
            {
                row = new Dictionary<string, object>();
                foreach (DataColumn col in dt.Columns)
                {
                    row.Add(col.ColumnName.Trim(), dr[col]);
                }
                rows.Add(row);
            }
            return serializer.Serialize(rows);
        }

        private string ReplaceFirst(string text, string search, string replace)
        {
            int pos = text.IndexOf(search);
            if (pos < 0)
            {
                return text;
            }
            return text.Substring(0, pos) + replace + text.Substring(pos + search.Length);


        }

        #endregion

        
        #region "POST Methods"
        
        public StatusMode SavePostByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded)
        {
            return SavePostConcrete(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded,"","");
        }

        public StatusMode UpdatePostByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded)
        {
            return SavePostConcrete(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded, "", "");
        }

        public StatusMode SavePost2ByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags)
        {
            return SavePostConcrete(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded, HashTags, UserTags);
        }

        public StatusMode UpdatePost2ByPOST(string UserId, string FuelPostId, string FuelPostDate, string FuelType, string FuelTitle, string FuelCaption, string FuelRating, string FuelData, string FuelThumbData, string FuelLatitude, string FuelLongitude, string UserLatitude, string UserLongitude, string AppVersion, string OSVersion, string DeviceId, string IsPublic, string IsFileUploaded, string HashTags, string UserTags)
        {
            return SavePostConcrete(UserId, FuelPostId, FuelPostDate, FuelType, FuelTitle, FuelCaption, FuelRating, FuelData, FuelThumbData, FuelLatitude, FuelLongitude, UserLatitude, UserLongitude, AppVersion, OSVersion, DeviceId, IsPublic, IsFileUploaded, HashTags, UserTags);
        }


        public StatusMode ChangePasswordByGUIDByPOST(string Password, string GUID)
        {
            return ChangePasswordByGUID(Password, GUID);
        }

        public string ChangePasswordByGUIDByPOSTNew(string Password, string GUID)
        {
            StatusMode status = ChangePasswordByGUID(Password, GUID);

            return status.Result;
        }

        public StatusMode ChangePasswordByPOST(string UserName, string Email, string Password, string GUID)
        {
            return  ChangePassword(UserName, Email, Password, GUID);
        }

        public StatusMode SaveUserByPOST(string UserId,string UserName, string Password, string Email)
        {
            return SaveUser(UserId, UserName, Password, Email);
        }

        public StatusMode UpdateUserByPOST(string UserId, string UserName, string Password, string Email)
        {
            return UpdateUser(UserId, UserName, Password, Email);
        }

        public StatusMode SaveUser2ByPOST(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location, string AvatarID)
        {
            return SaveUser2(UserId, UserName, Password, Email, IsProfilePublic, Location, AvatarID);
        }

        public StatusMode UpdateUser2ByPOST(string UserId, string UserName, string Password, string Email, string IsProfilePublic, string Location, string AvatarID)
        {
            return UpdateUser2(UserId, UserName, Password, Email, IsProfilePublic, Location, AvatarID);
        }

        public StatusMode SavePostRatingByPOST(string RatingId, string UserId, string FuelPostId, string FuelRating, string Comments, string IsPublic, string HashTags, string UserTags)
        {
            return  SavePostRating(RatingId, UserId, FuelPostId, FuelRating, Comments, IsPublic, HashTags, UserTags);
        }

        public string TagSearchByPOST(string SearchChars)
        {
            return TagSearch(SearchChars);
        }

        public StatusMode SearchUsersByPOST(string SearchChars)
        {
            return SearchUsers(SearchChars);
        }

        public StatusMode UploadFileByPOST(string FileName, Stream FileContent)
        {
            StatusMode status = new StatusMode();
           
            try
            {

                FileStream targetStream = null;
                Stream sourceStream = FileContent;

                string uploadFolder = @"C:\temp\";
                string filePath = Path.Combine(uploadFolder, FileName);

                using (targetStream = new FileStream(filePath, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    //read from the input stream in 6K chunks
                    //and save to output stream
                    const int bufferLen = 65000;
                    byte[] buffer = new byte[bufferLen];
                    int count = 0;
                    while ((count = sourceStream.Read(buffer, 0, bufferLen)) > 0)
                    {
                        targetStream.Write(buffer, 0, count);
                    }
                    targetStream.Close();
                    sourceStream.Close();
                }

                
/*                
                byte[] buffer;
                using (var memorystream = new MemoryStream())
                {
                    FileContent.CopyTo(memorystream);
                    buffer = memorystream.ToArray();
                }
              
                 File.WriteAllBytes(@"c:\temp\" + FileName, buffer);
                 
                 AmazonFileUpload.UploadFile(@"c:\temp\" + FileName, FileName);
  */
                AmazonFileUpload.UploadFile(filePath, FileName);
                status.Result = "SUCCESS: TotalBytesRead:";// +totalBytesRead;
                status.Status = true;

                //System.IO.File.Delete(filePath);

            }
            catch (Exception ex)
            {
                status.Result = "FAIL:" + ex.Message.ToString();
                status.ID = "0";
                status.Status = false;
            }
            return status;
        }

        public StatusMode GetAlertsByUserContactsByPOST(string UserId, string Emails)
        {
            return GetAlertsByUserContacts(UserId, Emails);
        }

        #endregion


    }
}
