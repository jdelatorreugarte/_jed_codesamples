/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 9/28/13
 * Time: 4:58 AM
 */
/**
 * Requires jQuery and jQuery.cookie (https://github.com/carhartl/jquery-cookie)
 * - Adds an 'options' parameter to $cookieStore.put and $cookieStore.remove.
 * - Default options can be set by calling $cookieStoreProvider.setDefaultOptions
 */


define(['ng/modules/mainApp', 'jquery', 'jquery-cookies'] , function (mainApp, $) {
	mainApp.provider('$cookieStore', [function(){
		var self = this;
		self.defaultOptions = {};

		self.setDefaultOptions = function(options){
			self.defaultOptions = options;
		};

		self.$get = function(){
			return {
				get: function(name){
					var jsonCookie = $.cookie(name);
					if(jsonCookie){
						return angular.fromJson(jsonCookie);
					}
				},
				set: function(name, value, options){
					options = $.extend({}, self.defaultOptions, options);
					$.cookie(name, angular.toJson(value), options);
				},
				remove: function(name, options){
					options = $.extend({}, self.defaultOptions, options);
					$.removeCookie(name, options);
				}
			};
		};
	}]);
});