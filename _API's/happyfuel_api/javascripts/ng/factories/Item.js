/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/14/13
 * Time: 12:08 AM
 */

define(['ng/modules/mainApp'] , function (mainApp) {
    mainApp.factory('Item' , ['$resource' , function ($resource) {
        $resource('/item/:id' , {id: '@id'});
    }]);
});