/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/25/13
 * Time: 8:41 AM
 */

define(['ng/modules/mainApp','ng/modules/googleOauth','ng/modules/angularOauth'] , function (mainApp,$rootScope,Token) {
	mainApp.

  config(function(TokenProvider) {
    // Demo configuration for the "angular-oauth demo" project on Google.
    // Log in at will!

    // Sorry about this way of getting a relative URL, powers that be.
    // var baseUrl = document.URL.replace('example/demo.html', '');

    TokenProvider.extendConfig({
      clientId: '193839560964-i2506hgcqrdu1j6epjpmkin8qlos710r.apps.googleusercontent.com',
      redirectUri: 'http://www.happyfuel.me/oauth2callback.html',  // allow lunching demo from a mirror
      // scopes: ["https://www.googleapis.com/auth/userinfo.email"]
      scopes: ["https://www.google.com/m8/feeds"]
    });
  }).controller('googleOauthController' , ['$scope' , function ($rootScope, $scope, $window, Token) {
		console.log(Token);
    $scope.accessToken = Token.get();

    $scope.authenticate = function() {	alert("testing");
      var extraParams = $scope.askApproval ? {approval_prompt: 'force'} : {};
      Token.getTokenByPopup(extraParams)
        .then(function(params) {
			console.log(params);
          // Success getting token from popup.

          // Verify the token before setting it, to avoid the confused deputy problem.
          Token.verifyAsync(params.access_token).
            then(function(data) {
              $rootScope.$apply(function() {
                $scope.accessToken = params.access_token;
                $scope.expiresIn = params.expires_in;

                Token.set(params.access_token);
              });
            }, function() {
              alert("Failed to verify token.")
            });

        }, function() {
          // Failure getting token from popup.
          alert("Failed to get token from popup.");
        });
    };
	}]);

  /* constant('GoogleTokenVerifier', function(config, accessToken) {
    var $injector = angular.injector(['ng']);
    return $injector.invoke(['$http', '$rootScope', '$q', function($http, $rootScope, $q) {
      var deferred = $q.defer();
      var verificationEndpoint = 'https://www.googleapis.com/oauth2/v1/tokeninfo';
      // var verificationEndpoint = 'https://www.google.com/m8/feeds/contacts/joe.smith@gmail.com/full';

      $rootScope.$apply(function() {
          $http({method: 'GET', url: verificationEndpoint, params: {access_token: accessToken}}).
          success(function(data) {
            if (data.audience == config.clientId) {
			console.log(data);
              deferred.resolve(data);
            } else {
              deferred.reject({name: 'invalid_audience'});
            }
          }).
          error(function(data, status, headers, config) {
            deferred.reject({
              name: 'error_response',
              data: data,
              status: status,
              headers: headers,
              config: config
            });
          }); 
      });
		$http.get('https://www.google.com/m8/feeds/contacts/default/full/?access_token=' + accessToken + "&alt=json").success( function(result){
					if (result.audience == config.clientId) {
						console.log(result);
						deferred.resolve(result);
					} else {
						deferred.reject({name: 'invalid_audience'});
					}
					console.log(JSON.stringify(result));
			}).error(function(data, status, headers, config) {
				deferred.reject({
              name: 'error_response',
              data: data,
              status: status,
              headers: headers,
              config: config
            });
          });
	 
      return deferred.promise;
    }]);
  }).

  config(function(TokenProvider, GoogleTokenVerifier) {
		TokenProvider.extendConfig({
		clientId: '507021478154.apps.googleusercontent.com',
		redirectUri: 'http://www.happyfuel.me/oauth2callback.html',  // allow lunching demo from a mirror
		// scopes: ["https://www.googleapis.com/auth/userinfo.email"]
		scopes: ["https://www.google.com/m8/feeds"],
		authorizationEndpoint: 'https://accounts.google.com/o/oauth2/auth',
		verifyFunc: GoogleTokenVerifier
    }); */
  });
// });