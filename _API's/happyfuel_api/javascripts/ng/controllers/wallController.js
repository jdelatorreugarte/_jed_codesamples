 /**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/25/13
 * Time: 8:41 AM
 */

 define(['ng/modules/mainApp', 'amd/app', 'amd/util'] , function (mainApp, amdApp, util) {
	 mainApp.controller('wallController' , ['$scope', '$rootScope', '$location' , function ($scope, $rootScope, $location) {
		 console.log('called from nested wall controller');
		 $scope.wallPostsResponse = null;
		 $scope.wallPostsIds = [];
		 $scope.postSizes = ['a','b','c','d'];
		 $scope.alignment = ['l','c','r'];
		 $scope.modules = [];
		 $scope.isLoadingModules = false;
         $scope.sharePost = null;

		 $rootScope.$watch('wallResponseData', function (newVal, oldVal) {
			 $scope.wallPostsResponse = $rootScope.wallResponseData;
		 });

		 $rootScope.$watch('selectedUser', function (newVal, oldVal) {
			 if (!$scope.isLoadingModules && newVal != null && newVal != '') {
				 $scope.modules = [];
				 $scope.wallPostsIds = [];
				 $scope.loadMoreModules();
			 }
		 });

		 $rootScope.$watch('selectedHashTag', function (newVal, oldVal) {
			 if (!$scope.isLoadingModules && newVal != null && newVal != '') {
				 $scope.modules = [];
				 $scope.wallPostsIds = [];
				 $rootScope.selectedPostType = 'T';
				 $scope.loadMoreModules();
			 }
		 });

		 $rootScope.$watch('selectedFuelType', function (newVal, oldVal) {
			 if (!$scope.isLoadingModules && newVal != null && newVal != '') {
				 $scope.modules = [];
				 $scope.wallPostsIds = [];
				 $scope.selectedUser = '';
                 $rootScope.selectedHashTag = '';
				 //$rootScope.selectedPostType = '-';
				 $scope.loadMoreModules();
                 amdApp.resetWallPosition();
                 amdApp.setFuelTypeMenuTitle(newVal);
			 }
		 });

		 $rootScope.$watch('selectedPostType', function (newVal, oldVal) {
			 if (!$scope.isLoadingModules && newVal != null && newVal != '') {
				 $scope.modules = [];
				 $scope.wallPostsIds = [];
				 $scope.selectedUser = '';
                 $rootScope.selectedHashTag = '';
				 //$rootScope.selectedFuelType = '';
				 $scope.loadMoreModules();
                 amdApp.resetWallPosition();
                 amdApp.setPostTypeMenuTitle(newVal);
			 }
		 });

		 $scope.randomizePostMetadata = function (postsList) {
			 for (var i=0; i<postsList.length; i++) {
				 postsList[i].postSize = $scope.getRandomSize();
				 postsList[i].align = $scope.getRandomAlignment();
			 }
		 };

		 $scope.getRandomSize = function () {
			 // leaving out size 'd' for now
			 return $scope.postSizes[Math.floor(Math.random()*3)];
		 };

		 $scope.getRandomAlignment = function () {
			 return $scope.alignment[Math.floor(Math.random()*3)];
		 };

		 $scope.ratePost = function() {
			 $scope.selectedRatingValue++;
			 $rootScope.selectedPost.FuelRatingCount = $scope.selectedRatingValue;
			 $scope.ratingValue = $scope.svc.post.getActiveRateValue();
			 $scope.svc.post.savePostRating(0, $rootScope.currentUser.UserId, $rootScope.selectedPost.FuelPostId, $scope.ratingValue, $scope.ratingComment)
				 .success(function(data, status) {
					 $rootScope.postRatings = data.Ratings;
				 })
				 .error(function(data, status) {
					 console.log(data, status);
				 });
			 console.log('rating:', $scope.selectedRatingValue);
		 };

		 $scope.$on(amdApp.eventTypes.onLoadMoreModules, function (event, args) {
			 $scope.loadMoreModules();
			 setTimeout(function () {amdApp.hasTriggeredScrollLoad = false;}, 2000);
		 });

		 $scope.loadMoreModules = function () {
			 console.log('requesting more modules');
			 $scope.isLoadingModules = true;
			 var randomModuleTypesToLoad = []
				 , wallModules = []
				 , pageSize = 0
				 , skipSize = $scope.wallPostsIds.length
				 , resultIteratorIndex = 0
				 , tagSearch = ($rootScope.selectedHashTag && $rootScope.selectedHashTag.length > 2) ? $rootScope.selectedHashTag : null
				 , selectedUserId = (isNaN($rootScope.selectedUser)) ? 0 : $rootScope.selectedUser
                 , postType = (tagSearch) ? 'T' : $rootScope.selectedPostType
                 , fuelType = $rootScope.selectedFuelType;

             // set the first module type to type 1
             if ($scope.modules.length === 0) {
                 randomModuleTypesToLoad.push(1);
             }

			 // need at least 16 modules for 4 across and 4 down
			 // since there are 6 module types, we'll load 18 random modules
			 for (var i=0; i<3; i++) {
				 randomModuleTypesToLoad.push.apply(randomModuleTypesToLoad, util.getArrayOfNonRepeatingRandomIntegers(1,6));
			 }
			 for (var i=0; i<(4*amdApp.modulesAcross); i++) {
				 var unitCount = amdApp.moduleTypes['m'+randomModuleTypesToLoad[i]].units;
				 wallModules.push({moduleTypeNumber: randomModuleTypesToLoad[i], unitCount: unitCount, unitPosts: []})
				 pageSize += unitCount;
			 }
			 $scope.svc.post.getWallPosts(fuelType, postType, selectedUserId, tagSearch, 1, pageSize, skipSize)
				 .success(function (data, status) {
				     //alert(pageSize, skipSize);
					 $scope.isLoadingModules = false;
					 if (data.WallPosts && data.WallPosts.length > 0) {
						 var postIndex = 0;
						 _.each(wallModules, function (module) {
							 module.unitPosts = data.WallPosts.slice(postIndex, postIndex + module.unitCount);
							 postIndex += module.unitCount;
						 });
						 $scope.modules.push.apply($scope.modules, wallModules);
						 _.each(data.WallPosts, function (item) {
							 $scope.wallPostsIds.push(item.FuelPostId);
						 });
						 console.log('wall modules loaded: ',wallModules);
						 // resize newly added modules
						 setTimeout(amdApp.sizeWallModules, 250);
					 }
				 })
				 .error(function (data, status) {
					 $scope.isLoadingModules = false;
				 });
		 };

		 $scope.init = function () {
             // handle shared deep link to post
             var shareUrlPath = /^\/fuel-post\/\d+$/gi;
             if (shareUrlPath.test($location.$$path)) {
                 var postId = $location.$$path.match(/\d*$/)[0];
                 hfsvc.post.getPost(postId)
                     .success(function (data, status) {
                         if (data.GetPostByIDResult.Post && data.GetPostByIDResult.Post.length > 0) {
                             $scope.sharePost = data.GetPostByIDResult.Post[0];
                             $scope.$apply();
                             amdApp.emulatePostClick($scope.sharePost.FuelPostId);
                         }
                     });
             }

			 amdApp.sizeWallModules().then(
				 function () {
					 if (amdApp.modulesAcross == 1) {
						 $rootScope.commentsPaneState = 'collapsed';
					 }
					 $rootScope.selectedPostType = 'H';
                     $scope.$broadcast(amdApp.eventTypes.onLoadMoreModules);
				 }
			 );
		 }
		 $scope.init();
	 }]);
 });