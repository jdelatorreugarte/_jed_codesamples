/**
 * Author: Wes Reid - www.kinesys180.com
 * Date: 8/14/13
 * Time: 12:08 AM
 */

define(['ng/modules/mainApp', 'amd/app'] , function (mainApp, amdApp) {
    mainApp.factory('hfService' , ['$resource', '$http', '$rootScope' , function ($rsrc, $http, $rootScope) {

	    var _svc = function () {}
		    , _svcRoot = '/HFWebServices.svc/';
		    // , _svcRoot = '/happyfuel_web/manish.php/';

	    $rootScope.wallResponseData = null;

	    _svc.prototype = {
			user: {
				getUser: function(userId){
					var url = _svcRoot+'GetUser/'+userId;
					return $http({method: 'GET', url: url});
				}
				, validateUser: function(username, password){
					var url = _svcRoot+'ValidateUser/'+username+'/'+password;
					return $http({method: 'GET', url: url});
				}
				, resetPassword: function(email) {
					//ResetPassword/kalyan10@gmail.com
					var url = _svcRoot+'ResetPassword/'+email;
					return $http({method: 'GET', url: url});
				}
				, changePassword: function(){
					///ChangePassword/kalyan10/kalyan10@gmail.com/Test100/730de540-cba1-4bda-ab93-ec957b062cd6
					var url = _svcRoot+'ChangePassword/'+userId;
					return $http({method: 'GET', url: url});
				}
				, saveUser: function (username, password, email) {
					//SaveUser/0/kalyan10/abcd1234/kalyan10@gmail.com
					var url = _svcRoot+'SaveUser/0/'+username+'/'+password+'/'+email;
					return $http({method: 'GET', url: url});
				}
				, updateUser: function (userId, username, password, email) {
					//UpdateUser/1/kalyan10/abcd1234/kalyan10@gmail.com
					var url = _svcRoot+'UpdateUser/'+userId+'/'+username+'/'+password+'/'+email;
					return $http({method: 'GET', url: url});
				}
				, isFollower: function(userId, friendId) {
					//IsFollower/{UserId}/{FriendId}
					var url = _svcRoot+'IsFollower/'+userId+'/'+friendId;
					return $http({method: 'GET', url: url});
				}
				, getFriendStatus: function (userId, friendId) {
					//GetFriendStatus/{UserId}/{FriendId}
					var url = _svcRoot+'GetFriendStatus/'+userId+'/'+friendId;
					return $http({method: 'GET', url: url});
				}
				, getFriendsByUserId: function (userId) {
					//”/GetFriendsByUserId/{UserId}”
					var url = _svcRoot+'GetFriendsByUserId/'+userId;
					return $http({method: 'GET', url: url});

				}
				, saveFriend: function (userFriendId, userId, friendId, statusId) {
					//”/SaveFriend/{UserFriendId}/{UserId}/{FriendId}/{StatusId}”
					var url = _svcRoot+'SaveFriend/'+userFriendId+'/'+userId+'/'+friendId+'/'+statusId;
					return $http({method: 'GET', url: url});

				}
				, deleteFriend: function (userFriendId, userId, friendId) {
				    //”/DeleteFriend/{UserFriendId}/{UserId}/{FriendId}”
					var url = _svcRoot+'DeleteFriend/'+userFriendId+'/'+userId+'/'+friendId;
					return $http({method: 'GET', url: url});
				}

                , deleteUser: function (userId) {
                    //DeleteUser/{UserId}
                    var url = _svcRoot + 'DeleteUser/' + userId;
                    return $http({ method: 'GET', url: url });

                }
				
				
				
			}

		    , search: {
			    searchTags: function (searchText) {
				    //TagSearch/{SearchChars}
				    var url = _svcRoot+'TagSearch/'+searchText;
				    return $http({method: 'GET', url: url});
			    }
			    , searchUsers: function (searchText) {
				    var url = _svcRoot+'SearchUsers/'+searchText;
				    //if (location.hostname === 'localhost')
					//    url = '/javascripts/json/MatchingUsers.json';
				    return $http({method: 'GET', url: url});
			    }
		    }

		    , post: {
			    getPost: function (postId) {
				    //GetPostByID/1
				    var url = _svcRoot+'GetPostByID/'+postId;
				    return $http({method: 'GET', url: url});
			    }
			    , getActiveRateValue: function () {
				    return amdApp.getCurrentRating();
			    }

			    , getWallPosts: function (fuelType, searchType, userId, searchTags, pg, pgSz, skip) {
				    fuelType = fuelType || '-';
				    searchType = searchType || 'H';
				    userId = userId || 0;
				    searchTags = searchTags || '-';
				    pg = pg || 1;
				    pgSz = pgSz || 32
				    skip = skip || 0;

				    if (skip > 0) {
				        pgSz = pgSz + skip;
				    }
			        //alert("pagesize=" + pgSz);
			        //alert("skip=" + skip);
				    var url = _svcRoot+'GetWallPosts/' + fuelType + '/' + searchType + '/' + userId + '/' + searchTags + '/' + pg + '/' + pgSz + '/' + skip;
				    //if (location.hostname === 'localhost')
					//    url = '/javascripts/json/WallPosts-Locations.json';
				    //alert(url);
				    return $http({method: 'GET', url: url});
			    }
		        // ”/GetFuelRatingsByPostId/{PostId}”
			    , getPostRatings: function (postId) {

				    var url = _svcRoot+'GetFuelRatingsByPostId/' + postId;
				    return $http({method: 'GET', url: url});
			    }

			    , getPostRatingsByUserId: function (userId) {
				    //GetFuelRatingsByUserId/{UserId}
				    var url = _svcRoot+'GetFuelRatingsByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }

			    //"/SavePostRating/{RatingId}/{UserId}/{FuelPostId}/{FuelRating}/{Comments}/{IsPublic}"
			    , savePostRating: function (ratingId, userId, fuelPostId, fuelRating, comments, isPublic, hashTags, userTags) {

				    ratingId = ratingId || 0;
				    userId = userId || 1;
				    fuelPostId = fuelPostId || 1;
				    fuelRating = fuelRating || 1;
				    comments = comments || '-';
				    isPublic = isPublic || 0;
				    hashTags = hashTags || '-';
				    userTags = userTags || '-';

                    var hashtagPattern = /#\S*/gi
                        , matches = comments.match(hashtagPattern);

                    if (matches !== null && matches.length > 0)
                        //hashTags = matches.join(',').replace(/#/gi, '')
                        hashTags = matches.join(',');

                    var url = _svcRoot+'SavePostRatingByPOST'; // + ratingId + '/' + userId + '/' + fuelPostId + '/' + fuelRating + '/' + encodeURIComponent(comments) + '/' + isPublic + '/' + hashTags + '/' + userTags;

				    return $http.post(url, {
                        RatingId: ratingId
                        , UserId: userId
                        , FuelPostId: fuelPostId
                        , FuelRating: fuelRating
                        , Comments: comments
                        , IsPublic: isPublic
                        , HashTags: hashTags
                        , UserTags: userTags
                    });
			    }

			    //"/SavePostRating/{RatingId}/{UserId}/{FuelPostId}/{FuelRating}/{Comments}/{IsPublic}"
			    , deleteRating: function (ratingId) {

				    ratingId = ratingId || 0;

				    var url = _svcRoot+'DeleteRating/' + ratingId;

				    return $http({method: 'GET', url: url});
				    /*
				     .success(function(data, status) {
				     $rootScope.postRatings = data;
				     })
				     .error(function(data, status) {
				     console.log(data, status);
				     });
				     */
			    }

			    , getPostHashTags: function(postId) {
				    //GetHashTags/{FuelPostId}
				    var url = _svcRoot+'GetHashTags/'+postId;
				    return $http({method: 'GET', url: url});
			    }

			    , getPostUserTags: function(postId) {
				    //GetUserTags/{FuelPostId}
				    var url = _svcRoot+'GetUserTags/'+postId;
				    return $http({method: 'GET', url: url});
			    }

			    , getUserPosts: function (userId, limit) {
				    //GetPostsByUser/1/1
				    limit = limit || 10;
				    var url = _svcRoot+'GetPostsByUser/'+userId+'/'+limit;
				    return $http({method: 'GET', url: url});
			    }
				
				, ReportPost: function (userId, PostId) {
				    //Report post 
				    var url = _svcRoot+'ReportPost/'+userId+'/'+PostId;
				    return $http({method: 'GET', url: url});
			    }
				
		    }
		    /*

		     Dashboard




		     */
		    , dashboard: {
			    getPostsByTypeByUserId: function (userId) {
				    //GetPostsByTypeByUserId/{UserId}
				    var url = _svcRoot+'GetPostsByTypeByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }
			    , getTopPostsByUserId: function (userId) {
				    //GetTopPostsByUserId/{UserId}
				    var url = _svcRoot+'GetTopPostsByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }
			    , getTopPostsByTypeByRatingByUserId: function (userId) {
				    //GetTopPostsByTypeByRatingByUserId/{UserId}
				    var url = _svcRoot+'GetTopPostsByTypeByRatingByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }
			    , getFavoritePostsByUserId: function (userId) {
				    //GetFavoritePostsByUserId/{UserId}
				    var url = _svcRoot+'GetFavoritePostsByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }
			    , getTopFansByUserId: function (userId) {
				    //GetBiggestFansByUserId/{UserId}
				    var url = _svcRoot+'GetBiggestFansByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }
			    , getFavoriteFuelersByUserId: function (userId) {
				    //GetFavoriteFuelersByUserId/{UserId}
				    var url = _svcRoot+'GetFavoriteFuelersByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }
			    , getUserFuelersByUserId: function (userId) {
				    //GetUserFuelersByUserId/{UserId}
				    var url = _svcRoot+'GetUserFuelersByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }
			    , getUserFollowedFuelersByUserId: function (userId) {
				    //GetUserFollowedFuelersByUserId/{UserId}
				    var url = _svcRoot+'GetUserFollowedFuelersByUserId/'+userId;
				    return $http({method: 'GET', url: url});
			    }
		    }
			
			/* 
				google auth
			*/
			,googleauth: {
			    GetAlertsByUserContacts: function (userId,emailsString) {
				    //GetPostsByTypeByUserId/{UserId}
				    var url = _svcRoot+'GetAlertsByUserContacts/'+userId+'/'+emailsString;
				    return $http({method: 'GET', url: url});
			    },
				GetAlertsByUserContactsByPOST: function (userId,emailsString) {
				    //GetPostsByTypeByUserId/{UserId}
				    var url = _svcRoot+'GetAlertsByUserContactsByPOST';
				    return $http.post(url,{UserId:userId,Emails:emailsString});
			    }
		    }
			
			/* 
			Invite User
			*/
			, userInfo :{
				 inviteFriend: function(userId,userEmail) {
					var url = _svcRoot + 'InviteFriend/' + userId+'/'+userEmail;
					return $http({method: 'GET', url: url});
				}
				,followFriend: function(userId,friendUserId) {
					var url = _svcRoot + 'SaveFriend/0/' + userId+'/'+friendUserId+'/1';
					return $http({method: 'GET', url: url});
				}
				,unfollowFriend: function(userId,friendUserId) {
					var url = _svcRoot + 'SaveFriend/0/' + userId+'/'+friendUserId+'/6';
					return $http({method: 'GET', url: url});
				}
			}
			
			/* 
				yahoo
			*/
			, yahoo:{
				getyahootoken: function() {
					var url = "/yahooinvites/process.php";
					// var url = _svcRoot + 'InviteFriend/' + userId+'/'+userEmail;
					return $http({method: 'GET', url: url});
				}
			}
	    };
	    window.hfsvc = new _svc();
	    return _svc;
    }]);
});