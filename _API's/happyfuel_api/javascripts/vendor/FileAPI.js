/**!
 * FileAPI — a set of tools for working with files
 *
 * @author  RubaXa  <trash@rubaxa.org>
 * @build	lib/canvas-to-blob lib/FileAPI.core lib/FileAPI.Image lib/FileAPI.Form lib/FileAPI.XHR lib/FileAPI.Flash
 */
(function (a) {
	var m = a.HTMLCanvasElement && a.HTMLCanvasElement.prototype,
		g;
	if (g = a.Blob) try {
		g = Boolean(new Blob)
	} catch (s) {
		g = !1
	}
	var p = g;
	if (g = p)
		if (g = a.Uint8Array) try {
			g = 100 === (new Blob([new Uint8Array(100)])).size
		} catch (f) {
			g = !1
		}
	var c = g, e = a.BlobBuilder || a.WebKitBlobBuilder || a.MozBlobBuilder || a.MSBlobBuilder, n = (p || e) && a.atob && a.ArrayBuffer && a.Uint8Array && function (a) {
		var r, f, g, n;
		r = 0 <= a.split(",")[0].indexOf("base64") ? atob(a.split(",")[1]) : decodeURIComponent(a.split(",")[1]);
		f = new ArrayBuffer(r.length);
		g = new Uint8Array(f);
		for (n = 0; n < r.length; n += 1) g[n] = r.charCodeAt(n);
		a = a.split(",")[0].split(":")[1].split(";")[0];
		if (p) return new Blob([c ? g : f], {
			type: a
		});
		g = new e;
		g.append(f);
		return g.getBlob(a)
	};
	a.HTMLCanvasElement && !m.toBlob && (m.mozGetAsFile ? m.toBlob = function (a, c) {
		a(this.mozGetAsFile("blob", c))
	} : m.toDataURL && n && (m.toBlob = function (a, c) {
		a(n(this.toDataURL(c)))
	}));
	a.dataURLtoBlob = n
})(this);
(function (a, m) {
	function g(b, t, d) {
		if (b)
			if (w(b))
				for (var a = 0, c = b.length; a < c; a++) a in b && t.call(d, b[a], a, b);
			else
				for (a in b) b.hasOwnProperty(a) && t.call(d, b[a], a, b)
	}

	function s(b, t, d) {
		if (b) {
			var a = k.uid(b);
			B[a] || (B[a] = {});
			g(t.split(/\s+/), function (t) {
				x ? x.event.add(b, t, d) : (B[a][t] || (B[a][t] = []), B[a][t].push(d), b.addEventListener ? b.addEventListener(t, d, !1) : b.attachEvent ? b.attachEvent("on" + t, d) : b["on" + t] = d)
			})
		}
	}

	function p(b, t, d) {
		if (b) {
			var a = k.uid(b),
				c = B[a] || {};
			g(t.split(/\s+/), function (t) {
				if (x) x.event.remove(b,
					t, d);
				else {
					for (var a = c[t] || [], h = a.length; h--;)
						if (a[h] === d) {
							a.splice(h, 1);
							break
						}
					b.addEventListener ? b.removeEventListener(t, d, !1) : b.detachEvent ? b.detachEvent("on" + t, d) : b["on" + t] = null
				}
			})
		}
	}

	function f(b, t, d) {
		s(b, t, function R(a) {
			p(b, t, R);
			d(a)
		})
	}

	function c(b, t, d, a, c) {
		b = {
			type: d.type || d,
			target: b,
			result: a
		};
		k.extend(b, c);
		t(b)
	}

	function e(b, t, d, a) {
		if (k.isFile(b) && v && v.prototype["readAs" + d]) {
			var h = new v;
			s(h, K, function S(d) {
				var a = d.type;
				"progress" == a ? c(b, t, d, d.target.result, {
					loaded: d.loaded,
					total: d.total
				}) : "loadend" ==
					a ? (p(h, K, S), h = null) : c(b, t, d, d.target.result)
			});
			try {
				if (a) h["readAs" + d](a, b);
				else h["readAs" + d](b)
			} catch (e) {
				c(b, t, "error", m, {
					error: e.toString()
				})
			}
		} else c(b, t, "error", m, {
			error: "filreader_not_support_" + d
		})
	}

	function n(b, d) {
		if (!b.type && 0 == b.size % 4096 && 102400 >= b.size)
			if (v) try {
				var a = new v;
				f(a, K, function (b) {
					b = "error" != b.type;
					d(b);
					b && a.abort()
				});
				a.readAsDataURL(b)
			} catch (c) {
				d(!1)
			} else d(null);
		else d(!0)
	}

	function q(b) {
		var d;
		b.getAsEntry ? d = b.getAsEntry() : b.webkitGetAsEntry && (d = b.webkitGetAsEntry());
		return d
	}

	function r(b,
	           d) {
		if (b)
			if (b.isFile) b.file(function (a) {
				a.fullPath = b.fullPath;
				d(!1, [a])
			}, function () {
				d("entry_file")
			});
			else if (b.isDirectory) {
				var a = [];
				b.createReader().readEntries(function (b) {
					k.afor(b, function (b, c) {
						r(c, function (c, h) {
							c || (a = a.concat(h));
							b ? b() : d(!1, a)
						})
					})
				}, function () {
					d("directory_reader")
				})
			} else r(q(b), d);
		else d("empty_entry")
	}

	function w(b) {
		return "object" == typeof b && b && "length" in b
	}

	function A(b) {
		var d = {};
		g(b, function (b, a) {
			b && "object" === typeof b && (b = k.extend({}, b));
			d[a] = b
		});
		return d
	}

	function C(b) {
		b.target ||
		(b.target = a.event && a.event.srcElement || L);
		3 === b.target.nodeType && (b.target = event.target.parentNode);
		return b
	}
	var l = 1,
		d = function () {}, h = navigator.userAgent,
		u = a.createObjectURL && a || a.URL && URL.revokeObjectURL && URL || a.webkitURL && webkitURL,
		D = a.Blob,
		E = a.File,
		v = a.FileReader,
		F = a.FormData,
		I = a.XMLHttpRequest,
		x = a.jQuery,
		y = !! (E && v && (a.Uint8Array || F || I.prototype.sendAsBinary)) && !(/safari\//i.test(h) && !/chrome\//i.test(h) && /windows/i.test(h)),
		h = y && "withCredentials" in new I,
		D = y && !! D && !! (D.prototype.webkitSlice ||
			D.prototype.mozSlice || D.prototype.slice),
		L = a.document,
		N = a.dataURLtoBlob,
		V = /img/i,
		W = /canvas/i,
		X = /img|canvas/,
		M = /input/i,
		Y = /^data:[^,]+,/,
		G = Math.pow,
		Z = Math.round,
		J = Number,
		F = function (b) {
			return Z(b * this)
		}, H = new J(1024),
		O = new J(G(H, 2)),
		P = new J(G(H, 3)),
		G = new J(G(H, 4)),
		B = {}, Q = [],
		K = "abort progress error load loadend",
		$ = "status statusText readyState response responseXML responseText responseBody".split(" "),
		k = {
			version: "1.2.6",
			cors: !1,
			html5: !0,
			debug: !1,
			pingUrl: !1,
			flashAbortTimeout: 0,
			withCredentials: !0,
			staticPath: "./",
			flashUrl: 0,
			flashImageUrl: 0,
			ext2mime: {
				jpg: "image/jpeg",
				tif: "image/tiff"
			},
			accept: {
				"image/*": "art bm bmp dwg dxf cbr cbz fif fpx gif ico iefs jfif jpe jpeg jpg jps jut mcf nap nif pbm pcx pgm pict pm png pnm qif qtif ras rast rf rp svf tga tif tiff xbm xbm xpm xwd",
				"audio/*": "m4a flac aac rm mpa wav wma ogg mp3 mp2 m3u mod amf dmf dsm far gdm imf it m15 med okt s3m stm sfx ult uni xm sid ac3 dts cue aif aiff wpl ape mac mpc mpp shn wv nsf spc gym adplug adx dsp adp ymf ast afc hps xs",
				"video/*": "m4v 3gp nsv ts ty strm rm rmvb m3u ifo mov qt divx xvid bivx vob nrg img iso pva wmv asf asx ogm m2v avi bin dat dvr-ms mpg mpeg mp4 mkv avc vp3 svq3 nuv viv dv fli flv wpl"
			},
			chunkSize: 0,
			chunkUploadRetry: 0,
			chunkNetworkDownRetryTimeout: 2E3,
			KB: (H.from = F, H),
			MB: (O.from = F, O),
			GB: (P.from = F, P),
			TB: (G.from = F, G),
			expando: "fileapi" + (new Date).getTime(),
			uid: function (b) {
				return b ? b[k.expando] = b[k.expando] || k.uid() : (++l, k.expando + l)
			},
			log: function () {
				k.debug && (a.console && console.log) && (console.log.apply ? console.log.apply(console, arguments) : console.log([].join.call(arguments, " ")))
			},
			newImage: function (b, d) {
				var a = L.createElement("img");
				if (d) k.event.one(a, "error load", function (b) {
					d("error" ==
						b.type, a);
					a = null
				});
				a.src = b;
				return a
			},
			getXHR: function () {
				var b;
				if (I) b = new I;
				else if (a.ActiveXObject) try {
					b = new ActiveXObject("MSXML2.XMLHttp.3.0")
				} catch (d) {
					b = new ActiveXObject("Microsoft.XMLHTTP")
				}
				return b
			},
			isArray: w,
			support: {
				dnd: h && "ondrop" in L.createElement("div"),
				cors: h,
				html5: y,
				chunked: D,
				dataURI: !0
			},
			event: {
				on: s,
				off: p,
				one: f,
				fix: C
			},
			throttle: function (b, d) {
				var c, h;
				return function () {
					h = arguments;
					c || (b.apply(a, h), c = setTimeout(function () {
						c = 0;
						b.apply(a, h)
					}, d))
				}
			},
			F: function () {},
			parseJSON: function (b) {
				return a.JSON &&
					JSON.parse ? JSON.parse(b) : (new Function("return (" + b.replace(/([\r\n])/g, "\\$1") + ");"))()
			},
			trim: function (b) {
				b = String(b);
				return b.trim ? b.trim() : b.replace(/^\s+|\s+$/g, "")
			},
			defer: function () {
				var b = [],
					a, c, h = {
						resolve: function (e, f) {
							h.resolve = d;
							c = e || !1;
							for (a = f; f = b.shift();) f(c, a)
						},
						then: function (d) {
							c !== m ? d(c, a) : b.push(d)
						}
					};
				return h
			},
			queue: function (b) {
				var d = 0,
					a = 0,
					c = !1,
					h = !1,
					e = {
						inc: function () {
							a++
						},
						next: function () {
							d++;
							setTimeout(e.check, 0)
						},
						check: function () {
							d >= a && !c && e.end()
						},
						isFail: function () {
							return c
						},
						fail: function () {
							!c &&
							b(c = !0)
						},
						end: function () {
							h || (h = !0, b())
						}
					};
				return e
			},
			each: g,
			afor: function (b, d) {
				var a = 0,
					c = b.length;
				w(b) && c-- ? function z() {
					d(c != a && z, b[a], a++)
				}() : d(!1)
			},
			extend: function (b) {
				g(arguments, function (d) {
					g(d, function (d, a) {
						b[a] = d
					})
				});
				return b
			},
			isFile: function (b) {
				return y && b && b instanceof E
			},
			isCanvas: function (b) {
				return b && W.test(b.nodeName)
			},
			getFilesFilter: function (b) {
				return (b = "string" == typeof b ? b : b.getAttribute && b.getAttribute("accept") || "") ? RegExp("(" + b.replace(/\./g, "\\.").replace(/,/g, "|") + ")$", "i") : /./
			},
			readAsDataURL: function (b,
			                         d) {
				k.isCanvas(b) ? c(b, d, "load", k.toDataURL(b)) : e(b, d, "DataURL")
			},
			readAsBinaryString: function (b, d) {
				v && v.prototype.readAsBinaryString ? e(b, d, "BinaryString") : e(b, function (b) {
					if ("load" == b.type) try {
						b.result = k.toBinaryString(b.result)
					} catch (a) {
						b.type = "error", b.message = a.toString()
					}
					d(b)
				}, "DataURL")
			},
			readAsArrayBuffer: function (b, d) {
				e(b, d, "ArrayBuffer")
			},
			readAsText: function (b, d, a) {
				a || (a = d, d = "utf-8");
				e(b, a, "Text", d)
			},
			toDataURL: function (b) {
				if ("string" == typeof b) return b;
				if (b.toDataURL) return b.toDataURL("image/png")
			},
			toBinaryString: function (b) {
				return a.atob(k.toDataURL(b).replace(Y, ""))
			},
			readAsImage: function (b, d, a) {
				if (k.isFile(b))
					if (u) {
						var h = u.createObjectURL(b);
						h === m ? c(b, d, "error") : k.readAsImage(h, d, a)
					} else k.readAsDataURL(b, function (h) {
						"load" == h.type ? k.readAsImage(h.result, d, a) : (a || "error" == h.type) && c(b, d, h, null, {
							loaded: h.loaded,
							total: h.total
						})
					});
				else k.isCanvas(b) ? c(b, d, "load", b) : V.test(b.nodeName) ? b.complete ? c(b, d, "load", b) : f(b, "error abort load", function z(a) {
					"load" == a.type && u && u.revokeObjectURL(b.src);
					p(b,
						"error abort load", z);
					c(b, d, a, b)
				}) : b.iframe ? c(b, d, {
					type: "error"
				}) : (h = k.newImage(b.dataURL || b), k.readAsImage(h, d, a))
			},
			checkFileObj: function (b) {
				var d = {}, a = k.accept;
				"object" == typeof b ? d = b : d.name = (b + "").split(/\\|\//g).pop();
				null == d.type && (d.type = d.name.split(".").pop());
				g(a, function (b, a) {
					b = RegExp(b.replace(/\s/g, "|"), "i");
					b.test(d.type) && (d.type = k.ext2mime[d.type] || a.split("/")[0] + "/" + d.type)
				});
				return d
			},
			getDropFiles: function (b, d) {
				var a = [],
					c = (b.originalEvent || b || "").dataTransfer || {}, h = w(c.items) && c.items[0] &&
						q(c.items[0]),
					e = k.queue(function () {
						d(a)
					});
				g((h ? c.items : c.files) || [], function (b) {
					e.inc();
					try {
						h ? r(b, function (b, d) {
							!b && a.push.apply(a, d);
							e.next()
						}) : n(b, function (d) {
							d && a.push(b);
							e.next()
						})
					} catch (d) {
						e.next(), k.log("getDropFiles.error:", d.toString())
					}
				});
				e.check()
			},
			getFiles: function (b, d, a) {
				var c = [];
				if (a) return k.filterFiles(k.getFiles(b), d, a), null;
				b.jquery && (b.each(function () {
					c = c.concat(k.getFiles(this))
				}), b = c, c = []);
				"string" == typeof d && (d = k.getFilesFilter(d));
				b.originalEvent ? b = C(b.originalEvent) : b.srcElement &&
					(b = C(b));
				b.dataTransfer ? b = b.dataTransfer : b.target && (b = b.target);
				b.files ? (c = b.files, y || (c[0].blob = b, c[0].iframe = !0)) : !y && M.test(b && b.tagName) ? k.trim(b.value) && (c = [k.checkFileObj(b.value)], c[0].blob = b, c[0].iframe = !0) : w(b) && (c = b);
				return k.filter(c, function (b) {
					return !d || d.test(b.name)
				})
			},
			getInfo: function (b, d) {
				var a = {}, c = Q.concat();
				k.isFile(b) ? function z() {
					var h = c.shift();
					h ? h.test(b.type) ? h(b, function (b, c) {
						b ? d(b) : (k.extend(a, c), z())
					}) : z() : d(!1, a)
				}() : d("not_support", a)
			},
			addInfoReader: function (b, d) {
				d.test =
					function (d) {
						return b.test(d)
					};
				Q.push(d)
			},
			filter: function (b, d) {
				for (var a = [], c = 0, h = b.length, e; c < h; c++) c in b && (e = b[c], d.call(e, e, c, b) && a.push(e));
				return a
			},
			filterFiles: function (b, d, a) {
				if (b.length) {
					var c = b.concat(),
						h, e = [],
						f = [];
					(function T() {
						c.length ? (h = c.shift(), k.getInfo(h, function (b, a) {
							(d(h, b ? !1 : a) ? e : f).push(h);
							T()
						})) : a(e, f)
					})()
				} else a([], b)
			},
			upload: function (b) {
				b = k.extend({
					prepare: k.F,
					beforeupload: k.F,
					upload: k.F,
					fileupload: k.F,
					fileprogress: k.F,
					filecomplete: k.F,
					progress: k.F,
					complete: k.F,
					pause: k.F,
					chunkSize: k.chunkSize,
					chunkUpoloadRetry: k.chunkUploadRetry
				}, b);
				b.imageAutoOrientation && !b.imageTransform && (b.imageTransform = {
					rotate: "auto"
				});
				var a = new k.XHR(b),
					c = this._getFilesDataArray(b.files),
					h = 0,
					e = 0,
					f = this,
					u, r = !1;
				g(c, function (b) {
					h += b.size
				});
				a.files = [];
				g(c, function (b) {
					a.files.push(b.file)
				});
				a.total = h;
				a.loaded = 0;
				a.filesLeft = c.length;
				b.beforeupload(a, b);
				(u = function U() {
					var u = c.shift(),
						q = u && u.file,
						l = !1,
						w = A(b);
					a.filesLeft = c.length;
					q && q.name === k.expando && (q = null, k.log("[warn] FileAPI.upload() \u2014 called without files"));
					("abort" != a.statusText || a.current) && u ? (r = !1, (a.currentFile = q) && b.prepare(q, w), this._getFormData(w, u, function (r) {
						e || b.upload(a, b);
						var p = new k.XHR(k.extend({}, w, {
							upload: q ? function () {
								b.fileupload(q, p, w)
							} : d,
							progress: q ? function (d) {
								l || (b.fileprogress({
									type: "progress",
									total: u.total = d.total,
									loaded: u.loaded = d.loaded
								}, q, p, w), b.progress({
									type: "progress",
									total: h,
									loaded: a.loaded = e + u.size * (d.loaded / d.total) | 0
								}, q, p, w))
							} : d,
							complete: function (d) {
								l = !0;
								g($, function (b) {
									a[b] = p[b]
								});
								q && (u.loaded = u.total, this.progress(u),
									e += u.size, a.loaded = e, b.filecomplete(d, p, q, w));
								U.call(f)
							}
						}));
						a.abort = function (b) {
							b || (c.length = 0);
							this.current = b;
							p.abort()
						};
						p.send(r)
					})) : (b.complete(200 == a.status || 201 == a.status ? !1 : a.statusText || "error", a, b), r = !0)
				}).call(this);
				a.append = function (b, d) {
					b = k._getFilesDataArray([].concat(b));
					g(b, function (b) {
						h += b.size;
						a.files.push(b.file);
						d ? c.unshift(b) : c.push(b)
					});
					a.statusText = "";
					r && u.call(f)
				};
				a.remove = function (b) {
					var d = -1;
					g(c, function (a) {
						d++;
						if (a.file == b) return c.splice(d, 1)
					})
				};
				return a
			},
			_getFilesDataArray: function (b) {
				var d = [],
					a = {};
				if (M.test(b && b.tagName)) {
					var c = k.getFiles(b);
					a[b.name || "file"] = null !== b.getAttribute("multiple") ? c : c[0]
				} else w(b) && M.test(b[0] && b[0].tagName) ? g(b, function (b) {
					a[b.name || "file"] = k.getFiles(b)
				}) : a = b;
				g(a, function z(b, a) {
					w(b) ? g(b, function (b, d) {
						z(b, a)
					}) : b && b.name && d.push({
						name: a,
						file: b,
						size: b.size,
						total: b.size,
						loaded: 0
					})
				});
				d.length || d.push({
					file: {
						name: k.expando
					}
				});
				return d
			},
			_getFormData: function (b, d, a) {
				var c = d.file,
					h = d.name,
					e = c.name,
					u = c.type;
				d = k.support.transform && b.imageTransform;
				var f = new k.Form,
					q = k.queue(function () {
						a(f)
					}),
					r = d && (0 < parseInt(d.maxWidth || d.minWidth || d.width, 10) || d.rotate);
				k.Image && d && (/image/.test(c.type) || X.test(c.nodeType)) ? (q.inc(), r && (d = [d]), k.Image.transform(c, d, b.imageAutoOrientation, function (d, a) {
					r && !d ? (N || k.flashEngine || (a[0] = k.toBinaryString(a[0]), f.multipart = !0), f.append(h, a[0], e, u)) : (d || (g(a, function (b, d) {
						N || k.flashEngine || (b = k.toBinaryString(b), f.multipart = !0);
						f.append(h + "[" + d + "]", b, e, u)
					}), h += "[original]"), (d || b.imageOriginal) && f.append(h, c, e, u));
					q.next()
				})) : e !==
					k.expando && f.append(h, c, e);
				g(b.data, function aa(b, d) {
					"object" == typeof b ? g(b, function (b, a) {
						aa(b, d + "[" + a + "]")
					}) : f.append(d, b)
				});
				q.check()
			},
			reset: function (b, d) {
				var a, c;
				x ? (c = x(b).clone(!0).insertBefore(b).val("")[0], d || x(b).remove()) : (a = b.parentNode, c = a.insertBefore(b.cloneNode(!0), b), c.value = "", d || a.removeChild(b), g(B[k.uid(b)], function (d, a) {
					g(d, function (d) {
						p(b, a, d);
						s(c, a, d)
					})
				}));
				return c
			},
			load: function (b, d) {
				var a = k.getXHR();
				a ? (a.open("GET", b, !0), a.overrideMimeType && a.overrideMimeType("text/plain; charset=x-user-defined"),
					s(a, "progress", function (b) {
						b.lengthComputable && d({
							type: b.type,
							loaded: b.loaded,
							total: b.total
						}, a)
					}), a.onreadystatechange = function () {
					if (4 == a.readyState)
						if (a.onreadystatechange = null, 200 == a.status) {
							b = b.split("/");
							var c = {
								name: b[b.length - 1],
								size: a.getResponseHeader("Content-Length"),
								type: a.getResponseHeader("Content-Type")
							};
							c.dataURL = "data:" + c.type + ";base64," + k.encode64(a.responseBody || a.responseText);
							d({
								type: "load",
								result: c
							})
						} else d({
							type: "error"
						})
				}, a.send(null)) : d({
					type: "error"
				});
				return a
			},
			encode64: function (b) {
				var d =
						"",
					a = 0;
				for ("string" !== typeof b && (b = String(b)); a < b.length;) {
					var c = b.charCodeAt(a++) & 255,
						h = b.charCodeAt(a++) & 255,
						e = b.charCodeAt(a++) & 255,
						f = c >> 2,
						c = (c & 3) << 4 | h >> 4;
					isNaN(h) ? h = e = 64 : (h = (h & 15) << 2 | e >> 6, e = isNaN(e) ? 64 : e & 63);
					d += "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(f) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(c) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(h) + "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=".charAt(e)
				}
				return d
			}
		};
	k.addInfoReader(/^image/, function (b, d) {
		if (!b.__dimensions) {
			var a = b.__dimensions = k.defer();
			k.readAsImage(b, function (b) {
				var d = b.target;
				a.resolve("load" == b.type ? !1 : "error", {
					width: d.width,
					height: d.height
				})
			})
		}
		b.__dimensions.then(d)
	});
	k.event.dnd = function (b, d, a) {
		var c, h;
		a || (a = d, d = k.F);
		v ? (s(b, "dragenter dragleave dragover", function (b) {
			for (var a = ((b.originalEvent || b || "").dataTransfer || {}).types, e = a && a.length; e--;)~ a[e].indexOf("File") && (b.preventDefault(), h !== b.type && (h = b.type, "dragleave" != h && d.call(b.currentTarget, !0, b), clearTimeout(c), c = setTimeout(function () {
				d.call(b.currentTarget, "dragleave" != h, b)
			}, 50)))
		}), s(b, "drop", function (b) {
			b.preventDefault();
			h = 0;
			d.call(b.currentTarget, !1, b);
			k.getDropFiles(b, function (d) {
				a.call(b.currentTarget, d, b)
			})
		})) : k.log("Drag'n'Drop -- not supported")
	};
	x && !x.fn.dnd && (x.fn.dnd = function (b, d) {
		return this.each(function () {
			k.event.dnd(this, b, d)
		})
	});
	a.FileAPI = k.extend(k, a.FileAPI);
	k.flashUrl || (k.flashUrl = k.staticPath + "FileAPI.flash.swf");
	k.flashImageUrl || (k.flashImageUrl = k.staticPath +
		"FileAPI.flash.image.swf")
})(window);
(function (a, m, g) {
	function s(a, c) {
		if (!(this instanceof s)) return new s(a);
		this.file = a;
		this.better = !c;
		this.matrix = {
			sx: 0,
			sy: 0,
			sw: 0,
			sh: 0,
			dx: 0,
			dy: 0,
			dw: 0,
			dh: 0,
			resize: 0,
			deg: 0
		}
	}
	var p = Math.min,
		f = Math.round,
		c = !1,
		e = {
			8: 270,
			3: 180,
			6: 90
		};
	try {
		c = -1 < m.createElement("canvas").toDataURL("image/png").indexOf("data:image/png")
	} catch (n) {}
	s.prototype = {
		constructor: s,
		set: function (c) {
			a.extend(this.matrix, c);
			return this
		},
		crop: function (a, c, e, f) {
			e === g && (e = a, f = c, a = c = 0);
			return this.set({
				sx: a,
				sy: c,
				sw: e,
				sh: f || e
			})
		},
		resize: function (a,
		                  c, e) {
			"string" == typeof c && (e = c, c = a);
			return this.set({
				dw: a,
				dh: c,
				resize: e
			})
		},
		preview: function (a, c) {
			return this.set({
				dw: a,
				dh: c || a,
				resize: "preview"
			})
		},
		rotate: function (a) {
			return this.set({
				deg: a
			})
		},
		_load: function (c, e) {
			var f = this;
			a.readAsImage(c, function (a) {
				e.call(f, "load" != a.type, a.result)
			})
		},
		_apply: function (a, c) {
			var e = m.createElement("canvas"),
				f = this.getMatrix(a),
				g = e.getContext("2d"),
				l = f.deg,
				d = f.dw,
				h = f.dh,
				u = a.width,
				p = a.height,
				n, v = a;
			if (this.better)
				for (; 2 < Math.min(u / d, p / h);) u = ~~ (u / 2 + 0.5), p = ~~ (p / 2 + 0.5), n =
					m.createElement("canvas"), n.width = u, n.height = p, v !== a ? (n.getContext("2d").drawImage(v, 0, 0, v.width, v.height, 0, 0, u, p), v = n) : (v = n, v.getContext("2d").drawImage(a, f.sx, f.sy, f.sw, f.sh, 0, 0, u, p), f.sx = f.sy = f.sw = f.sh = 0);
			e.width = l % 180 ? h : d;
			e.height = l % 180 ? d : h;
			g.rotate(l * Math.PI / 180);
			g.drawImage(v, f.sx, f.sy, f.sw || v.width, f.sh || v.height, 180 == l || 270 == l ? -d : 0, 90 == l || 180 == l ? -h : 0, d, h);
			c.call(this, !1, e)
		},
		getMatrix: function (c) {
			var e = a.extend({}, this.matrix),
				g = e.sw = e.sw || c.width;
			c = e.sh = e.sh || c.height;
			var n = e.dw = e.dw || e.sw,
				m = e.dh = e.dh || e.sh,
				l = g / c,
				d = n / m,
				h = e.resize;
			if ("preview" == h) {
				if (n != g || m != c)
					if (d >= l ? (l = g, h = l / d) : (h = c, l = h * d), l != g || h != c) e.sx = ~~ ((g - l) / 2), e.sy = ~~ ((c - h) / 2), g = l, c = h
			} else h && (g > n || c > m ? "min" == h ? (n = f(l < d ? p(g, n) : m * l), m = f(l < d ? n / l : p(c, m))) : (n = f(l >= d ? p(g, n) : m * l), m = f(l >= d ? n / l : p(c, m))) : (n = g, m = c));
			e.sw = g;
			e.sh = c;
			e.dw = n;
			e.dh = m;
			return e
		},
		_trans: function (a) {
			this._load(this.file, function (c, e) {
				c ? a(c) : this._apply(e, a)
			})
		},
		get: function (c) {
			if (a.support.transform) {
				var f = this;
				"auto" == f.matrix.deg ? a.getInfo(this.file, function (a,
				                                                        g) {
					f.matrix.deg = e[g && g.exif && g.exif.Orientation] || 0;
					f._trans(c)
				}) : f._trans(c)
			} else c("not_support")
		},
		toData: function (a) {
			this.get(a)
		}
	};
	s.exifOrientation = e;
	s.transform = function (c, e, f, n) {
		a.getInfo(c, function (p, l) {
			var d = {}, h = a.queue(function (a) {
				n(a, d)
			});
			p ? h.fail() : a.each(e, function (a, e) {
				if (!h.isFail()) {
					var n = s(l.nodeType ? l : c);
					if ("function" == typeof a) a(l, n);
					else if (a.width) n[a.preview ? "preview" : "resize"](a.width, a.height, a.type);
					else a.maxWidth && (l.width > a.maxWidth || l.height > a.maxHeight) && n.resize(a.maxWidth,
							a.maxHeight, "max");
					a.rotate === g && f && (a.rotate = "auto");
					n.rotate(a.rotate);
					h.inc();
					n.toData(function (a, c) {
						a ? h.fail() : (d[e] = c, h.next())
					})
				}
			})
		})
	};
	a.support.canvas = a.support.transform = c;
	a.Image = s
})(FileAPI, document);
(function (a, m, g) {
	var s = m.encodeURIComponent,
		p = m.FormData;
	m = function () {
		this.items = []
	};
	m.prototype = {
		append: function (a, c, e, g) {
			this.items.push({
				name: a,
				blob: c && c.blob || (void 0 == c ? "" : c),
				file: c && (e || c.name),
				type: c && (g || c.type)
			})
		},
		each: function (a) {
			for (var c = 0, e = this.items.length; c < e; c++) a.call(this, this.items[c])
		},
		toData: function (f, c) {
			c._chunked = a.support.chunked && 0 < c.chunkSize && 1 == a.filter(this.items, function (a) {
				return a.file
			}).length;
			a.support.html5 ? this.multipart || !p ? (a.log("FileAPI.Form.toMultipartData"),
				this.toMultipartData(f)) : c._chunked ? (a.log("FileAPI.Form.toPlainData"), this.toPlainData(f)) : (a.log("FileAPI.Form.toFormData"), this.toFormData(f)) : (a.log("FileAPI.Form.toHtmlData"), this.toHtmlData(f))
		},
		_to: function (f, c, e, g) {
			var p = a.queue(function () {
				c(f)
			});
			this.each(function (a) {
				e(a, f, p, g)
			});
			p.check()
		},
		toHtmlData: function (f) {
			this._to(g.createDocumentFragment(), f, function (c, e) {
				var f = c.blob,
					p;
				c.file ? (a.reset(f, !0), f.name = c.name, e.appendChild(f)) : (p = g.createElement("input"), p.name = c.name, p.type = "hidden",
					p.value = f, e.appendChild(p))
			})
		},
		toPlainData: function (a) {
			this._to({}, a, function (a, e, f) {
				a.file && (e.type = a.file);
				a.blob.toBlob ? (f.inc(), a.blob.toBlob(function (g) {
					e.name = a.name;
					e.file = g;
					e.size = g.length;
					e.type = a.type;
					f.next()
				}, "image/png")) : a.file ? (e.name = a.blob.name, e.file = a.blob, e.size = a.blob.size, e.type = a.type) : (e.params || (e.params = []), e.params.push(encodeURIComponent(a.name) + "=" + encodeURIComponent(a.blob)));
				e.start = -1;
				e.end = e.file.FileAPIReadPosition || -1;
				e.retry = 0
			})
		},
		toFormData: function (a) {
			this._to(new p,
				a, function (a, e, f) {
					a.file && e.append("_" + a.name, a.file);
					a.blob && a.blob.toBlob ? (f.inc(), a.blob.toBlob(function (g) {
						e.append(a.name, g, a.file);
						f.next()
					}, "image/png")) : a.file ? e.append(a.name, a.blob, a.file) : e.append(a.name, a.blob)
				})
		},
		toMultipartData: function (f) {
			this._to([], f, function (c, e, f, g) {
				var p = !! c.file,
					m = c.blob,
					A = function (a) {
						e.push("--_" + g + ('\r\nContent-Disposition: form-data; name="' + c.name + '"' + (p ? '; filename="' + s(c.file) + '"' : "") + (p ? "\r\nContent-Type: " + (c.type || "application/octet-stream") : "") + "\r\n\r\n" +
							(p ? a : s(a)) + "\r\n"));
						f.next()
					};
				f.inc();
				a.isFile(m) ? a.readAsBinaryString(m, function (a) {
					"load" == a.type && A(a.result)
				}) : A(m)
			}, a.expando)
		}
	};
	a.Form = m
})(FileAPI, window, document);
(function (a, m) {
	var g = function () {}, s = function (a) {
		this.uid = m.uid();
		this.xhr = {
			abort: g,
			getResponseHeader: g,
			getAllResponseHeaders: g
		};
		this.options = a
	};
	s.prototype = {
		status: 0,
		statusText: "",
		getResponseHeader: function (a) {
			return this.xhr.getResponseHeader(a)
		},
		getAllResponseHeaders: function () {
			return this.xhr.getAllResponseHeaders() || {}
		},
		end: function (p, f) {
			var c = this,
				e = c.options;
			c.end = c.abort = g;
			c.status = p;
			f && (c.statusText = f);
			m.log("xhr.end:", p, f);
			e.complete(200 == p || 201 == p ? !1 : c.statusText || "unknown", c);
			c.xhr && c.xhr.node &&
			setTimeout(function () {
				var e = c.xhr.node;
				try {
					e.parentNode.removeChild(e)
				} catch (f) {}
				try {
					delete a[c.uid]
				} catch (g) {}
				a[c.uid] = c.xhr.node = null
			}, 9)
		},
		abort: function () {
			this.end(0, "abort");
			this.xhr && (this.xhr.aborted = !0, this.xhr.abort())
		},
		send: function (a) {
			var f = this,
				c = this.options;
			a.toData(function (a) {
				c.upload(c, f);
				f._send.call(f, c, a)
			}, c)
		},
		_send: function (g, f) {
			var c = this,
				e, n = c.uid,
				q = g.url;
			m.log("XHR._send:", f);
			q += (~q.indexOf("?") ? "&" : "?") + m.uid();
			f.nodeName ? (g.upload(g, c), e = document.createElement("div"), e.innerHTML =
				'<form target="' + n + '" action="' + q + '" method="POST" enctype="multipart/form-data" style="position: absolute; top: -1000px; overflow: hidden; width: 1px; height: 1px;"><iframe name="' + n + '" src="javascript:false;"></iframe><input value="' + n + '" name="callback" type="hidden"/></form>', c.xhr.abort = function () {
				var a = e.getElementsByName("iframe")[0];
				if (a) try {
					a.stop ? a.stop() : a.contentWindow.stop ? a.contentWindow.stop() : a.contentWindow.document.execCommand("Stop")
				} catch (c) {}
				e = null
			}, q = e.getElementsByTagName("form")[0],
				q.appendChild(f), m.log(q.parentNode.innerHTML), document.body.appendChild(e), c.xhr.node = e, a[n] = function (a, f, g) {
				c.readyState = 4;
				c.responseText = g;
				c.end(a, f);
				e = null
			}, c.readyState = 2, q.submit(), q = null) : this.xhr && this.xhr.aborted ? m.log("Error: already aborted") : (e = c.xhr = m.getXHR(), f.params && (q += (0 > q.indexOf("?") ? "?" : "&") + f.params.join("&")), e.open("POST", q, !0), m.withCredentials && (e.withCredentials = "true"), g.headers && g.headers["X-Requested-With"] || e.setRequestHeader("X-Requested-With", "XMLHttpRequest"), m.each(g.headers,
				function (a, c) {
					e.setRequestHeader(c, a)
				}), g._chunked ? (e.upload && e.upload.addEventListener("progress", function (a) {
				f.retry || g.progress({
					type: a.type,
					total: f.size,
					loaded: f.start + a.loaded,
					totalSize: f.size
				}, c, g)
			}, !1), e.onreadystatechange = function () {
				c.status = e.status;
				c.statusText = e.statusText;
				c.readyState = e.readyState;
				if (4 == e.readyState) {
					for (var a in {
						"": 1,
						XML: 1,
						Text: 1,
						Body: 1
					}) c["response" + a] = e["response" + a];
					e.onreadystatechange = null;
					if (!e.status || 0 < e.status - 201)
						if (m.log("Error: " + e.status), (!e.status && !e.aborted ||
							500 == e.status || 416 == e.status) && ++f.retry <= g.chunkUploadRetry) {
							a = e.status ? 0 : m.chunkNetworkDownRetryTimeout;
							g.pause(f.file, g);
							var n = parseInt(e.getResponseHeader("X-Last-Known-Byte"), 10);
							m.log("X-Last-Known-Byte: " + n);
							f.end = n ? n : f.start - 1;
							setTimeout(function () {
								c._send(g, f)
							}, a)
						} else c.end(e.status);
					else f.retry = 0, f.end == f.size - 1 ? c.end(e.status) : (n = parseInt(e.getResponseHeader("X-Last-Known-Byte"), 10), m.log("X-Last-Known-Byte: " + n), n && (f.end = n), f.file.FileAPIReadPosition = f.end, setTimeout(function () {
						c._send(g,
							f)
					}, 0));
					e = null
				}
			}, f.start = f.end + 1, f.end = Math.max(Math.min(f.start + g.chunkSize, f.size) - 1, f.start), (n = "slice") in f.file || (n = "mozSlice") in f.file || (n = "webkitSlice"), e.setRequestHeader("Content-Range", "bytes " + f.start + "-" + f.end + "/" + f.size), e.setRequestHeader("Content-Disposition", "attachment; filename=" + encodeURIComponent(f.name)), e.setRequestHeader("Content-Type", f.type || "application/octet-stream"), n = f.file[n](f.start, f.end + 1), e.send(n), n = null) : (e.upload && e.upload.addEventListener("progress", m.throttle(function (a) {
				g.progress(a,
					c, g)
			}, 100), !1), e.onreadystatechange = function () {
				c.status = e.status;
				c.statusText = e.statusText;
				c.readyState = e.readyState;
				if (4 == e.readyState) {
					for (var a in {
						"": 1,
						XML: 1,
						Text: 1,
						Body: 1
					}) c["response" + a] = e["response" + a];
					e.onreadystatechange = null;
					c.end(e.status);
					e = null
				}
			}, m.isArray(f) ? (e.setRequestHeader("Content-Type", "multipart/form-data; boundary=_" + m.expando), f = f.join("") + "--_" + m.expando + "--", e.sendAsBinary ? e.sendAsBinary(f) : (n = Array.prototype.map.call(f, function (a) {
				return a.charCodeAt(0) & 255
			}), e.send((new Uint8Array(n)).buffer))) :
				e.send(f)))
		}
	};
	m.XHR = s
})(window, FileAPI);
(function (a, m, g) {
	a.support.flash = function () {
		var g = m.navigator,
			p = g.mimeTypes,
			f = !1;
		if (g.plugins && "object" == typeof g.plugins["Shockwave Flash"]) f = g.plugins["Shockwave Flash"].description && !(p && p["application/x-shockwave-flash"] && !p["application/x-shockwave-flash"].enabledPlugin);
		else try {
			f = !(!m.ActiveXObject || !new ActiveXObject("ShockwaveFlash.ShockwaveFlash"))
		} catch (c) {
			a.log("ShockwaveFlash.ShockwaveFlash -- does not supported.")
		}
		return f
	}();
	!a.support.flash || a.html5 && a.support.html5 && (!a.cors || a.support.cors) ||
	function () {
		function s(a) {
			return ('<object id="#id#" classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width="' + (a.width || "100%") + '" height="' + (a.height || "100%") + '"><param name="movie" value="#src#" /><param name="flashvars" value="#flashvars#" /><param name="swliveconnect" value="true" /><param name="allowscriptaccess" value="always" /><param name="allownetworking" value="all" /><param name="menu" value="false" /><param name="wmode" value="#wmode#" /><embed flashvars="#flashvars#" swliveconnect="true" allownetworking="all" allowscriptaccess="always" name="#id#" src="#src#" width="' +
				(a.width || "100%") + '" height="' + (a.height || "100%") + '" menu="false" wmode="transparent" type="application/x-shockwave-flash"></embed></object>').replace(/#(\w+)#/ig, function (c, e) {
					return a[e]
				})
		}

		function p(a, c) {
			if (a && a.style) {
				var e, f;
				for (e in c) {
					f = c[e];
					"number" == typeof f && (f += "px");
					try {
						a.style[e] = f
					} catch (g) {}
				}
			}
		}

		function f(d, c) {
			a.each(c, function (a, c) {
				var e = d[c];
				d[c] = function () {
					this.parent = e;
					return a.apply(this, arguments)
				}
			})
		}

		function c(d) {
			var c = d.wid = a.uid();
			l._fn[c] = d;
			return "FileAPI.Flash._fn." + c
		}

		function e(a) {
			try {
				l._fn[a.wid] =
					null, delete l._fn[a.wid]
			} catch (c) {}
		}

		function n(a, c) {
			if (!C.test(a)) {
				if (/^\.\//.test(a) || "/" != a.charAt(0)) {
					var e = location.pathname,
						e = e.substr(0, e.lastIndexOf("/"));
					a = (e + "/" + a).replace("/./", "/")
				}
				"//" != a.substr(0, 2) && (a = "//" + location.host + a);
				C.test(a) || (a = location.protocol + a)
			}
			c && (a += (/\?/.test(a) ? "&" : "?") + c);
			return a
		}

		function q(d, h, f) {
			function m() {
				try {
					l.get(v).setImage(h)
				} catch (d) {
					a.log('flash.setImage -- can not set "base64":', d)
				}
			}
			var q, v = a.uid(),
				r = g.createElement("div");
			for (q in d) r.setAttribute("data-img-" +
				q, d[q]);
			p(r, d);
			r.innerHTML = s(a.extend({
				id: v,
				src: n(a.flashImageUrl, "r=" + a.uid()),
				wmode: "opaque",
				flashvars: "scale=" + d.scale + "&callback=" + c(function x() {
					e(x);
					setTimeout(m, 99);
					return !0
				})
			}, d));
			f(!1, r);
			r = null
		}
		var r = a.uid(),
			w = 0,
			A = {}, C = /^https?:/i,
			l = {
				_fn: {},
				init: function () {
					var d = g.body && g.body.firstChild;
					if (d) {
						do
							if (1 == d.nodeType) {
								a.log("FlashAPI.Flash.init...");
								var c = g.createElement("div");
								p(c, {
									top: 1,
									right: 1,
									width: 5,
									height: 5,
									position: "absolute"
								});
								d.parentNode.insertBefore(c, d);
								l.publish(c, r);
								return
							} while (d =
							d.nextSibling)
					}
					10 > w && setTimeout(l.init, 50 * ++w)
				},
				publish: function (d, c) {
					d.innerHTML = s({
						id: c,
						src: n(a.flashUrl, "r=" + a.version),
						wmode: "transparent",
						flashvars: "callback=FileAPI.Flash.event&flashId=" + c + "&storeKey=" + navigator.userAgent.match(/\d/ig).join("") + "_" + a.version + (l.isReady || (a.pingUrl ? "&ping=" + a.pingUrl : "")) + "&timeout=" + a.flashAbortTimeout
					})
				},
				ready: function () {
					l.ready = a.F;
					l.isReady = !0;
					l.patch();
					a.event.on(g, "mouseover", l.mouseover);
					a.event.on(g, "click", function (a) {
						l.mouseover(a) && (a.preventDefault ?
							a.preventDefault() : a.returnValue = !0)
					})
				},
				getWrapper: function (a) {
					do
						if (/js-fileapi-wrapper/.test(a.className)) return a; while ((a = a.parentNode) && a !== g.body)
				},
				mouseover: function (d) {
					d = a.event.fix(d).target;
					if (/input/i.test(d.nodeName) && "file" == d.type) {
						var c = d.getAttribute(r);
						if ("i" == c || "r" == c) return !1;
						if ("p" != c) {
							d.setAttribute(r, "i");
							var c = g.createElement("div"),
								e = l.getWrapper(d);
							if (!e) {
								a.log("flash.mouseover.error: js-fileapi-wrapper not found");
								return
							}
							p(c, {
								top: 0,
								left: 0,
								width: d.offsetWidth + 100,
								height: d.offsetHeight + 100,
								zIndex: "1000000",
								position: "absolute"
							});
							e.appendChild(c);
							l.publish(c, a.uid());
							d.setAttribute(r, "p")
						}
						return !0
					}
				},
				event: function (d) {
					var c = d.type;
					if ("ready" == c) {
						try {
							l.getInput(d.flashId).setAttribute(r, "r")
						} catch (e) {}
						l.ready();
						setTimeout(function () {
							l.mouseenter(d)
						}, 50);
						return !0
					}
					"ping" === c ? a.log("(flash -> js).ping:", [d.status, d.savedStatus], d.error) : "log" === c ? a.log("(flash -> js).log:", d.target) : c in l && setTimeout(function () {
						a.log("Flash.event." + d.type + ":", d);
						l[c](d)
					}, 1)
				},
				mouseenter: function (d) {
					var c =
						l.getInput(d.flashId);
					if (c) {
						l.cmd(d, "multiple", null != c.getAttribute("multiple"));
						var e = [],
							f = {};
						a.each((c.getAttribute("accept") || "").split(/,\s*/), function (d) {
							a.accept[d] && a.each(a.accept[d].split(" "), function (a) {
								f[a] = 1
							})
						});
						a.each(f, function (a, d) {
							e.push(d)
						});
						l.cmd(d, "accept", e.length ? e.join(",") + "," + e.join(",").toUpperCase() : "*")
					}
				},
				get: function (a) {
					return g[a] || m[a] || g.embeds[a]
				},
				getInput: function (d) {
					try {
						var c = l.getWrapper(l.get(d));
						if (c) return c.getElementsByTagName("input")[0]
					} catch (e) {
						a.log('Can not find "input" by flashId:',
							d, e)
					}
				},
				select: function (d) {
					var c = l.getInput(d.flashId),
						e = a.uid(c);
					d = d.target.files;
					a.each(d, function (d) {
						a.checkFileObj(d)
					});
					A[e] = d;
					g.createEvent ? (e = g.createEvent("Event"), e.initEvent("change", !0, !1), c.dispatchEvent(e)) : g.createEventObject && (e = g.createEventObject(), c.fireEvent("onchange", e))
				},
				cmd: function (d, c, e, f) {
					try {
						return a.log("(js -> flash)." + c + ":", e), l.get(d.flashId || d).cmd(c, e)
					} catch (g) {
						a.log("(js -> flash).onError:", g), f || setTimeout(function () {
							l.cmd(d, c, e, !0)
						}, 50)
					}
				},
				patch: function () {
					a.flashEngine =
						a.support.transform = !0;
					f(a, {
						getFiles: function (d, c, e) {
							if (e) return a.filterFiles(a.getFiles(d), c, e), null;
							var f = a.isArray(d) ? d : A[a.uid(d.target || d.srcElement || d)];
							if (!f) return this.parent.apply(this, arguments);
							c && (c = a.getFilesFilter(c), f = a.filter(f, function (a) {
								return c.test(a.name)
							}));
							return f
						},
						getInfo: function (d, f) {
							if (d && !d.flashId) this.parent.apply(this, arguments);
							else {
								if (!d.__info) {
									var g = d.__info = a.defer();
									l.cmd(d, "getFileInfo", {
										id: d.id,
										callback: c(function E(a, c) {
											e(E);
											g.resolve(a, d.info = c)
										})
									})
								}
								d.__info.then(f)
							}
						}
					});
					a.support.transform = !0;
					a.Image && f(a.Image.prototype, {
						get: function (a, c) {
							this.set({
								scaleMode: c || "noScale"
							});
							this.parent(a)
						},
						_load: function (d, c) {
							a.log("FileAPI.Image._load:", d);
							if (d && !d.flashId) this.parent.apply(this, arguments);
							else {
								var e = this;
								a.getInfo(d, function (a, f) {
									c.call(e, a, d)
								})
							}
						},
						_apply: function (d, f) {
							a.log("FileAPI.Image._apply:", d);
							if (d && !d.flashId) this.parent.apply(this, arguments);
							else {
								var g = this.getMatrix(d.info);
								l.cmd(d, "imageTransform", {
									id: d.id,
									matrix: g,
									callback: c(function E(c, l) {
										a.log("FileAPI.Image._apply.callback:",
											c);
										e(E);
										c ? f(c) : !a.support.dataURI || 3E4 < l.length ? q({
											width: g.deg % 180 ? g.dh : g.dw,
											height: g.deg % 180 ? g.dw : g.dh,
											scale: g.scaleMode
										}, l, f) : a.newImage("data:" + d.type + ";base64," + l, f)
									})
								})
							}
						},
						toData: function (d) {
							var c = this.file,
								e = c.info,
								f = this.getMatrix(e);
							c && !c.flashId ? this.parent.apply(this, arguments) : ("auto" == f.deg && (f.deg = a.Image.exifOrientation[e && e.exif && e.exif.Orientation] || 0), d.call(this, !c.info, {
								id: c.id,
								flashId: c.flashId,
								name: c.name,
								type: c.type,
								matrix: f
							}))
						}
					});
					f(a.Form.prototype, {
						toData: function (d) {
							for (var c =
								this.items, e = c.length; e--;)
								if (c[e].file && c[e].blob && !c[e].blob.flashId) return this.parent.apply(this, arguments);
							a.log("flash.Form.toData");
							d(c)
						}
					});
					f(a.XHR.prototype, {
						_send: function (d, f) {
							if (f.nodeName || f.append && a.support.html5 || a.isArray(f) && "string" === typeof f[0]) return this.parent.apply(this, arguments);
							var g = {}, m = {}, p = this,
								q, r;
							a.each(f, function (a) {
								a.file ? (m[a.name] = a = {
									id: a.blob.id,
									name: a.blob.name,
									matrix: a.blob.matrix,
									flashId: a.blob.flashId
								}, r = a.id, q = a.flashId) : g[a.name] = a.blob
							});
							if (r || q) a.log("flash.XHR._send:",
								q, r, m);
							else return this.parent.apply(this, arguments);
							p.xhr = {
								headers: {},
								abort: function () {
									l.cmd(q, "abort", {
										id: r
									})
								},
								getResponseHeader: function (a) {
									return this.headers[a]
								},
								getAllResponseHeaders: function () {
									return this.headers
								}
							};
							var s = a.queue(function () {
								l.cmd(q, "upload", {
									url: n(d.url),
									data: g,
									files: m,
									headers: d.headers,
									callback: c(function y(c) {
										var f = c.type,
											g = c.result;
										a.log("flash.upload." + f + ":", c);
										if ("progress" == f) c.loaded = Math.min(c.loaded, c.total), c.lengthComputable = !0, d.progress(c);
										else if ("complete" == f) e(y),
											"string" == typeof g && (p.responseText = g.replace(/%22/g, '"').replace(/%5c/g, "\\").replace(/%26/g, "&").replace(/%25/g, "%")), p.end(c.status || 200);
										else if ("abort" == f || "error" == f) p.end(c.status || 0, c.message), e(y)
									})
								})
							});
							a.each(m, function (c) {
								s.inc();
								a.getInfo(c, s.next)
							});
							s.check()
						}
					})
				}
			};
		a.Flash = l;
		a.newImage("data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==", function (c, e) {
			a.support.dataURI = !(1 != e.width || 1 != e.height);
			l.init()
		})
	}()
})(FileAPI, window, document);
"undefined" !== typeof ajs && ajs.loaded && ajs.loaded("{fileapi}FileAPI.min");
"function" === typeof define && define.amd && define("FileAPI", [], function () {
	return window.FileAPI || {}
});