/**
 * Author: Wes Reid - kinesys180.com
 * Date: 8/13/13
 * Time: 11:14 PM
 */

require.config({
    baseUrl: '/javascripts',
    paths: {
        'jquery': '//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min',
        'angular': 'vendor/angular',
        'angular-resource': '//ajax.googleapis.com/ajax/libs/angularjs/1.0.7/angular-resource',
	    'angular-ui-router': 'vendor/angular-ui-router.min',
	    'lodash': 'vendor/lodash.min',
	    'jquery-cookies': 'vendor/jquery.cookie',
	    'fsapi': 'vendor/FileAPI',
	    'modernizr': 'vendor/modernizr',
	    'videojs': 'vendor/video',
	    'mejs': 'vendor/mediaelement/mediaelement-and-player.min'
    },
    shim: {
        'angular' : {'exports' : 'angular'},
        'angular-resource': { deps:['angular']},
        'jquery': {'exports' : 'jquery'},
	    'lodash': {'exports' : 'lodash'},
	    'jquery-cookies': {'deps' : ['jquery']},
	    'fsapi': {'exports' : 'fsapi'},
	    'modernizr': {'exports' : 'modernizr'},
	    'videojs': {'exports' : 'videojs'},
	    'mejs': {'deps':['jquery']}
    }
});

require(['jquery', 'lodash', 'angular', 'ng/routes', 'amd/app'] , function ($, _, angular, routes, app) {
    $(function () { // using jQuery because it will run this even if DOM load already happened
        angular.bootstrap(document , ['mainApp']);
    });


	var $w = window;
    $w['hfapp'] = $w.hfapp || app;
	$w.hfapp.deps = $w.hfapp.deps || [];
	$w.hfapp.deps.push('amd/util','modernizr');
    require($w.hfapp.deps);
});