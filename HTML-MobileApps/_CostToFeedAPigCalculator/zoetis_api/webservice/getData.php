<?php
$ch = curl_init();
$apikey = 'c04d0d8483f05df29c6678505a03be8b';

// construct the query with our apikey and the query we want to make
$endpoint = 'http://ondemand.websol.barchart.com/getQuote.json?apikey=' . $apikey;
$post_data="symbols=ZC*0,ZMH14,HEG14";	

$url = $endpoint;

$c = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));   
curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); 
$result = curl_exec($ch); // json string

if(array_key_exists('callback', $_GET)){

    header('Content-Type: text/javascript; charset=utf8');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Max-Age: 3628800');
    header('Access-Control-Allow-Methods: GET');

    $callback = $_GET['callback'];
    echo $callback.'('.$result.');';

}else{
    // normal JSON string
    header('Content-Type: application/json; charset=utf8');

    echo $result;
}
?>
