window.EV = {
    oDomCache: {
        oBackButtonElement: undefined,
        oHelpElements: undefined,
        oNextButtonElement: undefined,
        oSiteWrapperElement: undefined
    },
    oModules: {},
    bInTestMode: false
};

// adaped from SO
Number.prototype.formatNumber = function (nDecimalPlacesToDisplay, p, d, t) {
    var n = this;
    var nDecimalPlacesToDisplay = isNaN(nDecimalPlacesToDisplay = Math.abs(nDecimalPlacesToDisplay)) ? 2 : nDecimalPlacesToDisplay;
    var p = p == undefined ? '' : p;
    var d = d == undefined ? '.' : d;
    var t = t == undefined ? ',' : t;
    var s = n < 0 ? '-' : '';
    var i = parseInt(n = Math.abs(+n || 0).toFixed(nDecimalPlacesToDisplay)) + '',
        j = (j = i.length) > 3 ? j % 3 : 0;
    return s + p + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (nDecimalPlacesToDisplay ? d + Math.abs(n - i).toFixed(nDecimalPlacesToDisplay).slice(2) : '');
};



EV.oModules.oCalculator = {
    // Special "Number" constructor that identifies whether a value is a percentage, integer, or money value
    NumberOfType: function (sType, nValue) {
        // Locally-scoped variables
        var sExpectedType;

        // Function argument type checking
        sExpectedType = 'string';
        if (typeof (sType) !== sExpectedType) {
            window.alert('NumberOfType(): argument #1 must be of type "' + sExpectedType + '"');
            return;
        }
        sExpectedType = 'number';
        if (typeof (nValue) !== sExpectedType) {
            window.alert('NumberOfType(): argument #2 must be of type "' + sExpectedType + '"');
            return;
        }

        // Use the correct formatting function
        switch (sType) {
        case 'float':
            this.fsOutputToPage = function () {
                // show decimal places based on size
                // FIXME? for values>=1.0, always show three significant figures?
                // FIXME: use sprintf for zero padding to the right (for values under 100)?
                if (this.nValue < 0.9995) {
                    // for values less than 1.0, show three decimal places (unless it would round to 1.00 even)
                    return this.nValue.formatNumber(3);
                } else if (this.nValue < 9.995) {
                    // for values less than 10.0, show two decimal places (unless it would round to 10.0 even)
                    return this.nValue.formatNumber(2);
                } else if (this.nValue < 99.95) {
                    // for values less than 100.0, show one decimal place (unless it would round to 100 even)
                    return this.nValue.formatNumber(1);
                } else {
                    // for values of 100 or more, round to the nearest integer
                    return this.nValue.formatNumber(0);
                }
            };
            break;
        case 'integer':
            this.fsOutputToPage = function () {
                return Math.round(this.nValue).toLocaleString();
            };
            break;
        case 'money':
            this.fsOutputToPage = function () {
                if (this.nValue < 99.995) {
                    // for amounts under $100, show pennies (unless it would round to $100 even)
                    return this.nValue.formatNumber(2, '$');
                } else {
                    // for amounts $100 or more, round to the nearest dollar
                    return this.nValue.formatNumber(0, '$');
                }
            };
            break;
        case 'percent':
            this.fsOutputToPage = function () {
                // always return an integer percentage
                return (this.nValue * 100.0).formatNumber(0) + '%';
            };
            break;
        case 'ratio':
            this.fsOutputToPage = function () {
                //ratio returns percent as one place decimal compared to 1
                return (this.nValue).formatNumber(1) + ":1";
            };
            break;
        }

        // Assign values to member variables
        this.sType = sType;
        this.nValue = nValue;
    },

    // Populate all input AND output field values with the correct NumberOfType type
    fuInitializeOutputFieldNumberTypes: function () {
        var aRowValues,
            nRowIndex,
            oField,
            oInputFields = this.oFields.oInput,
            oOutputFields = this.oFields.oOutput,
            sKey;

        // Input fields
        for (sKey in oInputFields) {
            if (oInputFields.hasOwnProperty(sKey)) {
                if (oInputFields[sKey].hasOwnProperty('aRowValues')) {
                    // it's an array of values
                    oField = oInputFields[sKey];
                    aRowValues = oField.aRowValues;
                    // Give each row value the proper type
                    for (nRowIndex = aRowValues.length - 1; nRowIndex !== -1; --nRowIndex) {
                        aRowValues[nRowIndex] = new this.NumberOfType(oField.sNumberType, 0);
                    }
                } else {
                    // it's a single value
                    oInputFields[sKey] = new this.NumberOfType(oInputFields[sKey].sNumberType, 0);
                }
            }
        }

        // Output fields
        for (sKey in oOutputFields) {
            if (oOutputFields.hasOwnProperty(sKey)) {
                // all outputs are arrays
                oField = oOutputFields[sKey];
                aRowValues = oField.aRowValues;

                // Give each row value the proper type
                for (nRowIndex = aRowValues.length - 1; nRowIndex !== -1; --nRowIndex) {
                    aRowValues[nRowIndex] = new this.NumberOfType(oField.sNumberType, 0);
                }
            }
        }
    },

    // Compute all output values
    fuComputeOutputValues: function () {
        var oIntermediateFields = this.oFields.oIntermediate,
            oOutputFields = this.oFields.oOutput;

        // order is important (and a "for" loop won't guarantee it)
        oIntermediateFields.oFeedConsumption.fuCompute(); /**************************/
        oIntermediateFields.oAverageFeedCost.fuCompute(); /**************************/
        oOutputFields.oFeedSavedWeight.fuCompute(); /**************************/
        oOutputFields.oFeedSavedDollars.fuCompute(); /**************************/
    },

    // some of the output fields depend on which tab we're looking at
    nWhichTab: undefined,
    nPerPigTab: 0, // base
    nPerBarnTab: 1, // per pig * pigs/barn
    nPerSystemTab: 2, // per barn * barns/system  ( = per pig * pigs/barn * barns/system )
    /* ADDED BY Rodrigo START */
    nMatchTab: 3, // match albac-roi tab
    /* ADDED BY Rodrigo END */

    // interpolation data for feed consumed as a pig grows
    oGrowthData: {
        // week number = index + 3
        aWeight: [12, 15, 19, 26, 33, 41, 50, 60, 71, 82, 94.5, 107, 120, 133, 147, 161, 175, 189, 203, 217, 231, 245, 258, 271, 300],
        aDailyFeedConsumed: [0.48, 0.88, 1.35, 1.6, 1.96, 2.34, 2.9, 3.52, 3.87, 4.2, 4.57, 4.94, 5.28, 5.6, 5.9, 6.2, 6.4, 6.8, 7.91, 7.02, 7.12, 7.22, 7.32, 7.32, 7.32]
    },

    // feed mix ratios by week
    oFeedWeight: {
        // corn, soy, fat, premix weights
        aWeek10: [1095.00, 725.00, 115.00, 65.00],
        aWeek13: [1179.81, 650.19, 110.00, 60.00],
        aWeek16: [1265.37, 574.63, 105.00, 55.00],
        aWeek19: [1350.93, 499.07, 100.00, 50.00],
        aWeek22: [1434.01, 420.99, 100.00, 45.00],
        aWeek25: [1501.14, 383.86, 70.00, 45.00],
        // method to choose the array of ratios by week number
        fuGetWeight: function (nWeekNumber) {
            if (nWeekNumber < 13) {
                return this.aWeek10;
            } else if (nWeekNumber < 16) {
                return this.aWeek13;
            } else if (nWeekNumber < 19) {
                return this.aWeek16;
            } else if (nWeekNumber < 22) {
                return this.aWeek19;
            } else if (nWeekNumber < 25) {
                return this.aWeek22;
            } else {
                return this.aWeek25;
            }
        }
    },

    // medicated feed concentrations (from manufacturer data) in g/lb
    // (unmedicated baseline), Albac, Stafac, and Skycis
    aFeedConcentration: [0.0, 50.0, 20.0, 45.45],

    oFields: {
        oInput: {
            // baseline hog data
            oPigsInBarn: {
                sNumberType: 'integer'
            },
            oBarnsInSystem: {
                sNumberType: 'integer'
            },
            oStartWeight: {
                sNumberType: 'integer'
            }, // lbs
            oEndWeight: {
                sNumberType: 'integer'
            }, // lbs
            // non-medicated feed costs
            oCornCost: {
                sNumberType: 'money'
            }, // $/bushel
            oSoybeanCost: {
                sNumberType: 'money'
            }, // $/ton
            oFatCost: {
                sNumberType: 'money'
            }, // $/ton
            oPremixCost: {
                sNumberType: 'money'
            }, // $/ton
            // medicated feed data for baseline (all 0), Albac, Stafac, and Skycis
            // we include baseline so the indices line up with the output table
            oMedicatedFeedPrice: {
                aRowValues: [undefined, undefined, undefined, undefined],
                sNumberType: 'money' // $/lb
            },
            oInclusionRate: {
                aRowValues: [undefined, undefined, undefined, undefined],
                sNumberType: 'integer' // g/Ton
            },
            oImprovementADG: {
                aRowValues: [undefined, undefined, undefined, undefined],
                sNumberType: 'percent'
            },
            oImprovementFE: {
                aRowValues: [undefined, undefined, undefined, undefined],
                sNumberType: 'percent'
            }
        },

        oIntermediate: {
            // keep track of whether any Stafac or Skycis values are blank
            bStafacEmptyADG: false,
            bStafacEmptyFE: false,
            bSkycisEmptyADG: false,
            bSkycisEmptyFE: false,
            // calculated values
            oFeedConsumption: {
                nValue: undefined,
                fuCompute: function () {
                    var nStartWeight = EV.oModules.oCalculator.oFields.oInput.oStartWeight.nValue,
                        nEndWeight = EV.oModules.oCalculator.oFields.oInput.oEndWeight.nValue,
                        aWeight = EV.oModules.oCalculator.oGrowthData.aWeight,
                        aDailyFeedConsumed = EV.oModules.oCalculator.oGrowthData.aDailyFeedConsumed,
                        nFirstFullWeekIndex, nEndingPartialWeekIndex, nEndingFullWeekIndex, bIsEndingPartialWeek,
                        nFeedConsumed, nFeedConsumedinFullWeeks = 0,
                        nOneWeekConsumption;

                    // NOTE: partial weeks may be empty (have 0.0 days)
                    //       there can be a single partial week, two partial weeks (with no full weeks between), or two partials with one or more full weeks in between
                    //       UI restricts nStartWeight>=60 and nEndWeight<=300 so we're always in range
                    //       week number = week index + 3

                    // find the first index where nStartWeight<=aWeight[nFirstFullWeekIndex] but nStartWeight>aWeight[nFirstFullWeekIndex-1]
                    for (nFirstFullWeekIndex = 0; nStartWeight > aWeight[nFirstFullWeekIndex]; ++nFirstFullWeekIndex);
                    // find the first index where nEndWeight>=aWeight[nEndingPartialWeekIndex] but nEndWeight<aWeight[nEndingPartialWeekIndex+1]
                    for (nEndingPartialWeekIndex = 0; nEndWeight > aWeight[nEndingPartialWeekIndex + 1]; ++nEndingPartialWeekIndex);

                    // Determing if there is an ending partial week
                    if (nEndingPartialWeekIndex == aWeight.length - 1) {
                        bIsEndingPartialWeek = nEndWeight < aWeight[nEndingPartialWeekIndex]
                    } else {
                        bIsEndingPartialWeek = nEndWeight < aWeight[nEndingPartialWeekIndex + 1];
                    }


                    nEndingFullWeekIndex = bIsEndingPartialWeek ? nEndingPartialWeekIndex - 1 : nEndingPartialWeekIndex;

                    if (nEndingPartialWeekIndex < nFirstFullWeekIndex) {
                        // single partial week
                        var nDays = 7.0 * (nEndWeight - nStartWeight) / (aWeight[nFirstFullWeekIndex] - aWeight[nFirstFullWeekIndex - 1]);
                        nFeedConsumed = nDays * aDailyFeedConsumed[nFirstFullWeekIndex - 1];
                    } else {
                        // two partial weeks with zero or more full weeks between
                        // compute feed consumed in starting partial week
                        var nStartingDays = 7.0 * (aWeight[nFirstFullWeekIndex] - nStartWeight) / (aWeight[nFirstFullWeekIndex] - aWeight[nFirstFullWeekIndex - 1]);
                        nFeedConsumed = nStartingDays * aDailyFeedConsumed[nFirstFullWeekIndex - 1];
                        console.log("nInitialPartialFeedConsumed: ", nFeedConsumed);
                        // compute feed consumed in intervening full weeks (if any)
                        var testOutput = [];
                        for (var nWeekIndex = nFirstFullWeekIndex; nWeekIndex <= nEndingFullWeekIndex; ++nWeekIndex) {

                            nOneWeekConsumption = 7.0 * aDailyFeedConsumed[nWeekIndex];

                            nFeedConsumed += nOneWeekConsumption;
                            nFeedConsumedinFullWeeks += nOneWeekConsumption;
                            testOutput.push(nOneWeekConsumption);
                        }
                        //console.log("test Full Week Feed output: ", testOutput);
                        console.log("nFeedConsumedinFullWeeks: ", nFeedConsumedinFullWeeks);
                        // compute feed consumed in ending partial week
                        if (bIsEndingPartialWeek) {
                            var nEndingDays = 7.0 * (nEndWeight - aWeight[nEndingPartialWeekIndex]) / (aWeight[nEndingPartialWeekIndex + 1] - aWeight[nEndingPartialWeekIndex]);
                            var nEndingPartialFeedConsumed = nEndingDays * aDailyFeedConsumed[nEndingPartialWeekIndex];
                            console.log("nEndingPartialFeedConsumed: ", nEndingPartialFeedConsumed);
                            nFeedConsumed += nEndingPartialFeedConsumed;
                        } else console.log("No Partial Ending week");
                    }
                    console.log("nFeedConsumed: ", nFeedConsumed);
                    this.nValue = nFeedConsumed;
                }
            },
            oAverageFeedCost: {
                nValue: undefined,
                nDays: undefined, // expose total feed days for ADG improvement calculation
                fuCompute: function () {
                    var nStartWeight = EV.oModules.oCalculator.oFields.oInput.oStartWeight.nValue,
                        nEndWeight = EV.oModules.oCalculator.oFields.oInput.oEndWeight.nValue,
                        aWeight = EV.oModules.oCalculator.oGrowthData.aWeight,
                        oFeedWeight = EV.oModules.oCalculator.oFeedWeight,
                        aFeedWeight, aFeedCosts, oInputFields = EV.oModules.oCalculator.oFields.oInput,
                        nFirstFullWeekIndex, nEndingPartialWeekIndex, nEndingFullWeekIndex, bIsEndingPartialWeek,
                        nDays = 0.0,
                        nWholeWeekDays = 0.0,
                        nFeedCost = 0.0;

                    // compute feed costs per pound (from bushel, ton, ton, ton)
                    aFeedCosts = [oInputFields.oCornCost.nValue / 56.0,
                        oInputFields.oSoybeanCost.nValue / 2000.0,
                        oInputFields.oFatCost.nValue / 2000.0,
                        oInputFields.oPremixCost.nValue / 2000.0
                    ];

                    // NOTE: partial weeks may be empty (have 0.0 days)
                    //       there can be a single partial week, two partial weeks (with no full weeks between), or two partials with one or more full weeks in between
                    //       UI restricts nStartWeight>=60 and nEndWeight<=300 so we're always in range
                    //       week number = week index + 3
                    //       FIXME: UI restricts start/end weight so there is always at least one full week???
                    //       no partial weeks for average feed cost, but they are used in ADG calc

                    // find the first index where nStartWeight<=aWeight[nFirstFullWeekIndex] but nStartWeight>aWeight[nFirstFullWeekIndex-1]
                    for (nFirstFullWeekIndex = 0; nStartWeight > aWeight[nFirstFullWeekIndex]; ++nFirstFullWeekIndex);
                    // find the first index where nEndWeight>=aWeight[nEndingPartialWeekIndex] but nEndWeight<aWeight[nEndingPartialWeekIndex+1]
                    for (nEndingPartialWeekIndex = 0; nEndWeight > aWeight[nEndingPartialWeekIndex + 1]; ++nEndingPartialWeekIndex);

                    // Determing if there is an ending partial week
                    if (nEndingPartialWeekIndex == aWeight.length - 1) {
                        bIsEndingPartialWeek = nEndWeight < aWeight[nEndingPartialWeekIndex]
                    } else {
                        bIsEndingPartialWeek = nEndWeight < aWeight[nEndingPartialWeekIndex + 1];
                    }

                    nEndingFullWeekIndex = bIsEndingPartialWeek ? nEndingPartialWeekIndex - 1 : nEndingPartialWeekIndex;


                    if (nEndingPartialWeekIndex < nFirstFullWeekIndex) {
                        // single partial week
                        nDays = 7.0 * (nEndWeight - nStartWeight) / (aWeight[nFirstFullWeekIndex] - aWeight[nFirstFullWeekIndex - 1]);
                        nWholeWeekDays = 0.0;
                        aFeedWeight = oFeedWeight.fuGetWeight(nEndingPartialWeekIndex + 3);
                        for (var nFeedIndex = 0; nFeedIndex < 4; ++nFeedIndex) {
                            nFeedCost += nWholeWeekDays * aFeedWeight[nFeedIndex] * aFeedCosts[nFeedIndex];
                        }
                    } else {
                        // two partial weeks with zero or more full weeks between
                        // compute feed consumed in starting partial week
                        var nStartingDays = 7.0 * (aWeight[nFirstFullWeekIndex] - nStartWeight) / (aWeight[nFirstFullWeekIndex] - aWeight[nFirstFullWeekIndex - 1]);
                        nDays = nStartingDays;
                        nWholeWeekDays = 0.0;
                        aFeedWeight = oFeedWeight.fuGetWeight(nFirstFullWeekIndex - 1 + 3);
                        for (var nFeedIndex = 0; nFeedIndex < 4; ++nFeedIndex) {
                            nFeedCost += 0.0 * aFeedWeight[nFeedIndex] * aFeedCosts[nFeedIndex];
                        }
                        // compute feed consumed in intervening full weeks (if any)
                        for (var nWeekIndex = nFirstFullWeekIndex; nWeekIndex <= nEndingFullWeekIndex; ++nWeekIndex) {
                            nDays += 7.0;
                            nWholeWeekDays += 7.0;
                            aFeedWeight = oFeedWeight.fuGetWeight(nWeekIndex + 3);
                            for (var nFeedIndex = 0; nFeedIndex < 4; ++nFeedIndex) {
                                nFeedCost += 7.0 * aFeedWeight[nFeedIndex] * aFeedCosts[nFeedIndex];
                            }
                        }
                        // compute feed consumed in ending partial week
                        if (bIsEndingPartialWeek) {
                            var nEndingDays = 7.0 * (nEndWeight - aWeight[nEndingPartialWeekIndex]) / (aWeight[nEndingPartialWeekIndex + 1] - aWeight[nEndingPartialWeekIndex]);
                            nDays += nEndingDays;
                            nWholeWeekDays += 0.0;
                            aFeedWeight = oFeedWeight.fuGetWeight(nEndingPartialWeekIndex + 3);
                            for (var nFeedIndex = 0; nFeedIndex < 4; ++nFeedIndex) {
                                nFeedCost += 0.0 * aFeedWeight[nFeedIndex] * aFeedCosts[nFeedIndex];
                            }
                        }
                    }

                    this.nValue = nFeedCost / nWholeWeekDays;
                    this.nDays = nDays;
                }
            },
        },

        oOutput: {

            oFeedSavedWeight: {
                aRowValues: [undefined, undefined, undefined, undefined],
                fuCompute: function () {
                    var nBaseFeedWeight = EV.oModules.oCalculator.oFields.oIntermediate.oFeedConsumption.nValue,
                        aFeedEfficiencyImprovement = EV.oModules.oCalculator.oFields.oInput.oImprovementFE.aRowValues;

                    // adjust base weight for per barn or per system
                    if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerBarnTab) {
                        var nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue;
                        nBaseFeedWeight *= nPigsInBarn;
                    } else if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerSystemTab) {
                        var nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue,
                            nBarnsInSystem = EV.oModules.oCalculator.oFields.oInput.oBarnsInSystem.nValue;
                        nBaseFeedWeight *= nPigsInBarn * nBarnsInSystem;
                    }

                },
                sNumberType: 'float'
            },

            oFeedSavedDollars: {
                aRowValues: [undefined, undefined, undefined, undefined],
                fuCompute: function () {
                    var aFeedSavedWeight = EV.oModules.oCalculator.oFields.oOutput.oFeedSavedWeight.aRowValues, // lb
                        nAverageFeedCost = EV.oModules.oCalculator.oFields.oIntermediate.oAverageFeedCost.nValue, // $/ton
						nTotalFeedConsumption = EV.oModules.oCalculator.oFields.oIntermediate.oFeedConsumption.nValue,
						nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue,
						nBarnsInSystem = EV.oModules.oCalculator.oFields.oInput.oBarnsInSystem.nValue,
                        nRowValue;

                    console.log('Per pig: '+(nAverageFeedCost/2000)*nTotalFeedConsumption)
					console.log('Per barn: '+(nAverageFeedCost/2000)*(nTotalFeedConsumption*nPigsInBarn))
					console.log('Per system: '+(nAverageFeedCost/2000)*(nTotalFeedConsumption*(nPigsInBarn*nBarnsInSystem)))
                    // $ saved = $ / lb feed * lb feed saved
                    // baseline feed doesn't save any medicated feed; skip it
                    return false;
                },
                sNumberType: 'money'
            },
        }
    },

    oVolumeRebatesMap: undefined
};