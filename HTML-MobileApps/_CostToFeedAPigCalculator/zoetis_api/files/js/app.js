// Filename: app.js
var carcassValueArray = [];
var cornValueArray = [];
var sbmValueArray = [];
var costperpigarray = [];

var leftcounter = 1;
var breakdowncounter = 1;
var fat_value = 29.95;
var hist_cost_per_pig = 0.00;
define([
  'jquery','jquery-nouislider'
], function($, noUiSlider){
  feedAPig = {
	updateOnlineStatus:function(msg) {
	    var condition = window.navigator.onLine ? "online" : "offline";
	    //console.log(condition);

	    if (condition === "online")
	    {
	        feedAPig.getData();
	        feedAPig.getBreakDownData();
	    }
	    else
	    {
			feedAPig.useOffLineData("webapi-receiver","getData");
	    }

	},	
	getData: function(){
		$.ajax({
			type: "GET",
			dataType: "jsonp",
			timeout: 2500,
			cache: false,
			async: false,
			//url: "http://pbaranowski.com/zoetis_api/webservice/?callback=receiver",
			url: "http://pbaranowski.com/zoetis_api/webservice/index.php",
		    //url: "webservice/index.php/?callback=receiver",
			success: function (data) {
				if(typeof(Storage) !== "undefined"){
					localStorage.removeItem("webapi-receiver");
					localStorage["webapi-receiver"] = JSON.stringify(data);
				}
			    //console.log("successful data retrieval");
				feedAPig.displayLeftData(data);
			},
			error: function(statusCode, errorThrown) {
				if (statusCode.status == 0) {
				    //console.log("Error in getting data using offline");
				    feedAPig.useOffLineData("webapi-receiver", getData);
				}
			}
            , complete: function () {
                feedAPig.validateForm();
            }
		});
	},
	getBreakDownData: function () {
	    $.ajax({
	        type: "GET",
	        dataType: "jsonp",
	        timeout: 2500,
	        cache: false,
	        async: false,
	        url: "http://pbaranowski.com/zoetis_api/webservice/index.php",
	        //url: "http://incivek.dev.thebloc.com/cost_pig/getData.php",
	        //url: "webservice/index.php/?callback=receiver",
	        success: function (data) {
	            feedAPig.initializeBreakDown(data);
	        },
	        error: function (statusCode, errorThrown) {
	            if (statusCode.status == 0) {
	                //console.log("Error in getting data using offline");
	                //feedAPig.useOffLineData("webapi-receiver", getData);
	            }
	        }
            , complete: function () {
                //feedAPig.updateBreakDown();
            }
	    });
	},
	getFatValue: function () {
	    return fat_value;
	},
	setFatValue: function (fatdate) {
	    //var fat_value = 29.95;
	    var d = new Date();
	    var curr_month = d.getMonth();
	    var curr_year = d.getFullYear();
	    var current_full_date = curr_year + "-" + +curr_month + 1 + "-01";
	    if (fatdate == null) {
	        fatdate = current_full_date;
	    }
	    $.ajax({
	        type: "GET",
	        dataType: "json",
	        timeout: 2500,
	        cache: false,
	        async: false,
	        url: "fatvalues.json",
	        success: function (data) {

	            if (typeof (Storage) !== "undefined") {
	                localStorage.removeItem("fat-values");
	                localStorage["fat-values"] = JSON.stringify(data);
	            }

	            $.each(data, function (key, val) {
	                if (key === fatdate) {
	                    fat_value = val;
	                }
	                if (key === current_full_date) {
	                    fat_value = val;
	                }

	            });
	        },
	        error: function (statusCode, errorThrown) {
	            if (statusCode.status == 0) {
	                //feedAPig.useOffLineData("fat-values", getFatValue);
	            }
	        }
	    });

	},
	updateFatValue: function (fatdate) {
	    //var fat_value = 29.95;
	    var d = new Date();
	    var curr_month = d.getMonth();
	    var curr_year = d.getFullYear();
	    var current_full_date = curr_year + "-" + +curr_month + 1 + "-01";
	    if (fatdate==null) {
	        fatdate = current_full_date;
	    }
	    $.ajax({
	        type: "GET",
	        dataType: "json",
	        timeout: 5000,
	        cache: false,
	        async: false,
	        url: "fatvalues.json",
	        success: function (data) {

	            if (typeof (Storage) !== "undefined") {
	                localStorage.removeItem("fat-values");
	                localStorage["fat-values"] = JSON.stringify(data);
	            }
	          
	            $.each(data, function (key, val) {
	                if (key === fatdate) {
	                    //fat_value = val;
	                    //console.log("fat value from JSON = " + val);
	                    //console.log("historical fat value   " + val);
	                    $("#breakdown_fat_price").html(val);
	                    $("#ed-breakdown-fat-price").val(val);
	                    fat_value = val;
	                }
	                if (key === current_full_date) {
	                    //fat_value = val;
	                    //console.log("fat value from JSON = " + val);
	                    //console.log("fat value in the loop " + fat_value);
	                    $("span#prices_fat span").html(val);
	                    $("span#prices_fat input").val(val);
	                    fat_value = val;
	                }

	            });
	        },
	        error: function (statusCode, errorThrown) {
	            if (statusCode.status == 0) {
	                //feedAPig.useOffLineData("fat-values", getFatValue);
	            }
	        },
             complete: function () {
       			//when request finishes run calculator
       			//feedAPig.updateBreakDown();
       			feedAPig.populate_breakdown_calculator();
       			feedAPig.populate_profit();
   			}	
	    });

	},
	displayLeftData: function (data) {

	    var newChange = "";
	    var corn=0.0;
	    var sbm=0.0;
	    var fat=0.0;
	    var premix = 0.0;
	    var carcass = 0.00;

	    //no need for fat as it is derived from static data (fatvalues.json) file

		for(var i=0;i<data.results.length;i++){
		    if(data.results[i].symbol==="ZCH14"){
		        corn=(data.results[i].lastPrice/100).toFixed(2);
				$("span#prices_corn span").html((data.results[i].lastPrice/100).toFixed(2));
				$("span#prices_corn input").val((data.results[i].lastPrice/100).toFixed(2));
				newChange = data.results[i].netChange;
				if(newChange < 0){$("#prices p:eq(0) img").attr("src","files/images/arrow_sbm.jpg")}else{$("#prices p:eq(0) img").attr("src","files/images/arrow_fat.jpg")}
		    }else if(data.results[i].symbol==="ZMH14"){
		        sbm = (data.results[i].lastPrice).toFixed(2);
				$("span#prices_sbm span").html((data.results[i].lastPrice).toFixed(2));	
				$("span#prices_sbm input").val((data.results[i].lastPrice).toFixed(2));	
				newChange = data.results[i].netChange;
				if(newChange < 0){$("#prices p:eq(1) img").attr("src","files/images/arrow_sbm.jpg")}else{$("#prices p:eq(1) img").attr("src","files/images/arrow_fat.jpg")}	
		    }
		    //else if (data.results[i].symbol === "HEG14") {
		    //    carcass = (data.results[i].lastPrice).toFixed(2);
		    //}

		}

		//console.log("leftcounter=" + leftcounter);
		//++leftcounter;

		//console.log("display left data  = " + corn, sbm, carcass);
		//console.log("display left data input values = " + $("span#prices_corn input").val(), $("span#prices_sbm input").val(), $("#breakdown_cost_carcass").html());
	    
	},
	initializeBreakDown: function (data) {

	    var newChange = "";
	    var corn = 0.0;
	    var sbm = 0.0;
	    var fat = 0.0;
	    var premix = 0.0;
	    var carcass = 95.57;

	    //no need for fat as it is derived from static data (fatvalues.json) file

	    for (var i = 0; i < data.results.length; i++) {
	        if (data.results[i].symbol === "ZCH14") {
	            corn = (data.results[i].lastPrice / 100).toFixed(2);
	        } else if (data.results[i].symbol === "ZMH14") {
	            sbm = (data.results[i].lastPrice).toFixed(2);
	        }
	        else if (data.results[i].symbol === "HEG14") {
	            carcass = (data.results[i].lastPrice).toFixed(2);
	            $("#breakdown_cost_carcass").html(carcass);
	        }

	    }
	    feedAPig.updateFatValue(null);
        /*
	    corn = $("#ed-corn-cost").val();
	    sbm = $("#ed-soybean-cost").val();

	    console.log("initialize breakdown 1= " + corn, sbm, carcass);

	    corn = $("span#prices_corn input").val();
	    sbm = $("span#prices_sbm input").val();

	    carcass = $("#breakdown_cost_carcass").html();
	    carcass = parseFloat(carcass).toFixed(2);
        */
	    //console.log("initialize breakdown 2= " + corn, sbm, carcass);


	    feedAPig.displayBreakDown(corn, sbm, carcass);
	},
	updateBreakDown: function () {
	    var corn = 0.0;
	    var sbm = 0.0;
	    //var fat = 0.0;
	    var carcass_value = 0.0;

	    corn = $("#ed-breakdown-corn-price").val();
	    sbm = $("#ed-breakdown-sbm-price").val();
	    carcass_value = $("#breakdown_cost_carcass").html();
	    carcass_value = parseFloat(carcass_value).toFixed(2);

	    //corn = $("#breakdown_corn_price").html();
	    //sbm = $("#breakdown_sbm_price").html();

	    //alert($("#breakdown_corn_price").html());
	    //alert($("#breakdown_sbm_price").html());

	    //console.log("Values in updateBreakDown function from SPAN elements = " + corn1, sbm1 , carcass_value);

	    //console.log("Values in updateBreakDown function passing to displayBreakDown corn, sbm, carcass= " + corn, sbm, carcass_value);

	    feedAPig.displayBreakDown(corn, sbm, carcass_value);


	},
	displayBreakDown: function (corn, sbm, carcass) {

	    var premix = $("#ed-premix-cost").val();

	    if (premix == 0) {
	        //console.log("premix is zero");
	        premix = $("span#prices_premix span").html();

	    }

	    var fat = $("#ed-breakdown-fat-price").val();
        
	    $("#breakdown_cost_carcass").html(carcass);

	    //console.log("corn price in breakdown=" + corn);

	    $("#breakdown_corn_price").html(corn);
	    $("#ed-breakdown-corn-price").val(corn);

        $("#breakdown_sbm_price").html(sbm);
        $("#ed-breakdown-sbm-price").val(sbm);

	    //$("#breakdown_fat_price").html(fat);
	    //$("#ed-breakdown-fat-price").val(fat);

        $("#breakdown_premix_price").html(premix);
        $("#ed-breakdown-premix-price").val(premix);


        //console.log("breakdowncounter=" + breakdowncounter);
        //++breakdowncounter;

        //console.log("values in displayBreakDown function corn,sbm,carcass = " + corn, sbm, carcass);

        var total = +corn + +sbm + +fat + +premix;
        var corn_percent = ((corn / total) * 100).toFixed(2);
        var sbm_percent = ((sbm / total) * 100).toFixed(2);
        var fat_percent = ((fat / total) * 100).toFixed(2);
        var premix_percent = ((premix / total) * 100).toFixed(2);

        $("#breakdown_corn_percent").html(corn_percent + '%');
        $("#breakdown_sbm_percent").html(sbm_percent + '%');
        $("#breakdown_fat_percent").html(fat_percent + '%');
        $("#breakdown_premix_percent").html(premix_percent + '%');

	    //render breakdown PIE chart
        feedAPig.renderPie(null, 0);
        //console.log("PIE rendered");

        //Calculate left column values
        //feedAPig.populate_calculator();
        //console.log("Populate Calculator completed");

        //calculate breakdown calculator values    
        feedAPig.populate_breakdown_calculator();
        //console.log("Populate Breakdown Calculator completed");

        //calculate profit values
        feedAPig.populate_profit();
        //console.log("Populate Profit completed");

	},
	getHistory: function (symbol, startDate, endDate, this_index) {
		feedAPig.renderChartData(null,this_index);
		$.ajax({
			type: "GET",
			dataType: "jsonp",
			timeout: 2500,
			cache: false,
			async: false,
			//data:{'symbol':symbol,'startDate':startDate,'endDate':endDate,'type':'monthly'},
		    //url: "webservice/index.php",
			url: "http://pbaranowski.com/zoetis_api/webservice/index.php",
			success: function (data) {
				if(typeof(Storage) !== "undefined"){
					//localStorage.removeItem("getData"+symbol);
					//localStorage["getData"+symbol] = JSON.stringify(data);
				    //renderChartData(null, this_index);
				}
			},
			error: function(statusCode, errorThrown) {
				if (statusCode.status == 0) {
				    feedAPig.useOffLineData("getHistory" + symbol, getHistory);
				}
			}
		});
	},

	getHistoryValue: function (symbol, startDate, endDate, this_index) {
	    //feedAPig.renderChartData(null,this_index);
	    // var url_local = "http://iannuccidevelopment.com/paultest/index.php?getType=getHistory&symbol=" + symbol + "&maxRecords=1&startdate=" + startDate + "&endDate=" + endDate;
	    //url_local = "webservice/index.php?getType=getHistory&symbol=" + symbol + "&maxRecords=1&startdate=" + startDate + "&endDate=" + endDate;
	    var url_local = "http://pbaranowski.com/zoetis_api/webservice/index.php?getType=getHistory&symbol=" + symbol + "&maxRecords=1&startdate=" + startDate + "&endDate=" + endDate;
	    console.log("history value url =" + url_local);
	    //console.log("fetching history values");
	    var val = 0.00;
		$.ajax({
			type: "GET",
			dataType: "jsonp",
			timeout: 5000,
			crossDomain: true,
			cache: false,
			async: false,
			url: url_local,
			success: function (data) {
			    //alert('yup');
			    if (data.results.length > 0) {
			        val = data.results[0].close;
			    }

			    if (symbol.substring(0, 2) == "ZC") {
			        if (val)
			            val = (val / 100).toFixed(2);
			        else
			            val = 0.0;
			        $("#breakdown_corn_price").html(val);
			        $("#ed-breakdown-corn-price").val(val);

			    }
			    else if (symbol.substring(0, 2) == "ZM") {
			        if (val)
			            val = val.toFixed(2);
			        else
			            val = 0.0;
			        $("#breakdown_sbm_price").html(val);
			        $("#ed-breakdown-sbm-price").val(val);
			    }
			    else if (symbol.substring(0, 2) == "HE") {
			        if (val)
			            val = val.toFixed(2);
			        else
			            val = 0.0;
			        $("#breakdown_cost_carcass").html(val);
			    }


				if(typeof(Storage) !== "undefined"){
					localStorage.removeItem("getHistoryData"+symbol);
					localStorage["getHistoryData"+symbol] = JSON.stringify(data);

				}
			},
			error: function(statusCode, errorThrown) {
			    //console.log("error in gethistoryvalue = " + statusCode + " , error=" + errorThrown);
				if (statusCode.status == 0) {
				    feedAPig.useOffLineData("getHistoryData" + symbol, getHistoryData);
				}
			},
			complete: function () {
			    //when request finishes run calculator
			    //feedAPig.updateBreakDown();
			    feedAPig.populate_breakdown_calculator();
       			feedAPig.populate_profit();
			}
		});
	},
	getHistoryRangeValue: function (symbol, startDate, endDate, this_index, months_array) {
	    //url_local = "http://iannuccidevelopment.com/paultest/index.php?getType=getHistory&symbol=" + symbol + "&maxRecords=50&startdate=" + startDate + "&endDate=" + endDate;
	    //url_local = "webservice/index.php?getType=getHistory&symbol=" + symbol + "&maxRecords=50&startdate=" + startDate + "&endDate=" + endDate;
	    var url_local = "http://pbaranowski.com/zoetis_api/webservice/index.php?getType=getHistory&symbol=" + symbol + "&maxRecords=50&startdate=" + startDate + "&endDate=" + endDate;
	    //console.log("history range value url = " + url_local);
	    if (symbol == "HEG14") {
	        //carcass
	        carcassValueArray=[];
	    }
	    else if (symbol == "ZCH14") {
	        //corn
	        cornValueArray=[];
	    }
	    else if (symbol == "ZMH14") {
	        //sbm
	        sbmValueArray=[];
	    }
	    var cd = new Date();
	    var cy = cd.getYear();
		$.ajax({
			type: "GET",
			dataType: "jsonp",
			timeout: 6000,
			cache: false,
			async: false,
            crossDomain:true,
			url: url_local,
			success: function (data) {
			    var val = 0.00;

			    if (data.results && data.results.length > 0) {
			        //val = data.results[0].close;
			        //val = val.toFixed(2);
			        for (x = 0; x < months_array.length; x++) {
			            var parseDate = months_array[x].split("/");
			            var parsedYear = parseDate[1];
			            var parsedMonth;
			            var parsedDay;


			            //If month has only a single digit add a 0 in front
			            if (parseDate[0].length == 1) { parsedMonth = 0 + parseDate[0]; }
			            else { parsedMonth = parseDate[0]; }

			            var month_array_date = "20" + parsedYear + "-" + parsedMonth + "-" + "01";
			            var isadded = 0;

			            for (i = 0; i < data.results.length; i++) {

			                var carcassValue = JSON.parse(data.results[i].close);
			                //carcassValue = carcassValue;
			                //console.log(" startdate and tradingday = " + startDate, data.results[i].tradingDay);
			                if (month_array_date == data.results[i].tradingDay) {
			                    //console.log("month date and trading day = " + month_array_date, data.results[i].tradingDay);
			                    //isadded = 1;
			                    //console.log("value from history range = " + carcassValue);
			                    if (symbol == "HEG14") {
			                        //carcass
			                        carcassValueArray.push(carcassValue);
			                    }
			                    else if (symbol == "ZCH14") {
			                        //corn
			                        cornValueArray.push(carcassValue);
			                    }
			                    else if (symbol == "ZMH14") {
			                        //sbm
			                        sbmValueArray.push(carcassValue);
			                    }
			                }

			            }
			        }
			    }
				if(typeof(Storage) !== "undefined"){
					localStorage.removeItem("getHistoryData"+symbol);
					localStorage["getHistoryData"+symbol] = JSON.stringify(data);
				}
			},
			complete:function()
			{
			    if (symbol == "ZMH14") {
			        feedAPig.parseChartArrrays(this_index, months_array);
			    }
			    
			}
            ,error: function(statusCode, errorThrown) {
			    //console.log("error in gethistoryvalue = " + statusCode + " , error=" + errorThrown);
				if (statusCode.status == 0) {
					feedAPig.useOffLineData("getHistoryData"+symbol,getHistory);
				}
			}
		});
	},
	loadInChartData: function(this_index){
		var d = new Date(),curr_date = d.getDate(),curr_month = d.getMonth();
		curr_month++;
		var curr_year = d.getFullYear();
		
		var c = new Date();
		c.setMonth(c.getMonth() - 24);
		
		var pass_date = c.getDate(),pass_month = c.getMonth();
		var pass_year = c.getFullYear();
		
		
		var startDate = pass_year + "-" + pass_date + "-" + pass_month;
		endDate = curr_year + "-" + curr_date + "-" + curr_month;
		feedAPig.getHistory("ZC*0",startDate,endDate,this_index);
	},
	useOffLineData: function(storageName,type){
		var localData = "";
		if(typeof(Storage) !== "undefined"){
			localData = JSON.parse(localStorage[storageName]);
			//feedAPig.displayLeftData(localData);
		}
	},	
	populate_calculator: function() {
		var oInput = EV.oModules.oCalculator.oFields.oInput;
		var oSingletMap = {
			// map oInput keys to div IDs
			oPigsInBarn: 'ed-pigs-in-barn',
			oBarnsInSystem: 'ed-barns-in-system',
			oStartWeight: 'ed-start-weight',
			oEndWeight: 'ed-end-weight',
			oCornCost: 'ed-corn-cost',
			oSoybeanCost: 'ed-soybean-cost',
			oFatCost: 'ed-fat-cost',
			oPremixCost: 'ed-premix-cost'
		};
	
		// loop over map labels, matching textfield boxes to inputs
		for (var sKey in oSingletMap) {
			if (oSingletMap.hasOwnProperty(sKey)) {
			    var eTextfield = $('#' + oSingletMap[sKey])[0];
			    //console.log(eTextfield);
				var	sTextfieldText = eTextfield.value;
				//console.log("text field" + sTextfieldText);
				if (sTextfieldText.length !== 0) {
					oInput[sKey].nValue = parseFloat(sTextfieldText.replace(/\,/g, ""));
					// replace innerText with parsed value unless:
					//   (1) a user just typed a period, or
					//   (2) the field already contains a period, and the user typed a 0
					// FIXME? properly formatted version of parsed value??? only in tap_enter or focus change?
					if (('.' != sTextfieldText.substr(-1)) &&
						(-1 == sTextfieldText.indexOf('.') || '0' != sTextfieldText.substr(-1))) {
						//eTextfield.innerText = oInput[sKey].nValue;
					}
				} else {
					// the user blanked the field; zero out our stored value
					oInput[sKey].nValue = 0.0;
				}
				// use localStorage to preserve the textfield text
				localStorage.setItem(oSingletMap[sKey], sTextfieldText);
			}
		}
	
		EV.oModules.oCalculator.fuComputeOutputValues();
		
	},
	populate_breakdown_calculator: function () {
	    var oInput = EV.oModules.oCalculator.oFields.oInput;
	    var oSingletMap = {
	        // map oInput keys to div IDs
	        oPigsInBarn: 'ed-pigs-in-barn',
	        oBarnsInSystem: 'ed-barns-in-system',
	        oStartWeight: 'ed-start-weight',
	        oEndWeight: 'ed-end-weight',
	        oCornCost: 'ed-breakdown-corn-price',
	        oSoybeanCost: 'ed-breakdown-sbm-price',
	        oFatCost: 'ed-breakdown-fat-price',
	        oPremixCost: 'ed-premix-cost'
	    };

	    // loop over map labels, matching textfield boxes to inputs
	    for (var sKey in oSingletMap) {
	        if (oSingletMap.hasOwnProperty(sKey)) {
	            var eTextfield = $('#' + oSingletMap[sKey])[0],
					sTextfieldText = eTextfield.value;
                
	            //console.log(eTextfield, sTextfieldText);

	            if (sTextfieldText.length !== 0) {
	                oInput[sKey].nValue = parseFloat(sTextfieldText.replace(/\,/g, ""));
	                // replace innerText with parsed value unless:
	                //   (1) a user just typed a period, or
	                //   (2) the field already contains a period, and the user typed a 0
	                // FIXME? properly formatted version of parsed value??? only in tap_enter or focus change?
	                if (('.' != sTextfieldText.substr(-1)) &&
						(-1 == sTextfieldText.indexOf('.') || '0' != sTextfieldText.substr(-1))) {
	                    //eTextfield.innerText = oInput[sKey].nValue;
	                }
	            } else {
	                // the user blanked the field; zero out our stored value
	                oInput[sKey].nValue = 0.0;
	            }
	            // use localStorage to preserve the textfield text
	            localStorage.setItem(oSingletMap[sKey], sTextfieldText);
	        }
	    }

	    EV.oModules.oCalculator.fuComputeBreakDownOutputValues();
	},

	getCostPerPig: function (corn, sbm, fat) {

	    if (!corn || !sbm || !fat)
	        return;

	    console.log(" getCostPerPig received values=" + corn, sbm, fat);

	    var oInput = EV.oModules.oCalculator.oFields.oInput;
	    var oSingletMap = {
	        // map oInput keys to div IDs
	        oPigsInBarn: 'ed-pigs-in-barn',
	        oBarnsInSystem: 'ed-barns-in-system',
	        oStartWeight: 'ed-start-weight',
	        oEndWeight: 'ed-end-weight',
	        oPremixCost: 'ed-premix-cost',
	        oCornCost: 'ed-breakdown-corn-price',
	        oSoybeanCost: 'ed-breakdown-sbm-price',
	        oFatCost: 'ed-breakdown-fat-price'
	    };

	    // loop over map labels, matching textfield boxes to inputs
	    for (var sKey in oSingletMap) {

	        if (oSingletMap.hasOwnProperty(sKey))
	        {
	            if (sKey == "oCornCost")
	            {
	                oInput[sKey].nValue = corn;
	            }
	            else if (sKey == "oSoybeanCost") {
	                oInput[sKey].nValue = sbm;
	            }
	            else if (sKey == "oFatCost") {
	                oInput[sKey].nValue = fat;
	            }
	            else
	            {
	                var eTextfield = $('#' + oSingletMap[sKey])[0];
	                var sTextfieldText = eTextfield.value;

	                //console.log(eTextfield, sTextfieldText);

	                if (sTextfieldText.length !== 0) {
	                    oInput[sKey].nValue = parseFloat(sTextfieldText.replace(/\,/g, ""));
	                    // replace innerText with parsed value unless:
	                    //   (1) a user just typed a period, or
	                    //   (2) the field already contains a period, and the user typed a 0
	                    // FIXME? properly formatted version of parsed value??? only in tap_enter or focus change?
	                    //if (('.' != sTextfieldText.substr(-1)) && (-1 == sTextfieldText.indexOf('.') || '0' != sTextfieldText.substr(-1))) {
	                    //eTextfield.innerText = oInput[sKey].nValue;
	                    //}
	                }
	                else {
	                    // the user blanked the field; zero out our stored value
	                    oInput[sKey].nValue = 0.0;
	                }
	            }
	            // use localStorage to preserve the textfield text
	            localStorage.setItem(oSingletMap[sKey], sTextfieldText);
	        }
	    }

	    var cal = EV.oModules.oCalculator;
	    cal.fuComputeHistoricalValues();
	    hist_cost_per_pig = cal.oFields.CostPerPig();
	    console.log("hist_cost_per_pig= " + hist_cost_per_pig);

	},

	populate_profit:function()
	{
	    var carcass_value = $("#breakdown_cost_carcass").html();
	    carcass_value = parseFloat(carcass_value).toFixed(2);

	    var cost_per_pig = $("#breakdown_cost_pig").html();
	    cost_per_pig = parseFloat(cost_per_pig).toFixed(2);

	    var pigs_in_barn = $("#ed-pigs-in-barn").val();
	    var barns_in_system = $("#ed-barns-in-system").val();

	    var profit_per_pig = (+carcass_value) - (+cost_per_pig);
	    profit_per_pig = profit_per_pig.toFixed(2);

	    var total_profit = (+profit_per_pig) * (+pigs_in_barn) * (+barns_in_system);
	    total_profit = total_profit.toFixed(2);
	    
	    $("#profit_PerPig").html(profit_per_pig);
	    $("#profit_Total").html(total_profit);
	},
	validateForm: function () {
		var numberRegex = /^[+-]?\d+(\.\d+)?([eE][+-]?\d+)?$/;
		var x = document.forms["feedapigcalc"]["ed-pigs-in-barn"].value;
		if ((x == null || x == "") || !numberRegex.test(x)) {
			//alert("Please enter number of pigs in barn");
			return false;
		}
		var y = document.forms["feedapigcalc"]["ed-barns-in-system"].value;
		if ((y == null || y == "") || !numberRegex.test(y)) {
			//alert("Please enter number of barn(s)");
			return false;
		}
		//console.log("validate form called");
		//feedAPig.getData();
		feedAPig.populate_calculator();
		feedAPig.populate_breakdown_calculator();
		//feedAPig.getCostPerPig(0, 0, 0);
		feedAPig.populate_profit();
	},
	setLightBox:function(a){
		lightbox = '';
		lightbox += '<div id="lightbox">';
		lightbox += '<div class="lightbox_content">'+a+'</div>';
		lightbox += '</div>';
		$('body').append(lightbox);
	},
	renderChartDate:function(n){
		Date.prototype.calcMonthsNoRollover = function(n){
	    	var dt = new Date(this);
	    	dt.setMonth(dt.getMonth()+ n) ;
			if (dt.getDate() < this.getDate()) { dt.setDate(0); }
			var m = dt.getMonth()+1;
			var y = dt.getFullYear()+'';
			y = y.match(/\d{2}$/);
			var finalDate = m + "/" + y;
	     	return finalDate;
		};
		var finalDateCalculated = new Date().calcMonthsNoRollover(n);
   		return finalDateCalculated;
	},
	renderChartData: function (data, this_index) {
	    //feedAPig.getCostPerPig(0, 0, 0);
		var chartTitle = "";
		if(this_index === 0){
			chartTitle = "6 Months";
			feedAPig.renderChartDataContent(this_index);
		}else if(this_index === 1){
			chartTitle = "1 Year";
			feedAPig.renderChartDataContent(this_index);
		}else{
			chartTitle = "2 Years";
			feedAPig.renderChartDataContent(this_index);
		}
	},
	renderTable: function (index, months_array, carcass_data, costperpig_data) {

	    var local_costperpig_data = [];
	    local_costperpig_data[0] = 87.29;
	    local_costperpig_data[1] = 86.24;
	    local_costperpig_data[2] = 85.14;
	    local_costperpig_data[3] = 86.73;
	    local_costperpig_data[4] = 79.57;
	    local_costperpig_data[5] = 81.11;
	    local_costperpig_data[6] = 81.11;
	    local_costperpig_data[7] = 81.11;


	    var local_carcass_data = [];
	    local_carcass_data[0] = 81.55;
	    local_carcass_data[1] = 81.15;
	    local_carcass_data[2] = 83.8;
	    local_carcass_data[3] = 86.5;
	    local_carcass_data[4] = 92.05;
	    local_carcass_data[5] = 85.425;
	    local_carcass_data[6] = 86.28;

	    var bcostarray = 0;
	    var bcarcassarray = 0;
	    for (z = 0; z < costperpigArray.length; z++) {
	        if (isNaN(costperpigArray[z])) {
	            bcostarray = 1; return;
	        }
	    }

	    for (z = 0; z < carcassValueArray.length; z++) {
	        if (isNaN(carcassValueArray[z])) {
	            bcarcassarray = 1; return;
	        }
	    }

	    if(bcostarray==1)
	    {
            costperpig_data=local_costperpig_data;
	    }
	    if (bcarcassarray == 1) {
	        carcass_data = local_carcass_data;
	    }



	    $('div.value_chart_content.tab_content').hide();
	    $('div.value_chart_content.tab_content div').html("");
		$('div.value_chart_content.tab_content:eq('+index+')').show()
		$('div.value_chart_content.tab_content:eq('+index+') div').highcharts({ 
			chart: { 
				plotBackgroundImage: 'files/images/chart_bg.jpg',
				//marginLeft: 10,
				type: 'area',
				plotBorderColor: '#707070',
	        	plotBorderWidth: 1
			}, 
	        title: {
	            text: " ",
	            x: -20 //center
	        },
	        xAxis: {
	            categories: months_array,
	            opposite: true,
				labels: {

					enabled: true,
					useHTML: true,
            		formatter: function() {
                 		return '<span class="xAxisLabel">' + this.value + '</span>';
            		}
	            },

	        },
 			yAxis: {
                title: {
                    text: ''
                },
                align: 'center',
                //offset: 10,
                gridLineWidth: 0,
                tickInterval: 20,
                labels: {

                	style: {
						color: '#707070',
						'font-family': 'ProximaNovaExCn-Bold',
						fontWeight: '100',
						'font-size': '25px'
					}
                },
                tickColor: '#707070',
	    		tickLength: 10,
	    		tickWidth: 1,
	    		tickPosition: 'inside'
            },
            legend: {
            	enabled: false,
                align: 'left',
                verticalAlign: 'bottom',
                y: 0,
                x: 20,
                floating: true,
                borderWidth: 0
            },
	        tooltip: {
	            enabled: true
	        },
	        credits: {
	            enabled: false
	        }, 
			exporting: { enabled: false },

	        series: [{
	            name: 'Feed costs',
	            color: '#4b9cad',
	            fillColor: {
	                linearGradient: [0, 0, 0, 300],
	                stops: [
                      [0, 'rgba(194,194,196,100)'],
                      [1, 'rgba(194,194,196,0)']
	                ]
	            },
	            data: costperpig_data,
				lineWidth: 5

	        }, {


	            name: 'carcass price',
	            color: '#eb8b23',
	            fillColor: {
	                linearGradient: [0, 0, 0, 300],
	                stops: [
                      [0, 'rgba(194,194,196,100)'],
                      [1, 'rgba(194,194,196,0)']
	                ]
	            },
	            data: carcass_data,
				lineWidth: 5
	        }]
	    });

		$('.xAxisLabel').on('click', function () {

            //Grab the value of the clicked date, store it, format it, then pass it
            var parseDate = $(this).text().split("/");
            var parsedYear = parseDate[1];
            var parsedMonth;
            var parsedDay;

            //If month has only a single digit add a 0 in front
            if (parseDate[0].length == 1) { parsedMonth = 0 + parseDate[0]; }
            else { parsedMonth = parseDate[0]; }

            //Check for and set the day for months that have 28/30/31 days
            var monthShort = [02];
            var monthMid = [04, 06, 09, 11];
            var monthLong = [01, 03, 05, 07, 08, 10, 12];

            if ($.inArray(parseInt(parsedMonth), monthShort) !== -1) { parsedDay = 28; }
            else if ($.inArray(parseInt(parsedMonth), monthMid) !== -1) { parsedDay = 30; }
            else if ($.inArray(parseInt(parsedMonth), monthLong) !== -1) { parsedDay = 31; }
            else { }

            //Cool so we have the stuff calculated now save the start and end date to a variable
            var startDate = "20" + parsedYear + "-" + parsedMonth + "-" + "01";
            var endDate = "20" + parsedYear + "-" + parsedMonth + "-" + parsedDay;

            //Set the correct symbol to pass the service for a future click
            var futureCornSymbol;
            var futureSoySymbol;
            var futureSymbolLetter;
            var cornLetter = 'ZC';
            var soyLetter = 'ZM';

            //Symbols dont change every month but every couple of months so lets create some     array's
            var tier1 = [01, 02, 03];
            var tier2 = [04, 05];
            var tier3 = [06, 07];
            var tier4 = [08, 09];
            var tier5 = [10, 11, 12];

            if ($.inArray(parseInt(parsedMonth), tier1) !== -1) { futureSymbolLetter = 'H'; }
            else if ($.inArray(parseInt(parsedMonth), tier2) !== -1) { futureSymbolLetter = 'K'; }
            else if ($.inArray(parseInt(parsedMonth), tier3) !== -1) { futureSymbolLetter = 'N'; }
            else if ($.inArray(parseInt(parsedMonth), tier4) !== -1) { futureSymbolLetter = 'U'; }
            else if ($.inArray(parseInt(parsedMonth), tier5) !== -1) { futureSymbolLetter = 'Z'; }
            else { }

            //This is for future carcass price variables
            var futureCarcassSymbol;
            var futureCarcassSymbolLetter;
            var carcassLetter = 'HE';
            var carcass_tier1 = [01, 02, 03];
            var carcass_tier2 = [04];
            var carcass_tier3 = [05];
            var carcass_tier4 = [06];
            var carcass_tier5 = [07];
            var carcass_tier6 = [08, 09];
            var carcass_tier7 = [10,11];
            var carcass_tier8 = [12];

           	if ($.inArray(parseInt(parsedMonth), carcass_tier1) !== -1) { futureCarcassSymbolLetter = 'G'; }
            else if ($.inArray(parseInt(parsedMonth), carcass_tier2) !== -1) { futureCarcassSymbolLetter = 'J'; }
            else if ($.inArray(parseInt(parsedMonth), carcass_tier3) !== -1) { futureCarcassSymbolLetter = 'K'; }
            else if ($.inArray(parseInt(parsedMonth), carcass_tier4) !== -1) { futureCarcassSymbolLetter = 'M'; }
            else if ($.inArray(parseInt(parsedMonth), carcass_tier5) !== -1) { futureCarcassSymbolLetter = 'N'; }
            else if ($.inArray(parseInt(parsedMonth), carcass_tier6) !== -1) { futureCarcassSymbolLetter = 'Q'; }
            else if ($.inArray(parseInt(parsedMonth), carcass_tier7) !== -1) { futureCarcassSymbolLetter = 'V'; }
            else if ($.inArray(parseInt(parsedMonth), carcass_tier8) !== -1) { futureCarcassSymbolLetter = 'Z'; }
            else { }

            futureCornSymbol = cornLetter + futureSymbolLetter + parsedYear;
            futureSoySymbol = soyLetter + futureSymbolLetter + parsedYear;
            futureCarcassSymbol = carcassLetter + futureCarcassSymbolLetter + parsedYear;

		    //show start date on the breakdown bar
            $("#breakdown h2").html(parsedMonth + "/" + parsedYear + " BREAKDOWN");

            feedAPig.getHistoryValue(futureCornSymbol, startDate, endDate, 0);
            feedAPig.getHistoryValue(futureSoySymbol, startDate, endDate, 0);
            feedAPig.getHistoryValue(futureCarcassSymbol, startDate, endDate, 0);
		    //pass proper date to this function to get historical fat values
            feedAPig.updateFatValue(startDate);
           

		});



	},
	renderChartDataContent:function(this_index){
		if(this_index === 0){
			var months_array=  [
				feedAPig.renderChartDate(-6).toString(),
				feedAPig.renderChartDate(-5).toString(),
				feedAPig.renderChartDate(-4).toString(),
				feedAPig.renderChartDate(-3).toString(),
				feedAPig.renderChartDate(-2).toString(),
				feedAPig.renderChartDate(-1).toString(),
				feedAPig.renderChartDate(0).toString(),
				feedAPig.renderChartDate(4).toString()
			];
		}
		else if (this_index === 1){
			var months_array=  [
				feedAPig.renderChartDate(-12).toString(),
				feedAPig.renderChartDate(-10).toString(),
				feedAPig.renderChartDate(-8).toString(),
				feedAPig.renderChartDate(-6).toString(),
				feedAPig.renderChartDate(-4).toString(),
				feedAPig.renderChartDate(-2).toString(),
				feedAPig.renderChartDate(0).toString(),
				feedAPig.renderChartDate(4).toString()
			];
		}
		else{
			var months_array=  [
				feedAPig.renderChartDate(-24).toString(),
				feedAPig.renderChartDate(-20).toString(),
				feedAPig.renderChartDate(-16).toString(),
				feedAPig.renderChartDate(-12).toString(),
				feedAPig.renderChartDate(-8).toString(),
				feedAPig.renderChartDate(-4).toString(),
				feedAPig.renderChartDate(0).toString(),
				feedAPig.renderChartDate(4).toString()
			];
		}

		
		//This bit of code will generate our data points for carcass

		//Let grab the first and last date from the selected array
		var firstMonth = (months_array[0].toString());
		var lastMonth = (months_array[months_array.length - 2].toString());

		//This function will parse the variables above and return to us a fully formatted date that we can 
		//pass to our API call to get the data
		function parseDateForMe(w){
		    //Grab the value of the clicked date, store it, format it, then pass it

		    //console.log("parsedateforme=" + w);

	        var parseDate = w.split("/");
	        var parsedYear = parseDate[1];
	        var parsedMonth;
	        var parsedDay;

	        //If month has only a single digit add a 0 in front
	        if (parseDate[0].length == 1) { parsedMonth = 0 + parseDate[0]; }
	        else { parsedMonth = parseDate[0]; }

	        //Check for and set the day for months that have 28/30/31 days
	        var monthShort = [02];
	        var monthMid = [04, 06, 09, 11];
	        var monthLong = [01, 03, 05, 07, 08, 10, 12];

	        if ($.inArray(parseInt(parsedMonth), monthShort) !== -1) { parsedDay = 28; }
	        else if ($.inArray(parseInt(parsedMonth), monthMid) !== -1) { parsedDay = 30; }
	        else if ($.inArray(parseInt(parsedMonth), monthLong) !== -1) { parsedDay = 31; }
	        else { }

	        //Cool so we have the stuff calculated now save the start and end date to a variable
	        var myFinalParsedDate = "20" + parsedYear + "-" + parsedMonth + "-" + "01";
	        return myFinalParsedDate;
		};

		var parsedFirstMonth = parseDateForMe(firstMonth);
		var parsedlastMonth = parseDateForMe(lastMonth);

		console.log(parsedFirstMonth , parsedlastMonth );
		
		feedAPig.getHistoryRangeValue('HEG14', parsedFirstMonth, parsedlastMonth, this_index, months_array);
		feedAPig.getHistoryRangeValue('ZCH14', parsedFirstMonth, parsedlastMonth, this_index, months_array);
		feedAPig.getHistoryRangeValue('ZMH14', parsedFirstMonth, parsedlastMonth, this_index, months_array);
		
	},

	parseChartArrrays: function (this_index, months_array)
	{
	    var fatarray = [];

	    costperpigArray = [];

	    console.log("months array length = " + months_array.length);
	    console.log("carcass array length = " + carcassValueArray.length);
	    console.log("corn array length = " + cornValueArray.length);
	    console.log("sbm array length = " + sbmValueArray.length);

	    //This function will parse the variables above and return to us a fully formatted date that we can 
	    //pass to our API call to get the data
	    function parseDateForMe(w) {
	        //Grab the value of the clicked date, store it, format it, then pass it

	        //console.log("parsedateforme=" + w);

	        var parseDate = w.split("/");
	        var parsedYear = parseDate[1];
	        var parsedMonth;
	        var parsedDay;

	        //If month has only a single digit add a 0 in front
	        if (parseDate[0].length == 1) { parsedMonth = 0 + parseDate[0]; }
	        else { parsedMonth = parseDate[0]; }

	        //Check for and set the day for months that have 28/30/31 days
	        var monthShort = [02];
	        var monthMid = [04, 06, 09, 11];
	        var monthLong = [01, 03, 05, 07, 08, 10, 12];

	        if ($.inArray(parseInt(parsedMonth), monthShort) !== -1) { parsedDay = 28; }
	        else if ($.inArray(parseInt(parsedMonth), monthMid) !== -1) { parsedDay = 30; }
	        else if ($.inArray(parseInt(parsedMonth), monthLong) !== -1) { parsedDay = 31; }
	        else { }

	        //Cool so we have the stuff calculated now save the start and end date to a variable
	        var myFinalParsedDate = "20" + parsedYear + "-" + parsedMonth + "-" + "01";
	        return myFinalParsedDate;
	    };

	    for (var i = 0; i < months_array.length; i++) {
	        //console.log("months array value " + i + " = "  + months_array[i]);
	        var start_date = parseDateForMe(months_array[i]);

	        //console.log("parsed date = " + start_date);
	        feedAPig.setFatValue(start_date);
	        fatarray.push(feedAPig.updateFatValue(fat_value));
	        //var fat_val = feedAPig.getFatValue();
	        //console.log("fat value = " + fat_value);
	        var corn_local = (cornValueArray[i] / 100).toFixed(2);
	        console.log(" array values of start_date, corn, sbm , carcass, fat = " + start_date, corn_local, sbmValueArray[i], carcassValueArray[i], fat_value);

	        if (corn_local && sbmValueArray[i] && fat_value) {
	            feedAPig.getCostPerPig(corn_local, sbmValueArray[i], fat_value);
	        }
	        costperpigArray.push(+hist_cost_per_pig);
	        //console.log(costperpigArray[i]);
	    }

	    //costperpigArray = [56,65,75,85,68,93,64,73];
	    //Kalyan - Add current month and future values here
	    if (carcassValueArray.length < months_array.length) {

	        for (x = 0; x < (months_array.length - carcassValueArray.length) ; x++) {
	            carcassValueArray.push(86.28);
	        }
	    }
	    console.log("months array length = " + months_array.length);
	    console.log("costperpig array  length = " + costperpigArray.length);
	    console.log("carcass array length = " + carcassValueArray.length);

	    for (z = 0; z < costperpigArray.length; z++) {
	        console.log("costperpigArray[" + z + "]=" + costperpigArray[z]);
	    }

	    //costperpigArray = [89.19, 88.15, 87.05, 88.64, 81.48, 83.01, 83.01, 83.01];
	    feedAPig.renderTable(this_index, months_array, carcassValueArray, costperpigArray);
	},
	renderPie:function(data,this_index){
	    var chartTitle = "";

	    var corn_percent = $("#breakdown_corn_percent").html().replace('%','');
	    var sbm_percent = $("#breakdown_sbm_percent").html().replace('%', '');
	    var fat_percent = $("#breakdown_fat_percent").html().replace('%', '');
	    var premix_percent = $("#breakdown_premix_percent").html().replace('%', '');

	    //console.log("rendering breakdown pie");
		if(this_index === 0){
			chartTitle = "6 Months";
		}else if(this_index === 1){
			chartTitle = "1 Year";
		}else{
			chartTitle = "2 Years";
		}
		Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
		    return {
		        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
		        stops: [
		            [0, color],
		            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
		        ]
		    };
		});
		Highcharts.setOptions({
		 colors: ['#058DC7', '#fec614', '#e98724', '#ca204a', '#24CBE5', '#64E572', '#FF9655', '#FFF263', '#6AF9C4']
		});
		$('#breakdown_chart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
				 backgroundColor:'rgba(255, 255, 255, 0.1)'
            },
			title: false,
            tooltip: false,
            plotOptions: {
                pie: {
					size:'100%',
                    allowPointSelect: false,
                    cursor: 'pointer',
                    dataLabels: true
                }
            },credits: {
				enabled: false
			},exporting: { enabled: false },
            series: [{
                type: 'pie',
                name: 'Browser share',
                data: [
                        ['Corn', +corn_percent],
                         ['SBM', +sbm_percent],
                          ['Fat', +fat_percent],
                           ['Premix', +premix_percent]
                    ]
            }]
        });
	}
}
});
