// Filename: main.js
require.config({
  paths: {
    "jquery": "libs/jquery/jquery",
	"jquery-nouislider": "libs/jquery-nouislider/jquery-nouislider",
	"highcharts":"highcharts",
	"exporting":"exporting"
  },
  shim: {
		"jquery-nouislider": ['jquery'],
		"highcharts": ['jquery'],
		"exporting": ['highcharts']
    }
});

require([
  // Load our app module and pass it to our definition function
  'zoetis',
], function(Zoetis){
  // The "app" dependency is passed in as "App"
  Zoetis.initialize();
});