Project Scope:
Platform:
Ipad HTML E-Detailer Built on XCode and using Ipad Safari Web-Kit for Page Rendering
- Data Synching using RestAPI Web Services to synch data from NASDAQ
- Dymamic Charts
- Retina Display Mobile for Ipad
Technologies:
HTML5, CSS3, JavaScript, Ajax w/ JSon, RestAPI Web Services
Libraries/Frameworks:
JQuery
TouchPunch.js (for mobile touch)
HighCharts.js (For Dynamic Charts)
GreenSock.js (For Animation and Transitions)
Custom Built Calculation Algorithm Based on JavaScript

