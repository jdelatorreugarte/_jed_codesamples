(function() {
'use strict';


Number.prototype.toLocaleString = function() {
	var	nCharsBetweenSeparators = 3,
		nCharsSeen = 0,
		nFractionalValue,
		nIndex,
		sIntegerPart = this.toFixed(0),
		sOutput = '';

	// Add the commas
	for(nIndex=sIntegerPart.length-1; nIndex!==-1; --nIndex) {
		++nCharsSeen;
		sOutput = sIntegerPart[nIndex] + sOutput;
		if(nCharsSeen % nCharsBetweenSeparators === 0 && nIndex !== 0) {
			sOutput = ',' + sOutput;
		}
	}

	return sOutput;
};


}());