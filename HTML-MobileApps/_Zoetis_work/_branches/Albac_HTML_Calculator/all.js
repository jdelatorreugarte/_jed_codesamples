/***** chrome.module.js *****/

(function() {
'use strict';

// NOTE: THE CHROME MODULE IS THE PARENT TO ALL OTHER MODULES
// Add an EV object whose modules object will contain member variables that correspond to each active module
window.EV = {
	oDomCache : {
		oBackButtonElement : undefined,
		oHelpElements : undefined,
		oNextButtonElement : undefined,
		oSiteWrapperElement : undefined
	},
	oModules : {},
  bInTestMode: false
};


// jQuery-style selector support
window.$ = function(sSelector, oElement) {
	if(oElement !== undefined) {
		return oElement.querySelectorAll(sSelector);
	}
	return document.querySelectorAll(sSelector);
};


// "DOM loaded" event listener
document.addEventListener('DOMContentLoaded', function() {
	// Remove unnecessary text nodes from each page element
	var	nPagesIndex,
		nNodesIndex,
		oAddGPOElement,
		oNode,
		oNodes,
		oPageElement,
		oPageElements = document.getElementById('pages').children;
	for(nPagesIndex=oPageElements.length-1; nPagesIndex!==-1; --nPagesIndex) {
		oPageElement = oPageElements[nPagesIndex];
		oNodes = oPageElement.childNodes;
		for(nNodesIndex=oNodes.length-1; nNodesIndex!==-1; --nNodesIndex) {
			oNode = oNodes[nNodesIndex];
			
			// If this node is not an element
			if(oNode.nodeType !== 1) {
				oPageElement.removeChild(oNode);
			}
		}
	}

	// Prevent scrolling
	document.addEventListener('touchmove', function(oEvent) {
		oEvent.preventDefault();
	});
	
	// Populate the DOM Cache
	EV.oDomCache.oBackButtonElement = document.getElementById('back-button');
	EV.oDomCache.oHelpElements = document.getElementById('pages').getElementsByClassName('help');
	EV.oDomCache.oNextButtonElement = document.getElementById('next-button');
	EV.oDomCache.oSiteWrapperElement = document.getElementById('site-wrapper');
});


// Window "load" event listener
window.addEventListener('load', function() {
	// Reveal the site
	document.body.style.visibility = 'visible';
});


}());
/***** calculator.module.js *****/

(function() {
'use strict';


// adaped from SO
Number.prototype.formatNumber = function(nDecimalPlacesToDisplay, p, d, t){
    var n = this;
    var nDecimalPlacesToDisplay = isNaN(nDecimalPlacesToDisplay = Math.abs(nDecimalPlacesToDisplay)) ? 2 : nDecimalPlacesToDisplay;
    var p = p == undefined ? '' : p;
    var d = d == undefined ? '.' : d;
    var t = t == undefined ? ',' : t;
    var s = n < 0 ? '-' : '';
    var i = parseInt(n = Math.abs(+n || 0).toFixed(nDecimalPlacesToDisplay)) + '', j = (j = i.length) > 3 ? j % 3 : 0;
    return s + p + (j ? i.substr(0, j) + t : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '$1' + t) + (nDecimalPlacesToDisplay ? d + Math.abs(n - i).toFixed(nDecimalPlacesToDisplay).slice(2) : '');
};


EV.oModules.oCalculator = {
    // Special "Number" constructor that identifies whether a value is a percentage, integer, or money value
    NumberOfType : function(sType, nValue) {
        // Locally-scoped variables
        var sExpectedType;
        
        // Function argument type checking
        sExpectedType = 'string';
        if(typeof(sType) !== sExpectedType) {
            window.alert('NumberOfType(): argument #1 must be of type "' + sExpectedType + '"');
            return;
        }
        sExpectedType = 'number';
        if(typeof(nValue) !== sExpectedType) {
            window.alert('NumberOfType(): argument #2 must be of type "' + sExpectedType + '"');
            return;
        }
        
        // Use the correct formatting function
        switch(sType) {
            case 'float':
                this.fsOutputToPage = function() {
                    // show decimal places based on size
                    // FIXME? for values>=1.0, always show three significant figures?
                    // FIXME: use sprintf for zero padding to the right (for values under 100)?
                    if (this.nValue < 0.9995) {
                        // for values less than 1.0, show three decimal places (unless it would round to 1.00 even)
                        return this.nValue.formatNumber(3);
                    } else if (this.nValue < 9.995) {
                        // for values less than 10.0, show two decimal places (unless it would round to 10.0 even)
                        return this.nValue.formatNumber(2);
                    } else if (this.nValue < 99.95) {
                        // for values less than 100.0, show one decimal place (unless it would round to 100 even)
                        return this.nValue.formatNumber(1);
                    } else {
                        // for values of 100 or more, round to the nearest integer
                        return this.nValue.formatNumber(0);
                    }
                };
                break;
            case 'integer':
                this.fsOutputToPage = function() {
                    return Math.round(this.nValue).toLocaleString();
                };
                break;
            case 'money':
                this.fsOutputToPage = function() {
                    if (this.nValue < 99.995) {
                        // for amounts under $100, show pennies (unless it would round to $100 even)
                        return this.nValue.formatNumber(2,'$');
                    } else {
                        // for amounts $100 or more, round to the nearest dollar
                        return this.nValue.formatNumber(0,'$');
                    }
                };
                break;
            case 'percent':
                this.fsOutputToPage = function() {
                    // always return an integer percentage
                    return (this.nValue * 100.0).formatNumber(0) + '%';
                };
                break;
            case 'ratio':
                this.fsOutputToPage = function(){
                    //ratio returns percent as one place decimal compared to 1
                    return (this.nValue).formatNumber(1) + ":1";
                };
                break;
        }

        // Assign values to member variables
        this.sType = sType;
        this.nValue = nValue;
    },
    
    // Populate all input AND output field values with the correct NumberOfType type
    fuInitializeOutputFieldNumberTypes : function() {
        var aRowValues,
            nRowIndex,
            oField,
            oInputFields = this.oFields.oInput,
            oOutputFields = this.oFields.oOutput,
            sKey;

        // Input fields
        for(sKey in oInputFields) {
            if(oInputFields.hasOwnProperty(sKey)) {
                if (oInputFields[sKey].hasOwnProperty('aRowValues')) {
                    // it's an array of values
                    oField = oInputFields[sKey];
                    aRowValues = oField.aRowValues;
                    // Give each row value the proper type
                    for(nRowIndex=aRowValues.length-1; nRowIndex!==-1; --nRowIndex) {
                        aRowValues[nRowIndex] = new this.NumberOfType(oField.sNumberType, 0);
                    }
                } else {
                    // it's a single value
                    oInputFields[sKey] = new this.NumberOfType(oInputFields[sKey].sNumberType, 0);
                }
            }
        }

        // Output fields
        for(sKey in oOutputFields) {
            if(oOutputFields.hasOwnProperty(sKey)) {
                // all outputs are arrays
                oField = oOutputFields[sKey];
                aRowValues = oField.aRowValues;

                // Give each row value the proper type
                for(nRowIndex=aRowValues.length-1; nRowIndex!==-1; --nRowIndex) {
                    aRowValues[nRowIndex] = new this.NumberOfType(oField.sNumberType, 0);
                }
            }
        }
    },

    // Compute all output values
    fuComputeOutputValues : function() {
        var oIntermediateFields = this.oFields.oIntermediate,
            oOutputFields = this.oFields.oOutput;

        // order is important (and a "for" loop won't guarantee it)
        oIntermediateFields.oMeatPrice.fuCompute();
        oIntermediateFields.oFeedConsumption.fuCompute();
        oIntermediateFields.oAverageFeedCost.fuCompute();
        oOutputFields.oFeedEfficiency.fuCompute();
        oOutputFields.oAverageDailyGain.fuCompute();
        oOutputFields.oFeedSavedWeight.fuCompute();
        oOutputFields.oFeedSavedDollars.fuCompute();
        oOutputFields.oAddedPorkWeight.fuCompute();
        oOutputFields.oAddedPorkDollars.fuCompute();
        oOutputFields.oAdditionalFeedCost.fuCompute();
        oOutputFields.oROI.fuCompute();
        oOutputFields.oTargetFEImprovement.fuCompute();
        oOutputFields.oTargetADGImprovement.fuCompute();
    },

    // some of the output fields depend on which tab we're looking at
    nWhichTab : undefined,
    nPerPigTab : 0,    // base
    nPerBarnTab : 1,   // per pig * pigs/barn
    nPerSystemTab : 2, // per barn * barns/system  ( = per pig * pigs/barn * barns/system )
    /* ADDED BY Rodrigo START */
    nMatchTab : 3, // match albac-roi tab
    /* ADDED BY Rodrigo END */

    // interpolation data for feed consumed as a pig grows
    oGrowthData : {
        // week number = index + 3
        aWeight : [ 12, 15, 19, 26, 33, 41, 50, 60, 71, 82, 94.5, 107, 120, 133, 147, 161, 175, 189, 203, 217, 231, 245, 258, 271, 300 ],
        aDailyFeedConsumed : [ 0.48, 0.88, 1.35, 1.6, 1.96, 2.34, 2.9, 3.52, 3.87, 4.2, 4.57, 4.94, 5.28, 5.6, 5.9, 6.2, 6.4, 6.8, 7.91, 7.02, 7.12, 7.22, 7.32, 7.32, 7.32 ]
    },

    // feed mix ratios by week
    oFeedWeight : {
        // corn, soy, fat, premix weights
        aWeek10 : [ 1095.00, 725.00, 115.00, 65.00 ],
        aWeek13 : [ 1179.81, 650.19, 110.00, 60.00 ],
        aWeek16 : [ 1265.37, 574.63, 105.00, 55.00 ],
        aWeek19 : [ 1350.93, 499.07, 100.00, 50.00 ],
        aWeek22 : [ 1434.01, 420.99, 100.00, 45.00 ],
        aWeek25 : [ 1501.14, 383.86, 70.00, 45.00 ],
        // method to choose the array of ratios by week number
        fuGetWeight : function(nWeekNumber) {
            if (nWeekNumber<13) {
                return this.aWeek10;
            } else if (nWeekNumber<16) {
                return this.aWeek13;
            } else if (nWeekNumber<19) {
                return this.aWeek16;
            } else if (nWeekNumber<22) {
                return this.aWeek19;
            } else if (nWeekNumber<25) {
                return this.aWeek22;
            } else {
                return this.aWeek25;
            }
        }
    },

    // medicated feed concentrations (from manufacturer data) in g/lb
    // (unmedicated baseline), Albac, Stafac, and Skycis
    aFeedConcentration : [ 0.0, 50.0, 20.0, 45.45 ],

    oFields : {
        oInput : {
            // baseline hog data
            oPigsInBarn : { sNumberType : 'integer' },
            oBarnsInSystem : { sNumberType : 'integer' },
            oStartWeight : { sNumberType : 'integer' }, // lbs
            oEndWeight : { sNumberType : 'integer' },   // lbs
            oMeatPrice : { sNumberType : 'money' },     // $/cwt of live carcass (not hot carcass)
            // non-medicated feed costs
            oCornCost : { sNumberType : 'money' },    // $/bushel
            oSoybeanCost : { sNumberType : 'money' }, // $/ton
            oFatCost : { sNumberType : 'money' },     // $/ton
            oPremixCost : { sNumberType : 'money' },  // $/ton
            // medicated feed data for baseline (all 0), Albac, Stafac, and Skycis
            // we include baseline so the indices line up with the output table
            oMedicatedFeedPrice : {
                aRowValues : [undefined, undefined, undefined, undefined],
                sNumberType : 'money' // $/lb
            },
            oInclusionRate : {
                aRowValues : [undefined, undefined, undefined, undefined],
                sNumberType : 'integer' // g/Ton
            },
            oImprovementADG : {
                aRowValues : [undefined, undefined, undefined, undefined],
                sNumberType : 'percent'
            },
            oImprovementFE : {
                aRowValues : [undefined, undefined, undefined, undefined],
                sNumberType : 'percent'
            }
        },
        
        oIntermediate : {
            // keep track of whether any Stafac or Skycis values are blank
            bStafacEmptyADG : false,
            bStafacEmptyFE : false,
            bSkycisEmptyADG : false,
            bSkycisEmptyFE : false,
            // calculated values
            oMeatPrice : {
                nValue : undefined,
                fuCompute : function() {
                    var nLivePrice = EV.oModules.oCalculator.oFields.oInput.oMeatPrice.nValue; // $/cwt of live carcass (not hot carcass)
                    // 100 lb / cwt
                    // 74% of the live carcass weight yields hot carcass weight
                    this.nValue = nLivePrice * 0.74 / 100.0;
                }
            },
            oFeedConsumption : {
                nValue : undefined,
                fuCompute : function() {
                    var nStartWeight = EV.oModules.oCalculator.oFields.oInput.oStartWeight.nValue,
                        nEndWeight = EV.oModules.oCalculator.oFields.oInput.oEndWeight.nValue,
                        aWeight = EV.oModules.oCalculator.oGrowthData.aWeight,
                        aDailyFeedConsumed = EV.oModules.oCalculator.oGrowthData.aDailyFeedConsumed,
                        nFirstFullWeekIndex, nEndingPartialWeekIndex, nEndingFullWeekIndex, bIsEndingPartialWeek,
                        nFeedConsumed, nFeedConsumedinFullWeeks = 0, nOneWeekConsumption;

                    // NOTE: partial weeks may be empty (have 0.0 days)
                    //       there can be a single partial week, two partial weeks (with no full weeks between), or two partials with one or more full weeks in between
                    //       UI restricts nStartWeight>=60 and nEndWeight<=300 so we're always in range
                    //       week number = week index + 3

                    // find the first index where nStartWeight<=aWeight[nFirstFullWeekIndex] but nStartWeight>aWeight[nFirstFullWeekIndex-1]
                    for (nFirstFullWeekIndex=0; nStartWeight>aWeight[nFirstFullWeekIndex]; ++nFirstFullWeekIndex);
                    // find the first index where nEndWeight>=aWeight[nEndingPartialWeekIndex] but nEndWeight<aWeight[nEndingPartialWeekIndex+1]
                    for (nEndingPartialWeekIndex=0; nEndWeight>aWeight[nEndingPartialWeekIndex+1]; ++nEndingPartialWeekIndex);
                    
                    // Determing if there is an ending partial week
                    if (nEndingPartialWeekIndex == aWeight.length - 1) {
                      bIsEndingPartialWeek = nEndWeight < aWeight[nEndingPartialWeekIndex]
                    }
                    else {
                      bIsEndingPartialWeek = nEndWeight < aWeight[nEndingPartialWeekIndex + 1];                
                    }
                    

                    nEndingFullWeekIndex = bIsEndingPartialWeek ? nEndingPartialWeekIndex - 1 : nEndingPartialWeekIndex; 
                    
                    if (nEndingPartialWeekIndex<nFirstFullWeekIndex) {
                        // single partial week
                        var nDays = 7.0 * ( nEndWeight - nStartWeight ) / ( aWeight[nFirstFullWeekIndex] - aWeight[nFirstFullWeekIndex-1] );
                        nFeedConsumed = nDays * aDailyFeedConsumed[nFirstFullWeekIndex-1];
                    } else {
                        // two partial weeks with zero or more full weeks between
                        // compute feed consumed in starting partial week
                        var nStartingDays = 7.0 * ( aWeight[nFirstFullWeekIndex] - nStartWeight ) / ( aWeight[nFirstFullWeekIndex] - aWeight[nFirstFullWeekIndex-1] );
                        nFeedConsumed = nStartingDays * aDailyFeedConsumed[nFirstFullWeekIndex-1];
                        console.log("nInitialPartialFeedConsumed: ", nFeedConsumed);
                        // compute feed consumed in intervening full weeks (if any)
                        var testOutput = [];
                        for (var nWeekIndex=nFirstFullWeekIndex; nWeekIndex<=nEndingFullWeekIndex; ++nWeekIndex) {
                            
                            nOneWeekConsumption = 7.0 * aDailyFeedConsumed[nWeekIndex];
                            
                            nFeedConsumed += nOneWeekConsumption;
                            nFeedConsumedinFullWeeks += nOneWeekConsumption;
                            testOutput.push(nOneWeekConsumption);
                        }
                        //console.log("test Full Week Feed output: ", testOutput);
                        console.log("nFeedConsumedinFullWeeks: ", nFeedConsumedinFullWeeks);
                        // compute feed consumed in ending partial week
                        if (bIsEndingPartialWeek) {
                          var nEndingDays = 7.0 * ( nEndWeight - aWeight[nEndingPartialWeekIndex] ) / ( aWeight[nEndingPartialWeekIndex+1] - aWeight[nEndingPartialWeekIndex] );
                          var nEndingPartialFeedConsumed = nEndingDays * aDailyFeedConsumed[nEndingPartialWeekIndex];
                          console.log("nEndingPartialFeedConsumed: ", nEndingPartialFeedConsumed);
                          nFeedConsumed += nEndingPartialFeedConsumed;
                        }
                        else console.log ("No Partial Ending week");
                    }
                    console.log("nFeedConsumed: ", nFeedConsumed);
                    this.nValue = nFeedConsumed;
                }
            },
            oAverageFeedCost : {
                nValue : undefined,
                nDays : undefined, // expose total feed days for ADG improvement calculation
                fuCompute : function() {
                    var nStartWeight = EV.oModules.oCalculator.oFields.oInput.oStartWeight.nValue,
                        nEndWeight = EV.oModules.oCalculator.oFields.oInput.oEndWeight.nValue,
                        aWeight = EV.oModules.oCalculator.oGrowthData.aWeight,
                        oFeedWeight = EV.oModules.oCalculator.oFeedWeight,
                        aFeedWeight, aFeedCosts, oInputFields = EV.oModules.oCalculator.oFields.oInput,
                        nFirstFullWeekIndex, nEndingPartialWeekIndex, nEndingFullWeekIndex, bIsEndingPartialWeek,
                        nDays = 0.0, nWholeWeekDays = 0.0, nFeedCost = 0.0;

                    // compute feed costs per pound (from bushel, ton, ton, ton)
                    aFeedCosts = [ oInputFields.oCornCost.nValue / 56.0,
                                   oInputFields.oSoybeanCost.nValue / 2000.0,
                                   oInputFields.oFatCost.nValue / 2000.0,
                                   oInputFields.oPremixCost.nValue / 2000.0 ];

                    // NOTE: partial weeks may be empty (have 0.0 days)
                    //       there can be a single partial week, two partial weeks (with no full weeks between), or two partials with one or more full weeks in between
                    //       UI restricts nStartWeight>=60 and nEndWeight<=300 so we're always in range
                    //       week number = week index + 3
                    //       FIXME: UI restricts start/end weight so there is always at least one full week???
                    //       no partial weeks for average feed cost, but they are used in ADG calc

                    // find the first index where nStartWeight<=aWeight[nFirstFullWeekIndex] but nStartWeight>aWeight[nFirstFullWeekIndex-1]
                    for (nFirstFullWeekIndex=0; nStartWeight>aWeight[nFirstFullWeekIndex]; ++nFirstFullWeekIndex);
                    // find the first index where nEndWeight>=aWeight[nEndingPartialWeekIndex] but nEndWeight<aWeight[nEndingPartialWeekIndex+1]
                    for (nEndingPartialWeekIndex=0; nEndWeight>aWeight[nEndingPartialWeekIndex + 1]; ++nEndingPartialWeekIndex);

                    // Determing if there is an ending partial week
                    if (nEndingPartialWeekIndex == aWeight.length - 1) {
                      bIsEndingPartialWeek = nEndWeight < aWeight[nEndingPartialWeekIndex]
                    }
                    else {
                      bIsEndingPartialWeek = nEndWeight < aWeight[nEndingPartialWeekIndex + 1];    
                    }
                    
                    nEndingFullWeekIndex = bIsEndingPartialWeek ? nEndingPartialWeekIndex -1 : nEndingPartialWeekIndex;
                    
                    
                    if (nEndingPartialWeekIndex<nFirstFullWeekIndex) {
                        // single partial week
                        nDays = 7.0 * ( nEndWeight - nStartWeight ) / ( aWeight[nFirstFullWeekIndex] - aWeight[nFirstFullWeekIndex-1] );
                        nWholeWeekDays = 0.0;
                        aFeedWeight = oFeedWeight.fuGetWeight(nEndingPartialWeekIndex+3);
                        for (var nFeedIndex=0; nFeedIndex<4; ++nFeedIndex) {
                            nFeedCost += nWholeWeekDays * aFeedWeight[nFeedIndex] * aFeedCosts[nFeedIndex];
                        }
                    } else {
                        // two partial weeks with zero or more full weeks between
                        // compute feed consumed in starting partial week
                        var nStartingDays = 7.0 * ( aWeight[nFirstFullWeekIndex] - nStartWeight ) / ( aWeight[nFirstFullWeekIndex] - aWeight[nFirstFullWeekIndex-1] );
                        nDays = nStartingDays;
                        nWholeWeekDays = 0.0;
                        aFeedWeight = oFeedWeight.fuGetWeight(nFirstFullWeekIndex-1+3);
                        for (var nFeedIndex=0; nFeedIndex<4; ++nFeedIndex) {
                            nFeedCost += 0.0 * aFeedWeight[nFeedIndex] * aFeedCosts[nFeedIndex];
                        }
                        // compute feed consumed in intervening full weeks (if any)
                        for (var nWeekIndex=nFirstFullWeekIndex; nWeekIndex<=nEndingFullWeekIndex; ++nWeekIndex) {
                            nDays += 7.0;
                            nWholeWeekDays += 7.0;
                            aFeedWeight = oFeedWeight.fuGetWeight(nWeekIndex+3);
                            for (var nFeedIndex=0; nFeedIndex<4; ++nFeedIndex) {
                                nFeedCost += 7.0 * aFeedWeight[nFeedIndex] * aFeedCosts[nFeedIndex];
                            }
                        }
                        // compute feed consumed in ending partial week
                        if (bIsEndingPartialWeek) {
                          var nEndingDays = 7.0 * ( nEndWeight - aWeight[nEndingPartialWeekIndex] ) / ( aWeight[nEndingPartialWeekIndex + 1] - aWeight[nEndingPartialWeekIndex] );
                          nDays += nEndingDays;
                          nWholeWeekDays += 0.0;
                          aFeedWeight = oFeedWeight.fuGetWeight(nEndingPartialWeekIndex+3);
                          for (var nFeedIndex=0; nFeedIndex<4; ++nFeedIndex) {
                              nFeedCost += 0.0 * aFeedWeight[nFeedIndex] * aFeedCosts[nFeedIndex];
                          }
                        }
                    }

                    this.nValue = nFeedCost / nWholeWeekDays;
                    this.nDays = nDays;
                }
            },
        },
        
        oOutput : {
            oFeedEfficiency : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var nBaseWeightChange = EV.oModules.oCalculator.oFields.oInput.oEndWeight.nValue -
                                            EV.oModules.oCalculator.oFields.oInput.oStartWeight.nValue,
                        nTotalFeedConsumption = EV.oModules.oCalculator.oFields.oIntermediate.oFeedConsumption.nValue,
                        aImprovementFE = EV.oModules.oCalculator.oFields.oInput.oImprovementFE.aRowValues,
                        nNonmedicatedFE, nImprovedFE;

                    // non-medicated feed efficiency
                    nNonmedicatedFE = nTotalFeedConsumption / nBaseWeightChange;
                    this.aRowValues[0].nValue = nNonmedicatedFE;

                    // medicated feed efficiency
                    for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        nImprovedFE = nNonmedicatedFE * ( 1.0 - aImprovementFE[nFeedIndex].nValue / 100.0 );
                        this.aRowValues[nFeedIndex].nValue = nImprovedFE;
                    }
                },
                sNumberType : 'float'
            },
            
            oAverageDailyGain : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var nBaseWeightChange = EV.oModules.oCalculator.oFields.oInput.oEndWeight.nValue -
                                            EV.oModules.oCalculator.oFields.oInput.oStartWeight.nValue,
                        nDays = EV.oModules.oCalculator.oFields.oIntermediate.oAverageFeedCost.nDays,
                        aImprovementADG = EV.oModules.oCalculator.oFields.oInput.oImprovementADG.aRowValues,
                        nNonmedicatedADG, nImprovedADG;

                    // non-medicated average daily gain
                    nNonmedicatedADG = nBaseWeightChange / nDays;
                    this.aRowValues[0].nValue = nNonmedicatedADG;

                    // medicated average daily gain
                    for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        nImprovedADG = nNonmedicatedADG * ( 1.0 + aImprovementADG[nFeedIndex].nValue / 100.0 );
                        this.aRowValues[nFeedIndex].nValue = nImprovedADG;
                    }
                },
                sNumberType : 'float'
            },
            
            oFeedSavedWeight : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var nBaseFeedWeight = EV.oModules.oCalculator.oFields.oIntermediate.oFeedConsumption.nValue,
                        aFeedEfficiencyImprovement = EV.oModules.oCalculator.oFields.oInput.oImprovementFE.aRowValues;

                    // adjust base weight for per barn or per system
                    if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerBarnTab) {
                        var nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue;
                        nBaseFeedWeight *= nPigsInBarn;
                    } else if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerSystemTab) {
                        var nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue,
                            nBarnsInSystem = EV.oModules.oCalculator.oFields.oInput.oBarnsInSystem.nValue;
                        nBaseFeedWeight *= nPigsInBarn * nBarnsInSystem;
                    }

                    // baseline weight offers no weight saved; skip it
                    for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        this.aRowValues[nFeedIndex].nValue = nBaseFeedWeight * aFeedEfficiencyImprovement[nFeedIndex].nValue / 100.0;
                    }
                },
                sNumberType : 'float'
            },
            
            oFeedSavedDollars : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var aFeedSavedWeight = EV.oModules.oCalculator.oFields.oOutput.oFeedSavedWeight.aRowValues, // lb
                        nAverageFeedCost = EV.oModules.oCalculator.oFields.oIntermediate.oAverageFeedCost.nValue, // $/ton
                        nRowValue;
                    
                    // $ saved = $ / lb feed * lb feed saved
                    // baseline feed doesn't save any medicated feed; skip it
                    for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        nRowValue = (nAverageFeedCost / 2000.0) * aFeedSavedWeight[nFeedIndex].nValue;
                        this.aRowValues[nFeedIndex].nValue = nRowValue;
                    }
                },
                sNumberType : 'money'
            },

            oAddedPorkWeight : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var nBaseWeightChange = EV.oModules.oCalculator.oFields.oInput.oEndWeight.nValue -
                                            EV.oModules.oCalculator.oFields.oInput.oStartWeight.nValue,
                        aAverageDailyGainImprovement = EV.oModules.oCalculator.oFields.oInput.oImprovementADG.aRowValues;

                    // adjust base weight for per barn or per system
                    if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerBarnTab) {
                        var nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue;
                        nBaseWeightChange *= nPigsInBarn;
                    } else if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerSystemTab) {
                        var nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue,
                            nBarnsInSystem = EV.oModules.oCalculator.oFields.oInput.oBarnsInSystem.nValue;
                        nBaseWeightChange *= nPigsInBarn * nBarnsInSystem;
                    }

                    // baseline weight offers no additional weight; skip it
                    for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        this.aRowValues[nFeedIndex].nValue = nBaseWeightChange * aAverageDailyGainImprovement[nFeedIndex].nValue / 100.0;
                    }
                },
                sNumberType : 'float'
            },

            oAddedPorkDollars : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var aPorkAdded = EV.oModules.oCalculator.oFields.oOutput.oAddedPorkWeight.aRowValues, // lb
                        nMeatPrice = EV.oModules.oCalculator.oFields.oIntermediate.oMeatPrice.nValue, // $/lb
                        nRowValue;
                    
                    // $ added = $ / lb pork * lb pork added
                    // baseline feed doesn't add any meat; skip it
                    for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        nRowValue = nMeatPrice * aPorkAdded[nFeedIndex].nValue;
                        this.aRowValues[nFeedIndex].nValue = nRowValue;
                    }
                },
                sNumberType : 'money'
            },
            
            oAdditionalFeedCost : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var oInputFields = EV.oModules.oCalculator.oFields.oInput,
                        nFeedConsumption = EV.oModules.oCalculator.oFields.oIntermediate.oFeedConsumption.nValue,
                        aFeedConcentration = EV.oModules.oCalculator.aFeedConcentration, // from manufacturer data (g/lb)
                        nInclusionRate, nCostPerTon, nFeedCost;
                    
                    // baseline feed has no additional (medicated) feed cost; skip it
                    for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        // inclusion rate (lb/ton) = inclusion rate (g/ton) / concentration (g/lb)
                        nInclusionRate = oInputFields.oInclusionRate.aRowValues[nFeedIndex].nValue / aFeedConcentration[nFeedIndex];
                        // cost per ton = price per pound * inclusion rate (lb/ton)
                        nCostPerTon = oInputFields.oMedicatedFeedPrice.aRowValues[nFeedIndex].nValue * nInclusionRate;
                        // medicated feed cost = (extra feed in lbs / 2000) * medicated feed cost per ton
                        nFeedCost = (nFeedConsumption / 2000.0) * nCostPerTon;
                        this.aRowValues[nFeedIndex].nValue = nFeedCost;
                    }

                    // adjust for per pig vs per barn vs per system
                    if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerBarnTab) {
                        var nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue;
                        for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                            this.aRowValues[nFeedIndex].nValue *= nPigsInBarn;
                        }
                    } else if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerSystemTab) {
                        var nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue,
                            nBarnsInSystem = EV.oModules.oCalculator.oFields.oInput.oBarnsInSystem.nValue;
                        for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                            this.aRowValues[nFeedIndex].nValue *= nPigsInBarn * nBarnsInSystem;
                        }
                    }
                },
                sNumberType : 'money'
            },
            
            oROI : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var aFeedSavedDollars = EV.oModules.oCalculator.oFields.oOutput.oFeedSavedDollars.aRowValues,
                        aAddedPorkDollars = EV.oModules.oCalculator.oFields.oOutput.oAddedPorkDollars.aRowValues,
                        aAdditionalFeedCost = EV.oModules.oCalculator.oFields.oOutput.oAdditionalFeedCost.aRowValues,
                        nRowValue;
                    
                    // net ROI = (added value - additional cost) / additional cost
                    // baseline has no ROI; skip it
                    for(var nFeedIndex=1; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        nRowValue =
                            (aFeedSavedDollars[nFeedIndex].nValue +
                             aAddedPorkDollars[nFeedIndex].nValue -
                             aAdditionalFeedCost[nFeedIndex].nValue) /
                            aAdditionalFeedCost[nFeedIndex].nValue;
                        this.aRowValues[nFeedIndex].nValue = nRowValue;
                    }
                },
                sNumberType : 'ratio'
            },
            
            oTargetFEImprovement : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var aFeedSavedDollars = EV.oModules.oCalculator.oFields.oOutput.oFeedSavedDollars.aRowValues,
                        aAddedPorkDollars = EV.oModules.oCalculator.oFields.oOutput.oAddedPorkDollars.aRowValues,
                        aAdditionalFeedCost = EV.oModules.oCalculator.oFields.oOutput.oAdditionalFeedCost.aRowValues,
                        aROI = EV.oModules.oCalculator.oFields.oOutput.oROI.aRowValues,
                        nAverageFeedCost = EV.oModules.oCalculator.oFields.oIntermediate.oAverageFeedCost.nValue / 2000.0, // $/ton to $/lb
                        nFeedConsumption = EV.oModules.oCalculator.oFields.oIntermediate.oFeedConsumption.nValue,
                        nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue,
                        nBarnsInSystem = EV.oModules.oCalculator.oFields.oInput.oBarnsInSystem.nValue;
                    
                    // baseline has no ROI, and we're targeting the match to Albac; skip them
                    for(var nFeedIndex=2; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        var targetDollars = (1.0+aROI[1].nValue) * aAdditionalFeedCost[nFeedIndex].nValue,
                            nRowValue =
                                (targetDollars - aAddedPorkDollars[nFeedIndex].nValue) / 
                                (nAverageFeedCost * nFeedConsumption);

                        // un-adjust for per barn or per system; the two dollar figures incorporate them
                        if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerBarnTab) {
                            nRowValue /= nPigsInBarn;
                        } else if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerSystemTab) {
                            nRowValue /= nPigsInBarn * nBarnsInSystem;
                        }

                        this.aRowValues[nFeedIndex].nValue = nRowValue; // format as %age takes care of *100.0
                    }
                },
                sNumberType : 'percent'
            },
            
            oTargetADGImprovement : {
                aRowValues : [undefined, undefined, undefined, undefined],
                fuCompute : function() {
                    var aFeedSavedDollars = EV.oModules.oCalculator.oFields.oOutput.oFeedSavedDollars.aRowValues,
                        aAddedPorkDollars = EV.oModules.oCalculator.oFields.oOutput.oAddedPorkDollars.aRowValues,
                        aAdditionalFeedCost = EV.oModules.oCalculator.oFields.oOutput.oAdditionalFeedCost.aRowValues,
                        aROI = EV.oModules.oCalculator.oFields.oOutput.oROI.aRowValues,
                        nBaseWeightChange = EV.oModules.oCalculator.oFields.oInput.oEndWeight.nValue -
                                            EV.oModules.oCalculator.oFields.oInput.oStartWeight.nValue,
                        nMeatPrice = EV.oModules.oCalculator.oFields.oIntermediate.oMeatPrice.nValue, // $/lb
                        nPigsInBarn = EV.oModules.oCalculator.oFields.oInput.oPigsInBarn.nValue,
                        nBarnsInSystem = EV.oModules.oCalculator.oFields.oInput.oBarnsInSystem.nValue;
                    
                    // baseline has no ROI, and we're targeting the match to Albac; skip them
                    for(var nFeedIndex=2; nFeedIndex<this.aRowValues.length; ++nFeedIndex) {
                        var targetDollars = (1.0+aROI[1].nValue) * aAdditionalFeedCost[nFeedIndex].nValue,
                            nRowValue =
                                (targetDollars - aFeedSavedDollars[nFeedIndex].nValue) / 
                                (nBaseWeightChange * nMeatPrice);

                        // un-adjust for per barn or per system; the two dollar figures incorporate them
                        if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerBarnTab) {
                            nRowValue /= nPigsInBarn;
                        } else if (EV.oModules.oCalculator.nWhichTab == EV.oModules.oCalculator.nPerSystemTab) {
                            nRowValue /= nPigsInBarn * nBarnsInSystem;
                        }

                        this.aRowValues[nFeedIndex].nValue = nRowValue; // format as %age takes care of *100.0
                    }
                },
                sNumberType : 'percent'
            },
        }
    },
    
    oVolumeRebatesMap : undefined
};


document.addEventListener('DOMContentLoaded', function() {
    // Assign the correct number type to each field
    EV.oModules.oCalculator.fuInitializeOutputFieldNumberTypes();
    
    // Prepopulate fields that have defaultValues
    if (localStorage.length == 0) {
      var defaultValues = $('.p-ed-value');
      for(var i in defaultValues) {
        var nValue = defaultValues[i].innerHTML || 0;
        if (nValue) {
          defaultValues[i].previousElementSibling.style.display = 'none';
        }
      }
    }
});


}());
/***** navigation.module.js *****/

(function() {
'use strict';


EV.oModules.oNavigation = {
    oBack : {
        fbButtonEnabled : function(bEnabled) {
            var oButtonElement = EV.oDomCache.oBackButtonElement;

            // Set the state only if the first argument is used
            if(typeof(bEnabled) === 'boolean') {
                if(bEnabled === true) {
                    oButtonElement.classList.add('enabled');
                } else {
                    oButtonElement.classList.remove('enabled');
                }
            }

            // Return the enabled state
            return oButtonElement.classList.contains('enabled');
        },

        fuGo : function() {
            var nCurrentPageValue,
                nIndex,
                nPageValue,
                oLoopPageElement,
                oNavigation = EV.oModules.oNavigation,
                oPageElements = document.getElementById('pages').children,
                rPageValue = /page(\d+\-\d+)/,
                sNewPage = this.oMap[oNavigation.oState.sCurrentPage](),
                sStateClass;

            oNavigation.oNext.fbButtonEnabled(true);

            // Update the navigation state object
            EV.oModules.oNavigation.oState.sCurrentPage = sNewPage;
            nCurrentPageValue = parseFloat(sNewPage.replace('-', '.'));

            // Set all pages to be NEXT, CURRENT, or PREVIOUS
            // ALSO, set the site-wrapper state class to the CURRENT element's page class
            for(nIndex=oPageElements.length-1; nIndex!==-1; --nIndex) {
                oLoopPageElement = oPageElements[nIndex];
                nPageValue = parseFloat(
                    oLoopPageElement.id.match(rPageValue)[1].replace('-', '.')
                );
                if(nPageValue > nCurrentPageValue) {
                    sStateClass = 'next';
                } else if(nPageValue === nCurrentPageValue) {
                    sStateClass = 'current';
                    document.getElementById('site-wrapper').className = 'page' + sNewPage;
                } else {
                    sStateClass = 'previous';
                }
                oLoopPageElement.className = sStateClass;
            }
        },

        oMap : {
            '2-1' : function() {
                return '1-1';
            },
            '3-1' : function() {
                return '2-1';
            }
        }
    },

    oNext : {

        fbButtonEnabled : function(bEnabled) {
            var oButtonElement = EV.oDomCache.oNextButtonElement;

            // Set the state only if the first argument is used
            if(typeof(bEnabled) === 'boolean') {
                if(bEnabled === true) {
                    oButtonElement.classList.add('enabled');
                } else {
                    oButtonElement.classList.remove('enabled');
                }
            }

            // Return the enabled state
            return oButtonElement.classList.contains('enabled');
        },
        
        fuGo : function() {
            
            
            var nCurrentPageValue,
                nIndex,
                nPageValue,
                oLoopPageElement,
                oPageElements = document.getElementById('pages').children,
                oState = EV.oModules.oNavigation.oState,
                rPageValue = /page(\d+\-\d+)/,
                sNewPage = this.oMap[oState.sCurrentPage](),
                sNewPageModuleName = 'page' + sNewPage,
                sStateClass;

            
            // Update the navigation state object
            oState.sCurrentPage = sNewPage;
            nCurrentPageValue = parseFloat(sNewPage.replace('-', '.'));

            // Run the new page's initialize() function
            if(typeof(EV.oModules[sNewPageModuleName]) === 'object' && typeof(EV.oModules[sNewPageModuleName].initialize) === 'function') {
                EV.oModules[sNewPageModuleName].initialize();
            } else if(typeof(EV.oModules.oPages[sNewPageModuleName]) === 'object' && typeof(EV.oModules.oPages[sNewPageModuleName].initialize) === 'function') {
                EV.oModules.oPages[sNewPageModuleName].initialize();
            }

            // Set all pages to be NEXT, CURRENT, or PREVIOUS
            // ALSO, set the site-wrapper state class to the CURRENT element's page class
            for(nIndex=oPageElements.length-1; nIndex!==-1; --nIndex) {
                oLoopPageElement = oPageElements[nIndex];
                nPageValue = parseFloat(
                    oLoopPageElement.id.match(rPageValue)[1].replace('-', '.')
                );
                if(nPageValue > nCurrentPageValue) {
                    sStateClass = 'next';
                } else if(nPageValue === nCurrentPageValue) {
                    sStateClass = 'current';
                    document.getElementById('site-wrapper').className = 'page' + sNewPage;
                } else {
                    sStateClass = 'previous';
                }
                oLoopPageElement.className = sStateClass;
            }
        },

        oMap : {
            '1-1' : function() {
                return '2-1';
            },
            '2-1' : function() {
                return '3-1';
            },
            '3-1' : function() {
                return '2-1'; // return to data entry page to "Recalculate"
                // window.location.reload(); // Start over and wipe all values?
            }
        }
    },
    
    oState : {
        sCurrentPage : '1-1',
        sGPO : 'NONE'
    }

};

document.addEventListener('DOMContentLoaded', function() {
    // Attach the "Go()" functions to the BACK and NEXT buttons as "touchend" event listeners
    // NOTE: "click" event listeners are not used because they result in considerable input delay on iPads
    EV.oDomCache.oNextButtonElement.addEventListener('touchend', function() {
        var oNext = EV.oModules.oNavigation.oNext;
        if(oNext.fbButtonEnabled() === true) {
            var sLocalTgtPg = "2-1";
            if(EV.oModules.oNavigation.oState["sCurrentPage"] == sLocalTgtPg){
                EV.oModules["page" + sLocalTgtPg].check_specific_field_states_before_calculation();
            } else {
                oNext.fuGo();
            }
            
        }
    });
    
});


}());
/***** pages.module.js *****/

(function() {
'use strict';


// Include the PAGES module
EV.oModules.oPages = {
    // code common across several pages; nothing to see here for now
};


}());
/***** page1-1.submodule.js *****/

(function() {
'use strict';


// Document "loaded" event listener
document.addEventListener('DOMContentLoaded', function() {
    var eBackground = $('#site-wrapper.page1-1')[0];
    // remove existing background image
    eBackground.classList.remove("pig-image-1");
    eBackground.classList.remove("pig-image-2");
    eBackground.classList.remove("pig-image-3");
    // add a random choice
    var nWhichImage = Math.floor( Math.random() * 3.0 ) + 1;
    eBackground.classList.add("pig-image-" + nWhichImage);
});


}());
/***** page2-1.submodule.js *****/

(function(sThisPageElementID){


EV.oModules[sThisPageElementID] = {

    selected: undefined,
    
    field_list: [],
    
    populate_field_list: function()
    {
        var _editors = $('.p-editor');
        for ( var i = 0; i < _editors.length; i++ ) {
            this.field_list.push(_editors[i]);
        }
        this.field_list.sort(function(a,b){
            return a.tabIndex - b.tabIndex;
        });
    },

    restore_saved_values: function()
    {
        // restore previously typed values if available
        if (localStorage.length == 0) return;

        // however, we only restore certain ones (and not all available)
        var namesToRestore = [
            'ed-lean-hog-carcass',
            'ed-corn-cost',
            'ed-soybean-cost',
            'ed-fat-cost',
            'ed-premix-cost',
            'ed-albac-cost',
            'ed-albac-inclusion',
            'ed-stafac-cost',
            'ed-stafac-inclusion',
            'ed-skycis-cost',
            'ed-skycis-inclusion'
        ];
        
        
        if (EV.bInTestMode) namesToRestore.push("ed-pigs-in-barn", "ed-barns-in-system", "ed-start-weight", "ed-end-weight", "ed-albac-adg", "ed-albac-fe");
        
        namesToRestore.forEach( function(sElementID) {
            var eTextfield = $('#' + sElementID + ' .p-ed-value')[0],
                ePrefix    = $('#' + sElementID + ' .p-ed-prefix-mutable')[0],
                sTextfieldText = localStorage.getItem(sElementID);
            // set the textfield's text back to the stored item
            eTextfield.innerText = sTextfieldText;
            // show or hide the "tap to edit" prefix depending on whether we restored anything
            if (sTextfieldText == '') {
                ePrefix.style.display = '';
            } else {
                ePrefix.style.display = 'none';
            }
        });
    },

    focus: function(node) 
    {
        if ( this.selected !== node ) {
            if ( this.selected !== undefined ) {
                this.selected.classList.remove("p-selected");
                this.selected.classList.remove("p-fresh");
                //
                // new ask for this detail - auto formatting input numbers
                // when field is no longer focused
                //
                if(this.selected.childNodes[3].innerHTML != ''){
                    this.formatInputNumbers(this.selected);
                }
                
            }

            if(node != null){
                this.selected = node;
                this.selected.classList.add("p-selected");
                this.selected.classList.add("p-fresh");
            } else {
                this.selected = undefined;
            }

            if(node != null && node != undefined){
                //
                // for the case where there is an error state visible, once focused, remove error state
                //
                this.selected.childNodes[1].classList.remove("alert-red");
                //
                // because we check for bad number entries as well
                //
                this.selected.childNodes[3].classList.remove("alert-red");
            }

        } else {
            this.selected.classList.remove("p-fresh"); // cannot be undefined, so no checking for that
        }
        // disable enter button if the last field is selected
        this.fbSetButtonEnabled_enter(!(this.field_list.indexOf(node) == this.field_list.length-1));
    },

    formatInputNumbers: function(node)
    {
        var nVal = parseFloat(node.childNodes[3].innerHTML);
        if(node.classList.contains('type-pct')){
            // format for percentage
            node.childNodes[3].innerHTML = nVal.formatNumber(1);
        } else if (node.classList.contains('type-num')){
            // format for regular numbers
            node.childNodes[3].innerHTML = nVal.formatNumber(1);
        } else if (node.classList.contains('type-prc')){
            //format for money fields
            node.childNodes[3].innerHTML = nVal.formatNumber(2);
        } else if (node.classList.contains('type-int')){
            //format for integer
            node.childNodes[3].innerHTML = nVal.formatNumber(0);
        }
    },
    
    fbSetButtonEnabled_enter: function(enable_flag)
    {
        if ( enable_flag ) {
            $('.p-num-enter')[0].classList.remove('p-num-show-disabled');
        } else {
            $('.p-num-enter')[0].classList.add('p-num-show-disabled');
        }
    },
    
    selected_clear: function()
    {
        if ( this.selected !== undefined ) {
            var valdiv = $('.p-ed-value',this.selected)[0];
            valdiv.innerText = '';
        }
    },
    
    selected_append: function(value)
    {
        if ( this.selected !== undefined ) {
            var valdiv = $('.p-ed-value',this.selected)[0];
            if ( this.selected.classList.contains("p-fresh") ) {
                this.selected.classList.remove("p-fresh");
                valdiv.innerText = '';
            }
            
            // don't allow users to type two periods
            if(value !== '.' || valdiv.innerText.indexOf(".") === -1) {
                valdiv.innerText += value;
            }
            
            var mutable_prefix = $('.p-ed-prefix-mutable',this.selected);
            if ( mutable_prefix.length > 0 ) {
                    if ( valdiv.innerText !== '' ) {
                        mutable_prefix[0].style.display = 'none';
                    }
                // mutable_prefix[0].hidden = (valdiv.innerText !== ''); // does not work in target system
            }
        }
    },
    
    selected_delete: function()
    {
        if ( this.selected !== undefined ) {
            this.selected.classList.remove("p-fresh");
            var valdiv = $('.p-ed-value',this.selected)[0];
            valdiv.innerText = valdiv.innerText.slice(0,-1);
            var mutable_prefix = $('.p-ed-prefix-mutable',this.selected);
            if ( mutable_prefix.length > 0 ) {
                    if ( valdiv.innerText == '' ) {
                        mutable_prefix[0].style.display = '';
                    }
//                mutable_prefix[0].hidden = (valdiv.innerText !== ''); // does not work in target system
            }
        }
    },
    
    copy_dependent_fields: function(current_index)
    {
        // NB: tests below are correctly structured for skycisInclusionRate==NaN (a blank field, interpreted as 0.0)
        var self = EV.oModules[sThisPageElementID],
            skycisInclusionRate = parseFloat( $('#ed-skycis-inclusion .p-ed-value')[0].innerText );
        if (typeof current_index === "undefined") {
            current_index = self.field_list.indexOf(self.selected);
        }
        var current_node = self.field_list[current_index];
        
        // if we edit Albac's FE or ADG improvement, update the competitors' too
        if ( current_index === 11 ) { // Albac ADG improvement
            $('#ed-stafac-adg .p-ed-value')[0].innerText = current_node.childNodes[3].innerText;
            $('#ed-skycis-adg .p-ed-value')[0].innerText = current_node.childNodes[3].innerText;
        }
        if ( current_index === 12 ) { // Albac FE improvement
            $('#ed-stafac-fe .p-ed-value')[0].innerText = current_node.childNodes[3].innerText;
            $('#ed-skycis-fe .p-ed-value')[0].innerText = (skycisInclusionRate > 18.1) ? current_node.childNodes[3].innerText : '0.0';
        }
        // if we edit the Skycis inclusion rate, set the FE improvement appropriately
        if ( current_index === 16 ) {
            $('#ed-skycis-fe .p-ed-value')[0].innerText = (skycisInclusionRate > 18.1) ? self.field_list[12].childNodes[3].innerText : '0.0';
        }
    },
    
    tap_field: function(evt) 
    {
        var self = EV.oModules[sThisPageElementID];
        var oldIndex = self.field_list.indexOf(self.selected);
        // check inter-field dependencies
        if ( EV.oModules[sThisPageElementID].check_interfield_dependencies() ) { return; }
        // move to the newly selected field
        EV.oModules[sThisPageElementID].focus(this);
        // copy FE/ADG dependent fields
        EV.oModules[sThisPageElementID].copy_dependent_fields(oldIndex);
    },
    
    tap_num: function() 
    {
        EV.oModules[sThisPageElementID].selected_append(this.children[0].textContent.trim());
        // copy FE/ADG dependent fields
        EV.oModules[sThisPageElementID].copy_dependent_fields();
    },
    
    tap_delete: function() 
    {
        EV.oModules[sThisPageElementID].selected_delete();
        // copy FE/ADG dependent fields
        EV.oModules[sThisPageElementID].copy_dependent_fields();
    },

    simulate_help_tap: function(warningClass)
    {
        // FIXME?  UIEvent is a hack to get this work in Chrome; will the iPad barf???
        // generate a tap event on the warningClass "help" div
        var oEvent = document.createEvent('UIEvent'), // document.createEvent('TouchEvent'),
            eWarning = $('.help.' + warningClass)[0];
        oEvent.initUIEvent('touchend',true,true); // oEvent.initTouchEvent('touchend',true,true);
        oEvent.view = window;
        oEvent.altKey = false;
        oEvent.ctrlKey = false;
        oEvent.shiftKey = false;
        oEvent.metaKey = false;
        eWarning.dispatchEvent(oEvent);
    },
    
    check_interfield_dependencies: function() 
    {
        var self = EV.oModules[sThisPageElementID];
        var current_index = self.field_list.indexOf(self.selected);
        var nStartWeight = parseFloat( $('#ed-start-weight .p-ed-value')[0].innerText ),
            nEndWeight = parseFloat( $('#ed-end-weight .p-ed-value')[0].innerText );

        // FIXME: check for too narrow a weight diff???

        // if we're trying to move off the start weight field (and it's non-empty), make sure:
        //   start weight is at least 60lbs
        //   start weight < end weight (unless end weight is empty)
        if ( current_index === 2 && nStartWeight > 0.0 ) {
            if ( nStartWeight < 60.0 ||
                 ( nEndWeight > 0.0 && nStartWeight >= nEndWeight ) ) {
                // generate a tap event on the warning-start-weight "help" div
                self.simulate_help_tap('warning-start-weight');
                return true;
            }
        }
        
        // if we're trying to move off the end weight field (and it's non-empty), make sure:
        //   end weight is no more than 300lbs
        //   start weight < end weight (unless start weight is empty)
        if ( current_index === 3 && nEndWeight > 0.0 ) {
            if ( nEndWeight <= 60.0 ||
                 nEndWeight > 300.0 ||
                 ( nStartWeight > 0.0 && nStartWeight >= nEndWeight ) ) {
                // generate a tap event on the warning-end-weight "help" div
                self.simulate_help_tap('warning-end-weight');
                return true;
            }
        }
        
        // if we're trying to move off the either weight field (and both are non-empty), make sure:
        //   the start and end weights span at least one full week from the weight-gain calendar
        if ( (current_index === 2 || current_index === 3) && nStartWeight > 0.0 && nEndWeight > 0.0 ) {
            var nFirstFullWeekIndex, nEndingPartialWeekIndex,
                aWeight = EV.oModules.oCalculator.oGrowthData.aWeight;
            // find the first index where nStartWeight<=aWeight[nFirstFullWeekIndex] but nStartWeight>aWeight[nFirstFullWeekIndex-1]
            for (nFirstFullWeekIndex=0; nStartWeight>aWeight[nFirstFullWeekIndex]; ++nFirstFullWeekIndex);
            // find the first index where nEndWeight>=aWeight[nEndingPartialWeekIndex] but nEndWeight<aWeight[nEndingPartialWeekIndex+1]
            for (nEndingPartialWeekIndex=0; nEndWeight>=aWeight[nEndingPartialWeekIndex+1]; ++nEndingPartialWeekIndex);
            // NB: ending partial week may be empty
            if ( nEndingPartialWeekIndex <= nFirstFullWeekIndex ) {
                // generate a tap event on the warning-weight-range "help" div
                self.simulate_help_tap('warning-weight-range');
                return true;
            }
        }

        // we made it; return success
        return false;
    },
    
    tap_enter: function() 
    {
        var self = EV.oModules[sThisPageElementID];
        var current_index = self.field_list.indexOf(self.selected);

        // check inter-field dependencies
        if ( self.check_interfield_dependencies() ) { return; }
        
        // if -1 is returned, element in field_list[0] will be selected, conveniently
        if ( current_index < self.field_list.length-1 ) {
            self.focus(self.field_list[current_index+1]);
        }

        // copy FE/ADG dependent fields
        EV.oModules[sThisPageElementID].copy_dependent_fields(current_index);
    },
    
    initialize: function() {
        // Check whether the "NEXT" button should be enabled
        EV.oModules[sThisPageElementID].check_enable_next_button();
        EV.oModules[sThisPageElementID].restore_saved_values();
    },
    
    check_enable_next_button: function()
    {
        //
        /*

         new rules:  we need a visual indicator for what fields remaining need input
         we don't want the check to happen on initialization (which this call does), and in fact only when 'next' is tapped (which is impossible with this logic)
         since this is already built in to next button functionality, I'm going to leave it and have it default to 'enabled' so that the splash page doesn't need
         custom code tweaks, then make a new method that does a check, changes field colors and returns a true/false to the button function allowing it to go on or die.
        
        */
        // check if any input values have been left blank
        //var oInputFields = EV.oModules.oCalculator.oFields.oInput,
        //    bHasEmpty = false;

        //for(sKey in oInputFields) {
        //    if(oInputFields.hasOwnProperty(sKey)) {
        //        if (oInputFields[sKey].hasOwnProperty('aRowValues')) {
        //            // it's an array of values
        //            var aRowValues = oInputFields[sKey].aRowValues;
        //            // check each one for an empty value (skip the baseline zeros)
        //            for(nRowIndex=1; nRowIndex<aRowValues.length; ++nRowIndex) {
        //                bHasEmpty |= aRowValues[nRowIndex].nValue === 0;
        //            }
        //        } else {
        //            // it's a single value
        //            bHasEmpty |= oInputFields[sKey].nValue === 0;
        //        }
        //    }
        //}
        //if (bHasEmpty) {
        //    // disable the Next button
        //    EV.oModules.oNavigation.oNext.fbButtonEnabled(false);
        //} else {
            // enable the Next button
            EV.oModules.oNavigation.oNext.fbButtonEnabled(true);
        //}
    },

    check_specific_field_states_before_calculation: function(){
        //
        // CHANGING the way this runs through the fields
        // original search ran through calculator arrays.  Since we need to affect the css to show an alert, lets
        // go straight to the div and look at the value.
        //

        var oNext = EV.oModules.oNavigation.oNext;

        // check if any input values have been left blank
        var oValueFields = document.getElementsByClassName('p-editor'),
            oNoneditableFields = document.getElementsByClassName('p-noeditor'),
            nCurrNode = document.getElementsByClassName('p-selected'),
            bHasEmpty = false;

        //
        // hitting 'calculate' without un-focusing the last field selected will leave the field in a non-normalized state
        // and lead to a possible bug if the user tries to recalculate and use that field first.
        //
        if(nCurrNode != undefined){
            var self = EV.oModules[sThisPageElementID];
            var oldIndex = self.field_list.indexOf(self.selected);
            EV.oModules[sThisPageElementID].focus(null);
            EV.oModules[sThisPageElementID].copy_dependent_fields(oldIndex);
        }

        for(var v in oValueFields){
            if(oValueFields[v].nodeName == "div"){//sometimes adds a function or two for spice
                if(!oValueFields[v].classList.contains("no-error-check")){//some fields are not being error checked.  the have a manually added class
                    var sCheckText = oValueFields[v].childNodes[3].innerText;

                    if(parseFloat(sCheckText) == 0){
                        //
                        //if field value is equivalent to the number 0, reject
                        //
                        oValueFields[v].childNodes[3].classList.add("alert-red");
                        bHasEmpty = true;
                    } else if (sCheckText == '') {
                        //
                        // if field value is empty, reject
                        //'tap to edit' is in a completely different div than the p-value, so red gets applied differently
                        //
                        oValueFields[v].childNodes[1].classList.add("alert-red");
                        bHasEmpty = true;
                    }
                    
                } else { // track which blankable fields have been blanked

                    var sCheckText = oValueFields[v].childNodes[3].innerText,
                        oIntermediates = EV.oModules.oCalculator.oFields.oIntermediate;
                    if (oValueFields[v].id=='ed-stafac-adg') {
                        oIntermediates.bStafacEmptyADG = ( parseFloat(sCheckText) == 0 || sCheckText == '' );
                    } else if (oValueFields[v].id=='ed-stafac-fe') {
                        oIntermediates.bStafacEmptyFE  = ( parseFloat(sCheckText) == 0 || sCheckText == '' );
                    } else if (oValueFields[v].id=='ed-skycis-adg') {
                        oIntermediates.bSkycisEmptyADG = ( parseFloat(sCheckText) == 0 || sCheckText == '' );
                    } else if (oValueFields[v].id=='ed-skycis-fe') {
                        oIntermediates.bSkycisEmptyFE  = ( parseFloat(sCheckText) == 0 || sCheckText == '' );
                    }

                }
            }
        }
/* uncomment to allow zero values to blank out appropriate fields on totals page; NB: bXxxEmptyYyy is false by default
        for(var v in oNoneditableFields){
            if(oNoneditableFields[v].nodeName == "div"){//sometimes adds a function or two for spice
                if(oNoneditableFields[v].classList.contains("no-error-check")){//some fields are not being error checked.  the have a manually added class
                    
                    // track which blankable fields have been blanked
                    var sCheckText = oNoneditableFields[v].childNodes[1].innerText,
                        oIntermediates = EV.oModules.oCalculator.oFields.oIntermediate;
                    if (oNoneditableFields[v].id=='ed-stafac-adg') {
                        oIntermediates.bStafacEmptyADG = ( parseFloat(sCheckText) == 0 || sCheckText == '' );
                    } else if (oNoneditableFields[v].id=='ed-stafac-fe') {
                        oIntermediates.bStafacEmptyFE  = ( parseFloat(sCheckText) == 0 || sCheckText == '' );
                    } else if (oNoneditableFields[v].id=='ed-skycis-adg') {
                        oIntermediates.bSkycisEmptyADG = ( parseFloat(sCheckText) == 0 || sCheckText == '' );
                    } else if (oNoneditableFields[v].id=='ed-skycis-fe') {
                        oIntermediates.bSkycisEmptyFE  = ( parseFloat(sCheckText) == 0 || sCheckText == '' );
                    }

                }
            }
        }
*/

        if (!bHasEmpty) {
          // copy text fields to calculator inputs and go
          EV.oModules[sThisPageElementID].populate_calculator();
          oNext.fuGo();
        }
    },
    
    populate_calculator: function()
    {
        var oInput = EV.oModules.oCalculator.oFields.oInput;
        var oSingletMap = {
            // map oInput keys to div IDs
            oPigsInBarn    : 'ed-pigs-in-barn',
            oBarnsInSystem : 'ed-barns-in-system',
            oStartWeight   : 'ed-start-weight',
            oEndWeight     : 'ed-end-weight',
            oMeatPrice     : 'ed-lean-hog-carcass',
            oCornCost      : 'ed-corn-cost',
            oSoybeanCost   : 'ed-soybean-cost',
            oFatCost       : 'ed-fat-cost',
            oPremixCost    : 'ed-premix-cost'
        };
        var oArrayMap = {
            // map oInput keys to div IDs
            // baseline (all 0), Albac, Stafac, and Skycis
            oMedicatedFeedPrice : [ '', 'ed-albac-cost', 'ed-stafac-cost', 'ed-skycis-cost' ],
            oInclusionRate      : [ '', 'ed-albac-inclusion', 'ed-stafac-inclusion', 'ed-skycis-inclusion' ],
            oImprovementADG     : [ '', 'ed-albac-adg', 'ed-stafac-adg', 'ed-skycis-adg' ],
            oImprovementFE      : [ '', 'ed-albac-fe', 'ed-stafac-fe', 'ed-skycis-fe' ]
        };

        // loop over map labels, matching textfield boxes to inputs
        for (var sKey in oSingletMap) {
            if (oSingletMap.hasOwnProperty(sKey)) {
                var eTextfield = $('#' + oSingletMap[sKey] + ' .p-ed-value')[0],
                    sTextfieldText = eTextfield.innerText;
                if (sTextfieldText.length !== 0) {
                    oInput[sKey].nValue = parseFloat(sTextfieldText.replace(/\,/g,""));
                    // replace innerText with parsed value unless:
                    //   (1) a user just typed a period, or
                    //   (2) the field already contains a period, and the user typed a 0
                    // FIXME? properly formatted version of parsed value??? only in tap_enter or focus change?
                    if ( ('.' != sTextfieldText.substr(-1)) &&
                         (-1 == sTextfieldText.indexOf('.') || '0' != sTextfieldText.substr(-1)) ) {
                        eTextfield.innerText = oInput[sKey].nValue;
                        EV.oModules["page2-1"].formatInputNumbers ($('#' + oSingletMap[sKey])[0]);
                    }
                } else {
                    // the user blanked the field; zero out our stored value
                    oInput[sKey].nValue = 0.0;
                }
                // use localStorage to preserve the textfield text
                localStorage.setItem( oSingletMap[sKey], sTextfieldText );
            }
        }

        // match medicated feed prices, inclusion rates, and ADG/FE improvements
        for (var sKey in oArrayMap) {
            if (oArrayMap.hasOwnProperty(sKey)) {
                // skip over the baseline
                for (var nFeedIndex=1; nFeedIndex<4; ++nFeedIndex) {
                    var eTextfield = $('#' + oArrayMap[sKey][nFeedIndex] + ' .p-ed-value')[0],
                        sTextfieldText = eTextfield.innerText;
                    if (sTextfieldText.length !== 0) {
                        oInput[sKey].aRowValues[nFeedIndex].nValue = parseFloat(sTextfieldText);
                        // replace innerText with parsed value unless:
                        //   (1) a user just typed a period, or
                        //   (2) the field already contains a period, and the user typed a 0
                        // FIXME? properly formatted version of parsed value??? only in tap_enter or focus change?
                        if ( ('.' != sTextfieldText.substr(-1)) &&
                             (-1 == sTextfieldText.indexOf('.') || '0' != sTextfieldText.substr(-1)) ) {
                            eTextfield.innerText = oInput[sKey].nValue;
                            eTextfield.innerText = oInput[sKey].aRowValues[nFeedIndex].nValue;
                        }
                    } else {
                        // the user blanked the field; zero out our stored value
                        oInput[sKey].aRowValues[nFeedIndex].nValue = 0.0;
                    }
                    // use localStorage to preserve the textfield text
                    localStorage.setItem( oArrayMap[sKey][nFeedIndex], sTextfieldText );
                }
            }
        }
    }
}

window.addEventListener('DOMContentLoaded', function() {
    var fields = $("#" + sThisPageElementID + " .p-editor");

    for ( var i = 0; i < fields.length; i++ ) {
        fields[i].addEventListener('touchend', EV.oModules[sThisPageElementID].tap_field);
    }
    
    var buttons = $("#" + sThisPageElementID + " .ps-num");
    
    for ( var i = 0; i < buttons.length; i++ ) {
        buttons[i].addEventListener('touchend', EV.oModules[sThisPageElementID].tap_num);
    }
    
    EV.oModules[sThisPageElementID].populate_field_list();
    
    $("#" + sThisPageElementID + " .ps-delete")[0].addEventListener('touchend', EV.oModules[sThisPageElementID].tap_delete);
    $("#" + sThisPageElementID + " .ps-enter")[0].addEventListener('touchend', EV.oModules[sThisPageElementID].tap_enter);
});


})('page2-1');
/***** page3-1.submodule.js *****/

(function(sThisPageElementID) {
'use strict';


// Page-specific object
EV.oModules.oPages[sThisPageElementID] = {
    initialize : function() {
        // pick the per-pig tab and calculate and show all values
        this.fuSetTab(EV.oModules.oCalculator.nPerPigTab);
        this.fuResetTable();
    },

    fuSetTab : function(nSelectedTab) {
        var aTabElements = document.getElementById(sThisPageElementID).getElementsByClassName('tab');
        EV.oModules.oCalculator.nWhichTab = nSelectedTab;
        for (var nTabIndex=0; nTabIndex<3; ++nTabIndex) {
            if (nTabIndex==nSelectedTab) {
                aTabElements[nTabIndex].classList.add('active');
            } else {
                aTabElements[nTabIndex].classList.remove('active');
            }
        }
    },

    fuResetTable : function() {
        // calculate the output values
        EV.oModules.oCalculator.fuComputeOutputValues();
        // Output those values to the page
        this.fuPrintOutputValues();
    },

    // Update each table entry with its respective output value
    fuPrintOutputValues : function() {
        var oOutputFields   = EV.oModules.oCalculator.oFields.oOutput,
            bStafacEmptyADG = EV.oModules.oCalculator.oFields.oIntermediate.bStafacEmptyADG,
            bStafacEmptyFE  = EV.oModules.oCalculator.oFields.oIntermediate.bStafacEmptyFE,
            bSkycisEmptyADG = EV.oModules.oCalculator.oFields.oIntermediate.bSkycisEmptyADG,
            bSkycisEmptyFE  = EV.oModules.oCalculator.oFields.oIntermediate.bSkycisEmptyFE,
            // oThisPageElements = document.getElementById(sThisPageElementID), unusable: Element doesn't have a getElementById method
            oFeedEfficiency = document.getElementById('row-feed-efficiency').getElementsByClassName('text'),
            oAvgDailyGain   = document.getElementById('row-avg-daily-gain').getElementsByClassName('text'),
            oFeedPounds     = document.getElementById('row-feed-pounds').getElementsByClassName('text'),
            oFeedValue      = document.getElementById('row-feed-value').getElementsByClassName('text'),
            oPorkPounds     = document.getElementById('row-pork-pounds').getElementsByClassName('text'),
            oPorkValue      = document.getElementById('row-pork-value').getElementsByClassName('text'),
            oCost           = document.getElementById('row-cost').getElementsByClassName('text'),
            oROI            = document.getElementById('row-roi').getElementsByClassName('text'),
            oRowImprovementFE  = document.getElementById('row-improvement-fe-adg').getElementsByClassName('text'),
            oRowImprovementADG  = document.getElementById('row-improvement-adg-fe').getElementsByClassName('text');
            
        // Print each column's value
        for (var nFeedIndex=0; nFeedIndex<4; ++nFeedIndex) {
            if ( (bStafacEmptyFE && nFeedIndex==2) || (bSkycisEmptyFE && nFeedIndex==3) ) {
                // the user left out some needed values; print '-'
                oFeedEfficiency[nFeedIndex+1].textContent = '-';
                oFeedPounds[nFeedIndex+1].textContent     = '-';
                oFeedValue[nFeedIndex+1].textContent      = '-';
            } else {
                // the user filled in all the needed values; print away
                oFeedEfficiency[nFeedIndex+1].textContent = oOutputFields.oFeedEfficiency.aRowValues[nFeedIndex].fsOutputToPage();
                oFeedPounds[nFeedIndex+1].textContent     = oOutputFields.oFeedSavedWeight.aRowValues[nFeedIndex].fsOutputToPage() + ' lbs';
                oFeedValue[nFeedIndex+1].textContent      = oOutputFields.oFeedSavedDollars.aRowValues[nFeedIndex].fsOutputToPage();
                if(nFeedIndex > 1 && typeof oRowImprovementFE[nFeedIndex - 1].textContent != "undefined")
                  oRowImprovementFE[nFeedIndex - 1].textContent = oOutputFields.oTargetFEImprovement.aRowValues[nFeedIndex].fsOutputToPage();                
            }
            
            if ( (bStafacEmptyADG && nFeedIndex==2) || (bSkycisEmptyADG && nFeedIndex==3) ) {
                // the user left out some needed values; print '-'
                oAvgDailyGain[nFeedIndex+1].textContent   = '-';
                oPorkPounds[nFeedIndex+1].textContent     = '-';
                oPorkValue[nFeedIndex+1].textContent      = '-';
            } else {
                // the user filled in all the needed values; print away
                oAvgDailyGain[nFeedIndex+1].textContent   = oOutputFields.oAverageDailyGain.aRowValues[nFeedIndex].fsOutputToPage();
                oPorkPounds[nFeedIndex+1].textContent     = oOutputFields.oAddedPorkWeight.aRowValues[nFeedIndex].fsOutputToPage() + ' lbs';
                oPorkValue[nFeedIndex+1].textContent      = oOutputFields.oAddedPorkDollars.aRowValues[nFeedIndex].fsOutputToPage();
                if(nFeedIndex > 1 && typeof oRowImprovementADG[nFeedIndex-1].textContent != "undefined")
                  oRowImprovementADG[nFeedIndex-1].textContent = oOutputFields.oTargetADGImprovement.aRowValues[nFeedIndex].fsOutputToPage();               
            }

            // cost is always available (the necessary fields are always required)
            oCost[nFeedIndex+1].textContent           = oOutputFields.oAdditionalFeedCost.aRowValues[nFeedIndex].fsOutputToPage();
            if ( ((bStafacEmptyFE||bStafacEmptyADG) && nFeedIndex==2) || ((bSkycisEmptyFE||bSkycisEmptyADG) && nFeedIndex==3) ) {
                // the user left out some needed values; print '-'
                oROI[nFeedIndex+1].textContent            = '-';
            } else {
                // the user filled in all the needed values; print away
                oROI[nFeedIndex+1].textContent            = oOutputFields.oROI.aRowValues[nFeedIndex].fsOutputToPage();
            }
        }

                     
    }
};


document.addEventListener('DOMContentLoaded', function() {
    /*** ATTACH EVENT LISTENERS ***/
    // FIXME: recalculate button (from page2-1?)
    // tab touches
    var aTabElements = document.getElementById(sThisPageElementID).getElementsByClassName('tab'),
        oThisModule = EV.oModules.oPages[sThisPageElementID],
        oCalculator = EV.oModules.oCalculator;
    aTabElements[oCalculator.nPerPigTab].addEventListener( 'touchend', function(){ oThisModule.fuSetTab(oCalculator.nPerPigTab); oThisModule.fuResetTable(); } );
    aTabElements[oCalculator.nPerBarnTab].addEventListener( 'touchend', function(){ oThisModule.fuSetTab(oCalculator.nPerBarnTab); oThisModule.fuResetTable(); } );
    aTabElements[oCalculator.nPerSystemTab].addEventListener( 'touchend', function(){ oThisModule.fuSetTab(oCalculator.nPerSystemTab); oThisModule.fuResetTable(); } );
});


}('page3-1'));
/***** popups.module.js *****/

(function() {
'use strict';


// Include the POPUPS module
EV.oModules.oPopups = {
    popupOpen : false,
    fuAttachEventListeners : function() {
        // Locally-scoped variables
        var nIndex,
            oCloseElements = document.querySelectorAll('#pages > div > .popups > div > .close');

        // Loop through the set of all elements of class "help", attaching a "touchend" event listener to each one
        for(nIndex=EV.oDomCache.oHelpElements.length-1; nIndex!==-1; --nIndex) {
            EV.oDomCache.oHelpElements[nIndex].addEventListener('touchend', this.fuShow);
        }

        // Handle tapping the "close" button for each popup
        for(nIndex=oCloseElements.length-1; nIndex!==-1; --nIndex) {
            oCloseElements[nIndex].addEventListener('touchend', this.fuHide);
        }

        // show help when tapped on the entire pane
        var panes = document.getElementsByClassName('cattach-popup');
        for ( var i = 0; i < panes.length; i++ ) {
            panes[i].addEventListener('touchend', this.fuShow);
        }
    },

    fuHide : function(oEvent) {
        EV.oModules.oPopups.popupOpen = false;
        var oBackButtonElement = EV.oDomCache.oBackButtonElement,
            oNextButtonElement = EV.oDomCache.oNextButtonElement;

        // Restore the NEXT button's "enabled" state
        if(oNextButtonElement.bEnabled === true) {
            oNextButtonElement.classList.add('enabled');
        } else {
            oNextButtonElement.classList.remove('enabled');
        }
        delete oNextButtonElement.bEnabled;

        // Hide the popup
        this.parentElement.classList.remove('visible');

        // Hide the background
        document.querySelector('#popup-background').classList.remove('visible');
    },

    fuShow : function(oEvent) {
        // prevents multiple pop-ups open with multi-touch
        var self = EV.oModules.oPopups;
        if ( self.popupOpen )
            return;
        else
            self.popupOpen = true;
        // Store the BACK/NEXT buttons' current "enabled" state
        EV.oDomCache.oNextButtonElement.bEnabled = EV.oDomCache.oNextButtonElement.classList.contains('enabled');

        // Disable the BACK/NEXT buttons
        var oNavigation = EV.oModules.oNavigation;
        oNavigation.oNext.fbButtonEnabled(false);

        // Show the popup
        document.querySelector(
            '#page' + EV.oModules.oNavigation.oState.sCurrentPage + ' > .popups > .' + this.classList[1]
        ).classList.add('visible');

        // Show the background
        document.querySelector('#popup-background').classList.add('visible');
    }
};

// Attach event listeners to all help bubbles
document.addEventListener('DOMContentLoaded', function() {
    EV.oModules.oPopups.fuAttachEventListeners();
});


}());
