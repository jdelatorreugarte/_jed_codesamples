PAH.Content.Matrix = {
  "1.0": {
    "conditions": [
      {
        "id": "1.0",
        "name": "Breeding & Gestation",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P4.0",
            "name": "BMD",
            "type": "MFA"
          },
          {
            "id": "P6.0",
            "name": "ER Bac L5 Gold",
            "type": "BIO"
          },
          {
            "id": "P9.0",
            "name": "FarrowSure GOLD",
            "type": "BIO"
          },
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "P18.0",
            "name": "Litterguard LT-C",
            "type": "BIO"
          },
          {
            "id": "P20.0",
            "name": "RespiSure-ONE",
            "type": "BIO"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          },
          {
            "id": "P23.0",
            "name": "ER Bac Plus",
            "type": "BIO"
          },
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "P31.0",
            "name": "Linco-Spectin",
            "type": "ST"
          },
          {
            "id": "P32.0",
            "name": "LUTALYSE",
            "type": "ST"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "P33.0",
            "name": "Pre-Def",
            "type": "ST"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C4.0",
        "name": "Clostridium",
        "products": [
          {
            "id": "P18.0",
            "name": "Litterguard LT-C",
            "type": "BIO"
          },
          {
            "id": "P4.0",
            "name": "BMD",
            "type": "MFA"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C5.0",
        "name": "<em>E. coli</em>",
        "products": [
          {
            "id": "P18.0",
            "name": "Litterguard LT-C",
            "type": "BIO"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C7.0",
        "name": "Erysipelas",
        "products": [
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P6.0",
            "name": "ER Bac L5 Gold",
            "type": "BIO"
          },
          {
            "id": "P23.0",
            "name": "ER Bac Plus",
            "type": "BIO"
          },
          {
            "id": "P9.0",
            "name": "FarrowSure GOLD",
            "type": "BIO"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C10.0",
        "name": "Leptospirosis",
        "products": [
          {
            "id": "P9.0",
            "name": "FarrowSure GOLD",
            "type": "BIO"
          },
          {
            "id": "P6.0",
            "name": "ER Bac L5 Gold",
            "type": "BIO"
          },
          {
            "id": "P23.0",
            "name": "ER Bac Plus",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C11.0",
        "name": "<em>Mycoplasmal pneumonia</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P20.0",
            "name": "RespiSure-ONE",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C12.0",
        "name": "Pasteurella",
        "products": [
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C14.0",
        "name": "PPV",
        "products": [
          {
            "id": "P9.0",
            "name": "FarrowSure GOLD",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C16.0",
        "name": "<em>S. choleraesuis</em>",
        "products": [
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C22.0",
        "name": "Supportive therapies",
        "products": [
          {
            "id": "P31.0",
            "name": "Linco-Spectin",
            "type": "ST"
          },
          {
            "id": "P32.0",
            "name": "LUTALYSE",
            "type": "ST"
          },
          {
            "id": "P33.0",
            "name": "Pre-Def",
            "type": "ST"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      }
    ],
    "name": "Breeding & Gestation"
  },
  "2.0": {
    "conditions": [
      {
        "id": "2.0",
        "name": "Farrowing to Wean",
        "products": [
          {
            "id": "P13.0",
            "name": "Fostera PCV",
            "type": "BIO"
          },
          {
            "id": "P4.0",
            "name": "BMD",
            "type": "MFA"
          },
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P20.0",
            "name": "RespiSure-ONE",
            "type": "BIO"
          },
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "P18.0",
            "name": "Litterguard LT-C",
            "type": "BIO"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C4.0",
        "name": "Clostridium",
        "products": [
          {
            "id": "P4.0",
            "name": "BMD",
            "type": "MFA"
          },
          {
            "id": "P18.0",
            "name": "Litterguard LT-C",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C5.0",
        "name": "<em>E. coli</em>",
        "products": [
          {
            "id": "P18.0",
            "name": "Litterguard LT-C",
            "type": "BIO"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C8.0",
        "name": "<em>H. parasuis</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C11.0",
        "name": "<em>Mycoplasmal pneumonia</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P20.0",
            "name": "RespiSure-ONE",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C12.0",
        "name": "Pasteurella",
        "products": [
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C13.0",
        "name": "PCV",
        "products": [
          {
            "id": "P13.0",
            "name": "Fostera PCV",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C16.0",
        "name": "<em>S. choleraesuis</em>",
        "products": [
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C17.0",
        "name": "<em>S. suis</em>",
        "products": [
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C18.0",
        "name": "SIV/Pandemic SIV",
        "products": [
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      }
    ],
    "name": "Farrowing to Wean"
  },
  "3.0": {
    "conditions": [
      {
        "id": "3.0",
        "name": "Nursery & Starter",
        "products": [
          {
            "id": "P15.0",
            "name": "IMPROVEST",
            "type": "BIO"
          },
          {
            "id": "P14.0",
            "name": "Fostera PRRS",
            "type": "BIO"
          },
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P3.0",
            "name": "Aureomycin",
            "type": "MFA"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "P2.0",
            "name": "Aureo SP",
            "type": "MFA"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P20.0",
            "name": "RespiSure-ONE",
            "type": "BIO"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT6.0",
            "name": "BMD Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C19.0",
        "name": "Off odor control",
        "products": [
          {
            "id": "P15.0",
            "name": "IMPROVEST",
            "type": "BIO"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C3.0",
        "name": "Bordetella",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C5.0",
        "name": "<em>E. coli</em>",
        "products": [
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C6.0",
        "name": "Enteritis",
        "products": [
          {
            "id": "P3.0",
            "name": "Aureomycin",
            "type": "MFA"
          },
          {
            "id": "P2.0",
            "name": "Aureo SP",
            "type": "MFA"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C7.0",
        "name": "Erysipelas",
        "products": [
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C8.0",
        "name": "<em>H. parasuis</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C9.0",
        "name": "Ileitis",
        "products": [
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C11.0",
        "name": "<em>Mycoplasmal pneumonia</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P20.0",
            "name": "RespiSure-ONE",
            "type": "BIO"
          },
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C12.0",
        "name": "Pasteurella",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "P3.0",
            "name": "Aureomycin",
            "type": "MFA"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C13.0",
        "name": "PCV",
        "products": [
          {
            "id": "P13.0",
            "name": "Fostera PCV",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C15.0",
        "name": "PRRS",
        "products": [
          {
            "id": "P14.0",
            "name": "Fostera PRRS",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C16.0",
        "name": "<em>S. choleraesuis</em>",
        "products": [
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "P2.0",
            "name": "Aureo SP",
            "type": "MFA"
          },
          {
            "id": "P3.0",
            "name": "Aureomycin",
            "type": "MFA"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C17.0",
        "name": "<em>S. suis</em>",
        "products": [
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C2.0",
        "name": "Swine dysentery",
        "products": [
          {
            "id": "WT6.0",
            "name": "BMD Soluble ",
            "type": "WT"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C18.0",
        "name": "SIV/Pandemic SIV",
        "products": [
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      }
    ],
    "name": "Nursery & Starter"
  },
  "4.0": {
    "conditions": [
      {
        "id": "4.0",
        "name": "Grower",
        "products": [
          {
            "id": "P1.0",
            "name": "Albac",
            "type": "MFA"
          },
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P15.0",
            "name": "IMPROVEST",
            "type": "BIO"
          },
          {
            "id": "P14.0",
            "name": "Fostera PRRS",
            "type": "BIO"
          },
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P20.0",
            "name": "RespiSure-ONE",
            "type": "BIO"
          },
          {
            "id": "P2.0",
            "name": "Aureo SP",
            "type": "MFA"
          },
          {
            "id": "P3.0",
            "name": "Aureomycin",
            "type": "MFA"
          },
          {
            "id": "P4.0",
            "name": "BMD",
            "type": "MFA"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT6.0",
            "name": "BMD Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C1.0",
        "name": "<em>A. pleuropneumoniae</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C19.0",
        "name": "Off odor control",
        "products": [
          {
            "id": "P15.0",
            "name": "IMPROVEST",
            "type": "BIO"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C3.0",
        "name": "Bordetella",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C5.0",
        "name": "<em>E. coli</em>",
        "products": [
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C7.0",
        "name": "Erysipelas",
        "products": [
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C8.0",
        "name": "<em>H. parasuis</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C9.0",
        "name": "Ileitis",
        "products": [
          {
            "id": "P3.0",
            "name": "Aureomycin",
            "type": "MFA"
          },
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C20.0",
        "name": "Infectious arthritis",
        "products": [
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C11.0",
        "name": "<em>Mycoplasmal pneumonia</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P20.0",
            "name": "RespiSure-ONE",
            "type": "BIO"
          },
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C12.0",
        "name": "Pasteurella",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "P3.0",
            "name": "Aureomycin",
            "type": "MFA"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C15.0",
        "name": "PRRS",
        "products": [
          {
            "id": "P14.0",
            "name": "Fostera PRRS",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C13.0",
        "name": "PCV",
        "products": [
          {
            "id": "P13.0",
            "name": "Fostera PCV",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C16.0",
        "name": "<em>S. choleraesuis</em>",
        "products": [
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT1.0",
            "name": "OXYTET Soluble",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C17.0",
        "name": "<em>S. suis</em>",
        "products": [
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C2.0",
        "name": "Swine dysentery",
        "products": [
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "WT3.0",
            "name": "Lincomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT6.0",
            "name": "BMD Soluble ",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C18.0",
        "name": "SIV/Pandemic SIV",
        "products": [
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C21.0",
        "name": "Performance and growth",
        "products": [
          {
            "id": "P1.0",
            "name": "Albac",
            "type": "MFA"
          },
          {
            "id": "P2.0",
            "name": "Aureo SP",
            "type": "MFA"
          },
          {
            "id": "P4.0",
            "name": "BMD",
            "type": "MFA"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      }
    ],
    "name": "Grower"
  },
  "5.0": {
    "conditions": [
      {
        "id": "5.0",
        "name": "Finishing & Production",
        "products": [
          {
            "id": "P15.0",
            "name": "IMPROVEST",
            "type": "BIO"
          },
          {
            "id": "P1.0",
            "name": "Albac",
            "type": "MFA"
          },
          {
            "id": "P14.0",
            "name": "Fostera PRRS",
            "type": "BIO"
          },
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "P2.0",
            "name": "Aureo SP",
            "type": "MFA"
          },
          {
            "id": "P3.0",
            "name": "Aureomycin",
            "type": "MFA"
          },
          {
            "id": "P4.0",
            "name": "BMD",
            "type": "MFA"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT3.0",
            "name": "Lincomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT4.0",
            "name": "Neo-Sol 50",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C1.0",
        "name": "<em>A. pleuropneumoniae</em>",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P7.0",
            "name": "Excede",
            "type": "AIF"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C19.0",
        "name": "Off odor control",
        "products": [
          {
            "id": "P15.0",
            "name": "IMPROVEST",
            "type": "BIO"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C7.0",
        "name": "Erysipelas",
        "products": [
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "P21.0",
            "name": "Suvaxyn E-oral",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C9.0",
        "name": "Ileitis",
        "products": [
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C20.0",
        "name": "Infectious arthritis",
        "products": [
          {
            "id": "P17.0",
            "name": "Lincomix Injectable",
            "type": "AIF"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C12.0",
        "name": "Pasteurella",
        "products": [
          {
            "id": "P5.0",
            "name": "Draxxin",
            "type": "AIF"
          },
          {
            "id": "P8.0",
            "name": "Excenel RTU",
            "type": "AIF"
          },
          {
            "id": "P19.0",
            "name": "Naxcel",
            "type": "AIF"
          },
          {
            "id": "WT2.0",
            "name": "Aureomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "WT5.0",
            "name": "Tet-Sol 324",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S3.0",
            "name": "Virtual Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S2.0",
            "name": "Walking the Pens",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C13.0",
        "name": "PCV",
        "products": [
          {
            "id": "P13.0",
            "name": "Fostera PCV",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C15.0",
        "name": "PRRS",
        "products": [
          {
            "id": "P14.0",
            "name": "Fostera PRRS",
            "type": "BIO"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C2.0",
        "name": "Swine dysentery",
        "products": [
          {
            "id": "P16.0",
            "name": "Lincomix Feed Medication",
            "type": "MFA"
          },
          {
            "id": "WT3.0",
            "name": "Lincomycin Soluble ",
            "type": "WT"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C18.0",
        "name": "SIV/Pandemic SIV",
        "products": [
          {
            "id": "P11.0",
            "name": "FluSure Pandemic/XP",
            "type": "BIO"
          },
          {
            "id": "P10.0",
            "name": "Flu DETECT\u00ae Swine ",
            "type": "Diagnostics"
          },
          {
            "id": "S4.0",
            "name": "Husbandry Educator",
            "type": "Service"
          },
          {
            "id": "S1.0",
            "name": "Individual Pig Care Certification",
            "type": "Service"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      },
      {
        "id": "C21.0",
        "name": "Performance and growth",
        "products": [
          {
            "id": "P1.0",
            "name": "Albac",
            "type": "MFA"
          },
          {
            "id": "P2.0",
            "name": "Aureo SP",
            "type": "MFA"
          },
          {
            "id": "P4.0",
            "name": "BMD",
            "type": "MFA"
          },
          {
            "id": "S5.0",
            "name": "PeopleFirst Human Capital Solutions",
            "type": "Service"
          }
        ]
      }
    ],
    "name": "Finishing & Production"
  }
};