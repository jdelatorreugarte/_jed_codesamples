// Presenter name space
var presenter = { };
presenter.version = 1;

// Syncronously load JSON from the file system
presenter._jsonLoad = function(url, async, callback) {
	try {
		var request = new XMLHttpRequest();
		request.open("GET", url, async);
		
		if (async && callback) {
			request.onreadystatechange = function() {
				if (request.readyState == 4) {
					var success = (request.status === 200 || request.status === 0) ? true : false;
					var error = (!success) ? request.statusText : null;
					var data = JSON.parse(request.responseText);
					
					error = (success && data && data["error"]) ? data["error"] : null;
					success = (success && data && data["success"]) ? true : false;
					
					callback(success, error, data);
				}
			};
		}
		
		request.send(null);
		if (!async && (request.status === 200 || request.status === 0)) {
			return JSON.parse(request.responseText);
		}
	} catch(err) {
		if (async && callback) {
			callback(false, err, null);
		}
	}
	return null;
};

// Scape a string to be included withing JSON data
presenter._jsonEscape = function(input) {
	return input
		.replace(/[\\]/g, '\\')
		.replace(/[\/]/g, '\\/')
		.replace(/[\b]/g, '\\b')
		.replace(/[\f]/g, '\\f')
		.replace(/[\n]/g, '\\n')
		.replace(/[\r]/g, '\\r')
		.replace(/[\t]/g, '\\t')
		.replace(/[\"]/g, '\\"')
	;
};

// Output information to Master Rep through the # of the URL.
presenter._command = function(command, parameters, callback) {
	// For stripping _ prefixed properties from being saved to the #
	var filter = function(key, value) {
		if (key && (key instanceof String) && key.substring(0, 1) === "_") {
			return undefined;
		}
		return value;
	};
	
	// Convert the command to an array
	if (!(command instanceof Array)) {
		command = [ command ];
	}

	// Build a path
	var path = null;
	for (var i in command) {
		path = ((i==0)?"":path + "/") + command[i];
	}
	
	// Submit the command to Master Rep and return it's JSON response
	var url = "/presenter/" + presenter.version.toFixed(1) + "/" + path + "?" + JSON.stringify(parameters, filter);
	if (presenter.config.inApp) {
		return presenter._jsonLoad(url, true, callback);
	}	
	
	// If we're running offline then return a success response.
	console.log("presenter: " + url);
	
	// hack to open pdf documents inline when viewed from desktop browser
	if ( parameters instanceof Array && parameters[0] && parameters[0].viewPdf ) {
		var win = window.open('/'+parameters[0].viewPdf.path,'_blank');
		win.focus();
	}
	
	var result = {};
	result["success"] = true;
	if (callback) {
		callback(true, null, result);
		return null;
	}
	return result;
};

presenter._configLoad = function(defaultValues) {
	
	try {
		presenter.config = presenter._jsonLoad("/presenter/config.json", false);
		if (!presenter.config.inApp) {
			console.log("presenter: Config loaded from /presenter/config.json");
		}
	}
	catch (err) {}

	// If we didn't get any data, use the default
	if (!presenter.config && defaultValues instanceof Object) {
		presenter.config = defaultValues;
		if (!presenter.config.inApp) {
			console.log("presenter: Config loaded from default values");
		}
	}
	
	// If we have nothing, create an empty Object
	if (!presenter.config) {
		presenter.config = {};
		if (!presenter.config.inApp) {
			console.log("presenter: Config load failed");
		}
	}
	
};
// Configuration passed from the iOS container.
presenter._configLoad({
	"inApp": false,
	"appVersion": 1.0,
	"nameFirst": "John",
	"nameLast": "Doe",
	"email": "john.doe@sink.orchard.net.au"
});

// Tracking methods to augment tracking data passed through for reporting.
presenter.tracking = {};
// Log a page impression to be reported on.
presenter.tracking.page = function(path, callback) {
	var params = {};
	params["path"] = path;
	presenter._command(["tracking", "page"], params, callback);
};
// Log an action to be reported on.
presenter.tracking.action = function(path, value, callback) {
	var params = {};
	params["path"] = path;
	params["value"] = value;
	presenter._command(["tracking", "action"], params, callback);
};

// Document methods to allow opening documents from within documents.
presenter.document = {};
// Open a document with key and query.
presenter.document.open = function(key, query, callback) {
	var params = {};
	params["key"] = key;
	params["query"] = query;
	presenter._command(["document", "open"], params, callback);
};
// Check if document with key exists.
presenter.document.exists = function(key, callback) {
	var params = {};
	params["key"] = key;
	presenter._command(["document", "exists"], params, callback);
};

// Pass a command out to the Master Rep containing
presenter.command = function(command, params, callback) {
	// Convert up to an array of objects
	if (!(command instanceof Array)) {
		command = [ command ];
	}
	for (var i = 0; i < command.length; i++) {
		if (!(command[i] instanceof Object)) {
			var temp = command[i];
			command[i] = {};
			command[i][temp] = ((params)?params:null);
		}
	}
	presenter._command("command", command, callback);
};

// Two way data interchange with external systems
// Not yet implemented in Master Rep
presenter.data = {};
// Output data to be passed through to external system
presenter.data.output = function(data) {	
	presenter._command("data", data);
};
// Load data imported from external system.
presenter.data.input = {};
try {
	presenter.data.input = presenter._jsonLoad("/presenter/input.json", false);
	if (!presenter.config.inApp) {
		console.log("presenter: Data loaded from /presenter/input.json");
	}
}
catch (err) {}