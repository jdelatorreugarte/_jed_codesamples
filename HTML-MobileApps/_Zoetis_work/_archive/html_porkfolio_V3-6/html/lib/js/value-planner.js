function Presentation(arg) {

	if ( arg instanceof Object ) { // should be Presentation, but when stored in array it loses the type association

		this.name = arg.name;

		this.notes = arg.notes;

		this.phases = arg.phases.slice(0); // copy array

		this.conditions = arg.conditions.slice(0);

		this.products = arg.products.slice(0);

		this.narc_id = arg.narc_id;

		this.modified = false;

	} else {

		this.name = '';

		this.notes = 'Notes';

		this.phases = [false,false,false,false,false,false];

		this.conditions = [];

		this.products = [];

		this.narc_id = arg;
	}

	this.setPhase = function(phase_id,set_to) {
		this.phases[phase_id] = set_to;
	};

	this.addCondition = function(id) {
		var int_id = parseInt(id);
		if ( this.conditions.indexOf(int_id) == -1 ) {
			this.conditions.push(int_id);
			this.updateProductList();
			this.modified = true;
		}
	};

	this.removeCondition = function(id) {
		var int_id = parseInt(id);
		var i = this.conditions.indexOf(int_id);
		if ( i != -1 ) {
			this.conditions.splice(i,1);
			this.updateProductList();
			this.modified = true;
		}
	};

	this.addProduct = function(id) {
		if ( this.products.indexOf(id) == -1 ) {
			this.products.push(id);
			this.modified = true;
		}
	};

	this._sortProductList = function() {

		var id_regex = /(P|S|WT|ST)(\d+)\.(\d+)/;

		this.products.sort(function(a,b){

			var a_id_split = a.match(id_regex);
			var b_id_split = b.match(id_regex);

			if ( a_id_split[1] < b_id_split[1] ) return -1;
			if ( a_id_split[1] > b_id_split[1] ) return 1;
			if ( parseInt(a_id_split[2]) < parseInt(b_id_split[2]) ) return -1;
			if ( parseInt(a_id_split[2]) > parseInt(b_id_split[2]) ) return 1;

			return 0;
		});
	};

	this.updateProductList = function() {

		this.products = [];

		for ( var phase = 1; phase <= 5; phase++ ) {
			if ( this.phases[phase] ) {
				var matcnds = PAH.Content.Matrix[phase + ".0"].conditions;
				for ( var cnd = 0; cnd < matcnds.length; cnd++ ) {
					var o_cnd = matcnds[cnd];
					if ( o_cnd.id.substring(0,1) == "C"
						&& this.conditions.indexOf(parseInt(o_cnd.id.substring(1))) != -1 ) {
						for ( var prod = 0; prod < o_cnd.products.length; prod++ ) {
							this.addProduct(o_cnd.products[prod].id);
						}
					}
				}
			}
 		}
		this.modified = true;

		this._sortProductList();
	};

}




function ValuePlannerStorage() {

	this.territoryId = window.localStorage.getItem('vp_territory_id') || '';
	this.presentations = eval("("+localStorage.getItem('vp_presentations')+")") || [];

	this.setTerritoryId = function(tid) {
		this.territoryId = tid;
		window.localStorage.setItem('vp_territory_id', this.territoryId);	//	saving the searched id
	};

	this.savePresentations = function() {
		window.localStorage.setItem('vp_presentations', JSON.stringify(this.presentations));
	};

	this.selectPresentation = function(index) {
		var p = this.presentations;
		for ( i = 0; i < p.length; i++ ) {
			if( i == index ) {
				return p[i];
			}
		}
	};


	this.newPresentation = function(pres) {
		this.presentations.push(pres);
		this.savePresentations();
	};


	this.removePresentation = function(index) {
		this.presentations.splice(index,1);
		this.savePresentations();
	};


}



function Email() {

	this.cc = "";
	this.to = "";
	this.subject = "";
	this.body = "";
	this.attachments = [];

}

function SetNoneSavedPlanList() {
	$('<div/>').addClass('columnBoxDiv').addClass('presentationDiv')
		.append($('<a/>').html('None Saved'))
		.appendTo($('#planList'));
}



var VP = new (function ValuePlanner() {

	this.storage = new ValuePlannerStorage();

	this.presentation = new Presentation();

	this.presentation_slot = null;

	var that = this;

	this._updatePlanList = function() {

		$('#planList').html('');

		if ( this.storage.presentations.length == 0 ) {

			SetNoneSavedPlanList();

		} else {

			for ( var i = 0; i < this.storage.presentations.length; i++ ) {

				var pres = this.storage.presentations[i];

				$('<div/>').addClass('columnBoxDiv').addClass('presentationDiv')
					.append($('<div/>').addClass('planRemove').append(
								$('<a/>').attr('href','#none').attr('data-presentation-id',i)
								.addClass('action_remove_presentation')))
					.append($('<a/>').attr('href','#VP_8').attr('data-presentation-id',i)
							.addClass('action_load_presentation').html(pres.name))
					.append($('<div/>').addClass('emailButton').append($('<a/>')
								.attr('href','#VPP_01').attr('data-presentation-id',i)))
					.appendTo($('#planList'));

			}
		}
	}


	this["call_VP_0"] = function() {

		$('#planList').removeClass('edit');


		this._updatePlanList();

		this.presentation = undefined;
		this.presentation_slot = undefined;


		$('#customerInput').val(this.storage.territoryId);
		this.checkTerritoryId();

	};

	this["action_edge_email"] = function(e) {

		var pres = this.storage.presentations[e.currentTarget.dataset.presentationId];

		this.email = new Email();

		var str = '<p>Client: ' + pres.name + ' (NARC ID: '+ pres.narc_id+')' + "</p>\n";
		str += '<p>Meeting Objectives: ' + "<br/>"+ pres.notes + "</p>\n";
		str += '<p>Meeting Agenda:' + "<br/>\n";

		for ( var i = 0; i < pres.products.length; i++ ) {
			str += PAH.Content.Products[pres.products[i]].title + "<br/>\n";
		}

		str += '</p>';

		//	str += '<b>Meeting Agenda:</b><br>'+p.name+' <br><br>';
		//str += 'Products Presented:' + "\n";
		//	str += '<b>Products Presented:</b><br>'+pp+' <br><br>';

		this.email.body = str;
		this.email.subject = 'EDGE Entry Aid for ' + pres.name;


		presenter.command("emailSend",{
			"subject": this.email.subject,
			"bodyHtml": this.email.body
			});


	};


	this["action_pdfs_email"] = function(e) {

		var tbs = [];
		var pss = [];

		this.email = new Email();

		// some products show up in both tech bulletins and fact sheets, so need to filter
		$('ul#emailTechnical > li.selected a')
		.each(function(i,el) {
			if ( tbs.indexOf(el.dataset.productId) == -1 ) {
				tbs.push(el.dataset.productId);
			}
		});

		$('ul#emailFact > li.selected a')
		.each(function(i,el) {
			if ( pss.indexOf(el.dataset.productId) == -1 ) {
				pss.push(el.dataset.productId);
			}
		});

		for ( var i = 0; i < PAH.Content.Pdfs.length; i++ ) {
			var pdf = PAH.Content.Pdfs[i];
			if ( pdf.type == 'tb' && tbs.indexOf(pdf.product) != -1 ) {
				this.email.attachments.push('valueplanner_pdfs/'+pdf.file);
			} else if ( pdf.type == 'ps' && pss.indexOf(pdf.product) != -1) {
				this.email.attachments.push('valueplanner_pdfs/'+pdf.file);
			}
		}

		this.email.subject = "Follow up on our meeting";

		presenter.command("emailSend",{
			"subject": this.email.subject,
			"bodyHtml": this.email.body,
			"attachments": this.email.attachments
		});

	};

	this["call_VPP_01-sendmail"] = function() {

		this._showLightbox(true);
		$('#email_to').val(this.email.to);
		$('#email_cc').val(this.email.cc);
		$('#email_subject').val(this.email.subject);
		$('#fieldMsg').text(this.email.body);
	};

	this["call_VPP_01-client-sendmail"] = function(param) {

		var pres = this.storage.presentations[param.presentationId];

		window.ps_products = [];
		window.tb_products = [];


		for ( var i = 0; i < PAH.Content.Pdfs.length; i++ ) {
			var pdf = PAH.Content.Pdfs[i];
			if ( pres.products.indexOf(pdf.product) != -1 ) {
				switch ( pdf.type) {
				case 'ps' :
					if ( ps_products.indexOf(pdf.product) == -1 )
						ps_products.push(pdf.product);
					break;
				case 'tb' :
					if ( tb_products.indexOf(pdf.product) == -1 )
						tb_products.push(pdf.product);
					break;
				}
			}
		}

		ps_products.sort( function(a,b) {
			return PAH.Content.Products[a].title > PAH.Content.Products[b].title ? 1 : -1;
		});
		tb_products.sort( function(a,b) {
			return PAH.Content.Products[a].title > PAH.Content.Products[b].title ? 1 : -1;
		});

		var ul_tech = $('ul#emailTechnical');
		ul_tech.html('');
		if ( tb_products.length > 0 )
			ul_tech.prev().show();
		else
			ul_tech.prev().hide();
		for ( var i = 0; i < tb_products.length; i++ ) {
			$('<li/>')
			.append($('<a/>')
					.attr('data-product-id',tb_products[i])
					.attr('href','#none')
					.addClass('action_toggle_product')
					.text(PAH.Content.Products[tb_products[i]].title))
			.appendTo(ul_tech);
		}

		var ul_fact = $('ul#emailFact');
		ul_fact.html('');
		if ( ps_products.length > 0 )
			ul_fact.prev().show();
		else
			ul_fact.prev().hide();
		for ( var i = 0; i < ps_products.length; i++ ) {
			$('<li/>')
			.append($('<a/>')
					.attr('data-product-id',ps_products[i])
					.attr('href','#none')
					.addClass('action_toggle_product')
					.text(PAH.Content.Products[ps_products[i]].title))
			.appendTo(ul_fact);
		}


		var s = $('#email_subject').val();
		s = $('#email_subject').val('Presentation Aid Email'+s);
		$('.elemTitle').html('Presentation Aid Email');
	};


	this["action_toggle_product"] = function(e) {

		$(e.currentTarget).parent().toggleClass('selected');

	};


	this["call_VPP_01"] = function(param) {
		// forward parameter to the next dialogs
		$('#popup-VPP_01 a').attr('data-presentation-id',param.presentationId);
		$('#popup-VPP_01').css('top', 308 + parseInt(param.presentationId) * 44 + 'px');

	};

	this["call_VPP_preview_plan"] = function() {
		var $target = $('#popup-VPP_preview_plan > .items > div');
		$target.html('');

		for ( var prod = 0; prod < this.presentation.products.length; prod++ ) {
			$('<div/>')
				.addClass('item')
				.append( $('<div/>').addClass('number').text(''+(prod+1)) )
				.append( $('<div/>').addClass('name').html(PAH.Content.PageDB[this.presentation.products[prod]].title) )
				.appendTo($target);
		}

		$('#popup-VPP_preview_plan > .items').perfectScrollbar();
	};

	this["call_VPP_name_edit"] = function() {
		this._showLightbox(true);
		$('#presentationName').val(this.presentation.name).focus();
		this.presentation.modified = true;
	};

	this["action_new_presentation"] = function(e) {
    	this.presentation = new Presentation(e.currentTarget.dataset.narcId);
    	this.presentation_slot = null;
	};

	this["action_save_presentation"] = function(e) {
		if ( $('#popup-VPP_09-save ul.saveStatus > li').hasClass('selected') ) {
			this.storage.newPresentation(this.presentation);
		} else {
			var pres_li = $('#popup-VPP_09-save ul.selectPresentation > li.selected > a').get(0);
			if ( pres_li && pres_li.dataset.presentationId != undefined ) {
				var slot_id = pres_li.dataset.presentationId;
				this.storage.presentations[slot_id] = new Presentation(this.presentation);
				this.storage.savePresentations();
			}
		}
	};

	this._updateUIClientInfo = function() {
		var client_name = VP_Content[this.storage.territoryId].clients[this.presentation.narc_id].narc_name;
		$('.id-narc-id').text(this.presentation.narc_id);
		$('.id-client-name').html(client_name);
	};


	this["action_load_presentation"] = function(e) {
		this.presentation_slot = e.currentTarget.dataset.presentationId;
		this.presentation = new Presentation(this.storage.presentations[this.presentation_slot]);
		$('.presentation-name').text(this.presentation.name);
		this._updateUIClientInfo();
	};



	this._openDetail = function(navId) {

		var obj = {};
		obj.dataset = {};

		var id_components = navId.match(/(P|S|WT|ST)(\d+)\.(\d+)/);

		var filename = PAH.Content.PageDB[id_components[1]+id_components[2]+'.0'].filename.substring(0,4) + '_0_index.html';

		if ( navId.substring(0,1) == "S" ) {
			obj.dataset.filepath = "../service/" + filename;
		} else {
			obj.dataset.filepath = "../product/" + filename;
		}

		PAH.Tools.loadProductFrame(obj);

		if ( id_components[3] != '1') {
			setTimeout(function(){
				$('#popframe > iframe').get(0).contentWindow.PAH.Tools.gotoContent(navId);
			},1000);
		}




		this._showLightbox(true);

	};



	this["action_plan_link"] = function(e) {
		var link = e.currentTarget.dataset.link;

		if ( typeof link == 'string' ) {
			if ( link.match(/com\./) ) {
				presenter.document.open(link);
			} else if ( link.match(/(P|S|WT)\d+\.\d+/) ) {
				this._openDetail(link);
			} else if ( link.match(/\.pdf$/) ) {
				presenter.command('viewPdf',{'path': link});
			}
		}
	};

	this["call_VP_8"] = function(e) {

		$('.listProductCard').html('');

		for ( var i = 0; i < this.presentation.products.length; i++ ) {

			var product = PAH.Content.Products[this.presentation.products[i]];

		    // custom link for improvest app - id:P15.0
			if ((product.id) == 'P15.0') {
			    var $logo = $('<a/>')
				.attr('onclick', product.link)
				.attr('href', '#none')
				.addClass('action_plan_link')
				.append($('<img/>').attr('src', '../' + product.logo_file));
			} else {
			    var $logo = $('<a/>')
				.attr('data-link', product.link)
				.attr('href', '#none')
				.addClass('action_plan_link')
				.append($('<img/>').attr('src', '../' + product.logo_file));
			}

			var $feature1 = $('<p/>'), $feature2 = $('<p/>'), $feature3 = $('<p/>');

			if (product.feature1) {
				$feature1 = $('<p/>').append(
						$('<a/>')
						.attr('data-link',product.link1)
						.attr('href','#none')
						.addClass('action_plan_link')
						.html(product.feature1)
						);
			}

			if (product.feature2) {
				$feature2 = $('<p/>').append(
						$('<a/>')
						.attr('data-link',product.link2)
						.attr('href','#none')
						.addClass('action_plan_link')
						.html(product.feature2)
						);
			}

			if (product.feature3) {
				$feature3 = $('<p/>').append(
						$('<a/>')
						.attr('data-link',product.link3)
						.attr('href','#none')
						.addClass('action_plan_link')
						.html(product.feature3)
						);
			}

			$('<li/>')
				.append($logo)
				.append($feature1)
				.append($feature2)
				.append($feature3)
				.appendTo($('.listProductCard'));

		}

		if ( this.presentation_slot === null) {
			this.popup("VPP_name_edit",true);
			setTimeout(function(){
				$('#presentationName').focus();
			},100);
		}

	};

	this["call_VP_9"] = function(e) {

		$('#productList').html('');
		var presentation = this.presentation;
		for ( var i = 0; i < this.presentation.products.length; i++ ) {

			var product = PAH.Content.Products[this.presentation.products[i]];

			$('<div/>').addClass("productListItem")
				.append($('<div/>').addClass('productListItemSelect'))
				.append($('<div/>').addClass('productListItemName').html(product.title))
				.append($('<div/>').addClass('productListItemMoved'))
				.append($('<div/>')
						.addClass('productListItemRemove')
						.append($('<a/>')
								.attr('href','#VP_9')
								.attr('data-product-id',product.id)
								.addClass('action_delete_product')
						)
				)
				.appendTo($('#productList'));

		}

	    $( "#productList" ).sortable({
			stop: function (e, ui) {
				presentation.products = $.map( $(this).children(),
						function (el) {
						return $(el).find(".productListItemRemove").children("a").attr('data-product-id');
					}
				);
				presentation.modified = true;
			}
		})

	};

	this["action_delete_product"] = function(e) {
		var i = this.presentation.products.indexOf(e.currentTarget.dataset.productId);
		this.presentation.products.splice(i,1);
	};

	this["action_VPP_09-save-as-new"] = function(e) {
		$('#popup-VPP_09-save ul.selectPresentation > li').removeClass('selected');
		$('#popup-VPP_09-save ul.saveStatus > li').addClass('selected');
	};

	this["action_VPP_09-save-as-old"] = function(e) {
		var pres_id = e.currentTarget.dataset.presentationId;
		$('#popup-VPP_09-save li').removeClass('selected');
		$('#popup-VPP_09-save ul.selectPresentation > li').eq(pres_id).addClass('selected');
		$('#popup-VPP_09-save .saveChanges > a').addClass('action_save_presentation')
			.attr('href', '#VP_0').removeClass("disabled");
	};

	this["call_VPP_09-save"] = function() {

		if ( this.storage.presentations.length > 4 ) {
			$(".saveStatus li").addClass("disabled");
			$('#popup-VPP_09-save li').removeClass('selected');
			$('#popup-VPP_09-save li > a').removeAttr('href').html("SAVE AS NEW (LIMIT REACHED)")
				.removeClass("action_VPP_09-save-as-new");
			$('#popup-VPP_09-save .saveChanges > a').removeClass('action_save_presentation')
				.removeAttr('href').addClass('disabled');
		} else {
			$(".saveStatus li").removeClass("disabled");
			$('#popup-VPP_09-save ul.selectPresentation > li').removeClass('selected');
			$('#popup-VPP_09-save ul.saveStatus > li').addClass('selected');
			$('#popup-VPP_09-save li > a').attr('href', '#').html("SAVE AS NEW")
				.addClass("action_VPP_09-save-as-new");
		}

		var ul = $('#popup-VPP_09-save ul.selectPresentation');
		ul.html('');

		for ( var i = 0; i < this.storage.presentations.length; i++ ) {

			var pres = this.storage.presentations[i];

			$('<li/>')
				.append(
						$('<a/>')
							.attr('href','#none')
							.attr('data-presentation-id',i)
							.addClass('action_VPP_09-save-as-old')
							.text(pres.name)
				).appendTo(ul);

		}

	};


	this["call_VPP_meeting-notes-edit"] = function() {
		$('#popup-VPP_meeting-notes-edit textarea').val(this.presentation.notes);
		$('#popup-VPP_meeting-notes-edit textarea').focus();
	};


	this["action_generate_plan"] = function(e) {
		$(".arrowLine").removeClass("position1").removeClass("position2");
		this.popup("VPP_09",false,e.currentTarget.dataset);
	};


	this["call_VPP_09"] = function(e) {
		$('#popup-VPP_09 > input').focus();
	};

	this["call_VPP_09-selectcategory"] = function() {

		var ul = $('#popup-VPP_09-selectcategory ul.selectCategory');
		ul.html('');

		for ( var i = 0; i < PAH.Content.Categories.length; i++ ) {
			var cat = PAH.Content.Categories[i];

			$('<li/>')
				.append(
					$('<a/>')
						.attr("href","#VPP_09-selectproduct")
						.attr("data-param-cat-id",i)
						.text(cat.name)
				).appendTo(ul);

		}
	};

	this["call_VPP_09-selectproduct"] = function(param) {
		if ( param.paramCatId == undefined ) return;

		var ul = $('#popup-VPP_09-selectproduct ul.selectProductService');
		ul.html('');

        var title = PAH.Content.Categories[param.paramCatId].name
        var popupTopTitle = $('#popup-VPP_09-selectproduct div.popupTopTitle');
        popupTopTitle.html(title);

		var products = PAH.Content.Categories[param.paramCatId].products;

		for ( var i = 0; i < products.length; i++ ) {

			var prod = PAH.Content.Products[products[i]];

			$('<li/>').append(
					$('<a/>')
					.attr('href','#VP_9')
					.attr('data-product-id',products[i])
					.addClass('action_add_product')
					.text(prod.title)
			).appendTo(ul);

		}
	};

	this["action_add_product"] = function(e) {
		if ( this.presentation.products.indexOf(e.currentTarget.dataset.productId) == -1 ) {
			this.presentation.products.push(e.currentTarget.dataset.productId);
		}
	};

	this["call_VPP_09-customerselector"] = function() {

		var ul = $("#popup-VPP_09-customerselector ul.selectCustomer");
		ul.html('');

		var clients = VP_Content[this.storage.territoryId].clients;

		for ( var clid in clients ) {
			$('<li/>').append(
					$('<a/>').attr("href","#VP_9")
					.attr("data-narc-id",clid)
					.addClass("action_change_client")
					.text(clients[clid].narc_name)
					).appendTo(ul);
		}
	};

	this["action_change_client"] = function(e) {
		this.presentation.narc_id = e.currentTarget.dataset.narcId;
		this._updateUIClientInfo();
	};


   this.checkTerritoryId = function() {
 		var that = this;
		var territory_id = $("#customerInput").val();	//	assume territory id

		if ( typeof VP_Content[territory_id] == 'object' ) {
			var territory = VP_Content[territory_id];
			var str = '';
			var sa = [];
			for ( var clid in territory.clients ) {
				var client = territory.clients[clid];
				var name = client.narc_name.replace(/^ */g,'') + ' (' + client.narc_id + ')';
				sa.push(Object({"name": name, "narc_id": client.narc_id }));
			}
			sa.sort(that.orderByNameAscending);
			for ( i = 0; i < sa.length; i++ ) {
				str += '<div class="columnBoxDiv customerDiv">'
				+'<a href="#VP_6" class="action_new_presentation" data-narc-id="'+sa[i].narc_id+'">'+sa[i].name+'</a></div>';
			}
			$('#customerDivScroll').html(str);
			this.storage.setTerritoryId(territory_id);
		} else {
			$('#customerDivScroll').html('');
		}

		// $('#customerDivScroll').perfectScrollbar();
	};


	this._sortConditionList = function(conditions) {

		var strip_html_regex = /<[^>]*>/g;

		conditions.sort(function(a,b){
			var aclean = a.name.replace(strip_html_regex, "").toLowerCase();
			var bclean = b.name.replace(strip_html_regex, "").toLowerCase();

			if ( aclean < bclean ) return -1;
			if ( aclean > bclean ) return 1;

			return 0;

		});

	};



    this._populatePhaseConditions = function() {

    	var active_phases = $('#phaseLeft .phaseLeftList.active a');

    	if ( active_phases.length > 0) {

        	$(".arrowLine").addClass('position2');
        	$("#phaseTwo").removeClass('phaseSelectOff');
        	$(".planActionButtons").removeClass('off');

    	} else {

        	$("#phaseTwo").addClass('phaseSelectOff');
        	$(".arrowLine").removeClass('position2');
        	$(".planActionButtons").addClass('off');
    	}

    	var condition_list = {};

    	active_phases.each(function(){

    		var matrix_phase_id = "" + this.dataset.phaseId + ".0";

    		var phase_obj = PAH.Content.Matrix[matrix_phase_id];

    		for ( var i = 0; i < phase_obj.conditions.length; i++ ) {

    			var cond = phase_obj.conditions[i];

    			var cond_id = cond.id.match(/C(\d+)\.0/);

    			if ( cond_id != undefined && condition_list[cond_id[1]] == undefined ) {
    				cond.id_int = parseInt(cond_id[1]);
    				condition_list[cond_id[1]] = cond;
    			}

    		}

    	});

    	// turn the map into an array for sorting
    	var condition_list_array = [];

    	for ( var cond_id in condition_list ) {
    		condition_list_array.push(condition_list[cond_id]);
    	}

    	this._sortConditionList(condition_list_array);

    	var phase_middle = $('#phaseMiddle');
    	var phase_right = $('#phaseRight');

    	phase_middle.html('');
    	phase_right.html('');

    	for ( var i = 0; i < condition_list_array.length; i++ ) {

    		var cond = condition_list_array[i];

    		var cond_active = (this.presentation.conditions.indexOf(cond.id_int) != -1);

        	$('<div/>')
        	.attr('id','cond' + cond.id_int)
        	.addClass('phaseList plnew on' + (cond_active ? ' active' : ''))
        	.append($('<div/>').addClass('phaseListCheck'))
        	.append($('<a/>')
        			.addClass('phaseListName')
        			.addClass('action_toggle_condition')
        			.attr('data-condition-id',cond.id_int)
        			.attr('href','#none')
        			.html(cond.name))
        	.append($('<div/>').addClass('phaseListRelated'))
    		.appendTo( i < 12 ? phase_middle : phase_right);

        	if ( cond_active ) {
        		// this will be brand new list so only care about active conditions
        		this._showRelatedConditions(cond.id_int,true);
        	}
    	}

    };


    this._showRelatedConditions = function(cond_id_int,cond_active) {

    	if ( PAH.Content.RelatedConditions[cond_id_int] != undefined ) {

			var rel = PAH.Content.RelatedConditions[cond_id_int];

			if ( cond_active ) {

    			for ( var i = 0; i < rel.length; i++ ) {
    				var relelm = $('#VP_6 #cond' + rel[i] + ' .phaseListRelated');
    				relelm.addClass('active');
    				var relcount = (parseInt(relelm.attr('data-related-count')) || 0) + 1;
    				relelm.attr('data-related-count',relcount);
    			}

    		} else {
				for ( var i = 0; i < rel.length; i++ ) {
					var relelm = $('#VP_6 #cond' + rel[i] + ' .phaseListRelated');
					var relcount = parseInt(relelm.attr('data-related-count')) - 1;
					relelm.attr('data-related-count',relcount);
					if ( relcount == 0 )
						relelm.removeClass('active');
				}

    		}
    	}

    };


    this["action_toggle_condition"] = function(e) {

    	var listelm = $(e.currentTarget).parent();
    	listelm.toggleClass('active');

    	var cond_active = listelm.hasClass('active');

    	var cond_id_int = parseInt(e.currentTarget.dataset.conditionId);

    	if ( cond_active ) {
    		this.presentation.addCondition(cond_id_int);
    	} else {
    		this.presentation.removeCondition(cond_id_int);
    	}

    	this._showRelatedConditions(cond_id_int,cond_active);

		if ( $('.phaseList.plnew.on .phaseListRelated.active').size() > 0 ) {
    		$('#related_diseases').removeClass('hide');
    	} else {
    		$('#related_diseases').addClass('hide');
    	}
    };





    this["action_toggle_phase"] = function(e) {

    	var phaseelm = $(e.currentTarget).parent();

    	phaseelm.toggleClass('active');

    	this.presentation.setPhase(parseInt(e.currentTarget.dataset.phaseId), phaseelm.hasClass('active'));

    	this._populatePhaseConditions();

    };


	this["call_VP_6"] = function(e) {

		this._updateUIClientInfo();

//		this.presentation.conditions = [];

		$('#phaseLeft').html('');

		for ( var phase_id in PAH.Content.Matrix ) {
			var phase = PAH.Content.Matrix[phase_id];
			var phase_num = phase_id.substr(0,1);
			$('<div/>')
			.attr('id','phase'+phase_num)
			.addClass('phaseLeftList')
			.append($('<div/>').addClass('phaseLeftListCheck'))
			.append($('<a/>')
					.addClass('phaseLeftListName')
					.addClass('action_toggle_phase')
					.attr('data-phase-id',phase_num)
					.attr('href','#none')
					.text(phase.name))
			.appendTo('#phaseLeft');


//			str += '<div id="ph'+key.substr(0,1)+'" class="phaseLeftList"><div class="phaseLeftListCheck "></div><a href="#VP_6" class="phaseLeftListName">'+ obj.name+'</a></div>';

	//		$("#phaseLeft").prepend(str);

		}

		this._populatePhaseConditions();

	};

	this["check_save_plan"] = function(e,hash,act) {

		if ( this.presentation_slot == undefined ) {
			$('#popup-VPP_confirm a.button.save')
			.attr('href','#VPP_09-save');

			$('#popup-VPP_confirm a.button.dont-save')
			.attr('href',hash);

			this.popup('VPP_confirm',false,e.currentTarget.dataset);
			return false;
		}

	};

	this["check_modify_plan"] = function(e,hash,act) {

		if ( this.presentation.modified ) {
			$('#popup-VPP_confirm_modify a.button.save')
			.attr('href','#VPP_09-save');

			$('#popup-VPP_confirm_modify a.button.dont-save')
			.attr('href',hash);

			this.popup('VPP_confirm_modify',false,e.currentTarget.dataset);
			return false;
		}

	};

	this["action_close_popup"] = function(e) {
		$('#popup-close').click();
	};



	this["action_toggle_plan_list_edit"] = function(e) {
		$('#planList').toggleClass('edit');
	};


	this["action_remove_presentation"] = function(e) {
		var index = e.currentTarget.dataset.presentationId;
		$(e.currentTarget).parents('.columnBoxDiv').remove();
		this.storage.removePresentation(index);
		if ( this.storage.presentations.length == 0 )
			SetNoneSavedPlanList();
		this._updatePlanList();
	};



	this.orderByNameAscending = function(a, b) {
		if (a.name == b.name) {
			return 0;
		} else if (a.name.toLowerCase() > b.name.toLowerCase()) {
			return 1;
		}
		return -1;
	};


	this["action_send_email"] = function(e) {
		presenter.command("emailSend",{
			"toEmail": this.email.to,
			"subject": this.email.subject,
			"bodyHtml": this.email.body,
			"attachments": this.email.attachments
		});
	};



	this._showLightbox = function(show) {
		if ( show )
			$('#popup-lightbox').addClass('on');
		else
			$('#popup-lightbox').removeClass('on');
	};


	this.gotoPage = function(id,param) {
		if(!(typeof(id) == 'string' && id.indexOf('VP_') === 0)) id = 'VP_0';
		$('.container').removeClass('on');
		$('.popup').removeClass('on');
		$('#popup-close').removeClass('on');
		this._showLightbox(false);
		$('#'+id).addClass('on');


		if ( typeof this["call_"+id] == "function" )
			this["call_"+id](param);

	};




	this.popup = function(id,with_lightbox,param) {
		var $popup = $('#popup-'+id);
		$('.popup').removeClass('on');
		$popup.addClass('on');
		$('#popup-close').addClass('on');
		this._showLightbox(with_lightbox);

		if ( typeof this["call_"+id] == "function" )
			this["call_"+id](param);

	};


	this.gotoPage(window.location.hash.substring(1));






	$(document).on('click', '.productListItemSelect', function() {
		_item = $(this).parent();
		(_item.hasClass("selected")) ? _item.removeClass("selected") : _item.addClass("selected");
	});


	this._updatePresentationName = function() {
		this.presentation.name = $('#presentationName').val();
		$('.presentation-name').text(this.presentation.name);
	};

	this._updatePresentationNotes = function() {
		this.presentation.notes = $('#notes-edit').val();
		this.presentation.modified = true;
	};


	$(document).on('keyup', '#customerInput', function() {		// VP_0 TERRITORY ID/ CUSTOMER ENTRY FIELD ACTION
		that.checkTerritoryId();
	}).on('keyup', '#presentationName', function(e) {
		that._updatePresentationName();
		if ( e.keyCode == 13) $('#popup-close').click();
	}).on('change', '#notes-edit', function(e) {
		that._updatePresentationNotes();
	});

	// onhashchange does not work reliably in ios safari, hence this hack
	$(document).on('click','a',function(e) {

		var check = this.className.match('check_[^ ]+');

		var act = this.className.match('action_[^ ]+');

		if ( typeof that[check] == "function" ) {
			var rv = that[check](e,this.hash,act);
			if ( rv === false ) {
				e.preventDefault();
				return false;
			}
		}

		if ( typeof that[act] == "function" )
			that[act](e);

		if ( this.hash != undefined ) {

			var param = {};
			for ( var prop in e.currentTarget.dataset ) {
				if ( typeof prop == 'string' ) {
					param[prop] = e.currentTarget.dataset[prop];
				}
			}

			if ( this.hash.match("^#VP_.+") ) {

				that.gotoPage(this.hash.substring(1),param);

			} else if ( this.hash.match("^#VPP_.+") ) {

				that.popup(this.hash.substring(1),false,param);

			}
		}

	});



	$('#popup-close').on('click',function(){
		$(this).removeClass('on');
		$('.popup').removeClass('on');
		that._showLightbox(false);
	});


	$(".pah-close.close").click(function(e){
		var shiny_object = $("#shiny-object",$("#popframe > iframe")[0].contentDocument);
		if ( shiny_object.length > 0 && shiny_object[0].style.display != 'none' ) {
			// have to repeat close() event from kbp - on ipad it won't fire if called
			shiny_object.hide();
			$("#wrapper",$("#popframe > iframe")[0].contentDocument).show();
		} else {
			$('.pah-close[data-dismiss="modal"]').click();
			that._showLightbox(false);
		}
	});



})();
