$(document).ready(function() {
		window.content_scroll = new iScroll('content',{vScroll:true,vScrollbar:true,hideScrollbar:false,scrollbarClass:'contentScrollbar'});
		//window.allcontent_scroll = new iScroll('allcontent',{vScroll:true,vScrollbar:true,hideScrollbar:false,scrollbarClass:'allcontentScrollbar'});

		$.fn.exists = function() { 
   			return this.length > 0; 
		};
		
		var pageId = PAH.getQSValue("id");
		
		if (pageId == null) {
			pageId = "0.0";
		}
		refreshPage(pageId);

		// NAV BAR CLICK
		$('.navbar-fixed-bottom ul.nav li a').bind('click',function(e){
			refreshPage(this.rel);
		});
		
		function refreshPage(pageId) {
			PAH.Tools.phaseId = pageId;
		
			var num = pageId.charAt(0);
			var navbar = "url(../images/navbar_" + num + ".png)";
		
			var buttonId = "#button_" + num;
			var navbutton = $(buttonId);

			// var page = PAH.Tools.lookupPage(pageId);

			$("#headline").text(navbutton.text());
			$("#navbuttons").css('background-image',navbar);

			if (pageId=="0.0") {
				console.log(pageId);
				$("#allcontent").css("display", "block");
				$("#content").css("display", "none");
				$("#content").css("height", 640);
				$("#left-nav").css("display", "none").removeClass('resizable_spacing');
        		$('#content_wrapper').addClass('content_container');
        		$('#content_wrapper').removeClass('content_container_overlay');
        		$('#resizable').hide();
			} 
			else if(pageId=="1.0" || pageId=="2.0"){
				$('#resizable').show();
				$('#resizable').css("display", "block");
				$("#allcontent").css("display", "none");
				$("#content").css("display", "block");
				$("#content").css("height", 410);
				$("#left-nav").css("display", "block").addClass('resizable_spacing');
				$('#content_wrapper').removeClass('content_container');
				$('#content_wrapper').addClass('content_container_overlay');

				var pageMatrix = PAH.Tools.fetchContentMatrix();
				PAH.Tools.buildPhaseNav(pageMatrix);
				PAH.Tools.showDefaultFilter();
				//var dayone1 = $('#dayone1');
				//var dayone2 = $('#dayone2');

				// $('ul.thumbnails li').on('click',function(){
				// 	if($(this).data('key') == 'S_11_0'){
				// 		$('#dayone1').fadeIn(800, function(){
				// 			$('.closebox').on('click', function(){
				// 				$(this).parent().fadeOut('800');
				// 			});
				// 		});
				// 	}
				// 	else if($(this).data('key') == 'S_12_0'){
				// 		$('#dayone2').fadeIn(800, function(){
				// 			$('.closebox').on('click', function(){
				// 				$(this).parent().fadeOut('800');
				// 			});
				// 		});
				// 	}
				// 	else{}
				// });
			}
			else if (pageId!="1.0" || pageId!="2.0"){
				$('#resizable').hide();
				$("#allcontent").css("display", "none");
				$("#content").css("display", "block");
				$("#content").css("height", 640);
				$("#left-nav").css("display", "block").removeClass('resizable_spacing');;
        		$('#content_wrapper').removeClass('content_container_overlay');
        		$('#content_wrapper').addClass('content_container');
				var pageMatrix = PAH.Tools.fetchContentMatrix();
				PAH.Tools.buildPhaseNav(pageMatrix);
				PAH.Tools.showDefaultFilter();
			}
			else {}
		}
		// ALL PRODUCTS AND SERVICES SETUP
		

		$(".product-cell:not(.no-modal), li.calculator_text").click(function(e){
			$('.orange_bar').fadeOut(200);
			PAH.Tools.loadProductFrame(this);
		});

		$(".pah-close.close").click(function(e){
			var shiny_object = $("#shiny-object",$("#popframe > iframe")[0].contentDocument);
			if ( shiny_object.length == 0 ) {
				shiny_object = $(".shiny-object",$("#popframe > iframe")[0].contentDocument);
			}
			
			if ( shiny_object.length > 0 && shiny_object[0].style.display != 'none' ) {
				// have to repeat close() event from kbp - on ipad it won't fire if called
				shiny_object.hide();
				$("#wrapper",$("#popframe > iframe")[0].contentDocument).show();
			} else {
				$('.pah-close[data-dismiss="modal"]').click();
			}
			$('.orange_bar').fadeIn(200);
		});

		$("#cost-analysis-tool-link").click(function(e){
			e.stopPropagation();
			presenter.document.open('com.pfizer.pork.aif.cost.tool');
		});

		$("#he-calculator-link").click(function(e){
			e.stopPropagation();
			presenter.document.open('com.pfizer.pork.husbandry.education.calc');
		});

		$("#albac-calculator-link").click(function(e){
			e.stopPropagation();
			presenter.document.open('com.pfizer.pork.albac.calculator');
		});

		$("#fostera-prrs-link").click(function(e){
			e.stopPropagation();
			presenter.document.open('com.pfizer.pork.fosterra.prrs.edetail');
		});
		
		$("#draxxin-calculator-link").click(function (e) {
		    e.stopPropagation();
		    presenter.document.open('com.pfizer.pork.draxxin.calculator');
		});
		
		$("#draxxin-calculator25-link").click(function (e) {
		    e.stopPropagation();
		    presenter.document.open('com.pfizer.pork.draxxin25.calculator');
		});
		
	//Elena
	$('.overlay_more').on('click', function( ev ) {
	    ev.stopPropagation();
	    $('.calc_options').fadeToggle();
	});
	$("body").on('click', function(ev) {
	    var myID = ev.target.id;
	    if (myID !== 'calc_options') {
	        $('.calc_options').fadeOut();
	    }
	});
	setTimeout(function(){
		$('.body-phase').css('visibility','visible').css('opacity','1').css('-webkit-transition','opacity 0.2s');
	},300); 

	
	/*---------------------------------------
		Js for the SOW pulldown
	---------------------------------------*/

	//AJAX Call to pull in the SOW content
	var loadUrl = "../phase/sow/sow.html";

	$("#resizable").load( loadUrl, function(status){pulldownContent()});

	function pulldownContent(){

		//Variables
		var container = $("#resizable");
		var initialHeight = 256;
		var fullHeight = 768;
		var tippingPoint = 500;
		var currentHeight;

		var sow_overlay = $(".sow_overlay");
		var sow_landing = $(".sow_landing");
		var sow_container = $(".sow_container");
		var sow_pages = $('.sow_pages');
 		var sow_hotspots = $('.sow_hotspots');
		
		var grippieUp = $("#grippieUp");
		var grippieDown = $("#grippieDown");

		var closeBtn = $('.closeBtn');
		var infoBtn = $('.info-btn');
 		var solutionsbtn = $('.solutions-btn');
 		var back_arrow = $('.sowcare-arrow-back');
 		var back_arrow_overlay = $('.arrow-overlay');
 		var subpage_container = $('#subpage_container');
		var overlay_title_minimized = $('.overlay-title-minimized');
 		var section;


 		function globalPopup(){
 			$('#overlay-solutions-closeBtn').on('click', function(){
 				$(this).parent().parent().fadeOut(500);
				$(back_arrow).fadeIn(100);
				$('.overlay').fadeOut();
				circleReset();
 			});

 			function init(){

 			};

 			function circleClick(){
				
 				function animateTaget(e, leftMove){
 					e.animate({
 						width: 387,
 						height: 387,
 						top: 100,
 						left: leftMove,
 						opacity: 0.9
 					}, 500,false );
 					e.find('div').eq(0).stop().delay(200).fadeIn(600);
 					e.find('img').eq(0).stop().animate({height: 85},500);
 					e.find('img').eq(1).stop().animate({width: 90+'%'},500);
 					e.find('img').eq(2).stop().animate({height: 20},500);
 				}
 				function animateSiblings(p,leftMove){
 					p.find('div').eq(0).fadeOut(100);
 					p.animate({
 						left: leftMove,
 						width: 203,
 						height: 203,
 						top: 182,
 						opacity: 0.9
 					}, 500,false );
 					p.find('img').eq(0).stop().animate({height: 70,'margin-top': 35},500);
 					p.find('img').eq(1).stop().animate({width: 75+'%','margin-top': 10},500);
 					p.find('img').eq(2).stop().animate({height: 17,'margin-top': 10},500);
 				}
 				$('.overlay_solutions_circle').click('on', function(){
 					$('#sow-overlay-callout').fadeOut();
 					$('#sow-overlay-subhead').animate({top: 520},500);
 					if($(this).get(0).id == 'products'){
 						var $this = $(this);
 						animateTaget($this,100);
 						animateSiblings($this.next(),467);
 						animateSiblings($this.next().next(),651);
 						
 						//Close Expandable on click
 						closeDraggable($this.find('.overlay-solutions-link'));
 					}
 					else if ($(this).get(0).id == 'services'){
 						var $this = $(this);
 						animateTaget($this,308);
 						animateSiblings($this.prev(),120);
 						animateSiblings($this.next(),673);
 						
 						//Close Expandable on click
 						closeDraggable($this.find('.overlay-solutions-link'));
 					}
 					else if ($(this).get(0).id == 'expertise'){
 						var $this = $(this);
 						animateTaget($this,502);
 						animateSiblings($this.prev(),317);
 						animateSiblings($this.prev().prev(),136);

 						//Popup for last circle
 						$('.expertise_link').on('click', function() {
 							var overlay_content = $('.sow-overlay-content').find('div').eq(0);
 							overlay_content.fadeOut('500');
 							$('.exp_popup').fadeIn('500', function() {});
 							$('.exp_popup_close').on('click', function() { 
 								$(this).parent().fadeOut('500');
 								overlay_content.fadeIn('500');
 							});
 						});
 					}
 					else{
 					}
 				});

 			};
 			circleClick();

 			function circleReset(){
			$('#products').css("background-size", "100% 100%");
			$('#products').css("width", "273px");
			$('#products').css("height", "273px");
			$('#products').css("top", "100px");
			$('#products').css("left", "115px");
			$('#products').css("z-index", "5");
			$('#products').css("opacity", "0.8");
			$('#overlay-solutions-products-icon').css("margin", "35px 0 0 0");
			$('#overlay-solutions-products-divider').css("width", "100%");
			$('#overlay-solutions-products-divider').css("margin-top", "14px");
			$('#overlay-solutions-products-title').css("margin", "10px 0 0 0");
			$('#overlay-solutions-products-text').css("display", "none");
			
			$('#services').css("background-size", "100% 100%");
			$('#services').css("width", "273px");
			$('#services').css("height", "273px");
			$('#services').css("top", "100px");
			$('#services').css("left", "367px");
			$('#services').css("z-index", "4");
			$('#services').css("opacity", "0.8");
			$('#overlay-solutions-services-icon').css("margin", "35px 0 0 0");
			$('#overlay-solutions-services-divider').css("width", "100%");
			$('#overlay-solutions-services-divider').css("margin-top", "14px");
			$('#overlay-solutions-services-title').css("margin", "10px 0 0 0");
			$('#overlay-solutions-services-text').css("display", "none");
			
			$('#expertise').css("background-size", "100% 100%");
			$('#expertise').css("width", "273px");
			$('#expertise').css("height", "273px");
			$('#expertise').css("top", "100px");
			$('#expertise').css("left", "618px");
			$('#expertise').css("z-index", "3");
			$('#expertise').css("opacity", "0.8");
			$('#overlay-solutions-expertise-icon').css("margin", "35px 0 0 0");
			$('#overlay-solutions-expertise-divider').css("width", "100%");
			$('#overlay-solutions-expertise-divider').css("margin-top", "14px");
			$('#overlay-solutions-expertise-title').css("margin", "10px 0 0 0");
			$('#overlay-solutions-expertise-text').css("display", "none");
			
			$('#sow-overlay-subhead').css("top", "421px");
			
			$('#sow-overlay-callout').fadeIn();
			$('#sow-overlay-callout').css("top", "498px");
			$('#sow-overlay-callout').css("left", "158px");
			
			
			$('.icon').css("height", 85);
 			};
 		};
 		globalPopup();
		
		//Let set the initial height for the pulldown menu
		container.height(initialHeight);

		//Initialize the resizable property
		container.resizable({ 
			handles: {"s":"#grippieDown","n":"#grippieUp"},
	       	resize: resizeDrag,
	       	maxHeight: fullHeight,
	      	minHeight: initialHeight,
	       	start: startDrag,
	       	stop: endDrag
	 	});

		closeDraggable(closeBtn);
	 	closeLanding(sow_landing);
	 	grippieUp.hide();
	 	innerPages();

	 	function resizeDrag(){};

	 	function startDrag(){$('body').addClass('sow_open')};

	 	function endDrag(){
	 		if ($(container).height() > tippingPoint){
	 			$('body').addClass('sow_open');
	 			sow_overlay.animate({ opacity: 0 },800);
				
	 			infoBtn.on('click', function(){
	 				$('.sow-overlay-content').fadeIn(500);
					$('.overlay').fadeOut(500);
	 				$(back_arrow).fadeOut(100);					
	 			});
	 	    	$(container).animate({ height: fullHeight }, 700, function(){
	 	    		grippieUp.fadeIn('fast');
					grippieDown.fadeOut('fast');
	 	    		sow_overlay.hide();
	 	    	});

	 	  	}
	 	  	else{
	 	  		$('body').removeClass('sow_open');
	 			sow_overlay.show('fast',function(){$(this).animate({ opacity: 1 },900)});
	 			sow_landing.show('fast',function(){$(this).animate({ opacity: 1 },900)});
	 			sow_container.hide();
	 			closeBtn.fadeOut(500);
	 			infoBtn.fadeOut(500);
	 			/*grippieUp.fadeOut('fast');*/
	 	    	grippieDown.fadeIn('fast');
	 			$(container).animate({ height: initialHeight }, 800, function(){});
	 	  	}
	 	}

	 	function collapseDraggable(){

	 	}

	 	function closeDraggable(e){
	 		e.on('click',function(){
	 			$('body').removeClass('sow_open');
	 			sow_overlay.show('fast',function(){$(this).animate({ opacity: 1 },900)});
	 			sow_landing.show('fast',function(){$(this).animate({ opacity: 1 },900)});
	 			sow_container.hide();
	 			closeBtn.fadeOut(500);
	 			infoBtn.fadeOut(500);
	 			grippieUp.fadeOut('fast');
	 	    	grippieDown.fadeIn('fast');
	 			$(container).animate({ height: initialHeight }, 800, function(){});
	 		});
	 	};

	 	function closeLanding(e){
	 		e.on('click', function(event) {
	 			event.preventDefault();
	 			$(this).animate({ opacity: 0 }, 700, function(){
	 				$(this).hide()
	 			});
	 			infoBtn.show().animate({ opacity: 1 },700);
	 			sow_container.show();
	 		});
	 	};

	 	//Code for the inner pages 
	 	function innerPages(){
 			$('.hotspot').on('click', function(){
 				section = $(this).data('type');
 				var url = "../phase/sow/" + section + ".html";
 				sow_pages.show().load( url, function() {subContent()});
 			});
 			function subContent(){
 				loadContent();
 				buttonActions();
 				refreshContent();
 			};
 			function loadContent(){
 				pullOverlay();
 				sow_hotspots.hide();
 				$('#subpage_container').hide().fadeIn(700);
 			};
 			function pullOverlay(){
 				 $('.crosslink_trigger_btn').bind('click',function(e){
 				 	$tile = $(this);
 				 	e.preventDefault();

 				 	var obj = PAH.Tools.findByFileKey($tile[0].dataset.key);
 					
 				 	// load frame only if key benefits page is defined in the database
 					//if (PAH.Content.PageDB[obj.id.replace(".0",".1")]) {
 				 		PAH.Tools.loadProductFrame($tile[0]);
 				 	//}
 				});
 			};
 			function buttonActions(){
 				$('.solutions-btn').unbind().on('click', function(){
 					back_arrow.hide();
 					if(section.length){$('.' + section + '-content').find('.overlay').fadeIn();}
 					else{}
 				});
 				$('.arrow-overlay').on('click', function(){
 					back_arrow.show();
 					$('.overlay').fadeOut();
 				});
 				$('.infopanel_content').unbind().on('click', function(){
 					$(this).toggleClass('active_click');
 					$(this).find('.question_mark').toggleClass('active_click');
 				});
 			};
 			function refreshContent(){
 				back_arrow.fadeIn(500);
 				back_arrow.on('click', function(){
 					back_arrow.fadeOut(500);
 					sow_pages.fadeOut(500, function(){
 						$(this).empty();
 						sow_hotspots.show();
 					});
 				});
 			};
	 	};
	}
});