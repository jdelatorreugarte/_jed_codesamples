// JavaScript Document
$(document).ready(function(){
	var clickCount = 0;
	$(".hot").click(function(){
		butID = $(this).attr('btnID');
		sryingeID = $(this).attr('sry');
		aniID = $(this).attr('ani');
		if(clickCount == 0){
			$(butID).fadeIn();
			$(sryingeID).fadeOut();
			clickCount++;
			} else if(clickCount == 1 && $(butID).is(':visible')) {
				$(butID).fadeOut();
				$(aniID).fadeIn();
				$('#lightboxMask').fadeIn();
				clickCount++;
				$('.hot').addClass('below');
				if (aniID == '#ani1'){
					ani1()
				} else if(aniID == '#ani2'){
					ani2();
				} else if(aniID == '#ani3'){
					ani3();
				};
			} else if(clickCount == 1 && $(butID).is(':hidden')){
					$(butID).fadeIn().siblings().fadeOut();
					$(sryingeID).fadeOut().siblings().fadeIn();
				} else if(clickCount == 2 && $(aniID).is(':hidden')){
					resetAll();
				}
		});
	
	$('#hotspot4').click(function(){
			$('#reference, #lightboxMask').fadeIn();
		});
		
	$('#lightboxMask').click(function(){
			resetAll();
		});
	$('#dropdown').click(function(){
		$('#lightboxMask, #dropText').fadeIn();
		$('#dropArrow').addClass('arrowAni');
		$('#dropArrow, #dropdown').addClass('above');
		});
		
	function resetAll(){
			$('#shiny-object').find('*').stop();
			$('#lightboxMask, .viz').fadeOut(400, init());
			$('#dropArrow').removeClass('arrowAni');
			$('#dropArrow, #dropdown').removeClass('above');
			$('.sry').fadeIn();
			clickCount = 0;
			$('.hot').removeClass('below');
			/*init();*/
		};
		
	function ani1(){
			$('#ani1 .b1').animate({
					top:39,
					opacity:1
				}, 1000);
			$('.txt').delay(300).animate({opacity:1});
			$('.arrow1').delay(600).animate({
					left:540,
					opacity:1
				}, 1000);
			$('.arrow2').delay(600).animate({
					left:325,
					opacity:1
				}, 1000);
		};
		
	function ani2(){
			$('#ani2 .b1').animate({
					top:39,
					opacity:1
				});
			$('#ani2 .b2').delay(400).animate({
					top:36,
					opacity:1
				});
			$('#ani2 .b3').delay(800).animate({
					top:36,
					opacity:1
				});
			$('#ani2 .b4').delay(1200).animate({
					top:36,
					opacity:1
				});
			$('#ani2 .txt').delay(1000).animate({
					opacity:1
				});
			
		};
		
	function ani3(){
			$('#ani3 .b1').animate({
					top:39,
					opacity:1
				});
			$('#ani3 .b2').delay(200).animate({
					top:36,
					opacity:1
				});
			$('#ani3 .b3').delay(400).animate({
					top:36,
					opacity:1
				});
			$('#ani3 .b4').delay(600).animate({
					top:36,
					opacity:1
				});
			$('#ani3 .b5').delay(800).animate({
					top:36,
					opacity:1
				});
			$('#ani3 .b6').delay(1000).animate({
					top:41,
					opacity:1
				});
			$('#aniContainer').delay(1400).animate({
					top:41
				}, 1000);
			$('#h1').delay(1800).fadeIn();
			$('#h2').delay(3000).fadeIn();
			$('#h3').delay(5000).fadeIn();
			$('#h4').delay(7000).fadeIn();
			$('#h1').delay(9000).fadeOut();
			$('#h2').delay(11000).fadeOut();
			$('#h3').delay(13000).fadeOut();
			
		};
	
	function init(){
			$('.block').css('top', 139);
			$('.txt').css('opacity', 0);
			$('.arrow1').css({'opacity' : 0, 'left' : 445});
			$('.arrow2').css({'opacity' : 0, 'left' : 425});
			$('#aniContainer').css('top', 15);
			$('#h1, #h2, #h3, #h4').css('display', 'none');
		};
	
	
});