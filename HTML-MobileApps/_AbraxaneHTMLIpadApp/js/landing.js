$(document).ready(function() {
	
	
	$("#select_patient").click(function(){
		window.location = 'index.html';
	});

	$("#landingPat1, #patient1_close, #patient1_close2").click(function(event){
		window.location = 'patient1_dashboard.html';
	});
	$("#patient1_imaging_btn, #patient1_imaging_slide1").click(function(){
		window.location = 'patient1_imaging1.html';
	});
	$("#patient1_biopsy_btn").click(function(){
		window.location = 'patient1_biopsy.html';
	});
	$("#patient1_imaging_slide2").click(function(){
		window.location = 'patient1_imaging2.html';
	});
	
	
	$("#landingPat2, #patient2_close, #patient2_close2").click(function(event){
		window.location = 'patient2_dashboard.html';
	});
	$("#patient2_imaging_btn, #patient2_imaging_slide1").click(function(){
		window.location = 'patient2_imaging1.html';
	});
	$("#patient2_biopsy_btn").click(function(){
		window.location = 'patient2_biopsy.html';
	});
	$("#patient2_imaging_slide2").click(function(){
		window.location = 'patient2_imaging2.html';
	});
	

	$("#landingPat3, #patient3_close, #patient3_close2").click(function(event){
		window.location = 'patient3_dashboard.html';
	});
	$("#patient3_imaging_btn, #patient3_imaging_slide1").click(function(){
		window.location = 'patient3_imaging1.html';
	});
	$("#patient3_biopsy_btn").click(function(){
		window.location = 'patient3_biopsy.html';
	});
	$("#patient3_imaging_slide2").click(function(){
		window.location = 'patient3_imaging2.html';
	});
	
// continue button
	
	$('.continueBtn').click(function(){
		$('.isiContainer').show();
		var isiContainerElement, isiScroller;
		isiContainerElement = document.getElementById('ISI');
		isiScroller = new FTScroller (isiContainerElement, {
			scrollbars: true,
			scrollingX: false,
			bouncing:true
		});

// pi button
		$('.piBtn').click(function(){
			$('.isiContainer').hide();
			$('.piContainer').show();
			var piContainerElement, piScroller;
			piContainerElement = document.getElementById('PI');
			piScroller = new FTScroller (piContainerElement, {
				scrollbars: true,
				scrollingX: false,
				bouncing:true
			});
		});
// pi button

		$('.closeBtn').click(function()
		{
			$('.isiContainer, .piContainer').hide();
		});
	});
			
});

