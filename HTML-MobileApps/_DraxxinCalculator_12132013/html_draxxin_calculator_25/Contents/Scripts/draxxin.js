// Page Swipe
    $(document).on('swipeleft swiperight', function (event) {
        if (event.type == 'swipeleft') {
            var nextPage = $.mobile.activePage.next('[data-role=page]');
            if (nextPage) {
                $.mobile.changePage(nextPage, {
                    transition: "slide"
                });
            }
        }
        if (event.type == 'swiperight') {
            var prevPage = $.mobile.activePage.prev('[data-role=page]');
            if (prevPage) {
                $.mobile.changePage(prevPage, {
                    transition: "slide",
                    reverse: true
                });
            }
        }
    });

// Output Config
for (var element in presenter.config)
    if (!(presenter.config[element] instanceof Function) && element.substring(0, 1) != "_") {
        $("#config").append("<dt>" + element + "</dt><dd>" + presenter.config[element] + "</dd>");
    }

function status(success, error, data) {
    var dataList = "";
    for (var item in data)
        dataList += "<dt>" + item + "</dt><dd>" + data[item] + "</dd>";
    $("#status").prepend("<dl><dt>success</dt><dd>" + success + "</dd><dt>error</dt><dd>" + error + "</dd><dt>data</dt><dd><dl>" + dataList + "</dl></dd></dl>");
}

document.counter = 0;

// document ready for jquery
$(document).ready(function () {
    // declaration
    var _resetvalue;
    var _y = 0;

    // initial load
    initialize();

    // "more" overlay
    $('.overlay_more').on('click', function () {
        $(this).children('.overlay_info').fadeToggle();
        $('.info-panel-modal').show();
    });

	$('#calc-overlay-one.overlay_more').on('click', function () {
    	$('#calc-overlay-two .overlay_info').fadeOut();
    });
    
	$('#calc-overlay-two.overlay_more').on('click', function () {
   		 $('#calc-overlay-one .overlay_info').fadeOut();
    });

    // pi email
    $('#pi_email').on('click', function (e) {
        $('#footer li').removeClass('active').find('div.doc-type').fadeOut();

        presenter.command("emailSend", {
            "subject": "DRAXXIN 25 (tulathroycin) Injectable Solution Product Insert",
            "body": "Please find attached the Product Insert for DRAXXIN 25",
            "bodyHtml": "Please find attached the Product Insert for DRAXXIN 25",
            "attachments": ["/Contents/PDF/DRAXXIN25-Product-Insert.pdf"]
        }, status);

        return false;
    });

    // pi pdf
    $('#pi_view').on('click', function (e) {
        $('#footer li').removeClass('active').find('div.doc-type').fadeOut();
        
        presenter.command("viewPdf", {
            "path": "/Contents/PDF/DRAXXIN25-Product-Insert.pdf"
        }, status);

        return false;
    });

    // bulletin email
    $('#bulletin_email').on('click', function (e) {
        $('#footer li').removeClass('active').find('div.doc-type').fadeOut();

        presenter.command("emailSend", {
            "subject": "Results of DRAXXIN (tulathromycin) Injectable Solution Study",
            "body": "Please find attached the Technical Update Effects of DRAXXIN at weaning for control of swine repiratory disease in pigs experiencing a natural outbreak that provided the data for the DRAXXIN Benefits Calculator.",
            "bodyHtml": "Please find attached the Technical Update <i>Effects of DRAXXIN at weaning for control of swine repiratory disease in pigs experiencing a natural outbreak</i> that provided the data for the DRAXXIN Benefits Calculator.",
            "attachments": ["/Contents/PDF/Draxxin-Bulletin-SalineTB.pdf"]
        }, status);

        return false;
    });

    // bulletin pdf
    $('#bulletin_view').on('click', function (e) {
        $('#footer li').removeClass('active').find('div.doc-type').fadeOut();

        presenter.command("viewPdf", {
            "path": "/Contents/PDF/Draxxin-Bulletin-SalineTB.pdf"
        }, status);

        return false;
    });


    // footer link popups
    $('#footer li.pi').on('click', function (e) {
        $('#footer li.pdf').removeClass('active').find('div.doc-type').fadeOut();

        e.preventDefault();
        var $this = $(this);
            
        if (!$this.hasClass('active')) {
            //$('.info-panel-modal').show();
            $this.addClass('active');
            $this.find('div.doc-type').fadeIn();
        }
        else {
            $this.removeClass('active');
            $this.find('div.doc-type').fadeOut();
        }
    });

    $('#footer li.pdf').on('click', function (e) {
        $('#footer li.pi').removeClass('active').find('div.doc-type').fadeOut();

        e.preventDefault();
        var $this = $(this);
            
        if (!$this.hasClass('active')) {
            //$('.info-panel-modal').show();
            $this.addClass('active');
            $this.find('div.doc-type').fadeIn();
        }
        else {
            $this.removeClass('active');
            $this.find('div.doc-type').fadeOut();
        }
    });

    

    // per group/pig tabs
    $('#total-return-tabs li').on('click',function(e) {
        e.preventDefault();
        var $this = $(this),
            $tabId = $this.attr('data-tab');
            
        if (!$this.hasClass('active')) {
            $this.addClass('active').siblings().removeClass('active');
            $('span.hideable').hide();
            $('span.' + $tabId).show();
        }
        else {
            return false;
        }
    });
    
    // question list - use 'active' state as a bookmark for keyboard
    $('#calculator-main-left ol li').on('click', function (e) {
        _resetvalue = $(this).children('.value-box').text();
        if (validate()) {
            calculate();

            e.preventDefault();
            var $this = $(this);
            if (!$this.hasClass('active')) {
                $this.addClass('edited'); // historically update colors on updated items - not removed
                $this.addClass('active').siblings().removeClass('active');

                // retain reset value of the active state
                _resetvalue = $(this).children('.value-box').text();
            }
            else {
                return false;
            }
        }
    });

    
    // info overlay
    $('.total-return-info').on('click', function (e) {
        e.preventDefault();
        var $this = $(this),
            $infoPanel = $this.parent().find('.info-panel');
        if (!$this.hasClass('active')) {
            $('.info-panel-modal').show();
            $('.total-return-info').removeClass('active');
            $this.addClass('active');
            $('.info-panel').hide();
            $infoPanel.show();
        }
        else {
            $this.removeClass('active');
            $infoPanel.hide();
        }
    });

    // click-outside overlay to close
    $('.info-panel-modal').on('click', function (e){
        e.preventDefault()
        $('.total-return-info').removeClass('active');
        $('.info-panel-modal, .info-panel').hide();
        $('.overlay_info').hide();

        $('#footer li.pi').removeClass('active').find('div.doc-type').fadeOut();
        $('#footer li.pdf').removeClass('active').find('div.doc-type').fadeOut();
    })

    // keypad enabled - touch start
    $('#calculator-pad .key').on('touchstart', function (e) {
        $(this).addClass('hover');
    });

    // keypad enabled - touch start / calculate
    $('#calculator-pad .key').on('touchend', function (e) {
        $(this).removeClass('hover');

        // declare selector
        var _selected = $('#customerInformation .active').children('.value-box');
        var activeQuestion = $('li.entry.active');

        //// if 0 - clear first
        //if (_selected.text() == '0') _selected.text('');

        // key behavior
        switch ($(this).attr('id')) {

            // process reset to original value - for specific item
            case ('reset'):
                _resetvalue = _selected.attr('data-value');
                _selected.text(_selected.attr('data-value'));
                break;
            // process delete
            case ('delete'):
                var _value = _selected.text();

                // apply 0 if less than 1 digit
                if (_value.length > 1) {
                    _selected.text(_value.substr(0, _value.length - 1));
                } else {
                    _selected.text('0');
                }
                
                break;
            // process enter - calculate disable active state
            case ('enter'):
                // perform if validate is true
                if (validate()){
                    // calculate
                    calculate();

                    // moves down
                    if (!activeQuestion.hasClass('end')) {
                        activeQuestion.removeClass('active');
                        if (activeQuestion.next().hasClass('entry')) {
                            activeQuestion.next().addClass('active');
                        }
                        else {
                            activeQuestion.next().next().addClass('active');
                            _y = _y - 39; // extra spacing
                        }
                    }

                    // scroll down
                    if (_y > -625) {

                        _y = _y - 71;
                    } // new position - stop scroll after 1000
                    //scrollContent.scrollTo(0, _y, 1000)
                }
                break;
            // process +/-
            case ('plus'):
                _selected.text(_selected.text() * -1);
                break;

            //// process up
            //case ('up'):
            //    if (!activeQuestion.hasClass('start')) {
            //        activeQuestion.removeClass('active');
            //        if (activeQuestion.prev().hasClass('entry')) {
            //            activeQuestion.prev().addClass('active');
            //        }
            //        else {
            //            activeQuestion.prev().prev().addClass('active');
            //        }
            //    }
            //    break;
            //    // process down
            //case ('down'):
            //    if (!activeQuestion.hasClass('end')) {
            //        activeQuestion.removeClass('active');
            //        if (activeQuestion.next().hasClass('entry')) {
            //            activeQuestion.next().addClass('active');
            //        }
            //        else {
            //            activeQuestion.next().next().addClass('active');
            //        }
            //    }
            //    break;
            // process numeric
            default:
                // max is 8 digit
                if (_selected.text().length < 8) {
                    // if first time entering, clear
                    if (_resetvalue == _selected.text()) {
                        // exception to decimal
                        if ($(this).text() == '.') {
                            _selected.text('0');
                        } else {
                            _selected.text('');
                        }
                    }

                    // append
                    _selected.append($(this).text());

                    // correct format
                    //if (_selected.text().substr(_selected.text().length - 1, _selected.text().length) != '.') { _selected.text(parseFloat(_selected.text())); }
                    if (_selected.text().indexOf('.') < 0) { _selected.text(parseFloat(_selected.text())); }
                }
        }
    });
});

function showError(id) {
    $(id).addClass('error');
    $(id).parent().children('.error-message').show();
}

function showErrorTop(id) {
    $(id).addClass('error');
    $(id).parent().parent().children('.error-message-top').show();
}

// validation logic
function validate() {
    // declaration
    var val = true;

    // reset
    $('.error-message').hide();
    $('.error-message-top').hide();
    $('.value-box').removeClass('error');

    // validate
    NumPigsInGroup = $('#NumPigsInGroup');
    if (NumPigsInGroup.text() == 0 || NumPigsInGroup.text() > 10000) { showErrorTop(NumPigsInGroup); val = false; }

    WeanToFinishFE = $('#WeanToFinishFE');
    if (WeanToFinishFE.text() < 2 || WeanToFinishFE.text() > 3) { showError(WeanToFinishFE); val = false; }

    AvgWeightWeanedPig = $('#AvgWeightWeanedPig');
    if (AvgWeightWeanedPig.text() < 5 || AvgWeightWeanedPig.text() > 25) { showError(AvgWeightWeanedPig); val = false; }

    CostOfFeedPerPound = $('#CostOfFeedPerPound');
    if (CostOfFeedPerPound.text() < 0.01 || CostOfFeedPerPound.text() > 1) { showError(CostOfFeedPerPound); val = false; }

    AvgCostPerPoundOfFeed = $('#AvgCostPerPoundOfFeed');
    if (AvgCostPerPoundOfFeed.text() < 0.05 || AvgCostPerPoundOfFeed.text() > 0.30) { showError(AvgCostPerPoundOfFeed); val = false; }

    ValueOfWeanedPig = $('#ValueOfWeanedPig');
    if (ValueOfWeanedPig.text() < 1 || ValueOfWeanedPig.text() > 150) { showError(ValueOfWeanedPig); val = false; }

    ValuePerPoundOfLiveWeight = $('#ValuePerPoundOfLiveWeight');
    if (ValuePerPoundOfLiveWeight.text() < -3 || ValuePerPoundOfLiveWeight.text() > 3) { showError(ValuePerPoundOfLiveWeight); val = false; }

    AvgCostPerNurseryForSRD = $('#AvgCostPerNurseryForSRD');
    if (AvgCostPerNurseryForSRD.text() < 0 || AvgCostPerNurseryForSRD.text() > 10) { showError(AvgCostPerNurseryForSRD); val = false; }

    CurrentMortalityRate = $('#CurrentMortalityRate');
    if (CurrentMortalityRate.text() < 0 || CurrentMortalityRate.text() > 40) { showError(CurrentMortalityRate); val = false; }

    DrugsLostInDeadPig = $('#DrugsLostInDeadPig');
    if (DrugsLostInDeadPig.text() < 0 || DrugsLostInDeadPig.text() > 20) { showError(DrugsLostInDeadPig); val = false; }

    PoundsFeedLostInDeadPig = $('#PoundsFeedLostInDeadPig');
    if (PoundsFeedLostInDeadPig.text() < 0 || PoundsFeedLostInDeadPig.text() > 60) { showError(PoundsFeedLostInDeadPig); val = false; }

    DeadPigDisposalCost = $('#DeadPigDisposalCost');
    if (DeadPigDisposalCost.text() < 0 || DeadPigDisposalCost.text() > 10) { showError(DeadPigDisposalCost); val = false; }

    CostPerDraxxinBottle = $('#CostPerDraxxinBottle');
    if (CostPerDraxxinBottle.text() < 187.50 || CostPerDraxxinBottle.text() > 287.50) { showError(CostPerDraxxinBottle); val = false; }

    DraxxinMortalityRate = $('#DraxxinMortalityRate');
    if (DraxxinMortalityRate.text() < 0 || DraxxinMortalityRate.text() > 40) { showError(DraxxinMortalityRate); val = false; }

    DraxxinImpactOnFinalWeight = $('#DraxxinImpactOnFinalWeight');
    if (DraxxinImpactOnFinalWeight.text() < 0 || DraxxinImpactOnFinalWeight.text() > 10) { showError(DraxxinImpactOnFinalWeight); val = false; }

    SRDTreatmentReduction = $('#SRDTreatmentReduction');
    if (SRDTreatmentReduction.text() < 1 || SRDTreatmentReduction.text() > 10000) { showError(SRDTreatmentReduction); val = false; }

    // return
    return val;
}

// initialize
function initialize() {
    // iterate each question
    $("#customerInformation li .value-box").each(function () {
        $(this).text($(this).attr('data-value'));
    });

    // calculate
    calculate();
}

// calculation logic
function calculate() {
    // declaration
    NumPigsInGroup = parseFloat($('#NumPigsInGroup').text());
    DraxxinMortalityRate = parseFloat($('#DraxxinMortalityRate').text());
    DraxxinImpactOnFinalWeight = parseFloat($('#DraxxinImpactOnFinalWeight').text());
    AvgMarketHogSaleWeight = parseFloat($('#AvgMarketHogSaleWeight').text());
    AvgWeightWeanedPig = parseFloat($('#AvgWeightWeanedPig').text());
    CurrentMortalityRate = parseFloat($('#CurrentMortalityRate').text() / 100); // convert % to numbers
    DraxxinMortalityRate = parseFloat($('#DraxxinMortalityRate').text() / 100); // convert % to numbers
    NumPigsInGroup = parseFloat($('#NumPigsInGroup').text());
    WeanToFinishFE = parseFloat($('#WeanToFinishFE').text());
    AvgCostPerPoundOfFeed = parseFloat($('#AvgCostPerPoundOfFeed').text());
    ValuePerPoundOfLiveWeight = parseFloat($('#ValuePerPoundOfLiveWeight').text());
    SRDTreatmentReduction = parseFloat($('#SRDTreatmentReduction').text());
    AvgCostPerNurseryForSRD = parseFloat($('#AvgCostPerNurseryForSRD').text());
    ValueOfWeanedPig = parseFloat($('#ValueOfWeanedPig').text());
    PoundsFeedLostInDeadPig = parseFloat($('#PoundsFeedLostInDeadPig').text());
    CostOfFeedPerPound = parseFloat($('#CostOfFeedPerPound').text());
    DrugsLostInDeadPig = parseFloat($('#DrugsLostInDeadPig').text());
    DeadPigDisposalCost = parseFloat($('#DeadPigDisposalCost').text());
    CostPerDraxxinBottle = parseFloat($('#CostPerDraxxinBottle').text());

    // calculate - relative results (direct relationship: must be processed in order)
    _WeightGainPerGroup = parseFloat((NumPigsInGroup * (1 - DraxxinMortalityRate)) * DraxxinImpactOnFinalWeight) + ((AvgMarketHogSaleWeight - AvgWeightWeanedPig) * (CurrentMortalityRate - DraxxinMortalityRate) * NumPigsInGroup);
    $('#_WeightGainPerGroup').text(properAmountFormat(_WeightGainPerGroup));

    _CostForWeightGainPerGroup = parseFloat(_WeightGainPerGroup * WeanToFinishFE * AvgCostPerPoundOfFeed);
    $('#_CostForWeightGainPerGroup').text(properAmountFormat(_CostForWeightGainPerGroup));

    _RevenueGainPerGroup = parseFloat(_WeightGainPerGroup * ValuePerPoundOfLiveWeight);
    $('#_RevenueGainPerGroup').text(properAmountFormat(_RevenueGainPerGroup));

    _AmountSavedFromLessSRDTreatments = parseFloat(SRDTreatmentReduction * AvgCostPerNurseryForSRD);
    $('#_AmountSavedFromLessSRDTreatments').text(properAmountFormat(_AmountSavedFromLessSRDTreatments));

    _CostPerPigDeath = parseFloat(ValueOfWeanedPig + (PoundsFeedLostInDeadPig * CostOfFeedPerPound) + DrugsLostInDeadPig + DeadPigDisposalCost);
    $('#_CostPerPigDeath').text(properAmountFormat(_CostPerPigDeath));

    _TotalMortalityCostBeforeDraxxin = parseFloat(_CostPerPigDeath * CurrentMortalityRate * NumPigsInGroup);
    $('#_TotalMortalityCostBeforeDraxxin').text(properAmountFormat(_TotalMortalityCostBeforeDraxxin));

    _TotalMortalityCostAfterDraxxin = parseFloat(_CostPerPigDeath * DraxxinMortalityRate * NumPigsInGroup);
    $('#_TotalMortalityCostAfterDraxxin').text(properAmountFormat(_TotalMortalityCostAfterDraxxin));

    _AmountSavedFromLessMortality = parseFloat(_TotalMortalityCostBeforeDraxxin - _TotalMortalityCostAfterDraxxin);
    $('#_AmountSavedFromLessMortality').text(properAmountFormat(_AmountSavedFromLessMortality));

    _AmountSavedFromMortalityReduction = parseFloat(_AmountSavedFromLessMortality + _AmountSavedFromLessSRDTreatments);
    $('#_AmountSavedFromMortalityReduction').text(properAmountFormat(_AmountSavedFromMortalityReduction));

    _AmountSavedFromMortalityReductionPig = parseFloat(_AmountSavedFromMortalityReduction / NumPigsInGroup);
    $('#_AmountSavedFromMortalityReductionPig').text(properAmountFormat(_AmountSavedFromMortalityReductionPig));

    _DraxxinInvestmentPerPig = parseFloat((AvgWeightWeanedPig / 22) * (CostPerDraxxinBottle / 250));
    $('#_DraxxinInvestmentPerPig').text(properAmountFormat(_DraxxinInvestmentPerPig));

    _CostPerPigDeath = parseFloat(ValueOfWeanedPig + (PoundsFeedLostInDeadPig * CostOfFeedPerPound) + DrugsLostInDeadPig + DeadPigDisposalCost);
    $('#_CostPerPigDeath').text(properAmountFormat(_CostPerPigDeath));

    _AmountSavedFromLessMortalityPig = parseFloat(_AmountSavedFromLessMortality / NumPigsInGroup);
    $('#_AmountSavedFromLessMortalityPig').text(properAmountFormat(_AmountSavedFromLessMortalityPig));

    _AmountSavedFromLessSRDTreatmentsPig = parseFloat(_AmountSavedFromLessSRDTreatments / NumPigsInGroup);
    $('#_AmountSavedFromLessSRDTreatmentsPig').text(properAmountFormat(_AmountSavedFromLessSRDTreatmentsPig));

    _TotalMortalityCostAfterDraxxinPig = parseFloat(_TotalMortalityCostAfterDraxxin / NumPigsInGroup);
    $('#_TotalMortalityCostAfterDraxxinPig').text(properAmountFormat(_TotalMortalityCostAfterDraxxinPig));

    _WeightGainPerGroupPig = parseFloat(_WeightGainPerGroup / NumPigsInGroup);
    $('#_WeightGainPerGroupPig').text(properAmountFormat(_WeightGainPerGroupPig));

    _CostForWeightGainPerGroupPig = parseFloat(_CostForWeightGainPerGroup / NumPigsInGroup);
    $('#_CostForWeightGainPerGroupPig').text(properAmountFormat(_CostForWeightGainPerGroupPig));

    _RevenueGainPerGroupPig = parseFloat(_RevenueGainPerGroup / NumPigsInGroup);
    $('#_RevenueGainPerGroupPig').text(properAmountFormat(_RevenueGainPerGroupPig));

    _DraxxinInvestmentPerNursery = parseFloat(_DraxxinInvestmentPerPig * NumPigsInGroup);
    $('#_DraxxinInvestmentPerNursery').text(properAmountFormat(_DraxxinInvestmentPerNursery));

    _ValueGainForWeightGainPerGroup = parseFloat(_RevenueGainPerGroup - _CostForWeightGainPerGroup);
    $('#_ValueGainForWeightGainPerGroup').text(properAmountFormat(_ValueGainForWeightGainPerGroup));

    _ValueGainForWeightGainPerPig = parseFloat(_ValueGainForWeightGainPerGroup / NumPigsInGroup);
    $('#_ValueGainForWeightGainPerPig').text(properAmountFormat(_ValueGainForWeightGainPerPig));

    _TotalIncreasedValuePerGroup = parseFloat(_ValueGainForWeightGainPerGroup + _AmountSavedFromMortalityReduction);
    $('#_TotalIncreasedValuePerGroup').text(properAmountFormat(_TotalIncreasedValuePerGroup));

    _TotalIncreasedValuePerPig = parseFloat(_TotalIncreasedValuePerGroup / NumPigsInGroup);
    $('#_TotalIncreasedValuePerPig').text(properAmountFormat(_TotalIncreasedValuePerPig));

    // roi - single digit after decimal
    $('#roi_total').text((_TotalIncreasedValuePerGroup / _DraxxinInvestmentPerNursery).toFixed(1));
}

// properly format to currency
function properAmountFormat(amount) {
    // round off to 2 digit after decimal
    _val = amount.toFixed(2);

    // correctly fomat with commas
    while (/(\d+)(\d{3})/.test(_val.toString())) {
        _val = _val.toString().replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
    }

    // return val
    return _val;



}

// flickering neon
$( document ).ready(function() {
    $('#on1').hide().delay(2000).fadeIn(500).fadeOut(100).fadeIn(200).fadeOut(100).fadeIn(200).fadeOut(600).fadeIn(200);
    // $('#on2').hide().delay(200).fadeIn(100).fadeOut(100).fadeIn(150);
});