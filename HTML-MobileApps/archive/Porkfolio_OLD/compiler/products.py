import jsonpickle
import re

#from openpyxl import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet import Worksheet
from openpyxl.namedrange import NamedRange
from openpyxl.reader.worksheet import read_worksheet, read_dimension
from openpyxl.reader.excel import load_workbook
from openpyxl.shared.exc import InvalidFileException


from compiler_global import trace


class ProductData:
    id = ""
    title = ""
    logo_file = ""
    link = ""
    feature1 = ""
    link1 = ""
    feature2 = ""
    link2 = ""
    feature3 = ""
    link3 = ""


def loadProducts(wb):
    pageRows = []
    # Encoding options:  ISO-8859-1, UTF-8, UTF-16
    try:
        
        ws = wb.get_sheet_by_name('ProductInfo')
        
        lastrow = ws.get_highest_row()
        
        dbRange = "A2:L{0}".format(lastrow)
        
        print dbRange
        
        output = "{}\t{}"
        
        for row in ws.iter_rows(dbRange):
            values = [x.internal_value for x in row]

            prod = ProductData()

            prod.id = values[0]
            prod.title = values[1]
            prod.logo_file = values[2]
            prod.link = values[3]
            prod.feature1 = values[4]
            prod.link1 = values[5]
            prod.feature2 = values[6]
            prod.link2 = values[7]
            prod.feature3 = values[8]
            prod.link3 = values[9]
            prod.tagline = values[10]

            pageRows.append(prod)
            
        return pageRows
    
    except UnicodeEncodeError:
        traceback.print_exc()
    
    except ValueError:
        traceback.print_exc()



def update_products(wb,root):
    print "update products", root

    output_file_name = root + '/lib/data/pah-products.js'

    product_db = {}

    pageRows = loadProducts(wb)
    
    for pageRow in pageRows:
        product_db[pageRow.id] = pageRow

    jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=4)
    json = jsonpickle.encode(product_db, unpicklable=False)
    output = "PAH.Content.Products = {};".format(json)

    outf = open(output_file_name,'w')
    outf.write(output)
    outf.close()
