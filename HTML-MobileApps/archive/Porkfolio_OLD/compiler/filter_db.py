#!/usr/bin/python
# _*_ coding: UTF-8 _*_

'''
Created on Jun 11, 2012

@author: hlang

Dependencies: 
openpyxl -- http://packages.python.org/openpyxl/
jsonpickle -- http://jsonpickle.github.com/
simplejson -- http://pypi.python.org/pypi/simplejson/

'''
import logging
import traceback
import jsonpickle
import re

#from openpyxl import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet import Worksheet
from openpyxl.namedrange import NamedRange
from openpyxl.reader.worksheet import read_worksheet, read_dimension
from openpyxl.reader.excel import load_workbook
from openpyxl.shared.exc import InvalidFileException

from compiler_global import trace


class DataRow:
    phaseId = ""
    phase = ""
    conditionId = ""
    condition = ""
    productId = ""
    product = ""
    ptype = ""


class Phase:
    id = ""
    name = ""
    itemcount = 0
    conditions = []
    
class Condition:
    id = ""
    name = ""
    products = []
    services = []
    
class Product:
    id = ""
    name = ""
    type = ""
    
class Service:
    index = 0
    name = ""
                
class PageRange:
    email = ""
    max = 0
    min = 0


keyformat = "{}-{}"


'''
debug = True
filepath = "../docs/PAH_pages_08.xlsx"
filenameFormat = "{}_{}.html"

resultsArray = []
lastpage = ""


class PageData:
    id = ""
    name = ""
    title = ""
    type = ""
    section = ""
    filename = ""
    filekey = ""
    template = ""

'''


def loadFilters(wb):
    trace("loadFilters")
    results = []
    
    try:
        
        ws = wb.get_sheet_by_name('Filters')
        
        lastrow = ws.get_highest_row()
        
        dbRange = "A2:H{0}".format(lastrow)
        
        trace(dbRange)
        
        output = "{0}\t{1}\t{2}"
        phase = None
        for row in ws.iter_rows(dbRange):
            values = [x.internal_value for x in row]
            
            if values[0] is not None:
                data = DataRow()
                data.phaseId = values[0]
                data.phase = values[1]
                data.conditionId = values[2]
                data.condition = values[3]
                data.productId = values[4]
                data.product = values[5]
                data.ptype = values[6]
                results.append(data)
                
        return results
    
    except UnicodeEncodeError:
        traceback.print_exc()
    
    except ValueError:
        traceback.print_exc()
    

def loadPhases(rows):
    phaseDB = {}
    try:
        
        for row in rows:
            
            key = row.phaseId
            if (key is not None): 
                if (key not in phaseDB): 
                    phase = Phase()
                    phase.name = row.phase
                    phaseDB[key] = phase
                
        return phaseDB
    
    except UnicodeEncodeError:
        traceback.print_exc()
    
    except ValueError:
        traceback.print_exc()



def loadConditions(rows):
    results = []
    
    # Encoding options:  ISO-8859-1, UTF-8, UTF-16
    try:
        
        for i in range(5):
            batch = []
            results.append(batch)

        for row in rows:

            key = row.phaseId
            
            if key is not None:
                index = int(float(key)) - 1
                
                conditionId = row.conditionId
                
                if (not conditionExistsInArray(conditionId, results[index])):
#                    trace("{} {}".format(conditionId, row.condition))
                    
                    condition = Condition()
                    condition.id = conditionId
                    condition.name = row.condition
        
                    results[index].append(condition)
            
        return results
    
    except UnicodeEncodeError:
        traceback.print_exc()
    
    except ValueError:
        traceback.print_exc()


def loadProducts(rows):
    resultmap = {}
    bucket = []
    # Encoding options:  ISO-8859-1, UTF-8, UTF-16
    lastkey = ""
    try:
        
        for row in rows:

            nextkey = keyformat.format(row.phaseId, row.conditionId) 
            
            if nextkey != lastkey and lastkey != "":
                # Add to resultmap
                trace("Saving bucket for key {}".format(lastkey))
                resultmap[lastkey] = bucket
                bucket = []

            product = Product()
            product.id = row.productId
            product.name = row.product
            product.type = row.ptype
            
            bucket.append(product)
            lastkey = nextkey
            
        resultmap[lastkey] = bucket
        
        return resultmap
    
    except UnicodeEncodeError:
        traceback.print_exc()
    
    except ValueError:
        traceback.print_exc()
def XXXXX_HELPERS_XXXXX():
    print 'nothing'


def conditionExistsInArray(cid, conditions):
    
    for c in conditions:
        if (c.id == cid):
            return True
        
#    print "Test {} conditionExistsInArray size={}".format(cid, len(conditions))
    return False



def runFilterData(wb):
    
#    Load all rows from Filters worksheet
    rows = loadFilters(wb)
    
#    Create initial dictionary/map of phases: 1.0, 2.0, etc
    db = loadPhases(rows)

#    Read conditions as array of arrays
    conditionsArray = loadConditions(rows)
    index = 1
    
#    Assign conditions back to phases map (db)
    print "conditionsArray size={}".format(len(conditionsArray))
    for conditions in conditionsArray:
        key = "{}.0".format(index)
        print "index={} size={}".format(index, len(conditions))
        
        print key
        if db[key] is not None:
            db[key].conditions = conditions
            
        index += 1
    
    trace('PRODUCT MAP  ============================')
    
#    Create dictionary of products for each condition. Parse key to assign array to matching
#    phase and condition. Seriously, there's got to be a better way than this. HELP!
    productMap = loadProducts(rows)    
    
    for phaseId in db.keys():
        print "phaseId={}".format(phaseId)
        
        for i in range(len(db[phaseId].conditions)):
            try: 
                c = db[phaseId].conditions[i]
                pkey = keyformat.format(phaseId, c.id)
                db[phaseId].conditions[i].products = productMap[pkey]
                
            except ValueError:
                traceback.print_exc()
             
    
    return db




def update_matrix(wb,root):
    print "update matrix", root

    output_file_name = root + '/lib/data/pah-matrix.js'

    phaseDB = runFilterData(wb)
    
    jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=2)
    json = jsonpickle.encode(phaseDB, unpicklable=False)
    output = "PAH.Content.Matrix = {};".format(json)

    outf = open(output_file_name,'w')
    outf.write(output)
    outf.close()
