# _*_ coding: UTF-8 _*_

'''
Created on Jun 11, 2012

@author: hlang

Dependencies: 
openpyxl -- http://packages.python.org/openpyxl/
jsonpickle -- http://jsonpickle.github.com/
simplejson -- http://pypi.python.org/pypi/simplejson/
'''
import logging
import traceback
import jsonpickle
import re

from openpyxl import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet import Worksheet
from openpyxl.namedrange import NamedRange
from openpyxl.reader.worksheet import read_worksheet, read_dimension
from openpyxl.reader.excel import load_workbook
from openpyxl.shared.exc import InvalidFileException

from compiler_global import trace

class Client:
    narc_id = None
    narc_name = None
    business_class = None

class Territory:
    id = None
    manager = None
    area_id = None
    region_id = None
    sales_force_id = None
    clients = []

def load_client_list(wb):
    trace("load_client_list")
    results = {}
    
    try:
        
        sheets = wb.get_sheet_names()
        ws = wb.get_sheet_by_name(sheets[0])
        
        lastrow = ws.get_highest_row()
        trace("lastrow {0}".format(lastrow))
        
        dbRange = "A2:I{0}".format(lastrow)
        
        trace(dbRange)
        
        for row in ws.iter_rows(dbRange):
            values = [x.internal_value for x in row]

            territory_id = str(int(values[3]))

            if territory_id in results:
                territory = results[territory_id]
            else:
                territory = Territory()
                territory.id = territory_id
                territory.manager = values[4]
                territory.area_id = str(int(values[5]))
                territory.region_id = values[6]
                territory.sales_force_id = str(int(values[7]))
                territory.clients = {}
                
            narc_id = str(int(values[0]))

            if not narc_id in territory.clients:
                client = Client()
                client.narc_id = narc_id
                client.narc_name = values[1]
                client.business_class = values[2]
                territory.clients[narc_id] = client

            results[territory_id] = territory

        return results

                
    except UnicodeEncodeError:
        traceback.print_exc()
        return
    
    except ValueError:
        traceback.print_exc()


def update_valueplanner(wb,root):
    trace("update_valueplanner")

    output_file_name = root + '/lib/data/pah-value-planner-accounts.js'

    db = load_client_list(wb)

    jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=4)
    json = jsonpickle.encode(db, unpicklable=False)
    output = "var VP_Content = {};".format(json)

    outf = open(output_file_name,'w')
    outf.write(output)
    outf.close()

