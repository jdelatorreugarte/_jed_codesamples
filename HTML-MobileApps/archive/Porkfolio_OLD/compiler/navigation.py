# _*_ coding: UTF-8 _*_

'''
Created on Jun 11, 2012

@author: hlang

Dependencies: 
openpyxl -- http://packages.python.org/openpyxl/
jsonpickle -- http://jsonpickle.github.com/
simplejson -- http://pypi.python.org/pypi/simplejson/
'''
import logging
import traceback
import jsonpickle
import re

from openpyxl import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet import Worksheet
from openpyxl.namedrange import NamedRange
from openpyxl.reader.worksheet import read_worksheet, read_dimension
from openpyxl.reader.excel import load_workbook
from openpyxl.shared.exc import InvalidFileException


debug = True
#filepath = "/Sandbox/EV/PAH/docs/PAH_pages_08.xlsx"
keyformat = "{}-{}"
filenameFormat = "{}_{}.html"

resultsArray = []
lastpage = ""

class DataRow:
    id0= None
    name0 = ""
    id1 = None
    name1 = ""
    header_title1 = None
    href1 = None
    id2 = None
    name2 = ""
    header_title2 = None
    id3 = None
    name3 = ""
    id4 = None
    name4 = ""
    
class NavData:
    id = ""
    name = ""
    header_name = ""
    href = ""
    nodes = []
    
    
def trace(message):
    if (debug):
        print(message)



def loadNavigation(wb):
    trace("loadNavigation")
    results = []
    
    try:
        
        ws = wb.get_sheet_by_name('Navigation')
        
        lastrow = ws.get_highest_row()
        
        dbRange = "A2:P{0}".format(lastrow)
        
        trace(dbRange)
        
        for row in ws.iter_rows(dbRange):
            values = [x.internal_value for x in row]

            data = DataRow()
            
            data.id0 = values[0]
            data.name0 = values[1]
            data.id1 = values[3]
            data.name1 = values[4]
            data.header_title1 = values[5]
            data.href1 = values[6]
            data.id2 = values[7]
            data.name2 = values[8]
            data.header_title2 = values[9]
            data.id3 = values[11]
            data.name3 = values[12]
            data.id4 = values[13]
            data.name4 = values[14]
            results.append(data)
                
        return results
    
    except UnicodeEncodeError:
        traceback.print_exc()
    
    except ValueError:
        traceback.print_exc()

def readLevel0(pageRows, db):
    navs = []
    for row in pageRows:
        if row.id0 is not None and row.id1 is None:
            navdata = NavData()
            navdata.id = row.id0
            navdata.name = row.name0
            navs.append(navdata)
    
    for nav in navs:
        nodes = filterLevel0(nav.id, pageRows)
        nav.nodes = nodes
        db[nav.id] = nav
    
    return db

def filterLevel0(key, pageRows):
    results = []
    for row in pageRows:
        if row.id0 == key and row.id1 is not None and row.id2 is None:
            node = NavData()
            node.id = row.id1
            node.name = row.name1
            if row.header_title1 is not None:
                node.header_name = row.header_title1
            if row.href1 is not None:
                node.href = row.href1
            results.append(node)
    
    return results        
            
def readLevel1(pageRows, db):
    navs = []
    for row in pageRows:
        if row.id1 is not None and row.id2 is None:
            navdata = NavData()
            navdata.id = row.id1
            navdata.name = row.name1
            if row.header_title1 is not None:
                navdata.header_name = row.header_title1
            navs.append(navdata)
    
    for nav in navs:
        nodes = filterLevel1(nav.id, pageRows)
        nav.nodes = nodes
        db[nav.id] = nav
    
    return db

def filterLevel1(key, pageRows):
    results = []
    for row in pageRows:
        if row.id1 == key and row.id2 is not None and row.id3 is None:
            node = NavData()

            node.id = row.id2
            node.name = row.name2
            if row.header_title2 is not None:
                node.header_name = row.header_title2
            if row.href1 is not None:
                node.href = row.href2
            results.append(node)
    
    return results        

def readLevel2(pageRows, db):
    navs = []
    for row in pageRows:
        if row.id2 is not None and row.id3 is None:
            navdata = NavData()
            navdata.id = row.id2
            navdata.name = row.name2
            if row.header_title2 is not None:
                navdata.header_name = row.header_title2
            navs.append(navdata)
    
    for nav in navs:
        nodes = filterLevel2(nav.id, pageRows)
        nav.nodes = nodes
        db[nav.id] = nav
    
    return db

def filterLevel2(key, pageRows):
    results = []
    for row in pageRows:
        if row.id2 == key and row.id3 is not None and row.id4 is None:
            node = NavData()
            node.id = row.id3
            node.name = row.name3
            results.append(node)
    
    return results        


def conditionExistsInArray(cid, conditions):
    
    for c in conditions:
        if (c.id == cid):
            return True
        
#    print "Test {} conditionExistsInArray size={}".format(cid, len(conditions))
    return False


import os, errno
def mkdirs(newdir, mode=0777):
    try: os.makedirs(newdir, mode)
    except OSError, err:
        # Reraise the error unless it's about an already existing directory 
        if err.errno != errno.EEXIST or not os.path.isdir(newdir): 
            raise


def update_navigation(wb,root):

    output_file_name = root + '/lib/data/pah-navigation.js'

    pageRows = loadNavigation(wb)
    
    db = {}
    
    db = readLevel0(pageRows, db)
    db = readLevel1(pageRows, db)
    db = readLevel2(pageRows, db)

#    jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=4)

    jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=4)
    json = jsonpickle.encode(db, unpicklable=False)
    output = "PAH.Content.NavigationDB = {};".format(json)

    outf = open(output_file_name,'w')
    outf.write(output)
    outf.close()
