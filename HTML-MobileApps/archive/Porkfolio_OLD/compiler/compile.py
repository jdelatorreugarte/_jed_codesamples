#!/usr/bin/python
# _*_ coding: UTF-8 _*_

'''
Created on May 13, 2013

@author: dpetrov

Dependencies: 
openpyxl -- http://packages.python.org/openpyxl/
jsonpickle -- http://jsonpickle.github.com/
simplejson -- http://pypi.python.org/pypi/simplejson/
'''
import logging
import traceback
import jsonpickle
import re
import sys

from openpyxl import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet import Worksheet
from openpyxl.namedrange import NamedRange
from openpyxl.reader.worksheet import read_worksheet, read_dimension
from openpyxl.reader.excel import load_workbook
from openpyxl.shared.exc import InvalidFileException

from compiler_global import trace, set_debug
from navigation   import update_navigation
from page_db      import update_page_db
from filter_db    import update_matrix
from file_tree    import update_files
from products     import update_products
from valueplanner import update_valueplanner

'''
if len(sys.argv) < 3:
    print('Usage: compile <input file> <output tree root>')
    exit(1)
else:
    filename=sys.argv[1]
    root=sys.argv[2]
'''
set_debug(True)


root = '../web'
filename = '../source/PAH_pages_v3.xlsx'
vp_filename = '../source/PAH_VP_accounts.xlsx'

wb = load_workbook(filename=filename, use_iterators=True)
wbvp = load_workbook(filename=vp_filename,use_iterators=True)

update_navigation(wb,root)

update_page_db(wb,root)

update_matrix(wb,root)

update_files(wb,root)

update_products(wb,root)

update_valueplanner(wbvp,root)
