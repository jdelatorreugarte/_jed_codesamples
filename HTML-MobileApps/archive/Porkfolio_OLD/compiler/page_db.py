import jsonpickle
import re

#from openpyxl import load_workbook
from openpyxl.workbook import Workbook
from openpyxl.worksheet import Worksheet
from openpyxl.namedrange import NamedRange
from openpyxl.reader.worksheet import read_worksheet, read_dimension
from openpyxl.reader.excel import load_workbook
from openpyxl.shared.exc import InvalidFileException


from compiler_global import trace

# = "{}-{}"
filenameFormat = "{}_{}.html"

#resultsArray = []
#lastpage = ""


class PageData:
    id = ""
    name = ""
    title = ""
    type = ""
    section = ""
    filename = ""
    filekey = ""
    template = ""


def loadInventory(wb):
    pageRows = []
    # Encoding options:  ISO-8859-1, UTF-8, UTF-16
    try:
        
        ws = wb.get_sheet_by_name('Inventory')
        
        lastrow = ws.get_highest_row()
        
        dbRange = "A2:I{0}".format(lastrow)
        
        print dbRange
        
        output = "{}\t{}"
        
        for row in ws.iter_rows(dbRange):
            values = [x.internal_value for x in row]
            
            if values[3] is not None:
                pagedata = PageData()
                pagedata.id = values[0]
                
                pagedata.name = values[1]
                pagedata.title = values[2]
                pagedata.section = values[3]
                pagedata.template = values[4]
                pagedata.type = values[5]
                
                contentId = values[0]
                
                prefix =''
                num = ''
                filename = ''
                if re.match("^[A-Z]", contentId) is not None:
                    prefix = contentId[0:1] + '_'
                    num = contentId[1:]
                    if re.match("^\d\d", num) is None:
                        num = "0" + num
                else:
                    prefix = ''
                    num = contentId
                    
                filekey = prefix + num.replace('.', '_') 
                
                if values[4] != 'Nav':
                    filename = filenameFormat.format(filekey, values[5])

#                print len(values)
                if len(values) >= 8:
                    pagedata.filename = values[6]
                    pagedata.filekey = values[7]
                
#               This printout can be copy-pasted back into spreadsheet in filename and filekey columns 
                trace(output.format(filename, filekey))   
                
                pageRows.append(pagedata)
            
#            rowout = output.format(values[0], values[1], values[2])
#            print rowout
                
        return pageRows
    
    except UnicodeEncodeError:
        traceback.print_exc()
    
    except ValueError:
        traceback.print_exc()



def update_page_db(wb,root):
    print "update page db", root

    output_file_name = root + '/lib/data/pah-pagedb.js'

    pageDB = {};

    pageRows = loadInventory(wb)
    
    for pageRow in pageRows:
        pageDB[pageRow.id] = pageRow

    jsonpickle.set_encoder_options('simplejson', sort_keys=True, indent=4)
    json = jsonpickle.encode(pageDB, unpicklable=False)
    output = "PAH.Content.PageDB = {};".format(json)

    outf = open(output_file_name,'w')
    outf.write(output)
    outf.close()
