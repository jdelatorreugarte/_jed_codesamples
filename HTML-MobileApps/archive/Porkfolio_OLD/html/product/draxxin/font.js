			window.currentPage = 'homePageFirst';
			window.nextPage = 'homePageFirst';
			
			function gotoNext(){
				var next = getNextPage(window.currentPage);
				gotoPage(next);
			}
			
			function gotoPrev(){
				var prev = getPrevPage(window.currentPage);
				gotoPage(prev);
			}
			
			function getPrevPage(pageId){
				if(window.lookOop[pageId] == undefined) return null;
				return window.lookOop[pageId]['prev'];
			}
			
			function getNextPage(pageId){
				if(window.lookOop[pageId] == undefined) return null;
				return window.lookOop[pageId]['next'];
			}
			
			function gotoPage(pageId){
				if(pageId == null) return;
				if(pageId == window.currentPage) return;
				window.nextPage = pageId;
				hidePage(window.currentPage);
				showPage(window.nextPage);
				window.currentPage = window.nextPage;
			}
			
			function showPage(pageId){
				var page = window.lookOop[pageId]['name'];
				page.animationIn();
			}
			
			function hidePage(pageId){
				var page = window.lookOop[pageId]['name'];
				page.animationOut();
			}
			
			function ensurePos(pageId){
				var page = window.lookOop[pageId]['name'];
				page.ensurePos();
			}
			
			$(document).ready(function(){
				$("#yousavemore").css('opacity', 0);
				$(document ).trigger( "mobileinit" );
				
				window.lookOop = {
					'homePageFirst' : {
							'prev' : null,
							'next' : 'homePageSecond',
							'name' : $('#homePageFirst').data('HomePageFirst')
					},
					'homePageSecond' : {
							'prev' : 'homePageFirst',
							'next' : 'saveWithDraxxin',
							'name' : $('#homePageSecond').data('HomePageSecond')
					},
					'saveWithDraxxin' : {
							'prev' : 'homePageSecond',
							'next' : 'savePigs',
							'name' : $('#saveWithDraxxin').data('SaveWithDraxxin')
					},
					'savePigs' : {
							'prev' : 'saveWithDraxxin',
							'next' : 'saveTime',
							'name' : $('#savePigs').data('SavePigs')
					},
					'saveTime' : {
							'prev' : 'savePigs',
							'next' : 'saveTimeWithGraph',
							'name' : $('#saveTime').data('SaveTime')
					},
					'saveTimeWithGraph' : {
							'prev' : 'saveTime',
							'next' : 'saveMoney1',
							'name' : $('#saveTimeWithGraph').data('SaveTimeWithGraph')
					},
					'saveMoney1' : {
							'prev' : 'saveTimeWithGraph',
							'next' : 'saveMoney2',
							'name' : $('#saveMoney').data('SaveMoney')
					},
					'saveMoney2' : {
							'prev' : 'saveMoney1',
							'next' : 'learnMore',
							'name' : $('#saveMoney').data('SaveMoney')
					},
					'learnMore' : {
							'prev' : 'saveMoney2',
							'next' : null,
							'name' : $('#learnMore').data('LearnMore')
					}
				}
				resizeRadioHustle();
				$(window).resize(function(){
					resizeRadioHustle();
				});
				
				window.isiPad = navigator.userAgent.match(/iPad/i) != null;
				// window.isiPad = true;
				
				if(window.isiPad){
					$('#yousavemore img').css('top', 18);
				}else{
					$('#yousavemore img').css('top', 22);
				}
				
				if(window.isiPad){
					window.onorientationchange = function() {
						resizeRadioHustle();
						ensurePos(window.currentPage);
					};
				}
				
				$("#preloader").show();
				$("#draxxin").hide();
				
				var images = $("#draxxin").find('img');
				window.count = 0;
		       	images.bind("load", function() {
		       		window.count++;
		       		// console.log('attribute: -' + (300 - Math.round(window.count * 300/ images.length)));
		       		$("#percent").stop().animate({left: -(300 - Math.round(window.count * 300/ images.length)) + 'px'}, 1000, 'linear');
		       	});
		       	
		       	$(document).bind("swipeleft", function(){
						gotoNext();
		       	});
		       	
		       	$(document).bind("swiperight", function(){
						gotoPrev();
		       	});
		       	
			});
			
			
			
			
