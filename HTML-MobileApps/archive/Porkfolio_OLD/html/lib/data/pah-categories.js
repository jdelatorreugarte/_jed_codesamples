PAH.Content.Categories = [
    {name:"Injectable Anti-Infectives"
    	, products: ["P5.0","P7.0","P8.0","P17.0","P19.0"]},
    {name:"Feed Additives"
    	, products: ["P1.0","P2.0","P3.0","P4.0","P16.0"]},
   	{name:"Vaccines"
   		, products: ["P23.0","P6.0","P9.0","P11.0","P13.0","P14.0","P18.0","P20.0","P21.0"]},
   	{name:"IMPROVEST"
   		, products: ["P15.0"]},
   	{name:"Diagnostics"
   		, products: ["P10.0"]},
    {name:"Other"
    	, products: ["P31.0"]},
    {name:"Services"
    	, products: ["S5.0","S1.0","S2.0","S3.0"]},
    {name:"Supportive Therapies"
    	, products: ["P32.0","P33.0"]},
    {name:"Water-Based Therapies"
    	, products: ["WT1.0","WT2.0","WT3.0","WT4.0","WT5.0","WT6.0"]},
];
