PAH.Content.Matching = {
  "Clostridium":  ['LitterGuard','LitterGuard LT-C','BMD' ],
  "E. coli":  ['LitterGuard','LitterGuard LT-C','Aureomycin soluble','Neo-Sol','Oxytet soluble','Tet-Sol' ],
  "Erysipelas":  ['Lincomix Injectable','ER Bac/L5 GOLD','ER Bac Plus','FarrowSure GOLD','FarrowSure GOLD B','Suvaxyn E-oral' ],
  "Leptospirosis":  ['FarrowSure GOLD','FarrowSure GOLD B','ER Bac/L5 GOLD','ER Bac Plus' ]
};