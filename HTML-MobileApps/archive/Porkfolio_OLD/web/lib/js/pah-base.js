var PAH = {
	enabled : false,
	log : function(txt){
		if (!this.enabled) return;
		if (typeof(console) !== "undefined") {
			console.log(">> " + txt + " ");
		}
	},
	getQSValue: function(key) {
        var querystring = location.search.replace(/\?/, "");
        var parts = querystring.split("&");
        var match = {};
        for (var L = 0; L < parts.length; L++) {
            var kvpair = parts[L].split("=");
            match[kvpair[0]] = kvpair[1];
        }
        if (match[key]) {
            return match[key];
        } else {
            return null;
        }
    }
};

PAH.Content = {
};