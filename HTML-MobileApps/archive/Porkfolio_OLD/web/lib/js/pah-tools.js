PAH.Tools = {
	phaseId: "",
	conditionId: "",
	productId: "",
	pageId: "",
	url: 'pfizer://master-rep/open-external-pdf?url=lib/pdf/',
	openPdf: function(dataPdf){
		// presenter.tracking.action("pdf", dataPdf);
		window.location.href = url + dataPdf + '&title=' + dataPdf;
	},
	lookupPage: function(pageId) {
		var page = PAH.Content.PageDB[pageId];
		if (page != null) {
			return page;
		}
		return null;
	},
	fetchContentMatrix: function() {
		var page = PAH.Content.Matrix[PAH.Tools.phaseId];
		if (page != null) {
			return page;
		}
		return null;
	},
	showDefaultFilter: function() {
		$(".reset-button").each(function(index) {
			$(this).hide();
		});		
		$('.body-phase ul.nav').children().removeClass('selected');
		PAH.Tools.loadConditionHeader(PAH.Tools.phaseId);
		var filterlist = PAH.Tools.findConditionProducts(PAH.Tools.phaseId);
		PAH.Tools.loadProductsFilter(filterlist);
	},
	findConditionProducts: function(conditionId) {
		var phase = PAH.Content.Matrix[PAH.Tools.phaseId];
		
		if (phase != undefined && phase != null && phase.conditions != undefined) {
			var condition = PAH.Tools.findObjectById(conditionId, phase.conditions);
			if (condition != null && condition.products != undefined) {
				return condition.products;
			}
		}
		return null;
	},
	loadConditionHeader: function(conditionId) {
		var topDiv = $("#content-top");
		topDiv.empty();
		
		if (conditionId.indexOf("C") == 0) {
			$(".reset-button").each(function(index) {
				$(this).show();
			});		

			var page = PAH.Tools.lookupPage(conditionId);
			topDiv.load("../condition/" + page.filename);
		}
		
	},
	loadDefaultContent: function(navId) {
		var pageId = PAH.Tools.identifyTargetPage(navId);
		PAH.Tools.showSelectedNav(pageId.replace(/\./g,'_'));
		PAH.Tools.loadProductContent(navId);
	},
	loadProductContent: function(navId) {
		var contentDiv = $("#content");
		contentDiv.css('-webkit-transition','opacity 0.1s').css('opacity','0');
		setTimeout(function(){
			contentDiv.empty();
			
			var pageId = PAH.Tools.identifyTargetPage(navId);
			var page = PAH.Tools.lookupPage(pageId);
			PAH.Tools.pageId = pageId;
			
			var body = $('body');
			var page_ids = /([P|S])(\d+)\.(\d+).*/.exec(pageId);
			var re_page_class = /page-\S+/;
			body.attr('class',body.attr('class').split(' ').filter(function(el,index,array){
					return !re_page_class.test(el);
				})
				.concat('page-'+pageId.replace(/\./g,'_'))
				.concat('page-dot-'+page_ids[3])
				.concat(page_ids[3] == 1 ? 'page-key-benefits' : 'page-content')
				.join(' '));
			
			var page_record = PAH.Content.NavigationDB[page_ids[1]+page_ids[2]+'.'+page_ids[3]];
			
			$(".pah-header > div").html(
				page_ids[3] == 1 ? 
					"" : (page_record.header_name || page_record.name) );
			
			var subpath;
			if (page.filename.indexOf("P") == 0) {
				subpath = "../product/"; 
			} else if (page.filename.indexOf("S") == 0) {
				subpath = "../service/"; 
			} else {
				subpath = "";
			}
			var fpath = subpath + page.filename;
			
			contentDiv.load(fpath);

			contentDiv.css('-webkit-transition','').css('opacity','1');
			// Try to find parentId by clipping last dot-number from pageId
			if (pageId.lastIndexOf(".") > -1) {
				var parentId = pageId.substring(0, pageId.lastIndexOf("."));

				// Shameless hack to determine which variable to use when calling showSelectedSubNav
				if (parentId.split(".").length == 2) {
					PAH.Tools.showSelectedSubNav(pageId.replace(/\./g,'_'));	
				} else {
					PAH.Tools.showSelectedSubNav(parentId.replace(/\./g,'_'));
				}

				PAH.Tools.loadLevel3Nav(parentId, pageId);			
			}
		},300);
	},
	// After nav click, determine pageId to load. Navigate recursively for first nav node with no child nodes
	identifyTargetPage: function(pageId) {
		var nav = PAH.Content.NavigationDB[pageId];
		if (nav == undefined) {
			return pageId;
		} else if (nav.nodes.length == 0) {
			return pageId;
		} else {
			var subnav = nav.nodes[0];
			PAH.log("found subnav: " + subnav.id + " // " + subnav.name);
			return PAH.Tools.identifyTargetPage(subnav.id);
		}
	},
	loadLevel3Nav: function(navId, selectedId) {
		var navDiv = $("#toggle-nav");
		navDiv.empty();
		
		if (navId.split(".").length >= 3) {
			var nav = PAH.Content.NavigationDB[navId];
			var node;
			var itemId;
			if (nav.nodes != undefined && nav.nodes.length > 0 ) {
				for (var i=0; i < nav.nodes.length; i++) {
					node = nav.nodes[i];
					PAH.log("level 3: " + node.id + " // " + node.name);
				
			        $navitem = $('<span/>').addClass("navitem").appendTo(navDiv);
					
					if ( node.tertiary ) {
						$navitem.addClass("tertiary");
					}

					$a = $('<a/>').attr('rel', node.id).html(node.name).appendTo($navitem).bind('click',
							function(e){
								e.preventDefault();
							
								// TODO: Attach event to expand submenu
								PAH.log("CLICK: " + this.rel);
								PAH.Tools.loadProductContent(this.rel);
			             	});
					if (node.id == selectedId) {
						$navitem.addClass("selected brand-color");
					}
					
					if ( i != nav.nodes.length-1 ) {
						$('<span/>').addClass('divider').appendTo(navDiv);
					}
					
				}
			} 
			
		} else {
			PAH.log("Ignoring level 2 and above");
		} 
	},
	// Use findConditionProducts first to get array of products. Use this to reload #content
	loadProductsFilter: function(products) {
		var bodyDiv = $("#content-body");
		bodyDiv.empty();
		var product;

        $tiles = $('<ul/>').addClass("thumbnails").appendTo(bodyDiv);
		var subpath;
		for (var i=0; i < products.length; i++) {
			product = products[i];
			PAH.log(product.id + " // " + product.name);
			page = PAH.Tools.lookupPage(product.id);
			
			if (page != undefined) {
				var fpath;

				if (page.filename.indexOf("P") == 0) {
					subpath = "../product/"; 
				} else if (page.filename.indexOf("S") == 0) {
					subpath = "../service/"; 
				} else {
					subpath = "";
				}
				fpath = subpath + page.filekey + "_index.html";
				
            	$item = $('<li/>').attr({
				    'data-key': page.filekey,
				    'data-filepath': fpath 
				}).addClass("thumbnail span10 producttile").appendTo($tiles);
				
				if ( i < products.length-1 ) {
					$item.load(subpath + page.filename);
				} else {
					$item.load(subpath + page.filename,function(){
					
						// Some products do not have detail content. Only open if there are nav nodes
						// var navdata = PAH.Content.NavigationDB[product.id];
						// if (navdata != undefined && navdata.nodes.length > 0) {
					
						$(this).parent().find('div.product-title').bind('click',function(e){
							$tile = $(this).parents('li.producttile');
							e.preventDefault();

							PAH.log("PRODUCT CLICK: " + $tile[0].dataset.key);

							var obj = PAH.Tools.findByFileKey($tile[0].dataset.key);
							
							// load frame only if key benefits page is defined in the database
							if (PAH.Content.PageDB[obj.id.replace(".0",".1")]) {
								
								PAH.Tools.loadProductFrame($tile[0]);
							
							}
					
						});
						$(this).append(
							$('<script/>')
								.attr('type','text/javascript')
								.html('setTimeout(function(){content_scroll.refresh();content_scroll.scrollTo(0,0);},200);')
						);
					});
				
				}
			}
		}
		
	},
	// load product lightbox and default content from data-filepath attribute
	loadProductFrame: function(element) {
		$('#poplayer').modal();

		PAH.log(element.dataset.filepath);
		$('<iframe />', {
			src:  element.dataset.filepath
		}).appendTo('#popframe');
		
		// on modal hide event, remove iframe
		$('#poplayer').on('hide', function () {
			$('.modal').css('opacity','');
			$('.modal-backdrop').css('opacity','1');
			setTimeout(function(){
				$('#popframe').empty();
				$('.modal-backdrop').css('opacity','');
			},300);
		});

		setTimeout(function(){
			$('.modal').css('opacity','1');
		},500);
	},

	// Generic method for finding an object in a collection that has a property "id"
	findObjectById: function(id, objects) {
		var object;
		for (var i=0; i<objects.length; i++) {
			object = objects[i];
			if (object.id == id) {
				return object;
			}
		}
		return null;
	},

	findByFileKey: function(file_key) {
		for (id in PAH.Content.PageDB) {
			if ( PAH.Content.PageDB[id].filekey && PAH.Content.PageDB[id].filekey == file_key ) {
				return PAH.Content.PageDB[id];
			}
		}
		return null;
	},

	// Only used by all.html
	buildPhaseNav: function(pageMatrix) {
		var leftnav = $("#left-nav > .inner");
		leftnav.find("ul").remove();
		
		var $ul = $('<ul/>').addClass("nav nav-pills nav-stacked well").appendTo(leftnav);

		var condition;
		
		for (var i=0; i < pageMatrix.conditions.length; i++) {
			condition = pageMatrix.conditions[i];
			PAH.log(condition.id + " // " + condition.name);
			
			if (condition.id.indexOf("C") == 0) {
				var itemId = condition.id.replace(/\./g,'_');
	            $item = $('<li/>').appendTo($ul);
				$a = $('<a/>').attr('rel', condition.id).html(condition.name).appendTo($item).bind('click',
					function(e){
						e.preventDefault();
						PAH.log("CLICK: " + this.rel);
						$(e.target).parents('ul').children().removeClass('selected');
						$(e.target).parents('li').addClass('selected');
						PAH.Tools.loadConditionHeader(this.rel);
						var filterlist = PAH.Tools.findConditionProducts(this.rel);
						PAH.Tools.loadProductsFilter(filterlist);
	             	});				
			}
		}
		
		PAH.Tools.recalcConditionsNavHeight();
	},
	initPhaseNav: function() {
		$('#left-nav-buttons a').click(function(){
			PAH.log("CLICK: " + this.rel);
			PAH.Tools.loadConditionHeader(this.rel);
			var filterlist = PAH.Tools.findConditionProducts(this.rel);
			PAH.Tools.loadProductsFilter(filterlist);
		});
	},
	conditionClick: function(o) {
		PAH.log("CLICK: " + o.rel);
		PAH.Tools.loadConditionHeader(o.rel);
		var filterlist = PAH.Tools.findConditionProducts(o.rel);
		PAH.Tools.loadProductsFilter(filterlist);
	},
	buildProductNav: function(pageId) {
		var leftnav = $("#left-nav");
		leftnav.empty();
        var $ul = $('<ul/>').addClass("nav nav-pills nav-stacked level1").appendTo(leftnav);

		var nav = PAH.Content.NavigationDB[pageId];
		var node;
		var itemId;
		if (! nav) {
			console.log('missing page ' + pageId);
		}
		if (nav && nav.nodes != undefined) {
			for (var i=0; i < nav.nodes.length; i++) {
				node = nav.nodes[i];
				PAH.log(node.id + " // " + node.name);
				itemId = node.id.replace(/\./g,'_');
	            $item = $('<li/>').attr({
				    'id': itemId
				}).appendTo($ul);
	
				var subnav  = PAH.Content.NavigationDB[node.id];
				var subnode;
				if (subnav != undefined && subnav.nodes.length > 0) {
					// Subnav nodes exist, so create submenu and hide it until parent node is clicked

					$a = $('<a/>').html('<div><div>'+node.name+'</div></div>').attr('rel', node.id).appendTo($item).bind('click',
						function(e){
							e.preventDefault();
							
							// TODO: Attach event to expand submenu
							PAH.log("CLICK: " + this.rel);
							var navId = "subnav_" + this.rel.replace(/\./g,'_');
							PAH.Tools.showSelectedNav(this.rel.replace(/\./g,'_'));
							PAH.Tools.toggleLeftNav(navId);
							
							PAH.Tools.loadProductContent(this.rel);
		             	});

					// Set display:none for this <ul> ?
					var subitemId;
					var subnavId = "subnav_" + node.id.replace(/\./g,'_');
        			var $ul2 = $('<ul/>').attr('id', subnavId).addClass("nav nav-list level2").appendTo($item);
					$ul2.hide();
					
					for (var j=0; j < subnav.nodes.length; j++) {
						subnode = subnav.nodes[j];
						subitemId = subnode.id.replace(/\./g,'_');
						PAH.log(subnode.id + " ======== has id ========" + subitemId);
						
			            $subitem = $('<li/>').attr({
						    'id': subitemId
						}).appendTo($ul2);
						
						$a = $('<a/>').attr({
							'rel': subnode.id  
						}).html('<div><div><ul><li class="brand-ul"><div>'+subnode.name+'</div></li></ul></div></div>').appendTo($subitem).bind('click',
							function(e){
								e.preventDefault();
								PAH.log("Subnav click: " + this.rel);
								PAH.Tools.loadProductContent(this.rel);
			             	});
						
					}
					
				} else {
					// Node with no subnodes. Load content on click.
					if (node.href) {
						$a = $('<a/>').attr({
							'href': node.href  
						}).html('<div><div>'+node.name+'</div></div>').appendTo($item);
					} else {
						$a = $('<a/>').attr({
							'rel': node.id  
						}).html('<div><div>'+node.name+'</div></div>').appendTo($item).bind('click',
							function(e){
								e.preventDefault();
								PAH.log("CLICK: " + this.rel);
								// toggle left nav. this will close any expanded subnavs 
								PAH.Tools.showSelectedNav(this.rel.replace(/\./g,'_'));
								PAH.Tools.toggleLeftNav(this.rel);
								PAH.Tools.loadProductContent(this.rel);
							});
					}
				}
			}
			
		}
	},
	showSelectedNav: function(navId) {
		$('.level1 li').each(function(index) {
			// PAH.log("level1 nav " + $(this).attr('id'));
			if ($(this).attr('id') == navId) {
				$(this).addClass("selected ");
			} else {
				$(this).removeClass("selected ");
			}
		});		
		
	},
	showSelectedSubNav: function(navId) {
		PAH.log("showSelectedSubNav " + navId);
		$('.level2 li').each(function(index) {
			PAH.log("level2 nav " + $(this).attr('id'));
			if ($(this).attr('id') == navId) {
				$('a',this).addClass("brand-color");
				$('li',this).addClass("selected brand-ul");
			} else {
				$('a',this).removeClass("brand-color");
				$('li',this).removeClass("selected brand-ul");
			}
		});		
		
	},
	toggleLeftNav: function(navId) {
		PAH.log("toggleLeftNav " + navId);
		$('.level2').each(function(index) {
			// PAH.log("level2 nav " + $(this).attr('id'));
			if ($(this).attr('id') == navId) {
				if($(this).css('display') == 'none'){
					PAH.log("show: " + navId);
					$(this).show();
				} else {
					PAH.log("hide: " + navId);
					$(this).hide();
				}
			} else {
				PAH.log("not selected " + $(this).attr('id'));
				$(this).hide();
			}
		});		
	},
	
	gotoContent: function(navId) { // format: Px.x.x
		PAH.Tools.loadProductContent(navId);
		var parts = navId.split('.');
		PAH.Tools.showSelectedNav(parts[0] + '_' + parts[1]);
		PAH.Tools.toggleLeftNav('subnav_'+parts[0]+'_'+parts[1]);
		PAH.Tools.showSelectedSubNav(parts[0]+'_'+parts[1]+'_'+parts[2]);
	},
	
	playAudio: function(div_selector) {
	
		var div_audio = $(div_selector);
	
		div_audio.click(function(e){
			var $this = $(this);
			var $audio = $('audio',this);
			if ( $this.hasClass('canplay') && $this.hasClass('extended') ) {
				if ( $this.hasClass('playing') ) {
					$audio[0].pause();
				} else {
					$audio[0].play();
				}
				$this.toggleClass('playing');
			}
		});
		
		$('audio',div_audio).bind('canplay',function(e){
			PAH.log('can play');
			$(this).parent().parent().addClass('canplay').removeClass('loading');
		});

		$('.icon',div_audio).click(function(e){
			PAH.log('div.audio icon click');
			var $this_parent = $(this).parent();
			$this_parent.removeClass('canplay playing').addClass('loading').toggleClass('extended');
			$('audio',$this_parent)[0].load();
		});

	
	/*
		var audio_el = $('audio',$(this).parent())[0];
		if ( audio_el.playing ) {
			audio_el.load();
			audio_el.playing = false;
			$(this).parent().removeClass('extended');
		} else {
			audio_el.play();
			audio_el.playing = true;
			$(this).parent().addClass('extended');
		}
	*/
	},

	videoIconHandler: function(div_selector) {
	
		var div_video = $(div_selector);
	
		var video = $('video',div_video);

		$('.go',div_video).css('visibility','hidden');
		
		video.bind('canplay',function(e){
			PAH.log('can play');
			$('.go',div_video).css('visibility','visible');
		});
		
		video.bind('webkitendfullscreen',function(e){
			this.pause();
		});
		
		div_video.click(function(e){
			PAH.log('div.video click');
			var $this = $(this);
			var video = $('video',this);
			if ( $this.hasClass('extended') && $('.go',this).css('visibility') == 'visible' ) {
				PAH.log('div.video extended, ready');
				video[0].play();
				if (video[0].webkitSupportsFullscreen) {
					video[0].webkitEnterFullscreen();
				}
				$('.go',this).css('visibility','hidden');
				$this.toggleClass('extended');
			} else if ( !$this.hasClass('extended') ) {
				PAH.log('div.video closing');
				video[0].load();
				$this.toggleClass('extended');
			}
			
		});
	},
	
	documentIconHandler: function(div_selector) {
		
		var $div_selector = $(div_selector);
		var $icon = $('.icon',$div_selector);
		
		$icon.click(function(e){
			var $this = $(this);
			$this.parent().toggleClass('extended');
		});
		
		
	
	},

	setupTertiaryNav: function() {
		$('div.tertiary > ul > li.action').click(function(){
			$(".tertiary-container").load("../product/"+$(this).attr("data-content-file"));
		});
	},
	
	recalcConditionsNavHeight: function() 
	{
		$('#allpages #left-nav').removeClass('scrollable-down scrollable-up');
		
		window.setTimeout(
			function(){
				var inner_height = parseInt($('#allpages #left-nav .inner').css('max-height'));
				var inner_ul_height = $('#allpages #left-nav .inner > .nav').height();
				
				if ( inner_ul_height > inner_height ) {
					$('#allpages #left-nav').addClass('scrollable-down').removeClass('scrollable-up');
					$('#allpages #left-nav .bottom').unbind();
					$('#allpages #left-nav .bottom').bind('touchstart',function(e){
						var $this = $(this);
						$this.parent().toggleClass('scrollable-up scrollable-down');
						if ( $this.parent().hasClass('scrollable-up') ) {
							$('#allpages #left-nav .inner > .nav').css('margin-top',inner_height-inner_ul_height);
						} else {
							$('#allpages #left-nav .inner > .nav').css('margin-top','');
						}
					});
				}
			}
			,0
		);
	}

};

var init_toggler = function() {
	var li_selected_index = 1;
	var nav_ul = $('div.kb-nav ul');
	var nav_lis = $('li',nav_ul);
	var old_slide;
	var new_slide;
	$('.kb-nav-li-left').add('.kb-nav-left').click(function(e) {
		if ( li_selected_index > 1 ) {
			old_slide = $('.kb-main#main-'+(li_selected_index));
			$(nav_lis[li_selected_index]).removeClass('kb-selected');
			$('.kb-callout#callout-'+(li_selected_index)).hide();
			li_selected_index--;
			new_slide = $('.kb-main#main-'+(li_selected_index));
			$(nav_lis[li_selected_index]).addClass('kb-selected');
		    // skip #4
			if ((li_selected_index) != 4) {
			    $('.kb-callout#callout-' + (li_selected_index)).show();
			}
			old_slide.addClass('from-center-to-right');
			new_slide.addClass('from-left-to-center');
			setTimeout(function() {
				$('.kb-main').removeClass('center from-center-to-left from-right-to-center from-center-to-right from-left-to-center');
				new_slide.addClass('center');
			}, 500);
		}
	});
	$('.kb-nav-li-right').add('.kb-nav-right').click(function (e) {

		if ( li_selected_index < nav_lis.length - 2 ) {
			old_slide = $('.kb-main#main-'+(li_selected_index));
			$(nav_lis[li_selected_index]).removeClass('kb-selected');
			$('.kb-callout#callout-'+(li_selected_index)).hide();
			li_selected_index++;
			new_slide = $('.kb-main#main-'+(li_selected_index));
			$(nav_lis[li_selected_index]).addClass('kb-selected');
            // skip #4
			if ((li_selected_index) != 4) {
			    $('.kb-callout#callout-' + (li_selected_index)).show();
			}
			old_slide.addClass('from-center-to-left');
			new_slide.addClass('from-right-to-center');
			setTimeout(function() {
				$('.kb-main').removeClass('center from-center-to-left from-right-to-center from-center-to-right from-left-to-center');
				new_slide.addClass('center');
			}, 500);
		}
	});
	var touch_start = function(e){
		window.swipe = {};
		window.swipe.te = {};
		window.swipe.te.pageX = e.originalEvent.touches[0].pageX;
		window.swipe.te.identifier = e.originalEvent.touches[0].identifier;
		window.swipe.time = new Date().getTime();
		//console.log("touch start " + window.swipe.time + " " + window.swipe.te.pageX + " " + window.swipe.te.identifier);
		//e.preventDefault();
	};
	var touch_move = function(e){
		//console.log("touch move");
		e.preventDefault();
	};
	var touch_end = function(e){
		var ctime = new Date().getTime();
		var ct = e.originalEvent.changedTouches[0];
		//console.log("toucn end " + ctime + " " + ct.pageX + " " + ct.identifier + " " + typeof window.swipe);
		if ( typeof window.swipe == 'object' && ct.identifier == window.swipe.te.identifier && ctime - window.swipe.time < 500 ) {
			console.log("x diff = " + (ct.pageX - window.swipe.te.pageX));
			if ( ct.pageX - window.swipe.te.pageX > 50 ) {
				console.log('left click');
				$('.kb-nav-li-left').click();
				window.swipe = undefined;
			} else 
			if ( ct.pageX - window.swipe.te.pageX < -50 ) {
				console.log('right click');
				$('.kb-nav-li-right').click();
				window.swipe = undefined;
			}
		}
	};
	//$('#content').bind('touchmove',function(e){e.preventDefault();});
	//$('#content > div')
	$('.kb-main')
		.bind('touchstart',touch_start)
		.bind('touchmove',touch_move)
		.bind('touchend',touch_end)
		.bind('touchcancel',touch_end)
		.bind('touchleave',touch_end);
};

function init_p13kbp5() {
	$("#main-5")
		.bind('touchmove',function(e){
			var ct = e.originalEvent.changedTouches[0];
			if ( typeof this.scrl == 'object' && ct.identifier == this.scrl.identifier ) {
				$('div',this)[0].scrollTop = this.scrl.top + (this.scrl.mouseY - ct.pageY);
			}
		})
		.bind('touchstart',function(e){
			this.scrl = {};
			this.scrl.top = $('div',this)[0].scrollTop;
			this.scrl.mouseY = e.originalEvent.touches[0].pageY;
			this.scrl.identifier = e.originalEvent.touches[0].identifier;
		})
		.bind('touchend',function(e){
			this.scrl = undefined;
		});
}

