// JavaScript Document
$(document).ready(function(){
	var clickCount = 0;
	$(".hot").click(function(){
		butID = $(this).attr('btnID');
		sryingeID = $(this).attr('sry');
		
		aniID = $(this).attr('ani');
		if(clickCount == 0){
			$(butID).fadeIn();
			$('.syr img, .txtx').show();
			$(sryingeID).hide();
			clickCount++;
			} else if(clickCount == 1 && $(butID).is(':visible')) {
				$(butID).fadeOut();
				$(aniID).fadeIn();
				$('#lightboxMask').fadeIn();
				clickCount++;
				$('.hot').addClass('below');
				if (aniID == '#ani1'){
					ani1()
				} else if(aniID == '#ani2'){
					ani2();
				} else if(aniID == '#ani3'){
					ani3();
				} else if(aniID == '#ani4'){
					ani4();
				};
			} else if(clickCount == 1 && $(butID).is(':hidden')){
					$(butID).fadeIn().siblings().fadeOut();
					$('.syr img, .txtx').show();
					$(sryingeID).hide();
				} else if(clickCount == 2 && $(aniID).is(':hidden')){
					resetAll();
				}
		});
	
	$('#hotspot5').click(function(){
		if($('#reference').is(':hidden')){
				resetAll();
				$('#reference, #lightboxMask').fadeIn();
				$('.hot').addClass('below');
				$('#littleArrow').addClass('rota');
				$('#hotspot5').addClass('encima');
			} else {
				resetAll();
			}
		});
		
	$('#lightboxMask').click(function(){
			resetAll();
		});
	/*$('#dropdown').click(function(){
		$('#lightboxMask, #dropText').fadeIn();
		$('#dropArrow').addClass('arrowAni');
		$('#dropArrow, #dropdown').addClass('above');
		});*/
		
	function resetAll(){
			$('#main').find('*').stop(true, false);
			$('.aniTxt').hide();
			$('#lightboxMask, .viz').fadeOut(200, init());
			$('#dropArrow').removeClass('arrowAni');
			$('#dropArrow, #dropdown').removeClass('above');
			$('.syr img, .txtx').show();
			clickCount = 0;
			$('.hot').removeClass('below');
			$('#littleArrow').removeClass('rota');
			
			/*init();*/
		};
		
	function ani1(){
			$('#ani1 .b1').animate({
					top:39,
					opacity:1
				}, 1000);
			$('.txt').delay(1000).animate({opacity:1});
			$('.arrow1').delay(1100).animate({
					left:640,
					opacity:1
				}, 1000);
			$('.arrow2').delay(1100).animate({
					left:425,
					opacity:1
				}, 1000);
		};
		
	function ani2(){
			$('#ani2 .b1').animate({
					top:39,
					opacity:1
				});
			$('#ani2 .b2').delay(400).animate({
					top:36,
					opacity:1
				});
			$('#ani2 .b3').delay(900).animate({
					top:36,
					opacity:1
				});
			$('#ani2 .b4').delay(1400).animate({
					top:36,
					opacity:1
				});
			$('#ani2 .txt').delay(1800).animate({
					opacity:1
				});
			
		};
		
	function ani3(){
			$('#ani3 .b1').animate({
					top:33,
					opacity:1
				});
			$('#ani3 .b2').delay(200).animate({
					top:30,
					opacity:1
				});
			$('#ani3 .b3').delay(400).animate({
					top:31,
					opacity:1
				});
			$('#ani3 .b4').delay(600).animate({
					top:31,
					opacity:1
				});
			$('#ani3 .b5').delay(800).animate({
					top:31,
					opacity:1
				});
			$('#ani3 .b6').delay(1000).animate({
					top:31,
					opacity:1
				});
			$('#aniContainer').delay(1400).animate({
					top:41
				}, 1000);
			$('#h1').delay(1800).fadeIn('fast',h2);
		};
	function ani4()
	{
		$('#ani4 .b1').animate({
					top:33,
					opacity:1
				});
			$('#ani4 .b2').delay(200).animate({
					top:30,
					opacity:1
				});
			$('#ani4 .b3').delay(400).animate({
					top:31,
					opacity:1
				});
			$('#ani4 .b4').delay(600).animate({
					top:31,
					opacity:1
				});
			$('#ani4 .b5').delay(800).animate({
					top:31,
					opacity:1
				});
			$('#ani4 .b6').delay(1000).animate({
					top:31,
					opacity:1
				});
			$('#aniContainera').delay(1400).animate({
					top:41
				}, 1000);
			$('#h1a').delay(1800).fadeIn('fast',h2a);
		
	}
	//third row animation fade ins
	function h2()
	{
		$('#h1').delay(2200).fadeOut();
		$('#h2').delay(2200).fadeIn('fast', h3);
	}
	function h3()
	{
		$('#h2').delay(4200).fadeOut();
		$('#h3').delay(4200).fadeIn('fast', h4);
	}
	function h4()
	{
		$('#h3').delay(4200).fadeOut();
		$('#h4').delay(4200).fadeIn();
	}
	//fourth row animation fade ins
	function h2a()
	{
		$('#h1a').delay(2200).fadeOut();
		$('#h2a').delay(2200).fadeIn('fast', h3a);
	}
	function h3a()
	{
		$('#h2a').delay(4200).fadeOut();
		$('#h3a').delay(4200).fadeIn('fast', h4a);
	}
	function h4a()
	{
		$('#h3a').delay(4200).fadeOut();
		$('#h4a').delay(4200).fadeIn();
	}
	function init(){
			$('#hotspot5').removeClass('encima');
			$('.block').css('top', 139);
			$('.txt').css('opacity', 0);
			$('.arrow1').css({'opacity' : 0, 'left' : 545});
			$('.arrow2').css({'opacity' : 0, 'left' : 525});
			$('#aniContainer, #aniContainera').css('top', 15);
			$('#h1, #h2, #h3, #h4, #h1a, #h2a, #h3a, #h4a').css('display', 'none');
		};
	
	
});
